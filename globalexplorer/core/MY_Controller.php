<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class MY_Controller extends CI_Controller {
	
	/**
	 * @var boolean
	 */
	private $publicAccessible = FALSE;
	
	/**
	 * @var boolean
	 */
	private $allAccess = FALSE;
	
	/**
	 * @var string
	 */
	private $controllerName;
	
	/**
	 * Constructor
	 */
	public function __construct() {
		parent::__construct();

		$this->output->set_header("Cache-Control", "no-cache, no-store, must-revalidate");
		$this->load->model('user/userpersistence');
		$this->controllerName = $this->router->fetch_class();
		$this->methodName = $this->router->fetch_method();
		
		if (FALSE === $this->getPublicAccessible()) {
			if (!$this->isLoggedIn() && !$this->getAllAccess()) {
				redirect('errors/restricted_area');
			}
		} else {
			if ($this->isLoggedIn() && $this->getPublicAccessible()) {
				redirect('panel');
			}
		}
	}
	
	/**
	 * @return boolean
	 */
	protected function getPublicAccessible() {
		return $this->publicAccessible;
	}
	
	/**
	 * @param boolean $access
	 */
	protected function setPublicAccessible($access) {
		if (is_bool($access)) {
			$this->publicAccessible = $access;
		}
	}
	
	/**
	 * @return boolean
	 */
	protected function getAllAccess() {
		return $this->allAccess;
	}
	
	/**
	 * @param boolean $access
	 */
	protected function setAllAccess($access) {
		if (is_bool($access)) {
			$this->allAccess = $access;
		}
	}
	
	/**
	 * @param string $methodName
	 * @param array $data
	 */
	protected function publicView($data = null, $headData = null, $footData = null) {
		if (!$this->methodName) return;
		$viewPath = $this->createViewPath();
		if (!$viewPath) return;
		$headData['page'] = $this->methodName;
		$this->load->view('partials/public/header', $headData);
		$this->load->view($viewPath, $data);
		$this->load->view('partials/public/footer', $footData);
	}
	
	/**
	 * @param string $methodName
	 * @param array $data
	 */
	protected function panelView($data = null, $headData = null, $footData = null) {
		if (!$this->methodName) return;
		$viewPath = 'panel/' . $this->createViewPath();
		if (!$viewPath) return;
		$headData['page'] = $this->methodName;
		$this->load->view('partials/panel/header', $headData);
		$this->load->view($viewPath, $data);
		$this->load->view('partials/panel/footer', $footData);
	}
	
	/**
	 * @return boolean
	 */
	protected function isLoggedIn() {
		return ($this->userpersistence->getPersistedUser()) ? true : false;
	}
	
	/**
	 * @param string $str
	 * @return Ambigous <NULL, string>
	 */
	private function sanitizeString($str) {
		return (is_string($str)) ? strtolower($str) : null;
	}
	
	/**
	 * @param string $methodName
	 * @return void|string
	 */
	private function createViewPath() {
		$viewPath = false;
		if (!$this->controllerName || !$this->methodName) return;
		$controller = strtolower($this->controllerName);
		$method = strtolower($this->methodName);
		$viewPath = $controller . '/' . $method;
		
		return $viewPath;
	}
	public function booking_hotel_full($data_mapping,$id){
	 	$this->load->model('booking/booking');
	 	$mail = $this->mail;
        $mail->setFrom('no-reply@ge.com.sg', 'Global Explorer');						// Sender
    	//$mail->addReplyTo('no-reply@globlaexplorer.com', 'Global Explorer');			// Sender's optional
	    $mail->WordWrap = 50;                                 							// Set word wrap to 50 characters
	    $mail->isHTML(true);                                  							// Set email format to HTML
	 	$data=$this->booking->getByCancellation($data_mapping);
	    $mail->Subject = ''.$data[0]->bookedcode.' [Back to Amend] Global Explorer Booking Notification';
	    $recipients=$this->booking->getEmailadmin();
		$data_res=$this->booking->getByreject($id->getUser()->getId());
		$data_result['user_click']=$this->booking->getByreject($id->getUser()->getId());
		$data_result['mail_data']=$this->booking->getByCancellation($data_mapping);

		if($data[0]->agenttype == 'agent'){
    		$mail->ClearAddresses();
			$mail->ClearCCs();
			if($data[0]->agent_alter_email != "" || $data[0]->agent_alter_email != NULL){
				$wang=explode(',',$data[0]->agent_alter_email);
	    		for ($i=0; $i < count($wang); $i++) {
		     		$mail->addAddress($wang[$i],$data[0]->userfirstname ." ". $data[0]->userlastname);
		     		//$mail->addCC('delabahan@gmail.com',$booking['recipient']);
				}
			} 
		    $mail->addAddress($data[0]->agentemail,$data[0]->userfirstname ." ". $data[0]->userlastname);
		    //$mail->addCC('delabahan@gmail.com',$booking['recipient']);
		    $mail->Body    = $this->load->view('booking_back_amend_agent',$data_result,TRUE);
	    	if(!$mail->send()) return false;
		}

		if($data[0]->providertype == 'provider'){
			if($data[0]->provideremail != $data_res[0]->useremail){
	    		$mail->ClearAddresses();
				$mail->ClearCCs();
				if($data[0]->provider_alter_email != "" || $data[0]->provider_alter_email != NULL){
					$wang=explode(',',$data[0]->provider_alter_email);
		    		for ($i=0; $i < count($wang); $i++) {
			     		$mail->addCC($wang[$i],$data[0]->providerfirstname ." ". $data[0]->providerlastname);
			     		//$mail->addCC('delabahan@gmail.com',$booking['recipient']);
					}
				} 
				$mail->addAddress($data[0]->provideremail,$data[0]->providerfirstname ." ". $data[0]->providerlastname);// Add a recipient (Provider)
			    //$mail->addCC('delabahan@gmail.com',$booking['recipient']);
			    $mail->Body    = $this->load->view('booking_back_amend_provider',$data_result,TRUE);
			    if(!$mail->send()) return false;
			}
		}

		if($data[0]->admintype == 'admin'){
			if($data[0]->adminemail != $data_res[0]->useremail){
	    		$mail->ClearAddresses();
				$mail->ClearCCs();
				if($data[0]->admin_alter_email != "" || $data[0]->admin_alter_email != NULL){
					$wang=explode(',',$data[0]->admin_alter_email);
		    		for ($i=0; $i < count($wang); $i++) {
			     		$mail->addCC($wang[$i],$data[0]->adminfirstname ." ". $data[0]->adminlastname);
			     		//$mail->addCC('delabahan@gmail.com',$booking['recipient']);
					}
				} 
				$mail->addAddress($data[0]->adminemail,$data[0]->adminfirstname ." ". $data[0]->adminlastname);// Add a recipient (Provider)
			    //$mail->addAddress('delabahan@gmail.com',$booking['recipient']);
			    $mail->Body    = $this->load->view('booking_back_amend_admin',$data_result,TRUE);
			    if(!$mail->send()) return false;
			}
		}

		if($recipients != NULL || $recipients != ""){
     		foreach($recipients as $row){
	    		$mail->ClearAddresses();
				$mail->ClearCCs();
				//if($row->adminemail != $data[0]->adminemail){
					$data_result['recipient'] = @$row->adminfname.' '.@$row->adminlname;
		     		if($row->alteremail != '' || $row->alteremail != NULL) {
				     	$alternative_email=explode(',',$row->alteremail);
				     	for ($i=0; $i < count($alternative_email); $i++) {
				     		$mail->addCC($alternative_email[$i],$row->adminfirstname ." ". $row->adminlastname);
				     		//$mail->addCC('delabahan@gmail.com',$booking['recipient']);
						}
				    }
			    	$mail->addAddress($row->adminemail,$row->adminfname ." ". $row->adminlname);
			    	//$mail->addAddress('delabahan@gmail.com',$booking['recipient']);
			    	$mail->Body    = $this->load->view('booking_back_amend_all_admin',$data_result,TRUE);
			    	if(!$mail->send()) return false;
			   // }
     		}
     	}
	    $mail->AltBody = 'Booking Hotel Full [Global Explorer]';
		return true;
    }
	public function booking_reject($data_mapping,$id){
	 	$this->load->model('booking/booking');
	 	$mail = $this->mail;
        $mail->setFrom('no-reply@ge.com.sg', 'Global Explorer');						// Sender
    	//$mail->addReplyTo('no-reply@globlaexplorer.com', 'Global Explorer');			// Sender's optional
	    $mail->WordWrap = 50;                                 							// Set word wrap to 50 characters
	    $mail->isHTML(true);                                  							// Set email format to HTML
	 	$data=$this->booking->getByCancellation($data_mapping);
	    $mail->Subject = ''.$data[0]->bookedcode.' [rejected] Global Explorer Booking Notification';
	    $recipients=$this->booking->getEmailadmin();
		$data_res=$this->booking->getByreject($id->getUser()->getId());
		$data_result['user_click']=$this->booking->getByreject($id->getUser()->getId());
		$data_result['mail_data']=$this->booking->getByCancellation($data_mapping);

		if($data[0]->agenttype == 'agent'){
    		$mail->ClearAddresses();
			$mail->ClearCCs();
			if($data[0]->agent_alter_email != "" || $data[0]->agent_alter_email != NULL){
				$wang=explode(',',$data[0]->agent_alter_email);
	    		for ($i=0; $i < count($wang); $i++) {
		     		$mail->addAddress($wang[$i],$data[0]->userfirstname ." ". $data[0]->userlastname);
				}
			} 
		    $mail->addAddress($data[0]->agentemail,$data[0]->userfirstname ." ". $data[0]->userlastname);
		    $mail->Body    = $this->load->view('booking_rejected_agent',$data_result,TRUE);
	    	if(!$mail->send()) return false;
		}

		if($data[0]->providertype == 'provider'){
			if($data[0]->provideremail != $data_res[0]->useremail){
	    		$mail->ClearAddresses();
				$mail->ClearCCs();
				if($data[0]->provider_alter_email != "" || $data[0]->provider_alter_email != NULL){
					$wang=explode(',',$data[0]->provider_alter_email);
		    		for ($i=0; $i < count($wang); $i++) {
			     		$mail->addCC($wang[$i],$data[0]->providerfirstname ." ". $data[0]->providerlastname);
					}
				} 
				$mail->addAddress($data[0]->provideremail,$data[0]->providerfirstname ." ". $data[0]->providerlastname);// Add a recipient (Provider)
			    $mail->Body    = $this->load->view('booking_rejected_provider',$data_result,TRUE);
			    if(!$mail->send()) return false;
			}
		}

		if($data[0]->admintype == 'admin'){
			if($data[0]->adminemail != $data_res[0]->useremail){
	    		$mail->ClearAddresses();
				$mail->ClearCCs();
				if($data[0]->admin_alter_email != "" || $data[0]->admin_alter_email != NULL){
					$wang=explode(',',$data[0]->admin_alter_email);
		    		for ($i=0; $i < count($wang); $i++) {
			     		$mail->addCC($wang[$i],$data[0]->adminfirstname ." ". $data[0]->adminlastname);
					}
				} 
				$mail->addAddress($data[0]->adminemail,$data[0]->adminfirstname ." ". $data[0]->adminlastname);// Add a recipient (Provider)
			    $mail->Body    = $this->load->view('booking_rejected_admin',$data_result,TRUE);
			    if(!$mail->send()) return false;
			}
		}

		if($recipients != NULL || $recipients != ""){
     		foreach($recipients as $row){
	    		$mail->ClearAddresses();
				$mail->ClearCCs();
				//if($row->adminemail != $data[0]->adminemail){
					$data_result['recipient'] = @$row->adminfname.' '.@$row->adminlname;
		     		if($row->alteremail != '' || $row->alteremail != NULL) {
				     	$alternative_email=explode(',',$row->alteremail);
				     	for ($i=0; $i < count($alternative_email); $i++) {
				     		$mail->addCC($alternative_email[$i],$row->adminfirstname ." ". $row->adminlastname);
						}
				    }
			    	$mail->addAddress($row->adminemail,$row->adminfname ." ". $row->adminlname);
			    	$mail->Body    = $this->load->view('booking_rejected_all_admin',$data_result,TRUE);
			    	if(!$mail->send()) return false;
			   // }
     		}
     	}
	    $mail->AltBody = 'Booking Rejected [Global Explorer]';
		return true;
    }
    
	public function booking_confirmed($data_mapping,$id){
        $this->load->model('booking/booking');
	 	$mail = $this->mail;
        $mail->setFrom('no-reply@ge.com.sg', 'Global Explorer');						// Senter
    	//$mail->addReplyTo('no-reply@globlaexplorer.com', 'Global Explorer');			// Sender's optional
	    $mail->WordWrap = 50;                                 							// Set word wrap to 50 characters
	    $mail->isHTML(true);                                  							// Set email format to HTML
	 	$data=$this->booking->getByCancellation($data_mapping);
	    $mail->Subject = ''.$data[0]->bookedcode.' [confirmed] Global Explorer Booking Notification';
		$recipients=$this->booking->getEmailadmin();
		$data_res=$this->booking->getByreject($id->getUser()->getId());
		$data_result['user_click']=$this->booking->getByreject($id->getUser()->getId());
		$data_result['mail_data']=$this->booking->getByCancellation($data_mapping);
		
		if($data[0]->agenttype == 'agent'){
    		$mail->ClearAddresses();
			$mail->ClearCCs();
			if($data[0]->agent_alter_email != "" || $data[0]->agent_alter_email != NULL){
				$wang=explode(',',$data[0]->agent_alter_email);
	    		for ($i=0; $i < count($wang); $i++) {
		     		$mail->addCC($wang[$i],$data[0]->userfirstname ." ". $data[0]->userlastname);
				}
			} 
		    $mail->addAddress($data[0]->agentemail,$data[0]->userfirstname ." ". $data[0]->userlastname);
		    $mail->Body    = $this->load->view('booking_confirmed_agent',$data_result,TRUE);
		    if(!$mail->send()) return false;
		}

		if($data[0]->providertype == 'provider'){
			if($data[0]->provideremail != $data_res[0]->useremail){
	    		$mail->ClearAddresses();
				$mail->ClearCCs();
				if($data[0]->provider_alter_email != "" || $data[0]->provider_alter_email != NULL){
					$wang=explode(',',$data[0]->provider_alter_email);
		    		for ($i=0; $i < count($wang); $i++) {
			     		$mail->addCC($wang[$i],$data[0]->providerfirstname ." ". $data[0]->providerlastname);
					}
				} 
				$mail->addAddress($data[0]->provideremail,$data[0]->providerfirstname ." ". $data[0]->providerlastname);// Add a recipient (Provider)
			    $mail->Body    = $this->load->view('booking_confirmed_provider',$data_result,TRUE);
			    if(!$mail->send()) return false;
			}
		}

		if($data[0]->admintype == 'admin'){
			if($data[0]->adminemail != $data_res[0]->useremail){
	    		$mail->ClearAddresses();
				$mail->ClearCCs();
				if($data[0]->admin_alter_email != "" || $data[0]->admin_alter_email != NULL){
					$wang=explode(',',$data[0]->admin_alter_email);
		    		for ($i=0; $i < count($wang); $i++) {
			     		$mail->addCC($wang[$i],$data[0]->adminfirstname ." ". $data[0]->adminlastname);
					}
				} 
				$mail->addAddress($data[0]->adminemail,$data[0]->adminfirstname ." ". $data[0]->adminlastname);// Add a recipient (Provider)
			    $mail->Body    = $this->load->view('booking_confirmed_admin',$data_result,TRUE);
			    if(!$mail->send()) return false;
			}
		}

		if($recipients != NULL || $recipients != ""){
     		foreach($recipients as $row){
	    		$mail->ClearAddresses();
				$mail->ClearCCs();
				//if($row->adminemail != $data[0]->adminemail){
					$data_result['recipient'] = @$row->adminfname.' '.@$row->adminlname;
		     		if($row->alteremail != '' || $row->alteremail != NULL) {
				     	$alternative_email=explode(',',$row->alteremail);
				     	for ($i=0; $i < count($alternative_email); $i++) {
				     		$mail->addCC($alternative_email[$i],$row->adminfirstname ." ". $row->adminlastname);
						}
				    }
			    	$mail->addAddress($row->adminemail,$row->adminfname ." ". $row->adminlname);
			    	$mail->Body    = $this->load->view('booking_confirmed_all_admin',$data_result,TRUE);
			    	if(!$mail->send()) return false;
			    //}
     		}
     	}
     	$mail->AltBody = 'Booking Confirmed [Global Explorer]';
		return true;
    }

    public function booking_cancellation($data_mapping,$id){
        $this->load->model('booking/booking');
	 	$mail = $this->mail;
        $mail->setFrom('no-reply@ge.com.sg', 'Global Explorer');						// Sender
    	//$mail->addReplyTo('no-reply@globlaexplorer.com', 'Global Explorer');			// Sender's optional
	    $mail->WordWrap = 50;                                 							// Set word wrap to 50 characters
	    $mail->isHTML(true);                                  							// Set email format to HTML
	 	$data=$this->booking->getByCancellation($data_mapping);
	    $mail->Subject = ''.$data[0]->bookedcode.' [cancelled] Global Explorer Booking Notification';
	    $recipients=$this->booking->getEmailadmin();
		$data_res=$this->booking->getByreject($id->getUser()->getId());
		$data_result['user_click']=$this->booking->getByreject($id->getUser()->getId());
		$data_result['mail_data']=$this->booking->getByCancellation($data_mapping);

		if($data[0]->agenttype == 'agent'){
    		$mail->ClearAddresses();
			$mail->ClearCCs();
			if($data[0]->agent_alter_email != "" || $data[0]->agent_alter_email != NULL){
				$wang=explode(',',$data[0]->agent_alter_email);
	    		for ($i=0; $i < count($wang); $i++) {
		     		$mail->addCC($wang[$i],$data[0]->userfirstname ." ". $data[0]->userlastname);
				}
			}
		    $mail->addAddress($data[0]->agentemail,$data[0]->userfirstname ." ". $data[0]->userlastname);
		    $mail->Body    = $this->load->view('booking_cancellation_agent',$data_result,TRUE);
		    if(!$mail->send()) return false;
		}

		if($data[0]->providertype == 'provider'){
			if($data[0]->provideremail != $data_res[0]->useremail){
	    		$mail->ClearAddresses();
				$mail->ClearCCs();
				if($data[0]->provider_alter_email != "" || $data[0]->provider_alter_email != NULL){
					$wang=explode(',',$data[0]->provider_alter_email);
		    		for ($i=0; $i < count($wang); $i++) {
			     	$mail->addCC($wang[$i],$data[0]->providerfirstname ." ". $data[0]->providerlastname);
					}
				} 
				$mail->addAddress($data[0]->provideremail,$data[0]->providerfirstname ." ". $data[0]->providerlastname);// Add a recipient (Provider)
			    $mail->Body    = $this->load->view('booking_cancellation_provider',$data_result,TRUE);
			    if(!$mail->send()) return false;
			}
		}

		if($data[0]->admintype == 'admin'){
			if($data[0]->adminemail != $data_res[0]->useremail){
	    		$mail->ClearAddresses();
				$mail->ClearCCs();
				if($data[0]->admin_alter_email != "" || $data[0]->admin_alter_email != NULL){
					$wang=explode(',',$data[0]->admin_alter_email);
		    		for ($i=0; $i < count($wang); $i++) {
			     	$mail->addCC($wang[$i],$data[0]->adminfirstname ." ". $data[0]->adminlastname);
					}
				} 
				$mail->addAddress($data[0]->adminemail,$data[0]->adminfirstname ." ". $data[0]->adminlastname);// Add a recipient (Provider)
			    $mail->Body    = $this->load->view('booking_cancellation_admin',$data_result,TRUE);
			    if(!$mail->send()) return false;
			}
		}

		if($recipients != NULL || $recipients != ""){
     		foreach($recipients as $row){
	    		$mail->ClearAddresses();
				$mail->ClearCCs();
				//if($row->adminemail != $data[0]->adminemail){
					$data_result['recipient'] = @$row->adminfname.' '.@$row->adminlname;
		     		if($row->alteremail != '' || $row->alteremail != NULL) {
				     	$alternative_email=explode(',',$row->alteremail);
				     	for ($i=0; $i < count($alternative_email); $i++) {
				     		$mail->addCC($alternative_email[$i],$row->adminfirstname ." ". $row->adminlastname);
						}
				    }
			    	$mail->addAddress($row->adminemail,$row->adminfname ." ". $row->adminlname);
		    	$mail->Body    = $this->load->view('booking_cancellation_all_admin',$data_result,TRUE);
		    	if(!$mail->send()) return false;
		    	//}
     		}
     	}
     	$mail->AltBody = 'Booking Cancelled [Global Explorer]';
		return true;
    }
}