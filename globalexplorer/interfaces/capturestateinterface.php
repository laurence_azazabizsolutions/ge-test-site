<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @author Joshua Paylaga
 *
 */
interface CaptureStateInterface {
	
	/**
	 * 
	 */
	public function captureCurrentState();
	
	/**
	 * 
	 */
	public function prevState();
}

/* End of file capturestateinterface.php */
/* Location: ./globalexplorer/interfaces/capturestateinterface.php */