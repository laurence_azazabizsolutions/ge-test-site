<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Define how the authentication behaves in the system.
 * There are many authentications in the system that
 * needs uniform definition.
 * 
 * @author Joshua Paylaga
 */
interface AuthenticationInterface {
	
	/**
	 * Set the stored data to be verified.
	 * 
	 * @param string $stored
	 */
	public function setStored($stored);
	
	/**
	 * Take user input(s) ang compare it againts
	 * the stored data in the system. This is to
	 * anticipate multiple verifications.
	 * 
	 * @param string $input
	 */
	public function verify($input);
}

/* End of file authenticationinterface.php */
/* Location: ./globalexplorer/interfaces/authenticationinterface.php */