<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

interface SurchargeAwareInterface {
	public function addSurcharge(Surcharge $surcharge);
	public function getSurcharges();
	public function hasSurcharge();
	public function popSurcharge();
	public function removeSurcharge($pos);
}