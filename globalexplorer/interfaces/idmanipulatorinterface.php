<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Since id in any field in our models in the system are
 * highly secured - no model can directly set the id field.
 * The id of an object can only be set by a class implementing
 * this interface.
 * 
 * @author Joshua Paylaga
 */
interface IdManipulatorInterface {
	
	/**
	 * Let the implementing class to manipulate the highly
	 * secure id field in the model.
	 * 
	 * @param unknown $Object
	 * @param number $id
	 */
	public function setId($Object, $id);
}

/* End of file idmanipulatorinterface.php */
/* Location: ./globalexplorer/interfaces/idmanipulatorinterface.php */