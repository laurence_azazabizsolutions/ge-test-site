<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @author Joshua Paylaga
 *
 */
interface StoreItemInterface {
	
	/**
	 * 
	 */
	public function prepared();
	
	/**
	 * 
	 */
	public function stored();
}