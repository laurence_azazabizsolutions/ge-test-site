<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @author Joshua Paylaga
 */
interface StorageInterface {
	
	/**
	 * @param number $id
	 */
	public function getById($id);
	
	/**
	 * @param array $info
	 */
	public function getByInformation(array $info);
	
	/**
	 * @param unknown $obj
	 */
	public function store($obj);
	
	/**
	 * @param unknown $objArray
	 */
	public function batchStore(array $objArray);
	
	/**
	 * @param unknown $obj
	 */
	public function remove($obj);
}

/* End of file storageinterface.php */
/* Location: ./globalexplorer/interfaces/storageinterface.php */