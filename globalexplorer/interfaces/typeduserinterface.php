<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Define how a typed user interacts in the system.
 * 
 * @author Joshua Paylaga
 */
interface TypedUserInterface {
	
	/**
	 * Allows retrieval of a non-typed user
	 * from an instance implemented by this
	 * interface.
	 * 
	 * @return User
	 */
	public function getUser();
	
	/**
	 * A typed user must have a definition from
	 * the User instance.
	 * 
	 * @param User $user
	 */
	public function setUser(User $user);
}

/* End of file typeduserinterface.php */
/* Location: ./globalexplorer/interfaces/typeduserinterface.php */