<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

get_instance()->load->iface('idmanipulatorinterface');

/**
 * Define what are the methods you have to implement if
 * you want a users id and password be supplied in a user object.
 * The user object does not provide a setter method for the id
 * for security purposes. For the password, the getter method will
 * automatically hash the supplied password, therefore, if you
 * supply a password from database it will be hashed again which
 * is not ideal when dealing with authentication. You have to
 * implement this to properly supply sensitive information.
 * 
 * @author Joshua Paylaga
 */
interface SensitiveUserDataManipulatorInterface extends IdManipulatorInterface
{
	/**
	 * The setter for the password property of user.
	 * By implenting this method, you can supply the password
	 * property without going through hashing. For example
	 * you have a password in your database and you want to supply
	 * the user object as a return value, you can use the implementation
	 * of this method to solve that problem.
	 * @param User $user
	 * @param string $storedPassword
	 */
	public function setStoredPassword(User $user, $storedPassword);
}

/* End of file sensitiveuserdatamanipulatorinterface.php */
/* Location: ./globalexplorer/interfaces/sensitiveuserdatamanipulatorinterface.php */