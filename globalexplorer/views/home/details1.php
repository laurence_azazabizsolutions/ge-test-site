    <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <?php
            $img = base_url('img/feat/default.png');
            if(@$package->getImagePath()!='') $img = base_url($package->getImagePath());
        ?>
        <div class="carousel-inner">
            <div class="item active grad">
                <div class="carousel-fill" style="background-image:url('<?php echo $img;?>');"></div>
            </div>
        </div>
    </div><!-- /.carousel -->


    <!-- Content 
    ================================================== -->
    <div class="container-fluid container-fluid-overlay">

        <!-- details -->
        <div class="container-fluid">
            <div class="container pax-dest">
                <div class="row container-opacity" style="padding:0 10px;">
                    <div class="featurette">
                        <form method="post" accept-charset="utf-8" action="6cdfa1ad4dc4132a506fede240a2251c3e582d45e2868c2401b9cd73033f944f3f66fb34bec469f901b30427306401c9b34922139237b79d13e0d37962e857c2" />
                        <div class="featurette-image pull-left col-sm-5" style="padding:0 30px 20px 0;">
                            <img class="img-responsive img-thumbnail " src="<?php echo $img;?>">
                        </div>
                        <h3><?php echo $package->getTitle();?> (Effective from <?php echo $package->getDateFrom();?> - <?php echo $package->getDateTo();?>)</h3>
                        <strong>Package includes:</strong><br><br>
                        <?php
                            echo nl2br(htmlentities($package->getDescription()));
                            if ($package->getTermsAndConditions() != NULL) { 
                        ?>
                            <br>
                            <br>
                            <h5>Terms & Conditions</h5>
                            <p><?php echo nl2br(htmlentities($package->getTermsAndConditions())); ?></p>
                        <?php 
                            }
                            if($pkg_srchg!=NULL){
                                echo "<br><br><br><strong>Package Surcharcges:</strong><br><ul>";
                                foreach ($pkg_srchg as $packSurc) {
                                    if ($packSurc['rule_type'] == "date_range" || $packSurc['rule_type'] == "blackout" ) {
                                        $rule = "(effective from ". $packSurc['rule'] .")";
                                    }
                                    else if ($packSurc['rule_type'] == "day_of_week") {
                                        $rule = "(for every ". $packSurc['rule'] .")";
                                    }
                                    else if ($packSurc['rule_type'] == "agent") {
                                        $rule = "(by ". $packSurc['rule'] .")";
                                    }
                                    echo "<li>". $packSurc['description'] ." ". $rule ." @ S$". intVal($packSurc['cost'] + $packSurc['profit']) ." ". $packSurc['rate_type'] ."</li>";
                                }
                                echo "</ul>";
                            }
                        ?>
                        <!--?php echo base_url('details2/'.$package->getID()); ?-->
                        <br>
                        <!-- <a href="#" class="btn btn-orange" data-toggle="modal" data-target="#myModalLogin_"><span class="fa fa-book"></span> Book Now!</a> -->
                        </form>
                    </div>
                </div>
            </div>
            <br>
        </div>
        <!-- /details -->

        <!-- myModalLogin -->
        <div class="modal fade" id="myModalLogin_" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="pull-right btn btn-danger btn-sm" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Please Login:</h4>
                    </div>
                    <form id="login_form_" class="form-horizontal" role="form" action="<?php echo base_url('home/login'); ?>" method="post" enctype="multipart/form-data">
                        <div class="modal-body">
                            <?php echo '<input type="hidden" id="h-id" name="h-id" value="'.$package->getID().'">';?>
                            <?php if($error_message && isset($error_message)): ?>
                                <div class="alert alert-danger" ><?php echo $error_message?></div>
                            <?php endif; ?>
                            <div class="form-group">
                                <label class="col-xs-4 control-label">Email</label>
                                <div class="col-xs-8">
                                    <input type="email" class="form-control" placeholder="Email" name="email_login">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-4 control-label">Password</label>
                                <div class="col-xs-8">
                                    <input type="password" class="form-control" placeholder="Password" name="password_login">
                                </div>
                            </div>
                            <div class="form-group">
                                <a class="col-xs-offset-4 col-xs-8" href="#" data-dismiss="modal" data-toggle="modal" data-target="#myModalForget">Forgot password?</a>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <strong>No Account yet? <a href="#" data-dismiss="modal" data-toggle="modal" data-target="#myModalSignUp" role="button">Sign Up Now!</a></strong> &nbsp;
                            <button class="btn btn-orange" type="submit">Login</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>