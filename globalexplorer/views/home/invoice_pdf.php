<!DOCTYPE html>
<html lang="en">
	<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Global Explorer</title>
        <link rel="icon" href="<?php echo base_url('img/icon.png'); ?>">
        <link href="<?php echo base_url('css/bootstrap.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('css/carousel.css'); ?>" rel="stylesheet">
	</head>
	<body>
	<!-- Content 
        ================================================== -->
        <div class="container" style="border:0px solid red; max-width:730px;">
            <table class="table">
                <tr>
                    <td style="border:0px;" width="100px">
                        <img src="<?php echo base_url('img/invoice_header.png'); ?>" width="680px">
                    </td>
                </tr>
            </table>

            <p class="text-center h1"><strong>Invoice</strong></p>
            <table class="table">
                <tr>
                    <td style="border:0px;">
                        <p><strong>Prepared For:</strong> <?php echo @$agent_summary[0]->COMPANYNAME;?></p>
                    </td>
                    <td style="border:0px;">
                        <p class="text-right">
                            <strong>Invoice No:</strong> <?php echo @$room_summary[0]->bookingcode;?><br>
                            <!-- <strong>Agent Ref No:</strong> <br> -->
                            <strong>Date:</strong> <?php echo date( "jS F Y "); ?>
                        </p>
                    </td>
                </tr>
            </table>

            <p class="h4 strong"><strong>Information</strong></p>
            <table class="table table-bordered">
                <tr>
                    <th class="active">Name of Pax</th>
                    <td colspan="3"><?php echo @$guest[0]->name;?> (<?php echo @$guest[0]->type;?>)</td>
                </tr>
               <!--  <tr>
                    <th class="active">Hotel</th>
                    <td colspan="3"><?php //echo $r_summary[0][0]['hotel_name']; ?></td>
                </tr> -->
                <tr>
                    <th class="active">No of Pax</th>
                    <th class="active">Tour Code</th>
                    <th class="active">Check-In Date</th>
                    <th class="active">Check-Out Date</th>
                </tr>
                <tr>
                    <td><?php echo @$guest[0]->PAX; ?></td>
                    <td><?php echo @$invoice_pdf[0]->PACTOURCODE;?></td>
                    <td><?php echo date('d-M-Y',strtotime(@$invoice_pdf[0]->PACKCHECKIN));?></td>
                    <td><?php echo date('d-M-Y',strtotime(@$invoice_pdf[0]->PACKCHECKOUT));?></td>
                </tr>
            </table>

            <p class="h4 strong"><strong>Payment</strong></p>
            <table class="table table-bordered">
                <tr>
                    <th class="active">Charge Item</th>
                    <th class="active">Qty</th>
                    <th class="active">Total</th>
                </tr>

                <?php
                    $total_grand = 0;

                    #package summary -------------------------------------------
                    if(count($package_summary)!=0){
                        echo "
                        <tr class='summary_package_container'>
                            <th colspan='3'>Package Surcharges</th>
                        </tr>";

                        foreach ($package_summary as $destination) {
                            $total_grand+=$destination["subtotal"];

                            echo "
                            <tr class='summary_package_container'>
                                <td>{$destination['description']}</td>
                                <td>{$destination['computation']}</td>
                                <td>{$destination['subtotal']} SGD</td>
                            </tr>";
                        }
                    }
                    #end of package summary ------------------------------------

                    #destinations ----------------------------------------------
                    if(count($destination_summary)!=0){
                        for($i=0;$i<count($destination_summary);$i++){
                            $destination = $destination_summary[$i];
                            #destination header ----------------------------------------------  
                            echo "
                            <tr>
                                <th class='active'>Destination</th>
                                <td colspan='2'>{$destination['country']}</td>
                            </tr>
                            <tr>
                                <th class='active'>Hotel</th>
                                <td colspan='2'>{$r_summary[$i][0]['hotel_name']}</td>
                            </tr>";
                            #end of destination header ----------------------------------------------

                            #hotel surcharges ----------------------------------------------
                            if(count($hotel_summary[$i])!=0){
                               
                                echo "
                                <tr>
                                    <th colspan='3'>Hotel Surcharges</th>
                                </tr>";

                                foreach ($hotel_summary[$i] as $hotel) {
                                    $total_grand+=$hotel["subtotal"];
                                    echo "
                                    <tr>
                                        <td>{$hotel['description']}</td>
                                        <td>{$hotel['computation']}</td>
                                        <td>{$hotel['subtotal']} SGD</td>
                                    </tr>";
                                }
                            }
                            #end of hotel surcharges ----------------------------------------------

                            #roomrate surcharges ----------------------------------------------
                            if(count($rs_summary[$i])!=0){
                                echo "
                                <tr>
                                    <th colspan='3'>Roomrate Surcharges</th>
                                </tr>";

                                foreach ($rs_summary[$i] as $rs) {
                                    $total_grand+=$rs["subtotal"];
                                    echo "
                                     <tr>
                                        <td>{$rs['description']}</td>
                                        <td>{$rs['computation']}</td>
                                        <td>{$rs['subtotal']} SGD</td>
                                    </tr>";
                                }
                            }
                            #end of roomrate surcharges ----------------------------------------------

                            #rooms summaries ----------------------------------------------
                            if(count($r_summary[$i])!=0){
                                echo "
                                <tr>
                                    <th colspan='3'>Rooms</th>
                                </tr>";

                                foreach ($r_summary[$i] as $r) {
                                    $total_grand+=$r["subtotal"];
                                    echo "
                                     <tr>
                                        <td>{$r['description']}</td>
                                        <td>{$r['computation']}</td>
                                        <td>{$r['subtotal']} SGD</td>
                                    </tr>";
                                }
                            }
                            #end of rooms summaries ----------------------------------------------
                        }
                    }
                    #end of destinations ---------------------------------------

                    #addon summary ---------------------------------------------
                    if(count($addon_summary)!=0){
                        echo "
                        <tr>
                            <th colspan='3'>Addons</th>
                        </tr>";

                       foreach ($addon_summary as $destination) {
                            $total_grand+=$destination["subtotal"];
                            echo "
                            <tr>
                                <td>{$destination['description']}</td>
                                <td>{$destination['computation']}</td>
                                <td>{$destination['subtotal']} SGD</td>
                            </tr>";
                        }
                    }
                    #end of addon summary --------------------------------------

                    #extension summary -----------------------------------------
                    if(count($extension_summary)!=0){
                        echo "
                        <tr>
                            <th colspan='3'>Extensions</th>
                        </tr>";

                        foreach ($extension_summary as $destination) {
                            $total_grand+=$destination["subtotal"];
                            echo "
                            <tr>
                                <td>{$destination['description']}</td>
                                <td>{$destination['computation']}</td>
                                <td>{$destination['subtotal']} SGD</td>
                            </tr>";
                        }
                    }
                    #end of extension summary ----------------------------------

                ?>

                <!-- totals -->
                <?php if (isset($user) && get_class($user)!= 'Provider'): ?>
                    <tr>
                        <th class="active" colspan="2">Grand Total</th>
                        <th class="active"> <?php echo @$total_grand.' SGD';?></th> 
                    </tr>
                <?php endif; ?>
                <!-- totals-->

                <tr>
                    <td colspan="3" style="font-size:10px; border-color:white;">
                        <br>
                        <br>
                        <p>
                            <strong>Terms and Conditions:</strong><br>
                            Full Payment by Cash/Cheque or Electronic Transfer / Bank Draft must be made 3 days before group arrival. Otherwise the booking will be cancelled without notice. <br>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td style="font-size:10px; border-color:white;">
                        <p>
                            <strong>Name of Bank:</strong> UOB Bank<br>
                            <strong>Account Name:</strong> Global Explorer<br>
                            <strong>Bank Code:</strong> 7375<br>
                            <strong>Bank Branch:</strong> 016<br>
                            <strong>Account No:</strong> 339-303-316-7<br>
                        </p>
                    </td>
                    <td colspan="2" style="font-size:10px; border-color:white;">
                        <p>
                            <strong>Cancellation charge as follows:</strong><br>
                            - 3 days before guests arrival 1 night hotel room charged.<br>
                            - 2 days or less before guests arrival no refund. <br><br>

                            This is a computer generated invoice. No signature is required.
                         </p>
                    </td>
                </tr>
            </table>
        </div>
        <!-- /details -->
	</body>
</html>

