    <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">

        <!-- image -->
        <div class="carousel-inner">
            <div class="item active grad">
                <?php if ($packages != NULL): ?>
                    <?php
                        $img = base_url('../img/feat/default.png');
                        if($packages[0]->getImagePath() != '') $img = base_url('../'.$packages[0]->getImagePath());
                    ?>
                    <div class="carousel-fill" style="background-image:url('<?php echo $img;?>');"></div>
                <?php endif; ?>
            </div>
        </div>

        <!-- caption -->
        <div class="container carousel-select">
            <!-- <div class="carousel-caption" style="top:175px">
                <h1><?php //echo  $dest;?></h1>
                <p>Come! Indulge! Explore! Experience!</p>
            </div> -->
        </div>
    </div><!-- /.carousel -->

    <!-- Content 
    ================================================== -->
    <div class="container-fluid container-fluid-overlay">

        <!-- destination -->
        <div class="container-fluid">
            <div class="container pax-dest">
                <div class="row">
                    <div class="col-sm-12" style="z-index:10000;">
                        <?php if ($pdfs != NULL): ?>
                            <div class="dropdown">
                                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                                    Important Reference Documents:
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1" style="padding:0;">
                                    <?php foreach ($pdfs as $pdf):?>
                                        <a class="list-group-item" href="<?php echo base_url('uploads/destination_pdf/'.$pdf->DNAME); ?>" target="_blank"><li><?php echo $pdf->DTITLE;?></li></a>
                                    <?php endforeach; ?>
                                </ul>
                            </div>
                            <br>
                        <?php endif; ?>
                    </div>

                    <?php if ($packages != NULL): ?>
                        <?php foreach ($packages as $package):
                                    $toHash = sprintf("%d_%s_%s",'~!@#$%^&*()_+',
                                     'the hacker is not allowed',
                                     'QWERTYUIOPqwertyuiop');
                                    $hash = hashmonster($toHash);
                                    $key = sha1(md5($hash, true));
                                    $id = base_convert($package->getId(), 35, 36); 
                                    $data = mcrypt_encrypt(MCRYPT_BLOWFISH,$key, $id, 'ecb');
                                    $data = bin2hex($data);
                        ?>
                            <div class="col-xs-3">
                                <a href="<?php echo base_url('home/details1/'.$data)?>">
                                    <div class="pax-img">
                                        <?php
                                            $img = base_url('../img/feat/default.png');
                                            if(@$package->getImagePath() != '') $img = base_url('../'.$package->getImagePath());
                                        ?>
                                        <div class="pax-fill" style="background-image:url('<?php echo $img;?>');"></div>
                                    </div>
                                    <p class="pax-details small lead"><?php echo $package->getCode(); ?>: <?php echo $package->getTitle();?> (Effective from <?php echo $package->getDateFrom(); ?> - <?php echo $package->getDateTo(); ?>)</p>
                                </a>
                            </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
<!-- /destination -->