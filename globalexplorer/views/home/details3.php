    <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <?php
            $img = base_url('img/feat/default.png');
            if(@$package->getImagePath()!='') $img = base_url($package->getImagePath());
        ?>
        <div class="carousel-inner">
            <div class="item active grad">
                <div class="carousel-fill" style="background-image:url('<?php echo $img;?>');"></div>
            </div>
        </div>
    </div><!-- /.carousel -->


    <!-- Content 
    ================================================== -->
    <div class="container-fluid container-fluid-overlay">
        <input type="hidden" id="base_url_container" value="<?php echo base_url(); ?>">

        <!-- details -->
        <div class="container-fluid">
            <div class="container pax-dest">
                <div class="row container-opacity" style="padding-top:20px;">
                    <div class="col-md-12">
                        <div class="stepwizard">
                            <div class="stepwizard-row">
                                <div class="stepwizard-step">
                                    <button type="button" class="btn btn-default btn-circle" disabled="disabled">1</button>
                                    <p>Package</p> 
                                </div>
                                <div class="stepwizard-step">
                                    <button type="button" class="btn btn-default btn-circle" disabled="disabled">2</button>
                                    <p>Hotel</p>
                                </div>
                                <div class="stepwizard-step">
                                    <button type="button" class="btn btn-orange btn-circle" disabled="disabled">3</button>
                                    <p>Details</p>
                                </div> 
                            </div>
                        </div>
                    </div>

                    <div class='col-md-12'>
                        <br>
                        <div class="alert alert-success clearfix" id="step_details_sending_email"  style="display:none;">
                            <span class="pull-right">
                                <i class="fa fa-spinner fa-spin"></i>
                            </span>
                            <span class='pull-left'>
                                <strong>Sending Email</strong>
                            </span>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <img class="thumbnail img-responsive" src="<?php echo $img; ?>" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 style="margin-top:0px;"><?php echo $package->getTitle();?> (Effective from <?php echo $package->getDateFrom();?> - <?php echo $package->getDateTo();?>)</h4>
                        <strong>
                            For
                            <?php echo (isset($prev_details['adult_count']) && $prev_details['adult_count'] > 0) ? $prev_details['adult_count'] : 0 ;?> Adult, 
                            <?php echo (isset($prev_details['child_count']) && $prev_details['child_count'] > 0) ? $prev_details['child_count'] : 0 ;?> Child(ren),
                            <?php echo (isset($prev_details['infant_cnt']) && $prev_details['infant_cnt'] > 0) ? $prev_details['infant_cnt'] : 0 ;?> Infant(s).
                        </strong><br>
                        Total: <?php echo (isset($prev_details['total_holder']) && $prev_details['total_holder'] > 0) ? $prev_details['total_holder'] : 0 ;?> pax<br><br>

                        <strong>Departing on <?php echo $prev_details['depart_date']; ?></strong><br>
                        <strong>Returning on <?php echo $prev_details['return_date']; ?> </strong><br>
                        Package: <?php echo $package->getDays(); ?> day/s<br>
                        Extension: <?php echo $prev_details['extended_days']; ?> day/s<br>
                        Total: <?php echo $prev_details['total_days']; ?> day/s <br><br><br>
                    </div>
                    <form id="stepdetails_form" class="form-horizontal" role="form" action="<?php echo base_url('home/booking_cus'); ?>" method="post">
                        <?php
                            #dump($booking_data);

                            $prev_details = $this->session->userdata("step_booking");
                            $prev_count = count($prev_details);
                            $str = serialize($prev_details);
                            echo " <textarea style=\"display:none;\" name=\"step_booking_serialized\">{$str}</textarea>";
                           # dump($prev_details);
                        ?>
                
                        <div class="col-md-12">
                            <!-- start of alert box -->    
                            <div class="alert alert-danger" id="step_details_supplementary_errors" style="display:none;" >
                                <p>The number of selected rooms does not match with the total number of visitors!</p>
                                <p>Please select a room(s) before proceeding!</p>
                            </div>
                            <!-- end of alert box -->
                            

                            <?php 
                                if(!empty($prev_details["booking_id"]) && $prev_details["booking_id"]!=null){
                            ?>
                            <!-- guest checker -->
                            <div class="panel-group" role="tablist" id="booked_guests_master_list" aria-multiselectable="true" >

                                <div class="panel panel-orange">
                                    <div class="panel-heading" role="tab" id="heading1">
                                      <h4 class="panel-title">
                                        <a data-toggle="collapse"  style="color:white;" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapseOne">
                                          Adult Guest List
                                        </a>
                                      </h4>
                                    </div>
                                    
                                    <div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1">
                                      <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered booked_adult_guests">
                                                    <tr>
                                                        <th></th>
                                                        <th>Name</th>
                                                        <th width="150px;">DOB</th>
                                                        <th>Passport No</th>
                                                    </tr>
                                                    <?php 
                                                        $booked_adult_count = count(@$traveller_details["adult_cnt"]);
                                                        if($booked_adult_count){
                                                            for($i=0;$i<$booked_adult_count;$i++){
                                                                $booked_adult_name = @$traveller_details["adult_cnt"][$i]["name"];
                                                                $booked_adult_dob = @$traveller_details["adult_cnt"][$i]["date_of_birth"];
                                                                $booked_adult_pp = @$traveller_details["adult_cnt"][$i]["passport_no"];

                                                                echo "
                                                                <tr>
                                                                    <td>
                                                                        <input type='checkbox'  onchange='appendStepDetailsDyanmic(this);'  target='adult' name='traveller_data[]' f_name='{$booked_adult_name}' bod='{$booked_adult_dob}' pp='{$booked_adult_pp}'/>
                                                                    </td>
                                                                    <td>{$booked_adult_name}</td>
                                                                    <td>{$booked_adult_dob}</td>
                                                                    <td>{$booked_adult_pp}</td>
                                                                </tr>
                                                                     ";
                                                            }
                                                        }
                                                    ?>
                                                </table>
                                            </div>
                                      </div>
                                    </div>
                                </div>
                                
                                <div class="panel panel-orange">
                    
                                    <div class="panel-heading" role="tab" id="heading2">
                                      <h4 class="panel-title">
                                        <a data-toggle="collapse"  style="color:white;" data-parent="#accordion" href="#collapse2" aria-expanded="true" aria-controls="collapse2">
                                          Child(ren) Guest List
                                        </a>
                                      </h4>
                                    </div>
                                    
                                    <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
                                      <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table table-striped table-bordered booked_child_guests">
                                                    <tr>
                                                        <th></th>
                                                        <th>Name</th>
                                                        <th width="150px;">DOB</th>
                                                        <th>Passport No</th>
                                                    </tr>
                                                    
                                                    <?php 
                                                        $booked_child_count = count(@$traveller_details["child_cnt"]);
                                                        if($booked_child_count){
                                                            for($i=0;$i<$booked_child_count;$i++){
                                                                $booked_child_name = @$traveller_details["child_cnt"][$i]["name"];
                                                                $booked_child_dob = @$traveller_details["child_cnt"][$i]["date_of_birth"];
                                                                $booked_child_pp = @$traveller_details["child_cnt"][$i]["passport_no"];

                                                                echo "
                                                                <tr>
                                                                    <td>
                                                                        <input type='checkbox' onchange='appendStepDetailsDyanmic(this);' target='child'  name='traveller_data[]' f_name='{$booked_child_name}' bod='{$booked_child_dob}' pp='{$booked_child_pp}' />
                                                                    </td>
                                                                    <td>{$booked_child_name}</td>
                                                                    <td>{$booked_child_dob}</td>
                                                                    <td>{$booked_child_pp}</td>
                                                                </tr>
                                                                     ";
                                                            }
                                                        }
                                                    ?>
                                                </table>
                                            </div>
                                      </div>
                                    </div>
                                </div>

                                <div class="panel panel-orange">
                    
                                    <div class="panel-heading" role="tab" id="heading3">
                                      <h4 class="panel-title">
                                        <a data-toggle="collapse"  style="color:white;" data-parent="#accordion" href="#collapse3" aria-expanded="true" aria-controls="collapse3">
                                          Infant Guest List
                                        </a>
                                      </h4>
                                    </div>
                                    
                                    <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
                                      <div class="panel-body">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered booked_infant_guests">
                                                    <tr>
                                                        <th></th>
                                                        <th>Name</th>
                                                        <th width="150px;">DOB</th>
                                                        <th>Passport No</th>
                                                    </tr>
                                               
                                                        <?php 
                                                            $booked_infant_count = count(@$traveller_details["infant_cnt"]);
                                                            if($booked_infant_count){
                                                                for($i=0;$i<$booked_infant_count;$i++){
                                                                    $booked_infant_name = @$traveller_details["infant_cnt"][$i]["name"];
                                                                    $booked_infant_dob = @$traveller_details["infant_cnt"][$i]["date_of_birth"];
                                                                    $booked_infant_pp = @$traveller_details["infant_cnt"][$i]["passport_no"];

                                                                    echo "
                                                                    <tr>
                                                                        <td>
                                                                            <input type='checkbox' onchange='appendStepDetailsDyanmic(this);' target='infant' name='traveller_data[]' f_name='{$booked_infant_name}' bod='{$booked_infant_dob}' pp='{$booked_infant_pp}' />
                                                                        </td>
                                                                        <td>
                                                                            {$booked_infant_name}
                                                                        </td>
                                                                        <td>
                                                                            {$booked_infant_dob}
                                                                        </td>
                                                                        <td>
                                                                            {$booked_infant_pp}
                                                                        </td>
                                                                    </tr>
                                                                         ";
                                                                }
                                                            }
                                                        ?>
                                                </table>
                                            </div>
                                      </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end of guest checker -->
                            <?php 
                                }
                                $adult_counter_total = 0;
                                $child_counter_total = 0;
                                $infant_counter_total = 0;
                            ?>

                
                            <!-- traveller details -->
                            <div class="panel panel-orange">
                                <!-- Default panel contents -->
                                <div class="panel-heading text-center"><strong>Traveller Details</strong></div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered">
                                            <tr>
                                                <th></th>
                                                <th>Name</th>
                                                <th width="150px;">DOB</th>
                                                <th>Passport No</th>
                                            </tr>
                                            
                                            <!-- count and loop through adults -->
                                            <?php if (isset($prev_details['adult_count']) && $prev_details['adult_count'] > 0): ?>
                                                <?php for ($i=1; $i <= $prev_details['adult_count'] ; $i++) { $adult_counter_total = $i; ?>
                                                    <tr class="td_adult_new">
                                                        <td>Adult <?php echo $i;?></td>
                                                        <td>
                                                            <input   name="adult_name[]" class="form-control min-size-name" type="text" placeholder="Name">
                                                        </td>
                                                        <td >
                                                            <input   class="pickadate_DOB_adult form-control min-size-num" type="text" name="adult_dob[]" readonly/>
                                                        </td>
                                                        <td  class='form-group'>
                                                            <input   name="adult_ppno[]" class="form-control min-size-num" type="text" placeholder="12345">
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            <?php endif; ?>
                                            <!-- count and loop throught adults -->


                                            
                                            <!-- count the children -->
                                            <?php if(intVal($prev_details['child_count']) > 0): ?>
                                                <?php $i=0;?>
                                                <?php for($i=1;$i<=$prev_details['child_count'];$i++){  $child_counter_total = $i; ?>
                                                    <tr class="td_child_new">
                                                        <td>Child <?php echo $i;?></td>
                                                        <td>
                                                            <input  name="child_name[]" class="form-control min-size-name" type="text" placeholder="Child name"></td>
                                                        <td>
                                                            <input  class="pickadate_DOB_child form-control min-size-num" type="text" name="child_dob[]" readonly/>
                                                        </td>
                                                        <td>
                                                            <input  name="child_ppno[]" class="form-control min-size-num" type="text" placeholder="12345">
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            <?php endif; ?>
                                            <!-- end of count the children -->


                        
                                            <!-- count the infants -->
                                            <?php if(intVal($prev_details['infant_cnt']) > 0): ?>
                                                <?php $i=0;?>
                                                <?php for($i=1;$i<=intVal($prev_details['infant_cnt']);$i++){ $infant_counter_total = $i; ?>
                                                    <tr  class="td_infant_new">
                                                        <td>Infant <?php echo $i;?></td>
                                                        <td>
                                                            <input name="infant_name[]" class="form-control min-size-name" type="text" placeholder="Infant name"></td>
                                                        <td>
                                                            <input class="pickadate_DOB_infant form-control min-size-num" type="text" name="infant_dob[]" readonly/>
                                                        </td>
                                                        <td>
                                                            <input name="infant_ppno[]" class="form-control min-size-num" type="text" placeholder="12345">
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            <?php endif; ?>
                                            <!-- end of count the infants -->

                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- end of traveller details -->
                            
                            <?php 
                               # echo "adult counter : {$adult_counter_total} child counter : {$child_counter_total} infant counter : {$infant_counter_total}";
                               # echo "<br/> booked counter : {$booked_adult_count} child : {$booked_child_count} infant : {$booked_infant_count}";
                            ?>  
                                         
                            <script>
                                var adult_counter = parseInt("<?php echo $adult_counter_total; ?>");
                                var child_counter = parseInt("<?php echo $child_counter_total; ?>");
                                var infant_counter = parseInt("<?php echo $infant_counter_total; ?>");

                                var b_adult_counter = parseInt("<?php echo $booked_adult_count; ?>");
                                var b_child_counter = parseInt("<?php echo $booked_child_count; ?>");
                                var b_infant_counter = parseInt("<?php echo $booked_infant_count; ?>");


                                function appendStepDetailsDyanmic(element){
                                    var target = $(element).attr("target");
                                    
                                    $(".td_"+target+"_new").find("[name='"+target+"_name[]']").each(function(i,e){
                                        $(e).empty().val('');
                                    });
                                    $(".td_"+target+"_new").find("[name='"+target+"_dob[]']").each(function(i,e){
                                        $(e).empty().val('');
                                    });
                                    $(".td_"+target+"_new").find("[name='"+target+"_ppno[]']").each(function(i,e){
                                        $(e).empty().val('');
                                    });

                                    var new_arr = [];

                                    $(".booked_"+target+"_guests").find("[name='traveller_data[]']:checked").each(function(i,e){
                                        var name = $.trim($(e).attr("f_name"));
                                        var bod = $.trim($(e).attr("bod"));
                                        var pp = $.trim($(e).attr("pp"));

                                        new_arr.push({f_name:name, bod: bod, pp:pp});
                                    });

                                    if(new_arr.length){
                                        for(var i =0; i<new_arr.length; i++){
                                            $(".td_"+target+"_new:eq("+i+")").find("[name='"+target+"_name[]']").val(new_arr[i].f_name);
                                            $(".td_"+target+"_new:eq("+i+")").find("[name='"+target+"_dob[]']").val(new_arr[i].bod);
                                            $(".td_"+target+"_new:eq("+i+")").find("[name='"+target+"_ppno[]']").val(new_arr[i].pp);
                                        }
                                    }
                                }
                            </script>
                                            
                            <!-- start of flight details -->
                            <div class="panel panel-orange">
                                <!-- Default panel contents -->
                                <div class="panel-heading text-center"><strong>Flight Details</strong></div>
                                <div class="panel-body">
                                    <div class="table-responsive table-responsive-flight-details" style="">
                                        <table class="table table-striped table-bordered">
                                            <tr>
                                                <th width="150px"></th>
                                                <th colspan="2">Originating Flight</th>
                                                <th colspan="2">Returning Flight</th>
                                            </tr>
                            
                                            <tr>
                                                <td>Date</td>
                                                <td>
                                                    <div class="input-group date datetimepickerTimeOnly" style="min-width:180px;">
                                                        <span class="input-group-addon"><span class="fa fa-clock-o"></span></span>
                                                        <input name="flight_of_date_departure" value="<?php echo @$prev_details["depart_date"]." ".@$booking_data["of_departuretime"]; ?>00:00" type="calendar" data-date-format="YYYY-MM-DD HH:mm" class="form-control" readonly/>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="input-group date datetimepickerTimeOnly" style="min-width:180px;">
                                                        <span class="input-group-addon"><span class="fa fa-clock-o"></span></span>
                                                        <input name="flight_of_date_arrival" value="<?php echo  @$prev_details["depart_date"]." ".@$booking_data["of_arrivaltime"]; ?>00:00" type="calendar" data-date-format="YYYY-MM-DD HH:mm" class="form-control min-size-num" readonly/>
                                                    </div>
                                                </td>

                                                <td>
                                                    <div class="input-group date datetimepickerTimeOnly" style="min-width:180px;">
                                                        <span class="input-group-addon"><span class="fa fa-clock-o"></span></span>
                                                        <input name="flight_rf_date_departure" value="<?php echo  @$prev_details["return_date"]." ".@$booking_data["rf_departuretime"]; ?>00:00" type="calendar" data-date-format="YYYY-MM-DD HH:mm" class="form-control min-size-num" readonly/>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="input-group date datetimepickerTimeOnly" style="min-width:180px;">
                                                        <span class="input-group-addon"><span class="fa fa-clock-o"></span></span>
                                                        <input name="flight_rf_date_arrival" value="<?php echo  @$prev_details["return_date"]." ".@$booking_data["rf_arrivaltime"]; ?>00:00" type="calendar" data-date-format="YYYY-MM-DD HH:mm" class="form-control min-size-num" readonly/>
                                                    </div>
                                                </td>
                                            </tr>
                                            
                                            <tr>
                                                <td>FlightNo.</td>
                                                <td colspan="2">
                                                    <input name="flight_of_no" value="<?php echo @$booking_data["of_num"]; ?>"  class="form-control" type="text" placeholder="12345">
                                                </td>
                                                <td colspan="2">
                                                    <input name="flight_rf_no" value="<?php echo @$booking_data["rf_num"]; ?>"  class="form-control" type="text" placeholder="12345">
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>From</td>
                                                <td colspan="2">
                                                    <select name="flight_of_from" class="form-control" target-select="flight_of_to">
                                                        <option value=" ">- Select -</option>
                                                        <?php if (isset($airportcodes)): ?>
                                                            <?php foreach ($airportcodes as $a => $airportcode):
                                                                $of_from = trim(@$booking_data["of_from"]);
                                                                $tmp_explode = explode(".", $a);
                                                                $tmp_match = trim($tmp_explode[1]); ?>
                                                                <option value="<?php echo $a; ?>" <?php if($tmp_match==$of_from){ echo "selected"; } ?> ><?php echo $airportcode; ?></option>
                                                            <?php endforeach; ?>
                                                        <?php endif; ?>
                                                    </select>
                                                </td>
                                                <td colspan="2">
                                                    <select name="flight_rf_from" class="form-control" target-select="flight_rf_to">
                                                        <option value=" ">- Select -</option>
                                                        <?php if (isset($airportcodes)): ?>
                                                            <?php foreach ($airportcodes as $a => $airportcode):
                                                                $rf_from = trim(@$booking_data["rf_from"]);
                                                                $tmp_explode = explode(".", $a);
                                                                $tmp_match = trim($tmp_explode[1]); ?>
                                                                <option value="<?php echo $a; ?>" <?php if($tmp_match==$rf_from){ echo "selected"; } ?>  ><?php echo $airportcode; ?></option>
                                                            <?php endforeach; ?>
                                                        <?php endif; ?>
                                                    </select>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>To</td>
                                                <td colspan="2">
                                                    <select name="flight_of_to" class="form-control" >
                                                        <option value=" ">- Select -</option>
                                                        <?php if (isset($airportcodes)): ?>
                                                            <?php foreach ($airportcodes as $a => $airportcode):
                                                                $of_to = @$booking_data["of_to"];
                                                                $tmp_explode = explode(".", $a);
                                                                $tmp_match = $tmp_explode[1]; ?>
                                                                <option value="<?php echo $a; ?>" <?php if($tmp_match==$of_to){ echo "selected"; } ?> ><?php echo $airportcode; ?></option>
                                                            <?php endforeach; ?>
                                                        <?php endif; ?>
                                                    </select>
                                                </td>
                                                <td colspan="2">
                                                    <select name="flight_rf_to" class="form-control" >
                                                        <option value=" ">- Select -</option>
                                                        <?php if (isset($airportcodes)): ?>
                                                            <?php foreach ($airportcodes as $a => $airportcode): 
                                                                $rf_to = trim(@$booking_data["rf_to"]);
                                                                $tmp_explode = explode(".", $a);
                                                                $tmp_match = trim($tmp_explode[1]); ?>
                                                                <option value="<?php echo $a; ?>"  <?php if($tmp_match==$rf_to){ echo "selected"; } ?> ><?php echo $airportcode; ?></option>
                                                            <?php endforeach; ?>
                                                        <?php endif; ?>
                                                    </select>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>Remarks</td>
                                                <td colspan="4"><textarea name="flight_remarks" class="form-control" style="resize:none;" rows="3"><?php echo @$booking_data["of_remarks"]; ?></textarea></td>
                                            </tr>
                                            
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- end of flight details  -->
                            
                            
                            <!-- agent details -->
                            <div class="panel panel-orange">
                                <div class="panel-heading text-center"><strong>Agent Details</strong></div>
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered">
                                        <tr>
                                            <th><input name="agent_details_name" class="form-control" value="<?php echo @$booking_data["agent_name"]; ?>" type="text" placeholder="Full Name"></th>
                                            <th><input name="agent_details_no" class="form-control" value="<?php echo @$booking_data["agent_ref"]; ?>"  type="text" placeholder="Reference No."></th>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                            <!-- agent details  -->
                            
                            
                            <!-- start of developer mode -->
                            <div class="panel panel-orange" style="display:none;">
                                <!-- Default panel contents -->
                                <div class="panel-heading text-center"><strong>ajax returns (remove this during production) </strong></div>
                                <div class="panel-body" id="something_step_details_finalize"> </div>
                            </div>
                            <!-- end of developer mode -->
                            

                            <!-- start of book now -->
                            <button class="btn btn-orange center-block" id="stepDetailsFinalizeBTN" role="button" onclick="stepDetailsFinalize(this);">Book Now!</button>
                            <!-- end of book now -->
                        </div>
                    </form>
                </div>
                <br>
            </div>
        </div>
        <!-- /details-->

        <script>
            $(document).ready(function(){
                <?php 
                    if($booking_data){
                ?>  

                <?php
                    }else{
                ?>
                        $("[name='flight_of_to'] option:first").attr("selected","selected");
                        $("[name='flight_rf_to'] option:first").attr("selected","selected");
                        $("[name='flight_rf_from'] option:first").attr("selected","selected");
                        $("[name='flight_of_from'] option:first").attr("selected","selected");
                        //disable returning flights ----------------------------------------------
                        $("[name='flight_of_to'], [name='flight_rf_to']").attr("disabled","disabled");
                <?php
                    }
                ?>
            });
        </script>

