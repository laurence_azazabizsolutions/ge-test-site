    <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- images -->
        <div class="carousel-inner">
            <div class="item active grad">
                <div class="carousel-fill" style="background-image:url('<?php echo base_url('img/1.jpg'); ?>');"></div>
            </div>
            <div class="item grad">
                <div class="carousel-fill" style="background-image:url('<?php echo base_url('img/2.jpg'); ?>');"></div>
            </div>
            <div class="item grad">
                <div class="carousel-fill" style="background-image:url('<?php echo base_url('img/3.jpg'); ?>');"></div>
            </div>
        </div>

        <!-- caption -->
        <div class="container carousel-select">
            <div class="carousel-caption">
                <div class="form-group">
                    <p>
                        <label for="selectDestination" class="col-md-4 col-md-offset-2 control-label text-right"><span class="fa fa-plane"></span> Travelling To : </label>
                        <div class="col-md-4">
                            <select id="selectDestination" name="country" class="form-control">
                                <option value="">- Select -</option>
                                <?php if (isset($countries)): ?>
                                    <?php foreach ($countries as $country): ?>
                                        <option value="<?php echo base_url('browse/'.$country['code']); ?>"><?php echo $country['country']; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        </div>
                    </p>
                    <br><br><br><br><br><br><br><br><br><br>
                </div>
            </div>
        </div>

        <!-- arrow -->
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
        </div><!-- /.carousel -->


        <!-- Content 
        ================================================== -->
        <div class="container-fluid container-fluid-overlay">

        <!-- featured -->
        <div class="container-fluid">
            <div class="container pax-dest">
                <h5 class="text-shadow-white"><strong>FEATURED DESTINATIONS</strong></h5>
                <br>
                <div class="row">
                    <?php if (isset($packages)): ?>
                        <?php
                            $cnt = 0;
                            foreach ($packages as $package): 
                                if ($cnt < 4) {
                                    $toHash = sprintf("%d_%s_%s",'~!@#$%^&*()_+',
                                     'the hacker is not allowed',
                                     'QWERTYUIOPqwertyuiop');
                                    $hash = hashmonster($toHash);
                                    $key = sha1(md5($hash, true));
                                    $id = base_convert($package['id'], 35, 36); 
                                    $data = mcrypt_encrypt(MCRYPT_BLOWFISH,$key, $id, 'ecb');
                                    $data = bin2hex($data);
                                    ?>
                                    <div class="col-xs-3">
                                        <a href="<?php echo base_url('home/details1/'.$data);?>">
                                            <div class="pax-img">
                                                <?php if($package['image_path']!=''):?>
                                                    <div class="pax-fill" style="background-image:url('<?php echo base_url($package['image_path'])?>');"></div>
                                                <?php else:?>
                                                    <div class="pax-fill" style="background-image:url('img/feat/default.png');"></div>
                                                <?php endif; ?>
                                            </div>
                                            <p class="pax-details small lead"><?php echo $package['code'];?>: <?php echo $package['title'];?> (Effective from <?php echo $package['date_from'];?> - <?php echo $package['date_to'];?>)</p>
                                        </a>
                                    </div>
                        <?php 
                                }
                            $cnt ++;
                            endforeach;
                        ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <!-- /featured -->
