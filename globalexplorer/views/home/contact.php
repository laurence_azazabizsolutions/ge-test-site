    <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner">
        <div class="item active grad">
          <div class="carousel-fill" style="background-image:url(<?php echo base_url('img/6.jpg'); ?>)"></div>
        </div>
      </div>
    </div><!-- /.carousel -->
      

    <!-- Content 
    ================================================== -->
    <div class="container-fluid container-fluid-overlay">

      <div class="container-fluid">
        <div class="container pax-dest">
          <div class="row container-opacity" style="padding:0px;">
            <div class="col-md-8">
              <form id="mail_form" method="post" class="form-horizontal" action="<?php echo base_url('send_mail'); ?>">
                <h4>Leave us your message.</h4>
                <p>To get in touch with us, please fill up the form below.
                </p><br>
                <div class="alert" style="display: none;"></div>
                 <div class="form-group">
                    <label class="col-lg-2 control-label">Name</label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" name="sender_name" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-2 control-label">Email address</label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" name="sender_email" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-2 control-label">Message</label>
                    <div class="col-lg-10">
                         <textarea class="form-control" style="resize:none;" rows="3" name="sender_message"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-10 col-lg-offset-2">
                        <button type="submit" class="btn btn-orange">Send</button>
                    </div>
                </div>
              </form>
            </div>
            <div class="col-md-4">
              <h4>Address</h4>
              <p class="lead">
                <br><br>
                <span class="glyphicon glyphicon-home">&nbsp;</span> 150 South Bridge Road #03-08 Singapore 058727<br>
                <span class="glyphicon glyphicon-earphone">&nbsp;</span> +65 8451 6666<br>
                <span class="glyphicon glyphicon-envelope">&nbsp;</span> <a href="mailto:edmundwan@hotmail.com">edmundwan@hotmail.com</a>
              </p>
              <br>
            </div>
          </div>
        </div>
        <br>
      </div>
      <!-- /details -->
