<!-- Carousel
================================================== -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="item active grad">
      <div class="carousel-fill" style="background-image:url(<?php echo base_url('img/5.jpg'); ?>)"></div>
    </div>
  </div>
</div><!-- /.carousel -->
  

<!-- Content 
================================================== -->
<div class="container-fluid container-fluid-overlay">

  <div class="container-fluid">
    <div class="container pax-dest">
      <div class="row container-opacity" style="padding:0px;">
        <div class="col-md-12">
          <h1>Welcome to Global Explorer</h1>
          <h4>Your travel bookings made easy.</h4>
          <p>
            If you are a travel agent who is looking to <strong><em>expand your inventory</em></strong>, trips to offer or a <strong><em>ground service provider</em></strong> who is <strong><em>looking for new sources of customers</em>, Global Explorer is the place for you!</strong>
            <br><br>
            With our proprietary booking system, you can now make faster connections & more travel deals quickly and easily!
            <br><br>
            <strong>Give it a go today! See you inside!</strong>
          </p>
        </div>
      </div>
    </div>
    <br>
  </div>
  <!-- /details -->