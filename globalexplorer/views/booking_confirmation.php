<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Global Explorer</title>
 		<style type="text/css">
			h1,
			h4{
				font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
				font-weight: 500;
				line-height: 1.1;
				color: inherit;
			}
		</style>
	</head>
	<body style="color: #333333;">
		<table cellspacing="0" cellpadding="0" style="background-color: #ffffff; border: 1px solid transparent;  border-radius: 4px; -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05); box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05); border-color: #e95a3e;">
			<tr>
				<td style="padding: 10px 15px; border-bottom: 1px solid transparent; border-top-right-radius: 3px; border-top-left-radius: 3px; color: #fff; background-color: #e95a3e; border-color: #e95a3e;">
					<h1 style="margin-top: 0; margin-bottom: 0; font-size: 16px; color: inherit; text-align: center; ">
						Global Explorer
					</h1> 
				</td>
			</tr>
			<tr>
				<td style="padding: 15px;">
					<table style="width: 770px; margin-bottom: 20px; border-collapse: collapse;">
						<tr>
							<td><h4 style="font-size: 18px; margin-top: 10px; margin-bottom: 10px;"><?php echo $recipient; ?>:</h4></td>
						</tr>
						<tr>
							<td>
								<p style="margin: 0;">
									<?php if($ge_booking_status === 'amended'){ echo 'The booking is amended'; } ?><?php if($ge_booking_status === 'pending'){ echo 'A new booking has been created'; } ?> for <strong><?php echo $package[0]->getTitle();?><?php if($step_booking["extended_days"] > 0){ echo ' + '.$step_booking["extended_days"].' Night/s extension';} ?></strong> 
									with <?php echo $step_booking['adult_count'];?> adult/s, <?php echo $step_booking['child_count'];?> child/rens, <?php echo $step_booking['infant_cnt'];?> infant/s on <?php echo date_format(date_create($step_booking['depart_date']),'d-m-Y');?> until <?php echo date_format(date_create($step_booking['return_date']),'d-m-Y');?>.
								</p>
							</td>
						</tr>
					</table>

	                <?php 
	                    echo @$booking_summary[0];
	                    //echo @$booking_summary[1];
                    ?>
				</td>
				<tr>
					<td style="padding: 15px;">
						<p style="margin: 0">
						 	<i>Note: You may reply to this email for any changes in hotel.</i>
						</p>
					</td>
				</tr>
			</tr>
		</table>
	</body>
</html>