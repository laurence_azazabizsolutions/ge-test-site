<tr roomsurchargeId="<?php echo $roomsurcharge['id'];?>">
    <td>
        <a href="#" class="edit_roomrate_link" data-target="#myModalRoomRateSurcharge">Edit</a> | 
        <a href="javascript:void(0);" onclick="removeSurchargeHotelRooms(this);" surcharge-id="<?php echo $roomsurcharge["id"]; ?>">Delete</a>
    </td>
    <?php
        $cost       = (trim($roomsurcharge['cost'])!='' || $roomsurcharge['cost']!=NULL)        ? $roomsurcharge['cost'] : 0.00;
        $profit     = (trim($roomsurcharge['profit'])!='' || $roomsurcharge['profit']!=NULL)    ? $roomsurcharge['profit'] : 0.00;
    ?>

    <td><?php echo $roomsurcharge['description']; ?></td>
    <td><?php echo ucwords(str_replace('_', ' ', $roomsurcharge['rule_type'])); ?> : <?php echo $roomsurcharge['rule']; ?></td>
    <td><?php echo $cost ; ?></td>
    <td><?php echo $profit; ?></td>
    <td><?php echo ($profit + $cost); ?></td>
    <td><?php echo ucwords(str_replace('/', ' / ', $roomsurcharge['rate_type'])); ?></td>
</tr>
            