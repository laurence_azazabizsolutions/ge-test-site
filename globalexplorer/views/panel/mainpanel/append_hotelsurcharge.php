<?php if($surcharge->getStatus() != 'inactive'){ ?>
    <tr hotelsurcharge_id="<?php echo $surcharge->getId();?>">
        <td>
            <a href="#" name='edit_surcharge_link' data-target="#myModalHotelSurcharge">Edit</a> | 
            <a href="javascript:void(0);" onclick="removeGeneralElement(this)" element-container= "[hotelsurcharge_id='<?php echo $surcharge->getId(); ?>']"  element-table="hotel_surcharges" element-message="Do you really want to remove this surcharge?" element-id="<?php echo $surcharge->getId(); ?>" >Delete</a>
        </td>
        <td><?php echo $surcharge->getDescription(); ?></td>
        <td><?php echo str_replace('_', ' ', ucfirst($surcharge->getRuletype())); ?>: <?php echo $surcharge->getRule(); ?></td>
        <td><?php echo $surcharge->getCost(); ?></td>
        <td><?php echo $surcharge->getProfit(); ?></td>
        <td><?php echo $surcharge->getPrice(); ?></td>
        <td><?php echo $surcharge->getRatetype(); ?></td>
    
    </tr>
<?php } ?>
