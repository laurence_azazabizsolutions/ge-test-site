<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Global Explorer</title>
        <link rel="icon" href="<?php echo base_url('img/icon.png'); ?>">
        <link href="<?php echo base_url('css/bootstrap.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('css/carousel.css'); ?>" rel="stylesheet">
        <style type="text/css">
            /*table {
                page-break-before:auto;
            }*/
        </style>
    </head>
    <body>

        <!-- Content 
        ================================================== -->

        <!-- contact -->
        <div class="container" style="border:0px solid red; max-width:730px;">
            <br>
            <table class="table">
                <tr>
                    <td style="border:0px;" width="70px">
                        <img src="<?php echo base_url('img/ge.jpg'); ?>" height="70px" width="70px">
                    </td>
                    <td style="border:0px;">
                        <h4>Tour Name: <?php echo @$invoice_pdf[0]->PACTOURTITLE;?></h4>
                        <div class="col-md-12">
                        <strong>    Booking No:</strong> <?php echo @$invoice_pdf[0]->BOOKNO;?>
                        <strong>    Tour Code:</strong> <?php echo @$invoice_pdf[0]->PACTOURCODE;?>
                        <strong>    Extension:</strong>
                        <?php if(@$invoice_pdf[0]->BOOKEDEXTENDDAYS == 1):?>
                        <?php echo @$invoice_pdf[0]->BOOKEDEXTENDDAYS;?> day
                        <?php else: ?>
                        <?php echo @$invoice_pdf[0]->BOOKEDEXTENDDAYS;?> days
                        <?php endif; ?>
                        <br>
                        </div>
                    </td>
                </tr>
            </table>
            <strong>Booking Reference - SERVICE VOUCHER</strong>
            <table class="table table-bordered">
                <thead>
                    <?php
                        if (isset($guest)) {
                            $rows = 1;
                            $cols = 1;
                            $tbl_arr = array();

                            $adult = 0;
                            $child = 0;
                            $infant = 0;
                            $pax = 0;
                            //dump($guest);
                            foreach ($guest as $row){
                                //count user type
                                if (@$row->type == "adult") {
                                    $adult++;
                                    $pax++;
                                }
                                else if (@$row->type == "child") {
                                    $child++;
                                    $pax++;
                                }
                                else if (@$row->type == "infant") {
                                    $infant++;
                                    $pax++;
                                }

                                //initialize users
                                if ($cols <= 5) {
                                    $tbl_arr[$rows][$cols] = @$row->name ." (". ucfirst(substr(@$row->type, 0, 1)).")";
                                    $cols++;
                                }
                                else {
                                    $cols = 1;
                                    $rows++;
                                    $tbl_arr[$rows][$cols] = @$row->name ." (". ucfirst(substr(@$row->type, 0, 1)).")";
                                    $cols++;
                                }
                            }
                        }
                    ?>
                    <tr>
                        <th>Name if Passenger/s:</th>
                        <th>(A)dult: <?php echo $adult; ?></th>
                        <th>(C)hild: <?php echo $child; ?></th>
                        <th>(I)nfant: <?php echo $infant; ?></th>
                        <th>No of Pax: <?php echo $pax; ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        for ($r=1; $r <= $rows; $r++) {
                            echo "<tr>";
                            for ($c=1; $c <= 5 ; $c++) { 
                                echo "<td style='font-size:12px;'>";
                                    if (@$tbl_arr[$r][$c] != "" || @$tbl_arr[$r][$c] != NULL) {
                                        echo $tbl_arr[$r][$c];
                                    }
                                echo "</td>";
                            }
                            echo "</tr>";
                        }
                    ?>
                </tbody>
            </table>

            <strong>Your Itinerary</strong>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Date</th>
                        <th>Flight No.</th>
                        <th>From</th>
                        <th>To</th>
                        <th>Departure</th>
                        <th>Arriving</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="font-size:12px;"><?php echo date('M d, Y',strtotime(@$invoice_pdf[0]->PACKCHECKIN)); ?></td>
                        <td style="font-size:12px;"><?php echo @$invoice_pdf[0]->BOOKDEPARTFLIGHTNO; ?></td>
                        <td style="font-size:12px;"><?php echo @$invoice_pdf[0]->BOOKDEPFROM; ?></td>
                        <td style="font-size:12px;"><?php echo @$invoice_pdf[0]->BOOKDEPTO; ?></td>
                        <td style="font-size:12px;"><?php echo @$invoice_pdf[0]->BOOKDEPARTDATE; ?></td>
                        <td style="font-size:12px;"><?php echo @$invoice_pdf[0]->BOOKDEPARTADATEARRIVAL; ?></td>
                    </tr>
                    <tr>
                        <td style="font-size:12px;"><?php echo date('M d, Y',strtotime(@$invoice_pdf[0]->PACKCHECKOUT)); ?></td>
                        <td style="font-size:12px;"><?php echo @$invoice_pdf[0]->BOOKRETURNFLIGHTNO; ?></td>
                        <td style="font-size:12px;"><?php echo @$invoice_pdf[0]->BOOKRETFROM; ?></td>
                        <td style="font-size:12px;"><?php echo @$invoice_pdf[0]->BOOKRETTO; ?></td>
                        <td style="font-size:12px;"><?php echo @$invoice_pdf[0]->BOOKRETURNDATE; ?></td>
                        <td style="font-size:12px;"><?php echo @$invoice_pdf[0]->BOOKRETURNADATEARRIVAL; ?></td>
                    </tr>
                </tbody>
            </table>
            <p>All times indicated are the local times at the relevant airports.</p>

            <?php
                if(count($destination_summary)!=0){
                    for($i=0;$i<count($destination_summary);$i++){
                        $destination = $destination_summary[$i];

                        $tot_rooms = 0;
                        if(count($r_summary[$i])!=0){
                            foreach ($r_summary[$i] as $r) {
                                $desc = $r['description'];
                                $desc_exp = explode(" - ", $desc);
                                if (count($desc_exp) == 1) { $desc_val = $desc_exp[0]; }
                                else { $desc_val = $desc_exp[count($desc_exp)-1]; }

                                $comp = $r['computation'];
                                $comp_exp = explode(" ", $comp);
                                $comp_val = intval(strip_tags($comp_exp[0]));

                                if ($desc_val == "Single") { $rooms=$comp_val; }
                                else if ($desc_val == "Twin") { $rooms=($comp_val/2); }
                                else if ($desc_val == "Double") { $rooms=($comp_val/2); }
                                else if ($desc_val == "Triple") { $rooms=($comp_val/3); }
                                else if ($desc_val == "Child Half Twin") { $rooms=($comp_val/2); }
                                else if ($desc_val == "Child With Bed") { $rooms=0; }
                                else if ($desc_val == "Child Without Bed") { $rooms=0; }

                                $tot_rooms+=$rooms;
                            }
                        }
            ?>
                        <strong>Your Hotel Rooms</strong>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Hotel</th>
                                    <th>Check In Date</th>
                                    <th>Check Out Date</th>
                                    <th>Total Rooms Utilize</th>
                                    <th>Extension Night/s</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td style="font-size:12px;"><?php echo $r_summary[$i][0]['hotel_name']; ?></td>
                                    <td style="font-size:12px;"><?php echo date('M d, Y',strtotime(@$invoice_pdf[0]->PACKCHECKIN));?></td>
                                    <td style="font-size:12px;"><?php echo date('M d, Y',strtotime(@$invoice_pdf[0]->PACKCHECKOUT));?></td>
                                    <td style="font-size:12px;"><?php echo round($tot_rooms); ?></td>
                                    <td style="font-size:12px;">
                                        <?php if(@$invoice_pdf[0]->BOOKEDEXTENDDAYS == 1):?>
                                        <?php echo @$invoice_pdf[0]->BOOKEDEXTENDDAYS;?>
                                        <?php else: ?>
                                        <?php echo @$invoice_pdf[0]->BOOKEDEXTENDDAYS;?>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <p>Types of Rooms:</p>
                        <?php
                            echo '<table class="table table-bordered">
                                <tr>';

                                $i_cnt = 0;
                                $i_data = count($r_summary[$i]);
                                foreach ($r_summary[$i] as $r) {

                                    $desc = $r['description'];
                                    $desc_exp = explode(" - ", $desc);
                                    if (count($desc_exp) == 1) { $desc_val = $desc_exp[0]; }
                                    else { $desc_val = $desc_exp[count($desc_exp)-1]; }

                                    $comp = $r['computation'];
                                    $comp_exp = explode(" ", $comp);
                                    $comp_val = intval(strip_tags($comp_exp[0]));

                                    if ($desc_val == "Single") { $rooms=$comp_val; }
                                    else if ($desc_val == "Twin") { $rooms=($comp_val/2); }
                                    else if ($desc_val == "Double") { $rooms=($comp_val/2); }
                                    else if ($desc_val == "Triple") { $rooms=($comp_val/3); }
                                    else if ($desc_val == "Child Half Twin") { $rooms=($comp_val/2); }
                                    else if ($desc_val == "Child With Bed") { $rooms=$comp_val; }
                                    else if ($desc_val == "Child Without Bed") { $rooms=$comp_val; }

                                    if ($i_cnt >= 6) {
                                        $i_cnt=1;
                                        echo "</tr><tr>
                                            <td style='font-size:12px; border: 1px solid #dddddd;'>
                                                <strong>{$desc}</strong><br>
                                                ".round($rooms)."
                                            </td>";
                                    }
                                    else {
                                        $i_cnt++;
                                        echo "<td style='font-size:12px; border: 1px solid #dddddd;'>
                                            <strong>{$desc}</strong><br>
                                            ".round($rooms)."
                                        </td>";
                                    }
                                }
                                
                                if ($i_data>6) {
                                    $i_cols = $i_data % 6;
                                    if ($i_cols != 0) {
                                        $i_mods = 6 - $i_cols;
                                        echo "<td colspan='{$i_mods}' style='font-size:12px; border: 1px solid #dddddd;'>-</td>";
                                    }
                                }
                            echo '</tr></table>';
                        ?>
            <?php
                    }
                }
                #addon summary ---------------------------------------------
                if(count($addon_summary)!=0){
                    echo '<p>Addons:</p>
                    <table class="table table-bordered">';
                       foreach ($addon_summary as $destination) {
                            echo "
                            <tr>
                                <td>{$destination['description']}</td>
                                <td>{$destination['computation']}</td>
                            </tr>";
                        }
                    echo '</table>';
                }
                #end of addon summary --------------------------------------

                #addtional hotel bookings ----------------------------------------------
                if(count(@$add_hotel_bookings[0]->ext_hotel_name) != 0 || count(@$add_hotel_bookings[0]->ext_hotel_chkin_chkout) != 0 || @$add_hotel_bookings[0]->ext_hotel_name != "" || @$add_hotel_bookings[0]->ext_hotel_chkin_chkout != ""){ 
                    $hotel_name=explode(',',$add_hotel_bookings[0]->ext_hotel_name);
                    $chkin_chkout=explode(',',$add_hotel_bookings[0]->ext_hotel_chkin_chkout);
                    if(@$hotel_name[0] != "" || @$hotel_name[1] != ""){ 
                    $chkin_chkout_=explode(':',@$chkin_chkout[0]);
                    $chkin_chkouts_=explode(':',@$chkin_chkout[1]);
                    ?>
                    <!-- <strong style="page-break-before: always;">Addtional hotel bookings</strong> -->
                    <strong>Addtional hotel bookings</strong>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Hotel</th>
                                <th>Check In Date</th>
                                <th>Check Out Date</th>
                                <th>Remarks</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="font-size:12px;"><?php if(isset($hotel_name[0])){echo  $hotel_name[0];} ?></td>
                                <td style="font-size:12px;"><?php if(isset($chkin_chkout_[0])){ echo  date('d-M-Y',strtotime($chkin_chkout_[0])); }?></td>
                                    <?php if(isset($chkin_chkout_[1])){ ?>
                                <td style="font-size:12px;"><?php echo  date('d-M-Y',strtotime($chkin_chkout_[1])); ?></td>
                                    <?php } ?>
                                <td style="font-size:12px;" <?php if(isset($hotel_name[1])){ ?>rowspan="2"<?php } ?>><?php if(isset($add_hotel_bookings[0]->remarks)){ echo  $add_hotel_bookings[0]->remarks;} ?></td>
                            </tr>
                           <?php if(isset($hotel_name[1])){ ?><tr><?php } ?>
                                <?php if(isset($hotel_name[1])){ ?>
                                <td style="font-size:12px;"><?php echo  $hotel_name[1]; ?></td>
                                <?php }  ?>
                                <?php if(isset($chkin_chkouts_[1])){ ?>
                                <td style="font-size:12px;"><?php echo  date('d-M-Y',strtotime($chkin_chkouts_[0])); ?></td>
                                <?php } ?>
                                <?php if(isset($chkin_chkouts_[1])){ ?>
                                <td style="font-size:12px;"><?php echo  date('d-M-Y',strtotime($chkin_chkouts_[1])); ?></td>
                                <?php } ?>
                            <?php if(isset($hotel_name[1])){ ?></tr><?php } ?>
                        </tbody>
                    </table>
                <?php
            }
            }
                #end of addtional hotel bookings ----------------------------------------------
            ?>
            <table class="table">
                <tr>
                    <td style="border:0px;">
                        <p style="font-size:14px;">
                            <strong>For Emergency:</strong><br>
                            Telephone  : <?php echo @$package_summarys[0]->PROVIDERPHONE;?><br>
                            <?php if(empty($num)){?>
                            Contact numbers : 
                            <?php 
                            $num=json_decode($package_summarys[0]->PROVIDEREMERGENCYNUMBER);
                            $num1=json_decode($package_summarys[0]->PROVIDEREMERGENCYNAME);
                            if($num != null || $num1 != null || $num != "" || $num1 != ""){
                                for($i=0; $i<count($num); $i++){
                                    echo $combine_data=$num[$i].' - '.$num1[$i].', ';
                                    }
                                }
                            }
                            ?>
                            <br>
                        </p>
                        <ul style="font-size:9px;">
                            <li>Check out time is strictly before 12 noon. All additional charges will be bear by the passengers. If passengers are late for airport pick-up, they will have to make their own transfer.</li>
                            <li>All rooms are guarantted on the day of arrival. In the case of a no-show, your room(s) will be released and you will be subjected to the terms and conditions of the Cancellation/No-Show Policy specified at the time you made the booking as well as noted in the Confirmation Email.</li>
                            <li>All special request are subjected to availability upon arrival.</li>
                            <li>This is a shared transfer service. As such, there may be multiple hotel pick-ups en route to Airport. All times are approximate and pick up will be provided within 30 minutes from the final pick up time confirmed. Road, traffic and weather conditions may affect the schedule. The pick up time from your hotel will be at least 3 and a half hours prior to your scheduled flight time. Please make sure that you wait at the hotel conceierge desk 15 minutes prior to your confirmed pick up time. Once at the Airport, the shuttle will make stops at different terminals as necessary. Drivers are not expected to know which terminal, so please make sure you check this information with the company you booked your flight with before boarding the shuttle for your transfer to the Airport.</li>
                            <li>There is no charge for a maximum of 1 piece of medium sized luggage per person. Oversize or excess luggage may have restrictions and you will be responsible for the transfer and cost of any extra luggage.</li>
                        </ul>
                        <p style="font-size:9px;">This is a computer generated service voucher. No signature is required.</p>
                    </td>
                </tr>
            </table> 
        </div>
        <!-- /details -->

    </body>
</html>