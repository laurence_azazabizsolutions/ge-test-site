<div id="page-wrapper">
    <!-- /.row -->
    <div class="row">
        <br>
        <?php
            $this->load->model("booking/booking");
        ?>
        <div class="col-lg-12">
            <!-- <form class="form-horizontal" role="form" action="<?php // echo base_url('panel/filter_data')?>" method="post"> -->
            <?php echo form_open('','id="booking_data" class="form-horizontal" role="form" method="GET"'); ?>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Booking Code</label>
                    <div class="col-sm-8">
                        <input type="text" name="booked_code" <?php echo "value='".@$_GET["booked_code"]."'"; ?>class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Booking Status</label>
                    <div class="col-sm-8">
                        <select  class="form-control" name="booking_status">
                            <option value="" <?php if(isset($_GET["booking_status"])){ if($_GET["booking_status"]==""){ echo "selected"; } } ?>>-</option>
                            <option value="pending" <?php if(isset($_GET["booking_status"])){ if($_GET["booking_status"]=="pending"){ echo "selected"; } } ?>>Pending</option>
                            <option value="confirmed" <?php if(isset($_GET["booking_status"])){ if($_GET["booking_status"]=="confirmed"){ echo "selected"; } } ?>>Confirmed</option>
                            <option value="rejected" <?php if(isset($_GET["booking_status"])){ if($_GET["booking_status"]=="rejected"){ echo "selected"; } } ?>>Rejected</option>
                            <option value="ammended" <?php if(isset($_GET["booking_status"])){ if($_GET["booking_status"]=="ammended"){ echo "selected"; } } ?>>Amended</option>
                            <option value="cancelled" <?php if(isset($_GET["booking_status"])){ if($_GET["booking_status"]=="cancelled"){ echo "selected"; } } ?>>Cancelled</option>
                        </select>
                    </div>
                </div>

                <!-- <div class="form-group">
                    <label class="col-sm-3 control-label">User</label>

                    <?php if(get_class($user) == 'Admin'){?>
                        <div class="col-sm-4">
                            <select  class="form-control" name="a_companyname">
                                <option value="" >All Agents</option>
                                <?php foreach ($result_agentprovider as $row) { 
                                    if($row->type != 'provider'){ ?>
                                        <option value="<?php echo $row->cname; ?>">
                                            <?php echo strtoupper($row->cname); ?>
                                        </option>
                                <?php } }?>
                            </select> <br>
                        </div>
                        <div class="col-sm-4">
                            <select  class="form-control" name="p_companyname">
                                <option value="" >All Providers</option>
                                <?php foreach ($result_agentprovider as $row) { 
                                    if($row->type != 'agent'){ ?>
                                        <option value="<?php echo $row->cname; ?>">
                                            <?php echo strtoupper($row->cname); ?>
                                        </option>
                                <?php } }?>
                            </select>
                        </div>

                    <?php } elseif (get_class($user) == 'Agent') {?>
                        <div class="col-sm-8">
                            <select  class="form-control" name="p_companyname">
                                <option value="" >All Providers</option>
                                <?php foreach ($result_agentprovider as $row) { 
                                    if($row->type != 'agent'){ ?>
                                        <option value="<?php echo $row->cname; ?>">
                                            <?php echo strtoupper($row->cname); ?>
                                        </option>
                                <?php } }?>
                            </select>
                        </div>

                    <?php } elseif (get_class($user) == 'Provider') {?>
                        <div class="col-sm-8">
                            <select  class="form-control" name="a_companyname">
                                <option value="" >All Agents</option>
                                <?php foreach ($result_agentprovider as $row) { 
                                    if($row->type != 'provider'){ ?>
                                        <option value="<?php echo $row->cname; ?>">
                                            <?php echo strtoupper($row->cname); ?>
                                        </option>
                                <?php } }?>
                            </select>
                        </div>

                     <?php }?>
                </div> -->

                <div class="form-group">
                    <label class="col-sm-3 control-label">Date</label>
                    <div class="col-sm-4">
                        <div class="input-group date" id="datetimepickerMonthViewOrigin">
                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                            <input <?php echo "value='".@$_GET["origin"]."'"; ?> class="form-control" type="text" placeholder="Origin" name="origin" data-date-format="YYYY-MM"/>
                        </div>
                        <br>
                    </div>
                    <div class="col-sm-4">
                        <div class="input-group date" id="datetimepickerMonthViewDepart">
                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                            <input <?php echo "value='".@$_GET["return"]."'"; ?>  class="form-control" type="text" placeholder="Return" name="return" data-date-format="YYYY-MM"/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-3">
                        <button class="btn btn-orange">Filter</button>
                    </div>
                </div>
            </form>

            <?php if (get_class($user) !== 'Customer') { ?>
                <div class="panel-group" id="accordion">
                    <div class="panel">
                        <div class="panel-heading">
                            <small class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"> Show / Hide Fields</a>
                            </small>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse">
                            <div class="table-responsive" style="margin-bottom:0px;">
                                <table class="table table-bordered" style="margin-bottom:0px;">
                                    <thead>
                                        <tr>
                                            <th><div class="checkbox">Columns</div></th>
                                            <td><div class="checkbox"><label><input type="checkbox" id="chkCode" CHECKED> Code </label></div></td>
                                            <td><div class="checkbox"><label><input type="checkbox" id="chkDetails" CHECKED> Details </label></div></td>
                                            <td><div class="checkbox"><label><input type="checkbox" id="chkFlight" CHECKED> Flight </label></div></td>
                                            <td><div class="checkbox"><label><input type="checkbox" id="chkAmount" CHECKED> Amount </label></div></td>
                                            <td><div class="checkbox"><label><input type="checkbox" id="chkPayment" CHECKED> Payment </label></div></td>
                                            <td><div class="checkbox"><label><input type="checkbox" id="chkStatus" CHECKED> Status </label></div></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th><div class="checkbox">Fields</div></th>
                                            <td></td>
                                            <td>
                                                <div class="checkbox"><label><input type="checkbox" id="chkPackage" CHECKED> Package Name </label></div>
                                                <div class="checkbox"><label><input type="checkbox" id="chkAgent" CHECKED> Agent Name  </label></div>
                                                <div class="checkbox"><label><input type="checkbox" id="chkProvider"> Provider Name  </label></div>
                                                <div class="checkbox"><label><input type="checkbox" id="chkHotel" CHECKED> Hotel Name  </label></div>
                                                <div class="checkbox"><label><input type="checkbox" id="chkTraveller" CHECKED> Traveller Name </label></div>
                                            </td>
                                            <td>
                                                <div class="checkbox"><label><input type="checkbox" id="chkOrigin" CHECKED> Origin </label></div>
                                                <div class="checkbox"><label><input type="checkbox" id="chkReturn" CHECKED> Return </label></div>
                                            </td>
                                            <td>
                                                <div class="checkbox"><label><input type="checkbox" id="chkCost"> Cost </label></div>
                                                <div class="checkbox"><label><input type="checkbox" id="chkProfit"> Profit </label></div>
                                                <div class="checkbox"><label><input type="checkbox" id="chkPrice" CHECKED> Price </label></div>
                                            </td>
                                            <td>
                                                <div class="checkbox"><label><input type="checkbox" id="chkAgentPayment" CHECKED> Agent Payment </label></div>
                                                <div class="checkbox"><label><input type="checkbox" id="chkProviderPayment" CHECKED> Provider Payment </label></div>
                                            </td>
                                            <td></td>
                                        </tr>
                                       
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <?php if (get_class($user)   === 'Customer') { $cusHide = 'style="display:none;"'; } else { $cusHide = ''; } ?>

            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th class="hideCode">Booking Code</th>
                            <th class="hideDetails">Details</th>
                            <th class="hideFlight">Flight</th>
                            <th class="hideAmount">Amount</th>
                            <th class="hidePayment" <?php echo $cusHide; ?>>Payment</th>
                            <th class="hideStatus">Status</th>
                        </tr>
                    </thead>
                    <tbody id="data_list">
                        <?php
                        if (isset($booked_data)): ?>
                            <?php foreach ($booked_data as $row): 

                                if ($row->PSTAT === NULL) {
                                    $pstat_agent = "No";
                                    $pstat_provider = "No";
                                } else {
                                    if ($row->PSTAT === "collected") {
                                        $pstat_agent = "Yes";
                                        $pstat_provider = "No";
                                    } else {
                                        $pstat_agent = "Yes";
                                        $pstat_provider = "Yes";
                                    }
                                }

                                if ($row->BOOKINGSTATUS == "cancelled") {
                                    $tr_color = "danger";
                                } else if ($row->BOOKINGSTATUS === "ammended") {
                                    $row->BOOKINGSTATUS = "amended";
                                    $tr_color = "";
                                } else { $tr_color = ""; }
                                //dump($booked_data);
                            ?>

                                <tr class="<?php echo $tr_color;?>">
                                    <td class="hideCode">
                                        <a href="<?php echo base_url('panel/voucher/'.$row->BOOKID)?>" target="_blank"><?php echo $row->BOOKINGCODE;?></a> <br>
                                        <a href="<?php echo base_url('panel/package/'.$row->BOOKID)?>">View</a>
                                    </td>
                                    <td class="hideDetails">
                                        <div class="hidePackage"><strong>Package:</strong> <?php echo $row->PACKAGENAME; ?></div>
                                        <div class="hideAgent"><strong>Agent:</strong> <?php echo $row->COMPANYNAME; ?></div>
                                        <div class="hideProvider" style="display:none;"><strong>Provider:</strong> <?php echo $row->COMPANYNAMEP; ?></div>
                                        <div class="hideHotel"><strong>Hotel:</strong>
                                            <?php
                                                //$tmp=array();
                                                //foreach ($row->hotel as $key){
                                                //    $tmp[]=$key->hotels_name; 
                                                //}
                                                //echo implode(' ,',$tmp);

                                                 $hotelname = $this->booking->getHotelName($row->BOOKINGCODE);
                                                 //dump($hotelname);
                                                 foreach ($hotelname as $hn) {
                                                     echo $hn['name'];
                                                 }
                                            ?>
                                        </div>
                                        <div class="hideTraveller"><strong>Traveller:</strong> <?php echo $row->TRAVELLLERNAME ." (".$row->PAX." PAX)"; ?></div>
                                    </td>
                                    <td class="hideFlight">
                                        <div class="hideOrigin">
                                            <strong>Ori:</strong> <?php echo date("d-M-Y", mktime(0,0,0,substr($row->BOOKINGDEPARTUREDATE,5,2), substr($row->BOOKINGDEPARTUREDATE,8,2), substr($row->BOOKINGDEPARTUREDATE,0,4))); ?><br>
                                            <i><?php echo $row->OFDEPARTTIME; ?>-<?php echo $row->OFARRIVALTIME; ?> hrs</i>
                                        </div>
                                        <div class="hideReturn">
                                            <strong>Ret:</strong> <?php echo date("d-M-Y", mktime(0,0,0,substr($row->BOOKINGRETURNDATE,5,2), substr($row->BOOKINGRETURNDATE,8,2), substr($row->BOOKINGRETURNDATE,0,4))); ?><br>
                                            <i><?php echo $row->RFDEPARTTIME; ?>-<?php echo $row->RFARRIVALTIME; ?> hrs</i>
                                        </div>
                                    </td>
                                    <td class="hideAmount">
                                        <div class="hideCost" style="display:none;"><strong>Cost:</strong> <?php echo $row->BOOKINGCOST; ?></div>
                                        <div class="hideProfit" style="display:none;"><strong>Profit:</strong> <?php echo $row->BOOKINGPROFIT; ?></div>

                                        <div class="hidePrice"><strong>Price:</strong> <br/><?php echo $row->BOOKINGPRICE+$row->BOOKINGADDITIONAL-$row->BOOKINGDISCOUNT.' SGD'; ?></div>

                                    </td>
                                    <td class="hidePayment" <?php echo $cusHide; ?>>
                                        <div class="hideAgentPayment"><strong>Agent Paid:</strong> <?php echo $pstat_agent; ?></div>
                                        <?php foreach ($user_type as $type ) : ?>
                                            <?php if($type->type === 'admin'): ?>
                                                <div class="hideProviderPayment"><strong>Provider Paid:</strong> <?php echo $pstat_provider; ?></div>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    </td>
                                    <td class="hideStatus"><?php echo $row->BOOKINGSTATUS; ?></td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
            <!-- /.table-responsive -->

        </div>
    </div>

    <div id="fucker"class="row text-center">
        <?php
            echo $page_links;
        ?>
    </div>

     <?php if(in_array('cost',$this->session->userdata('fields'))){
            echo "<script type=text/javascript>
            document.getElementById('chkCost').checked = true;
            if(document.getElementById('chkCost').checked)
            { $('.hideCost').show();}</script>";
        }
        if(in_array('profit',$this->session->userdata('fields'))){
            echo "<script type=text/javascript>
            document.getElementById('chkProfit').checked = true;
            if(document.getElementById('chkProfit').checked)
            { $('.hideProfit').show();}
            </script>";
        }

        //provider
        if(in_array('provider',$this->session->userdata('fields'))){
            echo "<script type=text/javascript>
            document.getElementById('chkProvider').checked = true;
            if(document.getElementById('chkProvider').checked)
            { $('.hideProvider').show();}
            </script>";
        }

        //code
        if(in_array('code',$this->session->userdata('fields'))){
            echo "<script type=text/javascript>
            document.getElementById('chkCode').checked = true;
            if(document.getElementById('chkCode').checked)
            {  $('.hideCode').show();}
            </script>";
        }else{
            echo "<script type=text/javascript>document.getElementById('chkCode').checked = false;
            $('.hideCode').css('display','none');</script>";

        }

        //details
        if(in_array('details',$this->session->userdata('fields'))){
            echo "<script type=text/javascript>
            document.getElementById('chkDetails').checked = true;
            if(document.getElementById('chkDetails').checked)
            {  $('.hideDetails').show();}
            </script>";
        }else{
            echo "<script type=text/javascript>document.getElementById('chkDetails').checked = false;
            $('.hideDetails').css('display','none');</script>";

        }

        //flight
        if(in_array('flight',$this->session->userdata('fields'))){
            echo "<script type=text/javascript>
            document.getElementById('chkFlight').checked = true;
            if(document.getElementById('chkFlight').checked)
            {  $('.hideFlight').show();}
            </script>";
        }else{
            echo "<script type=text/javascript>document.getElementById('chkFlight').checked = false;
            $('.hideFlight').css('display','none');</script>";

        }

        //amount
        if(in_array('amount',$this->session->userdata('fields'))){
            echo "<script type=text/javascript>
            document.getElementById('chkAmount').checked = true;
            if(document.getElementById('chkAmount').checked)
            {  $('.hideAmount').show();}
            </script>";
        }else{
            echo "<script type=text/javascript>document.getElementById('chkAmount').checked = false;
            $('.hideAmount').css('display','none');</script>";

        }

        //payment
        if(in_array('payment',$this->session->userdata('fields'))){
            echo "<script type=text/javascript>
            document.getElementById('chkPayment').checked = true;
            if(document.getElementById('chkPayment').checked)
            {  $('.hidePayment').show();}
            </script>";
        }else{
            echo "<script type=text/javascript>document.getElementById('chkPayment').checked = false;
            $('.hidePayment').css('display','none');</script>";

        }

        //status
        if(in_array('status',$this->session->userdata('fields'))){
            echo "<script type=text/javascript>
            document.getElementById('chkStatus').checked = true;
            if(document.getElementById('chkStatus').checked)
            {  $('.hideStatus').show();}
            </script>";
        }else{
            echo "<script type=text/javascript>document.getElementById('chkStatus').checked = false;
            $('.hideStatus').css('display','none');</script>";

        }

        //package name
        if(in_array('package',$this->session->userdata('fields'))){
            echo "<script type=text/javascript>
            document.getElementById('chkPackage').checked = true;
            if(document.getElementById('chkPackage').checked)
            {  $('.hidePackage').show();}
            </script>";
        }else{
            echo "<script type=text/javascript>document.getElementById('chkPackage').checked = false;
            $('.hidePackage').css('display','none');</script>";

        }

        //agent name
        if(in_array('agent',$this->session->userdata('fields'))){
            echo "<script type=text/javascript>
            document.getElementById('chkAgent').checked = true;
            if(document.getElementById('chkAgent').checked)
            {  $('.hideAgent').show();}
            </script>";
        }else{
            echo "<script type=text/javascript>document.getElementById('chkAgent').checked = false;
            $('.hideAgent').css('display','none');</script>";

        }

        //hotel name
        if(in_array('hotel',$this->session->userdata('fields'))){
            echo "<script type=text/javascript>
            document.getElementById('chkHotel').checked = true;
            if(document.getElementById('chkHotel').checked)
            {  $('.hideHotel').show();}
            </script>";
        }else{
            echo "<script type=text/javascript>document.getElementById('chkHotel').checked = false;
            $('.hideHotel').css('display','none');</script>";

        }

        //traveller name
        if(in_array('traveller',$this->session->userdata('fields'))){
            echo "<script type=text/javascript>
            document.getElementById('chkTraveller').checked = true;
            if(document.getElementById('chkTraveller').checked)
            {  $('.hideTraveller').show();}
            </script>";
        }else{
            echo "<script type=text/javascript>document.getElementById('chkTraveller').checked = false;
            $('.hideTraveller').css('display','none');</script>";

        }

        //origin
        if(in_array('origin',$this->session->userdata('fields'))){
            echo "<script type=text/javascript>
            document.getElementById('chkOrigin').checked = true;
            if(document.getElementById('chkOrigin').checked)
            {  $('.hideOrigin').show();}
            </script>";
        }else{
            echo "<script type=text/javascript>document.getElementById('chkOrigin').checked = false;
            $('.hideOrigin').css('display','none');</script>";

        }

        //return
        if(in_array('return',$this->session->userdata('fields'))){
            echo "<script type=text/javascript>
            document.getElementById('chkReturn').checked = true;
            if(document.getElementById('chkReturn').checked)
            {  $('.hideReturn').show();}
            </script>";
        }else{
            echo "<script type=text/javascript>document.getElementById('chkReturn').checked = false;
            $('.hideReturn').css('display','none');</script>";

        }

        //price
        if(in_array('price',$this->session->userdata('fields'))){
            echo "<script type=text/javascript>
            document.getElementById('chkPrice').checked = true;
            if(document.getElementById('chkPrice').checked)
            {  $('.hidePrice').show();}
            </script>";
        }else{
            echo "<script type=text/javascript>document.getElementById('chkPrice').checked = false;
            $('.hidePrice').css('display','none');</script>";

        }

        //agent payment
        if(in_array('agent_pay',$this->session->userdata('fields'))){
            echo "<script type=text/javascript>
            if(document.getElementById('chkAgentPayment').checked)
            {  $('.hideAgentPayment').css('display','block');}
            </script>";
        }else{ 
            echo "<script type=text/javascript>document.getElementById('chkAgentPayment').checked = false;
            $('.hideAgentPayment').css('display','none');</script>";
        }

        //provider payment
        if(in_array('provider_pay',$this->session->userdata('fields'))){
            echo "<script type=text/javascript>
            if(document.getElementById('chkProviderPayment').checked)
            {  $('.hideProviderPayment').css('display','block');}
            </script>";
        }else{ 
            echo "<script type=text/javascript>document.getElementById('chkProviderPayment').checked = false;
            $('.hideProviderPayment').css('display','none');</script>";
        }

    ?>

</div>
<!-- /#page-wrapper -->