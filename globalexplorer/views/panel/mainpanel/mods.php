<div id="page-wrapper">
    <style>
        .table thead {background: #CCC}
        /*.table tbody > tr:nth-child(odd) {background: #CCC}*/
        /*.table tbody > tr:nth-child(even) {background: #FFF}*/
    </style>
    <!-- /.row -->
    <div class="row">
        <br>
        <div class="col-lg-12">
            <!-- Nav tabs -->
            <ul class="nav nav-pills" role="tablist">
                <!-- li class="active"><a href="#new" role="tab" data-toggle="tab">New</a></li -->
                <li class="active"><a href="#newdestination" role="tab" data-toggle="tab">New Destination</a></li>
                <li><a href="#countries" role="tab" data-toggle="tab">Countries/Destinations</a></li>
                <li><a href="#newairportcode" role="tab" data-toggle="tab">New Airport Code</a></li>
                <li><a href="#airportcodes" role="tab" data-toggle="tab">Airport Codes</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane active" id="newdestination">
                    <br>
                    <form id="create_destination" class="form-horizontal" role="form" method="post" action="<?php echo base_url('panel/create_destination')?>">
                        <div class="alert" style="display: none;"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Country</label>
                            <div class="col-sm-8">
                                <select name="country" class="dest form-control">
                                    <option value="">- Select -</option>
                                    <?php if (isset($countries_usr)): ?>
                                        <?php foreach ($countries_usr as $code=>$country): ?>
                                            <option value="<?php echo $country; ?>"><?php echo $country; ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">City</label>
                            <div class="col-sm-8">
                                <input type="text" class="dest form-control" placeholder="Cebu" name="city">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Code</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" placeholder="CEB" name="code">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-8 col-sm-offset-3">
                                <button type="submit" class="btn btn-orange">Add</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="tab-pane" id="countries">
                    <br>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Destination</th>
                                    <th>Code</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php if (isset($destinations)): ?>
                    			<?php foreach ($destinations as $destination): ?>
                                    <?php $this->view('panel/mainpanel/append_destination', array('destination' => $destination));?>
                                <?php endforeach; ?>
                			<?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane" id="newairportcode">
                    <br>
                    <form id="create_airportcode" class="form-horizontal" role="form" method="post" action="<?php echo base_url('panel/create_airportcode')?>">
                        <div class="alert" style="display: none;"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Airport/City</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" placeholder="Chiang Mai" name="airportcode">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Code</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" placeholder="CNX" name="code">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-8 col-sm-offset-3">
                                <button type="submit" class="btn btn-orange">Add</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="tab-pane" id="airportcodes">
                    <br>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Airport Code</th>
                                    <th>Code</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php if (isset($airportcodes)): ?>
                                <?php foreach ($airportcodes as $airportcode): ?>
                                    <?php $this->view('panel/mainpanel/append_airportcode', array('airportcode' => $airportcode));?>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- /#page-wrapper -->

