<div id="page-wrapper">
    <!-- /.row -->
    <div class="row">
        <br>
        

        <?php
            $status = @$this->session->flashdata("status");
            $msg = @$this->session->flashdata("msg");
            if($status!=NULL){
               $class = "";
               if($status=="success"){
                    $class = "alert alert-success";
               }else{
                    $class = "alert alert-danger";
               }

               echo "
                    <div class='{$class} text-center col-md-12' id='success_id'>
                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
                        {$msg}
                    </div>
                    ";
            }
        ?>

        <div class="col-lg-12">
            <!-- <form class="form-horizontal" role="form" action="<?php echo base_url('panel/do_upload')?>" method="post"> -->
                <?php echo form_open_multipart('panel/do_upload_attachment','class="form-horizontal" role="form"');?>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Destination</label>
                    <div class="col-sm-8">
                        <select class="form-control" name="destination">
                            <option value=" ">- Select -</option>
                            <?php if (isset($countries)): ?>
                            <?php foreach ($countries as $country): ?>
                            <option value="<?php echo $country['code']; ?>"><?php echo $country['country']; ?></option>
                            <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Title</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="title">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Upload</label>
                    <div class="col-sm-8">
                        <input type="file" name="pdf_file">
                        <p class="help-block">Only PDF file &lt; 2MB are allowed.</p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-3">
                        <input type="submit" class="btn btn-orange" value="Submit" name="button"/>
                    </div>
                </div>
            </form>

            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Action</th>
                            <th>Country</th>
                            <th>Title</th>
                        </tr>
                    </thead>
                    <tbody id="list_destination">    
                            <?php if (isset($link_destination)): ?>
                                <?php foreach ($link_destination as $link): ?>
                                    <tr> 
                                        <td><a href="javascript:void(0);" onclick="delete_dest('<?php echo $link->did;?>','<?php echo $link->title;?>','<?php echo $link->pdfname;?>'); return false;">Delete</a></td>
                                        <td><?php echo $link->Country;?></td>
                                        <td><a href="<?php echo base_url('uploads/destination_pdf/'.$link->pdfname); ?>" target="_blank"><?php echo $link->title;?></a></td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php endif; ?>
                    </tbody>
                </table>
            </div>
            <!-- /.table-responsive -->

        </div>
    </div>
    <!-- myModalDeletePDF -->
    <div class="modal fade" id="myModalDeletePDF" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="pull-right btn btn-danger btn-sm" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Are you sure you want to delete</h4>
                </div>
                <form id="contact_form" class="form-horizontal" role="form" method="post" action="#">
                    <div class="modal-body">
                        <p id="myModalDeletePDFTitle">file name?</p>
                    </div>
                    <div class="modal-footer" id="myModalDeletePDFButton">
                        <input type="hidden" id="myModalDeletePDFCode">
                        <input type="hidden" id="myModalDeletePDFFile">
                        <a class="btn btn-orange" href="" role="button" data-dismiss="modal" onclick="delete_dest_true()">Continue</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- /#page-wrapper -->