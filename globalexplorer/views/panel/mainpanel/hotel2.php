<div id="page-wrapper">
    <!-- /.row -->
    <div class="row">
        <br>
        <div class="col-lg-12">
            <form class="form-horizontal" role="form">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Hotel Name</label>
                    <div class="col-sm-8">
                        <select class="form-control">
                            <option>-</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-3">
                        <button type="button" class="btn btn-orange">Filter</button>
                        <button type="reset" class="btn btn-default" data-toggle="modal" data-target="#myModalHotel">Add New</button>
                    </div>
                </div>
            </form>

            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>HOTEL NAME</th>
                            <th>DETAILS</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                AMBA TAIPEI XIMENDING <br>
                                <a href="#" data-toggle="modal" data-target="#myModalHotel">Edit</a> | 
                                <a href="#">Delete</a>
                            </td>
                            <td>
                                Hotel Surcharges:
                                <a href="#" data-toggle="modal" data-target="#myModalHotelSurcharge">Add</a> <br>
                                <table class="table table-striped table-bordered table-hover">
                                    <tr>
                                        <th></th>
                                        <th>Description</th>
                                        <th>Rule</th>
                                        <th>Cost</th>
                                        <th>Profit</th>
                                        <th>Price</th>
                                        <th>Rate Type</th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="#" data-toggle="modal" data-target="#myModalHotelSurcharge">Edit</a> | 
                                            <a href="#">Delete</a>
                                        </td>
                                        <td>Peak Period</td>
                                        <td>Date Range: 16-May-2013#18-May-2013</td>
                                        <td>45</td>
                                        <td>0</td>
                                        <td>45</td>
                                        <td>Room / Night</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- /.table-responsive -->
        </div>
    </div>

    <!-- myModalHotel -->
    <div class="modal fade" id="myModalHotel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="pull-right btn btn-danger btn-sm" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="myModalLabel">Hotel:</h4>
          </div>
          <div class="modal-body">
            <form id="create_hotel_form" class="form-horizontal" role="form" method="post" action="<?php echo base_url('panel/register_hotel')?>">
              <div class="form-group">
                <label class="col-xs-4 control-label">Hotel Name</label>
                <div class="col-xs-8">
                  <input type="text" class="form-control" name="hotel_name">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-4 control-label">Location</label>
                <div class="col-xs-8">
                  <input type="text" class="form-control" name="hotel_location">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-4 control-label">Country</label>
                <div class="col-xs-8">
                  <select class="form-control" name="hotel_country">
                    <?php if (isset($countries)): ?>
                    <option value="">- Select-</option>
                    <?php foreach ($countries as $country): ?>
                    <option value="<?php echo $country['code']; ?>"><?php echo $country['country']; ?></option>
                    <?php endforeach; ?>
                    <?php endif; ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-4 control-label">Website</label>
                <div class="col-xs-8">
                  <input type="url" class="form-control" name="hotel_website">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-4 control-label">Phone</label>
                <div class="col-xs-8">
                  <input type="text" class="form-control" name="hotel_phone">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-4 control-label">Email</label>
                <div class="col-xs-8">
                  <input type="email" class="form-control" name="hotel_email">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-4 control-label">Description</label>
                <div class="col-xs-8">
                  <textarea  class="form-control" name="hotel_description"> </textarea>
                </div>
              </div>
              <div class="modal-footer">
                <button class="btn btn-orange" type="submit" role="button">Save</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

    <!-- myModalHotelSurcharge -->
    <div class="modal fade" id="myModalHotelSurcharge" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="pull-right btn btn-danger btn-sm" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="myModalLabel">Hotel Surcharge:</h4>
          </div>
          <div class="modal-body">
            <form class="form-horizontal" role="form">
              <div class="form-group">
                <label class="col-xs-4 control-label">Description</label>
                <div class="col-xs-8">
                  <input type="text" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-4 control-label">Rule Type</label>
                <div class="col-xs-8">
                  <select class="form-control">
                    <option>Date Range</option>
                    <option>Day of Week</option>
                    <option>Agent</option>
                    <option>Blockout</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-4 control-label">Rule</label>
                <div class="col-xs-8">
                  <select class="form-control">
                    <option>From and To</option>
                    <option>Mon - Sun</option>
                    <option>List of Agent</option>
                    <option>From and To</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-4 control-label">Cost</label>
                <div class="col-xs-8">
                  <input type="number" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-4 control-label">Profit</label>
                <div class="col-xs-8">
                  <input type="number" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-4 control-label">Rate Type</label>
                <div class="col-xs-8">
                  <select class="form-control">
                    <option>Pax / Night</option>
                    <option>Adult / Night</option>
                    <option>Child / Night</option>
                    <option>Infant / Night</option>
                    <option>Room / Night</option>
                    <option>Pax / Trip</option>
                    <option>Adult / Trip</option>
                    <option>Child / Trip</option>
                    <option>Infant / Trip</option>
                    <option>Room / Trip</option>
                  </select>
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <a class="btn btn-orange" href="#" data-dismiss="modal" role="button">Save</a>
          </div>
        </div>
      </div>
    </div>

    <div class="row text-center">
        <ul class="pagination pagination-sm">
            <li><a href="#">&laquo;</a></li>
            <li class="disabled"><a href="#">1</a></li>
            <li class="active"><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li><a href="#">&raquo;</a></li>
        </ul>
    </div>
</div>
<!-- /#page-wrapper -->