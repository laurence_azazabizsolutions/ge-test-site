<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Global Explorer</title>
        <style type="text/css">
            h1,
            h4{
                font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
                font-weight: 500;
                line-height: 1.1;
                color: inherit;
            }
        </style>
    </head>
    <body style="color: #333333;">
        <table cellspacing="0" cellpadding="0" style="background-color: #ffffff; border: 1px solid transparent;  border-radius: 4px; -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05); box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05); border-color: #e95a3e;">
            <tr>
                <td style="padding: 10px 15px; border-bottom: 1px solid transparent; border-top-right-radius: 3px; border-top-left-radius: 3px; color: #fff; background-color: #e95a3e; border-color: #e95a3e;">
                    <h1 style="margin-top: 0; margin-bottom: 0; font-size: 16px; color: inherit; text-align: center; ">
                        Global Explorer
                    </h1> 
                </td>
            </tr>
            <tr>
                <td style="padding: 15px;">
                    <?php
                        if (isset($guest_summary)) {
                            $rows = 1;
                            $cols = 1;
                            $tbl_arr = array();

                            $adult = 0;
                            $child = 0;
                            $infant = 0;
                            $pax = 0;
                            foreach ($guest_summary as $row){
                                //count user type
                                if (@$row->type == "adult") {
                                    $adult++;
                                    $pax++;
                                }
                                else if (@$row->type == "child") {
                                    $child++;
                                    $pax++;
                                }
                                else if (@$row->type == "infant") {
                                    $infant++;
                                    $pax++;
                                }

                                //initialize users
                                if ($cols <= 5) {
                                    $tbl_arr[$rows][$cols] = @$row->name ." (". ucfirst(substr(@$row->type, 0, 1)).")";
                                    $cols++;
                                }
                                else {
                                    $cols = 1;
                                    $rows++;
                                    $tbl_arr[$rows][$cols] = @$row->name ." (". ucfirst(substr(@$row->type, 0, 1)).")";
                                    $cols++;
                                }
                            }
                        }
                    ?>
                    <table style="width: 770px; margin-bottom: 20px; border-collapse: collapse;">
                        <tr>
                            <td><h4 style="font-size: 18px; margin-top: 10px; margin-bottom: 10px;">Good day, <?php echo $recipient; ?>!</h4></td>
                        </tr>
                        <tr>
                            <td>
                                <p style="margin: 0;">
                                    A booking has made for <strong><?php echo @$data_summary[0]->PACTOURTITLE;?></strong>
                                    with <?php echo $adult;?> adult/s, <?php echo $child;?> child/rens, <?php echo $infant;?> infant/s on <?php echo date('M d, Y',strtotime(@$data_summary[0]->PACKCHECKIN));?> until <?php echo date('M d, Y',strtotime(@$data_summary[0]->PACKCHECKOUT));?>.
                                </p>
                            </td>
                        </tr>
                    </table>

                    <table style="width: 770px; margin-bottom: 20px; border-collapse: collapse;">
                        <tr>
                            <td>
                                <p style="margin: 0;">
                                    <strong>Booked On:</strong> <?php echo date('d-M-Y',strtotime(@$data_summary[0]->BOOKDATE));?><br>
                                    <strong>Package:</strong> <?php echo @$data_summary[0]->PACNIGHTS;?> nights<br>
                                    <strong>Extension:</strong>
                                    <?php 
                                        if(@$data_summary[0]->BOOKEDEXTENDDAYS == 0){
                                            $total_extended= @$data_summary[0]->BOOKEDEXTENDDAYS;
                                        }else{
                                            $total_extended= @$data_summary[0]->BOOKEDEXTENDDAYS - 1;
                                        }
                                        echo $total_extended;
                                    ?>
                                    nights<br>
                                    <strong>Total:</strong>
                                    <?php 
                                        $totals= $total_extended + @$data_summary[0]->PACNIGHTS;     
                                        echo  $totals;
                                    ?>
                                    nights <br><br>

                                    <strong>Booking No:</strong> <?php echo @$data_summary[0]->BOOKNO;?> <br>
                                    <strong>Tour Code:</strong> <?php echo @$data_summary[0]->PACTOURCODE;?> <br>

                                    <?php
                                        if (@$data_summary[0]->BOOKSTATUS == "ammended") {
                                            @$data_summary[0]->BOOKSTATUS = "amended";
                                        }
                                    ?>
                                    <strong>Status:</strong> <?php echo @$data_summary[0]->BOOKSTATUS;?>
                                </p>
                            </td>
                        </tr>
                    </table>

                    <strong>Booking Reference</strong>
                    <table style="width: 770px; margin-bottom: 20px; border-collapse: collapse;">
                        <thead>
                            <tr>
                                <td style="padding: 8px; vertical-align: bottom; border: 1px solid #dddddd;"><strong>Name if Passenger/s:</strong></td>
                                <td style="padding: 8px; vertical-align: bottom; border: 1px solid #dddddd;"><strong>(A)dult: <?php echo $adult; ?></strong></td>
                                <td style="padding: 8px; vertical-align: bottom; border: 1px solid #dddddd;"><strong>(C)hild: <?php echo $child; ?></strong></td>
                                <td style="padding: 8px; vertical-align: bottom; border: 1px solid #dddddd;"><strong>(I)nfant: <?php echo $infant; ?></strong></td>
                                <td style="padding: 8px; vertical-align: bottom; border: 1px solid #dddddd;"><strong>No of Pax: <?php echo $pax; ?></strong></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                for ($r=1; $r <= $rows; $r++) {
                                    echo "<tr>";
                                    for ($c=1; $c <= 5 ; $c++) { 
                                        echo "<td style='padding: 8px; line-height: 1.428571429; vertical-align: top; border: 1px solid #dddddd;'>";
                                            if (@$tbl_arr[$r][$c] != "" || @$tbl_arr[$r][$c] != NULL) {
                                                echo $tbl_arr[$r][$c];
                                            }
                                        echo "</td>";
                                    }
                                    echo "</tr>";
                                }
                            ?>
                        </tbody>
                    </table>

                    <strong>Your Itinerary</strong>
                    <table style="width: 770px; margin-bottom: 20px; border-collapse: collapse;">
                        <thead>
                            <tr>
                                <td style="padding: 8px; vertical-align: bottom; border: 1px solid #dddddd;"><strong>Date</strong></td>
                                <td style="padding: 8px; vertical-align: bottom; border: 1px solid #dddddd;"><strong>Flight No.</strong></td>
                                <td style="padding: 8px; vertical-align: bottom; border: 1px solid #dddddd;"><strong>From</strong></td>
                                <td style="padding: 8px; vertical-align: bottom; border: 1px solid #dddddd;"><strong>To</strong></td>
                                <td style="padding: 8px; vertical-align: bottom; border: 1px solid #dddddd;"><strong>Departure</strong></td>
                                <td style="padding: 8px; vertical-align: bottom; border: 1px solid #dddddd;"><strong>Arriving</strong></td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="padding: 8px; line-height: 1.428571429; vertical-align: top; border: 1px solid #dddddd;"><?php echo date('M d, Y',strtotime(@$data_summary[0]->PACKCHECKIN)); ?></td>
                                <td style="padding: 8px; line-height: 1.428571429; vertical-align: top; border: 1px solid #dddddd;"><?php echo @$data_summary[0]->BOOKDEPARTFLIGHTNO; ?></td>
                                <td style="padding: 8px; line-height: 1.428571429; vertical-align: top; border: 1px solid #dddddd;"><?php echo @$data_summary[0]->BOOKDEPFROM; ?></td>
                                <td style="padding: 8px; line-height: 1.428571429; vertical-align: top; border: 1px solid #dddddd;"><?php echo @$data_summary[0]->BOOKDEPTO; ?></td>
                                <td style="padding: 8px; line-height: 1.428571429; vertical-align: top; border: 1px solid #dddddd;"><?php echo @$data_summary[0]->BOOKDEPARTDATE; ?></td>
                                <td style="padding: 8px; line-height: 1.428571429; vertical-align: top; border: 1px solid #dddddd;"><?php echo @$data_summary[0]->BOOKDEPARTADATE; ?></td>
                            </tr>
                            <tr>
                                <td style="padding: 8px; line-height: 1.428571429; vertical-align: top; border: 1px solid #dddddd;"><?php echo date('M d, Y',strtotime(@$data_summary[0]->PACKCHECKOUT)); ?></td>
                                <td style="padding: 8px; line-height: 1.428571429; vertical-align: top; border: 1px solid #dddddd;"><?php echo @$data_summary[0]->BOOKRETURNFLIGHTNO; ?></td>
                                <td style="padding: 8px; line-height: 1.428571429; vertical-align: top; border: 1px solid #dddddd;"><?php echo @$data_summary[0]->BOOKRETFROM; ?></td>
                                <td style="padding: 8px; line-height: 1.428571429; vertical-align: top; border: 1px solid #dddddd;"><?php echo @$data_summary[0]->BOOKRETTO; ?></td>
                                <td style="padding: 8px; line-height: 1.428571429; vertical-align: top; border: 1px solid #dddddd;"><?php echo @$data_summary[0]->BOOKRETURNDATE; ?></td>
                                <td style="padding: 8px; line-height: 1.428571429; vertical-align: top; border: 1px solid #dddddd;"><?php echo @$data_summary[0]->BOOKRETURNADATE; ?></td>
                            </tr>
                        </tbody>
                    </table>
                    <p>All times indicated are the local tiems at the relevant airports.</p>

                    <?php
                        if(count($destination_summary)!=0){
                            for($i=0;$i<count($destination_summary);$i++){
                                $destination = $destination_summary[$i];

                                $tot_rooms = 0;
                                if(count($r_summary[$i])!=0){
                                    foreach ($r_summary[$i] as $r) {
                                        $desc = $r['description'];
                                        $desc_exp = explode(" - ", $desc);
                                        if (count($desc_exp) == 1) { $desc_val = $desc_exp[0]; }
                                        else { $desc_val = $desc_exp[count($desc_exp)-1]; }

                                        $comp = $r['computation'];
                                        $comp_exp = explode(" ", $comp);
                                        $comp_val = intval(strip_tags($comp_exp[0]));

                                        if ($desc_val == "Single") { $rooms=$comp_val; }
                                        else if ($desc_val == "Twin") { $rooms=($comp_val/2); }
                                        else if ($desc_val == "Double") { $rooms=($comp_val/2); }
                                        else if ($desc_val == "Triple") { $rooms=($comp_val/3); }
                                        else if ($desc_val == "Child Half Twin") { $rooms=($comp_val/2); }
                                        else if ($desc_val == "Child With Bed") { $rooms=0; }
                                        else if ($desc_val == "Child Without Bed") { $rooms=0; }

                                        $tot_rooms+=$rooms;
                                    }
                                }
                    ?>
                                <strong>Your Hotel Rooms</strong>
                                <table style="width: 770px; margin-bottom: 20px; border-collapse: collapse;">
                                    <thead>
                                        <tr>
                                            <td style="padding: 8px; vertical-align: bottom; border: 1px solid #dddddd;"><strong>Hotel</strong></td>
                                            <td style="padding: 8px; vertical-align: bottom; border: 1px solid #dddddd;"><strong>Check In Date</strong></td>
                                            <td style="padding: 8px; vertical-align: bottom; border: 1px solid #dddddd;"><strong>Check Out Date</strong></td>
                                            <td style="padding: 8px; vertical-align: bottom; border: 1px solid #dddddd;"><strong>Total Rooms Utilize</strong></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="padding: 8px; line-height: 1.428571429; vertical-align: top; border: 1px solid #dddddd;"><?php echo $r_summary[$i][0]['hotel_name']; ?></td>
                                            <td style="padding: 8px; line-height: 1.428571429; vertical-align: top; border: 1px solid #dddddd;"><?php echo date('M d, Y',strtotime(@$data_summary[0]->PACKCHECKIN));?></td>
                                            <td style="padding: 8px; line-height: 1.428571429; vertical-align: top; border: 1px solid #dddddd;"><?php echo date('M d, Y',strtotime(@$data_summary[0]->PACKCHECKOUT));?></td>
                                            <td style="padding: 8px; line-height: 1.428571429; vertical-align: top; border: 1px solid #dddddd;"><?php echo round($tot_rooms); ?></td>
                                        </tr>
                                    </tbody>
                                </table>

                                <p>Types of Rooms:</p>
                                <?php
                                    echo '<table style="width: 770px; margin-bottom: 20px; border-collapse: collapse;">
                                        <tr>';

                                        $i_cnt = 0;
                                        $i_data = count($r_summary[$i]);
                                        foreach ($r_summary[$i] as $r) {

                                            $desc = $r['description'];
                                            $desc_exp = explode(" - ", $desc);
                                            if (count($desc_exp) == 1) { $desc_val = $desc_exp[0]; }
                                            else { $desc_val = $desc_exp[count($desc_exp)-1]; }

                                            $comp = $r['computation'];
                                            $comp_exp = explode(" ", $comp);
                                            $comp_val = intval(strip_tags($comp_exp[0]));

                                            if ($desc_val == "Single") { $rooms=$comp_val; }
                                            else if ($desc_val == "Twin") { $rooms=($comp_val/2); }
                                            else if ($desc_val == "Double") { $rooms=($comp_val/2); }
                                            else if ($desc_val == "Triple") { $rooms=($comp_val/3); }
                                            else if ($desc_val == "Child Half Twin") { $rooms=($comp_val/2); }
                                            else if ($desc_val == "Child With Bed") { $rooms=$comp_val; }
                                            else if ($desc_val == "Child Without Bed") { $rooms=$comp_val; }

                                            if ($i_cnt >= 6) {
                                                $i_cnt=1;
                                                echo "</tr><tr>
                                                    <td style='padding: 8px; line-height: 1.428571429; vertical-align: top; border: 1px solid #dddddd;'>
                                                        <strong>{$desc}</strong><br>
                                                        ".round($rooms)."
                                                    </td>";
                                            }
                                            else {
                                                $i_cnt++;
                                                echo "<td style='padding: 8px; line-height: 1.428571429; vertical-align: top; border: 1px solid #dddddd;'>
                                                    <strong>{$desc}</strong><br>
                                                    ".round($rooms)."
                                                </td>";
                                            }
                                        }
                                        
                                        if ($i_data>6) {
                                            $i_cols = $i_data % 6;
                                            if ($i_cols != 0) {
                                                $i_mods = 6 - $i_cols;
                                                echo "<td colspan='{$i_mods}' style='padding: 8px; line-height: 1.428571429; vertical-align: top; border: 1px solid #dddddd;'>-</td>";
                                            }
                                        }
                                    echo '</tr></table>';
                                ?>
                    <?php
                            }
                        }
                    ?>



                    <center>
                        <a href="<?php echo $confirm.'/'.$recipient_id; ?>" style="color: #C64E0A; text-decoration:none;">
                            <button style="padding: 6px 12px; margin-bottom: 0; font-size: 14px; font-weight: normal; line-height: 1.428571429; white-space: nowrap; cursor: pointer; background-image: none; border: 1px solid transparent; border-radius: 4px; -webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; -o-user-select: none; user-select: none;color: #fff; background-color: #e95a3e; border-color: #e02908;">Confirm</button>
                        </a>
                        <a href="<?php echo $reject.'/'.$recipient_id; ?>" style="color: #C64E0A; text-decoration:none;">
                            <button style="padding: 6px 12px; margin-bottom: 0; font-size: 14px; font-weight: normal; line-height: 1.428571429; white-space: nowrap; cursor: pointer; background-image: none; border: 1px solid transparent; border-radius: 4px; -webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; -o-user-select: none; user-select: none;color: #fff; background-color: #e95a3e; border-color: #e02908;">Reject</button>
                         </a>
                         <a href="<?php echo $cancel.'/'.$recipient_id; ?>" style="color: #C64E0A; text-decoration:none;">
                            <button style="padding: 6px 12px; margin-bottom: 0; font-size: 14px; font-weight: normal; line-height: 1.428571429; white-space: nowrap; cursor: pointer; background-image: none; border: 1px solid transparent; border-radius: 4px; -webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; -o-user-select: none; user-select: none;color: #fff; background-color: #e95a3e; border-color: #e02908;">Cancel</button>
                         </a>
                    </center>

                </td>
            </tr>
        </table>
    </body>
</html>
