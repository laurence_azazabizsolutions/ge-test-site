<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript">

</script>
<div id="page-wrapper">
    <!-- /.row -->
    <div class="row tab-pane">
        <br>
        <div class="col-lg-12">
            <!-- <ul class="nav nav-pills dest_view hide_package_view_display_tab" role="tablist"> -->
            <ul class="nav nav-pills dest_view hide_package_view_display_tab" role="tablist">
                <?php
                    // if (isset($destinations )) {
                    //     $dest_act = false;
                    //     foreach ($destinations as $destination) {
                    //          if ($dest_act === false) {
                    //             echo '<li class="active"><a href="#'.$destination['code'].'" id="'.$destination['code'].'" onClick="change_pkg_view(\''.$destination['code'].'\',this)" role="tab" data-toggle="tab">'.$destination['country'].'</a></li>';
                    //             $dest_act = true;
                    //         } else { echo '<li><a href="#'.$destination['code'].'" id="'.$destination['code'].'" onClick="change_pkg_view(\''.$destination['code'].'\',this)" role="tab" data-toggle="tab">'.$destination['country'].'</a></li>'; }

                    //     }

                    // }

                    if (isset($destinations )) {
                        $dest_act = false;
                        foreach ($destinations as $destination) {
                             if ($this->uri->segment(3) === $destination['code']) {
                                echo '<li class="active"><a href="'.base_url('panel/view_packagess').'/'.$destination['code'].'" >'.$destination['country'].'</a></li>';
                                $dest_act = true;
                            } else { echo '<li><a href="'.base_url('panel/view_packagess').'/'.$destination['code'].'" >'.$destination['country'].'</a></li>'; }

                        }

                    }
                ?>
            </ul>
            <input type="hidden" value="<?php echo $destinations[0]['code']; ?>" id="first_code">

            <form class="form-horizontal hide_package_view_display_select" role="form" name="form">
                <div class="form-group">
                    <label class="col-sm-3 control-label text-right">Destination</label>
                    <div class="col-sm-8">
                        <select class="form-control"  onChange="changeCode(this.value)">
                            <option value=" ">- Select -</option>
                            <?php if (isset($destinations)): ?>
                                <?php foreach ($destinations as $destination): ?>
                                    <?php if ($this->uri->segment(3) === $destination['code']) { ?>
                                        <option value="<?php echo $destination['code']; ?>" selected><?php echo $destination['country']; ?></option>
                                    <?php 
                                        } 
                                        else
                                        {
                                    ?>
                                        <option value="<?php echo $destination['code']; ?>" ><?php echo $destination['country']; ?></option>
                                    <?php } ?>

                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>
            </form>
            
            <br>

            <div id="display">
                <?php if (isset($packages)): ?>
                    <?php foreach($packages as $package): ?>
                        <div class="col-md-4">
                            <a href="<?php echo base_url('panel/package_edit/'.$package->id)?>">
                                <div class="pax-img">
                                    <?php if($package->image_path!=''):?>
                                        <div class="pax-fill" style="background-image:url('<?php echo base_url($package->image_path) ?>');"></div>
                                    <?php else:?>
                                        <div class="pax-fill" style="background-image:url('<?php echo base_url('img/feat/default.png')?>');"></div>
                                    <?php endif; ?>
                                </div>
                                <p class="pax-details small lead"><?php echo $package->code;?>: <?php echo $package->title;?> (Effective from <?php echo $package->date_from;?> - <?php echo $package->date_to; ?>)</p>
                            </a>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <div class="row text-center">
        <?php echo $paging; ?>
    </div>
</div>
<!-- /#page-wrapper -->