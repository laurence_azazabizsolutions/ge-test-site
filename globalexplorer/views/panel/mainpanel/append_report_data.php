                        <?php if (isset($booked_data)): 
                         $counter=1;
                         $totalcost=0; 
                         $totalprofit=0; 
                         $totalprice=0;
                         ?>
                            <?php foreach ($booked_data as $row):
                                $totalcost+=$row->BOOKINGCOST;
                                $totalprofit+=$row->BOOKINGPROFIT;
                                $totalprice+=$row->BOOKINGPRICE;
                                if ($row->BOOKINGSTATUS == "cancelled") {
                                    $tr_color = "danger";
                                } else { $tr_color = ""; }
                            ?>
                                <tr class="<?php echo $tr_color;?>">
                                    <td class="hideCode">
                                        <a href="<?php echo base_url('panel/voucher/'.$row->BOOKID)?>" target="_blank"><?php echo $row->BOOKINGCODE;?></a> <br>
                                        <a href="<?php echo base_url('panel/package/'.$row->BOOKID)?>">View</a>
                                    </td>
                                    <td class="hideDetails">
                                        <div class="hidePackage"><strong>Package:</strong> <?php echo $row->PACKAGENAME; ?></div>
                                        <div class="hideAgent"><strong>Agent:</strong> <?php echo $row->AGENTFNAME; ?> <?php echo $row->AGENTLNAME; ?></div>
                                        <div class="hideProvider" style="display:none;"><strong>Provider:</strong> <?php echo $row->PROVIDERFNAME; ?> <?php echo $row->PROVIDERLNAME; ?></div>
                                        <div class="hideHotel"><strong>Hotel:</strong> 
                                            <?php 
                                            $tmp=array();
                                            foreach ($booked_hotel_data as $key){
                                                $tmp[]=$key->hotels_name; 
                                            }
                                            echo implode(' ,',$tmp);
                                            ?></div>
                                        <div class="hideTraveller"><strong>Traveller:</strong> <?php echo $row->TRAVELLLERNAME; ?></div>
                                    </td>
                                    <td class="hideFlight">
                                        <div class="hideOrigin">
                                            <strong>Ori:</strong> <?php echo $row->BOOKINGDEPARTUREDATE; ?><br>
                                            <i><?php echo $row->ARRIVALTIME; ?> hrs - <?php echo $row->RETURNTIME; ?> hrs</i>
                                        </div>
                                        <div class="hideReturn">
                                            <strong>Ret:</strong> <?php echo $row->BOOKINGRETURNDATE; ?><br>
                                            <i><?php echo $row->RETURNTIME; ?> hrs - <?php echo $row->ARRIVALTIME; ?> hrs</i>
                                        </div>
                                    </td>
                                    <td class="hideAmount hideCost" style="display:none;">
                                        <div><strong>Cost:</strong> <?php echo $row->BOOKINGCOST; ?></div>
                                     </td>
                                    <td class="hideAmount hideProfit" style="display:none;">
                                       <div><strong>Profit:</strong> <?php echo $row->BOOKINGPROFIT; ?></div>
                                     </td>
                                    <td class="hideAmount hidePrice">
                                        <div><strong>Price:</strong> <?php echo $row->BOOKINGPRICE; ?></div>
                                    </td>
                                    <td class="hidePayment">
                                        <div class="hideAgentPayment"><strong>Agent Paid:</strong> No</div>
                                        <div class="hideProviderPayment"><strong>Provider Paid:</strong> No</div>
                                    </td>
                                    <td class="hideStatus"><?php echo $row->BOOKINGSTATUS; ?></td>
                                </tr>
                            <?php
                             $counter++; 
                            endforeach; ?>
                        <?php endif; ?>
