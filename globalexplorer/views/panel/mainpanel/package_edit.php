<style type="text/css">
    select[readonly]{
        background: #eee;
        cursor:no-drop;
    }

    select[readonly] option{
        display:none;
    }
</style>

<div id="page-wrapper">
    <!-- /.row -->
    <div class="row">
        <br>
        <div class="col-lg-12">
            <!-- Nav tabs -->
            <ul class="nav nav-pills" role="tablist">
                <li class="active"><a href="#general" role="tab" data-toggle="tab">General</a></li>
                <li><a href="#hotel" role="tab" data-toggle="tab">Hotel</a></li>
                <li><a href="#optional" role="tab" data-toggle="tab">Optional Tours / Add Ons</a></li>
                <li><a href="#surchages" role="tab" data-toggle="tab">Package Surcharge</a></li>
                <li><a href="#permissions" role="tab" data-toggle="tab">Permissions</a></li>
            </ul>

            <!-- Tab Panes -->
            <div class="tab-content">
                <div class="tab-pane active" id="general">
                    <br>
                    <!-- <form id="update_package_form" class="form-horizontal" action="<?php// echo base_url('panel/update_package'); ?>" method="post" enctype="multipart/form-data"> -->
                    <?php //echo form_open_multipart('panel/update_package','id="update_package_form" class="form-horizontal",role="form",id = "update_package_form"');?>
                    <?php
                        $attributes = array('id' => 'update_package_form','class' => 'form-horizontal', 'role' => 'form');
                        echo form_open_multipart('panel/update_package',$attributes);
                    ?>

                        <input type="hidden" name="package_admin" value="<?php echo $user->getUser()->getId();?>">
                        <input type="hidden" name="package_id" value="<?php echo $package->getId();?>">

                        <?php if ($this->session->flashdata('alert_type')) { ?>
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <?php echo $this->session->flashdata('message'); ?>
                            </div>
                        <?php } ?>
                        <div id="appendFormDestination">
                            <?php
                                if(count($package->getDestinations())){
                                    $i=1;

                                    foreach($package->getDestinations() as $dest){
                                        $disabled = '';

                                        $hasHotel = false;
                                        if($dest->getStatus() == 'active'){
                                    ?>
                                        <div class="form-group destinationFormGroup" counter="<?php echo $dest->getId(); ?>" id="destinationFromGroup<?php echo $dest->getId();  ?>">
                                            <?php foreach($package_hotels as $hotel){
                                                if ($hotel['country_code'] == $dest->getCountryCode())
                                                    $hasHotel = true;
                                                if ($hasbooking || $hasHotel) $disabled = 'readonly';
                                            }

                                            if(!$hasHotel): ?>
                                                <div class="col-sm-3 control-label" style="padding-top:5px;">
                                                    <button onclick="removebutton(this)" counter="<?php echo $dest->getId(); ?>" class="btn btn-danger btn-xs" type="button"><span class="fa fa-minus"></span> Remove</button>
                                                </div>
                                            <?php else: ?>
                                                <label class="col-sm-3 control-label">
                                                   Destination #<?php echo $i; ?>
                                                </label>
                                            <?php endif; ?>
                                           <div class="col-sm-5">
                                            <select class="form-control p_dest" name="destination[]" <?php echo $disabled; ?>>
                                                <option value=" ">- Select -</option>
                                                <?php if (isset($countries)): ?>
                                                    <?php foreach ($countries as $country): ?>
                                                        <option value="<?php echo $country['code']; ?>" <?php if($country['code']==$dest->getCountryCode()): ?>selected="selected"<?php endif; ?> ><?php echo $country['country']; ?></option>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </select>
                                            </div>
                                            <div class="col-sm-3">
                                                <input type="text" class="form-control" name="number_of_nights[]" placeholder="Nights" onkeypress="return isNumberKey(event)" value="<?php echo $dest->getNights(); ?>" <?php echo $disabled; ?>>
                                            </div>
                                        </div>
                                    <?php
                                        $i++;
                                    }
                                }
                            }
                        ?>
                        </div>
                        <?php if($hasbooking===FALSE){?>
                            <div class="form-group">
                                <div class="col-sm-8 col-sm-offset-3">
                                    <button class="btn btn-success btn-xs" type="button" id="appendDestination"><span class="fa fa-plus"></span> Add New</button>
                                </div>
                            </div>
                        <?php } ?>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Provider</label>
                            <div class="col-sm-8">
                                <select class="form-control" name="package_provider">
                                    <option value=" ">- Select -</option>
                                    <?php if (isset($providers)): ?>
                                        <?php foreach ($providers as $provider): ?>
                                            <option value="<?php echo $provider->getUser()->getId();?>" <?php if($provider->getUser()->getId() == $package->getProvider()->getUser()->getId()):?>selected<?php endif;?>><?php echo $provider->getUser()->getCompany() . ' ('. $provider->getUser()->getFirstname() . ' '. $provider->getUser()->getlastname(). ')'; ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Picture</label>
                            <div class="col-sm-8">
                                <!-- <?php echo $package->getImagePath();?>     -->
                                <?php $data_form = array('name'=>'userfile');
                                    echo form_upload($data_form);
                                ?>
                                <?php
                                    $img = '';
                                    if($package->getImagePath()!=''):
                                    $img = $package->getImagePath();
                                ?>
                                    <img src="<?php echo base_url($package->getImagePath()); ?>" height="100px">
                                <?php endif; ?>
                                <input type="hidden" name="package_image" value="<?php echo $img; ?>">
                                <p class="help-block">Only JPEG/PNG files are allowed.</p>
                            </div>
                        </div>

                        <!-- <div class="form-group">
                            <label class="col-sm-3 control-label">Price</label>
                            <div class="col-sm-8">
                                <?php //$data_form = array('name'=>'priceeeeefile[]','type'=>'file','multiple'=>'');
                                    //echo form_upload($data_form);
                                ?>
                                <p class="help-block">Only EXCEL, CSV and PDF files are allowed.</p>
                            </div>
                        </div> -->

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Price</label>
                            <div class="col-sm-8">
                                <input type="file" name="fckshit[]" multiple="">
                                <?php
                                    $img = '';
                                    if($package_image_price[0]->image_price != ""){
                                        $cnt=explode(',',$package_image_price[0]->image_price);
                                        $img=count($cnt) .' files';
                                        foreach ($cnt as $key) {
                                            echo '<input type="hidden" name="old_price[]" multiple="" value="'.$old_price[]=$key.'">';
                                        }
                                    }
                                ?>
                                <span><?php echo $img; ?></span>
                                <p class="help-block">Only EXCEL, CSV and PDF files are allowed.</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Title</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="package_title" value="<?php echo $package->getTitle();?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Description</label>
                            <div class="col-sm-8">
                                <textarea name="package_description" class="form-control" style="resize:none;" rows="3" ><?php echo $package->getDescription();?></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Terms and Conditions</label>
                            <div class="col-sm-8">
                                <textarea class="form-control" style="resize:none;" rows="3" name="terms_and_conditions"><?php echo $package->getTermsAndConditions();?></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Validity Date</label>
                            <div class="col-sm-4">
                                <input id="datetimepickerValidDateFrom" class="form-control" type="text" placeholder="From" name="valid_date_from" data-date-format="YYYY-MM-DD" value="<?php echo $package->getDateFrom();?>" readonly/>
                                <p class="help-block">e.g. 1987-11-13</p>
                            </div>
                            <div class="col-sm-4">
                                <input id="datetimepickerValidDateUntil" class="form-control" type="text" placeholder="Until" name="valid_date_until" data-date-format="YYYY-MM-DD" value="<?php echo $package->getDateTo();?>" readonly/>
                                <p class="help-block">e.g. 1987-11-13</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-8 col-sm-offset-3">
                                <button type="submit" class="btn btn-orange">Save Packages</button>
                            </div>
                        </div>

                    </form>
                </div>
                <div class="tab-pane" id="hotel">
                    <br>
                    <form class="form-horizontal" role="form">
                        <input type="hidden" id="package_id" value="<?php echo $package->getId();?>">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Hotel Name</label>
                            <div class="col-sm-8">
                                <select class="form-control" name="filter_hotel_slct">
                                    <option value="">- Select -</option>
                                    <?php if (isset($package_hotels)): ?>
                                        <?php foreach ($package_hotels as $package_hotel): ?>
                                            <option value="<?php echo $package_hotel['package_hotel_id']; ?>"><?php echo $package_hotel['name']; ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-8 col-sm-offset-3">
                                <!-- <button type="button" class="btn btn-orange">Filter</button> -->
                                <button id="show_hotels_btn" type="reset" class="btn btn-default" data-target="#myModalHotel">Add New</button>
                            </div>
                        </div>
                    </form>

                    <div class="table-responsive">
                        <table class="table table-bordered" id="packagehotels_table">
                            <thead>
                                <tr>
                                    <th>Hotel Name</th>
                                    <th>Details</th>
                                </tr>
                            </thead>
                            <tbody id="fck">
                                <?php if(isset($package_hotels)): ?>                               
                                    <?php foreach ($package_hotels as $code => $package_hotel): ?>
                                        <tr packageHotelId="<?php echo $package_hotel['package_hotel_id']; ?>">
                                            <td>
                                                <?php echo strtoupper($package_hotel['name']); ?>
                                                <?php if(trim($package_hotel['location']))      echo "<br>".$package_hotel['location']; ?>
                                                <?php if(trim($package_hotel['country']))       echo "<br>".$package_hotel['country'];?>
                                                <?php if(trim($package_hotel['phone']))         echo "<br>".$package_hotel['phone'];?>
                                                <?php if(trim($package_hotel['email']))         echo "<br>".$package_hotel['email'];?>
                                                <?php if(trim($package_hotel['website']))       echo "<br>".$package_hotel['website'];?>
                                                <?php if(trim($package_hotel['description']))   echo "<br>".$package_hotel['description'];?>
                                                <br>
                                                <a href="javascript:void(0);" onclick="removePackageHotel(this);" package_hotel_id="<?php  echo $package_hotel['package_hotel_id']; ?>" package_id="<?php echo $package_id; ?>" hotel_id="<?php echo $package_hotel["hotel_id"] ?>">Delete</a>
                                            </td>
                                            <td>
                                                Room Rates:
                                                <a href="#" class="package_hotel_add" data-target="#myModalRoomRate">Add</a>&nbsp; | &nbsp;<a href="<?php echo base_url('panel/package_batch_add').'/'.$package_hotel['package_hotel_id'];?>" class="package_batch_add">Batch Add</a><!-- &nbsp; | &nbsp;<a href="<?php //echo base_url('panel/package_batch_edit').'/'.$package_hotel['package_hotel_id'];?>" class="package_batch_edit">Batch Edit</a> --><br>
                                                <!-- <a href="#" class="package_hotel_add" data-target="#myModalRoomRate">Add</a>&nbsp; | &nbsp;<a href="<?php echo base_url('panel/package_batch_edit').'/'.$package_hotel['package_hotel_id'];?>" class="package_batch_edit">Batch Edit</a> <br> -->
                                                <table id="roomrate_table" class="table table-striped table-bordered table-hover">
                                                    
                                                    <?php if(isset($package_hotel['roomrates'])):
                                                        $pids=array();
                                                        $new=array();
                                                        $count=0;
                                                        foreach ($package_hotel['roomrates'] as $roomrate){
                                                            $pids[] = $roomrate['date_from'].'/'.$roomrate['date_to'];   
                                                        }
                                                        $uniquePids = array_unique($pids);
                                                        foreach($uniquePids as $id){
                                                            $new[$count]=$id;
                                                            $count++;
                                                        }
                                                        //dump($new);
                                                        for($a=0;$a<count($new);$a++){
                                                            if($new[$a]=='0000-00-00/0000-00-00'){ ?>
                                                                    <tr>
                                                                        <th colspan="9">Default &nbsp; | &nbsp;<a href="<?php echo base_url('panel/package_batch_copy').'/'.$package_hotel['package_hotel_id'];?>" class="package_batch_copy">Batch Copy</a> &nbsp; | &nbsp;<a href="<?php echo base_url('panel/package_batch_edit').'/'.$package_hotel['package_hotel_id'];?>" class="package_batch_edit" id="<?php echo $new[$a]?>">Batch Edit</a></th>
                                                                    </tr>
                                                                    <tr>
                                                                        <th></th>
                                                                        <th>Description</th>
                                                                        <th>Cost</th>
                                                                        <th>Profit</th>
                                                                        <th>Price</th>
                                                                        <th>Ext. Cost</th>
                                                                        <th>Ext. Profit</th>
                                                                        <th>Ext. Price</th>
                                                                        <th>Rate Type</th>
                                                                    </tr>
                                                            <?php }else{ ?>
                                                                    <tr>
                                                                        <th colspan="9"><?php 
                                                                        $dateArr = explode("/",$new[$a]);
                                                                        echo date('d-M-Y',strtotime($dateArr[0])).' - '.date('d-M-Y',strtotime($dateArr[1]))?>&nbsp; | &nbsp;<a href="<?php echo base_url('panel/package_batch_edit').'/'.$package_hotel['package_hotel_id'];?>" class="package_batch_edit" id="<?php echo $new[$a]?>">Batch Edit</a></form> 
                                                                        </th>
                                                                    </tr>
                                                                    <tr>
                                                                        <th></th>
                                                                        <th>Description</th>
                                                                        <th>Cost</th>
                                                                        <th>Profit</th>
                                                                        <th>Price</th>
                                                                        <th>Ext. Cost</th>
                                                                        <th>Ext. Profit</th>
                                                                        <th>Ext. Price</th>
                                                                        <th>Rate Type</th>
                                                                    </tr>
                                                            <?php }
                                                            foreach ($package_hotel['roomrates'] as $code => $roomrate): 
                                                                if($new[$a]==$roomrate['date_from'].'/'.$roomrate['date_to']){ ?>
                                                                    <tr roomrateId="<?php echo $roomrate['id'];?>">
                                                                        <td>
                                                                            <a href="javascript:void(0);" class="edit_roomrate_link" data-target="#myModalRoomRate">Edit</a> |
                                                                            <a href="javascript:void(0);" onclick="removePackageRoomRate(this);" roomrate-id="<?php echo $roomrate["id"]; ?>">Delete</a> |
                                                                            <a href="javascript:void(0);" class="package_hotel_add" data-target="#myModalRoomRateSurcharge">Add Surcharge</a>
                                                                        </td>
                                                                        <td>
                                                                            <?php
                                                                                if(trim($roomrate['description'])!='')
                                                                                    echo ucfirst($roomrate['description']).' - ';
                                                                                echo ucwords(str_replace('_', ' ', $roomrate['room_type']));
                                                                            ?>
                                                                        </td>
                                                                        <?php
                                                                            $cost       = (trim($roomrate['cost'])!='' || $roomrate['cost']!=NULL)              ? $roomrate['cost'] : 0.00;
                                                                            $profit     = (trim($roomrate['profit'])!='' || $roomrate['profit']!=NULL)          ? $roomrate['profit'] : 0.00;
                                                                            $ext_cost   = (trim($roomrate['ext_cost'])!='' || $roomrate['ext_cost']!=NULL)      ? $roomrate['ext_cost'] : 0.00;
                                                                            $ext_profit = (trim($roomrate['ext_profit'])!='' || $roomrate['ext_profit']!=NULL)  ? $roomrate['ext_profit'] : 0.00;
                                                                        ?>

                                                                        <td><?php echo $cost; ?></td>
                                                                        <td><?php echo $profit; ?></td>
                                                                        <td><?php echo ($profit+$cost); ?></td>
                                                                        <td><?php echo $ext_cost; ?></td>
                                                                        <td><?php echo $ext_profit; ?></td>
                                                                        <td><?php echo ($ext_cost+$ext_profit); ?></td>
                                                                        <td><?php if(trim($roomrate['rate_type']))  echo ucwords(str_replace('/', ' / ', $roomrate['rate_type'])); ?></td>
                                                                    </tr>
                                                               <?php }
                                                            endforeach;
                                                        }
                                                    ?>
                                                        
                                                    <?php endif;?>
                                                </table>
                                                <br>

                                                Room Rate Surcharges:
                                                <!-- <a href="#" class="package_hotel_add" data-target="#myModalRoomRateSurcharge">Add</a> <br> -->
                                                <table id="roomsurcharge_table" class="table table-striped table-bordered table-hover">
                                                    <tr>
                                                        <th></th>
                                                        <th>Description</th>
                                                        <th>Rule</th>
                                                        <th>Cost</th>
                                                        <th>Profit</th>
                                                        <th>Price</th>
                                                        <th>Rate Type</th>
                                                    </tr>
                                                    <?php if(isset($package_hotel['roomsurcharges'])):?>
                                                        <?php foreach ($package_hotel['roomsurcharges'] as $code => $roomsurcharge): ?>
                                                            <tr roomsurchargeId="<?php echo $roomsurcharge['id'];?>">
                                                                <td>
                                                                    <a href="javascript:void(0);" class="edit_roomrate_link" data-target="#myModalRoomRateSurcharge">Edit</a> |
                                                                    <a href="javascript:void(0);" onclick="removeSurchargeHotelRooms(this);" surcharge-id="<?php echo $roomsurcharge["id"]; ?>">Delete</a>
                                                                </td>
                                                                <?php
                                                                    $cost       = (trim($roomsurcharge['cost'])!='' || $roomsurcharge['cost']!=NULL)        ? $roomsurcharge['cost'] : 0.00;
                                                                    $profit     = (trim($roomsurcharge['profit'])!='' || $roomsurcharge['profit']!=NULL)    ? $roomsurcharge['profit'] : 0.00;
                                                                ?>

                                                                <td><?php echo $roomsurcharge['description']; ?></td>
                                                                <td><?php echo ucwords(str_replace('_', ' ', $roomsurcharge['rule_type'])); ?> : <?php echo $roomsurcharge['rule']; ?></td>
                                                                <td><?php echo $cost ; ?></td>
                                                                <td><?php echo $profit; ?></td>
                                                                <td><?php echo ($profit + $cost); ?></td>
                                                                <td><?php echo ucwords(str_replace('/', ' / ', $roomsurcharge['rate_type'])); ?></td>
                                                            </tr>
                                                        <?php endforeach;?>
                                                    <?php endif;?>
                                                </table>
                                            </td>
                                        </tr>
                                    <?php endforeach;?>
                                <?php endif;?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane" id="optional">
                    <br>
                    <div class="table-responsive">
                        <table id="addons_table" class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>
                                        <a href="#" data-toggle="modal" data-target="#myModalOptional">Add New</a>
                                    </th>
                                    <th>Description</th>
                                    <th>Unit</th>
                                    <th>Cost</th>
                                    <th>Profit</th>
                                    <th>Price</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(isset($addons)):?>
                                <?php foreach ($addons as $code => $addon): ?>
                                    <tr addonid="<?php echo $addon['id']; ?>">
                                        <td>
                                            <a href="javascript:void(0);" class="edit_addon_link" data-target="#myModalOptional">Edit</a> |
                                            <a href="javascript:void(0);" onclick="removeGeneralElement(this)" element-container= "[addonid='<?php echo $addon['id']; ?>']"  element-table="addons" element-message="Do you really want to remove this addon?" element-id="<?php echo $addon["id"]; ?>">Delete</a>
                                        </td>
                                        <?php
                                            $cost       = (trim($addon['cost'])!='' || $addon['cost']!=NULL)    ? $addon['cost'] : 0.00;
                                            $profit     = (trim($addon['profit'])!='' || $addon['profit']!=NULL)? $addon['profit'] : 0.00;
                                        ?>
                                        <td><?php echo $addon['description']; ?></td>
                                        <td><?php echo $addon['unit']; ?></td>
                                        <td><?php echo $cost; ?></td>
                                        <td><?php echo $profit; ?></td>
                                        <td><?php echo ($cost + $profit); ?></td>
                                    </tr>
                                <?php endforeach;?>
                                <?php endif;?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane" id="surchages">
                    <br>
                    <div class="table-responsive">
                        <table id="surcharge_table" class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>
                                        <a href="#" data-toggle="modal" data-target="#myModalSurcharge">Add New</a>
                                    </th>
                                    <th>Description</th>
                                    <th>Rule</th>
                                    <th>Cost</th>
                                    <th>Profit</th>
                                    <th>Price</th>
                                    <th>Rate Type</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if(isset($surcharges)):?>
                                    <?php foreach ($surcharges as $code => $surcharge): ?>
                                        <tr packageSurchargeId="<?php echo $surcharge['id']; ?>">
                                            <td>
                                                <a href="javascript:void(0);" class="edit_packagesurcharge_link" data-target="#myModalSurcharge">Edit</a> |
                                                <a href="javascript:void(0);" onclick="removeGeneralElement(this)" element-container= "[packageSurchargeId='<?php echo $surcharge['id']; ?>']"  element-table="package_surcharges" element-message="Do you really want to remove this package surcharge?" element-id="<?php echo $surcharge["id"]; ?>" >Delete</a>
                                            </td>
                                            <?php
                                                $cost       = (trim($surcharge['cost'])!='' || $surcharge['cost']!=NULL)    ? $surcharge['cost'] : 0.00;
                                                $profit     = (trim($surcharge['profit'])!='' || $surcharge['profit']!=NULL)? $surcharge['profit'] : 0.00;
                                            ?>
                                            <td><?php echo $surcharge['description']; ?></td>
                                            <td><?php echo ucwords(str_replace('_', ' ', $surcharge['rule_type'])); ?> : <?php echo $surcharge['rule']; ?></td>
                                            <td><?php echo $cost; ?></td>
                                            <td><?php echo $profit; ?></td>
                                            <td><?php echo ($cost + $profit); ?></td>
                                            <td><?php echo ucwords(str_replace('/', ' / ', $surcharge['rate_type'])); ?></td>
                                        </tr>
                                    <?php endforeach;?>
                                <?php endif;?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="tab-pane" id="permissions">
                    <br>
                    <div class="table-responsive">
                        <input type="hidden" name="package_hotel_id" value="<?php echo $package->getId();?>">
                        <div class="alert alert-danger" style="display: none;"></div>
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th width="50" class="text-center">
                                        <input id="permitAll" type="checkbox">
                                    </th>
                                    <th>Agents</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if (isset($agents)): ?>
                                    <?php foreach ($agents as $agent): ?>
                                        <tr>
                                            <td class="text-center"><input value="<?php echo $agent->getUser()->getId();?>" class="give_permission" type="checkbox" <?php if($agent->getUser()->isPermitted): ?>checked<?php endif; ?>></td>
                                            <td><?php echo $agent->getUser()->getLastname() . ', '. $agent->getUser()->getFirstname(); ?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <!-- myModalHotel -->
            <div class="modal fade" id="myModalHotel"  onsumbit="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="pull-right btn btn-danger btn-sm" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="myModalLabel">Select Hotel:</h4>
                        </div>
                        <form id="addpackagehotels_form" class="form-horizontal" role="form" action="" method="post"><!-- <?php //echo base_url('panel/add_packagehotels')?> -->
                            <input type="hidden" name="package_id_hotel" value="<?php echo $package->getId();?>">
                            <!-- <input type="hidden" name="package_hotel_id" value="<?php //echo $package_hotel['package_hotel_id']; ?>">
                            <input type="hidden" name="package_id" value="<?php //echo $package_id; ?>">
                            <input type="hidden" name="package_hotel" value="<?php //echo $package_hotel["hotel_id"] ?>"> -->
                            <div class="modal-body">
                                <div class="alert alert-danger" style="display: none;"></div>
                                <div class="form-group">
                                    <label class="col-xs-4 control-label">Hotel Name</label>
                                    <div class="col-xs-8">
                                        <select class="form-control" name="hotel_choice">
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button id="addpackagehotels_button" type="button" class="btn btn-orange" role="button">Add</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- myModalRoomRate -->
            <div class="modal fade" id="myModalRoomRate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="pull-right btn btn-danger btn-sm" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="myModalLabel">Hotel Room Rate:</h4>
                        </div>
                        <form id="addhotelroomrate_form" class="form-horizontal" role="form" action="<?php echo base_url('panel/add_hotelroomrate')?>" method="post">
                            <div class="alert alert-danger" style="display: none;"></div>
                            <input type="hidden" name="parent_id" value="">
                            <input type="hidden" name="act_type" value="add">
                            <div class="modal-body">
                                <div class="form-group">
                                    <label class="col-xs-4 control-label">Rate Name</label>
                                    <div class="col-xs-8">
                                        <select class="form-control" name="hotelroomrate_name">
                                            <option value='single'>Single</option>
                                            <option value='twin'>Twin</option>
                                            <option value='triple'>Triple</option>
                                            <option value='quadruple'>Quadruple</option>
                                            <option value='child_half_twin'>Child Half Twin</option>
                                            <option value='child_with_bed'>Child With Bed</option>
                                            <option value='child_without_bed'>Child Without Bed</option>
                                            <option value='extra_bed'>Extra Bed</option>
                                            <option value='breakfast'>Breakfast</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-4 control-label">Rate Type</label>
                                    <div class="col-xs-8">
                                        <select class="form-control" name="hotelroomrate_type">
                                            <option value='pax/night'>Pax / Night</option>
                                            <option value='adult/night'>Adult / Night</option>
                                            <option value='child/night'>Child / Night</option>
                                            <option value='room/night'>Room / Night</option>
                                            <option value='pax/trip'>Pax / Trip</option>
                                            <option value='adult/trip'>Adult / Trip</option>
                                            <option value='child/trip'>Child / Trip</option>
                                            <option value='room/trip'>Room / Trip</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-4 control-label">Description</label>
                                    <div class="col-xs-8">
                                        <input type="text" class="form-control" name="hotelroomrate_description">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-4 control-label">Cost</label>
                                    <div class="col-xs-4">
                                        <input type="number" step="any" class="form-control" name="hotelroomrate_cost" placeholder="Default" onkeypress="return isNumberKey(event)">
                                    </div>
                                    <div class="col-xs-4">
                                        <input type="number" step="any" class="form-control" name="hotelroomrate_cost_ext" placeholder="Extension" onkeypress="return isNumberKey(event)">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-4 control-label">Profit</label>
                                    <div class="col-xs-4">
                                        <input type="number" step="any" class="form-control" name="hotelroomrate_profit" placeholder="Default" onkeypress="return isNumberKey(event)">
                                    </div>
                                    <div class="col-xs-4">
                                        <input type="number" step="any" class="form-control" name="hotelroomrate_profit_ext" placeholder="Extension" onkeypress="return isNumberKey(event)">
                                    </div>
                                </div>
<<<<<<< HEAD
                                <!-- <div class="form-group" id="date_range" style="display:none">
=======
                                <div class="form-group" id="date_range" style="display:none">
>>>>>>> 24acdcc0a51bfe964f80486c4f9ef0e155cd1e85
                                    <label class="col-xs-4 control-label">Date Range</label>
                                    <div class="col-xs-4">
                                        <input type="text" placeholder="From" class="col-xs-4 pickadate_default form-control" id="range_from2" name="hotelroomrate_date_from" style="padding-right:0px;">
                                    </div>
                                    <div class="col-xs-4">
                                        <input type="text" placeholder="To" class="col-xs-4 pickadate_default form-control" id="range_to2" name="hotelroomrate_date_to" style="padding-right:0px;">
                                    </div>
                                </div> -->
                                <!-- <div class="form-group">
                                    <label class="col-xs-4 control-label">Pax</label>
                                    <div class="col-xs-8">
                                        <input type="number" class="form-control" name="hotelroomrate_pax" onkeypress="return isNumberKey(event)">
                                    </div>
                                </div> -->
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-orange" type="submit" role="button">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- myModalRoomRateSurcharge -->
            <div class="modal fade" id="myModalRoomRateSurcharge" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="pull-right btn btn-danger btn-sm" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="myModalLabel">Hotel Room Surcharge:</h4>
                        </div>
                        <form id="add_hotelroomsurcharge_form" class="form-horizontal" role="form" action="<?php echo base_url('panel/add_hotelroomsurcharge')?>" method="post">
                            <div class="alert alert-danger" style="display: none;"></div>
                            <input type="hidden" name="parent_id" value="">
                            <input type="hidden" name="act_type" value="add">
                            <div class="modal-body">
                                <div class="form-group">
                                    <label class="col-xs-4 control-label">Description</label>
                                    <div class="col-xs-8">
                                        <input type="text" class="form-control" name="roomratesurcharge_description">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-4 control-label">Rule Type</label>
                                    <div class="col-xs-8">
                                        <select name="roomratesurcharge_ruletype" class="form-control" id="selRuleType" onchange="ruleType()">
                                            <option value="date_range">Date Range</option>
                                            <option value="day_of_week">Day of Week</option>
                                            <option value="agent">Agent</option>
                                            <option value="blackout">Blackout</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-4 control-label">Rule</label>

                                    <!-- Date_Range -->
                                    <div class="col-xs-4" id="date_range_1">
                                        <input type="text" class="col-xs-4 pickadate_default form-control" id="range_from1" name="roomratesurcharge_rule_range_1" style="padding-right:0px;">
                                    </div>
                                    <div class="col-xs-4" id="date_range_2">
                                        <input type="text" class="col-xs-4 pickadate_default form-control" id="range_to1" name="roomratesurcharge_rule_range_2" style="padding-right:0px;">
                                    </div>

                                    <!-- Day_of_Week -->
                                    <div class="col-xs-8" id="day_of_week" style="display:none;">
                                        <select class="form-control" name="roomratesurcharge_rule_day">
                                            <option value="Monday">Monday</option>
                                            <option value="Tuesday">Tuesday</option>
                                            <option value="Wednesday">Wednesday</option>
                                            <option value="Thursday">Thursday</option>
                                            <option value="Friday">Friday</option>
                                            <option value="Saturday">Saturday</option>
                                            <option value="Sunday">Sunday</option>
                                        </select>
                                    </div>

                                    <!-- Agent -->
                                    <div class="col-xs-8" id="agent" style="display:none;">
                                        <select class="form-control" name="roomratesurcharge_rule_agent">
                                            <option value=" ">- Select -</option>
                                            <?php if (isset($agents)): ?>
                                                <?php foreach ($agents as $agent): ?>
                                                    <option value="<?php echo $agent->getUser()->getId();?>"><?php echo $agent->getUser()->getLastname() . ', '. $agent->getUser()->getFirstname(); ?></option>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </select>
                                    </div>

                                    <!-- Blackout -->
                                    <div class="col-xs-4" id="blackout_1" style="display:none;">
                                        <input type="text" class="col-xs-4 pickadate_default form-control" id="blackout_from1" name="roomratesurcharge_rule_out_1" style="padding-right:0px;">
                                    </div>
                                    <div class="col-xs-4" id="blackout_2" style="display:none;">
                                        <input type="text" class="col-xs-4 pickadate_default form-control" id="blackout_to1" name="roomratesurcharge_rule_out_2" style="padding-right:0px;">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-4 control-label">Cost</label>
                                    <div class="col-xs-8">
                                        <input type="number" step="any" class="form-control" name="roomratesurcharge_cost" onkeypress="return isNumberKey(event)">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-4 control-label">Profit</label>
                                    <div class="col-xs-8">
                                        <input type="number" step="any" class="form-control" name="roomratesurcharge_profit" onkeypress="return isNumberKey(event)">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-4 control-label">Rate Type</label>
                                    <div class="col-xs-8">
                                        <select class="form-control" name="roomratesurcharge_ratetype">
                                            <option value='pax/night'>Pax / Night</option>
                                            <option value='adult/night'>Adult / Night</option>
                                            <option value='child/night'>Child / Night</option>
                                            <option value='room/night'>Room / Night</option>
                                            <option value='pax/trip'>Pax / Trip</option>
                                            <option value='adult/trip'>Adult / Trip</option>
                                            <option value='child/trip'>Child / Trip</option>
                                            <option value='room/trip'>Room / Trip</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-orange">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- myModalOptional -->
            <div class="modal fade" id="myModalOptional" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="pull-right btn btn-danger btn-sm" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="myModalLabel">Optional Tour / Add On:</h4>
                        </div>
                        <form id="addaddon_form" class="form-horizontal" role="form" action="<?php echo base_url('panel/add_packageaddons')?>" method="post">
                            <div class="alert alert-danger" style="display: none;"></div>
                            <input type="hidden" name="package_id" value="<?php echo $package->getId();?>">
                            <input type="hidden" name="addon_id" value="">
                            <input type="hidden" name="act_type" value="add">
                            <div class="modal-body">
                                <div class="form-group">
                                    <label class="col-xs-4 control-label">Description</label>
                                    <div class="col-xs-8">
                                        <textarea class="form-control" style="resize:none;" rows="3" name="addon_description"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-4 control-label">Unit</label>
                                    <div class="col-xs-8">
                                        <input type="text" class="form-control" name="addon_unit">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-4 control-label">Cost</label>
                                    <div class="col-xs-8">
                                        <input type="number" step="any" class="form-control" name="addon_cost" onkeypress="return isNumberKey(event)">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-4 control-label">Profit</label>
                                    <div class="col-xs-8">
                                        <input type="number" step="any" class="form-control" name="addon_profit" onkeypress="return isNumberKey(event)">
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-orange" type="submit" role="button">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- myModalSurcharge -->
            <div class="modal fade" id="myModalSurcharge" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="pull-right btn btn-danger btn-sm" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="myModalLabel">Package Surcharge:</h4>
                        </div>
                        <form id="create_packagesurcharge_form" class="form-horizontal" method="post" action="<?php echo base_url('panel/create_packagesurcharge')?>" role="form">
                            <input type="hidden" name="package_id" value="<?php echo $package->getId();?>">
                            <input type="hidden" name="pckgsrchrg_id" value="">
                            <input type="hidden" name="act_type" value="add">
                            <div class="modal-body">
                                <div class="form-group">
                                    <label class="col-xs-4 control-label">Description</label>
                                    <div class="col-xs-8">
                                        <textarea class="form-control" style="resize:none;" rows="3" name="packagesurcharge_description"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-4 control-label">Rule Type</label>
                                    <div class="col-xs-8">
                                        <select name="packagesurcharge_ruletype" class="form-control" id="selRuleType2" onchange="ruleType2()">
                                            <option value="date_range">Date Range</option>
                                            <option value="day_of_week">Day of Week</option>
                                            <option value="agent">Agent</option>
                                            <option value="blackout">Blackout</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-4 control-label">Rule</label>

                                    <!-- Date_Range -->
                                    <div class="col-xs-4" id="date_range2_1">
                                        <input type="text" class="col-xs-4 pickadate_default form-control" id="range_from2" name="packagesurcharge_rule_range_1" style="padding-right:0px;">
                                    </div>
                                    <div class="col-xs-4" id="date_range2_2">
                                        <input type="text" class="col-xs-4 pickadate_default form-control" id="range_to2" name="packagesurcharge_rule_range_2" style="padding-right:0px;">
                                    </div>

                                    <!-- Day_of_Week -->
                                    <div class="col-xs-8" id="day_of_week2" style="display:none;">
                                        <select class="form-control" name="packagesurcharge_rule_day">
                                            <option value="Monday">Monday</option>
                                            <option value="Tuesday">Tuesday</option>
                                            <option value="Wednesday">Wednesday</option>
                                            <option value="Thursday">Thursday</option>
                                            <option value="Friday">Friday</option>
                                            <option value="Saturday">Saturday</option>
                                            <option value="Sunday">Sunday</option>
                                        </select>
                                    </div>

                                    <!-- Agent -->
                                    <div class="col-xs-8" id="agent2" style="display:none;">
                                        <select class="form-control" name="packagesurcharge_rule_agent">
                                            <option value=" ">- Select -</option>
                                            <?php if (isset($agents)): ?>
                                                <?php foreach ($agents as $agent): ?>
                                                    <option value="<?php echo $agent->getUser()->getId();?>"><?php echo $agent->getUser()->getLastname() . ', '. $agent->getUser()->getFirstname(); ?></option>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </select>
                                    </div>

                                    <!-- Blackout -->
                                    <div class="col-xs-4" id="blackout2_1" style="display:none;">
                                        <input type="text" class="col-xs-4 pickadate_default form-control" id="blackout_from2" name="packagesurcharge_rule_out_1" style="padding-right:0px;">
                                    </div>
                                    <div class="col-xs-4" id="blackout2_2" style="display:none;">
                                        <input type="text" class="col-xs-4 pickadate_default form-control" id="blackout_to2" name="packagesurcharge_rule_out_2" style="padding-right:0px;">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-4 control-label">Cost</label>
                                    <div class="col-xs-8">
                                        <input type="number" step="any" class="form-control" name="packagesurcharge_cost" onkeypress="return isNumberKey(event)">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-4 control-label">Profit</label>
                                    <div class="col-xs-8">
                                        <input type="number" step="any" class="form-control" name="packagesurcharge_profit" onkeypress="return isNumberKey(event)">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-4 control-label">Rate Type</label>
                                    <div class="col-xs-8">
                                        <select class="form-control" name="packagesurcharge_ratetype">
                                            <option value='pax/night'>Pax / Night</option>
                                            <option value='adult/night'>Adult / Night</option>
                                            <option value='child/night'>Child / Night</option>
                                            <option value='room/night'>Room / Night</option>
                                            <option value='pax/trip'>Pax / Trip</option>
                                            <option value='adult/trip'>Adult / Trip</option>
                                            <option value='child/trip'>Child / Trip</option>
                                            <option value='room/trip'>Room / Trip</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-orange" id="package_edit_BTN" role="button">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /#page-wrapper -->