<!DOCTYPE html>
<html>
    <head>
        <style>
            @page { margin: 1.5in 1in 1in 1in; }
            @thead { position: fixed; left: 0px; top: 0px; right: 0px; text-align: center; }
        
            body { font: 62.5% "Trebuchet MS", sans-serif; font-size: 12px; text-align: left; position: relative; margin-bottom: auto; margin-left: auto; margin-right: auto; }
            #header { position: fixed; left: -50px; top: -100px; right: -50px; height: 80px; text-align: center; }
            #footer { position: fixed; left: -50px; bottom: -100px; right: -50px; height: 50px; font-size:8pt; background-color: gray; text-align: center; color:#d3e0ca; }
            #footer .page:after { content: counter(page); }
            .title { border: 0px solid black; font-size: 26pt; font-family: "Lucida Console"; }
            tr.even, th { background-color:#d3e0ca; }
    }
        </style>
    </head>
    <body>
    <!-- Content 
        ================================================== -->
        <div class="container" style="border:0px solid red; max-width:730px;">
            <br>
            <div class="row">
                
                <h1>Price List</h1>
                
            </div>

            <hr class="featurette-divider">
            <div class="table-responsive">
                <table class="table" align="center" border="1">
                    <thead>
                        <tr>
                            <th>Package Title</th>
                            <th>Hotel Name</th>
                            <th>Hotel Room Type</th>
                            <th>Hotel Description</th>
                            <th>Hotel Rate Type</th>
                            <th>Hotel Cost</th>
                            <th>Hotel Extended Cost</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (isset($room_summary)): 
                        $counter=1;
                        ?>
                        <?php foreach ($room_summary as $row):?>
                            <tr>
                                <td>

                                    <?php

                                    if($counter == 1){
                                        echo $row->packagestitle;
                                    }else{
                                        echo "";
                                    }
                                    ?>

                                </td>
                                <td>
                                    <?php 
                                    if($counter == 1){
                                        echo $row->hotelname;
                                    }else{
                                        echo "";
                                    }

                                    ?>
                                </td>
                                <td>
                                    <?php echo $row->hotelroomtype;?>
                                </td>
                                <td>
                                    <?php echo $row->hoteldescription;?>
                                </td>
                                <td>
                                    <?php echo $row->hotelratetype;?>
                                </td>
                                <td>
                                    <?php echo $row->hotelcost;?>
                                </td>
                                <td>
                                    <?php echo $row->hotelext_cost;?>
                                </td>

                            </tr>
                         <?php
                         $counter++;
                          endforeach; ?>
                         <?php endif; ?>
                    </tbody>
                </table>
            </div>
            
        </div>
        <!-- /details -->
    </body>
</html>

