<tr addonid="<?php echo $addon['id']; ?>">
    <td>
        <a href="#" class="edit_addon_link" data-target="#myModalOptional">Edit</a> | 
        <a href="#">Delete</a>
    </td>
    <?php 
        $cost   = (trim($addon['cost'])!='' || $addon['cost']!=NULL)    ? $addon['cost'] : 0.00;
        $profit = (trim($addon['profit'])!='' || $addon['profit']!=NULL)? $addon['profit'] : 0.00;
    ?>
    <td><?php echo $addon['description']; ?></td>
    <td><?php echo $addon['unit']; ?></td>
    <td><?php echo $cost; ?></td>
    <td><?php echo $profit; ?></td>
    <td><?php echo ($cost + $profit); ?></td>
</tr>