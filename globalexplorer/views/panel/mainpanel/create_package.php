<div id="page-wrapper">
    <!-- /.row -->
    <div class="row">
        <br>
        <div class="col-lg-12"><!-- 'id' => 'create_package_form', -->
            <?php
                $attributes = array('id' => 'create_package_form','class' => 'form-horizontal', 'role' => 'form');
                echo form_open_multipart('panel/register_package',$attributes);
            ?>
                <div class="alert alert-danger" style="display: none;"></div>
                <input type="hidden" class="form-control" name="package_admin" value="<?php echo $user->getUser()->getId();?>">
                <!--div class="form-group">
                    <label class="col-sm-3 control-label">Code</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="package_code">
                    </div>
                </div-->
                    
                
                <div class="form-group destinationFormGroup">
                    <label class="col-sm-3 control-label">Destination</label>
                    <div class="col-sm-5">
                        <select class="form-control" name="destination[]">
                            <option value=" ">- Select -</option>
                            <?php if (isset($countries)): ?>
                                <?php foreach ($countries as $country): ?>
                                    <option value="<?php echo $country['code']; ?>"><?php echo $country['country']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="number_of_nights[]" placeholder="Night(s)" onkeypress="return isNumberKey(event)">
                    </div>
                </div>
                

                <!-- append destinations here -->
                <div id="appendFormDestination">
                </div>
                <!-- end of append destinations here -->
                

                <!-- add new destination -->
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-3">
                        <button class="btn btn-success btn-xs" type="button" id="appendDestination"><span class="fa fa-plus"></span> Add New</button>
                    </div>
                </div>
                <!-- end of add new destination -->
                <div class="form-group">
                    <label class="col-sm-3 control-label">Provider</label>
                    <div class="col-sm-8">
                        <select class="form-control" name="package_provider">
                            <option value=" ">- Select -</option>
                            <?php if (isset($providers)): ?>
                                <?php foreach ($providers as $provider): ?>
                                    <option value="<?php echo $provider->getUser()->getId(); ?>" ><?php echo $provider->getUser()->getCompany() . ' ('. $provider->getUser()->getFirstname() . ' '. $provider->getUser()->getLastname() . ')'; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Picture</label>
                    <div class="col-sm-8">
                        <?php $data_form = array('name'=>'userfile');
                            echo form_upload($data_form);
                        ?>
                        <p class="help-block">Only JPEG/PNG files are allowed 2MB.</p>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Price</label>
                    <div class="col-sm-8">
                        <?php $data_form = array('name'=>'file[]','type'=>'file','multiple'=>'');
                            echo form_upload($data_form);
                        ?>
                        <p class="help-block">Only EXCEL, CSV and PDF files are allowed 4MB.</p>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Title</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="package_title">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Description</label>
                    <div class="col-sm-8">
                        <textarea name="package_description" class="form-control" style="resize:none;" rows="3"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Terms and Conditions</label>
                    <div class="col-sm-8">
                        <textarea class="form-control" style="resize:none;" rows="3" name="terms_and_conditions"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Validity Date</label>
                    <div class="col-sm-4">
                        <input id="datetimepickerValidDateFrom" class="form-control" type="text" placeholder="From" name="valid_date_from" data-date-format="YYYY-MM-DD" readonly/>
                        <p class="help-block">e.g. 1987-11-13</p>
                    </div>
                    <div class="col-sm-4">
                        <input id="datetimepickerValidDateUntil" class="form-control" type="text" placeholder="Until" name="valid_date_until" data-date-format="YYYY-MM-DD" readonly/>
                        <p class="help-block">e.g. 1987-11-13</p>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-3">
                        <!-- <button type="reset" class="btn btn-default">Save Packages</button> -->
                        <button class="btn btn-orange" type="submit" id="createPackage_BTN">Create Package</button>
                    </div>
                </div>
            <!-- </form> -->
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
<!-- /#page-wrapper -->