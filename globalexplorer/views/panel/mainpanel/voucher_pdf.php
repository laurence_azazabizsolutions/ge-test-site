<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Global Explorer</title>
        <link rel="icon" href="<?php echo base_url('img/icon.png'); ?>">
        <link href="<?php echo base_url('css/bootstrap.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('css/carousel.css'); ?>" rel="stylesheet">
    </head>
    <body>

        <!-- Content 
        ================================================== -->

        <!-- contact -->
        <div class="container" style="border:0px solid red; max-width:730px;">
            <br>
            <table class="table">
                <tr>
                    <td style="border:0px;" width="100px">
                        <img src="<?php echo base_url('img/ge.jpg'); ?>">
                    </td>
                    <td style="border:0px;">
                        <h4>Tour Name: <?php echo @$invoice_pdf[0]->PACTOURTITLE;?></h4>
                    </td>
                </tr>
            </table>

            <table class="table">
                <thead>
                    <tr>
                        <th>BOOKING DETAILS</th>
                        <th>DEPARTURE</th>
                        <th>RETURN</th>
                        <th>GUEST/S</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="font-size:12px;">
                            <strong>Booking No:</strong> <?php echo @$invoice_pdf[0]->BOOKNO;?> <br>
                            <strong>Tour Code:</strong><?php echo @$invoice_pdf[0]->PACTOURCODE;?><br>
                            <strong>Status:</strong><?php echo @$invoice_pdf[0]->BOOKSTATUS;?>
                        </td>
                        <td style="font-size:12px;">
                            <strong><?php echo @$invoice_pdf[0]->BOOKDEPFROM;?>-<?php echo @$invoice_pdf[0]->BOOKDEPTO; ?></strong> <br>
                            <?php echo date('d-M-Y',strtotime(@$invoice_pdf[0]->PACKCHECKIN)); ?> <br>
                            <?php echo @$invoice_pdf[0]->BOOKDEPARTDATE; ?>-<?php echo @$invoice_pdf[0]->BOOKDEPARTADATE; ?> <br>
                            <?php echo @$invoice_pdf[0]->BOOKDEPARTFLIGHTNO; ?>
                        </td>
                        <td style="font-size:12px;">
                            <strong><?php echo @$invoice_pdf[0]->BOOKRETFROM;?>-<?php echo @$invoice_pdf[0]->BOOKRETTO;?></strong> <br>
                            <?php echo date('d-M-Y',strtotime(@$invoice_pdf[0]->PACKCHECKOUT)); ?> <br>
                            <?php echo @$invoice_pdf[0]->BOOKRETURNDATE; ?>-<?php echo @$invoice_pdf[0]->BOOKRETURNADATE; ?> <br>
                            <?php echo @$invoice_pdf[0]->BOOKRETURNFLIGHTNO; ?>
                        </td>
                        <td style="font-size:12px;">
                            <?php if (isset($guest)): ?>
                                <?php $counter=0; foreach ($guest as $row): ?>
                                    <?php echo $counter+1;?>. <?php echo @$row->name;?> (<?php echo @$row->type;?>) <br>             
                                <?php $counter++; endforeach; ?>
                            <?php endif; ?>
                        </td>
                    </tr>
                </tbody>
            </table>

            <hr class="featurette-divider">

            <h4>Hotel Details</h4>
            <table class="table">
                <thead>
                    <tr>
                        <th>CHECK IN</th>
                        <th>CHECK OUT</th>
                        <th>ADDITIONAL HOTEL</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="font-size:12px;"><?php echo date('d-M-Y',strtotime(@$invoice_pdf[0]->PACKCHECKIN));?></td>
                        <td style="font-size:12px;"><?php echo date('d-M-Y',strtotime(@$invoice_pdf[0]->PACKCHECKOUT));?></td>
                        <td style="font-size:12px;">
                            <strong><?php echo @$invoice_pdf[0]->PACKEXHOTELNAME;?></strong> <br>
                            <em><?php echo @$invoice_pdf[0]->PACKREMARKS;?></em>
                        </td>
                    </tr>
                </tbody>
            </table>

            <p class="text-center"><strong>SUMMARY</strong></p>

            <table class="table" style='border:0px;'>
                <tbody>
                    <!-- body of summary -->               
                    <?php 
                        $total_grand = 0;

                        if (get_class($user) == 'Provider') {  $cols = 2; }
                        else { $cols = 3; }

                        #package summary ----------------------------------------------
                        if(count($package_summary)!=0){
                            echo "<tr class='summary_package_container'>
                                <th colspan='2' style='border:0px;'>Package Surcharges</th>
                            </tr>";

                            foreach ($package_summary as $destination) {
                                $total_grand+=$destination["subtotal"];

                                echo "<tr class='summary_package_container'>
                                    <td>{$destination['description']}</td>
                                    <td>{$destination['computation']}</td>
                                </tr>";
                            }
                        }
                        #end of package summary ----------------------------------------------


                        #destinations ----------------------------------------------
                        if(count($destination_summary)!=0){
                            for($i=0;$i<count($destination_summary);$i++){
                                $destination = $destination_summary[$i];
                                #destination header ----------------------------------------------  
                                echo "<tr>
                                    <th class='text-center' colspan='2' style='border-top:0px;'>
                                        <br>
                                        Destination : {$destination['country']} / {$destination['nights']} night(s) / {$r_summary[$i][0]['hotel_name']}
                                    </th>
                                </tr>";
                                #end of destination header ----------------------------------------------

                                #hotel surcharges ----------------------------------------------
                                if(count($hotel_summary[$i])!=0){
                                   
                                    echo "<tr>
                                        <th class='text-left' colspan='2' style='border-top:0px;'>Hotel Surcharges</th>
                                    </tr>";

                                    foreach ($hotel_summary[$i] as $hotel) {
                                        $total_grand+=$hotel["subtotal"];
                                        echo "<tr>
                                            <td>{$hotel['description']}</td>
                                            <td>{$hotel['computation']}</td>
                                        </tr>";
                                    }
                                }
                                #end of hotel surcharges ----------------------------------------------

                                #roomrate surcharges ----------------------------------------------
                                if(count($rs_summary[$i])!=0){
                                    echo "
                                    <tr>
                                        <th class='text-left' colspan='2' style='border-top:0px;'>
                                            <br>
                                            Roomrate Surcharges
                                        </th>
                                    </tr>";

                                    foreach ($rs_summary[$i] as $rs) {
                                        $total_grand+=$rs["subtotal"];
                                        echo "<tr>
                                            <td>{$rs['description']}</td>
                                            <td>{$rs['computation']}</td>
                                        </tr>";
                                    }
                                }
                                #end of roomrate surcharges ----------------------------------------------

                                #rooms summaries ----------------------------------------------
                                if(count($r_summary[$i])!=0){
                                    echo "<tr>
                                        <th class='text-left' colspan='2' style='border-top:0px;'>
                                            <br>
                                            Rooms
                                        </th>
                                    </tr>";

                                    foreach ($r_summary[$i] as $r) {
                                        $total_grand+=$r["subtotal"];
                                        echo "<tr>
                                            <td>{$r['description']}</td>
                                            <td>{$r['computation']}</td>
                                        </tr>";
                                    }
                                }
                                #end of rooms summaries ----------------------------------------------
                            }
                        }
                        #end of destinations ----------------------------------------------


                        #addon summary ----------------------------------------------
                        if(count($addon_summary)!=0){
                            echo "<tr>
                                <th colspan='2' style='border-top:0px;'>
                                    <br>
                                    Addons
                                </th>
                            </tr>";

                           foreach ($addon_summary as $destination) {
                                $total_grand+=$destination["subtotal"];
                                echo "<tr>
                                    <td>{$destination['description']}</td>
                                    <td>{$destination['computation']}</td>
                                </tr>";
                            }
                        }
                        #end of addon summary ----------------------------------------------

                        #extension summary ----------------------------------------------
                        if(count($extension_summary)!=0){
                            echo "<tr>
                                <th colspan='2' style='border-top:0px;'>
                                    <br>
                                    Extensions
                                </th>
                            </tr>";

                            foreach ($extension_summary as $destination) {
                                $total_grand+=$destination["subtotal"];
                                echo "
                                <tr>
                                    <td>{$destination['description']}</td>
                                    <td>{$destination['computation']}</td>
                                </tr>";
                            }
                        }
                        #end of extension summary ----------------------------------------------
                    ?>
                    <!-- end of summary -->
                </tbody>
            </table>


            <p style="font-size:9px;">
                <strong>For Emergency:</strong><br>
                Office: 026 419 013 / 026 419 019<br>
                Telephone: 081 402 5354
            </p>
            <ul style="font-size:9px;">
                <li>Check out time is strictly before 12 noon. All additional charges will be bear by the passengers. If passengers are late for airport pick-up, they will have to make their own transfer.</li>
                <li>All rooms are guarantted on the day of arrival. In the case of a no-show, your room(s) will be released and you will be subjected to the terms and conditions of the Cancellation/No-Show Policy specified at the time you made the booking as well as noted in the Confirmation Email.</li>
                <li>All special request are subjected to availability upon arrival.</li>
                <li>This is a shared transfer service. As such, there may be multiple hotel pick-ups en route to Airport. All times are approximate and pick up will be provided within 30 minutes from the final pick up time confirmed. Road, traffic and weather conditions may affect the schedule. The pick up time from your hotel will be at least 3 and a half hours prior to your scheduled flight time. Please make sure that you wait at the hotel conceierge desk 15 minutes prior to your confirmed pick up time. Once at the Airport, the shuttle will make stops at different terminals as necessary. Drivers are not expected to know which terminal, so please make sure you check this information with the company you booked your flight with before boarding the shuttle for your transfer to the Airport.</li>
                <li>There is no charge for a maximum of 1 piece of medium sized luggage per person. Oversize or excess luggage may have restrictions and you will be responsible for the transfer and cost of any extra luggage.</li>
            </ul>
            <p style="font-size:9px;">This is a computer generated service voucher. No signature is required.</p>
        </div>
        <!-- /details -->

    </body>
</html>