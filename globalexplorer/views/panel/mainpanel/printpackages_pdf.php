<!DOCTYPE html>
<html lang="en">
	<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Global Explorer</title>
        <link rel="icon" href="<?php echo base_url('img/icon.png'); ?>">
        <link href="<?php echo base_url('css/bootstrap.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('css/carousel.css'); ?>" rel="stylesheet">
        <style type="text/css">
            table {
                page-break-before:auto;
            }
        </style>
	</head>
	<body>
	<!-- Content 
        ================================================== -->
        <div class="container" style="border:0px solid red; max-width:730px;">
            <p class="text-center h3"><strong><?php echo @$package[0]->title.'<br>(Effective from '.@$package[0]->date_from.'  -  '.@$package[0]->date_to.' )';?></strong></p>
            <hr>
            <b>Package includes:</b>
            <br>
            <br>
            <?php echo nl2br(htmlentities(@$package[0]->description));?>
            <br>
            <br>
            <?php if(@$package[0]->tnc != ""):?>
            <b>Terms & Conditions</b> <br>
                <?php echo nl2br(htmlentities(@$package[0]->tnc));?>
            <?php endif;?>
        </div>
        <!-- /details -->
	</body>
</html>

