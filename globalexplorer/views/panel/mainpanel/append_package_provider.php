
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <h3>Summary</h3>
            <strong>Booked On:</strong> <?php echo date('d-M-Y',strtotime(@$data_summary[0]->BOOKDATE));?><br>
            <strong>Package:</strong> <?php echo @$data_summary[0]->PACNIGHTS;?> nights<br>
            <strong>Extension:</strong>
            <?php 
                if(@$data_summary[0]->BOOKEDEXTENDDAYS == 0){
                    $total_extended= @$data_summary[0]->BOOKEDEXTENDDAYS;
                }else{
                    $total_extended= @$data_summary[0]->BOOKEDEXTENDDAYS - 1;
                }
                echo $total_extended;
            ?>
            nights<br>
            <strong>Total:</strong>
            <?php 
                $totals= $total_extended + @$data_summary[0]->PACNIGHTS;     
                echo  $totals;
            ?>
            nights

            <br><br>

        
            <?
                $d=strtotime("-0 Days");
                $datetime1=date_create(date("Y-m-d", $d));
                $datetime2=date_create(@$data_summary[0]->PACKCHECKIN);
                $diff=date_diff($datetime1,$datetime2);

                $booked_id=@$data_summary[0]->BOOKID;
                $packages_id=@$data_summary[0]->packages_id;
                if (isset($user) && get_class($user) == 'Agent') {
                    if (@$data_summary[0]->BOOKSTATUS == "confirmed") {
                        echo '<a href="'.base_url('panel/pdf/'.$booked_id).'" class="btn btn-orange btn-xs" target="_blank">Invoice</a>';
                        echo "<br><br>";
                    }
                    else if (@$data_summary[0]->BOOKSTATUS == "ammended" || @$data_summary[0]->BOOKSTATUS == "rejected" || @$data_summary[0]->BOOKSTATUS == "pending") {
                        if($diff->format("%R") == "+"){
                            echo '<a href="'.base_url('panel/step_booking/'.$packages_id.'/'.$booked_id).'" class="btn btn-orange btn-xs">Amend</a>';
                            echo "<br><br>";
                        }
                    }
                }
                else if (isset($user) && get_class($user) == 'Provider') {
                    if (@$data_summary[0]->BOOKSTATUS == "ammended" || @$data_summary[0]->BOOKSTATUS == "pending") {
                        echo '<a href="#" onclick="status_booking('.$booked_id.',3); return false;" class="btn btn-orange btn-xs">Confirm</a> | 
                        <a href="#" onclick="status_booking('.$booked_id.',2); return false;" class="btn btn-orange btn-xs">Reject</a> | 
                        <a href="#" onclick="status_booking('.$booked_id.',1); return false;" class="btn btn-orange btn-xs">Cancel</a>';
                        echo "<br><br>";
                    }
                }
                else if (isset($user) && get_class($user)== 'Admin') {
                    if (@$data_summary[0]->BOOKSTATUS == "confirmed") {
                        if($diff->format("%R") == "+"){
                            echo '<a href="'.base_url('panel/step_booking/'.$packages_id.'/'.$booked_id).'" class="btn btn-orange btn-xs">Amend</a> | ';
                        }
                        echo '<a href="#" onclick="status_booking('.$booked_id.',1); return false;" class="btn btn-orange btn-xs">Cancel</a> | 
                        <a href="'.base_url('panel/pdf/'.$booked_id).'" class="btn btn-orange btn-xs" target="_blank">Invoice</a>';
                        echo "<br><br>";
                        // <a href="#" class="btn btn-orange btn-xs" data-toggle="modal" data-target="#myModalDiscount">Discount</a> | 
                    }
                    else if (@$data_summary[0]->BOOKSTATUS == "ammended" || @$data_summary[0]->BOOKSTATUS == "pending") {
                        if($diff->format("%R") == "+"){
                            echo '<a href="'.base_url('panel/step_booking/'.$packages_id.'/'.$booked_id).'" class="btn btn-orange btn-xs">Amend</a> | ';
                        }
                        echo '<a href="#" onclick="status_booking('.$booked_id.',3); return false;" class="btn btn-orange btn-xs">Confirm</a> | 
                        <a href="#" onclick="status_booking('.$booked_id.',2); return false;" class="btn btn-orange btn-xs">Reject</a> | 
                        <a href="#" onclick="status_booking('.$booked_id.',1); return false;" class="btn btn-orange btn-xs">Cancel</a> | 
                        <a href="'.base_url('panel/pdf/'.$booked_id).'" class="btn btn-orange btn-xs" target="_blank">Invoice</a>';
                        echo "<br><br>";
                        // <a href="#" class="btn btn-orange btn-xs" data-toggle="modal" data-target="#myModalDiscount">Discount</a> | 
                    }
                    else if (@$data_summary[0]->BOOKSTATUS == "cancelled") {
                        echo '<a href="'.base_url('panel/pdf/'.$booked_id).'" class="btn btn-orange btn-xs" target="_blank">Invoice</a>';
                        echo "<br><br>";
                    }
                }
            ?>

            <div class="panel panel-orange">
                <div class="panel-heading">
                    <h3 class="panel-title">Flight and Traveller Details</h3>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>BOOKING DETAILS</th>
                                <th>DEPARTURE</th>
                                <th>RETURN</th>
                                <th>GUEST/S</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                  <strong>Booking No:</strong> <?php echo @$data_summary[0]->BOOKNO;?> <br>
                                  <strong>Tour Code:</strong> <?php echo @$data_summary[0]->PACTOURCODE;?><br>
                                  <strong>Status:</strong> <span id="action_append"><?php echo @$data_summary[0]->BOOKSTATUS;?></span>
                                </td>
                                <td>
                                  <strong> <?php echo @$data_summary[0]->BOOKDEPFROM;?>-<?php echo @$data_summary[0]->BOOKDEPTO; ?></strong> <br>
                                  <?php echo date('d-M-Y',strtotime(@$data_summary[0]->PACKCHECKIN)); ?> <br>
                                  <?php echo @$data_summary[0]->BOOKDEPARTDATE; ?>-<?php echo @$data_summary[0]->BOOKDEPARTADATE; ?> <br>
                                  <?php echo @$data_summary[0]->BOOKDEPARTFLIGHTNO; ?>
                                </td>
                                <td>
                                  <strong> <?php echo @$data_summary[0]->BOOKRETFROM;?>-<?php echo @$data_summary[0]->BOOKRETTO;?></strong> <br>
                                  <?php echo date('d-M-Y',strtotime(@$data_summary[0]->PACKCHECKOUT)); ?> <br>
                                  <?php echo @$data_summary[0]->BOOKRETURNDATE; ?>-<?php echo @$data_summary[0]->BOOKRETURNADATE; ?> <br>
                                  <?php echo @$data_summary[0]->BOOKRETURNFLIGHTNO; ?>
                                </td>
                                <td>      
                                    <?php if (isset($guest_summary)): ?>
                                        <?php $counter=0; foreach ($guest_summary as $row): ?>
                                        <?php echo $counter+1;?>. <?php echo @$row->name;?> (<?php echo @$row->type;?>)<br>             
                                        <?php $counter++; endforeach; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>REMARKS</th>
                                <td colspan="3"><?php echo @$data_summary[0]->BOOKREMARKS;?></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>

            <div class="panel panel-orange">
                <div class="panel-heading">
                    <h3 class="panel-title">Package Hotel and Optional Tours </h3>
                </div>

                <div class="table-responsive ">
                    <table class="table" style="margin-bottom:20px;">
                        <thead>
                            <tr>
                                <th>CHECK IN</th>
                                <th>CHECK OUT</th>
                                <th>ADDITIONAL HOTEL</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td> <?php echo date('d-M-Y',strtotime(@$data_summary[0]->PACKCHECKIN));?></td>
                                <td> <?php echo date('d-M-Y',strtotime(@$data_summary[0]->PACKCHECKOUT));?></td>
                                <td> 
                                    <strong><?php echo @$data_summary[0]->PACKEXHOTELNAME;?></strong> <br>
                                    <em><?php echo @$data_summary[0]->PACKREMARKS;?></em>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <p class="text-center"><strong>SUMMARY</strong></p>
                <div class="table-responsive ">
                    <table class="table">
                         

                        <!-- body of summary -->               
                        <?php 
                            $total_grand = 0;
                    

                            #package summary ----------------------------------------------
                            if(count($package_summary)!=0){
                                echo "
                                <tr class='summary_package_container'>
                                    <th colspan='3' style='border-top:0px;'>Package Surcharges</th>
                                </tr>";
                                
                                foreach ($package_summary as $destination) {
                                    $total_grand+=$destination["subtotal"];

                                    echo "
                                    <tr class='summary_package_container'>
                                        <td>{$destination['description']}</td>
                                        <td>{$destination['computation']}</td>
                                        <td>{$destination['subtotal']} SGD</td>
                                    </tr>";
                                }
                            }
                            #end of package summary ----------------------------------------------


                            #destinations ----------------------------------------------
                            if(count($destination_summary)!=0){
                                for($i=0;$i<count($destination_summary);$i++){
                                    $destination = $destination_summary[$i];
                                    #destination header ----------------------------------------------  
                                    echo "
                                    <tr>
                                        <th class='text-center' colspan='3' style='border-top:0px;'>
                                            <br>
                                            Destination : {$destination['country']} / {$destination['nights']} night(s) / {$r_summary[$i][0]['hotel_name']}
                                        </th>
                                    </tr>";
                                    #end of destination header ----------------------------------------------

                                    #hotel surcharges ----------------------------------------------
                                    if(count($hotel_summary[$i])!=0){
                                       
                                        echo "
                                        <tr>
                                            <th class='text-left' colspan='3' style='border-top:0px;'>Hotel Surcharges</th>
                                        </tr>";

                                        foreach ($hotel_summary[$i] as $hotel) {
                                            $total_grand+=$hotel["subtotal"];
                                            echo "
                                            <tr>
                                                <td>{$hotel['description']}</td>
                                                <td>{$hotel['computation']}</td>
                                                <td>{$hotel['subtotal']} SGD</td>
                                            </tr>";
                                        }
                                    }
                                    #end of hotel surcharges ----------------------------------------------

                                    #roomrate surcharges ----------------------------------------------
                                    if(count($rs_summary[$i])!=0){
                                        echo "
                                        <tr>
                                            <th class='text-left' colspan='3' style='border-top:0px;'>
                                                <br>
                                                Roomrate Surcharges
                                            </th>
                                        </tr>";

                                        foreach ($rs_summary[$i] as $rs) {
                                            $total_grand+=$rs["subtotal"];
                                            echo "
                                             <tr>
                                                <td>{$rs['description']}</td>
                                                <td>{$rs['computation']}</td>
                                                <td>{$rs['subtotal']} SGD</td>
                                            </tr>";
                                        }
                                    }
                                    #end of roomrate surcharges ----------------------------------------------

                                    #rooms summaries ----------------------------------------------
                                    if(count($r_summary[$i])!=0){
                                        echo "
                                        <tr>
                                            <th class='text-left' colspan='3' style='border-top:0px;'>
                                                <br>
                                                Rooms
                                            </th>
                                        </tr>";

                                        foreach ($r_summary[$i] as $r) {
                                            $total_grand+=$r["subtotal"];
                                            echo "
                                             <tr>
                                                <td>{$r['description']}</td>
                                                <td>{$r['computation']}</td>
                                                <td>{$r['subtotal']} SGD</td>
                                            </tr>";
                                        }
                                    }
                                    #end of rooms summaries ----------------------------------------------
                                }
                            }
                            #end of destinations ----------------------------------------------


                            #addon summary ----------------------------------------------
                            if(count($addon_summary)!=0){
                                echo "
                                <tr>
                                    <th colspan='3' style='border-top:0px;'>
                                        <br>
                                        Addons
                                    </th>
                                </tr>";

                               foreach ($addon_summary as $destination) {
                                    $total_grand+=$destination["subtotal"];
                                    echo "
                                    <tr>
                                        <td>{$destination['description']}</td>
                                        <td>{$destination['computation']}</td>
                                        <td>{$destination['subtotal']} SGD</td>
                                    </tr>";
                                }
                            }
                            #end of addon summary ----------------------------------------------

                            #extension summary ----------------------------------------------
                            if(count($extension_summary)!=0){
                                echo "
                                <tr>
                                    <th colspan='3' style='border-top:0px;'>
                                        <br>
                                        Extensions
                                    </th>
                                </tr>";

                                $remark = "";
                                foreach ($extension_summary as $destination) {
                                    $total_grand+=$destination["subtotal"];
                                    $remark = $destination["remark"];
                                    echo "
                                    <tr>
                                        <td>{$destination['description']}</td>
                                        <td>{$destination['computation']}</td>
                                        <td>{$destination['subtotal']} SGD</td>
                                    </tr>";
                                }

                                echo "
                                <tr>
                                    <td><strong>Remarks</strong></td>
                                    <td colspan=2>{$remark}</td>
                                </tr>";
                            }
                            #end of extension summary ----------------------------------------------

                        ?>
                        <!-- end of summary -->


                            
                                        
                        <!-- totals -->
                        <tfoot>
                            <?php if (isset($user) && get_class($user)!= 'Provider'): ?>
                                <tr>
                                    <td colspan="3" style="border-top:0px;"><br></td>
                                </tr>
                                <tr>
                                    <th colspan="2">TOTAL</th>
                                    <th><?php echo @$total_grand.' SGD';?></th> 
                                </tr>
                            <?php endif; ?>
                        </tfoot>
                        <!-- totals-->
                    </table>
                </div>
            </div>

            <!-- Agent/Provider -->
            <div class="panel panel-orange">
                <div class="panel-heading">
                    <h3 class="panel-title">Agent Details</h3>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <th width="200px">Company Name</th>
                            <td> <?php echo @$data_summary[0]->AGENTCOMPANYNAME;?></td>
                        </tr>
                        <tr>
                            <th>Staff Name</th>
                            <td> <?php 
                                if(@$data_summary[0]->agentdetailsname != NULL)
                                    echo @$data_summary[0]->agentdetailsname;
                                else
                                    echo @$data_summary[0]->AGENTLNAME.' ,'.@$data_summary[0]->AGENTFNAME;
                            ?></td>
                        </tr>
                        <tr>
                            <th>Email Address</th>
                            <td> <?php echo @$data_summary[0]->AGENTEMAIL;?></td>
                        </tr>
                        <tr>
                            <th>Contact Number</th>
                            <td> <?php echo @$data_summary[0]->AGENTPHONE;?></td>
                        </tr>
                        <tr>
                            <th>Reference Number</th>
                            <td><?php 
                                if(@$data_summary[0]->agentdetailsref != NULL)
                                    echo @$data_summary[0]->agentdetailsref;
                                else
                                    echo '0';
                             ?></td>
                        </tr>
                    </table>
                </div>
            </div>

            <?php if (isset($user) && get_class($user) <> 'Agent') : ?>

                <div class="panel panel-orange">
                    <div class="panel-heading">
                        <h3 class="panel-title">Provider Details</h3>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th width="200px">Company Name</th>
                                <td> <?php echo @$provider_summary[0]->COMNAME;?></td>
                            </tr>
                            <tr>
                                <th>Staff Name</th>
                                <td> <?php echo @$provider_summary[0]->providerlname.' ,'.@$provider_summary[0]->providerfname;?></td>
                            </tr>
                            <tr>
                                <th>Email Address</th>
                                <td> <?php echo @$provider_summary[0]->provideremail;?></td>
                            </tr>
                            <tr>
                                <th>Contact Number</th>
                                <td> <?php echo @$provider_summary[0]->providerphone;?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            <?php endif; ?>
            <!-- /.table-responsive -->

        </div>
    </div>
