    <tr roomrateId="<?php echo $roomrate['id'];?>">
        <td>
            <a href="#" class="edit_roomrate_link" data-target="#myModalRoomRate">Edit</a> | 
            <a href="javascript:void(0);" onclick="removePackageRoomRate(this);" roomrate-id="<?php echo $roomrate["id"]; ?>">Delete</a> | 
            <a href="#" class="package_hotel_add" data-target="#myModalRoomRateSurcharge">Add Surcharge</a>
        </td>
        <td>
            <?php
                if(trim($roomrate['description'])!='')
                    echo ucfirst($roomrate['description']).' - ';
                echo ucwords(str_replace('_', ' ', $roomrate['room_type']));
            ?>
        </td>
        <?php 
            $cost       = (trim($roomrate['cost'])!='' || $roomrate['cost']!=NULL)              ? $roomrate['cost'] : 0.00;
            $profit     = (trim($roomrate['profit'])!='' || $roomrate['profit']!=NULL)          ? $roomrate['profit'] : 0.00;
            $ext_cost   = (trim($roomrate['ext_cost'])!='' || $roomrate['ext_cost']!=NULL)      ? $roomrate['ext_cost'] : 0.00;
            $ext_profit = (trim($roomrate['ext_profit'])!='' || $roomrate['ext_profit']!=NULL)  ? $roomrate['ext_profit'] : 0.00; 
        ?>
        <td><?php echo $cost; ?></td>
        <td><?php echo $profit; ?></td>
        <td><?php echo ($profit+$cost); ?></td>
        <td><?php echo $ext_cost; ?></td>
        <td><?php echo $ext_profit; ?></td>
        <td><?php echo ($ext_cost+$ext_profit); ?></td>
        <td><?php if(trim($roomrate['rate_type']))  echo ucwords(str_replace('/', ' / ', $roomrate['rate_type'])); ?></td>
    </tr>

