<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Global Explorer</title>
 		<style type="text/css">
			h1,
			h4{
				font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
				font-weight: 500;
				line-height: 1.1;
				color: inherit;
			}
		</style>
	</head>
	<body style="color: #333333;">
		<div><img src="<?php echo base_url('img/ge-header.jpg'); ?>" style="width: 100%;height: auto;"></div>
		<h3>Just a friendly reminder</h3>
		<p>This is a reminder about the upcoming booking.<br>We would like to share some information with you before the arrival you get outta here with us</p>
		<strong>DATE ARRIVAL: <?php echo date('j F Y',strtotime(date('Y-m-d')."+ 2 days"))?></strong><br><br>
		<table cellspacing="0" cellpadding="0" style="background-color: #ffffff; border: 1px solid transparent;  border-radius: 4px; -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05); box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05); border-color: #e95a3e;">
			<tr>
				<td style="padding: 10px 15px; border-bottom: 1px solid transparent; border-top-right-radius: 3px; border-top-left-radius: 3px; color: #fff; background-color: #e95a3e; border-color: #e95a3e;">
					<h1 style="margin-top: 0; margin-bottom: 0; font-size: 16px; color: inherit; text-align: center; ">
						Global Explorer
					</h1> 
				</td>
			</tr>
			<tr>
				
			</tr>
			<tr>
				<td style="padding: 15px;">
                	<strong>No. of Departure Bookings(<i><?php echo count($departure_list); ?></i>)</strong>
                    <table style="width: 770px; margin-bottom: 20px; border-collapse: collapse;">
                    	<thead>
	                        <tr>
	                            <td style="padding: 8px; vertical-align: bottom; border: 1px solid #dddddd;"><strong>Booking Code</strong></td>
	                            <td style="padding: 8px; vertical-align: bottom; border: 1px solid #dddddd; width: 20%;"><strong>Guest Name</strong></td>
	                            <td style="padding: 8px; vertical-align: bottom; border: 1px solid #dddddd;"><strong>Pax</strong></td>
	                            <td style="padding: 8px; vertical-align: bottom; border: 1px solid #dddddd;"><strong>Hotel</strong></td>
	                            <td style="padding: 8px;width:40%; vertical-align: bottom; border: 1px solid #dddddd;"><strong>No. of Room</strong></td>
	                            <td style="padding: 8px;width:125%; vertical-align: bottom; border: 1px solid #dddddd;text-align:center"><strong>Flight information</strong></td>
	                            <td style="padding: 8px; vertical-align: bottom; border: 1px solid #dddddd;"><strong>Extensions</strong></td>
	                        </tr>
                    	</thead>
                    	<tbody>
                    		<?php
                    			// if($user_data != NULL || $user_data != ""){
                       //              foreach($user_data as $row){
                                    	//@$total_all_price += $row['total_price'];
                                    	//if(@$row['email'] == @$mail_checker){
                           // ?>
	                            			
					                           <?php foreach($departure_list as $depart): ?>
					                           	<tr>
					                           	<td align="center" style="padding: 8px; border: 1px solid #dddddd; margin-top:-35px"><?php echo $depart->bCode; ?><br><br>
					                           	<?php $package = $this->db->get_where('packages',array('id'=>$depart->package_id))->result(); echo $package[0]->title?>
					                           	</td>
					                           	<td align="center" style="padding: 8px; border: 1px solid #dddddd;">
					                           		<!---<?php if(count($traveller_details) > 1): ?>
						                           		<?php foreach($traveller_details as $travellers): ?>
						                           			<?php $nameArray[] = $travellers->name; ?>
						                           		<?php endforeach; echo implode( ', ', $nameArray ); ?>
						                           	<?php else: ?>
						                           		<?php foreach($traveller_details as $travellers): ?>
						                           			<?php echo $travellers->name; ?>
						                           		<?php endforeach; ?>
						                           	<?php endif; ?> -->
						                           	<?php 
						                           	$traveller_details = $this->booking->get_booking_info($depart->bId);
						                           	echo $traveller_details[0]->name; ?>
					                           	</td>
					                           	<td align="center" style="padding: 8px;  border: 1px solid #dddddd;"><p><?php echo count($traveller_details); ?></p></td>
					                           	<td align="center" style="padding: 8px;  border: 1px solid #dddddd;">
					                           		<p>
					                           		<?php
														$hotelname = $this->booking->getHotelName($depart->bCode);
														//dump($hotelname);
														foreach ($hotelname as $hn) {
														echo $hn['name'];
														}
													?>
													</p>
					                           		<!--- <?php echo $hotel_details->name; ?> -->
					                           	</td>
					                           	<td align="center" style="padding: 8px;width:40%;  border: 1px solid #dddddd;">
					                           		<p>
					                           		<?php
														$room_info = $this->booking->get_room_info($depart->bId);
														//dump($hotelname);
														foreach ($room_info as $rm) {
															if(strpos($rm->description, 'Single')  || strpos($rm->description, 'Child')){
																$array_computation = explode(" ",$rm->computation);
																$number = strip_tags($array_computation[0]);
																echo $number.' '.$rm->description.'<br>';
															}
															else if(strpos($rm->description, 'Twin')){
																$array_computation = explode(" ",$rm->computation);
																$number = strip_tags($array_computation[0]);
																$total = $number/2;
																echo $total.' '.$rm->description.'<br>';
															}
															else if(strpos($rm->description, 'Triple')){
																$array_computation = explode(" ",$rm->computation);
																$number = strip_tags($array_computation[0]);
																$total = $number/3;
																echo $total.' '.$rm->description.'<br>';
															}
															else if(strcasecmp($rm->description, 'Single') == 0){
																$array_computation = explode(" ",$rm->computation);
																$number = strip_tags($array_computation[0]);
																echo $number.' '.$rm->description.'<br>';
															}
															else if(strcasecmp($rm->description, 'Twin') == 0){
																$array_computation = explode(" ",$rm->computation);
																$number = strip_tags($array_computation[0]);
																$total = $number/2;
																echo $total.' '.$rm->description.'<br>';
															}
															else if(strcasecmp($rm->description, 'Triple') == 0){
																$array_computation = explode(" ",$rm->computation);
																$number = strip_tags($array_computation[0]);
																$total = $number/3;
																echo $total.' '.$rm->description.'<br>';
															}
															else{
																$array_computation = explode(" ",$rm->computation);
																$number = strip_tags($array_computation[0]);
																echo $number.' '.$rm->description.'<br>';
															}

														//echo $rm->computation.'-'.$rm->description.'<br>';
														}
													?>
													</p>
					                           		<!--- <?php echo $hotel_details->name; ?> -->
					                           	</td>				                          
					                           	<td align="center" style="padding: 8px;width:125%;  border: 1px solid #dddddd;">
					                           	<?php
					                           		$of_from = $this->booking->get_airportcode($depart->of_from);
					                           		$of_to = $this->booking->get_airportcode($depart->of_to);
					                           		$rf_from = $this->booking->get_airportcode($depart->rf_from);
					                           		$rf_to = $this->booking->get_airportcode($depart->rf_to);
					                           	?>
					                           	<?php $depart_of_time = explode(":",$depart->of_departuretime);
					                           		  $arrival_of_time = explode(":",$depart->of_arrivaltime);
					                           		  $depart_rf_time = explode(":",$depart->rf_departuretime);
					                           		  $arrival_rf_time = explode(":",$depart->rf_arrivaltime);
					                           	?>
					                           		<p><?php echo date_format(date_create($depart->departure_date),"j F Y").' '.$depart->of_num.'-'.$of_from[0]->code.'/'.$of_to[0]->code.' '.$depart_of_time[0].$depart_of_time[1].'/'.$arrival_of_time[0].$arrival_of_time[1]?><br>
					                           		<?php echo date_format(date_create($depart->return_date),"j F Y").' '.$depart->rf_num.'-'.$rf_from[0]->code.'/'.$rf_to[0]->code.' '.$depart_rf_time[0].$depart_rf_time[1].'/'.$arrival_rf_time[0].$arrival_rf_time[1]?></p>
					                           	</td>
					                           	<td align="center" style="padding: 8px;  border: 1px solid #dddddd;">
					                           		
					                           		<?php if($depart->extended_days>0){?>
					                           		<p><?php echo $depart->extended_days?> NIGHT/S</p>
					                           		<?php }else{ ?>
					                           		<p>0 NIGHT/S</p>
					                           		<?php } ?>
					                           	</td>
					                            </tr>
					                           <?php endforeach; ?>
<!--                             <?php
                                    	//}
                                //     }
                                // }
                             ?> -->
                    	</tbody>
                    	<!-- <tfoot>
                    		<tr>
                    			<td colspan="1" style="padding: 9px; vertical-align: top; border: 1px solid #dddddd;">
                    				<?php echo 'Bookings: '.$user_data;?>
                    			</td>
                    			<td colspan="1" style="padding: 8px; vertical-align: top; border: 1px solid #dddddd;">
                    				<?php echo number_format(@$total, 2, '.', ',');?>
                    			</td>
                    			<td colspan="3" style="padding: 8px; vertical-align: top; border: 1px solid #dddddd;">
                    			</td>
                    		</tr>
                    	</tfoot> -->
                    </table>
				</td>
			</tr>
		</table>
	</body>
</html>