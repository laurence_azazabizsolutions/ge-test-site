<div class="batch-modal" style="height:100%; position: fixed; z-index: 9999999; width: 100%; background: none repeat scroll 0px 0px rgba(51, 51, 51, 0.7);">
	<div class="container">
		<div class="row">
			<div class="batch_modal_panel">
				<div id="myModalRoomRate" class="col-md-12" style="overflow-y: auto; background: none repeat scroll 0% 0% rgb(255, 255, 255); margin: 5% 0px 0px; border-radius: 10px; padding: 20px;">
					<div class="batch-header">
                        <button id="batch_close" class="pull-right btn btn-danger btn-sm" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                        <h4 id="myModalLabel" class="modal-title">Hotel Room Rate (Batch Edit)</h4>
                    </div>
                    <div class="row">
                    	<div class="col-md-12">
                    		<div class="form-group">
                    			<label class="col-md-3 control-label" style="text-align:right;">Cost</label>
                    			<div class="col-md-3">
                    				<input type="text" step="any" class="form-control" name="hotelroomrate_cost" onkeyup="formatAlphaNum(this); return false;" id="cost_all" placeholder="Cost">
                    			</div>
                    			<label class="col-md-3 control-label" style="text-align:right;">Ext. Cost</label>
                    			<div class="col-md-3">
                    				<input type="text" step="any" class="form-control" name="hotelroomrate_cost_ext"  onkeyup="formatAlphaNum(this); return false;" id="ex_cost_all" placeholder="Ext. Cost">
                    			</div>
                    		</div>
                    		<br>
                    		<div class="form-group">
                    			<label class="col-md-3 control-label" style="text-align:right;">Profit</label>
                    			<div class="col-md-3">
                    				<input type="text" step="any" class="form-control" name="hotelroomrate_profit"  onkeyup="formatAlphaNum(this); return false;" id="profit_all"  placeholder="Profit">
                    			</div>
                    			<label class="col-md-3 control-label" style="text-align:right;">Ext. Profit</label>
                    			<div class="col-md-3">
                    				<input type="text" step="any" class="form-control" name="hotelroomrate_profit_ext"  onkeyup="formatAlphaNum(this); return false;" id="ex_profit_all"  placeholder="Ext. Profit">
                    			</div>
                    		</div>
                    	</div>
                    </div>
                    <div class="batch-body">
						<form id="addhotelroomrate_form" class="form-horizontal form_batch_edit" role="form" action="<?php echo base_url('panel/packageBatchUpdate')?>" method="post">
                            <div class="alert alert-danger" style="display: none;"></div>
                            <input type="hidden" name="parent_id" value="">
                            <input type="hidden" name="act_type" value="edit">
                            <div class="clearfix"></div>
                            <div class="modal-body modal-nofixed">
                            <div class="col-md-12">
                            <table class="table_bedit table table-bordered table-condensed" data-height="299">
								  <thead>
								  	<tr>
								  		<th>Rate Name</th>
								  		<th>Rate Type</th>
								  		<th>Description</th>
								  		<th>Cost</th>
								  		<th>Ext. Cost</th>
								  		<th>Profit</th>
								  		<th>Ext. Profit</th>
								  	</tr>
								  </thead>
							      <tbody>
							        
			                            <?php
			                            if(!count($data)):
			                            	echo '<tr><td colspan="9">NO DATA FOUND.</tr></td>';
			                            else:
			                            	//dump($data);
			                            	foreach($data as $batch_edit => $value):
			                            ?>
			                        	<tr id="bedit_roomrate">
										<td>
											<input type="hidden" class="packageid" name="package_id" value="<?php echo $value['package_hotel_id']; ?>">											
			                        		<input type="hidden" name="id[]" value="<?php echo $value['id']; ?>">
											<select name="hotelroomrate_name[]" class="form-control">
											<?php switch ($value['room_type']) {
													case 'single':
														echo '<option value="single" selected="selected">Single</option>';
														echo '<option value="twin">Twin</option>';
														echo '<option value="triple">Triple</option>';
														echo '<option value="child_half_twin">Child Half Twin</option>';
														echo '<option value="child_with_bed">Child With Bed</option>';
														echo '<option value="child_without_bed">Child Without Bed</option>';
														echo '<option value="extra_bed">Extra Bed</option>';
														echo '<option value="breakfast">Breakfast</option>';
														break;
													
													case 'twin':
														echo '<option value="twin" selected="selected">Twin</option>';
														echo '<option value="single">Single</option>';
														echo '<option value="twin">Twin</option>';
														echo '<option value="triple">Triple</option>';
														echo '<option value="child_half_twin">Child Half Twin</option>';
														echo '<option value="child_with_bed">Child With Bed</option>';
														echo '<option value="child_without_bed">Child Without Bed</option>';
														echo '<option value="extra_bed">Extra Bed</option>';
														echo '<option value="breakfast">Breakfast</option>';
														break;

													case 'triple':
														echo '<option value="triple" selected="selected">Triple</option>';
														echo '<option value="single">Single</option>';
														echo '<option value="twin">Twin</option>';
														echo '<option value="child_half_twin">Child Half Twin</option>';
														echo '<option value="child_with_bed">Child With Bed</option>';
														echo '<option value="child_without_bed">Child Without Bed</option>';
														echo '<option value="extra_bed">Extra Bed</option>';
														echo '<option value="breakfast">Breakfast</option>';
														break;

													case 'child_with_bed':
														echo '<option value="child_with_bed" selected="selected">Child With Bed</option>';
														echo '<option value="single">Single</option>';
														echo '<option value="twin">Twin</option>';
														echo '<option value="triple">Triple</option>';
														echo '<option value="child_half_twin">Child Half Twin</option>';
														echo '<option value="child_without_bed">Child Without Bed</option>';
														echo '<option value="extra_bed">Extra Bed</option>';
														echo '<option value="breakfast">Breakfast</option>';
														break;

													case 'child_without_bed':
														echo '<option value="child_without_bed" selected="selected">Child Without Bed</option>';
														echo '<option value="single">Single</option>';
														echo '<option value="twin">Twin</option>';
														echo '<option value="triple">Triple</option>';
														echo '<option value="child_half_twin">Child Half Twin</option>';
														echo '<option value="child_with_bed">Child With Bed</option>';
														echo '<option value="extra_bed">Extra Bed</option>';
														echo '<option value="breakfast">Breakfast</option>';
														break;

													case 'extra_bed':
														echo '<option value="extra_bed" selected="selected">Extra Bed</option>';
														echo '<option value="single">Single</option>';
														echo '<option value="twin">Twin</option>';
														echo '<option value="triple">Triple</option>';
														echo '<option value="child_half_twin">Child Half Twin</option>';
														echo '<option value="child_with_bed">Child With Bed</option>';
														echo '<option value="child_without_bed">Child Without Bed</option>';
														echo '<option value="breakfast">Breakfast</option>';
														break;

													case 'breakfast':
														echo '<option value="breakfast" selected="selected">Breakfast</option>';
														echo '<option value="single">Single</option>';
														echo '<option value="twin">Twin</option>';
														echo '<option value="triple">Triple</option>';
														echo '<option value="child_half_twin">Child Half Twin</option>';
														echo '<option value="child_with_bed">Child With Bed</option>';
														echo '<option value="child_without_bed">Child Without Bed</option>';
														echo '<option value="extra_bed">Extra Bed</option>';
														break;

													case 'child_half_twin':
														echo '<option value="child_half_twin">Child Half Twin</option>';
														echo '<option value="breakfast" selected="selected">Breakfast</option>';
														echo '<option value="single">Single</option>';
														echo '<option value="twin">Twin</option>';
														echo '<option value="triple">Triple</option>';
														echo '<option value="child_with_bed">Child With Bed</option>';
														echo '<option value="child_without_bed">Child Without Bed</option>';
														echo '<option value="extra_bed">Extra Bed</option>';
														break;
												}
											?>
										</select>
										</td>
										<td>
							          	<select name="hotelroomrate_type[]" class="form-control">
											<?php switch ($value['rate_type']) {
												case 'pax/night':
													echo '<option value="pax/night" selected="selected">Pax / Night</option>';
		                                            echo '<option value="adult/night">Adult / Night</option>';
		                                            echo '<option value="child/night">Child / Night</option>';
		                                            echo '<option value="room/night">Room / Night</option>';
		                                            echo '<option value="pax/trip">Pax / Trip</option>';
		                                            echo '<option value="adult/trip">Adult / Trip</option>';
		                                            echo '<option value="child/trip">Child / Trip</option>';
		                                            echo '<option value="room/trip">Room / Trip</option>';
												break;

												case 'adult/night':
													echo '<option value="pax/night">Pax / Night</option>';
		                                            echo '<option value="adult/night" selected="selected">Adult / Night</option>';
		                                            echo '<option value="child/night">Child / Night</option>';
		                                            echo '<option value="room/night">Room / Night</option>';
		                                            echo '<option value="pax/trip">Pax / Trip</option>';
		                                            echo '<option value="adult/trip">Adult / Trip</option>';
		                                            echo '<option value="child/trip">Child / Trip</option>';
		                                            echo '<option value="room/trip">Room / Trip</option>';
												break;

												case 'child/night':
													echo '<option value="pax/night">Pax / Night</option>';
		                                            echo '<option value="adult/night">Adult / Night</option>';
		                                            echo '<option value="child/night" selected="selected">Child / Night</option>';
		                                            echo '<option value="room/night">Room / Night</option>';
		                                            echo '<option value="pax/trip">Pax / Trip</option>';
		                                            echo '<option value="adult/trip">Adult / Trip</option>';
		                                            echo '<option value="child/trip">Child / Trip</option>';
		                                            echo '<option value="room/trip">Room / Trip</option>';
												break;

												case 'room/night':
													echo '<option value="pax/night">Pax / Night</option>';
		                                            echo '<option value="adult/night">Adult / Night</option>';
		                                            echo '<option value="child/night">Child / Night</option>';
		                                            echo '<option value="room/night" selected="selected">Room / Night</option>';
		                                            echo '<option value="pax/trip">Pax / Trip</option>';
		                                            echo '<option value="adult/trip">Adult / Trip</option>';
		                                            echo '<option value="child/trip">Child / Trip</option>';
		                                            echo '<option value="room/trip">Room / Trip</option>';
												break;

												case 'pax/trip':
													echo '<option value="pax/night">Pax / Night</option>';
		                                            echo '<option value="adult/night">Adult / Night</option>';
		                                            echo '<option value="child/night">Child / Night</option>';
		                                            echo '<option value="room/night">Room / Night</option>';
		                                            echo '<option value="pax/trip" selected="selected">Pax / Trip</option>';
		                                            echo '<option value="adult/trip">Adult / Trip</option>';
		                                            echo '<option value="child/trip">Child / Trip</option>';
		                                            echo '<option value="room/trip">Room / Trip</option>';
												break;

												case 'adult/trip':
													echo '<option value="pax/night">Pax / Night</option>';
		                                            echo '<option value="adult/night">Adult / Night</option>';
		                                            echo '<option value="child/night">Child / Night</option>';
		                                            echo '<option value="room/night">Room / Night</option>';
		                                            echo '<option value="pax/trip">Pax / Trip</option>';
		                                            echo '<option value="adult/trip" selected="selected">Adult / Trip</option>';
		                                            echo '<option value="child/trip">Child / Trip</option>';
		                                            echo '<option value="room/trip">Room / Trip</option>';
												break;

												case 'child/trip':
													echo '<option value="pax/night">Pax / Night</option>';
		                                            echo '<option value="adult/night">Adult / Night</option>';
		                                            echo '<option value="child/night">Child / Night</option>';
		                                            echo '<option value="room/night">Room / Night</option>';
		                                            echo '<option value="pax/trip">Pax / Trip</option>';
		                                            echo '<option value="adult/trip">Adult / Trip</option>';
		                                            echo '<option value="child/trip" selected="selected">Child / Trip</option>';
		                                            echo '<option value="room/trip">Room / Trip</option>';
												break;

												case 'room/trip':
													echo '<option value="pax/night">Pax / Night</option>';
		                                            echo '<option value="adult/night">Adult / Night</option>';
		                                            echo '<option value="child/night">Child / Night</option>';
		                                            echo '<option value="room/night">Room / Night</option>';
		                                            echo '<option value="pax/trip">Pax / Trip</option>';
		                                            echo '<option value="adult/trip">Adult / Trip</option>';
		                                            echo '<option value="child/trip">Child / Trip</option>';
		                                            echo '<option value="room/trip" selected="selected">Room / Trip</option>';
												break;
											}
											?>
                                            <!--
                                            
                                            -->
                                        </select>
							          </td>
							          <td><input type="text" name="hotelroomrate_description[]" class="form-control"  value="<?php echo $value['description']; ?>"></td>
							          <td class="form-group"><input type="number" onkeypress="return isNumberKey(event)" placeholder="Default" name="hotelroomrate_cost[]" id="sum_cost_all" class="form-control" step="any" value="<?php echo $value['cost']; ?>"></td>
							          <td><input type="number" onkeypress="return isNumberKey(event)" placeholder="Extension" name="hotelroomrate_cost_ext[]" id="exsum_cost_all" class="form-control" step="any" value="<?php echo $value['ext_cost']; ?>"></td>
							          <td ><input type="number" onkeypress="return isNumberKey(event)" placeholder="Default" name="hotelroomrate_profit[]" class="form-control" step="any" value="<?php echo $value['profit']; ?>"></td>
							          <td><input type="number" onkeypress="return isNumberKey(event)" placeholder="Extension" name="hotelroomrate_profit_ext[]" class="form-control" step="any" value="<?php echo $value['ext_profit']; ?>"></td>
			                        </tr>
			                        	<?php
			                            	endforeach;
			                            endif;
			                            ?>
							      </tbody>
							    </table>
                            </div>
							<div class="clearfix"></div>
                                <!-- <div class="form-group">
                                    <label class="col-xs-4 control-label">Pax</label>
                                    <div class="col-xs-8">
                                        <input type="number" class="form-control" name="hotelroomrate_pax" onkeypress="return isNumberKey(event)">
                                    </div>
                                </div> -->
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-orange batch_apply_submit" type="button" role="button">Apply</button>
                          		<button class="btn btn-orange batch_edit_submit" type="submit" role="button">Save</button>
                            </div>
                        </form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(function(){
	  $(window).resize(function(){
	    var windows = $(this);
	    $('.batch-wrap').find('.modal-body').css('height',windows.height() - 315);
	    $('.table_bedit').attr('data-height',windows.height() - 340);
	  });
	  $(window).resize();
	});

	$(document).ready(function(){
		/*$('.batch_edit_submit').click(function(e){
			var package_id = $('#bedit_roomrate').attr('data-packageid');
			var action = $('.form_batch_edit').attr('action');
			var baseurl = $('html').attr('base-url');
			var data = $('.form_batch_edit').serializeArray();
			$.post(action,data,function(result)
            {
              result = $.parseJSON(result);
              if(result.status === true)
              {
                
                $('table#packagehotels_table').find('[packagehotelid='+package_id+']').find('#roomrate_table.table > tbody').load(baseurl + 'panel/package_batch_edit_view/' + package_id);
                data = "";
                $('.batch-wrap .batch-modal').remove();

              }else{
                alert('Failed');
              }
              
            }).fail(function(error){
              //console.log(error.responseText);
            });

			e.preventDefault();
		});*/

		$('.table_bedit').bootstrapTable();

		$('.form_batch_edit')
		.bootstrapValidator({
			message: 'This value is not valid',
			feedbackIcons: {
				valid: 'glyphicon glyphicon-ok',
				invalid: 'glyphicon glyphicon-remove',
				validating: 'glyphicon glyphicon-refresh'
			},
			fields: {
						'hotelroomrate_cost[]': {
							message: 'The Cost is not valid',
							validators: {
								notEmpty: {
									message: 'Cost is required and can\'t be empty'
								},
								greaterThan: {
								    value: 0,
								    message: 'Please enter a value greater than or equal to %s'
								}
							}
						},
						'hotelroomrate_cost_ext[]': {
							message: 'The Cost is not valid',
							validators: {
								notEmpty: {
									message: 'Cost Ext. is required and can\'t be empty'
								},
								greaterThan: {
								    value: 0,
								    message: 'Please enter a value greater than or equal to %s'
								}
							}
						},
						'hotelroomrate_profit[]': {
							message: 'The Profit is not valid',
							validators: {
								notEmpty: {
									message: 'Profit is required and can\'t be empty'
								},
								greaterThan: {
								    value: 0,
								    message: 'Please enter a value greater than or equal to %s'
								}
							}
						},
						'hotelroomrate_profit_ext[]': {
							message: 'The Profit is not valid',
							validators: {
								notEmpty: {
									message: 'Profit Ext. is required and can\'t be empty'
								},
								greaterThan: {
								    value: 0,
								    message: 'Please enter a value greater than or equal to %s'
								}
							}
						},
						'hotelroomrate_pax[]': {
							message: 'The Pax is not valid',
							validators: {
								// notEmpty: {
								// 	message: 'Pax is required and can\'t be empty'
								// },
								greaterThan: {
								    value: 1,
								    message: 'Please enter a value greater than or equal to %s'
								}
							}
						}
					}
		}).on('success.form.bv',function(e){
			var package_id = $('.packageid').val();
			var action = $('.form_batch_edit').attr('action');
			var baseurl = $('html').attr('base-url');
			var data = $('.form_batch_edit').serializeArray();
			$.post(action,data,function(result)
            {
              result = $.parseJSON(result);
              if(result.status === true)
              {
                $('table#packagehotels_table').find('[packagehotelid='+package_id+']').find('#roomrate_table.table > tbody').load(baseurl + 'panel/package_batch_edit_view/' + package_id);
                data = "";
                $('.batch-wrap .batch-modal').remove();

              }else{
                alert('Failed');
              }
              
            }).fail(function(error){
            });

			e.preventDefault();
		});
		$('.batch_apply_submit').click(function(e){
			var txtFirstNumberValue = document.getElementById('cost_all').value;
			if (txtFirstNumberValue == ""){
			}else{
				var result = parseFloat(txtFirstNumberValue);
				if (!isNaN(result)) {
					var myElements = $("[name='hotelroomrate_cost[]']");
					for (var i=0;i<myElements.length;i++) {
						var total_cost = parseFloat(myElements.eq(i).attr("value")) + parseFloat(result);
						myElements.eq(i).attr("value",total_cost);
					}
				}
			}
			var txtSecondNumberValue = document.getElementById('ex_cost_all').value;
			if (txtSecondNumberValue == ""){
			}else{
				var result2 = parseFloat(txtSecondNumberValue);
				if (!isNaN(result2)) {
					var myElements = $("[name='hotelroomrate_cost_ext[]']");
					for (var i=0;i<myElements.length;i++) {
						var total_cost2 = parseFloat(myElements.eq(i).attr("value")) + parseFloat(result2);
						myElements.eq(i).attr("value",total_cost2);
					}
				}
			}
			var txtFirstNumberValue = document.getElementById('profit_all').value;
			if (txtFirstNumberValue == ""){
			}else{
				var result3 = parseFloat(txtFirstNumberValue);
				if (!isNaN(result3)) {
					var myElements = $("[name='hotelroomrate_profit[]']");
					for (var i=0;i<myElements.length;i++) {
						var total_cost3 = parseFloat(myElements.eq(i).attr("value")) + parseFloat(result3);
						myElements.eq(i).attr("value",total_cost3);
					}
				}
			}
			var txtSecondNumberValue = document.getElementById('ex_profit_all').value;
			if (txtSecondNumberValue == ""){
			}else{
				var result4 = parseFloat(txtSecondNumberValue);
				if (!isNaN(result4)) {
					var myElements = $("[name='hotelroomrate_profit_ext[]']");
					for (var i=0;i<myElements.length;i++) {
						var total_cost4 = parseFloat(myElements.eq(i).attr("value")) + parseFloat(result4);
						myElements.eq(i).attr("value",total_cost4);
					}
				}
			}
		});
		});
</script>