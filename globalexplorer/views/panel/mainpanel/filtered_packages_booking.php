<?php if (isset($pack)): ?>
    <?php foreach ($pack as $package): ?>
        <div class="col-md-4">
            <a href="<?php echo base_url('panel/Frontpagesset/'.$package->id)?>">

            <a href="<?php echo base_url('panel/step_booking/'.$package->id)?>">
                <div class="pax-img">
                    <?php if($package->image_path!=''):?>
                        <div class="pax-fill" style="background-image:url(<?php echo base_url($package->image_path) ?>);"></div>
                    <?php else:?>
                        <div class="pax-fill" style="background-image:url('../img/feat/default.png');"></div>
                    <?php endif; ?>
                </div>
                <p class="pax-details small lead"><?php echo $package->code;?>: <?php echo $package->title;?> (Effective from <?php echo $package->date_from;?> - <?php echo $package->date_to; ?>)</p>
            </a>

            <label style="display:none;">
                <input type="checkbox" name="cbxsetfrontpages[]" value="<?php echo $package->id; ?>">
                Set Image
            </label>
            
        </div>
    <?php endforeach; ?>
<?php endif; ?>