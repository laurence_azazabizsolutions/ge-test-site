<div id="page-wrapper">
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <h3>Pendings Bookings</h3>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Booking Code</th>
                            <th>Details</th>
                            <th>Flight</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php if (isset($booked_data)): ?>
                            <?php foreach ($booked_data as $row): ?>
                                    <tr>
                                        <td>
                                            <a href="<?php echo base_url('panel/voucher/'.$row->BOOKID)?>" target="_blank"><?php echo $row->BOOKINGCODE;?></a> <br>
                                            <a href="<?php echo base_url('panel/package/'.$row->BOOKID)?>">View</a>
                                        </td>
                                        <td>
                                            <strong>Package:</strong> <?php echo $row->PACKAGENAME; ?><br>
                                            <strong>Agent:</strong> <?php echo $row->COMPANYNAME; ?><br>
                                            <strong>Provider:</strong> <?php echo $row->COMPANYNAMEP; ?><br>
                                            <strong>Hotel:</strong> 
                                            <?php 
                                                // $tmp=array();
                                                // foreach ($row->hotel as $key){
                                                //     $tmp[]=$key->hotels_name; 
                                                // }
                                                // echo implode(' ,',$tmp);
                                                $hotelname = $this->booking->getHotelName($row->BOOKINGCODE);
                                                //dump($hotelname);
                                                foreach ($hotelname as $hn) {
                                                echo $hn['name'];
                                                }
                                            ?><br>
                                            <strong>Traveller:</strong> <?php echo $row->TRAVELLLERNAME; ?><br>
                                        </td>
                                        <td>
                                            <strong>Ori:</strong> <?php echo date("d-M-Y", mktime(0,0,0,substr($row->BOOKINGDEPARTUREDATE,5,2), substr($row->BOOKINGDEPARTUREDATE,8,2), substr($row->BOOKINGDEPARTUREDATE,0,4))); ?><br>
                                            <i><?php echo $row->OFDEPARTTIME; ?>-<?php echo $row->OFARRIVALTIME; ?> hrs</i><br>
                                            <strong>Ret:</strong> <?php echo date("d-M-Y", mktime(0,0,0,substr($row->BOOKINGRETURNDATE,5,2), substr($row->BOOKINGRETURNDATE,8,2), substr($row->BOOKINGRETURNDATE,0,4))); ?><br>
                                            <i><?php echo $row->RFDEPARTTIME; ?>-<?php echo $row->RFARRIVALTIME; ?> hrs</i>
                                        </td>
                                        <td><?php echo $row->BOOKINGSTATUS; ?></td>
                                    </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
            <!-- /.table-responsive -->
        </div>
    </div>

  <!--   <div class="row text-center">
        <?php
            //echo $this->pagination->create_links();
        ?>
    </div> -->
  <div id="fucker"class="row text-center">
        <?php
            echo $page_links;
        ?>
    </div> 
</div>
<!-- /#page-wrapper -->