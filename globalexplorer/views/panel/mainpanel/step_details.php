<script src="//oss.maxcdn.com/momentjs/2.8.2/moment.min.js"></script>
<div id="page-wrapper" >
    <input type="hidden" id="base_url_container" value="<?php echo base_url(); ?>">

  
    <!-- /.row -->
    <div class="row">
        <br>
        <div class="col-md-12">
            <div class="stepwizard">
                <div class="stepwizard-row">
                    <div class="stepwizard-step">
                        <button type="button" class="btn btn-default btn-circle" disabled="disabled">1</button>
                        <p>Hotel</p>
                    </div>
                    <div class="stepwizard-step">
                        <button type="button" class="btn btn-default btn-circle" disabled="disabled">2</button>
                        <p>Hotel</p>
                    </div>
                    <div class="stepwizard-step">
                        <button type="button" class="btn btn-orange btn-circle" disabled="disabled">3</button>
                        <p>Details</p>
                    </div> 
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class='col-md-12'>
            <div class="alert alert-success clearfix" id="step_details_sending_email"  style="display:none;">
                <span class="pull-right">
                    <i class="fa fa-spinner fa-spin"></i>
                </span>
                <span class='pull-left'>
                    <strong>Sending Email</strong>
                </span>
            </div>
        </div>
    </div>
    
    <!-- banner / header -->
    <div class="row">
        <div class="col-md-4">
            <?php
                $img = base_url('img/feat/default.png');
                if($package->getImagePath()!='') $img = base_url($package->getImagePath());
              ?>
            <img class="thumbnail img-responsive" src="<?php echo $img; ?>" alt="...">
        </div>
        <div class="col-md-8">
            <h4 style="margin-top:0px;"><?php echo $package->getTitle();?> (Effective from <?php echo date('d-M-Y',strtotime($package->getDateFrom()));?> - <?php echo date('d-M-Y',strtotime($package->getDateTo()));?>)</h4>
            <strong>
                For
                <?php echo (isset($prev_details['adult_count']) && $prev_details['adult_count'] > 0) ? $prev_details['adult_count'] : 0 ;?> Adult, 
                <?php echo (isset($prev_details['child_count']) && $prev_details['child_count'] > 0) ? $prev_details['child_count'] : 0 ;?> Child(ren),
                <?php echo (isset($prev_details['infant_cnt']) && $prev_details['infant_cnt'] > 0) ? $prev_details['infant_cnt'] : 0 ;?> Infant(s).
            </strong><br>
            Total: <?php echo (isset($prev_details['total_holder']) && $prev_details['total_holder'] > 0) ? $prev_details['total_holder'] : 0 ;?> pax<br><br>

            <strong>Departing on <?php echo $prev_details['depart_date']; ?></strong><br>
            <strong>Returning on <?php echo $prev_details['return_date']; ?> </strong><br>
            <?php foreach ($days as $row) 
                {
                    echo 'Package: '.$row->days.' day/s<br>';
                }
            ?>
            Extension: <?php echo $prev_details['extended_days']; ?> day/s<br>
            Total: <?php echo $prev_details['total_days']; ?> day/s <br><br><br>
        </div>
    </div>
    <!-- header / banner -->
    
        
	<!-- traveller details -->
    <div class="row">
        <form id="stepdetails_form" class="form-horizontal" role="form" action="<?php echo base_url('panel/booking'); ?>" method="post">
            <?php
                #dump($booking_data);

                $prev_details = $this->session->userdata("step_booking");
                $prev_count = count($prev_details);
                $str = serialize($prev_details);
                echo " <textarea style=\"display:none;\" name=\"step_booking_serialized\">{$str}</textarea>";
               # dump($prev_details);
            ?>
    
            <div class="col-md-12">
                <!-- start of alert box -->    
                <div class="alert alert-danger" id="step_details_supplementary_errors" style="display:none;" >
                    <p>The number of selected rooms does not match with the total number of visitors!</p>
                    <p>Please select a room(s) before proceeding!</p>
                </div>
                <!-- end of alert box -->
                

                <?php 
                    if(!empty($prev_details["booking_id"]) && $prev_details["booking_id"]!=null){
                ?>
                <!-- guest checker -->
                <div class="panel-group" role="tablist" id="booked_guests_master_list" aria-multiselectable="true" >

                    <div class="panel panel-orange">
                        <div class="panel-heading" role="tab" id="heading1">
                          <h4 class="panel-title">
                            <a data-toggle="collapse"  style="color:white;" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapseOne">
                              Adult Guest List
                            </a>
                          </h4>
                        </div>
                        
                        <div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1">
                          <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered booked_adult_guests">
                                        <tr>
                                            <th width="70px;"><input type="checkbox" id="selectAllAdult"> All</th>
                                            <th>Name</th>
                                            <th width="150px;">DOB</th>
                                            <th>Passport No</th>
                                        </tr>
                                        <?php 
                                            $booked_adult_count = count(@$traveller_details["adult_cnt"]);
                                            if($booked_adult_count){
                                                for($i=0;$i<$booked_adult_count;$i++){
                                                    $booked_adult_name = @$traveller_details["adult_cnt"][$i]["name"];
                                                    $booked_adult_dob = @$traveller_details["adult_cnt"][$i]["date_of_birth"];
                                                    $booked_adult_pp = @$traveller_details["adult_cnt"][$i]["passport_no"];
                                                    $a_dob = date('d-M-Y',strtotime($booked_adult_dob));
                                                    echo "
                                                    <tr>
                                                        <td>
                                                            <input type='checkbox'  onchange='appendStepDetailsDyanmic(this);'  target='adult' name='traveller_data[]' f_name='{$booked_adult_name}' bod='{$booked_adult_dob}' pp='{$booked_adult_pp}'/>
                                                        </td>
                                                        <td>{$booked_adult_name}</td>
                                                        <td>{$a_dob}</td>
                                                        <td>{$booked_adult_pp}</td>
                                                    </tr>
                                                         ";
                                                }
                                            }
                                        ?>
                                    </table>
                                </div>
                          </div>
                        </div>
                    </div>
                    
                    <div class="panel panel-orange">
        
                        <div class="panel-heading" role="tab" id="heading2">
                          <h4 class="panel-title">
                            <a data-toggle="collapse"  style="color:white;" data-parent="#accordion" href="#collapse2" aria-expanded="true" aria-controls="collapse2">
                              Child(ren) Guest List
                            </a>
                          </h4>
                        </div>
                        
                        <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
                          <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered booked_child_guests">
                                        <tr>
                                            <th width="70px;"><input type="checkbox" id="selectAllChild"> All</th>
                                            <th>Name</th>
                                            <th width="150px;">DOB</th>
                                            <th>Passport No</th>
                                        </tr>
                                        
                                        <?php 
                                            $booked_child_count = count(@$traveller_details["child_cnt"]);
                                            if($booked_child_count){
                                                for($i=0;$i<$booked_child_count;$i++){
                                                    $booked_child_name = @$traveller_details["child_cnt"][$i]["name"];
                                                    $booked_child_dob = @$traveller_details["child_cnt"][$i]["date_of_birth"];
                                                    $booked_child_pp = @$traveller_details["child_cnt"][$i]["passport_no"];
                                                    $c_dob = date('d-M-Y',strtotime($booked_child_dob));

                                                    echo "
                                                    <tr>
                                                        <td>
                                                            <input type='checkbox' onchange='appendStepDetailsDyanmic(this);' target='child'  name='traveller_data[]' f_name='{$booked_child_name}' bod='{$booked_child_dob}' pp='{$booked_child_pp}' />
                                                        </td>
                                                        <td>{$booked_child_name}</td>
                                                        <td>{$c_dob}</td>
                                                        <td>{$booked_child_pp}</td>
                                                    </tr>
                                                         ";
                                                }
                                            }
                                        ?>
                                    </table>
                                </div>
                          </div>
                        </div>
                    </div>

                    <div class="panel panel-orange">
        
                        <div class="panel-heading" role="tab" id="heading3">
                          <h4 class="panel-title">
                            <a data-toggle="collapse"  style="color:white;" data-parent="#accordion" href="#collapse3" aria-expanded="true" aria-controls="collapse3">
                              Infant Guest List
                            </a>
                          </h4>
                        </div>
                        
                        <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
                          <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered booked_infant_guests">
                                        <tr>
                                            <th width="70px;"><input type="checkbox" id="selectAllInfant"> All</th>
                                            <th>Name</th>
                                            <th width="150px;">DOB</th>
                                            <th>Passport No</th>
                                        </tr>
                                   
                                            <?php 
                                                $booked_infant_count = count(@$traveller_details["infant_cnt"]);
                                                if($booked_infant_count){
                                                    for($i=0;$i<$booked_infant_count;$i++){
                                                        $booked_infant_name = @$traveller_details["infant_cnt"][$i]["name"];
                                                        $booked_infant_dob = @$traveller_details["infant_cnt"][$i]["date_of_birth"];
                                                        $booked_infant_pp = @$traveller_details["infant_cnt"][$i]["passport_no"];
                                                        $i_dob = date('d-M-Y',strtotime($booked_infant_count));

                                                        echo "
                                                        <tr>
                                                            <td>
                                                                <input type='checkbox' onchange='appendStepDetailsDyanmic(this);' target='infant' name='traveller_data[]' f_name='{$booked_infant_name}' bod='{$booked_infant_dob}' pp='{$booked_infant_pp}' />
                                                            </td>
                                                            <td>
                                                                {$booked_infant_name}
                                                            </td>
                                                            <td>
                                                                {$i_dob}
                                                            </td>
                                                            <td>
                                                                {$booked_infant_pp}
                                                            </td>
                                                        </tr>
                                                             ";
                                                    }
                                                }
                                            ?>
                                    </table>
                                </div>
                          </div>
                        </div>
                    </div>
                </div>
                <!-- end of guest checker -->
                <?php 
                    }
                    $adult_counter_total = 0;
                    $child_counter_total = 0;
                    $infant_counter_total = 0;
                ?>
                        

                <!-- traveller details -->
                <div class="panel panel-orange">
                    <!-- Default panel contents -->
                    <div class="panel-heading text-center"><strong>Traveller Details</strong></div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <tr>
                                    <th></th>
                                    <th>Name</th>
                                    <th width="200px;">DOB</th>
                                    <th>Passport No</th>
                                </tr>

                                    <?php 
                                      //$booked_adult_count = count(@$traveller_details["adult_cnt"]);
                                       if (isset($prev_details['adult_count']) && $prev_details['adult_count'] > 0){
                                           for($i=0;$i<$prev_details['adult_count'];$i++){
                                               $booked_adult_name = @$traveller_details["adult_cnt"][$i]["name"];
                                               $booked_adult_dob = @$traveller_details["adult_cnt"][$i]["date_of_birth"];
                                               $booked_adult_pp = @$traveller_details["adult_cnt"][$i]["passport_no"]; 
                                    ?>
                                                <input  value="<?php echo $booked_adult_name; ?>" name="adult_name1[]" class="form-control min-size-name" type="hidden" placeholder="Name">
                                                <input value="<?php echo $booked_adult_dob; ?>" name="adult_dob1[]" class="form-control min-size-num" type="hidden" placeholder="Y-M-D" data-date-format="YYYY-MM-DD"/> 
                                                <input value="<?php echo $booked_adult_pp; ?>" name="adult_ppno1[]" class="form-control min-size-num" type="hidden" placeholder="12345">
                                    <?php
                                            }
                                        }
                                    ?>
                                
                                <!-- count and loop through adults -->
                                <?php if (isset($prev_details['adult_count']) && $prev_details['adult_count'] > 0): ?>
                                    <?php for ($i=1; $i <= $prev_details['adult_count'] ; $i++) { $adult_counter_total = $i; ?>
                                        <tr class="td_adult_new">
                                            <td>Adult <?php echo $i;?></td>
                                            <td>
                                            	<input   name="adult_name[]" class="form-control min-size-name" type="text" placeholder="Name">
                                                
                                            </td>
                                            <td class='form-group'>
                                                <!-- <input   class="pickadate_DOB_adult form-control min-size-num" type="text" name="adult_dob[]" readonly/> -->
                                                <div class="input-group date" id="datetimepickerDOBAdult">
                                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                    <input class="form-control min-size-num" type="text" placeholder="01-Jan-2016" name="adult_dob[]" data-date-format="DD-MMM-YYYY"/>
                                                   <!--  <input value="<?php echo $booked_adult_dob; ?>" class="form-control min-size-num" type="text" placeholder="Y-M-D" name="adult_dob1[]" data-date-format="YYYY-MM-DD"/> -->
                                                </div>
                                            </td>
                                            <td  class='form-group'>
                                            	<input   name="adult_ppno[]" class="form-control min-size-num" type="text" placeholder="12345">
                                                <!-- <input value="<?php echo $booked_adult_pp; ?>" name="adult_ppno1[]" class="form-control min-size-num" type="text" placeholder="12345"> -->
											</td>
                                        </tr>

                                    

                                    <?php } ?>
                                <?php endif; ?>
                                <!-- count and loop throught adults -->

                                <?php 
                                      //$booked_adult_count = count(@$traveller_details["adult_cnt"]);
                                       if(intVal($prev_details['child_count']) > 0){
                                           for($i=0;$i<$prev_details['child_count'];$i++){
                                               $booked_child_name = @$traveller_details["child_cnt"][$i]["name"];
                                               $booked_child_dob = @$traveller_details["child_cnt"][$i]["date_of_birth"];
                                               $booked_child_pp = @$traveller_details["child_cnt"][$i]["passport_no"]; 
                                    ?>
                                                <input  value="<?php echo $booked_child_name; ?>" name="child_name1[]" class="form-control min-size-name" type="hidden" placeholder="Name">
                                                <input value="<?php echo $booked_child_dob; ?>" name="child_dob1[]" class="form-control min-size-num" type="hidden" placeholder="Y-M-D" data-date-format="YYYY-MM-DD"/> 
                                                <input value="<?php echo $booked_child_pp; ?>" name="child_ppno1[]" class="form-control min-size-num" type="hidden" placeholder="12345">
                                    <?php
                                            }
                                        }
                                    ?>
                                
                                <!-- count the children -->
                                <?php if(intVal($prev_details['child_count']) > 0): ?>
                                    <?php $i=0;?>
                                    <?php for($i=1;$i<=$prev_details['child_count'];$i++){  $child_counter_total = $i; ?>
                                        <tr class="td_child_new">
                                            <td>Child <?php echo $i;?></td>
                                            <td>
                                            	<input  name="child_name[]" class="form-control min-size-name" type="text" placeholder="Child name"></td>
                                            <td>
                                                <!-- <input  class="pickadate_DOB_child form-control min-size-num" type="text" name="child_dob[]" readonly/> -->
                                                 <div class="input-group date" id="datetimepickerDOBChild">
                                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                    <input class="form-control min-size-num" type="text" placeholder="01-Jan-2016" name="child_dob[]" data-date-format="DD-MMM-YYYY"/>
                                                </div>
                                            </td>
                                            <td>
                                            	<input  name="child_ppno[]" class="form-control min-size-num" type="text" placeholder="12345">
											</td>
                                        </tr>
                                    <?php } ?>
                                <?php endif; ?>
                                <!-- end of count the children -->

                                <?php 
                                      //$booked_adult_count = count(@$traveller_details["adult_cnt"]);
                                       if(intVal($prev_details['infant_cnt']) > 0){
                                           for($i=0;$i<intVal($prev_details['infant_cnt']);$i++){
                                               $booked_infant_name = @$traveller_details["infant_cnt"][$i]["name"];
                                               $booked_infant_dob = @$traveller_details["infant_cnt"][$i]["date_of_birth"];
                                               $booked_infant_pp = @$traveller_details["infant_cnt"][$i]["passport_no"]; 
                                    ?>
                                                <!-- <input  value="<?php echo $booked_infant_name; ?>" name="infant_name1[]" class="form-control min-size-name" type="text" placeholder="Name">
                                                <input value="<?php echo $booked_infant_dob; ?>" name="infant_dob1[]" class="form-control min-size-num" type="text" placeholder="Y-M-D" data-date-format="YYYY-MM-DD"/> 
                                                <input value="<?php echo $booked_infant_pp; ?>" name="infant_ppno1[]" class="form-control min-size-num" type="text" placeholder="12345"> -->
                                    <?php
                                            }
                                        }
                                    ?>
            
                                <!-- count the infants -->
                                <?php if(intVal($prev_details['infant_cnt']) > 0): ?>
                                    <?php $i=0;?>
                                    <?php for($i=1;$i<=intVal($prev_details['infant_cnt']);$i++){ $infant_counter_total = $i; ?>
                                        <tr  class="td_infant_new">
                                            <td>Infant <?php echo $i;?></td>
                                            <td>
                                                <input name="infant_name[]" class="form-control min-size-name" type="text" placeholder="Infant name"></td>
                                            <td>
                                                <!-- <input class="pickadate_DOB_infant form-control min-size-num" type="text" name="infant_dob[]" readonly/> -->
                                                <div class="input-group date" id="datetimepickerDOBInfant">
                                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                    <input class="form-control min-size-num" type="text" placeholder="01-Jan-2016" name="infant_dob[]" data-date-format="DD-MMM-YYYY"/>
                                                </div>
                                            </td>
                                            <td>
                                                <input name="infant_ppno[]" class="form-control min-size-num" type="text" placeholder="12345">
                                            </td>
                                        </tr>
                                    <?php } ?>
                                <?php endif; ?>
                                <!-- end of count the infants -->

                            </table>
                        </div>
                    </div>
                </div>
                <!-- end of traveller details -->
                
                <?php 
                   # echo "adult counter : {$adult_counter_total} child counter : {$child_counter_total} infant counter : {$infant_counter_total}";
                   # echo "<br/> booked counter : {$booked_adult_count} child : {$booked_child_count} infant : {$booked_infant_count}";
                ?>  
                             
                <script>
                    var adult_counter = parseInt("<?php echo $adult_counter_total; ?>");
                    var child_counter = parseInt("<?php echo $child_counter_total; ?>");
                    var infant_counter = parseInt("<?php echo $infant_counter_total; ?>");

                    var b_adult_counter = parseInt("<?php echo @$booked_adult_count; ?>");
                    var b_child_counter = parseInt("<?php echo @$booked_child_count; ?>");
                    var b_infant_counter = parseInt("<?php echo @$booked_infant_count; ?>");


                    function appendStepDetailsDyanmic(element){
                        var target = $(element).attr("target");

                        if(target == 'adult')
                            $('#selectAllAdult').attr('checked', false);
                        else if(target == 'child')
                            $('#selectAllChild').attr('checked', false);
                        
                        $(".td_"+target+"_new").find("[name='"+target+"_name[]']").each(function(i,e){
                            $(e).empty().val('');
                        });
                        $(".td_"+target+"_new").find("[name='"+target+"_dob[]']").each(function(i,e){
                            $(e).empty().val('');
                        });
                        $(".td_"+target+"_new").find("[name='"+target+"_ppno[]']").each(function(i,e){
                            $(e).empty().val('');
                        });

                        var new_arr = [];

                        $(".booked_"+target+"_guests").find("[name='traveller_data[]']:checked").each(function(i,e){
                            var name = $.trim($(e).attr("f_name"));
                            var bod = $.trim(moment($(e).attr("bod")).format("DD-MMM-YYYY"));
                            var pp = $.trim($(e).attr("pp"));

                            new_arr.push({f_name:name, bod: bod, pp:pp});
                        });

                        if(new_arr.length){
                            for(var i =0; i<new_arr.length; i++){
                                $(".td_"+target+"_new:eq("+i+")").find("[name='"+target+"_name[]']").val(new_arr[i].f_name);
                                $(".td_"+target+"_new:eq("+i+")").find("[name='"+target+"_dob[]']").val(new_arr[i].bod);
                                $(".td_"+target+"_new:eq("+i+")").find("[name='"+target+"_ppno[]']").val(new_arr[i].pp);
                            }
                        }
                    }
                    $('#selectAllAdult').click(function (e) {
                        if($(this).is(':checked')){
                            console.log(e);
                            var target = 'adult';
                            $(this).closest('table').find('td input:checkbox').prop('checked', this.checked);
                            console.log(<?php echo json_encode(@$traveller_details["adult_cnt"]) ?>);
                            var adults = [];
                            adults = <?php echo json_encode(@$traveller_details["adult_cnt"]) ?>;
                            if(adults.length){
                                for(var i =0; i<adults.length; i++){
                                    $(".td_"+target+"_new:eq("+i+")").find("[name='"+target+"_name[]']").val(adults[i].name);
                                    $(".td_"+target+"_new:eq("+i+")").find("[name='"+target+"_dob[]']").val(moment(adults[i].date_of_birth).format("DD-MMM-YYYY"));
                                    $(".td_"+target+"_new:eq("+i+")").find("[name='"+target+"_ppno[]']").val(adults[i].passport_no);
                                }
                            }
                        }
                        else{
                            var target = 'adult';
                            $(this).closest('table').find('td input:checkbox').prop('checked', this.checked);
                            var adults = [];
                            adults = <?php echo json_encode(@$traveller_details["adult_cnt"]) ?>;
                            if(adults.length){
                                for(var i =0; i<adults.length; i++){
                                    $(".td_"+target+"_new:eq("+i+")").find("[name='"+target+"_name[]']").val('');
                                    $(".td_"+target+"_new:eq("+i+")").find("[name='"+target+"_dob[]']").val('');
                                    $(".td_"+target+"_new:eq("+i+")").find("[name='"+target+"_ppno[]']").val('');
                                }
                            }
                        }
                    });

                    $('#selectAllChild').click(function (e) {
                        if($(this).is(':checked')){
                            console.log(e);
                            var target = 'child';
                            $(this).closest('table').find('td input:checkbox').prop('checked', this.checked);
                            console.log(<?php echo json_encode(@$traveller_details["child_cnt"]) ?>);
                            var children = [];
                            children = <?php echo json_encode(@$traveller_details["child_cnt"]) ?>;
                            if(children.length){
                                for(var i =0; i<children.length; i++){
                                    $(".td_"+target+"_new:eq("+i+")").find("[name='"+target+"_name[]']").val(children[i].name);
                                    $(".td_"+target+"_new:eq("+i+")").find("[name='"+target+"_dob[]']").val(moment(children[i].date_of_birth).format("DD-MMM-YYYY"));
                                    $(".td_"+target+"_new:eq("+i+")").find("[name='"+target+"_ppno[]']").val(children[i].passport_no);
                                }
                            }
                        }
                        else{
                            var target = 'child';
                            $(this).closest('table').find('td input:checkbox').prop('checked', this.checked);
                            var children = [];
                            children = <?php echo json_encode(@$traveller_details["child_cnt"]) ?>;
                            if(children.length){
                                for(var i =0; i<children.length; i++){
                                    $(".td_"+target+"_new:eq("+i+")").find("[name='"+target+"_name[]']").val('');
                                    $(".td_"+target+"_new:eq("+i+")").find("[name='"+target+"_dob[]']").val('');
                                    $(".td_"+target+"_new:eq("+i+")").find("[name='"+target+"_ppno[]']").val('');
                                }
                            }
                        }
                    });

                    $('#selectAllInfant').click(function (e) {
                        if($(this).is(':checked')){
                            console.log(e);
                            var target = 'infant';
                            $(this).closest('table').find('td input:checkbox').prop('checked', this.checked);
                            console.log(<?php echo json_encode(@$traveller_details["infant_cnt"]) ?>);
                            var infants = [];
                            infants = <?php echo json_encode(@$traveller_details["infant_cnt"]) ?>;
                            if(infants.length){
                                for(var i =0; i<infants.length; i++){
                                    $(".td_"+target+"_new:eq("+i+")").find("[name='"+target+"_name[]']").val(infants[i].name);
                                    $(".td_"+target+"_new:eq("+i+")").find("[name='"+target+"_dob[]']").val(moment(infants[i].date_of_birth).format("DD-MMM-YYYY"));
                                    $(".td_"+target+"_new:eq("+i+")").find("[name='"+target+"_ppno[]']").val(infants[i].passport_no);
                                }
                            }
                        }
                        else{
                            var target = 'infant';
                            $(this).closest('table').find('td input:checkbox').prop('checked', this.checked);
                            var infants = [];
                            infants = <?php echo json_encode(@$traveller_details["infant_cnt"]) ?>;
                            if(infants.length){
                                for(var i =0; i<infants.length; i++){
                                    $(".td_"+target+"_new:eq("+i+")").find("[name='"+target+"_name[]']").val('');
                                    $(".td_"+target+"_new:eq("+i+")").find("[name='"+target+"_dob[]']").val('');
                                    $(".td_"+target+"_new:eq("+i+")").find("[name='"+target+"_ppno[]']").val('');
                                }
                            }
                        }
                    });

                </script>
                                
                <!-- start of flight details -->
                <div class="panel panel-orange">
                    <!-- Default panel contents -->
                    <div class="panel-heading text-center"><strong>Flight Details</strong></div>
                    <div class="panel-body">
                        <div class="table-responsive table-responsive-flight-details" style="">
                            <table class="table table-striped table-bordered">
                                <tr>
                                    <th width="150px"></th>
                                    <th colspan="2">Originating Flight <span style="float:right"><?php echo date('d-M-Y', strtotime(@$prev_details["depart_date"])); ?></span></th>
                                    <th colspan="2">Returning Flight <span style="float:right"><?php echo date('d-M-Y', strtotime(@$prev_details["return_date"])); ?></span></th>
                                </tr>
              
                
                                <tr>
                                    <td>Flight Time</td>
                                    <?php
                                        if(@$booking_data["of_departuretime"] === NULL) { $of_departuretime = "00:00"; } else { $of_departuretime = $booking_data["of_departuretime"]; }
                                        if(@$booking_data["of_arrivaltime"] === NULL) { $of_arrivaltime = "00:00"; } else { $of_arrivaltime = $booking_data["of_arrivaltime"]; }
                                        if(@$booking_data["rf_departuretime"] === NULL) { $rf_departuretime = "00:00"; } else { $rf_departuretime = $booking_data["rf_departuretime"]; }
                                        if(@$booking_data["rf_arrivaltime"] === NULL) { $rf_arrivaltime = "00:00"; } else { $rf_arrivaltime = $booking_data["rf_arrivaltime"]; }
                                    ?>

                                    <input style="display:none;" name="flight_of_date_departure1" value="<?php echo $of_departuretime; ?>" type="calendar" data-date-format="YYYY-MM-DD HH:mm:ss" class="form-control" readonly/>
                                    <input style="display:none;" name="flight_of_date_arrival1" value="<?php echo  $of_arrivaltime; ?>" type="calendar" data-date-format="YYYY-MM-DD HH:mm:ss" class="form-control min-size-num" readonly/>
                                    <input style="display:none;" name="flight_rf_date_departure1" value="<?php echo  $rf_departuretime; ?>" type="calendar" data-date-format="YYYY-MM-DD HH:mm:ss" class="form-control min-size-num" readonly/>
                                    <input style="display:none;"name="flight_rf_date_arrival1" value="<?php echo  $rf_arrivaltime; ?>" type="calendar" data-date-format="YYYY-MM-DD HH:mm:ss" class="form-control min-size-num" readonly/>
                                    
                                    <td>
                                        <div class="input-group date datetimepickerTimeOnly" style="min-width:180px;">
                                            <span class="input-group-addon"><span class="fa fa-clock-o"></span></span>
                                            <input name="flight_of_date_departure"  value="<?php $of_depTime = explode(':', $of_departuretime); echo $of_depTime[0].':'.$of_depTime[1]; ?>" type="calendar" data-date-format="HH:mm" class="form-control" readonly/>
                                        </div>
                                    </td>
                                            
                                    <td>
                                        <div class="input-group date datetimepickerTimeOnly" style="min-width:180px;">
                                            <span class="input-group-addon"><span class="fa fa-clock-o"></span></span>
                                            <input name="flight_of_date_arrival"  value="<?php $of_arrTime = explode(':', $of_arrivaltime); echo $of_arrTime[0].':'.$of_arrTime[1]; ?>" type="calendar" data-date-format="HH:mm" class="form-control min-size-num" readonly/>
                                        </div>
                                    </td>

                                    <td>
                                        <div class="input-group date datetimepickerTimeOnly" style="min-width:180px;">
                                            <span class="input-group-addon"><span class="fa fa-clock-o"></span></span>
                                            <input name="flight_rf_date_departure" value="<?php $rf_depTime = explode(':', $rf_departuretime); echo $rf_depTime[0].':'.$rf_depTime[1]; ?>" type="calendar" data-date-format="HH:mm" class="form-control min-size-num" readonly/>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="input-group date datetimepickerTimeOnly" style="min-width:180px;">
                                            <span class="input-group-addon"><span class="fa fa-clock-o"></span></span>
                                            <input name="flight_rf_date_arrival"  value="<?php $rf_arrTime = explode(':', $rf_arrivaltime); echo $rf_arrTime[0].':'.$rf_arrTime[1]; ?>" type="calendar" data-date-format="HH:mm" class="form-control min-size-num" readonly/>
                                        </div>
                                    </td>
                                </tr>

                                <input name="flight_of_no1" value="<?php echo @$booking_data["of_num"]; ?>"  class="form-control" type="hidden" placeholder="12345">
                                <input name="flight_rf_no1" value="<?php echo @$booking_data["rf_num"]; ?>"  class="form-control" type="hidden" placeholder="12345">
                                
                                <tr>
                                    <td>Flight No.</td>
                                    <td colspan="2">
                                    	<input name="flight_of_no" value="<?php echo @$booking_data["of_num"]; ?>"  class="form-control" type="text" placeholder="12345">
									</td>
                                    <td colspan="2">
                                    	<input name="flight_rf_no" value="<?php echo @$booking_data["rf_num"]; ?>"  class="form-control" type="text" placeholder="12345">
                                    </td>
                                </tr>

                                <tr>
                                    <td>From</td>
                                    <td colspan="2">
                                        <select name="flight_of_from" class="form-control" target-select="flight_of_to" onchange="flight_from_of()">
                                            <option value=" ">- Select -</option>
                                            <?php if (isset($airportcodes)): ?>
                                                <?php foreach ($airportcodes as $a => $airportcode):
                                                    $of_from = trim(@$booking_data["of_from"]);
                                                    $tmp_explode = explode(".", $a);
                                                    $tmp_match = trim($tmp_explode[1]); ?>
                                                    <option value="<?php echo $a; ?>" <?php if($tmp_match==$of_from){ echo "selected"; } ?> ><?php echo $airportcode; ?></option>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </select>
                                    </td>
                                    <td colspan="2">
                                        <select name="flight_rf_from" class="form-control" target-select="flight_rf_to" onchange="flight_from_rf()">
                                            <option value=" ">- Select -</option>
                                            <?php if (isset($airportcodes)): ?>
                                                <?php foreach ($airportcodes as $a => $airportcode):
                                                    $rf_from = trim(@$booking_data["rf_from"]);
                                                    $tmp_explode = explode(".", $a);
                                                    $tmp_match = trim($tmp_explode[1]); ?>
                                                    <option value="<?php echo $a; ?>" <?php if($tmp_match==$rf_from){ echo "selected"; } ?>  ><?php echo $airportcode; ?></option>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </select>
                                    </td>
                                </tr>

                                <input type="hidden" name="of_from">
                                <input type="hidden" name="of_from1">
                                <input type="hidden" name="rf_from">
                                <input type="hidden" name="rf_from1">

                                <tr>
                                    <td>To</td>
                                    <td colspan="2">
                                    	<select name="flight_of_to" class="form-control" onchange="flight_to_of()">
                                            <option value=" ">- Select -</option>
                                            <?php if (isset($airportcodes)): ?>
                                                <?php foreach ($airportcodes as $a => $airportcode):
                                                    $of_to = @$booking_data["of_to"];
                                                    $tmp_explode = explode(".", $a);
                                                    $tmp_match = $tmp_explode[1]; ?>
                                                    <option value="<?php echo $a; ?>" <?php if($tmp_match==$of_to){ echo "selected"; } ?> ><?php echo $airportcode; ?></option>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </select>
                                    </td>
                                    <td colspan="2">
	                                    <select name="flight_rf_to" class="form-control" onchange="flight_to_rf()">
                                            <option value=" ">- Select -</option>
                                            <?php if (isset($airportcodes)): ?>
                                                <?php foreach ($airportcodes as $a => $airportcode): 
                                                    $rf_to = trim(@$booking_data["rf_to"]);
                                                    $tmp_explode = explode(".", $a);
                                                    $tmp_match = trim($tmp_explode[1]); ?>
                                                    <option value="<?php echo $a; ?>"  <?php if($tmp_match==$rf_to){ echo "selected"; } ?> ><?php echo $airportcode; ?></option>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </select>
                                    </td>
                                </tr>

                                <input type="hidden" name="of_to">
                                <input type="hidden" name="of_to1">
                                <input type="hidden" name="rf_to">
                                <input type="hidden" name="rf_to1">

                                <input type="hidden" name="flight_remarks1" value="<?php echo @$booking_data["of_remarks"]; ?>">

                                <tr>
                                    <td>Remarks</td>
                                    <td colspan="4"><textarea name="flight_remarks" class="form-control" style="resize:none;" rows="3"><?php echo @$booking_data["of_remarks"]; ?></textarea></td>
                                </tr>
                                
                            </table>
                        </div>
                    </div>
                </div>
				<!-- end of flight details  -->
                
                
                <!-- agent details -->
                <div class="panel panel-orange">
                    <div class="panel-heading text-center"><strong>Agent Details</strong></div>
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered">
                            <tr>
                                <th><input name="agent_details_name" class="form-control" value="<?php echo @$booking_data["agent_name"]; ?>" type="text" placeholder="Full Name"></th>
                                <th><input name="agent_details_no" class="form-control" value="<?php echo @$booking_data["agent_ref"]; ?>"  type="text" placeholder="Reference No."></th>
                            </tr>
                        </table>
                    </div>
                </div>
                <!-- agent details  -->
				
				
				<!-- start of developer mode -->
                <div class="panel panel-orange" style="display:none;">
                    <!-- Default panel contents -->
                    <div class="panel-heading text-center"><strong>ajax returns (remove this during production) </strong></div>
                    <div class="panel-body" id="something_step_details_finalize"> </div>
                </div>
                <!-- end of developer mode -->
                

                <!-- start of book now -->
                <button class="btn btn-orange center-block" id="stepDetailsFinalizeBTN" role="button" onclick="stepDetailsFinalize(this);">Book Now!</button>
                <!-- end of book now -->
            </div>
        </form>
    </div>
    <br>
</div>

<script>
    $(document).ready(function(){
        var of_from = $("[name='flight_of_from']").val();
        var of_from_ = of_from.split('.');
        var rf_from = $("[name='flight_rf_from']").val();
        var rf_from_ = rf_from.split('.');
        var of_to = $("[name='flight_of_to']").val();
        var of_to_ = of_to.split('.');
        var rf_to = $("[name='flight_rf_to']").val();
        var rf_to_ = rf_to.split('.');

        $("[name='of_from']").val(of_from_[1]);
        $("[name='of_from1']").val(of_from_[1]);
        $("[name='rf_from']").val(rf_from_[1]);
        $("[name='rf_from1']").val(rf_from_[1]);

        $("[name='of_to']").val(of_to_[1]);
        $("[name='of_to1']").val(of_to_[1]);
        $("[name='rf_to']").val(rf_to_[1]);
        $("[name='rf_to1']").val(rf_to_[1]);


        <?php 
            if($booking_data){
        ?>  

        <?php
            }else{
        ?>
                $("[name='flight_of_to'] option:first").attr("selected","selected");
                $("[name='flight_rf_to'] option:first").attr("selected","selected");
                $("[name='flight_rf_from'] option:first").attr("selected","selected");
                $("[name='flight_of_from'] option:first").attr("selected","selected");
                //disable returning flights ----------------------------------------------
                //$("[name='flight_of_to'], [name='flight_rf_to']").attr("disabled","disabled");
        <?php
            }
        ?>
    });

    function flight_from_of()
    {
        var a = $('[name="flight_of_from"]').val();
        var e = a.split('.');
        $("[name='of_from']").val(e[1]);
    }
    function flight_from_rf()
    {
        var a = $('[name="flight_rf_from"]').val();
        var e = a.split('.');
        $("[name='rf_from']").val(e[1]);
    }
    function flight_to_of()
    {
        var a = $('[name="flight_of_to"]').val();
        var e = a.split('.');
        $("[name='of_to']").val(e[1]);
    }
    function flight_to_rf()
    {
        var a = $('[name="flight_rf_to"]').val();
        var e = a.split('.');
        $("[name='rf_to']").val(e[1]);
    }
</script>