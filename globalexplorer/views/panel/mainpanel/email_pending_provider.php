<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Global Explorer</title>
 		<style type="text/css">
			h1,
			h4{
				font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
				font-weight: 500;
				line-height: 1.1;
				color: inherit;
			}
		</style>
	</head>
	<body style="color: #333333;">
		<div><img src="<?php echo base_url('img/ge-header.jpg'); ?>" style="width: 100%;height: auto;"></div>
		<h1>This is a reminder about the pending booking. Kindly advice confirmation.</h1>
        <table class="table" style="width: 770px; margin-bottom: 20px; border-collapse: collapse;">
        <?php foreach ($infos as $i) { ?>
        	<tr class="active">
	        	<td style="font-size:15px"><strong>Booking No.</strong></td>
	        	<td style="font-size:12px">:<?php echo $i->bCode?></td>
        	</tr>
        	<tr>
	        	<td style="font-size:15px"><strong>Package</strong></td>
	        	<td style="font-size:12px">:<?php $package = $this->db->get_where('packages',array('id'=>$i->package_id))->result(); echo $package[0]->title.' + Extensions '.$i->extended_days.' NIGHT/S'?></td>
        	</tr>
        	<tr>
	        	<td style="font-size:15px"><strong>Hotel</strong></td>
	        	<td style="font-size:12px">:<?php
		        		$this->db->select('hotels.name');
		        		$this->db->join('booked_hotels','bookings.id=booked_hotels.booking_id');
		        		$this->db->join('package_hotels','booked_hotels.package_hotel_id=package_hotels.id');
		        		$this->db->join('hotels', 'package_hotels.hotel_id=hotels.id');
		        		$hotel = $this->db->from('bookings')->where('bookings.id',$i->bId)->get()->result();
		        		echo $hotel[0]->name;
					?>
	        	</td>
        	</tr>
        	<tr>
	        	<td style="font-size:15px"><strong>Traveller</strong></td>
	        	<td style="font-size:12px">:<?php
		        		$traveller_details = $this->booking->get_booking_info($i->bId);
						echo $traveller_details[0]->name.' ('.count($traveller_details).' PAX)';
	        		?>
	        	</td>
        	</tr>
        	<tr>
        		<td style="font-size:15px"><strong>Travel Date</td>
        		<td style="font-size:12px">:<?php echo date_format(date_create($i->departure_date),"j F Y")?></td>
        	</tr>
        <?php }?>
        </table>
	</body>
</html>