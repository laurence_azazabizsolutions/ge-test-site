<tr packageSurchargeId="<?php echo $surcharge['id']; ?>">
    <td>
        <a href="#" class="edit_packagesurcharge_link" data-target="#myModalSurcharge">Edit</a> | 
        <a href="#">Delete</a>
    </td>
    <?php 
        $cost       = (trim($surcharge['cost'])!='' || $surcharge['cost']!=NULL)    ? $surcharge['cost'] : 0.00;
        $profit     = (trim($surcharge['profit'])!='' || $surcharge['profit']!=NULL)? $surcharge['profit'] : 0.00;
    ?>
    <td><?php echo $surcharge['description']; ?></td>
    <td><?php echo ucwords(str_replace('_', ' ', $surcharge['rule_type'])); ?> : <?php echo $surcharge['rule']; ?></td>
    <td><?php echo $cost; ?></td>
    <td><?php echo $profit; ?></td>
    <td><?php echo ($cost + $profit); ?></td>
    <td><?php echo ucwords(str_replace('/', ' / ', $surcharge['rate_type'])); ?></td>
</tr>
            