<?php //dump_exit($data); ?>
	<tr>
		<th></th>
		<th>Description</th>
		<th>Cost</th>
		<th>Profit</th>
		<th>Price</th>
		<th>Ext. Cost</th>
		<th>Ext. Profit</th>
		<th>Ext. Price</th>
		<th>Rate Type</th>
	</tr>
	<?php
if(!count($data)):
	echo '<tr><td colspan="9">NO DATA FOUND.</tr></td>';
else:
	//dump($data);
	foreach($data as $batch_edit => $value):
?>
	<tr roomrateid="<?php echo $value['id']; ?>">
		<td>
		<a data-target="#myModalRoomRate" class="edit_roomrate_link" href="javascript:void(0);">Edit</a> | 
		<a roomrate-id="<?php echo $value['id']; ?>" onclick="removePackageRoomRate(this);" href="javascript:void(0);">Delete</a> | 
		<a data-target="#myModalRoomRateSurcharge" class="package_hotel_add" href="javascript:void(0);">Add Surcharge</a>
		</td>
		<td>
		<?php 
		if(empty($value['description'])){
			echo ucwords(str_replace('_',' ',$value['room_type']));
		}else{
			echo $value['description'].' - '.ucfirst(str_replace('_',' ',$value['room_type']));
		}
		?>
		</td>
		<td><?php echo $value['cost']; ?></td>
		<td><?php echo $value['profit']; ?></td>		
		<td><?php echo $value['cost'] + $value['profit']; ?></td>
		<td><?php echo $value['ext_cost']; ?></td>
		<td><?php echo $value['ext_profit']; ?></td>
		<td><?php echo $value['ext_cost'] + $value['ext_profit']; ?></td>
		<td><?php echo $value['rate_type']; ?></td>
	</tr>
<?php
	endforeach;
	endif;
?>