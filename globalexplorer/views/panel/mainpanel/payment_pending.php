<div id="page-wrapper">
    <!-- /.row -->
    <div class="row">
        <br>
        <div class="col-lg-12">
            <form class="form-horizontal" role="form" action="" method="post">
                <div class="form-group"><h3>
                    <label class="col-md-4 col-md-offset-2 control-label viewPendingDate"><?php echo $date_month." ".$date_year; ?></label>
                    <div class="col-md-4">
                        <div class="input-group date datetimepickerMonthViewPendingPayment">
                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                            <input class="form-control" type="text" placeholder="Origin" name="origin" value="<?php echo $date_year."-".$date_m; ?>" data-date-format="YYYY-MM"/>
                        </div>
                    </div></h3>
                </div>
            </form>

            <div class="table-responsive">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Booking Code</th>
                            <th>Details</th>
                            <th>Flight</th>
                            <th>Amount</th>
                            <th>Booking Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $totalAmount = 0; $arrMonth = array(); $arrAmount = array();?>
                        <?php foreach($booked_data as $row) : ?>
                            <?php
                                if($row->BOOKINGSTATUS === 'cancelled') {$tr = 'class="danger"';} else {$tr = ''; }

                                $cur_date = date("Y-m");
                                //if ($cur_date !== $date_year."-".$date_m) { $hide_cur_date = 'style="display:none;"'; } else { $hide_cur_date= ''; }
                                if (substr($row->BOOKINGDEPARTUREDATE, 0, 7) !== $date_year."-".$date_m) { $hide_cur_date = 'style="display:none;"'; } else { $hide_cur_date= ''; }
                            ?>
                           
                            <tr <?php echo $tr;?> class="tr-hide <?php echo substr($row->BOOKINGDEPARTUREDATE,0,7); ?>" <?php echo $hide_cur_date; ?>>
                                <td>
                                    <a href="<?php echo base_url('panel/voucher/'.$row->BOOKID)?>" target="_blank"><?php echo $row->BOOKINGCODE;?></a> <br>
                                    <a href="<?php echo base_url('panel/package/'.$row->BOOKID)?>">View</a>
                                </td>
                                <td>
                                    <strong>Package:</strong> <?php echo $row->PACKAGENAME; ?><br>
                                    <strong>Agent:</strong> <?php echo $row->COMPANYNAME; ?><br>
                                    <strong>Provider:</strong> <?php echo $row->COMPANYNAMEP; ?><br>
                                    <strong>Hotel:</strong>
                                    <?php 
                                        $tmp=array();
                                        foreach ($row->hotel as $key){
                                            $tmp[]=$key->hotels_name; 
                                        }
                                        echo implode(' ,',$tmp);
                                    ?><br>
                                    <strong>Traveller:</strong> <?php echo $row->TRAVELLLERNAME; ?>
                                </td>
                                <td>
                                    <strong>Ori:</strong> <?php echo date("d-M-Y", mktime(0,0,0,substr($row->BOOKINGDEPARTUREDATE,5,2), substr($row->BOOKINGDEPARTUREDATE,8,2), substr($row->BOOKINGDEPARTUREDATE,0,4))); ?><br>
                                    <i><?php echo $row->OFDEPARTTIME; ?>-<?php echo $row->OFARRIVALTIME; ?> hrs</i> <br>
                                    <strong>Ret:</strong> <?php echo date("d-M-Y", mktime(0,0,0,substr($row->BOOKINGRETURNDATE,5,2), substr($row->BOOKINGRETURNDATE,8,2), substr($row->BOOKINGRETURNDATE,0,4))); ?><br>
                                    <i><?php echo $row->RFDEPARTTIME; ?>-<?php echo $row->RFARRIVALTIME; ?> hrs</i>
                                </td>
                                <td><?php echo $row->BOOKINGPRICE; ?></td>
                                <td><?php echo $row->BOOKINGSTATUS; ?></td>
                            </tr>
                            <?php $totalAmount += $row->BOOKINGCOST; ?>
                        <?php endforeach; ?>
                    </tbody>
                    <!-- <tfoot>
                        <?php// foreach($booked_data as $row) : ?>
                            <?php //if($row->PDATE === NULL) : ?>
                                <?php
                                    // $count_index = str_replace("-","",substr($row->BOOKINGDEPARTUREDATE,0,7));
                                    // $count_value = count(substr($row->BOOKINGDEPARTUREDATE,0,7));
                                    // $cost = $row->BOOKINGCOST;
                                    // //echo $sum;
                                    // // echo $count_value ."; ";

                                    // array_push($arrMonth, $count_index);
                                    // array_push($arrAmount, $count_index ." => ". $cost);
                                    // //echo "<br>";

                                    // // print_r(array_count_values($arrAmount));
                                    // // echo "<br>";
                                    // //$input = array_map("unserialize", array_unique(array_map("serialize", $arrMonth)));
                                    // //print_r ($arrAmount);
                                    // //$sam = $arrMonth[0];
                                    // //echo "<br>";
                                    // //$sample = array_count_values($arrMonth);
                                    // foreach (array_count_values($arrMonth) as $key => $values) {
                                    //     //echo $key -> $values ."<br>";
                                    //    // echo $key;
                                    //    // echo $values;
                                    // }
                                    // //print_r($arrMonth[201410]);

                                    // //echo "<br>";


   

                                ?>
                                <tr class="tr-hide <?php// echo $count_index; ?>" style="display:none;">
                                    <th><?php //echo $count_index; ?></th>
                                    <th>Total: <span id="totalBookings"> <?php //echo $values; ?></span> Booking(s)</th>
                                    <th></th>
                                    <th> <span id="totalAmount"> <?php //echo $totalAmount;?></span></th>
                                    <th></th>
                                </tr>
                            <?php //endif; ?>
                        <?php// endforeach; ?>
                    </tfoot> -->
                </table>
            </div>

        </div>
    </div>
</div>
<!-- /#page-wrapper -->