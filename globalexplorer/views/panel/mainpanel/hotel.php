<div id="page-wrapper">
    <!-- /.row -->
    <div class="row">
        <br>
        <div class="col-lg-12">
            <form class="form-horizontal" role="form">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Hotel Name</label>
                    <div class="col-sm-8">
                        <select class="form-control">
                            <option>-</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-3">
                        <button type="button" class="btn btn-orange">Filter</button>
                        <button type="reset" class="btn btn-default" data-toggle="modal" data-target="#myModalHotel">Add New</button>
                    </div>
                </div>
            </form>

            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Hotel Name</th>
                            <th>Details</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                AMBA TAIPEI XIMENDING <br>
                                <a href="#" data-toggle="modal" data-target="#myModalHotel">Edit</a> |
                                <a href="#">Delete</a>
                            </td>
                            <td>
                                Room Rates:
                                <a href="#" data-toggle="modal" data-target="#myModalRoomRate">Add</a> <br>
                                <table class="table table-striped table-bordered table-hover">
                                    <tr>
                                        <th></th>
                                        <th>Description</th>
                                        <th>Cost</th>
                                        <th>Profit</th>
                                        <th>Price</th>
                                        <th>Rate Type</th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="#" data-toggle="modal" data-target="#myModalRoomRate">Edit</a> |
                                            <a href="#">Delete</a>
                                        </td>
                                        <td>Single</td>
                                        <td>156</td>
                                        <td>15</td>
                                        <td>171</td>
                                        <td>Room / Night</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="#" data-toggle="modal" data-target="#myModalRoomRate">Edit</a> |
                                            <a href="#">Delete</a>
                                        </td>
                                        <td>Twin</td>
                                        <td>195</td>
                                        <td>15</td>
                                        <td>210</td>
                                        <td>Room / Night</td>
                                    </tr>
                                </table>
                                <br>

                                Room Rate Surcharges:
                                <a href="#" data-toggle="modal" data-target="#myModalRoomRateSurcharge">Add</a> <br>
                                <table class="table table-striped table-bordered table-hover">
                                    <tr>
                                        <th></th>
                                        <th>Description</th>
                                        <th>Rule</th>
                                        <th>Cost</th>
                                        <th>Profit</th>
                                        <th>Price</th>
                                        <th>Rate Type</th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="#" data-toggle="modal" data-target="#myModalRoomRateSurcharge">Edit</a> |
                                            <a href="#">Delete</a>
                                        </td>
                                        <td>Room Surcharge for Fridays</td>
                                        <td>Day of Week: Fri</td>
                                        <td>18</td>
                                        <td>0</td>
                                        <td>18</td>
                                        <td>Room / Night</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="#" data-toggle="modal" data-target="#myModalRoomRateSurcharge">Edit</a> |
                                            <a href="#">Delete</a>
                                        </td>
                                        <td>Room Surcharge for Saturdays</td>
                                        <td>Day of Week: Sat</td>
                                        <td>18</td>
                                        <td>0</td>
                                        <td>18</td>
                                        <td>Room / Night</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="#" data-toggle="modal" data-target="#myModalRoomRateSurcharge">Edit</a> |
                                            <a href="#">Delete</a> |
                                        </td>
                                        <td>Peak Period</td>
                                        <td>Date Range: 16-May-2013#18-May-2013</td>
                                        <td>45</td>
                                        <td>0</td>
                                        <td>45</td>
                                        <td>Room / Night</td>
                                    </tr>
                                </table>
                                <br>

                                Hotel Surcharges:
                                <a href="#" data-toggle="modal" data-target="#myModalHotelSurcharge">Add</a> <br>
                                <table class="table table-striped table-bordered table-hover">
                                    <tr>
                                        <th></th>
                                        <th>Description</th>
                                        <th>Rule</th>
                                        <th>Cost</th>
                                        <th>Profit</th>
                                        <th>Price</th>
                                        <th>Rate Type</th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="#" data-toggle="modal" data-target="#myModalHotelSurcharge">Edit</a> |
                                            <a href="#">Delete</a>
                                        </td>
                                        <td>Peak Period</td>
                                        <td>Date Range: 16-May-2013#18-May-2013</td>
                                        <td>45</td>
                                        <td>0</td>
                                        <td>45</td>
                                        <td>Room / Night</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- /.table-responsive -->
        </div>
    </div>

    <!-- myModalHotel -->
    <div class="modal fade" id="myModalHotel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="pull-right btn btn-danger btn-sm" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="myModalLabel">Hotel:</h4>
          </div>
          <div class="modal-body">
            <form class="form-horizontal" role="form">
              <div class="form-group">
                <label class="col-xs-4 control-label">Hotel Name</label>
                <div class="col-xs-8">
                  <input type="text" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-4 control-label">Website</label>
                <div class="col-xs-8">
                  <input type="url" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-4 control-label">Phone</label>
                <div class="col-xs-8">
                  <input type="text" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-4 control-label">Email</label>
                <div class="col-xs-8">
                  <input type="email" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-4 control-label">Comments</label>
                <div class="col-xs-8">
                  <input type="text" class="form-control">
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <a class="btn btn-orange" href="#" data-dismiss="modal" role="button">Save</a>
          </div>
        </div>
      </div>
    </div>

    <!-- myModalRoomRate -->
    <div class="modal fade" id="myModalRoomRate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="pull-right btn btn-danger btn-sm" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="myModalLabel">Hotel Room Rate:</h4>
          </div>
          <div class="modal-body">
            <form class="form-horizontal" role="form">
              <div class="form-group">
                <label class="col-xs-4 control-label">Rate Name</label>
                <div class="col-xs-8">
                  <select class="form-control">
                    <option>Single</option>
                    <option>Twin</option>
                    <option>Tripple</option>
                    <option>Child With Bed</option>
                    <option>Child Half Twin</option>
                    <option>Child Without Bed</option>
                    <option>Extension</option>
                    <option>Extension (Single)</option>
                    <option>Extension (Twin)</option>
                    <option>Ex Bed</option>
                    <option>Breakfast</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-4 control-label">Rate Type</label>
                <div class="col-xs-8">
                  <select class="form-control">
                    <option>Pax / Night</option>
                    <option>Adult / Night</option>
                    <option>Child / Night</option>
                    <option>Infant / Night</option>
                    <option>Room / Night</option>
                    <option>Pax / Trip</option>
                    <option>Adult / Trip</option>
                    <option>Child / Trip</option>
                    <option>Infant / Trip</option>
                    <option>Room / Trip</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-4 control-label">Cost</label>
                <div class="col-xs-8">
                  <input type="number" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-4 control-label">Profit</label>
                <div class="col-xs-8">
                  <input type="number" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-4 control-label">Pax</label>
                <div class="col-xs-8">
                  <input type="number" class="form-control">
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <a class="btn btn-orange" href="#" data-dismiss="modal" role="button">Save</a>
          </div>
        </div>
      </div>
    </div>

    <!-- myModalRoomRateSurcharge -->
    <div class="modal fade" id="myModalRoomRateSurcharge" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="pull-right btn btn-danger btn-sm" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="myModalLabel">Hotel Room Rate Surcharge:</h4>
          </div>
          <div class="modal-body">
            <form class="form-horizontal" role="form">
              <div class="form-group">
                <label class="col-xs-4 control-label">Description</label>
                <div class="col-xs-8">
                  <input type="text" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-4 control-label">Rule Type</label>
                <div class="col-xs-8">
                  <select class="form-control">
                    <option>Date Range</option>
                    <option>Day of Week</option>
                    <option>Agent</option>
                    <option>Blockout</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-4 control-label">Rule</label>
                <div class="col-xs-8">
                  <select class="form-control">
                    <option>From and To</option>
                    <option>Mon - Sun</option>
                    <option>List of Agent</option>
                    <option>From and To</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-4 control-label">Cost</label>
                <div class="col-xs-8">
                  <input type="number" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-4 control-label">Profit</label>
                <div class="col-xs-8">
                  <input type="number" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-4 control-label">Rate Type</label>
                <div class="col-xs-8">
                  <select class="form-control">
                    <option>Pax / Night</option>
                    <option>Adult / Night</option>
                    <option>Child / Night</option>
                    <option>Infant / Night</option>
                    <option>Room / Night</option>
                    <option>Pax / Trip</option>
                    <option>Adult / Trip</option>
                    <option>Child / Trip</option>
                    <option>Infant / Trip</option>
                    <option>Room / Trip</option>
                  </select>
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <a class="btn btn-orange" href="#" data-dismiss="modal" role="button">Save</a>
          </div>
        </div>
      </div>
    </div>

    <!-- myModalHotelSurcharge -->
    <div class="modal fade" id="myModalHotelSurcharge" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="pull-right btn btn-danger btn-sm" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title" id="myModalLabel">Hotel Surcharge:</h4>
          </div>
          <div class="modal-body">
            <form class="form-horizontal" role="form">
              <div class="form-group">
                <label class="col-xs-4 control-label">Description</label>
                <div class="col-xs-8">
                  <input type="text" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-4 control-label">Rule Type</label>
                <div class="col-xs-8">
                  <select class="form-control">
                    <option>Date Range</option>
                    <option>Day of Week</option>
                    <option>Agent</option>
                    <option>Blockout</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-4 control-label">Rule</label>
                <div class="col-xs-8">
                  <select class="form-control">
                    <option>From and To</option>
                    <option>Mon - Sun</option>
                    <option>List of Agent</option>
                    <option>From and To</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-4 control-label">Cost</label>
                <div class="col-xs-8">
                  <input type="number" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-4 control-label">Profit</label>
                <div class="col-xs-8">
                  <input type="number" class="form-control">
                </div>
              </div>
              <div class="form-group">
                <label class="col-xs-4 control-label">Rate Type</label>
                <div class="col-xs-8">
                  <select class="form-control">
                    <option>Pax / Night</option>
                    <option>Adult / Night</option>
                    <option>Child / Night</option>
                    <option>Infant / Night</option>
                    <option>Room / Night</option>
                    <option>Pax / Trip</option>
                    <option>Adult / Trip</option>
                    <option>Child / Trip</option>
                    <option>Infant / Trip</option>
                    <option>Room / Trip</option>
                  </select>
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <a class="btn btn-orange" href="#" data-dismiss="modal" role="button">Save</a>
          </div>
        </div>
      </div>
    </div>

    <div class="row text-center">
        <ul class="pagination pagination-sm">
            <li><a href="#">&laquo;</a></li>
            <li class="disabled"><a href="#">1</a></li>
            <li class="active"><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li><a href="#">&raquo;</a></li>
        </ul>
    </div>
</div>
<!-- /#page-wrapper -->