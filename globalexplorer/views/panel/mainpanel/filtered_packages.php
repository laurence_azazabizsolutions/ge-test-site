<?php if (isset($packages)):  ?>
    <?php foreach ($packages as $package): ?>
        <div class="col-md-4">
            <a href="<?php echo base_url('panel/package_edit/'.$package->id)?>">
                <div class="pax-img">
                    <?php if($package->image_path!=''):?>
                        <div class="pax-fill" style="background-image:url('<?php echo base_url('../'.$package->image_path) ?>');"></div>
                    <?php else:?>
                        <div class="pax-fill" style="background-image:url('<?php echo base_url('../img/feat/default.png')?>');"></div>
                    <?php endif; ?>
                </div>
                <p class="pax-details small lead"><?php echo $package->code;?>: <?php echo $package->title;?> (Effective from <?php echo $package->date_from;?> - <?php echo $package->date_to; ?>)</p>
            </a>
        </div>
    <?php endforeach; ?>
<?php endif; ?>