<tr packageHotelId="<?php echo $package_hotel->package_hotel_id;?>">
    <td>
        <?php echo strtoupper($package_hotel->name); ?>
        <?php if(trim($package_hotel->location))      echo "<br>".$package_hotel->location; ?>
        <?php if(trim($package_hotel->country))       echo "<br>".$package_hotel->country; ?>
        <?php if(trim($package_hotel->phone))         echo "<br>".$package_hotel->phone;?>
        <?php if(trim($package_hotel->email))         echo "<br>".$package_hotel->email;?>
        <?php if(trim($package_hotel->website))       echo "<br>".$package_hotel->website;?>
        <?php if(trim($package_hotel->description))   echo "<br>".$package_hotel->description;?>
        <br>
        <a href="javascript:void(0);" onclick="removePackageHotel(this);" package_hotel_id="<?php echo $package_hotel->package_hotel_id;?>" package_id="<?php echo $packageid;?>" hotel_id="<?php echo $package_hotel->id;?>">Delete</a>
    </td>
    <td>
        Room Rates:
        <a href="#" class="package_hotel_add" data-target="#myModalRoomRate">Add</a>
        <table id="roomrate_table" class="table table-striped table-bordered table-hover">
            <tr>
                <th></th>
                <th>Description</th>
                <th>Cost</th>
                <th>Profit</th>
                <th>Price</th>
                <th>Ext. Cost</th>
                <th>Ext. Profit</th>
                <th>Ext. Price</th>
                <th>Rate Type</th>
            </tr>
        </table>
        <br>

        Room Rate Surcharges:
        <table id="roomsurcharge_table" class="table table-striped table-bordered table-hover">
            <tr>
                <th></th>
                <th>Description</th>
                <th>Rule</th>
                <th>Cost</th>
                <th>Profit</th>
                <th>Price</th>
                <th>Rate Type</th>
            </tr>
        </table>
    </td>
</tr>
