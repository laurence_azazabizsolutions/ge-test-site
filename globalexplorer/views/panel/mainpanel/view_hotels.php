<div id="page-wrapper">
    <!-- /.row -->
    <div class="row">
        <br>
        <div class="col-lg-12">
            <form class="form-horizontal" role="form" action="<?php echo base_url('panel/filter_hotels_jqry')?>" method="post">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Hotel Name</label>
                    <div class="col-sm-8">
                        <select name="hotel_filter" class="form-control">
                            <option value="">- Select -</option>
                            <?php if (isset($all_hotels)): ?>
                            <?php foreach ($all_hotels as $hotel): ?>
                                <option value="<?php echo $hotel->getId();?>"><?php echo $hotel->getName();?></option>
                             <?php endforeach; ?>
                        <?php endif; ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-3">
                        <button type="button" class="btn btn-orange" id="filter_hotel_btn"> Filter</button>
                        <button type="reset" class="btn btn-default" data-toggle="modal" data-target="#myModalHotel">Add New</button>
                    </div>
                </div>
            </form>

            <div class="table-responsive">
                <table id="hotelTable" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>HOTEL NAME</th>
                            <th>DETAILS</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (isset($hotels)): ?>
                            <?php $this->view('panel/mainpanel/append_hotels', array('hotels' => $hotels));?>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
            <!-- /.table-responsive -->
        </div>
    </div>
    
    <!-- myModalHotel -->
    <div class="modal fade" id="myModalHotel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="pull-right btn btn-danger btn-sm" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Hotel:</h4>
                </div>
                <form id="create_hotel_form" class="form-horizontal" role="form" method="post" action="<?php echo base_url('panel/register_hotel')?>">
                    <div class="modal-body">
                        <div class="alert alert-danger" style="display: none;"></div>
                        <input type="hidden" class="form-control" name="hotel_id" value="">
                        <div class="form-group">
                            <label class="col-xs-4 control-label">Hotel Name</label>
                            <div class="col-xs-8">
                                <input type="text" class="form-control" name="hotel_name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-4 control-label">Country</label>
                            <div class="col-xs-8">
                                <select class="form-control" name="hotel_country">
                                    <option value=" ">- Select -</option>
                                    <?php if (isset($countries)): ?>
                                        <?php foreach ($countries as $country): ?>
                                            <option value="<?php echo $country['code']; ?>"><?php echo $country['country']; ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-4 control-label">Location</label>
                            <div class="col-xs-8">
                                <input type="text" class="form-control" name="hotel_location">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-4 control-label">Website</label>
                            <div class="col-xs-8">
                                <input type="url" class="form-control" name="hotel_website">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-4 control-label">Phone</label>
                            <div class="col-xs-8">
                                <input type="text" class="form-control" name="hotel_phone">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-4 control-label">Email</label>
                            <div class="col-xs-8">
                                <input type="email" class="form-control" name="hotel_email">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-4 control-label">Description</label>
                            <div class="col-xs-8">
                                <textarea class="form-control" style="resize:none;" rows="3" name="hotel_description"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-orange" type="submit" role="button">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- myModalEditHotel -->
    <div class="modal fade" id="myModalEditHotel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="pull-right btn btn-danger btn-sm" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Hotel:</h4>
                </div>
                <form id="edit_hotel_form" class="form-horizontal" role="form" method="post" action="<?php echo base_url('panel/update_hotel')?>">
                    <div class="modal-body">
                        <div class="alert alert-danger" style="display: none;"></div>
                        <input type="hidden" class="form-control" name="hotel_id" value="">
                        <div class="form-group">
                            <label class="col-xs-4 control-label">Hotel Name</label>
                            <div class="col-xs-8">
                                <input type="text" class="form-control" name="hotel_name">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-4 control-label">Country</label>
                            <div class="col-xs-8">
                                <select class="form-control" name="hotel_country">
                                    <option value=" ">- Select -</option>
                                    <?php if (isset($countries)): ?>
                                        <?php foreach ($countries as $country): ?>
                                            <option value="<?php echo $country['code']; ?>"><?php echo $country['country']; ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-4 control-label">Location</label>
                            <div class="col-xs-8">
                                <input type="text" class="form-control" name="hotel_location">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-4 control-label">Website</label>
                            <div class="col-xs-8">
                                <input type="url" class="form-control" name="hotel_website">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-4 control-label">Phone</label>
                            <div class="col-xs-8">
                                <input type="text" class="form-control" name="hotel_phone">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-4 control-label">Email</label>
                            <div class="col-xs-8">
                                <input type="email" class="form-control" name="hotel_email">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-4 control-label">Description</label>
                            <div class="col-xs-8">
                                <textarea  class="form-control" style="resize:none;" name="hotel_description"> </textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-orange" type="submit" role="button">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- myModalHotelSurcharge -->
    <div class="modal fade" id="myModalHotelSurcharge" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="pull-right btn btn-danger btn-sm" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Hotel Surcharge:</h4>
                </div>
                <form id="create_hotelsurcharge_form" class="form-horizontal" role="form" method="post" action="<?php echo base_url('panel/register_hotelsurcharge')?>">
                    <div class="modal-body">
                        <div class="alert alert-danger" style="display: none;"></div>
                        <input type="hidden" class="form-control" name="hotel_id" value="">
                        <input type="hidden" class="form-control" name="hotelsurcharge_id" value="">
                        <div class="form-group">
                            <label class="col-xs-4 control-label">Description</label>
                            <div class="col-xs-8">
                                <input type="text" class="form-control" name="hotelsurcharge_description">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-4 control-label">Rule Type</label>
                            <div class="col-xs-8">
                                <select name="hotelsurcharge_ruletype" class="form-control" id="selRuleType" onchange="ruleType()">
                                    <option value="date_range" SELECTED>Date Range</option>
                                    <option value="day_of_week">Day of Week</option>
                                    <option value="agent">Agent</option>
                                    <option value="blackout">Blackout</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-4 control-label">Rule</label>
                            
                            <!-- Date_Range -->
                            <div class="col-xs-4" id="date_range_1">
                                <input type="text" class="col-xs-4 pickadate_default form-control" id="range_from" name="hotelsurcharge_rule_range_1" style="padding-right:0px;">
                            </div>
                            <div class="col-xs-4" id="date_range_2">
                                <input type="text" class="col-xs-4 pickadate_default form-control" id="range_to" name="hotelsurcharge_rule_range_2" style="padding-right:0px;">
                            </div>


                            <!-- Day_of_Week -->
                            <div class="col-xs-8" id="day_of_week" style="display:none;">
                                <select class="form-control" name="hotelsurcharge_rule_day">
                                    <option value="Monday">Monday</option>
                                    <option value="Tuesday">Tuesday</option>
                                    <option value="Wednesday">Wednesday</option>
                                    <option value="Thursday">Thursday</option>
                                    <option value="Friday">Friday</option>
                                    <option value="Saturday">Saturday</option>
                                    <option value="Sunday">Sunday</option>
                                </select>
                            </div>

                            <!-- Agent -->                    
                            <div class="col-xs-8" id="agent" style="display:none;">
                                <select class="form-control" name="hotelsurcharge_rule_agent">
                                    <option value=" ">- Select -</option>
                                    <?php if (isset($agents)): ?>
                                        <?php foreach ($agents as $agent): ?>
                                            <option value="<?php echo $agent->getUser()->getId();?>"><?php echo $agent->getUser()->getLastname() . ', '. $agent->getUser()->getFirstname(); ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>

                            <!-- Blackout -->
                            <div class="col-xs-4" id="blackout_1" style="display:none;">
                                <input type="text" class="col-xs-4 pickadate_default form-control" id="blackout_from" name="hotelsurcharge_rule_out_1" style="padding-right:0px;">
                            </div>
                            <div class="col-xs-4" id="blackout_2" style="display:none;">
                                <input type="text" class="col-xs-4 pickadate_default form-control" id="blackout_to" name="hotelsurcharge_rule_out_2" style="padding-right:0px;">
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="col-xs-4 control-label">Cost</label>
                            <div class="col-xs-8">
                                <input type="number" step="any" class="form-control" name="hotelsurcharge_cost" onkeypress="return isNumberKey(event)">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-4 control-label">Profit</label>
                            <div class="col-xs-8">
                                <input type="number" step="any" class="form-control" name="hotelsurcharge_profit" onkeypress="return isNumberKey(event)">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-4 control-label">Rate Type</label>
                            <div class="col-xs-8">
                                <select class="form-control" name="hotelsurcharge_ratetype">
                                    <option value="<?php echo $surcharge::RATE_PAX_NIGHT; ?>">Pax / Night</option>
                                    <option value="<?php echo $surcharge::RATE_ADULT_NIGHT; ?>">Adult / Night</option>
                                    <option value="<?php echo $surcharge::RATE_CHILD_NIGHT; ?>">Child / Night</option>
                                    <option value="<?php echo $surcharge::RATE_ROOM_NIGHT; ?>">Room / Night</option>
                                    <option value="<?php echo $surcharge::RATE_PAX_TRIP; ?>">Pax / Trip</option>
                                    <option value="<?php echo $surcharge::RATE_ADULT_TRIP; ?>">Adult / Trip</option>
                                    <option value="<?php echo $surcharge::RATE_CHILD_TRIP; ?>">Child / Trip</option>
                                    <option value="<?php echo $surcharge::RATE_ROOM_TRIP; ?>">Room / Trip</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-orange" type="submit" role="button">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="row text-center">
        <?php
            echo $this->pagination->create_links();
        ?>
    </div>
</div>

<!-- myModalSuccess -->
<div class="modal fade" id="myModalSuccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="pull-right btn btn-danger btn-xs" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <p class="modal-title" id="myModalLabel">&nbsp;</p>
            </div>
        </div>
    </div>
</div>

<!-- /#page-wrapper -->