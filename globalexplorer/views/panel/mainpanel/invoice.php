<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="<?php echo base_url('img/icon.png'); ?>">
        <?php 
        header('Content-type: text/csv');
        header('Content-Disposition: attachment; filename="filename.csv"');
        ?>
        <title>Global Explorer</title>

        <link href="<?php echo base_url('css/bootstrap.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('css/carousel.css'); ?>" rel="stylesheet">
    </head>
    <body>

        <!-- Content 
        ================================================== -->
        <div class="container" style="border:0px solid red; max-width:730px;">
            <br>
            <div class="row">
                <div class="col-xs-3">
                    <img src="<?php echo base_url('img/ge.jpg'); ?>" class="center-block img-responsive">
                </div>
                <div class="col-xs-6">
                    <address>
                        <small>
                            <span class="glyphicon glyphicon-home">&nbsp;</span> 150 South Bridge Road #03-08 Singapore 058727<br>
                            <span class="glyphicon glyphicon-earphone">&nbsp;</span> +65 8451 6666<br>
                            <span class="glyphicon glyphicon-envelope">&nbsp;</span> <a href="mailto:edmundwan@hotmail.com">edmundwan@hotmail.com</a>
                        </small>
                    </address>
                </div>
                <div class="col-xs-3">
                    <p class="text-right">
                        <small>Reg No: 533123713C</small>
                    </p>
                </div>
            </div>

            <hr class="featurette-divider">

            <h3><p class="text-center">INVOICE</p></h3>
            <div class="row">
                <div class="col-xs-6">
                    <p><strong>Prepared For:</strong> GE TRAVEL PTE LTD</p>
                </div>
                <div class="col-xs-6">
                    <p class="text-right">
                        <strong>Invoice No:</strong> GE01964 <br>
                        <strong>Agent Ref No:</strong> <br>
                        <strong>Date:</strong> 03-Aug-2014
                    </p>
                </div>
            </div>

            <h4>Information</h4>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>CHECK IN</th>
                            <th>CHECK OUT</th>
                            <th>HOTEL NAME</th>
                            <th>TOUR CODE</th>
                            <th>GUEST/S</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>09-Aug-2014</td>
                            <td>11-Aug-2014</td>
                            <td>
                                Pratunam City Hotel - Free Wifi
                            </td>
                            <td>
                                123456
                            </td>
                            <td>
                                <p>1. My Name (Adult)</p>
                                <p>2. Your Name (Adult)</p>
                                <p>3. Their Name (Adult)</p>
                                <p>4. His Name (Child)</p>
                                <p>5. Her Name (Infant)</p>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <hr class="featurette-divider">

            <h4>Payment</h4>
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th>ROOM CHARGE</th>
                            <th>QTY</th>
                            <th>TOTAL</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Twin (56 / Pax)</td>
                            <td>2 PAX</td>
                            <td>112</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="2">GRAND TOTAL</th>
                            <th>112</th>
                        </tr>
                    </tfoot>
                </table>
            </div>

            <small>
                <p>
                    <strong>Terms and Conditions:</strong><br>
                    Full Payment by Cash/Cheque or Electronic Transfer / Bank Draft must be made 3 days before group arrival. Otherwise the booking will be cancelled without notice. <br>
                </p>
                <div class="row">
                    <div class="col-xs-6">
                        <p>
                            <small>
                                <strong>Name of Bank:</strong> UOB Bank<br>
                                <strong>Account Name:</strong> Global Explorer<br>
                                <strong>Bank Code:</strong> 7375<br>
                                <strong>Bank Branch:</strong> 016<br>
                                <strong>Account No:</strong> 339-303-316-7<br>
                            </small>
                        </p>
                    </div>
                    <div class="col-xs-6">
                        <p>
                            <small>
                                <strong>Cancellation charge as follows:</strong><br>
                                - 3 days before guests arrival 1 night hotel room charged.<br>
                                - 2 days or less before guests arrival no refund. <br><br>

                                This is a computer generated invoice. No signature is required.
                            </small>
                        </p>
                    </div>
                </div>
            </small>
        </div>
        <!-- /details -->
    </body>
</html>