
<div id="page-wrapper">
<?php 
    $role = trim($this->session->userdata("type"));
?>

    <!-- /.row -->
    <div class="row">
        <br>
        <div class="col-lg-12">


            <!-- Nav tabs -->
            <ul class="nav nav-pills" role="tablist">
                <li class="<?php if($tab=="pending"){ echo "active"; } ?>"><a href="<?php echo base_url('panel/payment?tab=pending') ?>">Pending Payments</a></li>
                <li class="<?php if($tab=="view"){ echo "active"; } ?>"><a href="<?php echo base_url('panel/payment?tab=view') ?>">Collected Bookings</a></li>
                <li class="<?php if($tab=="add"){ echo "active"; } ?>"><a href="<?php echo base_url('panel/payment?tab=add') ?>">Paid Bookings</a></li>
            </ul>
            
            <?php
                $cur_date = date("Y-m");
                $sub_hide= 0;
            ?>
            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane <?php if($tab=="pending"){ echo "active"; } ?>" id="pendingpayments">
                    <br>
                    <form class="form-horizontal" role="form" method="GET">

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Booking Code</label>
                            <div class="col-sm-8">
                                <input type="text" name="booked_code" <?php echo "value='".@$_GET["booked_code"]."'"; ?>class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Passenger Name</label>
                            <div class="col-sm-8">
                                <input type="text" name="name" <?php echo "value='".@$_GET["TRAVELLLERNAME"]."'"; ?>class="form-control">
                            </div>
                        </div>
                
                        <input type="hidden" value="<?php echo $tab; ?>" name="tab">

                        <?php if($role=="admin"){ ?>
                            <!-- start of admin toggle -->
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Agent</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="report_agent_id" onchange="event.preventDefault();">
                                        <option value="">- Select -</option>
                                        <?php if (isset($payment_agent)): ?>
                                            <?php foreach ($payment_agent as $row): ?>
                                                <option value="<?php echo $row->id; ?>" <?php if(isset($_GET["report_agent_id"])){ if($_GET["report_agent_id"]==$row->id){ echo "selected"; } } ?>><?php echo $row->compname; ?></option>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Provider</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="report_provider_id">
                                        <option  value="">- Select -</option>
                                            <?php if (isset($payment_provider)): ?>
                                                <?php foreach ($payment_provider as $row): ?>
                                                    <option value="<?php echo $row->id; ?>" <?php if(isset($_GET["report_provider_id"])){ if($_GET["report_provider_id"]==$row->id){ echo "selected"; } } ?>><?php echo $row->compname; ?></option>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Booking Status</label>
                                <div class="col-sm-8">
                                    <select class="form-control" name="status">
                                        <option value=false <?php if(isset($_GET["status"])){ if($_GET["status"]==""){ echo "selected"; } } ?>>-</option>
                                        <option value="pending" <?php if(isset($_GET["status"])){ if($_GET["status"]=="pending"){ echo "selected"; } } ?>>Pending</option>
                                        <option value="confirmed" <?php if(isset($_GET["status"])){ if($_GET["status"]=="confirmed"){ echo "selected"; } } ?>>Confirmed</option>
                                        <option value="rejected" <?php if(isset($_GET["status"])){ if($_GET["status"]=="rejected"){ echo "selected"; } } ?>>Rejected</option>
                                        <option value="ammended" <?php if(isset($_GET["status"])){ if($_GET["status"]=="ammended"){ echo "selected"; } } ?>>Amended</option>
                                        <option value="cancelled" <?php if(isset($_GET["status"])){ if($_GET["status"]=="cancelled"){ echo "selected"; } } ?>>Cancelled</option>
                                    </select>
                                </div>
                            </div>
                            <!-- end of  admin toggle -->
                        <?php } ?>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Date</label>
                             <div class="col-sm-8">
                                <div class="input-group date" id="datetimepickerMonthViewDate">
                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                    <input class="form-control"   type="text" placeholder="Origin" name="origin" data-date-format="YYYY-MM" <?php if(isset($_GET["origin"])){ if($_GET["origin"]!="" ){ echo "value={$_GET["origin"]}"; }  }else{ echo 'value="'.$date_year.'-'.$date_m.'"';} ?> />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-8 col-sm-offset-3">
                                <button type="submit" class="btn btn-orange">Filter</button>
                                <button type="reset" class="btn btn-default">Clear</button>
                            </div>
                        </div>
                    </form>

                    <h2><?php echo $date_month." ".$date_year; ?></h2>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Booking Code</th>
                                    <th>Details</th>
                                    <th>Flight</th>
                                    <th>Additional Charge</th>
                                    <th>Discount</th>
                                    <th>Amount</th>
                                    <th>Booking Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $totalAmount = 0;?>
                                <?php  foreach($booked_data as $row) {

                                    if($row->BOOKINGSTATUS === 'cancelled')  {$tr = 'class="danger"';} else {$tr = ''; }
                                    if (substr($row->BOOKINGDEPARTUREDATE, 0, 7) !== $date_year."-".$date_m) { 
                                        $hide_cur_date = 'style="display:none;"'; 
                                    } else { 
                                        $hide_cur_date=''; //$totalAmount += $row->BOOKINGPRICE - $row->BOOKINGDISCOUNT;
                                        if($row->BOOKINGSTATUS !== 'cancelled'){$totalAmount += $row->BOOKINGPRICE - $row->BOOKINGDISCOUNT;}

                                        if($row->BOOKINGSTATUS !== 'confirmed') { $chkBtn = 'style="display:none;"'; } else { $chkBtn = ''; $sub_hide++;}
                                        if ($row->BOOKINGSTATUS == "ammended") { $row->BOOKINGSTATUS = "amended"; }
                                    } 
                                ?>
                                    <tr <?php echo $tr; echo $hide_cur_date; ?>>
                                        <td>
                                            <!-- <a href="<?php //echo base_url('panel/voucher/'.$row->BOOKID)?>" target="_blank"><?php //echo $row->BOOKINGCODE;?></a> <br> -->
                                            <a href="#"><?php echo $row->BOOKINGCODE;?></a> <br>
                                            <a href="<?php echo base_url('panel/package/'.$row->BOOKID)?>">View</a> <br> 
                                            <!-- <a href="#">View</a> <br> -->
                                            <a href="#" data-toggle="modal" onClick="discount(<?php echo $row->BOOKID;?>, <?php echo $row->BOOKINGDISCOUNT;?>)">Discount</a>
                                        </td>
                                        <td>
                                            <strong>Package:</strong> <?php echo $row->PACKAGENAME; ?><br>
                                            <strong>Agent:</strong> <?php echo $row->COMPANYNAME; ?><br>
                                            <strong>Provider:</strong> <?php echo $row->COMPANYNAMEP; ?><br>
                                            <strong>Hotel:</strong> 
                                            <?php 
                                                $hotelname = $this->booking->getHotelName($row->BOOKINGCODE);
                                                 //dump($hotelname);
                                                 foreach ($hotelname as $hn) {
                                                     echo $hn['name'];
                                                 }
                                                // $tmp=array();
                                                // foreach ($row->hotel as $key){
                                                //     $tmp[]=$key->hotels_name; 
                                                // }
                                                // echo implode(', ',$tmp);
                                            ?> <br>
                                            <strong>Traveller:</strong> <?php echo $row->TRAVELLLERNAME ." (".$row->PAX." PAX)"; ?>
                                        </td>
                                        <td>
                                            <strong>Ori:</strong> <?php echo date("d-M-Y", mktime(0,0,0,substr($row->BOOKINGDEPARTUREDATE,5,2), substr($row->BOOKINGDEPARTUREDATE,8,2), substr($row->BOOKINGDEPARTUREDATE,0,4))); ?><br>
                                            <i><?php echo $row->OFDEPARTTIME; ?> hrs - <?php echo $row->OFARRIVALTIME; ?> hrs</i> <br>
                                            <strong>Ret:</strong> <?php echo date("d-M-Y", mktime(0,0,0,substr($row->BOOKINGRETURNDATE,5,2), substr($row->BOOKINGRETURNDATE,8,2), substr($row->BOOKINGRETURNDATE,0,4))); ?><br>
                                            <i><?php echo $row->RFDEPARTTIME; ?> hrs - <?php echo $row->RFARRIVALTIME; ?> hrs</i>
                                        </td>
                                        <td>
                                            <?php
                                                echo number_format($row->BOOKINGADDITIONAL, 2, '.', ','); 
                                                $row_total = $row->BOOKINGPRICE + $row->BOOKINGADDITIONAL;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                echo number_format($row->BOOKINGDISCOUNT, 2, '.', ','); 
                                                $row_total = $row_total - $row->BOOKINGDISCOUNT;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                                echo number_format($row_total, 2, '.', ','); 
                                            ?>
                                        </td>
                                        <td><?php echo $row->BOOKINGSTATUS; ?></td>
                                        <td>
                                            <center> <input type="checkbox" <?php echo $chkBtn; ?> value="<?php echo $row->BOOKID.'IamADelimiter'.$row_total;?>" class="sum" name="all_confirmed[]"/> </center>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="4">Total</th>
                                    <th colspan="2"><?php echo number_format($totalAmount, 2, '.', ','); ?></th>
                                    <th>
                                        <center><?php if ($sub_hide > 0) {?> <input type="checkbox" id="CheckAll"/><?php } ?></center>
                                    </th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <?php if ($sub_hide > 0) { echo '<button type="reset" class="btn btn-orange payment_tab_btn" data-toggle="modal" data-target="#myModalPayment">Submit</button>'; } ?>
                    <div class="row text-center"> <?php //echo $add_collection_page_links; ?> </div>

                    <!-- myModalDiscount -->
                    <div class="modal fade" id="myModalDiscount" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class=" pull-right btn btn-danger btn-sm" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Discount:</h4>
                                </div>
                                <form id="append_package_form" class="form-horizontal" role="form" method="post" action="">
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label class="col-xs-4 control-label">Amount</label>
                                            <div class="col-xs-8">
                                                <input type="number" step="any" class="form-control" name="discount" placeholder="Default" onkeypress="return isNumberKey(event)"  value="">
                                                <input type="hidden" name="old_url" value="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-orange">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <br>
                </div>
                
                <div class="tab-pane <?php if($tab=="view"){ echo "active"; } ?>" id="viewcollection">
                    <form class="form-horizontal" role="form" action="" method="post">
                        <div class="form-group"><h3>
                            <label class="col-md-4 col-md-offset-2 control-label viewPendingDate"><?php echo $date_month." ".$date_year; ?></label>
                            <div class="col-md-4">
                                <div class="input-group date datetimepickerMonthViewPendingPayment">
                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                    <input class="form-control" type="text" placeholder="Origin" name="origin" value="<?php echo $date_year."-".$date_m; ?>" data-date-format="YYYY-MM"/>
                                </div>
                            </div></h3>
                        </div>
                    </form>
                    <br>
                    <?php
                        $cnt_cbook = 0;
                        foreach($booked_data_collection as $row){
                            if ($row->PTYPE === "cash") { $ptype = "Cash: ". number_format($row->PTOT, 2, '.', ',') ." SGD"; }
                            else { $ptype = ucfirst($row->PTYPE) .": ". number_format($row->PTOT, 2, '.', '') ." SGD [". $row->PDESC ."]"; }
                            $view_total = 0;

                            if ($row->PAYSTAT === "collected") {
                                if ($cur_date !== $date_year."-".$date_m) { $hide_cur_date = 'style="display:none;"'; } else { $hide_cur_date= ''; $cnt_cbook++; }
                    ?>
                                <div class="tr-hide <?php echo substr($row->PAYDATE,0,7); ?>" <?php echo $hide_cur_date; ?>>
                                    <h4><?php echo $ptype; ?></h4>
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Booking Code</th>
                                                    <th>Details</th>
                                                    <th>Flight</th>
                                                    <th>Amount</th>
                                                    <th>Date Collected</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="append_payment_collections">
                                                <?php foreach($row->booked as $booked_row){
                                                  /*  $view_total += $booked_row->BOOKINGCOST; */
                                                    $view_total_diff = $booked_row->BOOKINGPRICE + $booked_row->BOOKINGADDITIONAL - $booked_row->BOOKINGDISCOUNT; 
                                                    $view_total += $view_total_diff; 
                                                    ?>
                                                    <?php  if($booked_row->BOOKINGSTATUS === 'cancelled')  {$tr = '';} else {$tr = ''; }?>
                                                    <tr <?php echo $tr;?> >
                                                        <td>
                                                            <!-- <a href="<?php //echo base_url('panel/voucher/'.$booked_row->BOOKID)?>" target="_blank"><?php //echo $booked_row->BOOKINGCODE;?></a> <br> -->
                                                            <a href="#"><?php echo $booked_row->BOOKINGCODE;?></a> <br>
                                                            <!-- <a href="<?php //echo base_url('panel/package/'.$booked_row->BOOKID)?>">View</a><br> -->
                                                            <a href="#">View</a><br>
                                                            <a href="#" data-toggle="modal" onClick="difference(<?php echo $booked_row->BOOKID;?>,<?php echo $booked_row->BOOKINGDIFFERENCE; ?>)">Difference</a>
                                     
                                                        </td>
                                                        <td>
                                                            <strong>Package:</strong><?php echo $booked_row->PACKAGENAME; ?><br>
                                                            <strong>Agent:</strong> <?php echo $booked_row->COMPANYNAME; ?><br>
                                                            <strong>Provider:</strong> <?php echo $booked_row->COMPANYNAMEP; ?><br>
                                                            <strong>Hotel:</strong>
                                                            <?php 
                                                                print_r($booked_row->hotel[0]['name']);
                                                                // $tmp=array();
                                                                // foreach ($booked_row->hotel as $key){
                                                                //     $tmp[]=$key->hotels_name; 
                                                                // }
                                                                // echo implode(', ',$tmp);
                                                            ?> <br>
                                                            <strong>Traveller:</strong> <?php echo $booked_row->TRAVELLLERNAME ." (".$booked_row->PAX." PAX)"; ?>
                                                        </td>
                                                        <td>
                                                            <strong>Ori:</strong> <?php echo date("d-M-Y", mktime(0,0,0,substr($booked_row->BOOKINGDEPARTUREDATE,5,2), substr($booked_row->BOOKINGDEPARTUREDATE,8,2), substr($booked_row->BOOKINGDEPARTUREDATE,0,4))); ?><br>
                                                            <i><?php echo $booked_row->OFDEPARTTIME; ?>-<?php echo $booked_row->OFARRIVALTIME; ?> hrs</i> <br>
                                                            <strong>Ret:</strong> <?php echo date("d-M-Y", mktime(0,0,0,substr($booked_row->BOOKINGRETURNDATE,5,2), substr($booked_row->BOOKINGRETURNDATE,8,2), substr($booked_row->BOOKINGRETURNDATE,0,4))); ?><br>
                                                            <i><?php echo $booked_row->RFDEPARTTIME; ?>-<?php echo $booked_row->RFARRIVALTIME; ?> hrs</i>
                                                        </td>
                                                        <!-- <td><?php //echo number_format($booked_row->BOOKINGCOST, 2, '.', ','); ?></td> -->
                                                        <td><?php echo number_format($view_total_diff, 2, '.', ','); ?></td>
                                                        <td><?php echo $booked_row->PDATE; ?></td>
                                                        <td>
                                                            <input type="checkbox" value="<?php echo $booked_row->BOOKID.'IamADelimiter'.$booked_row->BOOKINGCOST;?>" class="btn" name="chkCollected[]"/>
                                                        </td>
                                                    </tr>
                                                 <?php } ?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th colspan="3">Total</th>
                                                    <th colspan="3"><?php echo number_format($view_total, 2, '.', ','); ?></th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                    <?php   
                            }
                        }

                        if ($cnt_cbook > 0) {
                    ?>
                            <button type="reset" class="btn btn-orange payment_tab_btn" data-toggle="modal" data-target="#myModalPayment">Submit</button>
                    <?php } ?>
                </div>
                    <!-- myModalDifference -->
                    <div class="modal fade" id="myModalDifference" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class=" pull-right btn btn-danger btn-sm" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Difference:</h4>
                                </div>
                                <form id="append_package_form" class="form-horizontal" role="form" method="post" action="">
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label class="col-xs-4 control-label">Amount</label>
                                            <div class="col-xs-8">
                                                <input type="number" step="any" class="form-control" name="difference" placeholder="Default" onkeypress="return isNumberKey(event)"  value="">
                                                <input type="hidden" name="old_urls" value="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-orange">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                <div class="tab-pane <?php if($tab=="add"){ echo "active"; } ?>" id="addcollection">
                    <form class="form-horizontal" role="form" action="" method="post">
                        <div class="form-group"><h3>
                            <label class="col-md-4 col-md-offset-2 control-label viewPendingDate"><?php echo $date_month." ".$date_year; ?></label>
                            <div class="col-md-4">
                                <div class="input-group date datetimepickerMonthViewPendingPayment">
                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                    <input class="form-control" type="text" placeholder="Origin" name="origin" value="<?php echo $date_year."-".$date_m; ?>" data-date-format="YYYY-MM"/>
                                </div>
                            </div></h3>
                        </div>
                    </form>
                    <br>
                    <?php
                        if ($cur_date !== $date_year."-".$date_m) { $hide_cur_date = 'style="display:none;"'; } else { $hide_cur_date= ''; }

                        foreach($booked_data_collection as $row){
                            if ($row->PTYPE === "cash") {
                                $ptype = "Cash: ". number_format($row->PTOT, 2, '.', ',') ." SGD";
                            }
                            else {
                                $ptype = ucfirst($row->PTYPE) .": ". number_format($row->PTOT, 2, '.', ',') ." SGD [". $row->PDESC ."]";
                            }
                            $view_total = 0;

                            if ($row->PAYSTAT === "paid") {
                    ?>
                                <div class="tr-hide <?php echo substr($row->PAYDATE,0,7); ?>" <?php echo $hide_cur_date; ?>>
                                    <h4><?php echo $ptype; ?></h4>
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Booking Code</th>
                                                    <th>Details</th>
                                                    <th>Flight</th>
                                                    <th>Amount</th>
                                                    <th>Date Paid</th>
                                                </tr>
                                            </thead>
                                            <tbody id="append_payment_collections">
                                                <?php foreach($row->booked as $booked_row){
                                                    $view_total += $booked_row->BOOKINGCOST; ?>
                                                    <?php  if($booked_row->BOOKINGSTATUS === 'cancelled')  {$tr = '';} else {$tr = ''; }?>
                                                    <tr <?php echo $tr;?> >
                                                        <td>
                                                           <!--  <a href="<?php //echo base_url('panel/voucher/'.$booked_row->BOOKID)?>" target="_blank"><?php //echo $booked_row->BOOKINGCODE;?></a> <br>
                                                            <a href="<?php //echo base_url('panel/package/'.$booked_row->BOOKID)?>">View</a> -->
                                                            <a href="#"><?php echo $booked_row->BOOKINGCODE;?></a> <br>
                                                            <a href="#">View</a>
                                                        </td>
                                                        <td>
                                                            <strong>Package:</strong><?php echo $booked_row->PACKAGENAME; ?><br>
                                                            <strong>Agent:</strong> <?php echo $booked_row->COMPANYNAME; ?><br>
                                                            <strong>Provider:</strong> <?php echo $booked_row->COMPANYNAMEP; ?><br>
                                                            <strong>Hotel:</strong>
                                                            <?php 
                                                                 $hotelname = $this->booking->getHotelName($booked_row->BOOKINGCODE);
                                                                    //dump($hotelname);
                                                                    foreach ($hotelname as $hn) {
                                                                    echo $hn['name'];
                                                                    }
                                                            ?> <br>
                                                            <strong>Traveller:</strong> <?php echo $booked_row->TRAVELLLERNAME ." (".$booked_row->PAX." PAX)"; ?>
                                                        </td>
                                                        <td>
                                                            <strong>Ori:</strong> <?php echo date("d-M-Y", mktime(0,0,0,substr($booked_row->BOOKINGDEPARTUREDATE,5,2), substr($booked_row->BOOKINGDEPARTUREDATE,8,2), substr($booked_row->BOOKINGDEPARTUREDATE,0,4))); ?><br>
                                                            <i><?php echo $booked_row->OFDEPARTTIME; ?>-<?php echo $booked_row->OFARRIVALTIME; ?> hrs</i> <br>
                                                            <strong>Ret:</strong> <?php echo date("d-M-Y", mktime(0,0,0,substr($booked_row->BOOKINGRETURNDATE,5,2), substr($booked_row->BOOKINGRETURNDATE,8,2), substr($booked_row->BOOKINGRETURNDATE,0,4))); ?><br>
                                                            <i><?php echo $booked_row->RFDEPARTTIME; ?>-<?php echo $booked_row->RFARRIVALTIME; ?> hrs</i>
                                                        </td>
                                                        <td><?php echo number_format($booked_row->BOOKINGCOST, 2, '.', ','); ?></td>
                                                        <td><?php echo $booked_row->PDATE; ?></td>
                                                    </tr>
                                                 <?php } ?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th colspan="3">Total</th>
                                                    <th colspan="2"><?php echo number_format($view_total, 2, '.', ','); ?></th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                    <?php }} ?>
                </div>
            </div>
            <br>
        </div>
    </div>

    <div class="modal fade" id="myModalPayment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Total Amount: <span id="result_collection" ></span><span id="result_collections" ></span> SGD</h4>
                </div>
                <form id="login_form" class="form-horizontal" role="form" action="<?php echo base_url('panel/agent_payment');  ?>" method="post">
                    <input type="hidden" id="arr_res" value="" name="arr_res">
                    <input type="hidden" id="result_total_payment" name="result_total_payment" value="">
                    <input type="hidden" id="result_total_payments"  value="">
                    <input type="hidden" name="tabVal" value="<?php echo $tab; ?>">
                    <div class="modal-body">
                        <div class="radio"><label style="display: block;"><input type="radio" name="radPaymenttype" id="rdnCash" value="cash" checked> Cash </label><input type="hidden" class="form-control" name="description" value="cash" id="txtCash" disabled/></div>
                        <div class="radio"><label style="display: block;"><input type="radio" name="radPaymenttype" id="rdnCheque" value="cheque"> Cheque </label><input type="text" class="form-control" name="description" value="" id="txtCheque" disabled/></div>
                        <div class="radio"><label style="display: block;"><input type="radio" name="radPaymenttype" id="rdnOther" value="others"> Others </label><input type="text" class="form-control" name="description" value="" id="txtOther" disabled/></div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-orange">Save</button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>
<!-- /#page-wrapper -->