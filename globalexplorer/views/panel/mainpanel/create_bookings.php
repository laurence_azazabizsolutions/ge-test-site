<div id="page-wrapper">
    <!-- /.row -->
    <div class="row">
        <br>
        <div class="col-md-12">
            <div class="stepwizard">
                <div class="stepwizard-row">
                    <div class="stepwizard-step">
                        <button type="button" class="btn btn-orange btn-circle" disabled="disabled">1</button>
                        <p>Package</p>
                    </div>
                    <div class="stepwizard-step">
                        <button type="button" class="btn btn-default btn-circle" disabled="disabled">2</button>
                        <p>Hotel</p>
                    </div>
                    <div class="stepwizard-step">
                        <button type="button" class="btn btn-default btn-circle" disabled="disabled">3</button>
                        <p>Details</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <ul class="nav nav-pills dest_ hide_package_view_display_tabs" role="tablist">
            <!--this is for the tabs-->
            <!--this is for the tabs-->
            <!--this is for the tabs-->
                <?php
                    if (isset($destinations )) {
                        $dest_act = false;
                        foreach ($destinations as $destination) {
                             if ($this->uri->segment(3) === $destination['code']) {
                                // echo '<li class="active"><a href="#'.$destination['code'].'" id="'.$destination['code'].'" onClick="change_pkg_views(\''.$destination['code'].'\',this)" role="tab" data-toggle="tab">'.$destination['country'].'</a></li>';
                                echo '<li class="active"><a href="'.base_url('panel/create_bookings').'/'.$destination['code'].'" >'.$destination['country'].'</a></li>';
                                $dest_act = true;
                            } else { echo '<li><a href="'.base_url('panel/create_bookings').'/'.$destination['code'].'" >'.$destination['country'].'</a></li>'; }
                            // } else { echo '<li><a href="#'.$destination['code'].'" id="'.$destination['code'].'" onClick="change_pkg_views(\''.$destination['code'].'\',this)" role="tab" data-toggle="tab">'.$destination['country'].'</a></li>'; }

                        }

                    }
                ?>
<input type="hidden" id="userdata_id" name="userdata_id" value="<?php echo $user_id; ?>">

<input type="hidden" id="first_code" value="<?php echo $first_code; ?>" >


            </ul>
            <!-- <form class="form-horizontal hide_package_view_display_selects" role="form" name="form">
                <div class="form-group">
                    <label class="col-sm-3 control-label text-right">Destination</label>
                    <div class="col-sm-8">
                        <select class="form-control" name="package_destination" >
                            <option value=" ">- Select -</option>
                            <?php if (isset($destinations)): ?>
                                <?php foreach ($destinations as $destination): ?>
                                    <option value="<?php echo $destination['code']; ?>" ><?php echo $destination['country']; ?></option>
                                <?php  endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>
            </form> -->

            <form class="form-horizontal hide_package_view_display_selects" role="form" name="form">
                <div class="form-group">
                    <label class="col-sm-3 control-label text-right">Destination</label>
                    <div class="col-sm-8">
                        <select class="form-control"  onChange="changeCodes(this.value)">
                            <option value=" ">- Select -</option>
                            <?php if (isset($destinations)): ?>
                                <?php foreach ($destinations as $destination): ?>
                                    <?php if ($this->uri->segment(3) === $destination['code']) { ?>
                                        <option value="<?php echo $destination['code']; ?>" selected><?php echo $destination['country']; ?></option>
                                    <?php 
                                        } 
                                        else
                                        {
                                    ?>
                                        <option value="<?php echo $destination['code']; ?>" ><?php echo $destination['country']; ?></option>
                                    <?php } ?>

                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>
            </form>
            <br>
            <div id="displays_package_data">
                <?php if (isset($packages)): ?>

                    <?php foreach($packages as $package): ?>
                        <div class="col-md-4">
                            <a href="<?php echo base_url('panel/step_booking/'.$package->id)?>">
                                <div class="pax-img">
                                    <?php if($package->image_path!=''):?>

                                        <div class="pax-fill" style="background-image:url('<?php echo base_url($package->image_path) ?>');"></div>


                                        <div class="pax-fill" style="background-image:url('<?php echo base_url($package->image_path) ?>');"></div>

                                    <?php else:?>
                                        <div class="pax-fill" style="background-image:url('<?php echo base_url('img/feat/default.png')?>');"></div>
                                    <?php endif; ?>
                                </div>
                                <p class="pax-details small lead"><?php echo $package->code;?>: <?php echo $package->title;?> (Effective from <?php echo $package->date_from;?> - <?php echo $package->date_to; ?>)</p>
                            </a>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <div class="row text-center">
        <?php
            echo $paging;
        ?>
    </div>
</div>
<!-- /#page-wrapper -->