<div id="page-wrapper">
    <!-- /.row -->
    <div class="row">
        <br>
        <div class="col-md-4">
            <?php
                $img = base_url('img/feat/default.png');
                if($package->getImagePath()!='') $img = base_url($package->getImagePath());
              ?>
            <img class="thumbnail img-responsive" src="<?php echo $img; ?>" alt="...">
        </div>
        <div class="col-md-8">
            <h4 style="margin-top:0px;"><?php echo $package->getTitle();?> (Effective from <?php echo $package->getDateFrom();?> - <?php echo $package->getDateTo();?>)</h4>
            <strong>For <?php echo (isset($prev_details['adult_count']) && $prev_details['adult_count'] > 0) ? $prev_details['adult_count'] : 0 ;?> Adult, <?php if(isset($prev_details['child_date']) && !empty($prev_details['child_date'])): ?> <?php echo count($prev_details['child_date']['toddler_data']);?> Child(ren), <?php echo count($prev_details['child_date']['infant_data']);?> Infant(s)<?php else:?>0 Children<?php endif;?></strong><br>
            Total: <?php echo (isset($prev_details['total_holder']) && $prev_details['total_holder'] > 0) ? $prev_details['total_holder'] : 0 ;?> pax<br><br>

            <strong>Departing on <?php echo $prev_details['depart_date']; ?></strong><br>
            <strong>Returning on <?php echo $prev_details['return_date']; ?> </strong><br>
            Package: <?php echo $package->getDays(); ?> day/s<br>
            Extension: <?php echo $prev_details['extended_days']; ?> day/s<br>
            Total: <?php echo $prev_details['total_days']; ?> day/s <br><br><br>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <form id="stephotel_form" class="form-horizontal" role="form" action="<?php echo base_url('panel/step_details'); ?>" method="post">
                <div class="panel panel-orange">
                    <!-- Default panel contents -->
                    <div class="panel-heading text-center"><strong>Package Hotel</strong></div>
                    <div id="rooms_div" class="panel-body">
                        <div class="form-group">
                            <label class="col-md-4 control-label">Hotel Choice :</label>
                            <div class="col-md-5">
                                <select class="form-control" name="package_hotel_choice">
                                    <option value=" ">- Select -</option>
                                    <?php if (isset($package_hotels)): ?>
                                        <?php foreach ($package_hotels as $package_hotel): ?>
                                            <option value="<?php echo $package_hotel['package_hotel_id']; ?>"><?php echo $package_hotel['name']; ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>

                        <!-- Table -->
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <tr>
                                    <th class="brown text-center" colspan="3">Package</th>
                                </tr>
                            </table>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <tr>  
                                    <th class="brown text-center" colspan="3">Rooms Only</th>
                                </tr>
                            </table>
                        </div>

                    </div>
                </div>

                <div class="panel panel-orange">
                    <!-- Default panel contents -->
                    <div class="panel-heading text-center"><strong>Optional Tours / Add Ons</strong></div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <tr>
                                    <th>Description</th>
                                    <th>Price</th>
                                    <th>Qty</th>
                                </tr>
                                <?php if(isset($addons)):?>
                                    <?php foreach ($addons as $addon): ?>    
                                        <tr>
                                            <td><?php echo $addon['description']; ?></td>
                                            <td>
                                                <?php $price = ($addon['cost'] + $addon['profit']); echo $price; ?> / <?php echo $addon['unit']; ?>
                                                <input name="addon_id[]" type="hidden" value="<?php echo $addon['id']; ?>">
                                                <input name="addon_price[]" type="hidden" value="<?php echo $price; ?>">
                                            </td>
                                            <td><input name="addon_quantity[]" class="form-control min-size-num" type="number"></td>
                                        </tr>
                                    <?php endforeach;?>
                                <?php endif;?>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="panel panel-orange">
                    <!-- Default panel contents -->
                    <div class="panel-heading text-center"><strong>Additional Hotel Arrangments</strong></div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <tr>
                                    <th></th>
                                    <th>Hotel Name</th>
                                    <th colspan="2">Remarks</th>
                                </tr>
                                <tr>
                                    <td>Hotel 2</td>
                                    <td>
                                        <select name="exhotel_name" class="form-control min-size-name">
                                            <option value=" ">- Select -</option>
                                        </select>
                                    </td>
                                    <td>
                                        <div class="input-group date" id="datetimepickerCheckIn">
                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                            <input name="exhotel_checkin" type="calendar" data-date-format="YYYY-MM-DD" class="form-control min-size-num" readonly/>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="input-group date" id="datetimepickerCheckOut">
                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                            <input name="exhotel_checkout" type="calendar" data-date-format="YYYY-MM-DD" class="form-control min-size-num" readonly/>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Remarks</td>
                                    <td colspan="3"><textarea name="exhotel_remarks" class="form-control" style="resize:none;" rows="3"></textarea></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <button class="btn btn-orange center-block" role="button">Continue</button>
            </form>
        </div>
    </div>
    <br>
</div>
<!-- /#page-wrapper -->