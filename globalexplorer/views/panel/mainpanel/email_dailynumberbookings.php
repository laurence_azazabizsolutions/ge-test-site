<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Global Explorer</title>
 		<style type="text/css">
			h1,
			h4{
				font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
				font-weight: 500;
				line-height: 1.1;
				color: inherit;
			}
			td{
				word-break:break-all;
			}
			table {
		    border: 1px solid #ccc;
		    width: 100%;
		    margin:0;
		    padding:0;
		    border-collapse: collapse;
		    border-spacing: 0;
		  }

		  table tr {
		    border: 1px solid #ddd;
		    padding: 5px;
		  }

		  table th, table td {
		    padding: 10px;
		    text-align: center;
		  }

		  table th {
		    text-transform: uppercase;
		    font-size: 14px;
		    letter-spacing: 1px;
		  }

		  @media screen and (max-width: 600px) {

		    table {
		      border: 0;
		    }

		    table thead {
		      display: none;
		    }

		    table tr {
		      margin-bottom: 10px;
		      display: block;
		      border-bottom: 2px solid #ddd;
		      border-top: 2px solid #E26C0A;
		    }

		    table td {
		      display: block;
		      text-align: right;
		      font-size: 13px;
		      border-bottom: 1px dotted #ccc;
		    }

		    table td:last-child {
		      border-bottom: 0;
		    }

		    table td:before {
		      content: attr(data-label);
		      float: left;
		      text-transform: uppercase;
		      font-weight: bold;
		    }
		  }
		</style>
	</head>
	<body style="color: #333333;"> 
		<div><img src="<?php echo base_url('img/ge-header.jpg'); ?>" style="width: 100%;height: auto;"></div>
		<h2 style="color:#e95a3e">Global Explorer System Notification</h2><hr>
		<!-- <h3>Total number of confirmed bookings for today is <?php //echo count($confirmed)?></h3> -->
		<p><h2>CONFIRMED BOOKINGS</h2><span><h3>TOTAL COUNT: <?php echo count($confirmed)?></h3></span></p>
		<table>
		<thead>
			<tr>
				<th style="text-align:center;"><h3>Booking Code</h3></th>
				<th style="text-align:center;"><h3>Company</h3></th>
				<th style="text-align:center;"><h3>Agent</h3></th>
				<!-- <th style="text-align:center;"><h3>Passenger Details</h3></th>
				<th colspan="2" style="text-align:center;"><h3>Flight Details</h3></th>
				<th style="text-align:center;"><h3>Hotel Details</h3></th> -->
			</tr>
		</thead>
		<?php if(count($confirmed) > 0){
			$this->load->model('booking/booking');
        	$this->load->model('booking/bookingstorage');
			foreach($confirmed as $dl){
				$guest=$this->booking->getByGuestVoucher($dl->id);
				$travel=$this->booking->getBysummary($dl->id);
				$hotel=$this->booking->getBysummaryRoomtype($dl->id);
				$invoice_pdf=$this->booking->getBysummary($dl->id);
				$destination_summary = $this->booking->getSummaryDestinations($dl->id);
				$agent = $this->booking->getAgentandCompany($dl->user_id);
				$r_summary = '';
		        for($i=0;$i<count($destination_summary);$i++){
		            $destination = $destination_summary[$i];
		            $r_summary[] = $this->booking->getSummaryInformation(array("booking_id"=>$dl->id,"destination_id"=>$destination["destination_id"],"type"=>"3room_payment"));
		        }
			?>
				<tr>
					<td data-label="Code"><a href="<?php echo base_url('home/booking_info_email/'.$dl->id)?>" target="_blank"><b><?php echo $dl->code;?></a></b></td>
					<td data-label="Company"><?php echo $agent[0]->cname?></td>
					<td data-label="Agent"><?php echo $agent[0]->fname.' '.$agent[0]->lname?></td>
					<!-- <td data-label="Passenger">
					<?php
						$count = 1;
						foreach($guest as $g){ 
							echo $count.". ".$g->name."<br>";
							$count++;
						}
					?>
					</td>				
					<td data-label="Departure">
					<b><?php echo $travel[0]->BOOKDEPFROM.' - '.$travel[0]->BOOKDEPTO; ?></b><br>
					<?php echo date('d-M-Y',strtotime($travel[0]->PACKCHECKIN)) ?><br>
					<?php echo $travel[0]->BOOKDEPARTDATE.' - '.$travel[0]->BOOKDEPARTADATEARRIVAL; ?><br>
					<?php echo $travel[0]->BOOKDEPARTFLIGHTNO; ?>
					</td>
					<td data-label="Arrival">
					<b><?php echo $travel[0]->BOOKRETFROM.' - '.$travel[0]->BOOKRETTO; ?></b><br>
					<?php echo date('d-M-Y',strtotime($travel[0]->PACKCHECKOUT)) ?><br>
					<?php echo $travel[0]->BOOKRETURNDATE.' - '.$travel[0]->BOOKRETURNADATEARRIVAL; ?><br>
					<?php echo $travel[0]->BOOKRETURNFLIGHTNO; ?>
					</td>
					<td data-label="Hotel">
					<b><?php echo $r_summary[0][0]['hotel_name']; ?></b><br>
					Extension: 
					<?php if(@$invoice_pdf[0]->BOOKEDEXTENDDAYS == 1):?>
                    <?php echo @$invoice_pdf[0]->BOOKEDEXTENDDAYS;?>
                    <?php else: ?>
                    <?php echo @$invoice_pdf[0]->BOOKEDEXTENDDAYS;?>
                    <?php endif; ?><br>
						<?php
						$i_cnt = 0;
                    	$i_data = count($r_summary[0]);

                    	foreach ($r_summary[0] as $r) {
							$desc = $r['description'];
	                        $desc_exp = explode(" - ", $desc);
	                        if (count($desc_exp) == 1) { $desc_val = $desc_exp[0]; }
	                        else { $desc_val = $desc_exp[count($desc_exp)-1]; }

	                        $comp = $r['computation'];
	                        $comp_exp = explode(" ", $comp);
	                        $comp_val = intval(strip_tags($comp_exp[0]));

	                        if ($desc_val == "Single") { $rooms=$comp_val; }
	                        else if ($desc_val == "Twin") { $rooms=($comp_val/2); }
	                        else if ($desc_val == "Double") { $rooms=($comp_val/2); }
	                        else if ($desc_val == "Triple") { $rooms=($comp_val/3); }
	                        else if ($desc_val == "Child Half Twin") { $rooms=($comp_val/2); }
	                        else if ($desc_val == "Child With Bed") { $rooms=$comp_val; }
	                        else if ($desc_val == "Child Without Bed") { $rooms=$comp_val; }

	                        if ($i_cnt >= 6) {
	                            $i_cnt=1;
	                            echo round($rooms)." ".$desc."<br>";
	                        }
	                        else {
	                            $i_cnt++;
	                            echo round($rooms)." ".$desc."<br>";
	                        }
			            }
                        ?>
					</td> -->
					
					
				</tr>
			<?php }
		}

		?>
		</table>

		<p><h2>PENDING BOOKINGS</h2><span><h3>TOTAL COUNT: <?php echo count($pending)?></h3></span></p>
		<table>
		<thead>
			<tr>
				<th style="text-align:center;"><h3>Booking Code</h3></th>
				<th style="text-align:center;"><h3>Company</h3></th>
				<th style="text-align:center;"><h3>Agent</h3></th>
				<!-- <th style="text-align:center;"><h3>Passenger Details</h3></th>
				<th colspan="2" style="text-align:center;"><h3>Flight Details</h3></th>
				<th style="text-align:center;"><h3>Hotel Details</h3></th> -->
			</tr>
		</thead>
		<?php if(count($pending) > 0){
			$this->load->model('booking/booking');
        	$this->load->model('booking/bookingstorage');
			foreach($pending as $dl){
				$guest=$this->booking->getByGuestVoucher($dl->id);
				$travel=$this->booking->getBysummary($dl->id);
				$hotel=$this->booking->getBysummaryRoomtype($dl->id);
				$invoice_pdf=$this->booking->getBysummary($dl->id);
				$destination_summary = $this->booking->getSummaryDestinations($dl->id);
				$agent = $this->booking->getAgentandCompany($dl->user_id);
				$r_summary = '';
		        for($i=0;$i<count($destination_summary);$i++){
		            $destination = $destination_summary[$i];
		            $r_summary[] = $this->booking->getSummaryInformation(array("booking_id"=>$dl->id,"destination_id"=>$destination["destination_id"],"type"=>"3room_payment"));
		        }
			?>
				<tr>
					<td data-label="Code"><a href="<?php echo base_url('home/booking_info_email/'.$dl->id)?>" target="_blank"><b><?php echo $dl->code;?></a></b></td>
					<td data-label="Company"><?php echo $agent[0]->cname?></td>
					<td data-label="Agent"><?php echo $agent[0]->fname.' '.$agent[0]->lname?></td>
					<!-- <td data-label="Passenger">
					<?php
						$count = 1;
						foreach($guest as $g){ 
							echo $count.". ".$g->name."<br>";
							$count++;
						}
					?>
					</td>				
					<td data-label="Departure">
					<b><?php echo $travel[0]->BOOKDEPFROM.' - '.$travel[0]->BOOKDEPTO; ?></b><br>
					<?php echo date('d-M-Y',strtotime($travel[0]->PACKCHECKIN)) ?><br>
					<?php echo $travel[0]->BOOKDEPARTDATE.' - '.$travel[0]->BOOKDEPARTADATEARRIVAL; ?><br>
					<?php echo $travel[0]->BOOKDEPARTFLIGHTNO; ?>
					</td>
					<td data-label="Arrival">
					<b><?php echo $travel[0]->BOOKRETFROM.' - '.$travel[0]->BOOKRETTO; ?></b><br>
					<?php echo date('d-M-Y',strtotime($travel[0]->PACKCHECKOUT)) ?><br>
					<?php echo $travel[0]->BOOKRETURNDATE.' - '.$travel[0]->BOOKRETURNADATEARRIVAL; ?><br>
					<?php echo $travel[0]->BOOKRETURNFLIGHTNO; ?>
					</td>
					<td data-label="Hotel">
					<b><?php echo $r_summary[0][0]['hotel_name']; ?></b><br>
					Extension: 
					<?php if(@$invoice_pdf[0]->BOOKEDEXTENDDAYS == 1):?>
                    <?php echo @$invoice_pdf[0]->BOOKEDEXTENDDAYS;?>
                    <?php else: ?>
                    <?php echo @$invoice_pdf[0]->BOOKEDEXTENDDAYS;?>
                    <?php endif; ?><br>
						<?php
						$i_cnt = 0;
                    	$i_data = count($r_summary[0]);

                    	foreach ($r_summary[0] as $r) {
							$desc = $r['description'];
	                        $desc_exp = explode(" - ", $desc);
	                        if (count($desc_exp) == 1) { $desc_val = $desc_exp[0]; }
	                        else { $desc_val = $desc_exp[count($desc_exp)-1]; }

	                        $comp = $r['computation'];
	                        $comp_exp = explode(" ", $comp);
	                        $comp_val = intval(strip_tags($comp_exp[0]));

	                        if ($desc_val == "Single") { $rooms=$comp_val; }
	                        else if ($desc_val == "Twin") { $rooms=($comp_val/2); }
	                        else if ($desc_val == "Double") { $rooms=($comp_val/2); }
	                        else if ($desc_val == "Triple") { $rooms=($comp_val/3); }
	                        else if ($desc_val == "Child Half Twin") { $rooms=($comp_val/2); }
	                        else if ($desc_val == "Child With Bed") { $rooms=$comp_val; }
	                        else if ($desc_val == "Child Without Bed") { $rooms=$comp_val; }

	                        if ($i_cnt >= 6) {
	                            $i_cnt=1;
	                            echo round($rooms)." ".$desc."<br>";
	                        }
	                        else {
	                            $i_cnt++;
	                            echo round($rooms)." ".$desc."<br>";
	                        }
			            }
                        ?>
					</td> -->
					
					
				</tr>
			<?php }
		}

		?>
		</table>

		<p><h2>AMENDED BOOKINGS</h2><span><h3>TOTAL COUNT: <?php echo count($amended)?></h3></span></p>
		<table>
		<thead>
			<tr>
				<th style="text-align:center;"><h3>Booking Code</h3></th>
				<th style="text-align:center;"><h3>Company</h3></th>
				<th style="text-align:center;"><h3>Agent</h3></th>
				<!-- <th style="text-align:center;"><h3>Passenger Details</h3></th>
				<th colspan="2" style="text-align:center;"><h3>Flight Details</h3></th>
				<th style="text-align:center;"><h3>Hotel Details</h3></th> -->
			</tr>
		</thead>
		<?php if(count($amended) > 0){
			$this->load->model('booking/booking');
        	$this->load->model('booking/bookingstorage');
			foreach($amended as $am){
				$guest=$this->booking->getByGuestVoucher($am->id);
				$travel=$this->booking->getBysummary($am->id);
				$hotel=$this->booking->getBysummaryRoomtype($am->id);
				$invoice_pdf=$this->booking->getBysummary($am->id);
				$destination_summary = $this->booking->getSummaryDestinations($am->id);
				$agent = $this->booking->getAgentandCompany($am->user_id);
				$r_summary = '';
		        for($i=0;$i<count($destination_summary);$i++){
		            $destination = $destination_summary[$i];
		            $r_summary[] = $this->booking->getSummaryInformation(array("booking_id"=>$am->id,"destination_id"=>$destination["destination_id"],"type"=>"3room_payment"));
		        }
			?>
				<tr>
					<td data-label="Code"><a href="<?php echo base_url('home/booking_info_email/'.$am->id)?>" target="_blank"><b><?php echo $am->code;?></a></b></td>
					<td data-label="Company"><?php echo $agent[0]->cname?></td>
					<td data-label="Agent"><?php echo $agent[0]->fname.' '.$agent[0]->lname?></td>
					<!-- <td data-label="Passenger">
					<?php
						$count = 1;
						foreach($guest as $g){ 
							echo $count.". ".$g->name."<br>";
							$count++;
						}
					?>
					</td>				
					<td data-label="Departure">
					<b><?php echo $travel[0]->BOOKDEPFROM.' - '.$travel[0]->BOOKDEPTO; ?></b><br>
					<?php echo date('d-M-Y',strtotime($travel[0]->PACKCHECKIN)) ?><br>
					<?php echo $travel[0]->BOOKDEPARTDATE.' - '.$travel[0]->BOOKDEPARTADATEARRIVAL; ?><br>
					<?php echo $travel[0]->BOOKDEPARTFLIGHTNO; ?>
					</td>
					<td data-label="Arrival">
					<b><?php echo $travel[0]->BOOKRETFROM.' - '.$travel[0]->BOOKRETTO; ?></b><br>
					<?php echo date('d-M-Y',strtotime($travel[0]->PACKCHECKOUT)) ?><br>
					<?php echo $travel[0]->BOOKRETURNDATE.' - '.$travel[0]->BOOKRETURNADATEARRIVAL; ?><br>
					<?php echo $travel[0]->BOOKRETURNFLIGHTNO; ?>
					</td>
					<td data-label="Hotel">
					<b><?php echo $r_summary[0][0]['hotel_name']; ?></b><br>
					Extension: 
					<?php if(@$invoice_pdf[0]->BOOKEDEXTENDDAYS == 1):?>
                    <?php echo @$invoice_pdf[0]->BOOKEDEXTENDDAYS;?>
                    <?php else: ?>
                    <?php echo @$invoice_pdf[0]->BOOKEDEXTENDDAYS;?>
                    <?php endif; ?><br>
						<?php
						$i_cnt = 0;
                    	$i_data = count($r_summary[0]);

                    	foreach ($r_summary[0] as $r) {
							$desc = $r['description'];
	                        $desc_exp = explode(" - ", $desc);
	                        if (count($desc_exp) == 1) { $desc_val = $desc_exp[0]; }
	                        else { $desc_val = $desc_exp[count($desc_exp)-1]; }

	                        $comp = $r['computation'];
	                        $comp_exp = explode(" ", $comp);
	                        $comp_val = intval(strip_tags($comp_exp[0]));

	                        if ($desc_val == "Single") { $rooms=$comp_val; }
	                        else if ($desc_val == "Twin") { $rooms=($comp_val/2); }
	                        else if ($desc_val == "Double") { $rooms=($comp_val/2); }
	                        else if ($desc_val == "Triple") { $rooms=($comp_val/3); }
	                        else if ($desc_val == "Child Half Twin") { $rooms=($comp_val/2); }
	                        else if ($desc_val == "Child With Bed") { $rooms=$comp_val; }
	                        else if ($desc_val == "Child Without Bed") { $rooms=$comp_val; }

	                        if ($i_cnt >= 6) {
	                            $i_cnt=1;
	                            echo round($rooms)." ".$desc."<br>";
	                        }
	                        else {
	                            $i_cnt++;
	                            echo round($rooms)." ".$desc."<br>";
	                        }
			            }
                        ?>
					</td> -->
					
					
				</tr>
			<?php }
		}

		?>
		</table>
		</div>
	</body>
</html>