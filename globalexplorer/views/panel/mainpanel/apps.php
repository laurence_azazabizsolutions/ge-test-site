<div id="page-wrapper">
    <!-- /.row -->
    <div class="row">
        <div class="col-md-6" style="text-align:center;padding-top:20px;">
        	<h3><i class="fa fa-android"></i> Download GE Agent Assistant now</h3><br>
        	<a href="https://play.google.com/store/apps/details?id=com.azaza.ge&hl=en"><img src="<?php echo base_url('img/feat/playstore.png')?>"></a>
        </div>
    </div><br>
    <div class="row">
        <div class="col-md-6" style="text-align:center;padding-top:20px;">
        	<h3><i class="fa fa-apple"></i> Soon on iOS</h3><br>
        	<img src="<?php echo base_url('img/feat/appstore.png')?>">
        </div>
    </div>
</div>
<!-- /#page-wrapper -->