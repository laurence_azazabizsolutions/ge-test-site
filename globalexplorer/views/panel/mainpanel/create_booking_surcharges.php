<?php
    #load the surchargestorage model ----------------------------------------------
    $this->load->model("surcharge/surchargestorage");
    $this->load->model("user/userpersistence");
    #get the adult count ----------------------------------------------
    $adult_count              = $bdat['adult_count'];

    #get the infant count ----------------------------------------------
    $infant_count             = $bdat["infant_cnt"];

    #get the child count ----------------------------------------------
    $child_count              = $bdat["child_count"];

    #get the depart date ----------------------------------------------
    $depart_date              = $bdat['depart_date'];

    #get the room count ----------------------------------------------
    $room_count               = $bdat['room_count'];

    #get the return date ----------------------------------------------
    $return_date              = $bdat['return_date'];

    #insert into the array the counter ----------------------------------------------
    $total_count              = array("adult_cnt"=>$adult_count,"infant_cnt"=>$infant_count,"child_cnt"=>$child_count, "room_cnt"=>$room_count);

    #total payments ----------------------------------------------
    $total_payments           = 0;
    $total_profit             = 0;
    $total_cost               = 0;
    $total_hotel_surcharges   = 0;
    $total_package_surcharges = 0;
    $total_room_surcharges    = 0;

    #get user information ----------------------------------------------
    $user                     = $this->userpersistence->getPersistedUser();
    $user_id                  = $user->getUser()->getId(); 

    #get the days of the package ----------------------------------------------
    $extended_days            = intVal($bdat['extended_days']);
    $pacakge_days             = intVal($bdat["package_days"]);

    #get package name ----------------------------------------------
    $package_name             = $bdat['package_name'];
    /*$st_room                = $bdat['roomrate_id'];*/
?>

<!-- start of header -->
<div class='row text-center' style='margin-bottom:20px;'>
   <h2><?php  echo $package_name; ?></h2>
    Adult(s) : <b><?php echo intVal($adult_count); ?></b> <b>/</b> Child(ren) : <b><?php echo intVal($child_count); ?></b> <b>/</b> Infant(s) : <b><?php echo intVal($infant_count); ?></b> <br/>
    Depart Date : <b><?php echo $depart_date ?></b> <b>/</b> Return Date : <b><?php echo $return_date; ?></b> <br/>
    Package Days : <b><?php  echo $bdat['package_days'] ?></b> <b>/</b> Extended Days : <b><?php echo $extended_days; ?></b>
</div>
<!-- end of header -->

<!-- container for package surcharges -->
<div class='clearfix alert alert-warning pkg_surcharges_container text-left' style='display:none;'>
    <strong><i class="fa fa-dropbox"></i>&nbsp;Package Surcharge(s)</strong>
</div>
<div id="summary_package_surcharges_table" class=" pkg_surcharges_container" style="margin-bottom:20px; display:none;">
    <?php 
    
        #get all the package surcharges ----------------------------------------------
        $package_surcharges = $bdat['pkg_surcharge_id'];
            
        #count all the package surcharges ----------------------------------------------
        $count_p_s          = count($package_surcharges);
            
        #check if package has surcharges ----------------------------------------------
        if($count_p_s!=0 && $count_p_s && intVal($package_surcharges[0])!==0){
           
            #hide if it has none ----------------------------------------------
            echo "<script>$('.pkg_surcharges_container').show();</script>";

            #counte for the package surcharges ----------------------------------------------
            $pkg_counters = 0;

            #loop through each surcharge information ----------------------------------------------
            for($i=0;$i<count($bdat['pkg_surcharge_id']);$i++){

                $pkg_id             =  $bdat['pkg_surcharge_id'][$i];  
                $pkg                = $this->surchargestorage->getSurchargeInformation("package",$pkg_id);
                $price              = $pkg->cost+$pkg->profit;
                $cost               = $pkg->cost;
                $profit             = $pkg->profit;
                $rule_type          = $pkg->rule_type;
                $rule               = $pkg->rule;
                $rate_type          = $pkg->rate_type;
                
                $bd_pkg_description = "";
                $bd_pkg_computation = "";
                $bd_pkg_subtotal    = "";


                #catch if blackout ---------------------------------------------- 
                if($pkg->rule_type!="blackout"){

                    #compute the surcharges ----------------------------------------------
                    $total_surcharge          = computeHotelPackageSurcharges($rate_type,array("rule"=>$rule, "rule_type"=>$rule_type), $total_count, $price,$depart_date,$return_date,$user_id);
                    
                    #compute the total profit ----------------------------------------------
                    $t_p_p_s                  = computeHotelPackageSurcharges($rate_type,array("rule"=>$rule, "rule_type"=>$rule_type), $total_count, $profit ,$depart_date,$return_date,$user_id);
                    #dump($t_p_p_s);
                    $total_profit             += $t_p_p_s['total'];
                    
                    #compute the total cost ----------------------------------------------
                    $t_p_p_c                  = computeHotelPackageSurcharges($rate_type,array("rule"=>$rule, "rule_type"=>$rule_type), $total_count, $cost ,$depart_date,$return_date,$user_id);
                    #dump($t_p_p_c);
                    $total_cost               += $t_p_p_c['total'];
                    
                    
                    #get the tentative surcharges
                    $total_package_surcharges +=$total_surcharge['total'];
                    $total_payments           += $total_surcharge['total'];
                    
                    $bd_pkg_description       = $pkg->description;
                    $bd_pkg_computation       = "<b>{$price}</b> SGD x  {$total_surcharge['pax']} x  {$total_surcharge['rates']}";
                    $bd_pkg_subtotal          = $total_surcharge['total'];

                    echo "
                    <div class='clearfix' id='pkg_surcharge_individual{$pkg_id}'>
                        <div class='col-md-4' >{$pkg->description}</div>
                        <div class='col-md-6'>
                            <b>{$price}</b> SGD x
                            {$total_surcharge['pax']} x
                            {$total_surcharge['rates']}
                        </div>
                        <div class='col-md-2'><b>{$total_surcharge['total']}</b> SGD</div>
                    </div>
                         ";

                    if($total_surcharge['total']==0){
                       echo "<script>$('#pkg_surcharge_individual{$pkg_id}').hide();</script>";
                    }else{
                        $pkg_counters++;

                        #row information ----------------------------------------------
                        echo "
                        <input type='hidden' name='bd_pkg_sur_description[]' value='{$bd_pkg_description}'>
                        <input type='hidden' name='bd_pkg_sur_computation[]' value='{$bd_pkg_computation}'>
                        <input type='hidden' name='bd_pkg_sur_subtotal[]' value='{$bd_pkg_subtotal}'>
                         ";
                        #end of row information ----------------------------------------------
                    }
                }

            }
            #end of loop

            if($pkg_counters==0){
                echo "<script>$('.pkg_surcharges_container').hide();</script>";  
            }
        }else{
                echo "<script>$('.pkg_surcharges_container').hide();</script>";  
        }
    ?>
</div>
<!-- end of container for package surcharges -->


<!-- destinations computation -->
<?php 
    $counter_destination = count($bdat['destination_id']);
    if($counter_destination!=0){
        $extension_checker_ann = false;
        for($i=0;$i<$counter_destination;$i++){

            if(($counter_destination-1)==$i){
                if($extended_days!=0){
                    $extension_checker_ann = $extended_days;
                }
            }

            #country/destination information ----------------------------------------------
            $country_name          = $bdat["destination_country"][$i];
            $dest_id               = $bdat["destination_id"][$i];
            $days                  = $bdat["destination_nights"][$i];
            
            #hotel information ----------------------------------------------
            $hotel_id              = $bdat["package_hotel_choice"][$i];
            
            $hotel_name            = $bdat["package_hotel_name"][$i];
            
            #roomrate id ----------------------------------------------
            $room_id               = $bdat["roomrate_id"][$i];
            
            #roomrate information ----------------------------------------------
            $room_price            = $bdat["roomrate_price"][$i];
            $room_pax              = $bdat["roomrate_pax"][$i];
            $room_qty              = $bdat["roomrate_quantity"][$i];
            $room_rate_type        = $bdat["roomrate_rate_type"][$i];
            $room_surcharges       = @$bdat["roomrate_surcharges"][$i];
            $room_desc             = $bdat["roomrate_desc"][$i];
            $room_profit           = @$bdat["roomrate_profit"][$i];
            $room_cost             = @$bdat["roomrate_cost"][$i];
            
            #roomrate extension information ----------------------------------------------
            $room_ext_cost         = $bdat["extension_roomrate_cost"][$i];
            $room_ext_prof         = $bdat["extension_roomrate_profit"][$i];
            $room_ext_price        = $bdat["extension_roomrate_price"][$i];
            
            #extension remarks
            $exremarks             = $bdat["exremarks"];
            
            #hotel surcharges meta data ---------------------------------------------- 
            $hotel_surcharges      = @$bdat["hotel_surcharge_id"][$i];
            $hotel_surch_rule_type = @$bdat["hotel_surcharge_rule_type"][$i];
            $hotel_surch_profit    = @$bdat["hotel_surcharge_profit"][$i];
            $hotel_surch_cost      = @$bdat["hotel_surcharge_cost"][$i];
            $hotel_surch_price     = @$bdat["hotel_surcharge_price"][$i];
            $hotel_surch_rule      = @$bdat["hotel_surcharge_rule"][$i];
            $hotel_surch_desc      = @$bdat["hotel_surcharge_desc"][$i];


            echo "
            <input type='hidden' name='bd_destination_id[{$i}]' value='{$dest_id}'/>
            <input type='hidden' name='bd_destination_name[{$i}]' value='{$country_name}' />
            <input type='hidden' name='bd_destination_nights[{$i}]'  value='{$days}' />
            <input type='hidden' name='bd_destination_hotel[{$i}]' value='{$hotel_id}'>
             ";
?>
            <!-- start of destination header -->
            <div class='clearfix alert alert-warning destination_container<?php echo $dest_id; ?> text-left'>
               <strong>
                    <i class="fa fa-map-marker"></i>&nbsp;<?php echo  $country_name; ?> /
                    <i class="fa fa-building-o" style="margin-left:10px;"></i>  <?php echo $hotel_name; ?> /
                    <i class="fa fa-calendar" style="margin-left:10px;"></i> <?php echo $days; ?> night(s)
                </strong>
            </div>
            <!-- end of destination header -->
            

            <!-- destination container -->
            <div class="destination_container destination_container<?php echo $dest_id; ?>" style="margin-bottom:20px;">

                <!-- hotel surcharge container -->
                <div class="clearfix" id="summary_hotel_surcharges_container<?php echo $dest_id;  ?>" style="margin-top:20px;">
                    <label class="col-md-12">Hotel surcharge(s)</label>
                    <?php 
                    
                       #checks the number of non-zero surcharge subtotals ----------------------------------------------
                       $h_s_counter = 0;

                       #couns the total number of surcharges ----------------------------------------------
                       $hotel_surch_counter = count($hotel_surcharges);

                       if($hotel_surch_counter!=0 && $hotel_surcharges!="0"){

                            #loop ----------------------------------------------
                            for($j=0;$j<$hotel_surch_counter;$j++){
                                
                                #fetch the hotel id ----------------------------------------------
                                $hotel_surch          = $hotel_surcharges[$j];
                                $hotel                = $this->surchargestorage->getSurchargeInformation("hotel",$hotel_surch);
                                
                                $price                = $hotel->cost+$hotel->profit;
                                $cost                 = $hotel->cost;
                                $profit               = $hotel->profit;
                                $rule_type            = $hotel->rule_type;
                                $rule                 = $hotel->rule;
                                $rate_type            = $hotel->rate_type;
                                
                                $bd_hotel_description = "";
                                $bd_hotel_computation = "";
                                $bd_hotel_subtotal    = "";
                                
                                //dump($days);
                                $total_surcharge      = computeHotelPackageSurcharges($rate_type,array("rule"=>$rule, "rule_type"=>$rule_type), $total_count, $price,$depart_date,$return_date,$user_id, $days,$extension_checker_ann, (intVal($adult_count)+intval($child_count)));
                                $total_payments       += $total_surcharge["total"];
                                
                                #compute the total profit ----------------------------------------------
                                $total_hotel_profit   = computeHotelPackageSurcharges($rate_type,array("rule"=>$rule, "rule_type"=>$rule_type), $total_count, $profit ,$depart_date,$return_date,$user_id, $days,$extension_checker_ann, (intVal($adult_count)+intval($child_count)));
                                #dump($total_hotel_profit);
                                $total_profit         += $total_hotel_profit['total'];
                                
                                #compute the total cost ----------------------------------------------
                                $total_hotel_cost     = computeHotelPackageSurcharges($rate_type,array("rule"=>$rule, "rule_type"=>$rule_type), $total_count, $cost ,$depart_date,$return_date,$user_id, $days,$extension_checker_ann, (intVal($adult_count)+intval($child_count)));
                                #dump($total_hotel_cost);
                                $total_cost           += $total_hotel_cost['total'];
                                
                                
                                #summary data
                                $bd_hotel_description = $hotel_surch_desc[$j];
                                $bd_hotel_computation = "<b>{$price}</b> SGD x {$total_surcharge['pax']} x {$total_surcharge['rates']}";
                                $bd_hotel_subtotal    = "{$total_surcharge['total']}";
                                #end of summary data

                    ?>
                                <div class="clearfix" id="summary_hotel_surcharge_container<?php echo $hotel_surch; ?>">
                                    <div class="col-md-4"><?php echo $bd_hotel_description; ?></div>
                                    <div class="col-md-6">
                                        <?php echo $bd_hotel_computation; ?>
                                    </div>
                                    <div class="col-md-2"><b><?php echo $total_surcharge["total"]; ?></b> SGD</div>
                                </div>
                    <?php
                                #checks if the subtotal is equal to zero ----------------------------------------------
                                if($total_surcharge["total"]==0){
                                    echo "<script>$('#summary_hotel_surcharge_container{$hotel_surch}').hide();</script>";
                                }else{
                                    $h_s_counter++;
                                }

                                #row information ----------------------------------------------
                                echo "
                                <input type='hidden' name='bd_hotel_sur_description[{$i}][]' value='{$bd_hotel_description}'>
                                <input type='hidden' name='bd_hotel_sur_computation[{$i}][]' value='{$bd_hotel_computation}'>
                                <input type='hidden' name='bd_hotel_sur_subtotal[{$i}][]' value='{$bd_hotel_subtotal}'>
                                 ";
                                #end of row information ----------------------------------------------
                            }

                            #checks if the total number of non-zero surcharges is equal to zero ----------------------------------------------
                            if($h_s_counter==0){
                                echo "<script>$('#summary_hotel_surcharges_container{$dest_id}').hide();</script>";
                            }
                        }else{
                             #row information ----------------------------------------------
                            echo "
                            <input type='hidden' name='bd_hotel_sur_description[{$i}][]' value=''>
                            <input type='hidden' name='bd_hotel_sur_computation[{$i}][]' value=''>
                            <input type='hidden' name='bd_hotel_sur_subtotal[{$i}][]' value=''>
                             ";
                            #end of row information ----------------------------------------------
                            echo "<script>$('#summary_hotel_surcharges_container{$dest_id}').hide();</script>";
                       }
                    ?>
                </div>
                <!-- end of of hotel surcharge container-->
                

                <!-- roomrate surcharges -->
                <div id="summary_room_surcharges<?php echo $dest_id; ?>" class="clearfix" style="margin-top:20px;">
                    <label class="col-md-12">Roomrate surcharge(s)</label>
                        
                    <?php
                        #count the total room surcharges ----------------------------------------------
                        $room_surch_counter = count($room_surcharges);

                        $room_surcharge_counter = 0;

                       // dump($room_surcharges);

                        #loop through the room id ----------------------------------------------
                        for($j=0;$j<$room_surch_counter;$j++){

                            #get the room surcharges ----------------------------------------------
                            $r_surcharge = explode(",", $room_surcharges[$j]);
                            $inut_qty = @$bdat["roomrate_quantity"][$j];

                            if($room_qty[$j]!="" || !empty($room_qty[$j])){
                                #check if surcharge is empty or the first value is 0 ----------------------------------------------
                                if(count($r_surcharge)!=0 && intVal($r_surcharge[0])!=0){

                                    #loop through the room surcharges ----------------------------------------------
                                    for($k=0;$k<count($r_surcharge); $k++){

                                        $r_s               = $r_surcharge[$k];
                                        $room              = $this->surchargestorage->getSurchargeInformation("rooms",$r_s);
                                        
                                        #room information ----------------------------------------------
                                        $price             = $room->cost+$room->profit;
                                        $cost              = $room->cost;
                                        $profit            = $room->profit;
                                        $rule_type         = $room->rule_type;
                                        $rule              = $room->rule;
                                        $rate_type         = $room->rate_type;
                                        $description       = $room->description;

                                        
                                        #summary information ----------------------------------------------
                                        $bd_rs_description = "";
                                        $bd_rs_computation = "";
                                        $bd_rs_subtotal    = "";
                                        

                                        #total price ----------------------------------------------
                                        $total_surcharge   = computeTheSurcharges($rate_type,array("rule"=>$rule, "rule_type"=>$rule_type), $total_count, $price,$depart_date,$return_date,$user_id,$days,$extension_checker_ann,$inut_qty[$k]);
                                        $total_payments    += $total_surcharge["total"];
                                        
                                        #compute the total profit ----------------------------------------------
                                        $total_rs_profit   = computeTheSurcharges($rate_type,array("rule"=>$rule, "rule_type"=>$rule_type), $total_count, $profit ,$depart_date,$return_date,$user_id, $days,$extension_checker_ann,$inut_qty[$k]);
                                        #dump($total_rs_profit);
                                        $total_profit      += $total_rs_profit['total'];
                                        
                                        #compute the total cost ----------------------------------------------
                                        $total_rs_cost     = computeTheSurcharges($rate_type,array("rule"=>$rule, "rule_type"=>$rule_type), $total_count, $cost ,$depart_date,$return_date,$user_id, $days,$extension_checker_ann,$inut_qty[$k]);
                                        #dump($total_rs_cost);
                                        $total_cost        += $total_rs_cost['total'];
                                        
                                        
                                        #summary data
                                        $bd_rs_description = $description;
                                        $bd_rs_computation = "<b>{$price}</b> SGD x {$total_surcharge['pax']} x {$total_surcharge['rates']}";
                                        $bd_rs_subtotal    = "{$total_surcharge['total']}";
                                        #end of summary data


                                        #check if it's not blackout ----------------------------------------------
                                        if($room->rule_type!="blackout"){
                                              #echo "id : {$r_s} price : {$price} cost : {$cost} profit : {$profit} rule_type : {$rule_type} rule : {$rule} rate type : {$rate_type} description : {$description}";
                                            $room_surcharge_counter++;
                    ?>
                                            <div id="summary_room_surcharge_container<?php  echo $r_s; ?>" class="clearfix">
                                                <div class="col-md-4">
                                                    <?php echo $bd_rs_description; ?>
                                                </div>
                                                <div class="col-md-6">
                                                    <?php echo $bd_rs_computation; ?>
                                                </div>
                                                <div class="col-md-2">
                                                    <?php echo "<b>{$total_surcharge["total"]}</b> SGD"; ?>
                                                </div>
                                            </div>
                    <?php
                                        }

                                        if($total_surcharge["total"]==0){
                                            echo "<script>//$('#summary_room_surcharge_container{$r_s}').hide();</script>";
                                        }else{
                                        
                                        }

                                        #row information ----------------------------------------------
                                        echo "
                                        <input type='hidden' name='bd_rs_description[{$i}][]' value='{$bd_rs_description}'>
                                        <input type='hidden' name='bd_rs_computation[{$i}][]' value='{$bd_rs_computation}'>
                                        <input type='hidden' name='bd_rs_subtotal[{$i}][]' value='{$bd_rs_subtotal}'>
                                         ";
                                        #end of row information ----------------------------------------------
                                    }
                                }else{
                                    #row information ----------------------------------------------
                                    echo "
                                    <input type='hidden' name='bd_rs_description[{$i}][]' value=''>
                                    <input type='hidden' name='bd_rs_computation[{$i}][]' value=''>
                                    <input type='hidden' name='bd_rs_subtotal[{$i}][]' value=''>
                                     ";
                                    #end of row information ----------------------------------------------
                                }
                            }
                        }
                        #end of loop through the room id ----------------------------------------------

                            
                                
                        #hide if ther are no roomrate surcharges ----------------------------------------------
                        if($room_surcharge_counter==0){
                            echo "<script>$('#summary_room_surcharges{$dest_id}').hide();</script>";
                        }

                    ?>    
                </div>
                <!-- end of roomrate surcharges -->
                
                <!-- roomrate qty -->
                <div id="summary_room_qty<?php echo $dest_id; ?>" class="clearfix" style="margin-top:20px;">
                    <label class="col-md-12">Room(s)</label>
                    <?php 

                        $room_id_count = count($room_id);

                        for($j=0;$j<$room_id_count;$j++){
                            $qty       = intVal($room_qty[$j]);
                            $pax       = intVal($room_pax[$j]);
                            $rate_type = $room_rate_type[$j];
                            $r_id      = $room_id[$j];
                            $price     = floatVal($room_price[$j]);
                            $r_profit  = floatVal($room_profit[$j]);
                            $r_cost    = floatVal($room_cost[$j]);

                            $new_qty = $qty;
                            if($rate_type=="room/trip" || $rate_type=="room/night"){
                                $new_qty = $qty/$pax;
                            }

                            if($new_qty!=0){
                                                      
                                #total room payments ----------------------------------------------   
                                $new_arr_r_c = $total_count;
                                $new_arr_r_c["room_cnt"] = $qty;
                                $new_arr_r_c["adult_cnt"] = $qty;
                                $new_arr_r_c["child_cnt"] = $qty;

                                $subtotal_payment    = computeTheSurcharges($rate_type,false, $new_arr_r_c, $price,$depart_date,$return_date,$user_id,$days,$extension_checker_ann,$new_qty);
                                $total_payments      += $subtotal_payment["total"];
                                
                                #compute the total profit ----------------------------------------------
                                $total_rs_profit     = computeTheSurcharges($rate_type,false, $new_arr_r_c, $r_profit,$depart_date,$return_date,$user_id,$days,$extension_checker_ann,$new_qty);
                                $total_profit        += $total_rs_profit['total'];
                                
                                #compute the total cost ----------------------------------------------
                                $total_rs_cost       = computeTheSurcharges($rate_type,false, $new_arr_r_c, $r_cost,$depart_date,$return_date,$user_id,$days,$extension_checker_ann,$new_qty);
                                $total_cost          += $total_rs_cost['total']; 
                                
                                #summary data
                                $bd_room_description = $room_desc[$j];
                                $bd_room_computation = "<b>{$price}</b> SGD x {$subtotal_payment['pax']} x {$subtotal_payment['rates']}";
                                $bd_room_subtotal    = "{$subtotal_payment['total']}";
                                #end of summary data


                                #row information ----------------------------------------------
                                echo "
                                <input type='hidden' name='bd_room_description[{$i}][]' value='{$bd_room_description}'>
                                <input type='hidden' name='bd_room_computation[{$i}][]' value='{$bd_room_computation}'>
                                <input type='hidden' name='bd_room_subtotal[{$i}][]' value='{$bd_room_subtotal}'>
                                 ";
                                #end of row information ----------------------------------------------

                    ?>
                                <div class='clearfix' id="summary_room_payment_container<?php echo $r_id;  ?>">
                                    <div class="col-md-4">
                                        <?php echo $bd_room_description; ?>
                                    </div>
                                    <div class="col-md-6">
                                        <?php echo $bd_room_computation; ?>
                                    </div>
                                    <div class="col-md-2">
                                        <?php echo "<b>{$subtotal_payment["total"]}</b> SGD"; ?>
                                    </div>
                                </div>
                    <?php
                                //echo "id = {$r_id} qty = {$qty} pax = {$pax} rate type = {$rate_type} <br>";
                            }else{
                               
                            }
                        }
                    ?>
                </div>
                <!-- end of roomrate qty -->
                
            </div>
            <!-- end of destination container -->

<?php
        }
    }
?>
<!-- end of desinations computation -->


<!-- start of addons-->
<div class="clearfix alert alert-warning addons_container text-left">
    <strong><i class="fa fa-plus-square"></i>&nbsp;Addon(s) </strong>
</div>
<div class="addons_container clearfix" style="margin-bottom:20px;">
    <?php 
        $addon_price  = @$bdat["addon_price"];
        $addon_id     = @$bdat["addon_id"];
        $addon_desc   = @$bdat["addon_desc"];
        $addon_cost   = @$bdat["addon_cost"];
        $addon_profit = @$bdat["addon__profit"];
        $addon_qty    = @$bdat["addon_quantity"];

        $addon_count = count($addon_id);
        if($addon_count!=0){
            #counter ----------------------------------------------
            $counter = 0;

            #loop ----------------------------------------------
            for($i=0; $i<$addon_count; $i++){
                $bd_addon_description = "";
                $bd_addon_computation = "";
                $bd_addon_subtotal    = "";
                
                $a_qty                = intVal($bdat['addon_quantity'][$i]);

                if($a_qty){
                    $counter++;
                    $cost                 = $bdat['addon_cost'][$i];
                    $profit               = $bdat['addon_profit'][$i];
                    $unit                 = $bdat['addon_unit'][$i];
                    $qty                  = intVal($bdat['addon_quantity'][$i]);
                    $price                = floatVal($cost)+floatVal($profit);
                    $desc                 = $bdat['addon_desc'][$i];
                    
                    
                    $total_profit         += $qty*$profit;
                    $total_cost           += $qty*$cost;
                    
                    
                    $total_surcharge      = $qty * $price;
                    
                    
                    $bd_addon_description = "{$desc}";
                    $bd_addon_computation = "<b>{$price}</b> SGD x <b>{$a_qty}</b> {$unit}";
                    $bd_addon_subtotal    = "{$total_surcharge}";

                    echo "
                        <div id='summary_addon_container{$addon_id[$i]}' class='clearfix'>
                            <div class='col-md-4'>{$desc}</div>
                            <div class='col-md-2'><b>{$price}</b> SGD x <b>{$a_qty}</b> {$unit}</div>
                            <div class='col-md-4'></div>
                            <div class='col-md-2'><b>{$total_surcharge}</b> SGD</div>
                        </div>
                         ";

                    $total_surcharge = floatVal($total_surcharge);

                    $total_payments += $total_surcharge;
                    
                    echo "
                    <input type='hidden' name='bd_addon_description[]' value='{$bd_addon_description}'>
                    <input type='hidden' name='bd_addon_computation[]' value='{$bd_addon_computation}'>
                    <input type='hidden' name='bd_addon_subtotal[]' value='{$bd_addon_subtotal}'>
                     ";
                }
            }

            if($counter==0){
                echo "<script>$('.addons_container').hide();</script>";
            }
        }
        else{
            echo "<script>$('.addons_container').hide();</script>";
        }
    ?>
</div>
<!-- end of addons-->

<?php 
    #check if extended days is not 0
    if(($extended_days)!=0 && ($extended_days)>0){
        $days = $extended_days;
?>
        <!-- start of extensions -->
        <div class="clearfix alert alert-warning text-left">
            <strong>
                <i class="fa fa-plus-square"></i>&nbsp;Room Extension(s) /
                <i class="fa fa-calendar"></i>&nbsp;<?php echo $extended_days;  ?> night(s)
            </strong>
        </div>

        <div class="extension_container clearfix" style="margin-bottom:20px;">
            <?php 
                #get the last destination ----------------------------------------------
                $last_destination = $counter_destination-1;
                for($j=0;$j<count($bdat["roomrate_id"][$last_destination]);$j++){
                    $room_id   =  $bdat["roomrate_id"][$last_destination][$j];
                    $room_info = $this->surchargestorage->getHotelRoomInformation($room_id);
                    $r_q       = $bdat["roomrate_quantity"][$last_destination][$j];
                    $pax = $bdat["roomrate_pax"][$last_destination][$j];


                    if($r_q!=0){

                        $new_qty = $r_q/$pax;
                        

                        #total payment ----------------------------------------------
                        $new_arr_r_c = $total_count;
                        $new_arr_r_c["room_cnt"] = $qty;
                        $new_arr_r_c["adult_cnt"] = $qty;
                        #$ext_subtotal_payment = computeThePayment("pax/night", $total_count, $bdat["extension_roomrate_price"][$last_destination][$j], $pacakge_days,$r_q,$user_id, true, $extended_days);
                        $ext_subtotal_payment = computeTheSurcharges("room/night",false, $new_arr_r_c, $bdat["extension_roomrate_price"][$last_destination][$j],$depart_date,$return_date,$user_id,$extended_days,false,$new_qty);
                        $total_payments       += $ext_subtotal_payment["total"];
                        
                        #compute the total profit ----------------------------------------------
                        #$total_rs_profit      = computeThePayment("pax/night", $total_count, $room_info->profit , $pacakge_days,$r_q,$user_id, true, $extended_days);
                        $total_rs_profit      = computeTheSurcharges("room/night",false, $new_arr_r_c, $room_info->ext_profit,$depart_date,$return_date,$user_id,$extended_days,false,$new_qty);
                        $total_profit         += $total_rs_profit['total'];
                        
                        #compute the total cost ----------------------------------------------
                        #$total_rs_cost        = computeThePayment("pax/night",$total_count, $room_info->cost , $pacakge_days,$r_q,$user_id, true, $extended_days);
                        $total_rs_cost        = computeTheSurcharges("room/night",false, $new_arr_r_c, $room_info->ext_cost,$depart_date,$return_date,$user_id,$extended_days,false,$new_qty);
                        $total_cost           += $total_rs_cost['total'];

                        $room_type_val = ucwords(str_replace("_"," ", $room_info->room_type));

                        $bd_ext_description   =  $room_desc[$j];
                        $bd_ext_computation   = "<b>{$bdat["extension_roomrate_price"][$last_destination][$j]}</b> SGD x {$ext_subtotal_payment['rates']} x {$ext_subtotal_payment['pax']}";
                        $bd_ext_subtotal      = $ext_subtotal_payment["total"];
            ?>
                        <div id="summary_extension_row" class="clearfix">
                            <div class="col-md-4">
                                <?php echo $bd_ext_description; ?>
                            </div>
                            <div class="col-md-6">
                                <?php echo $bd_ext_computation; ?>
                            </div>
                            <div class="col-md-2">
                                <b><?php echo $bd_ext_subtotal; ?></b> SGD
                            </div>
                        </div>
            
            <?php
                         echo "
                            <input type='hidden' name='bd_ext_description[]' value='{$bd_ext_description}'>
                            <input type='hidden' name='bd_ext_computation[]' value='{$bd_ext_computation}'>
                            <input type='hidden' name='bd_ext_subtotal[]' value='{$bd_ext_subtotal}'>
                              ";

                   }
                   //dump($room_info);
                }
            ?>
        </div>
        <!-- end of extensions -->
<?php 
    }
?>


<!-- start of toal payments -->
<div class="total_payment_container clearfix" style="padding-top:10px; border-top:1px solid #ccc;">
    <div class="col-md-10 text-left">
        <b>Total</b>
    </div>
    <div class="col-md-2 text-left">
        <b><?php echo $total_payments; ?></b> SGD
    </div>
</div>
<!-- end of total payments -->

<?php 
    //dump("total payments : {$total_payments} cost: {$total_cost} profit : {$total_profit}");
    echo "
        <script>
            $('#package_total_payment').val('{$total_payments}');
            $('#package_total_profit').val('{$total_profit}');
            $('#package_total_cost').val('{$total_cost}');
            $('#package_total_surcharge').val('{$total_package_surcharges}');
            $('#package_hotel_total_surcharge').val('{$total_hotel_surcharges}');
            $('#package_room_total_surcharges').val('{$total_room_surcharges}');
        </script>
         ";
    #end of append the data to the hidden fields ----------------------------------------------
?>

<?php 
    #compute the surcharges ----------------------------------------------
    function computeTheSurcharges($rate_type="pax/night",$rules=array(), $count=array(), $price, $date_start, $date_end,$user_id,$days_destination = FALSE,$extension= FALSE,$room_pax = false){
        #explode the rates ----------------------------------------------
        $rate       = explode("/", $rate_type);
        $multiplier = 0;
        $rates      = "";
        $pax        = 0;
        $totes      = 0;
        
        $time_start = intVal(strtotime($date_start));
        $time_end   = intVal(strtotime($date_end));

        if($rules){
            if(trim($rate[1])=="night"){
                #dump("night compute deparat:{$date_start}/{$date_end}");
                if($rules["rule_type"]=="date_range"){

                    #get range and explode the start and end ----------------------------------------------
                    $rule       = explode(" to ", $rules['rule']); 
                    $rule_start = intVal(strtotime($rule[0]));
                    $rule_end   = intVal(strtotime($rule[1]));
                    #end of get he range and explode the start and end ----------------------------------------------
                    
                    #compute the date ranger ----------------------------------------------
                    $start      = $rule_start <= $time_start ? $time_start : $rule_start;
                    $end        = $rule_end <= $time_end ? $rule_end : $time_end;
                    #end of compute the date range ----------------------------------------------
                    
                    #get the days by subtracting the end from the date to 
                    $days       = floor((((($end-$start)/60)/60)/24));
                    
                    $days       = $days<0 ? 0:$days;


                    #check if the days is greater than the set destination days
                    if($days>$days_destination && $days_destination){
                        #if the extension is true, then don't limite the days multiplier
                        if($extension){
                            $tmp_extensions = $extension+$days_destination;
                            if($tmp_extensions>$days){
                                $tmp_extensions = $days;
                            }
                            $days = $tmp_extensions;
                        }else{
                            $days = $days_destination;
                        }
                    }

                    #set the pax ----------------------------------------------
                    $rates = "<b>{$days}</b> night(s)";

                    #multiple by the days and price
                    $multiplier = $days*$price;
                }
                else if($rules['rule_type']=="day_of_week"){
                    $rule       = $rules['rule']; #get day of the week ----------------------------------------------
                    $time_start = intVal(strtotime($date_start));
                    $time_end   = intVal(strtotime($date_end));
                    
                    $days       = floor((((($time_end-$time_start)/60)/60)/24));
                    
                    #check if the days is greater than the set destination days
                    if($days>$days_destination && $days_destination){
                        #if the extension is true, then don't limite the days multiplier
                        if($extension){
                            $tmp_extensions = $extension+$days_destination;
                            if($tmp_extensions>$days){
                                $tmp_extensions = $days;
                            }
                            $days = $tmp_extensions;
                        }else{
                            $days = $days_destination;
                        }
                    }

                    #surcharge by days of the week ----------------------------------------------
                    if($days<7){
                        $multiplier = 1*$price;
                        $rates = "<b>1</b> {$rules['rule']}(s)";
                    }else{
                        $multiplier = floor($days/7)*$price;
                        $tmp_days = floor($days/7);
                        $rates ="<b>{$tmp_days}</b> {$rules['rule']}(s)";
                    }
                }
                else{
                    $rule       = $rules['rule']; #get agent ----------------------------------------------
                    $time_start = intVal(strtotime($date_start));
                    $time_end   = intVal(strtotime($date_end));
                    $days       = floor((((($time_end-$time_start)/60)/60)/24));

                    #check if the days is greater than the set destination days
                    if($days>$days_destination && $days_destination){
                        #if the extension is true, then don't limite the days multiplier
                        if($extension){
                            $tmp_extensions = $extension+$days_destination;
                            if($tmp_extensions>$days){
                                $tmp_extensions = $days;
                            }
                            $days = $tmp_extensions;
                        }else{
                            $days = $days_destination;
                        }
                    }

                    #agent
                    if($user_id==$rule){
                        $multiplier = $price;
                        $rates = "(<b>{$days}</b>)agent surcharge";
                    }else{
                        $multiplier = 0;
                        $rates = "(<b>0</b>) agent surcharge";
                    }
                }
            }
            else{
                #dump("1 time only");
                #dump("night compute deparat:{$date_start}/{$date_end}");
                if($rules["rule_type"]=="date_range"){
                    #get range and explode the start and end ----------------------------------------------
                    $rule       = explode(" to ", $rules['rule']); 
                    $rule_start = intVal(strtotime($rule[0]));
                    $rule_end   = intVal(strtotime($rule[1]));
                    #end of get he range and explode the start and end ----------------------------------------------
                    
                    #compute the date ranger ----------------------------------------------
                    $start      = $rule_start <= $time_start ? $time_start : $rule_start;
                    $end        = $rule_end <= $time_end ? $rule_end : $time_end;
                    #end of compute the date range ----------------------------------------------
                    
                    #get the days by subtracting the end from the date to 
                    $days       = floor((((($end-$start)/60)/60)/24));

                    #check if the days is greater than the set destination days
                    if($days>$days_destination && $days_destination){
                        #if the extension is true, then don't limite the days multiplier
                        if($extension){
                            $tmp_extensions = $extension+$days_destination;
                            if($tmp_extensions>$days){
                                $tmp_extensions = $days;
                            }
                            $days = $tmp_extensions;
                        }else{
                            $days = $days_destination;
                        }
                    }
                    $days = 1;

                    #set the pax ----------------------------------------------
                    $rates = "<b>{$days}</b> trip";

                    #multiple by the days and price
                    $multiplier =  $days*$price;
                }
                else if($rules['rule_type']=="day_of_week"){
                    $rule       = $rules['rule']; #get day of the week ----------------------------------------------
                    $time_start = intVal(strtotime($date_start));
                    $time_end   = intVal(strtotime($date_end));
                    
                    $days       = floor((((($time_end-$time_start)/60)/60)/24));

                    
                    #check if the days is greater than the set destination days
                    if($days>$days_destination && $days_destination){
                        #if the extension is true, then don't limite the days multiplier
                        if($extension){
                            $tmp_extensions = $extension+$days_destination;
                            if($tmp_extensions>$days){
                                $tmp_extensions = $days;
                            }
                            $days = $tmp_extensions;
                        }else{
                            $days = $days_destination;
                        }
                    }

                    $repeater = floor($days);

                    #surcharge by days of the week ----------------------------------------------
                    $multiplier = floor($repeater/7);

                    #surcharge by days of the week ----------------------------------------------
                    if($days<7){
                        $multiplier = 1*$price;
                         #set the pax ----------------------------------------------
                        $rates = "<b>1</b> trip";
                    }else{
                        $multiplier = floor($days/7)*$price;

                        #set the pax ----------------------------------------------
                        $rates = "<b>{$days}</b> trip";
                    }
                }
                else{
                    $rule = intVal($rules['rule']); #get agent id ----------------------------------------------
                    $multiplier = 1;
                    
                    if($user_id==$rule){
                        $multiplier = $price;
                        $rates = "agent surcharge";
                    }else{
                        $multiplier = 0;
                        $rates = "(<b>0</b>) agent surcharge";
                    }
                }
            }
        }else{
            if($rate[1]=="night"){
                $multiplier = $days_destination*$price;
                #set the pax ----------------------------------------------
                $rates = "<b>{$days_destination}</b> night(s)";
            }else{
                $multiplier = 1*$price;
                 #set the pax ----------------------------------------------
                $rates = "<b>1</b> trip";
            }
        }


        $totes = $multiplier*intval($room_pax);
        $pax   = "<b>".intval($room_pax)."</b>";
        

        #if adult ----------------------------------------------
        if(trim($rate[0])=="adult"){
           $pax.=" adult(s)";
        }

        #if infant ----------------------------------------------
        else if(trim($rate[0])=="infant"){
           $pax   .= " infant(s)";
        }

        #if child ----------------------------------------------
        else if(trim($rate[0])=="child"){
           $pax   .= " child(ren)";
        }
        #if room ----------------------------------------------
        else if(trim($rate[0])=="room"){
           $pax   .= " room(s)";
        }

        #if all ----------------------------------------------
        else{
           $pax   .= " people";
        }

        #return to multiplier ----------------------------------------------
        return array("total"=>floatVal($totes),"rates"=>$rates, "multiplier"=>$multiplier, "pax"=>$pax);
    }
    #end of surcharges computation ----------------------------------------------

    /*
    * compute outer surcharges
    */
    function computeHotelPackageSurcharges($rate_type="pax/night",$rules=array(), $count=array(), $price, $date_start, $date_end,$user_id,$days_destination = FALSE,$extension= FALSE,$room_pax = false){
        #explode the rates ----------------------------------------------
        $rate       = explode("/", $rate_type);
        $multiplier = 0;
        $rates      = "";
        $pax        = 0;
        $totes      = 0;
        
        $time_start = intVal(strtotime($date_start));
        $time_end   = intVal(strtotime($date_end));

        if($rules){
            if(trim($rate[1])=="night"){
                #dump("night compute deparat:{$date_start}/{$date_end}");
                if($rules["rule_type"]=="date_range"){

                    #get range and explode the start and end ----------------------------------------------
                    $rule       = explode(" to ", $rules['rule']); 
                    $rule_start = intVal(strtotime($rule[0]));
                    $rule_end   = intVal(strtotime($rule[1]));
                    #end of get he range and explode the start and end ----------------------------------------------
                    
                    #compute the date ranger ----------------------------------------------
                    $start      = $rule_start <= $time_start ? $time_start : $rule_start;
                    $end        = $rule_end <= $time_end ? $rule_end : $time_end;
                    #end of compute the date range ----------------------------------------------
                    
                    #get the days by subtracting the end from the date to 
                    $days       = floor((((($end-$start)/60)/60)/24));
                    
                    $days       = $days<0 ? 0:$days;

                  

                    #check if the days is greater than the set destination days
                    if($days>$days_destination && $days_destination){
                        #if the extension is true, then don't limite the days multiplier
                        if($extension){
                            $tmp_extensions = $extension+$days_destination;
                            if($tmp_extensions>$days){
                                $tmp_extensions = $days;
                            }
                            $days = $tmp_extensions;
                        }else{
                            $days = $days_destination;
                        }
                    }

                    #set the pax ----------------------------------------------
                    $rates = "<b>{$days}</b> night(s)";

                    #multiple by the days and price
                    $multiplier = $days*$price;
                }
                else if($rules['rule_type']=="day_of_week"){
                    $rule       = $rules['rule']; #get day of the week ----------------------------------------------
                    $time_start = intVal(strtotime($date_start));
                    $time_end   = intVal(strtotime($date_end));
                    
                    $days       = floor((((($time_end-$time_start)/60)/60)/24));
                    
                    #check if the days is greater than the set destination days
                    if($days>$days_destination && $days_destination){
                        #if the extension is true, then don't limite the days multiplier
                        if($extension){
                            $tmp_extensions = $extension+$days_destination;
                            if($tmp_extensions>$days){
                                $tmp_extensions = $days;
                            }
                            $days = $tmp_extensions;
                        }else{
                            $days = $days_destination;
                        }
                    }

                    #surcharge by days of the week ----------------------------------------------
                    if($days<7){
                        $multiplier = 1*$price;
                        $rates = "<b>1</b> {$rules['rule']}(s)";
                    }else{
                        $multiplier = floor($days/7)*$price;
                        $tmp_days = floor($days/7);
                        $rates ="<b>{$tmp_days}</b> {$rules['rule']}(s)";
                    }
                }
                else{
                    $rule       = $rules['rule']; #get agent ----------------------------------------------
                    $time_start = intVal(strtotime($date_start));
                    $time_end   = intVal(strtotime($date_end));
                    $days       = floor((((($time_end-$time_start)/60)/60)/24));

                    #check if the days is greater than the set destination days
                    if($days>$days_destination && $days_destination){
                        #if the extension is true, then don't limite the days multiplier
                        if($extension){
                            $tmp_extensions = $extension+$days_destination;
                            if($tmp_extensions>$days){
                                $tmp_extensions = $days;
                            }
                            $days = $tmp_extensions;
                        }else{
                            $days = $days_destination;
                        }
                    }

                    #agent
                    if($user_id==$rule){
                        $multiplier = $price;
                        $rates = "(<b>{$days}</b>)agent surcharge";
                    }else{
                        $multiplier = 0;
                        $rates = "(<b>0</b>) agent surcharge";
                    }
                }
            }
            else{
                #dump("1 time only");
                #dump("night compute deparat:{$date_start}/{$date_end}");
                if($rules["rule_type"]=="date_range"){
                    #get range and explode the start and end ----------------------------------------------
                    $rule       = explode(" to ", $rules['rule']); 
                    $rule_start = intVal(strtotime($rule[0]));
                    $rule_end   = intVal(strtotime($rule[1]));
                    #end of get he range and explode the start and end ----------------------------------------------
                    
                    #compute the date ranger ----------------------------------------------
                    $start      = $rule_start <= $time_start ? $time_start : $rule_start;
                    $end        = $rule_end <= $time_end ? $rule_end : $time_end;
                    #end of compute the date range ----------------------------------------------
                    
                    #get the days by subtracting the end from the date to 
                    $days       = floor((((($end-$start)/60)/60)/24));

                    #check if the days is greater than the set destination days
                    if($days>$days_destination && $days_destination){
                        #if the extension is true, then don't limite the days multiplier
                        if($extension){
                            $tmp_extensions = $extension+$days_destination;
                            if($tmp_extensions>$days){
                                $tmp_extensions = $days;
                            }
                            $days = $tmp_extensions;
                        }else{
                            $days = $days_destination;
                        }
                    }
                    $days = 1;

                    #set the pax ----------------------------------------------
                    $rates = "<b>{$days}</b> trip";

                    #multiple by the days and price
                    $multiplier =  $days*$price;
                }
                else if($rules['rule_type']=="day_of_week"){
                    $rule       = $rules['rule']; #get day of the week ----------------------------------------------
                    $time_start = intVal(strtotime($date_start));
                    $time_end   = intVal(strtotime($date_end));
                    
                    $days       = floor((((($time_end-$time_start)/60)/60)/24));

                    
                    #check if the days is greater than the set destination days
                    if($days>$days_destination && $days_destination){
                        #if the extension is true, then don't limite the days multiplier
                        if($extension){
                            $tmp_extensions = $extension+$days_destination;
                            if($tmp_extensions>$days){
                                $tmp_extensions = $days;
                            }
                            $days = $tmp_extensions;
                        }else{
                            $days = $days_destination;
                        }
                    }

                    $repeater = floor($days);

                    #surcharge by days of the week ----------------------------------------------
                    $multiplier = floor($repeater/7);

                    #surcharge by days of the week ----------------------------------------------
                    if($days<7){
                        $multiplier = 1*$price;
                         #set the pax ----------------------------------------------
                        $rates = "<b>1</b> trip";
                    }else{
                        $multiplier = floor($days/7)*$price;
                        #set the pax ----------------------------------------------
                        $rates = "<b>1</b> trip";
                    }

                }
                else{
                    $rule = intVal($rules['rule']); #get agent id ----------------------------------------------
                    $multiplier = 1;
                    
                    if($user_id==$rule){
                        $multiplier = $price;
                        $rates = "agent surcharge";
                    }else{
                        $multiplier = 0;
                        $rates = "(<b>0</b>) agent surcharge";
                    }
                }
            }
        }else{
            if($rate[1]=="night"){
                $multiplier = $days_destination*$price;
                #set the pax ----------------------------------------------
                $rates = "<b>{$days_destination}</b> night(s)";
            }else{
                $multiplier = 1*$price;
                 #set the pax ----------------------------------------------
                $rates = "<b>1</b> trip";
            }
        }

        #if adult ----------------------------------------------
        if(trim($rate[0])=="adult"){
            $totes = $multiplier*intval($count["adult_cnt"]);
            $pax   = "<b>".intval($count["adult_cnt"])."</b>";
            $pax.=" adult(s)";
        }

        #if infant ----------------------------------------------
        else if(trim($rate[0])=="infant"){
            $totes = $multiplier*intval($count["infant_cnt"]);
            $pax   = "<b>".intval($count["infant_cnt"])."</b>";
            $pax   .= " infant(s)";
        }

        #if child ----------------------------------------------
        else if(trim($rate[0])=="child"){
            $totes = $multiplier*intval($count["child_cnt"]);
            $pax   = "<b>".intval($count["child_cnt"])."</b>";
            $pax   .= " child(ren)";
        }

        #if room ----------------------------------------------
        else if(trim($rate[0])=="room"){
            $totes = $multiplier*intval($count["room_cnt"]);
            $pax   = "<b>".intval($count["room_cnt"])."</b>";
            $pax   .= " room(s)";
        }

        #if all ----------------------------------------------
        else{
            $totes = $multiplier*intval($count["adult_cnt"]+$count["child_cnt"]);
            $pax   = "<b>".intval($count["adult_cnt"]+$count["child_cnt"])."</b>";
            $pax   .= " people";
        }

        #return to multiplier ----------------------------------------------
        return array("total"=>floatVal($totes),"rates"=>$rates, "multiplier"=>$multiplier, "pax"=>$pax);
    }
?>