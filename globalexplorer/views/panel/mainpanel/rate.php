<div id="page-wrapper">
    <!-- /.row -->
    <div class="row">
        <br>
        <div class="col-lg-12">
            <form class="form-horizontal" role="form" action="<?php echo base_url('panel/rate')?>" method="post">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Destination</label>
                    <div class="col-sm-8">
                        <select class="form-control" name="destination" id="destination_">
                            <option value="">- Select -</option>
                            <?php if (isset($destinations)):?>
                                <?php foreach ($destinations as $destination):?>
                                        <option value="<?php echo $destination['code']; ?>"><?php echo $destination['country']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Package</label>
                    <div class="col-sm-8">
                        <select class="form-control" name="package_id" id="package_id_">
                            <option value="">- Select -</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Hotel</label>
                    <div class="col-sm-8">
                        <select class="form-control" name="hotel_name_id" id="hotel_name">
                             <option value=" ">- Select -</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Date</label>
                    <div class="col-sm-4">
                        <input id="datetimepickerValidDateCheckIn" class="form-control" type="text" placeholder="Check In" name="date_in" data-date-format="YYYY-MM-DD"/>
                        <p class="help-block">e.g. 1987-11-13</p>
                    </div>
                    <div class="col-sm-4">
                        <input id="datetimepickerValidDateCheckOut" class="form-control" type="text" placeholder="Check Out" name="date_out" data-date-format="YYYY-MM-DD"/>
                        <p class="help-block">e.g. 1987-11-13</p>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-3">
                        <input type="submit" class="btn btn-orange" value="Check" name="button"/>
                    </div>
                </div>
            </form>

            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th colspan="6">
                                Package Title : <?php echo @$room_summary[0]->packagestitle;?>
                            </th>
                        </tr>
                        <tr>
                           <!--  <th>Package Title</th> -->
                            <th>Hotel Name</th>
                            <th>Hotel Room Type</th>
                            <th>Hotel Description</th>
                            <th>Hotel Rate Type</th>
                            <th>Hotel Cost</th>
                            <th>Hotel Extended Cost (Per Night)</th>
                        </tr>
                    </thead>
                    <tbody>
                       <?php 
                       // if (isset($room_summary)): 
                       //    unset($bool_hotel);
                       //  $counter=1;
                        ?>

                        <?php
                            $i=0;
                            foreach ($list_hotel as $hotel) {
                         ?>
                                    <tr>
                                        <th rowspan="<?php echo count($hotel->hotelroom)+1; ?>"> <?php echo $hotel->HOTEL_NAME; ?> </th>
                        <?php
                                foreach ($hotel->hotelroom as $room) {
                        ?>
                                        <td> <?php echo $room->HRM_ROOM_TYPE; ?> </td>
                                        <td> <?php echo $room->HRM_DESC; ?> </td>
                                        <td> <?php echo $room->HRM_RATE_TYPE; ?> </td>
                                        <td> <?php echo $room->HRM_COST + $room->HRM_PROFIT; ?> </td>
                                        <td> <?php echo $room->HRM_EXT_COST + $room->HRM_EXT_PROFIT; ?> </td>
                                    </tr>
                        <?php            
                                }
                        ?>
                                    <tr>
                                        <td colspan="5">
                                            <table class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>Surcharges</th>
                                                        <th>Description</th>
                                                        <th>Rate Type</th>
                                                        <th>Cost</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                        if (count($list_srchg[$i]->pkg) > 0) {
                                                            echo '<tr>
                                                                <th rowspan="'.count($list_srchg[$i]->pkg).'">Package</th>';
                                                                foreach ($list_srchg[$i]->pkg as $pkg) {
                                                                    //dump($pkg[]->description);

                                                                echo '<td>'.$pkg['description'].'</td>
                                                                <td>'.$pkg['rate_type'].'</td>
                                                                <td>'.(floatval($pkg['cost']) + floatval($pkg['profit'])).'</td>
                                                            </tr>';

                                                                }
                                                        }

                                                        if (count($list_srchg[$i]->htl) > 0) {
                                                            echo '<tr>
                                                                <th rowspan="'.count($list_srchg[$i]->htl).'">Hotel</th>';
                                                                foreach ($list_srchg[$i]->htl as $htl) {

                                                                echo '<td>'.$htl['description'].'</td>
                                                                <td>'.$htl['rate_type'].'</td>
                                                                <td>'.(floatval($htl['cost']) + floatval($htl['profit'])).'</td>
                                                            </tr>';

                                                                }
                                                        }

                                                        $cnt_rm = 0;
                                                        if (count($list_srchg[$i]->hrm) > 0) {
                                                            for ($rm=0; $rm < count($list_srchg[$i]->hrm); $rm++) {
                                                                if (count($list_srchg[$i]->hrm[$rm]) > 0) {
                                                                    $cnt_rm += count($list_srchg[$i]->hrm[$rm]);
                                                                }
                                                            }
                                                            
                                                            if ($cnt_rm > 0) {
                                                                echo '<tr>
                                                                    <th rowspan="'.$cnt_rm.'">Room</th>';

                                                                for ($rm=0; $rm < count($list_srchg[$i]->hrm); $rm++) {
                                                                    if (count($list_srchg[$i]->hrm[$rm]) > 0 && $list_srchg[$i]->hrm[$rm] !== FALSE) {
                                                                        foreach ($list_srchg[$i]->hrm[$rm] as $hrm) {

                                                                    echo '<td>'.$hrm['description'].'</td>
                                                                    <td>'.$hrm['rate_type'].'</td>
                                                                    <td>'.(floatval($hrm['cost']) + floatval($hrm['profit'])).'</td>
                                                                </tr>';

                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                        <?php
                                $i++;
                            }
                        ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- /#page-wrapper -->
