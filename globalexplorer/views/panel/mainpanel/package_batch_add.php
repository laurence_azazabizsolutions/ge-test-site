<div class="batch-modal" style="height:100%; position: fixed; z-index: 9999999; width: 100%; background: none repeat scroll 0px 0px rgba(51, 51, 51, 0.7);">
	<div class="container">
		<div class="row">
			<div class="batch_modal_panel">
				<div id="myModalRoomRate" class="col-md-12" style="overflow-y: auto; background: none repeat scroll 0% 0% rgb(255, 255, 255); margin: 5% 0px 0px; border-radius: 10px; padding: 20px;">
					<div class="batch-header">
                        <button id="batch_close" class="pull-right btn btn-danger btn-sm" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                        <h4 id="myModalLabel" class="modal-title">Hotel Room Rate (Batch Add)</h4>
                    </div>
                    <div class="batch-body">
						<form id="addhotelroomrate_form" class="form-horizontal form_batch_add" role="form" action="<?php echo base_url('panel/packageBatchUpdate')?>" method="post">
                            <div class="alert alert-danger" style="display: none;"></div>
                            <input type="hidden" name="packageid" value="<?php echo @$packageid;?>">
                            <input type="hidden" name="parent_id" value="">
                            <input type="hidden" name="act_type" value="add">
                            <div class="modal-body" style="overflow-y: auto;">
                            <div class="col-md-12">
                            	<button id="add_batch_button" class="pull-right btn btn-danger btn-sm" type="button" style="border-radius: 5px 5px 0px 0px; width: 35px;"><span aria-hidden="true">+</span><span class="sr-only">Add Room Rate</span></button>
	                            <div class="clearfix"></div>
	                            <table id="batch_table" class="table table-bordered table-condensed">
								  <thead>
								  	<tr>
								  		<th>Rate Name</th>
								  		<th>Rate Type</th>
								  		<th>Description</th>
								  		<th>Cost</th>
								  		<th>Ext. Cost</th>
								  		<th>Profit</th>
								  		<th>Ext. Profit</th>
								  	</tr>
								  </thead>

							      <tbody class="batch_add_tbody" id="badd_roomrate" data-packageid="<?php echo @$packageid;?>">
							        
							        <tr>
							          <td class="form-group">
							          	<select name="hotelroomrate_name[]" class="form-control">
                                            <option value="single">Single</option>
                                            <option value="twin">Twin</option>
                                            <option value="triple">Triple</option>
                                            <option value="quadruple">Quadruple</option>
                                            <option value="child_half_twin">Child Half Twin</option>
                                            <option value="child_with_bed">Child With Bed</option>
                                            <option value="child_without_bed">Child Without Bed</option>
                                            <option value="extra_bed">Extra Bed</option>
                                            <option value="breakfast">Breakfast</option>
                                        </select>
							          </td>
							          <td>
							          	<select name="hotelroomrate_type[]" class="form-control">
                                            <option value="pax/night">Pax / Night</option>
                                            <option value="adult/night">Adult / Night</option>
                                            <option value="child/night">Child / Night</option>
                                            <option value="room/night">Room / Night</option>
                                            <option value="pax/trip">Pax / Trip</option>
                                            <option value="adult/trip">Adult / Trip</option>
                                            <option value="child/trip">Child / Trip</option>
                                            <option value="room/trip">Room / Trip</option>
                                        </select>
							          </td>
							          <td class="form-group"><input type="text" name="hotelroomrate_description[]" class="form-control"></td>
							          <td class="form-group"><input type="number" onkeypress="return isNumberKey(event)" placeholder="Default" name="hotelroomrate_cost[]" class="form-control" step="any"></td>
							          <td class="form-group"><input type="number" onkeypress="return isNumberKey(event)" placeholder="Extension" name="hotelroomrate_cost_ext[]" class="form-control" step="any"></td>
							          <td class="form-group"><input type="number" onkeypress="return isNumberKey(event)" placeholder="Default" name="hotelroomrate_profit[]" class="form-control" step="any"></td>
							          <td class="form-group">
							          <input type="number" onkeypress="return isNumberKey(event)" placeholder="Extension" name="hotelroomrate_profit_ext[]" class="form-control" step="any">
							          <a href="#" class="remove_field"><i class="fa fa-minus"></i></a>
							          </td>
							        </tr>


							      </tbody>


							    </table>
                            </div>
							<div class="clearfix"></div>
                                <!-- <div class="form-group">
                                    <label class="col-xs-4 control-label">Pax</label>
                                    <div class="col-xs-8">
                                        <input type="number" class="form-control" name="hotelroomrate_pax" onkeypress="return isNumberKey(event)">
                                    </div>
                                </div> -->
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-orange batch_add_submit" type="submit" role="button" disabled="disabled">Save</button>
                            </div>
                        </form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(function(){
	  $(window).resize(function(){
	    var windows = $(this);
	    $('.batch-wrap').find('.modal-body').css('height',windows.height() - 315);
	  });
	  $(window).resize();
	});

	$(document).ready(function() {
	    var max_fields      = 25; //maximum input boxes allowed
	    var wrapper         = $(".batch_add_tbody"); //Fields wrapper
	    var add_button      = $("#add_batch_button"); //Add button ID
	    var appendeddata    = $('.batch_add_tbody').html();
	    var x = 1;

	    $(add_button).click(function(e){
	        e.preventDefault();
	        if(x < max_fields){
	        	var tr_count = $('.batch_add_tbody').find('tr').length;
	            wrapper.append(appendeddata);
	            
	            $('.batch_add_tbody').find('tr:eq('+tr_count+')').find('[type="number"]').val('');
	            $('.form_batch_add').data('bootstrapValidator').addField($('.batch_add_tbody').find('tr:eq('+tr_count+')').find('[name="hotelroomrate_cost[]"]'));
	            $('.form_batch_add').data('bootstrapValidator').addField($('.batch_add_tbody').find('tr:eq('+tr_count+')').find('[name="hotelroomrate_cost_ext[]"]'));
	            $('.form_batch_add').data('bootstrapValidator').addField($('.batch_add_tbody').find('tr:eq('+tr_count+')').find('[name="hotelroomrate_profit[]"]'));
	            $('.form_batch_add').data('bootstrapValidator').addField($('.batch_add_tbody').find('tr:eq('+tr_count+')').find('[name="hotelroomrate_profit_ext[]"]'));
	            $('.form_batch_add').data('bootstrapValidator').addField($('.batch_add_tbody').find('tr:eq('+tr_count+')').find('[name="hotelroomrate_pax[]"]'));
	        	$('.form_batch_add').data('bootstrapValidator').resetForm();
	        }
	    });
	   
	    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
	        e.preventDefault(); $(this).parent().parent().remove(); x--;
	    });

	    /*$('.batch_add_submit').click(function(e){
			var package_id = $('#badd_roomrate').attr('data-packageid');
			var action = $('.form_batch_add').attr('action');
			var baseurl = $('html').attr('base-url');
			var data = $('.form_batch_add').serializeArray();
			$.post(action,data,function(result)
            {
              result = $.parseJSON(result);
              if(result.status === true)
              {
                
                $('table#packagehotels_table').find('[packagehotelid='+package_id+']').find('#roomrate_table.table > tbody').load(baseurl + 'panel/package_batch_add_view/' + package_id);
                $('.batch-wrap .batch-modal').remove();
                data = "";

              }else{
                alert('Failed');
              }
              
            }).fail(function(error){
              //console.log(error.responseText);
            });

			e.preventDefault();
		});*/

	    $('.form_batch_add')
		.bootstrapValidator({
			message: 'This value is not valid',
			feedbackIcons: {
				valid: 'glyphicon glyphicon-ok',
				invalid: 'glyphicon glyphicon-remove',
				validating: 'glyphicon glyphicon-refresh'
			},
			fields: {
						'hotelroomrate_cost[]': {
							message: 'The Cost is not valid',
							validators: {
								notEmpty: {
									message: 'Cost is required and can\'t be empty'
								},
								greaterThan: {
								    value: 0,
								    message: 'Please enter a value greater than or equal to %s'
								}
							}
						},
						'hotelroomrate_cost_ext[]': {
							message: 'The Cost is not valid',
							validators: {
								notEmpty: {
									message: 'Cost Ext. is required and can\'t be empty'
								},
								greaterThan: {
								    value: 0,
								    message: 'Please enter a value greater than or equal to %s'
								}
							}
						},
						'hotelroomrate_profit[]': {
							message: 'The Profit is not valid',
							validators: {
								notEmpty: {
									message: 'Profit is required and can\'t be empty'
								},
								greaterThan: {
								    value: 0,
								    message: 'Please enter a value greater than or equal to %s'
								}
							}
						},
						'hotelroomrate_profit_ext[]': {
							message: 'The Profit is not valid',
							validators: {
								notEmpty: {
									message: 'Profit Ext. is required and can\'t be empty'
								},
								greaterThan: {
								    value: 0,
								    message: 'Please enter a value greater than or equal to %s'
								}
							}
						},
						'hotelroomrate_pax[]': {
							message: 'The Pax is not valid',
							validators: {
								// notEmpty: {
								// 	message: 'Pax is required and can\'t be empty'
								// },
								greaterThan: {
								    value: 1,
								    message: 'Please enter a value greater than or equal to %s'
								}
							}
						}
					}
		}).on('success.form.bv',function(e){
			var package_id = $('#badd_roomrate').attr('data-packageid');
			var action = $('.form_batch_add').attr('action');
			var baseurl = $('html').attr('base-url');
			var data = $('.form_batch_add').serializeArray();
			$.post(action,data,function(result)
            {
              result = $.parseJSON(result);
              if(result.status === true)
              {
                
                $('table#packagehotels_table').find('[packagehotelid='+package_id+']').find('#roomrate_table.table > tbody').load(baseurl + 'panel/package_batch_edit_view/' + package_id);
                $('.batch-wrap .batch-modal').remove();
                data = "";
              }else{
                alert('Failed');
              }
              
            }).fail(function(error){
              //console.log(error.responseText);
            });

			e.preventDefault();
		});

	});
</script>