<div id="page-wrapper">
    <!-- /.row -->
    <div class="row">
        <br>
        <div class="col-lg-12">
            <!-- Nav tabs -->
            <ul class="nav nav-pills" role="tablist">
                <li <?php if($tab==null || $tab=="new"){ echo "class='active'";} ?> ><a href="javascript:void(0);" onclick="relocateManageUsers('new');" role="tab" data-toggle="tab">New</a></li>
                <li <?php if($tab=="customers"){ echo "class='active'";} ?> ><a href="javascript:void(0);" onclick="relocateManageUsers('customers');" role="tab" data-toggle="tab">Customers</a></li>
                <li <?php if($tab=="agents"){ echo "class='active'";} ?> ><a href="javascript:void(0);"onclick="relocateManageUsers('agents');" role="tab" data-toggle="tab">Agents</a></li>
                <li <?php if($tab=="providers"){ echo "class='active'";} ?> ><a href="javascript:void(0);" onclick="relocateManageUsers('providers');" role="tab" data-toggle="tab">Providers</a></li>
                <li <?php if($tab=="admins"){ echo "class='active'";} ?> ><a href="javascript:void(0);" onclick="relocateManageUsers('admins');" role="tab" data-toggle="tab">Admins</a></li>
                <li <?php if($tab=="accounts"){ echo "class='active'";} ?> ><a href="javascript:void(0);" onclick="relocateManageUsers('accounts');" role="tab" data-toggle="tab">Accounts</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane <?php if($tab==NULL || $tab=="new"){ echo "active";} ?> " id="new">
                    <br>

                    <form id="admin_register" class="form-horizontal" role="form" method="post" action="<?php echo base_url('panel/register')?>">
                        <div class="alert alert-success" style="display: none;"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Salutation
                                <div class="requiredfld"> *</div>
                            </label>
                            <div class="col-sm-8">
                                <select class="form-control" name="salutation">
                                    <?php if (isset($salutations)): ?>
                                        <?php foreach ($salutations as $key => $salutation): ?>
                                            <option value="<?php echo $key; ?>"><?php echo $salutation; ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                  </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Type
                                <div class="requiredfld"> *</div>
                            </label>
                            <div class="col-sm-8">
                                <select class="form-control" name="user_type">
                                    <option value='customer'>Customer</option>
                                    <option value='agent'>Agent</option>
                                    <option value='provider'>Provider</option>
                                    <option value='admin'>Admin</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">First Name<div class="requiredfld"> *</div></label>
                            <div class="col-sm-8">
                              <input type="text" class="form-control" placeholder="John" name="firstname">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Middle Name</label>
                            <div class="col-sm-8">
                              <input type="text" class="form-control" name="middlename">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Last Name<div class="requiredfld"> *</div></label>
                            <div class="col-sm-8">
                              <input type="text" class="form-control"placeholder="Doe" name="lastname">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Username<div class="requiredfld"> *</div></label>
                            <div class="col-sm-8">
                              <input type="text" class="form-control" placeholder="johndoe101" name="username">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Date of Birth<div class="requiredfld"> *</div></label>
                            <div class="col-sm-8">
                              <div class="input-group date" id="datetimepickerDOB">
                                <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                <input class="form-control" type="text" placeholder="e.g. 1987-11-13" name="dob" data-date-format="YYYY-MM-DD"/>
                              </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Company</label>
                            <div class="col-sm-8">
                                <input id="typeahead_manage" type="text" class="form-control" placeholder="Company XYZ" name="company">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Phone<div class="requiredfld"> *</div></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" placeholder="+65123456" name="phone_number">
                            </div>
                        </div>
                        <div class="emergency-area">
                            <div class="form-group emergency-dynamic">
                                    <label class="col-sm-3 control-label">Emergency Phone<div class="requiredfld"> *</div></label>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" placeholder="Jane Doe" name="emergency_name[]">
                                    </div>
                                    <div class="col-sm-5 ge-sctop">
                                        <i class="fa fa-plus em-addmore"></i>
                                        <input type="text" class="form-control" placeholder="+65123456" name="emergency_phone_number[]">
                                    </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Email<div class="requiredfld"> *</div></label>
                            <div class="col-sm-8">
                                <input type="email" class="form-control" placeholder="johndoe@domail.com" name="email">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Alternative Email</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" placeholder="email separated with comma" name="alteremail" onblur="validateMultipleEmailsCommaSeparated(this,',')">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Country<div class="requiredfld"> *</div></label>
                            <div class="col-sm-8">
                                <select class="form-control" name="country">
                                    <?php if (isset($countries_usr)): ?>
                                        <?php foreach ($countries_usr as $code=>$country): ?>
                                            <option value="<?php echo $code; ?>"><?php echo $country; ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Password<div class="requiredfld"> *</div></label>
                            <div class="col-sm-8">
                                <input type="password" class="form-control" placeholder="******" name="password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Repeat Password<div class="requiredfld"> *</div></label>
                            <div class="col-sm-8">
                                <input type="password" class="form-control" placeholder="******" name="confirm_password">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-8 col-sm-offset-3">
                                <button class="btn btn-orange" type="submit">Submit New User</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="tab-pane <?php if($tab=="customers"){ echo "active";} ?> " id="customers">
                    <br>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Action</th>
                                    <th>Contact Name</th>
                                    <th>Company Name</th>
                                    <th>Email</th>
                                    <th>Username</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php if (isset($customers)): ?>
                    			<?php foreach ($customers as $customer): ?>
                                    <tr user_id="<?php echo $customer->getUser()->getId(); ?>">
                                        <td>
                                            <?php $user_stat = $customer->getUser()->getStatus(); ?>
                                            <?php if($user_stat == 'pending'): ?>
                                                <a href="#" onclick="updateStatus(this); event.preventDefault();">Activate</a> | <a href="#" onclick="updateStatus(this); event.preventDefault();">Delete</a>
                                            <?php elseif($user_stat == 'active'): ?>
                                                <a href="#" onclick="updateStatus(this); event.preventDefault();">Deactivate</a> | <a href="#" onclick="updateStatus(this); event.preventDefault();">Delete</a>
                                            <?php elseif($user_stat == 'inactive'): ?>
                                                <a href="#" onclick="updateStatus(this); event.preventDefault();" >Activate</a> | <a href="#" onclick="updateStatus(this); event.preventDefault();">Delete</a>
                                            <?php elseif($user_stat == 'deleted'): ?>
                                                <a href="#" onclick="updateStatus(this); event.preventDefault();">Deactivate</a> | <a href="#" onclick="updateStatus(this); event.preventDefault();">Activate</a>
                                            <?php endif; ?>
                                        </td>
                                        <td name="name_td"><?php echo $customer->getUser()->getLastname() . ', '. $customer->getUser()->getFirstname(); ?></td>
                                        <td name="company_td"><?php echo $customer->getUser()->getCompany(); ?></td>
                                        <td name="email_td"><?php echo $customer->getUser()->getEmail(); ?></td>
                                        <td name="email_td"><?php echo $customer->getUser()->getUsername(); ?></td>
                                    </tr>
                                <?php endforeach; ?>
                			<?php endif; ?>
                            </tbody>
                        </table>
                    </div>

                    <!-- paginator for customers -->
                    <div class="row text-center">
                        <?php echo $p_link_customers; ?>
                    </div>
                    <!-- end of paginator for customers -->
                </div>
                <div class="tab-pane  <?php if($tab=="agents"){ echo "active";} ?> " id="agents">
                    <br>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Action</th>
                                    <th>Contact Name</th>
                                    <th>Company Name</th>
                                    <th>Email</th>
                                    <th>Username</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php if (isset($agents)): ?>
                    			<?php foreach ($agents as $agent): ?>
                                    <tr user_id="<?php echo $agent->getUser()->getId(); ?>">
                                        <td>
                                            <a href="#" class="edit_link" data-target="#myModalEditUser">Edit</a> |
                                            <?php $user_stat = $agent->getUser()->getStatus(); ?>
                                            <?php if($user_stat == 'pending'): ?>
                                                <a href="#" onclick="updateStatus(this); event.preventDefault();">Activate</a> | <a href="#" onclick="updateStatus(this); event.preventDefault();">Delete</a>
                                            <?php elseif($user_stat == 'active'): ?>
                                                <a href="#" onclick="updateStatus(this); event.preventDefault();">Deactivate</a> | <a href="#" onclick="updateStatus(this); event.preventDefault();">Delete</a>
                                            <?php elseif($user_stat == 'inactive'): ?>
                                                <a href="#" onclick="updateStatus(this); event.preventDefault();" >Activate</a> | <a href="#" onclick="updateStatus(this); event.preventDefault();">Delete</a>
                                            <?php elseif($user_stat == 'deleted'): ?>
                                                <a href="#" onclick="updateStatus(this); event.preventDefault();">Deactivate</a> | <a href="#" onclick="updateStatus(this); event.preventDefault();">Activate</a>
                                            <?php endif; ?>
                                        </td>
                                        <td name="name_td">     <?php echo $agent->getUser()->getLastname() . ', '. $agent->getUser()->getFirstname(); ?></td>
                                        <td name="company_td">  <?php echo $agent->getUser()->getCompany(); ?></td>
                                        <td name="email_td">    <?php echo $agent->getUser()->getEmail(); ?></td>
                                        <td name="email_td">    <?php echo $agent->getUser()->getUsername(); ?></td>
                                    </tr>
                                <?php endforeach; ?>
                			<?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- paginator for agents -->
                    <div class="row text-center">
                        <?php echo $p_link_agents; ?>
                    </div>
                    <!-- end of paginator agents -->
                </div>
                <div class="tab-pane  <?php if($tab=="providers"){ echo "active";} ?> " id="providers">
                    <br>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Action</th>
                                    <th>Contact Name</th>
                                    <th>Company Name</th>
                                    <th>Email</th>
                                    <th>Username</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php if (isset($providers)): ?>
                    			<?php foreach ($providers as $provider): ?>
                                    <tr user_id="<?php echo $provider->getUser()->getId(); ?>">
                                        <td>
                                            <a href="#" class="edit_link" data-target="#myModalEditUser">Edit</a> |
                                            <?php $user_stat = $provider->getUser()->getStatus(); ?>
                                            <?php if($user_stat == 'pending'): ?>
                                                <a href="#" onclick="updateStatus(this); event.preventDefault();">Activate</a> | <a href="#" onclick="updateStatus(this); event.preventDefault();">Delete</a>
                                            <?php elseif($user_stat == 'active'): ?>
                                                <a href="#" onclick="updateStatus(this); event.preventDefault();">Deactivate</a> | <a href="#" onclick="updateStatus(this); event.preventDefault();">Delete</a>
                                            <?php elseif($user_stat == 'inactive'): ?>
                                                <a href="#" onclick="updateStatus(this); event.preventDefault();">Activate</a> | <a href="#" onclick="updateStatus(this); event.preventDefault();">Delete</a>
                                            <?php elseif($user_stat == 'deleted'): ?>
                                                <a href="#" onclick="updateStatus(this); event.preventDefault();">Deactivate</a> | <a href="#" onclick="updateStatus(this); event.preventDefault();">Activate</a>
                                            <?php endif; ?>
                                        </td>
                                        <td name="name_td">     <?php echo $provider->getUser()->getLastname() . ', '. $provider->getUser()->getFirstname(); ?></td>
                                        <td name="company_td">  <?php echo $provider->getUser()->getCompany(); ?></td>
                                        <td name="email_td">    <?php echo $provider->getUser()->getEmail(); ?></td>
                                        <td name="email_td">    <?php echo $provider->getUser()->getUsername(); ?></td>
                                    </tr>
                                <?php endforeach; ?>
                			<?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- paginator for providers -->
                    <div class="row text-center">
                        <?php echo $p_link_providers; ?>
                    </div>
                    <!-- end of paginator providers -->
                </div>
                <div class="tab-pane  <?php if($tab=="admins"){ echo "active";} ?> " id="admins">
                    <br>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Action</th>
                                    <th>Contact Name</th>
                                    <th>Company Name</th>
                                    <th>Email</th>
                                    <th>Username</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php if (isset($admins)): ?>
                    			<?php foreach ($admins as $admin): ?>
                                    <tr user_id="<?php echo $admin->getUser()->getId(); ?>">
                                        <td>
                                            <a href="#" class="edit_link" data-target="#myModalEditUser">Edit</a> |
                                            <?php $user_stat = $admin->getUser()->getStatus(); ?>
                                            <?php if($user_stat == 'pending'): ?>
                                                <a href="#" onclick="updateStatus(this); event.preventDefault();">Activate</a> | <a href="#" onclick="updateStatus(this); event.preventDefault();">Delete</a>
                                            <?php elseif($user_stat == 'active'): ?>
                                                <a href="#" onclick="updateStatus(this); event.preventDefault();">Deactivate</a> | <a href="#" onclick="updateStatus(this); event.preventDefault();">Delete</a>
                                            <?php elseif($user_stat == 'inactive'): ?>
                                                <a href="#" onclick="updateStatus(this); event.preventDefault();">Activate</a> | <a href="#" onclick="updateStatus(this); event.preventDefault();">Delete</a>
                                            <?php elseif($user_stat == 'deleted'): ?>
                                                <a href="#" onclick="updateStatus(this); event.preventDefault();">Deactivate</a> | <a href="#" onclick="updateStatus(this); event.preventDefault();">Activate</a>
                                            <?php endif; ?>
                                        </td>
                                        <td name="name_td">     <?php echo $admin->getUser()->getLastname() . ', '. $admin->getUser()->getFirstname(); ?></td>
                                        <td name="company_td">  <?php echo $admin->getUser()->getCompany(); ?></td>
                                        <td name="email_td">    <?php echo $admin->getUser()->getEmail(); ?></td>
                                        <td name="email_td">    <?php echo $admin->getUser()->getUsername(); ?></td>
                                    </tr>
                                <?php endforeach; ?>
                			<?php endif; ?>
                            </tbody>
                        </table>
                    </div>

                    <!-- paginator for admins -->
                    <div class="row text-center">
                        <?php echo $p_link_admins; ?>
                    </div>
                    <!-- end of paginator for admins -->
                </div>
                <div class="tab-pane  <?php if($tab=="accounts"){ echo "active";} ?>  " id="accounts">
                    <br>
                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Action</th>
                                    <th>Contact Name</th>
                                    <th>Company Name</th>
                                    <th>Email</th>
                                    <th>Username</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php if (isset($accounts)): ?>
                   				<?php foreach ($accounts as $account): ?>
                                    <tr user_id="<?php echo $account->getUser()->getId(); ?>">
                                        <td>
                                            <a href="#" class="edit_link" data-target="#myModalEditUser">Edit</a> |
                                            <?php $user_stat = $account->getUser()->getStatus(); ?>
                                            <?php if($user_stat == 'pending'): ?>
                                                <a href="#" onclick="updateStatus(this); event.preventDefault();">Activate</a> | <a href="#" onclick="updateStatus(this); event.preventDefault();">Delete</a>
                                            <?php elseif($user_stat == 'active'): ?>
                                                <a href="#" onclick="updateStatus(this); event.preventDefault();">Deactivate</a> | <a href="#" onclick="updateStatus(this); event.preventDefault();">Delete</a>
                                            <?php elseif($user_stat == 'inactive'): ?>
                                                <a href="#" onclick="updateStatus(this); event.preventDefault();">Activate</a> | <a href="#" onclick="updateStatus(this); event.preventDefault();">Delete</a>
                                            <?php elseif($user_stat == 'deleted'): ?>
                                                <a href="#" onclick="updateStatus(this); event.preventDefault();">Deactivate</a> | <a href="#" onclick="updateStatus(this); event.preventDefault();">Activate</a>
                                            <?php endif; ?>
                                        </td>
                                        <td name="name_td">     <?php echo $account->getUser()->getLastname() . ', '. $account->getUser()->getFirstname(); ?></td>
                                        <td name="company_td">  <?php echo $account->getUser()->getCompany(); ?></td>
                                        <td name="email_td">    <?php echo $account->getUser()->getEmail(); ?></td>
                                        <td name="email_td">    <?php echo $account->getUser()->getUsername(); ?></td>
                                    </tr>
                                <?php endforeach; ?>
                			<?php endif; ?>
                            </tbody>
                        </table>
                    </div>

                    <!-- paginator for accounts -->
                    <div class="row text-center">
                        <?php echo $p_link_accounts; ?>
                    </div>
                    <!-- end of paginator for accounts -->
                </div>
            </div>
        </div>
    </div>
</div>

<!-- /#page-wrapper -->


<script>
    function relocateManageUsers(target){
        window.location.href="/panel/manage_user?tab="+target;
    }
</script>

