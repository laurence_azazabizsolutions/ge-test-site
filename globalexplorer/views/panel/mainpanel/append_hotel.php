<tr hotel_id="<?php echo $hotel->getId(); ?>">
    <?php $this->view('panel/mainpanel/append_hotel_details', array('hotel' => $hotel));?>
    <td>
        Hotel Surcharges:
        <a name="addHotelSurcharge_link" href="#">Add</a> <br>
        <table class="table table-striped table-bordered table-hover">
            <tr>
                <th></th>
                <th>Description</th>
                <th>Rule</th>
                <th>Cost</th>
                <th>Profit</th>
                <th>Price</th>
                <th>Rate Type</th>
            </tr>
            <?php $this->view('panel/mainpanel/append_hotelsurcharges', array('hotel' => $hotel));?>
        </table>
    </td>
</tr>