<div id="page-wrapper">
    <!-- /.row -->
    <div class="row">
        <br>
        <div class="col-lg-12">
            <?php echo form_open('','id="report_data" class="form-horizontal" role="form" method="GET"'); ?>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Booking Code</label>
                    <div class="col-sm-8">
                        <input type="text" name="booked_code" <?php echo "value='".@$_GET["booked_code"]."'"; ?>class="form-control">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Passenger Name</label>
                    <div class="col-sm-8">
                        <input type="text" name="name" <?php echo "value='".@$_GET["TRAVELLLERNAME"]."'"; ?>class="form-control">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Agent</label>
                    <div class="col-sm-8">
                        <select class="form-control" name="report_agent_id">
                            <option id="all" value="">- Select -</option>
                            <?php if (isset($report_agent)): ?>
                                <?php foreach ($report_agent as $row): ?>
                                    <option value="<?php echo $row->id; ?>" <?php if(isset($_GET["report_agent_id"])){ if($_GET["report_agent_id"]==$row->id){ echo "selected"; } } ?> ><?php echo $row->compname; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Provider</label>
                    <div class="col-sm-8">
                        <select class="form-control" name="report_provider_id">
                            <option id="all" value="" <?php if(isset($_GET["report_provider_id"])){ if($_GET["report_provider_id"]==""){ echo "selected"; } } ?> >- Select -</option>
                                <?php if (isset($report_provider)): ?>
                                    <?php foreach ($report_provider as $row): ?>
                                        <option value="<?php echo $row->id; ?>" <?php if(isset($_GET["report_provider_id"])){ if($_GET["report_provider_id"]==$row->id){ echo "selected"; } } ?> ><?php echo $row->compname; ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Date</label>
                    <div class="col-sm-4">
                        <div class="input-group date" id="datetimepickerMonthViewOrigin">
                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                            <input class="form-control" <?php echo "value='".@$_GET['depart']."'"; ?> type="text" name="depart" data-date-format="YYYY-MM" placeholder="Departure"/>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="input-group date" id="datetimepickerMonthViewDepart">
                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                            <input class="form-control" <?php echo "value='".@$_GET['return']."'"; ?> type="text" name="return" data-date-format="YYYY-MM" placeholder="Return"/>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Booking Status</label>
                    <div class="col-sm-8">
                         <select  class="form-control" name="book_status">
                            <option value="" <?php if(isset($_GET["book_status"])){ if($_GET["book_status"]==""){ echo "selected"; } } ?>>-</option>
                            <option value="pending" <?php if(isset($_GET["book_status"])){ if($_GET["book_status"]=="pending"){ echo "selected"; } } ?>>Pending</option>
                            <option value="confirmed" <?php if(isset($_GET["book_status"])){ if($_GET["book_status"]=="confirmed"){ echo "selected"; } } ?>>Confirmed</option>
                            <option value="rejected" <?php if(isset($_GET["book_status"])){ if($_GET["book_status"]=="rejected"){ echo "selected"; } } ?>>Rejected</option>
                            <option value="ammended" <?php if(isset($_GET["book_status"])){ if($_GET["book_status"]=="ammended"){ echo "selected"; } } ?>>Amended</option>
                            <option value="cancelled" <?php if(isset($_GET["book_status"])){ if($_GET["book_status"]=="cancelled"){ echo "selected"; } } ?>>Cancelled</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-3">
                        <button class="btn btn-orange">Filter</button>
                        <button type="reset" class="btn btn-default">Clear</button>
                    </div>
                </div>
            </form>

                <div class="panel-group" id="accordion">
                    <div class="panel" style="box-shadow: none;">
                        <div class="panel-heading">
                            <small class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"> Show / Hide Fields</a>
                            </small>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse">
                                <div class="table-responsive" style="margin-bottom:0px;">
                                    <table class="table table-bordered" style="margin-bottom:0px;">
                                        <thead>
                                            <tr>
                                                <th><div class="checkbox">Columns</div></th>
                                                <td><div class="checkbox"><label><input type="checkbox" id="chkCode" CHECKED> Code </label></div></td>
                                                <td><div class="checkbox"><label><input type="checkbox" id="chkDetails" CHECKED> Details </label></div></td>
                                                <td><div class="checkbox"><label><input type="checkbox" id="chkFlight" CHECKED> Flight </label></div></td>
                                                <td><div class="checkbox"><label><input type="checkbox" id="chkAmount" CHECKED> Amount </label></div></td>
                                                <td><div class="checkbox"><label><input type="checkbox" id="chkPayment" CHECKED> Payment </label></div></td>
                                                <td><div class="checkbox"><label><input type="checkbox" id="chkStatus" CHECKED> Status </label></div></td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th><div class="checkbox">Fields</div></th>
                                                <td></td>
                                                <td>
                                                    <div class="checkbox"><label><input type="checkbox" id="chkPackage" CHECKED> Package Name </label></div>
                                                    <div class="checkbox"><label><input type="checkbox" id="chkAgent" CHECKED> Agent Name  </label></div>
                                                    <div class="checkbox"><label><input type="checkbox" id="chkProvider"> Provider Name  </label></div>
                                                    <div class="checkbox"><label><input type="checkbox" id="chkHotel" CHECKED> Hotel Name  </label></div>
                                                    <div class="checkbox"><label><input type="checkbox" id="chkTraveller" CHECKED> Traveller Name </label></div>
                                                </td>
                                                <td>
                                                    <div class="checkbox"><label><input type="checkbox" id="chkOrigin" CHECKED> Origin </label></div>
                                                    <div class="checkbox"><label><input type="checkbox" id="chkReturn" CHECKED> Return </label></div>
                                                </td>
                                                <td>
                                                    <div class="checkbox"><label><input type="checkbox" id="chkCost"> Cost </label></div>
                                                    <div class="checkbox"><label><input type="checkbox" id="chkProfit"> Profit </label></div>
                                                    <div class="checkbox"><label><input type="checkbox" id="chkPrice" CHECKED> Selling </label></div>
                                                </td>
                                                <td>
                                                    <div class="checkbox"><label><input type="checkbox" id="chkAgentPayment" CHECKED> Agent Payment </label></div>
                                                    <div class="checkbox"><label><input type="checkbox" id="chkProviderPayment" CHECKED> Provider Payment </label></div>
                                                </td>
                                                <td></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                        </div>
                    </div>
                </div>
            <div class="clearfix"></div>
            <div class="pull-right">
                
            </div>
            <div class="clearfix"></div>
            <div class="table-responsive">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th class="hideCode">Booking Code</th>
                            <th class="hideDetails">Details</th>
                            <th class="hideFlight">Flight</th>
                            <th class="hideAmount hideCost" style="display:none;">Cost</th>
                            <th class="hideAmount hideProfit" style="display:none;">Profit</th>
                            <th class="hideAmount hidePrice">Selling</th>
                            <th class="hidePayment">Payment</th>
                            <th class="hideStatus">Status</th>
                        </tr>
                    </thead>
                    <tbody id="report_list">
                         <?php if (isset($booked_data)): 
                             $counter=1;
                             $totalcost=0; 
                             $totalprofit=0; 
                             $totalprice=0;

                             $totalp = 0;
                             $profit_count = 0;
                             $loss_count = 0;
                         ?>
                            <?php foreach ($booked_data as $row):
                                if ($row->PSTAT === NULL) {
                                    $pstat_agent = "No";
                                    $pstat_provider = "No";
                                } else {
                                    if ($row->PSTAT === "collected") {
                                        $pstat_agent = "Yes";
                                        $pstat_provider = "No";
                                    } else {
                                        $pstat_agent = "Yes";
                                        $pstat_provider = "Yes";
                                    }
                                }

                                $totalcost+=$row->BOOKINGCOST;
                                $totalprofit+=$row->BOOKINGPROFIT;
                                $totalprice+=$row->BOOKINGPRICE;
                                $tr_color = "";
                                if ($row->BOOKINGSTATUS == "cancelled") {
                                    $tr_color = "danger";
                                } else if ($row->BOOKINGSTATUS == "ammended") {
                                    $row->BOOKINGSTATUS = "amended";
                                }
                            ?>
                                <tr class="<?php echo $tr_color;?>">
                                    <td class="hideCode">
                                        <a href="<?php echo base_url('panel/voucher/'.$row->BOOKID)?>" target="_blank"><?php echo $row->BOOKINGCODE;?></a> <br>
                                        <a href="<?php echo base_url('panel/package/'.$row->BOOKID)?>">View</a>
                                    </td>
                                    <td class="hideDetails">
                                        <div class="hidePackage"><strong>Package:</strong> <?php echo $row->PACKAGENAME; ?></div>
                                        <div class="hideAgent"><strong>Agent:</strong> <?php echo $row->COMPANYNAME; ?></div>
                                        <div class="hideProvider" style="display:none;"><strong>Provider:</strong> <?php echo $row->COMPANYNAMEP; ?></div>
                                        <div class="hideHotel"><strong>Hotel:</strong> 
                                            <?php 
                                                // $tmp=array();
                                                // foreach ($row->hotel as $key){
                                                //     $tmp[]=$key->hotels_name; 
                                                // }
                                                // echo implode(' ,',$tmp);
                                                $hotelname = $this->booking->getHotelName($row->BOOKINGCODE);
                                                 //dump($hotelname);
                                                 foreach ($hotelname as $hn) {
                                                     echo $hn['name'];
                                                 }
                                            ?>
                                        </div>
                                        <div class="hideTraveller"><strong>Traveller:</strong> <?php echo $row->TRAVELLLERNAME ." (".$row->PAX." PAX)"; ?></div>
                                    </td>
                                    <td class="hideFlight">
                                        <div class="hideOrigin">
                                            <strong>Ori:</strong> <?php echo date("d-M-Y", mktime(0,0,0,substr($row->BOOKINGDEPARTUREDATE,5,2), substr($row->BOOKINGDEPARTUREDATE,8,2), substr($row->BOOKINGDEPARTUREDATE,0,4))); ?><br>
                                            <i><?php echo $row->OFDEPARTTIME; ?>-<?php echo $row->OFARRIVALTIME; ?> hrs</i>
                                        </div>
                                        <div class="hideReturn">
                                            <strong>Ret:</strong> <?php echo date("d-M-Y", mktime(0,0,0,substr($row->BOOKINGRETURNDATE,5,2), substr($row->BOOKINGRETURNDATE,8,2), substr($row->BOOKINGRETURNDATE,0,4))); ?><br>
                                            <i><?php echo $row->RFDEPARTTIME; ?>-<?php echo $row->RFARRIVALTIME; ?> hrs</i>
                                        </div>
                                    </td>
                                    <td class="hideAmount hideCost" style="display:none;">
                                        <div><strong>Cost:</strong> <?php echo $row->BOOKINGCOST; ?></div>
                                     </td>
                                    <td class="hideAmount hideProfit" style="display:none;">
                                       <div><strong>Profit:</strong> <?php echo $row->BOOKINGPROFIT; ?></div>
                                     </td>
                                    <td class="hideAmount hidePrice">
                                        <div><strong>Price:</strong> <?php echo $row->BOOKINGPRICE-$row->BOOKINGDISCOUNT; ?></div>
                                    </td>
                                    <td class="hidePayment">
                                        <div class="hideAgentPayment"><strong>Agent Paid:</strong> <?php echo $pstat_agent; ?></div>
                                        <div class="hideProviderPayment"><strong>Provider Paid:</strong> <?php echo $pstat_provider; ?></div>
                                    </td>
                                    <td class="hideStatus"><?php echo $row->BOOKINGSTATUS; ?></td>
                                </tr>
                            <?php $counter++; 
                            if($row->BOOKINGPROFIT < 0):
                                $loss_count++;
                            else:
                                $profit_count++;
                            endif; 
                            $totalp += $row->BOOKINGPROFIT;
                              endforeach; ?>
                        <?php endif; ?>
                                <tr>
                                    <td colspan="5"><p class="pull-right"><strong>Total Profit:</p></strong></td>
                                    <td><?php echo '$ '.number_format($totalp); ?></td>
                                </tr>
                                 <tr>
                                    <td colspan="5"><p class="pull-right"><strong>Total Profit Count:</p></strong></td>
                                    <td><?php echo $profit_count; ?></td>
                                </tr>
                                <tr>
                                    <td colspan="5"><p class="pull-right"><strong>Total Loss Count:</p></strong></td>
                                    <td><?php echo $loss_count; ?></td>
                                </tr>
                                
                               
                    </tbody>
                </table>
            </div>
                
        </div>
    </div>

    <div class="row text-center">
        <?php echo $page_links; ?>
    </div>

</div>
<!-- /#page-wrapper -->