                    <?php 
                        if($tracks != '' || $tracks != NULL)
                        {
                            if($tracks['num_pax'] != '' || $tracks['num_pax'] != null || $tracks["ext_days"] != '' || $tracks["ext_days"] != null || $tracks["travel_dates"] != '' || $tracks["travel_dates"] != null || $tracks["hotel"] != '' || $tracks["hotel"] != null || $tracks['passenger_detail'] != '' || $tracks['passenger_detail'] != null  || $tracks['flight_detail'] != '' || $tracks['flight_detail'] != null )
                            {
                                echo '<h4 style="font-size: 18px; margin-top: 10px; margin-bottom: 0px;">List of changes:</h4>';
                                foreach($tracks as $row => $value)
                                {
                                    if($value !== NULL)
                                    {
                                      echo "<i><strong style='color: red;'>".$value.'</strong></i>, ';
                                    }
                                } 
                                echo '<br><br>';
                            }
                            else
                            {
                                echo '<h4 style="font-size: 18px; margin-top: 10px; margin-bottom: 0px;">No details were changed</h4><br><br>';
                            }
                        }
                            
                    ?>
                    <table style="width: 770px; margin-bottom: 20px; border-collapse: collapse;">
                        <tr>
                            <td>
                                <p style="margin: 0;">
                                    <strong>Booked On:</strong> <?php echo date('d-M-Y',strtotime(@$data_summary[0]->BOOKDATE));?><br>
                                    <strong>Package:</strong> <?php echo @$data_summary[0]->PACNIGHTS - 1;?> nights<br>
                                    <strong>Extension:</strong>
                                    <?php 
                                        $total_extended= @$data_summary[0]->BOOKEDEXTENDDAYS;
                                        echo $total_extended;
                                    ?>
                                    nights<br>
                                    <strong>Total:</strong>
                                    <?php 
                                        $totals= $total_extended + @$data_summary[0]->PACNIGHTS - 1;     
                                        echo  $totals;
                                    ?>
                                    nights <br><br>

                                    <strong>Booking No:</strong> <?php echo @$data_summary[0]->BOOKNO;?> <br>
                                    <strong>Tour Code:</strong> <?php echo @$data_summary[0]->PACTOURCODE;?> <br>

                                    <?php
                                        if (@$data_summary[0]->BOOKSTATUS == "ammended") {
                                            @$data_summary[0]->BOOKSTATUS = "amended";
                                        }
                                    ?>
                                    <strong>Status:</strong> <?php echo @$data_summary[0]->BOOKSTATUS;?>
                                </p>
                            </td>
                        </tr>
                    </table>

                    <strong>Booking Reference</strong>
                    <table style="width: 770px; margin-bottom: 20px; border-collapse: collapse;">
                        <thead>
                            <?php
                                if (isset($data_summary)) {
                                    $rows = 1;
                                    $cols = 1;
                                    $tbl_arr = array();

                                    $adult = 0;
                                    $child = 0;
                                    $infant = 0;
                                    $pax = 0;
                                    foreach ($data_summary as $row){
                                        //count user type
                                        if (@$row->TRAVELTYPE == "adult") {
                                            $adult++;
                                            $pax++;
                                        }
                                        else if (@$row->TRAVELTYPE == "child") {
                                            $child++;
                                            $pax++;
                                        }
                                        else if (@$row->TRAVELTYPE == "infant") {
                                            $infant++;
                                            $pax++;
                                        }

                                        //initialize users
                                        if ($cols <= 5) {
                                            $tbl_arr[$rows][$cols] = @$row->TRAVELNAME ." (". ucfirst(substr(@$row->TRAVELTYPE, 0, 1)).")";
                                            $cols++;
                                        }
                                        else {
                                            $cols = 1;
                                            $rows++;
                                            $tbl_arr[$rows][$cols] = @$row->TRAVELNAME ." (". ucfirst(substr(@$row->TRAVELTYPE, 0, 1)).")";
                                            $cols++;
                                        }
                                    }
                                }
                            ?>
                            <tr>
                                <td style="padding: 8px; vertical-align: bottom; border: 1px solid #dddddd;"><strong>Name if Passenger/s:</strong></td>
                                <td style="padding: 8px; vertical-align: bottom; border: 1px solid #dddddd;"><strong>(A)dult: <?php echo $adult; ?></strong></td>
                                <td style="padding: 8px; vertical-align: bottom; border: 1px solid #dddddd;"><strong>(C)hild: <?php echo $child; ?></strong></td>
                                <td style="padding: 8px; vertical-align: bottom; border: 1px solid #dddddd;"><strong>(I)nfant: <?php echo $infant; ?></strong></td>
                                <td style="padding: 8px; vertical-align: bottom; border: 1px solid #dddddd;"><strong>No of Pax: <?php echo $pax; ?></strong></td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                for ($r=1; $r <= $rows; $r++) {
                                    echo "<tr>";
                                    for ($c=1; $c <= 5 ; $c++) { 
                                        echo "<td style='padding: 8px; line-height: 1.428571429; vertical-align: top; border: 1px solid #dddddd;'>";
                                            if (@$tbl_arr[$r][$c] != "" || @$tbl_arr[$r][$c] != NULL) {
                                                echo $tbl_arr[$r][$c];
                                            }
                                        echo "</td>";
                                    }
                                    echo "</tr>";
                                }
                            ?>
                        </tbody>
                    </table>

                    <strong>Your Itinerary</strong>
                    <table style="width: 770px; margin-bottom: 20px; border-collapse: collapse;">
                        <thead>
                            <tr>
                                <td style="padding: 8px; vertical-align: bottom; border: 1px solid #dddddd;"><strong>Date</strong></td>
                                <td style="padding: 8px; vertical-align: bottom; border: 1px solid #dddddd;"><strong>Flight No.</strong></td>
                                <td style="padding: 8px; vertical-align: bottom; border: 1px solid #dddddd;"><strong>From</strong></td>
                                <td style="padding: 8px; vertical-align: bottom; border: 1px solid #dddddd;"><strong>To</strong></td>
                                <td style="padding: 8px; vertical-align: bottom; border: 1px solid #dddddd;"><strong>Departure</strong></td>
                                <td style="padding: 8px; vertical-align: bottom; border: 1px solid #dddddd;"><strong>Arriving</strong></td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="padding: 8px; line-height: 1.428571429; vertical-align: top; border: 1px solid #dddddd;"><?php echo date('M d, Y',strtotime(@$data_summary[0]->PACKCHECKIN)); ?></td>
                                <td style="padding: 8px; line-height: 1.428571429; vertical-align: top; border: 1px solid #dddddd;"><?php echo @$data_summary[0]->BOOKDEPARTFLIGHTNO; ?></td>
                                <td style="padding: 8px; line-height: 1.428571429; vertical-align: top; border: 1px solid #dddddd;"><?php echo @$data_summary[0]->BOOKDEPFROM; ?></td>
                                <td style="padding: 8px; line-height: 1.428571429; vertical-align: top; border: 1px solid #dddddd;"><?php echo @$data_summary[0]->BOOKDEPTO; ?></td>
                                <td style="padding: 8px; line-height: 1.428571429; vertical-align: top; border: 1px solid #dddddd;"><?php echo @$data_summary[0]->BOOKDEPARTDATE; ?></td>
                                <td style="padding: 8px; line-height: 1.428571429; vertical-align: top; border: 1px solid #dddddd;"><?php echo @$data_summary[0]->BOOKDEPARTADATEARRIVAL; ?></td>
                            </tr>
                            <tr>
                                <td style="padding: 8px; line-height: 1.428571429; vertical-align: top; border: 1px solid #dddddd;"><?php echo date('M d, Y',strtotime(@$data_summary[0]->PACKCHECKOUT)); ?></td>
                                <td style="padding: 8px; line-height: 1.428571429; vertical-align: top; border: 1px solid #dddddd;"><?php echo @$data_summary[0]->BOOKRETURNFLIGHTNO; ?></td>
                                <td style="padding: 8px; line-height: 1.428571429; vertical-align: top; border: 1px solid #dddddd;"><?php echo @$data_summary[0]->BOOKRETFROM; ?></td>
                                <td style="padding: 8px; line-height: 1.428571429; vertical-align: top; border: 1px solid #dddddd;"><?php echo @$data_summary[0]->BOOKRETTO; ?></td>
                                <td style="padding: 8px; line-height: 1.428571429; vertical-align: top; border: 1px solid #dddddd;"><?php echo @$data_summary[0]->BOOKRETURNDATE; ?></td>
                                <td style="padding: 8px; line-height: 1.428571429; vertical-align: top; border: 1px solid #dddddd;"><?php echo @$data_summary[0]->BOOKRETURNADATEARRIVAL; ?></td>
                            </tr>
                        </tbody>
                    </table>
                    <p>All times indicated are the local times at the relevant airports.</p>

                    <?php
                        if(count($destination_summary)!=0){
                            for($i=0;$i<count($destination_summary);$i++){
                                $destination = $destination_summary[$i];

                                $tot_rooms = 0;
                                if(count($r_summary[$i])!=0){
                                    foreach ($r_summary[$i] as $r) {
                                        $desc = $r['description'];
                                        $desc_exp = explode(" - ", $desc);
                                        if (count($desc_exp) == 1) { $desc_val = $desc_exp[0]; }
                                        else { $desc_val = $desc_exp[count($desc_exp)-1]; }

                                        $comp = $r['computation'];
                                        $comp_exp = explode(" ", $comp);
                                        $comp_val = intval(strip_tags($comp_exp[0]));

                                        if ($desc_val == "Single") { $rooms=$comp_val; }
                                        else if ($desc_val == "Twin") { $rooms=($comp_val/2); }
                                        else if ($desc_val == "Double") { $rooms=($comp_val/2); }
                                        else if ($desc_val == "Triple") { $rooms=($comp_val/3); }
                                        else if ($desc_val == "Quadruple") { $rooms=($comp_val/4); }
                                        else if ($desc_val == "Child Half Twin") { $rooms=($comp_val/2); }
                                        else if ($desc_val == "Child With Bed") { $rooms=0; }
                                        else if ($desc_val == "Child Without Bed") { $rooms=0; }

                                        $tot_rooms+=$rooms;
                                    }
                                }
                    ?>
                                <strong>Your Hotel Rooms</strong>
                                <table style="width: 770px; margin-bottom: 20px; border-collapse: collapse;">
                                    <thead>
                                        <tr>
                                            <td style="padding: 8px; vertical-align: bottom; border: 1px solid #dddddd;"><strong>Hotel</strong></td>
                                            <td style="padding: 8px; vertical-align: bottom; border: 1px solid #dddddd;"><strong>Check In Date</strong></td>
                                            <td style="padding: 8px; vertical-align: bottom; border: 1px solid #dddddd;"><strong>Check Out Date</strong></td>
                                            <td style="padding: 8px; vertical-align: bottom; border: 1px solid #dddddd;"><strong>Total Rooms Utilize</strong></td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="padding: 8px; line-height: 1.428571429; vertical-align: top; border: 1px solid #dddddd;"><?php echo $r_summary[$i][0]['hotel_name']; ?></td>
                                            <td style="padding: 8px; line-height: 1.428571429; vertical-align: top; border: 1px solid #dddddd;"><?php echo date('M d, Y',strtotime(@$data_summary[0]->PACKCHECKIN));?></td>
                                            <td style="padding: 8px; line-height: 1.428571429; vertical-align: top; border: 1px solid #dddddd;"><?php echo date('M d, Y',strtotime(@$data_summary[0]->PACKCHECKOUT));?></td>
                                            <td style="padding: 8px; line-height: 1.428571429; vertical-align: top; border: 1px solid #dddddd;"><?php echo round($tot_rooms); ?></td>
                                        </tr>
                                    </tbody>
                                </table>

                                <p>Types of Rooms:</p>
                                <?php
                                    echo '<table style="width: 770px; margin-bottom: 20px; border-collapse: collapse;">
                                        <tr>';

                                        $i_cnt = 0;
                                        $i_data = count($r_summary[$i]);
                                        foreach ($r_summary[$i] as $r) {

                                            $desc = $r['description'];
                                            $desc_exp = explode(" - ", $desc);
                                            if (count($desc_exp) == 1) { $desc_val = $desc_exp[0]; }
                                            else { $desc_val = $desc_exp[count($desc_exp)-1]; }

                                            $comp = $r['computation'];
                                            $comp_exp = explode(" ", $comp);
                                            $comp_val = intval(strip_tags($comp_exp[0]));

                                            if ($desc_val == "Single") { $rooms=$comp_val; }
                                            else if ($desc_val == "Twin") { $rooms=($comp_val/2); }
                                            else if ($desc_val == "Double") { $rooms=($comp_val/2); }
                                            else if ($desc_val == "Triple") { $rooms=($comp_val/3); }
                                            else if ($desc_val == "Quadruple") { $rooms=($comp_val/4); }
                                            else if ($desc_val == "Child Half Twin") { $rooms=($comp_val/2); }
                                            else if ($desc_val == "Child With Bed") { $rooms=$comp_val; }
                                            else if ($desc_val == "Child Without Bed") { $rooms=$comp_val; }

                                            if ($i_cnt >= 6) {
                                                $i_cnt=1;
                                                echo "</tr><tr>
                                                    <td style='padding: 8px; line-height: 1.428571429; vertical-align: top; border: 1px solid #dddddd;'>
                                                        <strong>{$desc}</strong><br>
                                                        ".round($rooms)."
                                                    </td>";
                                            }
                                            else {
                                                $i_cnt++;
                                                echo "<td style='padding: 8px; line-height: 1.428571429; vertical-align: top; border: 1px solid #dddddd;'>
                                                    <strong>{$desc}</strong><br>
                                                    ".round($rooms)."
                                                </td>";
                                            }
                                        }
                                        
                                        if ($i_data>6) {
                                            $i_cols = $i_data % 6;
                                            if ($i_cols != 0) {
                                                $i_mods = 6 - $i_cols;
                                                echo "<td colspan='{$i_mods}' style='padding: 8px; line-height: 1.428571429; vertical-align: top; border: 1px solid #dddddd;'>-</td>";
                                            }
                                        }
                                    echo '</tr></table>';
                                ?>
                    <?php
                            }
                        }
                    ?>
                    <?php if(count($addon_summary)!=0){ ?>
                        Addons:
                        <table style="width: 770px; margin-bottom: 20px; border-collapse: collapse;">
                            <thead>
                                <tr>
                                    <td style="padding: 8px; vertical-align: bottom; border: 1px solid #dddddd;"><strong>Description</strong></td>
                                    <td style="padding: 8px; vertical-align: bottom; border: 1px solid #dddddd; width:200px;"><strong>Computation</strong></td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    foreach ($addon_summary as $add_sum) {
                                        echo "<tr>
                                            <td style='padding: 8px; line-height: 1.428571429; vertical-align: top; border: 1px solid #dddddd;'>{$add_sum['description']}</td>
                                            <td style='padding: 8px; line-height: 1.428571429; vertical-align: top; border: 1px solid #dddddd;'>{$add_sum['computation']}</td>
                                        </tr>";
                                    }
                                ?>
                            </tbody>
                        </table>
                    <?php } ?>

                    <?php if(count($extension_summary)!=0){ ?>
                        Extensions:
                        <table style="width: 770px; margin-bottom: 20px; border-collapse: collapse;">
                            <thead>
                                <tr>
                                    <td style="padding: 8px; vertical-align: bottom; border: 1px solid #dddddd;"><strong>Description</strong></td>
                                    <td style="padding: 8px; vertical-align: bottom; border: 1px solid #dddddd; width:200px;"><strong>Computation</strong></td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $remark = "";
                                    foreach ($extension_summary as $ex_sum) {
                                        $remark = $ex_sum["remark"];
                                        echo "<tr>
                                            <td style='padding: 8px; line-height: 1.428571429; vertical-align: top; border: 1px solid #dddddd;'>{$ex_sum['description']}</td>
                                            <td style='padding: 8px; line-height: 1.428571429; vertical-align: top; border: 1px solid #dddddd;'>{$ex_sum['computation']}</td>
                                        </tr>";
                                    }
                                ?>
                            </tbody>
                        </table>
                    <?php } ?>

