<?php foreach ($surcharges as $rule_type=>$surcharge_types): ?>    
   <table id="<?php echo $rule_type;?>_surcharge_tbl" class="table table-striped table-bordered">
        <tr>
            <th class="brown text-center" colspan="3"><?php echo ucwords(str_replace('_', ' ', $rule_type)); ?></th>
        </tr>
        <tr>
            <th>Description</th>
            <th>Price</th>
            <th>Qty</th>
        </tr>
            <?php foreach ($surcharge_types as $surcharge): ?>    
                <tr>
                    <td><?php echo $surcharge['description']; ?>
                    <?php if(isset($surcharge['type'])) echo '('.ucwords(substr($surcharge['type'], 0, -1)).')'; ?>
                    </td>
                    <td><?php echo ucwords(str_replace('_', ' ', $surcharge['rule_type'])); ?> : <?php echo $surcharge['rule']; ?></td>
                    <td>
                        <?php $price = ($surcharge['cost'] + $surcharge['profit']); echo $price; ?> / <?php echo ucwords(str_replace('/', ' / ', $surcharge['rate_type'])); ?>
                        <input name="<?php echo $surcharge['table'];?>_surcharge_id[]" type="hidden" value="<?php echo $surcharge['id']; ?>">
                        <input name="<?php echo $surcharge['table'];?>_surcharge_price[]" type="hidden" value="<?php echo $price; ?>">
                    </td>
                    <!-- <td><input name="surcharge_quantity[]" class="form-control min-size-num" type="number"></td> -->
                </tr>
            <?php endforeach;?>
    </table>
<?php endforeach;?>

