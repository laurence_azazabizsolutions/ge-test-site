
<div id="page-wrapper">
    <!-- /.row -->
    <div class="row">
        <br>
        <div class="col-lg-12">

            <form class="form-horizontal" role="form">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Agent</label>
                    <div class="col-sm-8">
                        <select class="form-control">
                            <option>Agent Name List</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Booking Status</label>
                    <div class="col-sm-8">
                        <select class="form-control">
                            <option>-</option>
                            <option>Pending</option>
                            <option selected>Confirmed</option>
                            <option>Rejected</option>
                            <option>Amended</option>
                            <option>Cancelled</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Date</label>
                    <div class="col-sm-4">
                        <select class="form-control">
                            <option>Current Month</option>
                        </select>
                        <br>
                    </div>
                    <div class="col-sm-4">
                        <select class="form-control">
                            <option>Current Year</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-3">
                        <button type="reset" class="btn btn-orange">Filter</button>
                        <button type="reset" class="btn btn-default">Clear</button>
                    </div>
                </div>
            </form>
    
            <!-- Nav tabs -->
            <ul class="nav nav-pills" role="tablist">
                <li class="active"><a href="#pendingpayments" role="tab" data-toggle="tab">Pending Payments</a></li>
                <li><a href="#addcollection" role="tab" data-toggle="tab">Add Collection</a></li>
                <li><a href="#viewcollection" role="tab" data-toggle="tab">View Collection</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">

                <div class="tab-pane active" id="pendingpayments">
                    <h2>December 2011</h2>
                    <br>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Booking Code</th>
                                    <th>Details</th>
                                    <th>Flight</th>
                                    <th>Amount</th>
                                    <th>Booking Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $totalAmount = 0; $total = 0;?>
                                <?php foreach($booked_data as $row) :?>
                                <?php if($row->PDATE === NULL) : ?>
                                    <?php  if($row->BOOKINGSTATUS === 'cancelled')  {$tr = 'class="danger"';} else {$tr = ''; }?>
                                    <?php $button = ''; if($row->BOOKINGSTATUS !== 'confirmed') {$button = 'style="display:none;"';}?>
                                    <tr <?php echo $tr;?> id="<?php echo $row->BOOKID;?>">
                                        <td>
                                            <a href="<?php echo base_url('panel/voucher/'.$row->BOOKID)?>" target="_blank"><?php echo $row->BOOKINGCODE;?></a> <br>
                                            <a href="<?php echo base_url('panel/package/'.$row->BOOKID)?>">View</a>
                                        </td>
                                        <td>
                                            <strong>Package:</strong><?php echo $row->PACKAGENAME; ?><br>
                                            <strong>Agent:</strong> <?php echo $row->AGENTFNAME; ?> <?php echo $row->AGENTLNAME; ?> <br>
                                            <strong>Provider:</strong> <?php echo $row->PROVIDERFNAME; ?> <?php echo $row->PROVIDERLNAME; ?><br>
                                            <strong>Hotel:</strong> 
                                            <?php 
                                            $tmp=array();
                                            foreach ($booked_hotel_data as $key){
                                                $tmp[]=$key->hotels_name; 
                                            }
                                            echo implode(' ,',$tmp);
                                            ?> <br>
                                            <strong>Traveller:</strong> <?php echo $row->TRAVELLLERNAME; ?>
                                        </td>
                                        <td>
                                            <strong>Ori:</strong> <?php echo $row->BOOKINGDEPARTUREDATE; ?> <br>
                                            <?php echo $row->ARRIVALTIME; ?> hrs - <?php echo $row->RETURNTIME; ?> hrs <br>
                                            <strong>Ret:</strong><?php echo $row->BOOKINGRETURNDATE; ?><br>
                                            <?php echo $row->RETURNTIME; ?> hrs - <?php echo $row->ARRIVALTIME; ?> hrs
                                        </td>
                                        <td><?php echo $row->BOOKINGCOST; ?></td>
                                        <td><?php echo $row->BOOKINGSTATUS; ?></td>
                                        <?php echo form_open('panel/collect','id="form'.$total.'"');?>
                                            <td>
                                                <div id="loader<?php echo $row->BOOKID;?>" style="background: transparent url(<?php echo base_url('./img/loader.gif');?>) no-repeat center;background-size:100% auto;height:25px;width:25px;margin:0px auto;display:none;"></div>
                                                <button type="button" value="<?php echo $row->BOOKID.'IamADelimiter'.$row->BOOKINGCOST;?>" onclick="clickME(this);"class="btn btn-default" id="btn_collect<?php echo $total;?>"<?php echo $button;?> >Collect</button>
                                            </td>
                                        <?php echo form_close();?>
                                    </tr>
                                <?php $total++; $totalAmount += $row->BOOKINGCOST; endif;endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="tab-pane" id="addcollection">
                    <h2>December 2011</h2>
                    <br>
                    <div class="table-responsive">
                         <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Booking Code</th>
                                    <th>Details</th>
                                    <th>Flight</th>
                                    <th>Amount</th>
                                    <th>Booking Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $totalAmount = 0;?>
                                <?php foreach($booked_data as $row) :?>
                                    <?php $total = 1; if($row->BOOKINGSTATUS === 'cancelled')  {$tr = 'class="danger"';} else {$tr = ''; }?>
                                    <?php $button = ''; if($row->BOOKINGSTATUS !== 'confirmed') {$button = 'style="display:none;"';}?>
                                    <tr <?php echo $tr;?>  >
                                        <td>
                                            <a href="<?php echo base_url('panel/voucher/'.$row->BOOKID)?>" target="_blank"><?php echo $row->BOOKINGCODE;?></a> <br>
                                            <a href="<?php echo base_url('panel/package/'.$row->BOOKID)?>">View</a>
                                        </td>
                                        <td>
                                            <strong>Package:</strong> <?php echo $row->PACKAGENAME; ?><br>
                                            <strong>Agent:</strong> <?php echo $row->AGENTFNAME; ?> <?php echo $row->AGENTLNAME; ?><br>
                                            <strong>Provider:</strong> <?php echo $row->PROVIDERFNAME; ?> <?php echo $row->PROVIDERLNAME; ?><br>
                                            <strong>Hotel:</strong> <?php echo $row->HOTELSNAME; ?><br>
                                            <strong>Traveller:</strong> <?php echo $row->TRAVELLLERNAME; ?>
                                        </td>
                                        <td>
                                            <strong>Ori:</strong> <?php echo $row->BOOKINGDEPARTUREDATE; ?> <br>
                                            <?php echo $row->ARRIVALTIME; ?> hrs - <?php echo $row->RETURNTIME; ?> hrs <br>
                                            <strong>Ret:</strong><?php echo $row->BOOKINGRETURNDATE; ?><br>
                                            <?php echo $row->RETURNTIME; ?> hrs - <?php echo $row->ARRIVALTIME; ?> hrs
                                        </td>
                                        <td><?php echo $row->BOOKINGCOST; ?></td>
                                        <td><?php echo $row->BOOKINGSTATUS; echo $row->PSTAT;?></td>
                                        <td>
                                            <input type="checkbox"></input>
                                        </td>
                                    </tr>
                                <?php $total++; $totalAmount += $row->BOOKINGCOST;endforeach; ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th>Total: <?php echo $total;?> Booking(s)</th>
                                    <th></th>
                                    <th><?php echo $totalAmount;?></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <button type="reset" class="btn btn-orange">Submit</button>
                </div>

                <div class="tab-pane" id="viewcollection">
                    <h2>Cheque number: uob 9768ASDASDS43</h2>
                    <button type="reset" class="btn btn-default">Revoke</button>
                    <br><br>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Booking Code</th>
                                    <th>Details</th>
                                    <th>Flight</th>
                                    <th>Amount</th>
                                    <th>Date Collected</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $totalAmount = 0;$total = 0;?>
                                <?php foreach($booked_data as $row) :?>
                                    <?php if($row->PDATE !== NULL) : ?>
                                    <?php  if($row->BOOKINGSTATUS === 'cancelled')  {$tr = '';} else {$tr = ''; }?>
                                    <?php $button = ''; if($row->BOOKINGSTATUS !== 'confirmed') {$button = 'style="display:none;"';}?>
                                    <tr <?php echo $tr;?> >
                                        <td>
                                            <a href="<?php echo base_url('panel/voucher/'.$row->BOOKID)?>" target="_blank"><?php echo $row->BOOKINGCODE;?></a> <br>
                                            <a href="<?php echo base_url('panel/package/'.$row->BOOKID)?>">View</a>
                                        </td>
                                        <td>
                                            <strong>Package:</strong><?php echo $row->PACKAGENAME; ?><br>
                                            <strong>Agent:</strong> <?php echo $row->AGENTFNAME; ?> <?php echo $row->AGENTLNAME; ?><br>
                                            <strong>Provider:</strong> <?php echo $row->PROVIDERFNAME; ?> <?php echo $row->PROVIDERLNAME; ?><br>
                                            <strong>Hotel:</strong> <?php echo $row->HOTELSNAME; ?><br>
                                            <strong>Traveller:</strong> <?php echo $row->TRAVELLLERNAME; ?>
                                        </td>
                                        <td>
                                            <strong>Ori:</strong> <?php echo $row->BOOKINGDEPARTUREDATE; ?> <br>
                                            <?php echo $row->ARRIVALTIME; ?> hrs - <?php echo $row->RETURNTIME; ?> hrs <br>
                                            <strong>Ret:</strong> <?php echo $row->BOOKINGRETURNDATE; ?><br>
                                            <?php echo $row->RETURNTIME; ?> hrs - <?php echo $row->ARRIVALTIME; ?> hrs
                                        </td>
                                        <td><?php echo $row->BOOKINGCOST; ?></td>
                                        <td><?php echo $row->PDATE; ?></td>
                                    </tr>
                                 <?php $total++; $totalAmount += $row->BOOKINGCOST; endif;endforeach; ?>
                            </tbody>
                            <tfoot> 
                                <tr>
                                    <th></th>
                                    <th>Total: <?php echo $total;?> Booking(s)</th>
                                    <th></th>
                                    <th><?php echo $totalAmount;?></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <br>
            </div>
        </div>
    </div>
</div>



<!-- /#page-wrapper -->