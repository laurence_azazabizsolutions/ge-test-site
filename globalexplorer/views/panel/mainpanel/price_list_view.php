<div class="batch-modal" style="height:100%; position: fixed; z-index: 9999999; width: 100%; background: none repeat scroll 0px 0px rgba(51, 51, 51, 0.7);">
	<div class="container">
		<div class="row">
			<div class="batch_modal_panel">
				<div id="myModalPriceNew" class="col-md-12" style="overflow-y: auto; background: none repeat scroll 0% 0% rgb(255, 255, 255); margin: 5% 0px 0px; border-radius: 10px; padding: 20px;">
					<div class="batch-header">
                        <button id="batch_close" class="pull-right btn btn-danger btn-sm" type="button"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                        <h4 id="myModalLabel" class="modal-title">Download Price List</h4>
                    </div>
                    <div class="batch-body">
                    	<ul class="nav nav-pills price_ hide_package_view_display_tabs" role="tablist">
                    		<?php 
				                if (isset($data)) {
			                        $dest_act = false;
			                        foreach ($data as $key) {
			                            if ($dest_act === false) {
			                                echo '<li class="active"><a href="#'.@$key->id.'" id="'.@$key->id.'" onClick="priceList(\''.@$key->id.'\',this)" role="tab" data-toggle="tab">'.@$key->country.'</a></li>';
			                                $dest_act = true;
			                            } else { echo '<li><a href="#'.@$key->id.'" id="'.@$key->id.'" onClick="priceList(\''.@$key->id.'\',this)" role="tab" data-toggle="tab">'.@$key->country.'</a></li>'; }
			                        }
			                    }
	                        ?>
                    	</ul>
			            <form class="form-horizontal hide_package_view_display_selects" role="form" name="form">
			                <div class="form-group">
			                    <label class="col-sm-3 control-label text-right">Packages</label>
			                    <div class="col-sm-8">
			                        <select class="form-control" name="cbxpricelist" >
			                            <option value=" ">- Select -</option>
			                            <?php if (isset($data)): ?>
			                                <?php foreach ($data as $key): ?>
			                                    <option value="<?php echo @$key->id; ?>" ><?php echo @$key->country; ?></option>
			                                <?php endforeach; ?>
			                            <?php endif; ?>
			                        </select>
			                    </div>
			                </div>
			            </form>
                    	<br>
                    	<div id="displays_price_list">
	                    	
			            </div>

                                  
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(function(){
	  $(window).resize(function(){
	    var windows = $(this);
	    $('.batch-wrap').find('.modal-body').css('height',windows.height() - 315);
	  });
	  $(window).resize();
	});
	function priceList($code,$data) {
		var data_id = $.trim($($data).attr('id'));
		$("[name='cbxpricelist']").val(data_id).trigger('change');
	}
	$('[name=cbxpricelist]').change(function(e){
        	e.preventDefault();
			var code = $(this).val();
			try{
	        	$div_package = $('#displays_price_list');
	        	$div_pagination = $('.row.text-center');
	        	url_link = "<?php echo base_url('panel/filter_view_price_jqrys'); ?>";
	    		$.post(url_link,  {"packagesId": code} , function(result) {
	    			// return false;
	    			return_arr 	= result.split('_:_');
	    			links	 	= return_arr[0];
	    			packages 	= return_arr[1];
	    			$div_package.empty();
	    			$div_package.append(packages);
	    			return true;
		    	});
			}catch(err){
				bootbox.alert("Package not found!");
			}
	    });
</script>