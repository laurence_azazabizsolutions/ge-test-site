<div id="page-wrapper">
    <!-- /.row -->
    <div class="row">
        <br>
        <div class="col-md-12">
            <div class="stepwizard">
                <div class="stepwizard-row">
                    <div class="stepwizard-step">
                        <button type="button" class="btn btn-default btn-circle">2</button>
                        <p>Hotel<?echo ?></p>
                    </div>
                    <div class="stepwizard-step">
                        <button type="button" class="btn btn-orange btn-circle" disabled="disabled">3</button>
                        <p>Details</p>
                    </div> 
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?php
                $img = base_url('../img/feat/default.png');
                if($package->getImagePath()!='') $img = base_url('../'.$package->getImagePath());
              ?>
            <img class="thumbnail img-responsive" src="<?php echo $img; ?>" alt="...">
        </div>
        <div class="col-md-8">
            <h4 style="margin-top:0px;"><?php echo $package->getTitle();?> (Effective from <?php echo $package->getDateFrom();?> - <?php echo $package->getDateTo();?>)</h4>
            <strong>For <?php echo (isset($prev_details['adult_count']) && $prev_details['adult_count'] > 0) ? $prev_details['adult_count'] : 0 ;?> Adult, <?php if(isset($prev_details['child_date']) && !empty($prev_details['child_date'])): ?> <?php echo count($prev_details['child_date']['toddler_data']);?> Child(ren), <?php echo count($prev_details['child_date']['infant_data']);?> Infant(s)<?php else:?>0 Children<?php endif;?></strong><br>
            Total: <?php echo (isset($prev_details['total_holder']) && $prev_details['total_holder'] > 0) ? $prev_details['total_holder'] : 0 ;?> pax<br><br>

            <strong>Departing on <?php echo $prev_details['depart_date']; ?></strong><br>
            <strong>Returning on <?php echo $prev_details['return_date']; ?> </strong><br>
            Package: <?php echo $package->getDays(); ?> day/s<br>
            Extension: <?php echo $prev_details['extended_days']; ?> day/s<br>
            Total: <?php echo $prev_details['total_days']; ?> day/s <br><br><br>
        </div>
    </div>
    <div class="row">
        <form id="stepdetails_form" class="form-horizontal" role="form" action="<?php echo base_url('panel/booking_change_agent'); ?>" method="post">
            <div class="col-md-12">
                <div class="panel panel-orange">
                    <!-- Default panel contents -->
                    <input type="hidden" name="booking_id" value="<?php echo $booking_id;?>" />
                    <input type="hidden" name="package_id" value="<?php echo $packageid;?>" />
                    <div class="panel-heading text-center"><strong>Traveller Details</strong></div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <tr>
                                    <th></th>
                                    <th>Name
                                        </th>
                                    <th width="150px;">DOB</th>
                                    <th>Passport No</th>
                                </tr>
                                <?php if (isset($prev_details['adult_count']) && $prev_details['adult_count'] > 0): ?>
                                    <?php for ($i=1; $i <= $prev_details['adult_count'] ; $i++) { ?>
                                        <tr>
                                            <td>Adult <?php echo $i;?></td>
                                            <td><input name="adult_name[]" class="form-control min-size-name" type="text" placeholder="A"></td>
                                            <td>
                                                <div class="input-group date" id="datetimepickerDOB1">
                                                    <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                    <input name="adult_dob[]" data-date-format="YYYY-MM-DD" class="form-control min-size-num" type="calendar" readonly/>
                                                </div>
                                            </td>
                                            <td><input name="adult_ppno[]" class="form-control min-size-num" type="text" placeholder="12345"></td>
                                        </tr>
                                    <?php } ?>
                                <?php endif; ?>
                                <?php if (!empty($prev_details['child_date'])): ?>
                                    <?php if(count($prev_details['child_date']['infant_data']) > 0): ?>
                                        <?php $i=0;?>
                                        <?php foreach($prev_details['child_date']['infant_data'] as $dob): ?>
                                            <tr>
                                                <td>Infant <?php echo ++$i;?></td>
                                                <td><input name="infant_name[]" class="form-control min-size-name" type="text" placeholder="A"></td>
                                                <td>
                                                    <div class="input-group date" id="datetimepickerDOB1">
                                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                        <input name="infant_dob[]" value="<?php echo $dob; ?>" data-date-format="YYYY-MM-DD" class="form-control min-size-num" type="calendar" readonly/>
                                                    </div>
                                                </td>
                                                <td><input name="infant_ppno[]" class="form-control min-size-num" type="text" placeholder="12345"></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                     <?php if(count($prev_details['child_date']['toddler_data']) > 0): ?>
                                        <?php $i=0;?>
                                        <?php foreach($prev_details['child_date']['toddler_data'] as $dob): ?>
                                            <tr>
                                                <td>Toddler <?php echo ++$i;?></td>
                                                <td><input name="toddler_name[]" class="form-control min-size-name" type="text" placeholder="A"></td>
                                                <td>
                                                    <div class="input-group date" id="datetimepickerDOB1">
                                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                        <input name="toddler_dob[]" value="<?php echo $dob; ?>" data-date-format="YYYY-MM-DD" class="form-control min-size-num" type="calendar" readonly/>
                                                    </div>
                                                </td>
                                                <td><input name="toddler_ppno[]" class="form-control min-size-num" type="text" placeholder="12345"></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="panel panel-orange">
                    <!-- Default panel contents -->
                    <div class="panel-heading text-center"><strong>Flight Details</strong></div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <tr>
                                    <th width="150px"></th>
                                    <th>Originating Flight</th>
                                    <th>Returning Flight</th>
                                </tr>
                                <tr>
                                    <td>Date</td>
                                    <td>
                                        <div class="input-group date" id="datetimepickerDepartTime">
                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                            <input name="flight_of_date" type="calendar" data-date-format="YYYY-MM-DD HH:mm:ss" class="form-control min-size-num" readonly/>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="input-group date" id="datetimepickerReturnTime">
                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                            <input name="flight_rf_date" type="calendar" data-date-format="YYYY-MM-DD HH:mm:ss" class="form-control min-size-num" readonly/>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Flight No(no spaces)</td>
                                    <td><input name="flight_of_no" class="form-control" type="text" placeholder="12345"></td>
                                    <td><input name="flight_rf_no" class="form-control" type="text" placeholder="12345"></td>
                                </tr>
                                <tr>
                                    <td>From</td>
                                    <td>
                                        <select name="flight_of_from" class="form-control">
                                            <option value=" ">- Select -</option>
                                            <?php if (isset($airportcodes)): ?>
                                            <?php foreach ($airportcodes as $a => $airportcode): ?>
                                                <option value="<?php echo $a; ?>"><?php echo $airportcode; ?></option>
                                            <?php endforeach; ?>
                                            <?php endif; ?>
                                        </select>
                                    </td>
                                    <td>
                                        <select name="flight_rf_from" class="form-control">
                                            <option value=" ">- Select -</option>
                                            <?php if (isset($airportcodes)): ?>
                                            <?php foreach ($airportcodes as $a => $airportcode): ?>
                                                <option value="<?php echo $a; ?>"><?php echo $airportcode; ?></option>
                                            <?php endforeach; ?>
                                            <?php endif; ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>To</td>
                                    <td>
                                        <!-- <input id="typeahead_of_to" name="flight_of_to" class="form-control" type="text" placeholder="CEBU"> -->
                                        <select name="flight_of_to" class="form-control">
                                            <option value=" ">- Select -</option>
                                            <?php if (isset($airportcodes)): ?>
                                            <?php foreach ($airportcodes as $a => $airportcode): ?>
                                                <option value="<?php echo $a; ?>"><?php echo $airportcode; ?></option>
                                            <?php endforeach; ?>
                                            <?php endif; ?>
                                        </select>
                                    </td>
                                    <td>
                                        <!-- <input id="typeahead_rf_to" name="flight_rf_to" class="form-control" type="text" placeholder="CDO"> -->
                                        <select name="flight_rf_to" class="form-control">
                                            <option value=" ">- Select -</option>
                                            <?php if (isset($airportcodes)): ?>
                                            <?php foreach ($airportcodes as $a => $airportcode): ?>
                                                <option value="<?php echo $a; ?>"><?php echo $airportcode; ?></option>
                                            <?php endforeach; ?>
                                            <?php endif; ?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Remarks</td>
                                    <td colspan="2"><textarea name="flight_remarks" class="form-control" style="resize:none;" rows="3"></textarea></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <button class="btn btn-orange center-block" role="button">Book Now!</button>
            </div>
        </form>
    </div>
    <br>
</div>
<!-- /#page-wrapper -->