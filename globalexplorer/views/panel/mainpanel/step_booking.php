
<div id="page-wrapper" >
    <!-- /.row -->
    <div class="row">
        <br>
        <div class="col-md-12">
            <div class="stepwizard">
                <div class="stepwizard-row">
                    <div class="stepwizard-step">
                        <button type="button" class="btn btn-default btn-circle" disabled="disabled">1</button>
                        <p>Package</p> 
                    </div>
                    <div class="stepwizard-step">
                        <button type="button" class="btn btn-orange btn-circle" disabled="disabled">2</button>
                        <p>Hotel</p>
                    </div>
                    <div class="stepwizard-step">
                        <button type="button" class="btn btn-default btn-circle" disabled="disabled">3</button>
                        <p>Details</p>
                    </div> 
                </div>
            </div>
        </div>
    </div>
    
    

    <div class="row">
        <!-- -->
        <form id="stephotel_form" class="form-horizontal" role="form" action="<?php echo base_url('panel/step_details'); ?>" method="post">
     
            <!-- hidden div for package surcharges -->
            <div id="hidden_package_surcharge_div" class="col-md-12"></div>
            <!-- end of hidden div for package surcharges -->
            
            <!-- hidden div for hotel surcharges -->
            <div id="hidden_hotel_surcharge_div"></div>
            <!--  hidden div for hotel surcharges -->
            
            <div class="col-md-4">
                <?php
                    $img = base_url('img/feat/default.png');
                    if($package->getImagePath()!='') $img = base_url($package->getImagePath());
                ?>
                <img class="thumbnail img-responsive" src="<?php echo $img; ?>" alt="...">        
                
                <!-- booking code -->
                <input type="hidden" name="booking_code" value="<?php echo @$booking_data['code']; ?>">

                <!-- booking code -->
                <input type="hidden" name="booking_id" value="<?php echo @$booking_data['id']; ?>">


                <input type="hidden" name="package_id"  id="package_id" value="<?php echo $package->getId();?>" />
                    
                <!-- this will contain the package days (fixed) -->
                <?php foreach ($days as $row) {

                echo '<input type="hidden" id="package_days" name="package_days" value="'.$row->days.'" />';
                    }
                ?>
                <!-- this will contain the days that extend beyon the fixed package days -->
                <input type="hidden" id="extended_days" name="extended_days" value="0"/>
                <input type="hidden" id="extended_days1" name="extended_days1" value="0"/>

                <!-- this is will contain the package surcharge -->
                <input type="hidden" id="package_surcharge_final" value="<?php  echo $package_surcharge; ?>">
                
                <!-- hotel surcharge -->   
                <input type="hidden" id="hotel_surcharge_final" name="hotel_surcharge_final" value="0">

                <!-- rooms surcharge -->
                <input type="hidden" id="room_surcharge_final" name="room_surcharge_final" value="0">      
                
                <!-- total cost -->
                <input type="hidden" id="package_total_cost" name="package_total_cost" value="0">

                <!-- total profit -->
                <input type="hidden" id="package_total_profit" name="package_total_profit" value="0">
                
                <!-- total payment -->
                <input type="hidden" id="package_total_payment" name="package_total_payment" value="0">  
            
                <!-- total package surcharges -->
                <input type="hidden" name="package_total_surcharge" id="package_total_surcharge" value="0">
               
                <!-- total hotel surcharges -->
                <input type="hidden" name="package_hotel_total_surcharge" id="package_hotel_total_surcharge" value="0">

                <!-- total room surcharges -->
                <input type="hidden" name="package_room_total_surcharges" id="package_room_total_surcharges" value="0">
    
                <!-- selected hotel name -->
                <input type="hidden" id="package_hotel_name" name="package_hotel_name" value="">
    
                <!-- set overflow days -->
                <input type="hidden" id="days_overflow" name="days_overflow" value="">
                
                <!-- base url container -->
                <input type="hidden" id="base_url_container" value="<?php echo base_url(); ?>">

                <!-- controller for generation of surcharges-->
                <input type="hidden" id="package_surcharge_generation" value="false">

                <!-- counter for total rooms -->
                <input type="hidden" id="room_count" name="room_count" value="0">
                
                <input type="hidden" value="<?php echo $package->getTitle(); ?>" name="package_name">
                
                <!-- optional hotel name -->
                <input type="hidden" name="exhotel_name_full" id="exhotel_name_full" value="">
                
                <!-- adult count guest -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">Adult</label>
                    <div class="col-sm-8">
                        <input type="text" name="adult_count"  class="form-control count_val" onkeypress="return isNumberKey(event)">
                        <input type="hidden" name="adult_count1"  >
                    </div>
                </div>
                <!-- adult count guest -->
                
                <!-- start of children input -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">Children</label>
                    <div class="col-sm-8">
                        <input type="text" name="child_count"  class="form-control count_val"  onkeypress="return isNumberKey(event)">
                        <input type="hidden" name="child_count1" >
                    </div>
                </div>
                <!-- end of children container -->
    
                <!-- start of children input -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">Infant</label>
                    <div class="col-sm-8">
                        <input type="text" name="infant_cnt"  class="form-control" onkeypress="return isNumberKey(event)">
                        <input type="hidden" name="infant_cnt1">
                    </div>
                </div>
                <!-- end of children container -->
                
                <!-- dynamic wrapper for children date -->
                <div id="child_wrapper"></div>
                <!-- end of dynamic wrapper for children date -->
    
                <!-- start of guests section -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">Guest</label>
                    <div class="col-sm-8">
                        <input name="total_holder" id="total_holder_all" class="form-control" type="number" placeholder="Total" readonly/>
                        <input name="total_holder1" type="hidden" readonly/>
                    </div>
                </div>
                <!-- end of guests section -->
                
                <!-- start of departure -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">Departure</label>
                    <div class="col-sm-8">
                        <input id="datetimepickerBookingDepart" name="depart_date" type="calendar" data-date-format="DD-MMM-YYYY" class="form-control" readonly/>
                        <input id="datetimepickerBookingDepart1" name="depart_date1" type="hidden" data-date-format="DD-MMM-YYYY" class="form-control" readonly/>
                    </div>
                </div>
                <!-- end of departure -->
    
                <!-- start of return dtpicker-->
                <div class="form-group">
                    <label class="col-sm-4 control-label">Return</label>
                    <div class="col-sm-8">
                        <input id="datetimepickerBookingReturn" name="return_date" type="calendar" data-date-format="DD-MMM-YYYY" class="form-control" readonly/>
                        <input id="datetimepickerBookingReturn1" name="return_date1" type="hidden" data-date-format="DD-MMM-YYYY" class="form-control" readonly/>
                    </div>
                </div>
                <!-- end of return dtpicker --> 
                
                <?php
                    $booking_extended = @$booking_data["extended_days"]==null ? false : intVal(@$booking_data["extended_days"]);
                ?>
                <div class="form-group" id="label_extended_days_container_wrapper"  style="<?php if(!$booking_extended){ echo "display:none;"; }else{} ?>">
                    <label class="col-xs-4 control-label">Ext. Days</label>
                    <label class="col-xs-8 control-label text-right" id="label_extended_days_container"><?php echo $booking_extended; ?></label>
                </div>
            </div>
    
            <div class="col-md-8">
                <h3><?php echo $package->getTitle();?> (Effective from <?php echo date('d-M-Y',strtotime($package->getDateFrom()));?> - <?php echo date('d-M-Y',strtotime($package->getDateTo()));?>)</h3>
                <strong>Package includes: <a href="<?php echo base_url('panel/Printpackages').'/'.$package->getId(); ?>" style="color: #C64E0A; text-decoration:none;"><button type="button" style="margin:-19px 0px 0px 132px; float:right; height: 30px;
    width: 20%;" class="btn btn-orange center-block" role="button">Print Package</button></a></strong><br><br>
    <a href="" data-toggle="modal" data-target="#myModalAirportMap" style="color: #C64E0A; text-decoration:none;"><button type="button" style="margin:-19px 0px 0px 132px; float:right; height: 30px;
    width: 20%;" class="btn btn-orange center-block" role="button">Print Airport Map</button></a></strong><br><br>
    <a href="<?php echo base_url('pdfs/'.$package->getId()); ?>" style="color: #C64E0A; text-decoration:none;"><button type="button" style="margin:-19px 0px 0px 132px; float:right; height: 30px;
    width: 20%;" class="btn btn-orange center-block" role="button">Chinese Itinerary</button></a></strong><br><br>
                <?php if($package->getDescription()!=''): echo nl2br(htmlentities($package->getDescription())); endif; ?>
                <?php if ($package->getTermsAndConditions() != NULL) { ?>
                    <br>
                    <br>
                    <h5>Terms & Conditions</h5>
                    <p><?php echo nl2br(htmlentities($package->getTermsAndConditions())); ?></p>
                <?php }?>
                <?php
                    if($pkg_srchg!=NULL){
                        echo "<br><br><br><strong>Package Surcharcges:</strong><br><ul>";
                        foreach ($pkg_srchg as $packSurc) {
                            $date_range     =   explode(" ",$packSurc['rule']);
                            if ($packSurc['rule_type'] == "date_range" || $packSurc['rule_type'] == "blackout" ) {
                                $rule = "(effective from ". date_format(date_create($date_range[0]),'d-M-Y') ." to ".  date_format(date_create($date_range[2]),'d-M-Y') .")";
                            }
                            else if ($packSurc['rule_type'] == "day_of_week") {
                                $rule = "(for every ". $packSurc['rule'] .")";
                            }
                            else if ($packSurc['rule_type'] == "agent") {
                                $rule = "(by ". $packSurc['rule'] .")";
                            }
                            echo "<li>". $packSurc['description'] ." ". $rule ." @ S$". intVal($packSurc['cost'] + $packSurc['profit']) ." ". $packSurc['rate_type'] ."</li>";
                        }
                        echo "</ul>";
                    }
                ?>
            </div>  
            <div class="modal fade" id="myModalAirportMap" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="pull-right btn btn-danger btn-sm" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="myModalLabel">Attachment(s):</h4>
                        </div>
                        <div class="modal-body">
                            <ul>
                                <?php
                                    if(@$airport_map != ""):
                                    foreach($airport_map as $key){ ?>
                                        <a href="<?php echo base_url('uploads/destination_pdf/'.$key->pdf_name); ?>" target="_blank"><li><?php echo $key->title; ?></li></a>
                                <?php }
                                   endif;
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>     

            <div class="col-md-12">
                <br>
                
                <!-- start of alert box -->    
                <div class="alert alert-danger text-center" id="supplementary_errors" style='display:none;'></div>
                <!-- end of alert box -->

                <!-- container for the destinations --> 
                <?php 
                    if(count($destinations)){
                       echo "<input type='hidden' id='total_destinations' value='".count($destinations)."' />";

                       for($i=0;$i<count($destinations);$i++){
                            $destination = $destinations[$i];
                            $booked_hotel = @$booking_data["hotels"][$i];

                            #dump($package_hotels[$i]);
                            #dump($booked_hotel);

                            echo "  <!-- set extended over flow days-->
                            <input type=\"hidden\" id=\"days_overflow{$destination['id']}\"  destination-id='{$destination['id']}' class='days_overflow' value=\"true\">
                            <input type=\"hidden\" id=\"bfast_overflow{$destination['id']}\"  destination-id='{$destination['id']}' class='bfast_overflow' value=\"true\">
                            <input type=\"hidden\" id=\"child_overflow{$destination['id']}\"  destination-id='{$destination['id']}' class='child_overflow' value=\"true\">
                            <input type='hidden' id='room_count{$destination['id']}' value='0'>
                            <input type='hidden' id='adult_overflow{$destination['id']}' value='0'>";
                ?>
                    <!-- package destinations -->
                    <div class="panel panel-orange package_destination_container" destination-id="<?php echo $destination['id']; ?>" style="padding-bottom:0px;">
                        <div class="panel-heading clearfix">
                            <div class="pull-left">Destination - &nbsp;</div>
                            <div class="pull-left"><?php echo $destination['country']; ?></div>
                            <div class="pull-right"><?php echo $destination['nights'];  ?> night(s)</div>
                            <input type="hidden" name='destination_id[]' value='<?php echo $destination['id']; ?>'>
                            <input type="hidden" name="destination_country[]" value="<?php echo $destination['country']; ?>">
                            <input type="hidden" name="destination_nights[]" value="<?php echo $destination["nights"]; ?>">
                            <input type="hidden" name="package_hotel_name[]" value="" id="package_hotel_name<?php echo $destination["id"]; ?>">
                        </div>
                        <div class="panel-body" style="padding-bottom:0px;">
            
                            <!-- hotel surcharges -->
                            <div id="destination_hotel_surcharges<?php echo $destination['id']; ?>">
                                
                            </div>
                            <!-- end of hotel surcharges -->
                            
                            <!-- start of alert box -->    
                            <div class="alert alert-danger text-center" id="blackout_error_container<?php echo $destination['id']; ?>" style='display:none;'></div>
                            <!-- end of alert box -->
    

                            <div class="alert alert-danger text-center" id="supplmentary_errors<?php echo $destination['id']; ?>" style="display:none;"></div>

                            <!-- container for hotel choices -->
                            <div class="form-group">
                                <label class="col-md-4 control-label">Hotel Choice :</label>
                                <div class="col-md-5">
                                    <select class="form-control additional_select_hotel" destination-code="<?php echo $destination['country_code']; ?>" disabled='disabled' id="package_hotel_choice<?php  echo $destination["id"]; ?>" destination-index="<?php echo $i; ?>" destination-id="<?php echo $destination['id']; ?>" name="package_hotel_choice[]" package-id="<?php echo $package->getId();?>" booking-id="<?php echo @$booking_data['id']; ?>" onChange="asd()">
                                        <option value=" ">- Select -</option>
                                        <?php if (isset($package_hotels[$i])): ?>
                                            <?php foreach ($package_hotels[$i] as $package_hotel): ?>
                                                <option  <?php if(trim($package_hotel["id"]==trim($booked_hotel["package_hotel_id"]))){ echo "selected"; } ?> id="package_hotel_choice_name<?php echo $destination['id']; ?>-<?php echo $package_hotel['hotel_id']  ?>" hotel-name="<?php echo $package_hotel['name']; ?>" value="<?php echo $package_hotel['hotel_id'].".".$package_hotel['id']; ; ?>"><?php echo $package_hotel['name']; ?></option>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </select>
                                    <!-- <select class="form-control additional_select_hotel" destination-code="<?php echo $destination['country_code']; ?>" disabled='disabled' id="package_hotel_choice<?php  echo $destination["id"]; ?>" destination-index="<?php echo $i; ?>" destination-id="<?php echo $destination['id']; ?>" name="package_hotel_choice1[]" package-id="<?php echo $package->getId();?>">
                                        <option value=" ">- Select -</option>
                                        <?php if (isset($package_hotels[$i])): ?>
                                            <?php foreach ($package_hotels[$i] as $package_hotel): ?>
                                                <option  <?php if(trim($package_hotel["id"]==trim($booked_hotel["package_hotel_id"]))){ echo "selected"; } ?> id="package_hotel_choice_name<?php echo $destination['id']; ?>-<?php echo $package_hotel['hotel_id']  ?>" hotel-name="<?php echo $package_hotel['name']; ?>" value="<?php echo $package_hotel['hotel_id'].".".$package_hotel['id']; ; ?>"><?php echo $package_hotel['name']; ?></option>
                                            <?php endforeach; ?>
                                        <?php endif; ?>
                                    </select> -->
                                    <input type="hidden" name="hotelid" id="hotelid">
                                    <input type="hidden" name="hotelid1" id="hotelid1">

                                </div>
                            </div>
                            <!-- end of container for hotel choices -->
                            
                            <!-- container for roomrates -->
                            <div class="form-group" id="roomrates_container<?php echo $destination['id']; ?>" style="padding:15px; padding-bottom:0px; margin-bottom:0px;">
                            </div>  
                            <!-- end of container for room rate -->

                        
                            <!-- loader container -->
                            <div class='form-group text-center' id="destination_roomrates_loader<?php echo $destination['id']; ?>" style="padding:20px; display:none;">
                                <i class="fa fa-spinner fa-spin" style="font-size:30px;"></i>
                                <h5>Fetching Rooms</h5>
                            </div>
                            <!-- end of loader container -->
                        </div>
                    </div>
                    <!-- end of package destinations -->
                <?php
                       }
                    }else{
                ?>  
                    <!-- show if the package does not have any destinations -->
                    <div class="panel panel-orange">
                        <div class="panel-heading clearfix">
                            <div class="pull-left">Destination Error</div>
                        </div>
                        <div class="panel-body text-center">
                            <i class="fa fa-map-marker" style="font-size:100px;"></i>
                            <h5>This package contains <u>0</u> destinations.</h5>
                        </div>
                    </div>
                    <!-- end of show if the package does not have any destinations-->
                <?php
                    }
                ?>
                <!-- end of container for the destinations -->
                <div class="checkbox">
                    <label>
                      <input type="checkbox" id="check" onclick="isChecked()"> Show Optional Tours
                    </label>
                  </div><br>
                <!--start of addons -->
                <?php if(isset($addons) && (count($addons)!=0)): ?>
                    <div class="panel panel-orange hotel_package_user_addons" id="optional" style="display:none">
                        <!-- Default panel contents -->
                        <div class="panel-heading text-center"><strong>Optional Tours / Add Ons</strong></div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered">
                                    <tr>
                                        <th>Description</th>
                                        <th>Price</th>
                                        <th>Qty</th>
                                    </tr>
                                    <?php foreach ($addons as $addon): ?>
                                        <tr>
                                            <td><?php echo $addon['description']; ?></td>
                                            <td> 
                                                <?php $price = ($addon['cost'] + $addon['profit']); echo $price; ?> / <?php echo $addon['unit']; ?>
                                                <input name="addon_price[]" id="addon_price<?php echo $addon['id']; ?>" type="hidden" value="<?php echo $price; ?>">
                                                <input type="hidden" name="addon_id[]" value="<?php  echo $addon['id']; ?>">
                                                <input type="hidden" value="<?php echo $addon['description']; ?>" name="addon_desc[]">
                                                <input type="hidden" value="<?php echo $addon['cost']; ?>" name="addon_cost[]">
                                                <input type="hidden" value="<?php echo $addon['profit']; ?>" name="addon_profit[]">
                                                <input type="hidden" value="<?php echo $addon['unit']; ?>" name="addon_unit[]">
                                            </td>
                                            <td>
                                                <?php  
                                                   $book_qty = $addon["user_qty"]==null ? "":intVal($addon["user_qty"]);
                                                ?>
                                                <input name="addon_quantity[]" value="<?php echo $book_qty; ?>" addon-profit='<?php echo $addon['profit']; ?>' addon-cost='<?php echo $addon['cost']; ?>' addon-desc="<?php echo $addon['description']; ?> " addon-id="<?php  echo $addon['id']; ?>" class="hotel_package_user_num form-control min-size-num" type="text" onkeypress="return isNumberKey(event)">
                                            </td>
                                        </tr>
                                    <?php endforeach;?>
                                </table>
                            </div>
                        </div>
                    </div>
                <?php endif;?>
                <!-- end of addons -->
                
                <!-- additional hotel arrangements -->
                <div class="panel panel-orange">
                    <!-- Default panel contents --> 
                    <div class="panel-heading text-center"><strong>Additional Hotel Arrangements</strong></div>
                    <div class="panel-body">
                        <!-- <div class="form-group">
                            <label class="col-md-2 control-label">Hotel Name</label>
                            <div class="col-md-10">
                                <select name="exhotel_name" class="form-control min-size-name">
                                    <option value=" ">- Selects -</option>
                                    <?php 
                                        /*if($booking_data){ 
                                            if($booking_data["exhotel_id"]!=0 && $booking_data["exhotel_id"]!=null){
                                                echo "<option value='{$booking_data["exhotel_id"]}' selected>{$booking_data["exhotel_name"]}</option>";
                                            }
                                        }*/
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Remarks</label>
                            <div class="col-md-10">
                                <textarea name="exhotel_remarks" class="form-control" style="resize:none;" cols="50" rows="3"><?php if($booking_data["exhotel_id"]!=0 && $booking_data["exhotel_id"]!=null){ echo @$booking_data["hotels"][0]["remarks"]; }?></textarea>
                            </div>
                        </div> -->

                        <!-- NEW LAYOUT -->
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <td></td>
                                    <td>Hotel Name</td>
                                    <td>Check In</td>
                                    <td>Check Out</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Hotel #2</td>
                                    <td width="60%" class="form-group">
                                        <input type="text" style="width:100%" class="form-control" id="add_hotel_name2" name="add_hotel_name2"/>
                                        <i class="form-control-feedback glyphicon glyphicon-ok" style="top: 15%; display: none;"></i>
                                    </td>
                                    <td width="20%">
                                        <input id="datepicker_checkin2" name="additional_checkin2" type="calendar" data-date-format="YYYY-MM-DD" class="form-control" disabled="disabled" readonly/>
                                    </td>
                                    <td width="20%">
                                        <input id="datepicker_checkout2" name="additional_checkout2" type="calendar" data-date-format="YYYY-MM-DD" class="form-control" disabled="disabled" readonly/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Hotel #3</td>
                                    <td width="60%" class="form-group">
                                        <input type="text" style="width:100%" class="form-control" id="add_hotel_name3" name="add_hotel_name3"/>
                                        <i class="form-control-feedback glyphicon glyphicon-ok" style="top: 15%; display: none;"></i>
                                    </td>
                                    <td width="20%">
                                        <input id="datepicker_checkin3" name="additional_checkin3" type="calendar" data-date-format="YYYY-MM-DD" class="form-control" disabled="disabled" readonly/>
                                    </td>
                                    <td width="20%">
                                        <input id="datepicker_checkout3" name="additional_checkout3" type="calendar" data-date-format="YYYY-MM-DD" class="form-control" disabled="disabled" readonly/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Remarks</td>
                                    <td colspan="3">
                                        <textarea name="exhotel_remarks" class="form-control" style="resize:none;" cols="50" rows="3"><?php if($booking_data["exhotel_id"]!=0 && $booking_data["exhotel_id"]!=null){ echo @$booking_data["hotels"][0]["remarks"]; }?></textarea>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- end of additional hotel arrangements  -->
                
    
                <!-- start of alert box -->    
                <div class="alert alert-danger" id="surcharge_generation_error" style="display:none;">
                    <p class='text-center'><i class="fa fa-warning"></i>  Please generate a summary before proceeding!</p>
                </div>
                <!-- end of alert box -->
                
                
                <!-- summary -->
                <div class="panel panel-orange" id="main_surcharges_table">
                    <!-- Default panel contents -->
                    <div class="panel-heading text-center clearfix">
                        <strong class='pull-left'>Summary</strong>
                        <button  onclick=" generateSurcharges(this); event.preventDefault();" id="surchargeGenerationButton" class='pull-right  btn btn-primary btn-sm'><i class="fa fa-gear"></i>&nbsp;GENERATE SUMMARY</button>
                        <div class="pull-right" id="generateSurchargeSpinner" style="display:none; font-size:20px;"><i class="fa fa-spinner fa-spin"></i></div>
                    </div>
                    <div class="panel-body" id="surchargeGeneratorContainer" style="display:none; word-wrap:break-word;">
                        <div class="table-responsive" > </div>
                    </div>
                </div>
                <!-- end of summary -->
            </div>
        </form>

        <!-- start of proceed to the next step -->
        <div class="col-md-12" style="margin-bottom:20px;">
            <button type="submit" id="stepbook_submit" class="btn btn-orange center-block" onclick="stepBookingFunctionFinalize(this);" role="button">Continue</button>
        </div>
        <!-- end of proceed to the next step -->
    </div>

    <!-- surcharges alert -->
    <div class="modal fade" id="sam_mod">
        <div class="modal-dialog">
            <div class="modal-content alert-danger">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Surcharges apply to the travel dates selected.</h4>
                </div>
                <!-- <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-orange">Ok</button>
                </div> -->
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>
<!-- /#page-wrapper -->
<script>
    $(document).ready(function(){
        <?php 
            if($booking_data){
        ?>  
                $("[name='adult_count']").val("<?php echo $booking_data['people_count']['adult_cnt']; ?>");
                $("[name='adult_count1']").val("<?php echo $booking_data['people_count']['adult_cnt']; ?>");

                $("[name='child_count']").val("<?php echo $booking_data['people_count']['child_cnt']; ?>");
                $("[name='child_count1']").val("<?php echo $booking_data['people_count']['child_cnt']; ?>");

                $("[name='infant_cnt']").val("<?php echo $booking_data['people_count']['infant_cnt']; ?>");
                $("[name='infant_cnt1']").val("<?php echo $booking_data['people_count']['infant_cnt']; ?>");

                $("[name='total_holder']").val(<?php echo intval($booking_data['people_count']['adult_cnt']) + intval($booking_data['people_count']['child_cnt']) + intval($booking_data['people_count']['infant_cnt']) ?>);
                $("[name='total_holder1']").val(<?php echo intval($booking_data['people_count']['adult_cnt']) + intval($booking_data['people_count']['child_cnt']) + intval($booking_data['people_count']['infant_cnt']) ?>);

                var d = new Date("<?php echo $booking_data['departure_date']; ?>");
                $("#datetimepickerBookingDepart").val("<?php echo date('d-M-Y', strtotime($booking_data['departure_date'])); ?>");
                $("#datetimepickerBookingDepart1").val("<?php echo date('d-M-Y', strtotime($booking_data['departure_date'])); ?>");

                $("#datetimepickerBookingReturn").val("<?php echo date('d-M-Y', strtotime($booking_data['return_date'])); ?>");
                $("#datetimepickerBookingReturn1").val("<?php echo date('d-M-Y', strtotime($booking_data['return_date'])); ?>");

                $('#extended_days').val("<?php echo $booking_data['extended_days']; ?>");
                $('#extended_days1').val("<?php echo $booking_data['extended_days']; ?>");


                $("[name='hotelid']").val("<?php echo $booking_data['hotels'][0]['package_hotel_id']; ?>");
                $("[name='hotelid1']").val("<?php echo $booking_data['hotels'][0]['package_hotel_id']; ?>");

                $("[name='package_hotel_choice[]']").removeAttr("disabled");

                $("[name='package_hotel_choice[]']").each(function(i,e){
                    var destination_id = $(e).attr("destination-id");
                    var booking_id = $(e).attr("booking-id");
                    updateRoomRatesContainers(destination_id,booking_id);
                });
        <?php
            }
        ?>
    });

    function isChecked(){
        if(document.getElementById('check').checked)
            $("#optional").show();
        else
            $("#optional").hide();

    }

    function asd()
    {
        var a = $('[name="package_hotel_choice[]"]').val();
        var e = a.split('.');
        $("[name='hotelid']").val(e[0]);
    }
</script>