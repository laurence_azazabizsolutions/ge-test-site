<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Global Explorer</title>
 		<style type="text/css">
			h1,
			h4{
				font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
				font-weight: 500;
				line-height: 1.1;
				color: inherit;
			}
		</style>
	</head>
	<body style="color: #333333;">
		<table cellspacing="0" cellpadding="0" style="background-color: #ffffff; border: 1px solid transparent;  border-radius: 4px; -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05); box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05); border-color: #e95a3e;">
			<tr>
				<td style="padding: 10px 15px; border-bottom: 1px solid transparent; border-top-right-radius: 3px; border-top-left-radius: 3px; color: #fff; background-color: #e95a3e; border-color: #e95a3e;">
					<h1 style="margin-top: 0; margin-bottom: 0; font-size: 16px; color: inherit; text-align: center; ">
						Global Explorer
					</h1> 
				</td>
			</tr>
			<tr>
				<td style="padding: 15px;">
                	<strong>No. of Bookings for <i><?php echo date("d-M-Y"). "</i>: " . count($booking_every_day)?></strong>
                    <table style="width: 770px; margin-bottom: 20px; border-collapse: collapse;">
                    	<thead>
	                        <tr>
	                            <td style="padding: 8px; vertical-align: bottom; border: 1px solid #dddddd;"><strong>Booking Code</strong></td>
	                            <td style="padding: 8px; vertical-align: bottom; border: 1px solid #dddddd;"><strong>Company Name(s)</strong></td>
	                            <td style="padding: 8px; vertical-align: bottom; border: 1px solid #dddddd;"><strong>Status</strong></td>
	                        </tr>
                    	</thead>
                    	<tbody>
                    		<?php
                    			if($booking_every_day != NULL || $booking_every_day != ""){
                                    foreach($booking_every_day as $row){
                            ?>
                            			<tr>
				                            <td style="padding: 8px; vertical-align: top; border: 1px solid #dddddd;"><?php echo $row->booking_code; ?></td>
				                            <td style="padding: 8px; vertical-align: top; border: 1px solid #dddddd;"><?php echo $row->company_name; ?></td>
				                            <td style="padding: 8px; vertical-align: top; border: 1px solid #dddddd;"><?php echo $row->booking_status; ?></td>
				                        </tr>
                            <?php
                                    }
                                }
                    		?>
                    	</tbody>
                    </table>
				</td>
			</tr>
		</table>
	</body>
</html>