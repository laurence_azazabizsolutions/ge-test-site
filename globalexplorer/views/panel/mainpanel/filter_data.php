<div id="page-wrapper">
    <!-- /.row -->
    <div class="row">
        <br>
        <div class="col-lg-12">
        <form class="form-horizontal" role="form" action="<?php echo base_url('panel/filter_data')?>" method="post"> 
            <!--?php echo form_open('#','id="booking_data" class="form-horizontal" role="form"'); ?-->
                <div class="form-group">
                    <label class="col-sm-3 control-label">Booking Code</label>
                    <div class="col-sm-8">
                        <input type="text" name="booked_code" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Booking Status</label>
                    <div class="col-sm-8">
                        <select class="form-control" name="booking_status">
                            <option value= ''>- Select -</option>
                            <option value='pending'>Pending</option>
                            <option value='confirmed'>Confirmed</option>
                            <option value='ammended'>Ammended</option>
                            <option value='rejected'>Rejected</option>
                            <option value='cancelled'>Cancelled</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Date</label>
                    <div class="col-sm-4">
                        <div class="input-group date" id="datetimepickerMonthView">
                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                            <input class="form-control" type="text" placeholder="Origin" name="origin" data-date-format="YYYY-MM"/>
                        </div>
                        <br>
                    </div>
                    <div class="col-sm-4">
                        <div class="input-group date" id="datetimepickerMonthView">
                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                            <input class="form-control" type="text" placeholder="Return" name="return" data-date-format="YYYY-MM"/>
                        </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-3">
                        <input type="submit" class="btn btn-orange" value="Filter"  name="button"/>
                        <button type="reset" class="btn btn-default">Clear</button>
                    </div>
                </div>
            </form>
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Booking Code</th>
                            <th>Details</th>
                            <th>Flight</th>
                            <th>Amount</th>
                            <th>Payment</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody id="data_list">
                        <?php
                            if(strtolower(get_class($user)) == 'admin') {
                                $data = $booked_data;
                            }
                            else if(strtolower(get_class($user)) == 'agent') {
                                $data = $booked_data_agent;
                            }
                            else if(strtolower(get_class($user)) == 'provider') {
                                $data = $booked_data_provider;
                            }
                        ?>

                        <?php if (isset($data)): ?>
                            <?php foreach ($data as $row): ?>

                                <tr>
                                    <td>
                                        <a href="<?php echo base_url('panel/voucher')?>">GE01949</a> <br>
                                        <a href="<?php echo base_url('panel/package')?>">View</a>
                                    </td>
                                    <td>
                                        <strong>Package:</strong> <?php echo $row->PACKAGENAME; ?><br>
                                        <strong>Agent:</strong> <?php echo $row->AGENTNAME; ?><br>
                                        <strong>Provider:</strong><?php echo $row->PROVIDERNAME; ?><br>
                                        <strong>Hotel:</strong> <?php echo $row->HOTELSNAME; ?><br>
                                        <strong>Traveller:</strong> <?php echo $row->TRAVELLLERID; ?><br>
                                    </td>
                                    <td>
                                        <strong>Ori:</strong><?php echo $row->BOOKINGDEPARTUREDATE; ?><br>
                                        <i><?php echo $row->ARRIVALTIME; ?> hrs - <?php echo $row->RETURNTIME; ?> hrs</i><br>
                                        <strong>Ret:</strong><?php echo $row->BOOKINGRETURNDATE; ?><br>
                                        <i><?php echo $row->RETURNTIME; ?> hrs - <?php echo $row->ARRIVALTIME; ?> hrs</i>
                                    </td>
                                    <td>
                                        <strong>Cost:</strong><?php echo $row->BOOKINGCOST; ?><br>
                                        <strong>Profit:</strong><?php echo $row->BOOKINGPROFIT; ?><br>
                                        <strong>Price:</strong><?php echo $row->BOOKINGPRICE; ?>
                                    </td>
                                    <td>
                                        <strong>Agent Paid:</strong> No<br>
                                        <strong>Provider Paid:</strong> No
                                    </td>
                                    <td><?php echo $row->BOOKINGSTATUS; ?></td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
            <!-- /.table-responsive -->

        </div>
    </div>

    <div class="row text-center">
        <ul class="pagination pagination-sm">
            <li><a href="#">&laquo;</a></li>
            <li class="disabled"><a href="#">1</a></li>
            <li class="active"><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li><a href="#">&raquo;</a></li>
        </ul>
    </div>
</div>
<!-- /#page-wrapper -->