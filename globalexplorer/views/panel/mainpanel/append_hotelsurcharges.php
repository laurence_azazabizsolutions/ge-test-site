<?php if($hotel->hasSurcharge()): ?>
    <?php foreach ($hotel->getSurcharges() as $surcharge): ?>
        <?php $this->view('panel/mainpanel/append_hotelsurcharge', array('surcharge' => $surcharge));?>
    <?php endforeach; ?>
<?php endif; ?>