<div id="page-wrapper">
    <!-- /.row -->
    <div class="row">
        <br>
        <div class="col-md-12">
        <?php if ($this->session->flashdata('alert_type')) { ?>
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <?php echo $this->session->flashdata('msg'); ?>
            </div>
        <?php } ?>
    </div>
        <div class="col-lg-12">
            <ul class="nav nav-pills dest_front_pages hide_package_view_display_tabs" role="tablist">
                <?php
                    if (isset($destinations)) {
                        $dest_act = false;
                        foreach ($destinations as $destination) {
                            if ($dest_act === false) {
                                echo '<li class="active"><a href="#'.$destination['code'].'" id="'.$destination['code'].'" onClick="change_pkg_views_front_pages(\''.$destination['code'].'\',this)" role="tab" data-toggle="tab">'.$destination['country'].'</a></li>';
                                $dest_act = true;
                            } else { echo '<li><a href="#'.$destination['code'].'" id="'.$destination['code'].'" onClick="change_pkg_views_front_pages(\''.$destination['code'].'\',this)" role="tab" data-toggle="tab">'.$destination['country'].'</a></li>'; }
                        }
                    }
                ?>
            </ul>
            <form class="form-horizontal hide_package_view_display_selects" role="form" name="form">
                <div class="form-group">
                    <label class="col-sm-3 control-label text-right">Destination</label>
                    <div class="col-sm-8">
                        <select class="form-control" name="cbxpackages" >
                            <option value=" ">- Select -</option>
                            <?php if (isset($destinations)): ?>
                                <?php foreach ($destinations as $destination): ?>
                                    <option value="<?php echo $destination['code']; ?>" ><?php echo $destination['country']; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>
            </form>
            <br>
            <form class="form-horizontal" role="form" method="post" name="form" action="<?php echo base_url('panel/Frontpagesset/');?>">
            <div id="id-displayspackagedata">
                <?php if (isset($packages)): ?>
                    <?php foreach($packages as $package): ?>
                        <div class="col-md-4">
                            <a href="<?php echo base_url('panel/Frontpagesset/'.$package->id)?>">
                                <div class="pax-img">
                                    <?php if($package->image_path!=''):?>
                                        <div class="pax-fill" style="background-image:url(<?php echo base_url($package->image_path) ?>);"></div>
                                    <?php else:?>
                                        <div class="pax-fill" style="background-image:url('../img/feat/default.png');"></div>
                                    <?php endif; ?>
                                </div>
                                <p class="pax-details small lead"><?php echo $package->code;?>: <?php echo $package->title;?> (Effective from <?php echo $package->date_from;?> - <?php echo $package->date_to; ?>)</p>
                            </a>
                            <label>
                                <input type="checkbox" name="cbxsetfrontpages[]" value="<?php echo $package->id; ?>">
                                Set Image
                            </label>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="form-group">
            <button type="submit" style="float:right;" class="btn btn-orange">Save</button><br><br>
        </div>
        </form>
    </div>
</div>
<!-- /#page-wrapper -->