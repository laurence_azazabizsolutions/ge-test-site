<?php 
#check if there are room rates ----------------------------------------------


if(!isset($none_table)){
?>

<!-- start of roomrates container -->
<div class="table-responsive">
    <table class="table table-striped table-bordered hotel_package_user_num_table">
		
		<!-- header -->
        <tr>
            <th class='brown text-center '>Room Type</th>
            <th class='brown text-center '>Room Rate</th>
            <th class='brown text-center <?php if($destination_index==$total_destinations){ echo "extension_container"; } ?>'  style="display:none;">Extension</th>
			<th class='brown text-center '>No. of Pax</th>
        </tr>
        <!-- end of header -->

		<?php if (isset($roomrates['standard'])): 
		$start_index = 0;?>
    	<?php foreach ($roomrates['standard'] as $roomrate): ?>
		<?php $price = ($roomrate['cost'] + $roomrate['profit']); ?> 
			<?php if($price!=0):?>
			<!-- table row data -->
	        <tr roomrateid="<?php echo $roomrate['id']; ?>" class="roomrate_row<?php echo $roomrate['id']; ?>">

	        	<!-- show room types -->
	            <td>
					<?php
						$roomrate_desc= "";
			            if(trim($roomrate['description'])!='')
			            	$roomrate_desc.=ucfirst($roomrate['description']).' - ';
			            	$roomrate_desc.=ucwords(str_replace('_', ' ', $roomrate['room_type']));
		       
		                echo $roomrate_desc;
	        		?>
	        		
    			</td>
    			<!-- room types -->

				<!-- room rates -->
	            <td>
  	            	
	                <b><?php echo $price; ?></b> / <?php echo ucwords(str_replace('/', ' / ', $roomrate['rate_type'])); ?>
<!--  	                <?php 
	                	if($price!=0){
	                		
	                	}else{
	                		echo "<script>$('.roomrate_row{$roomrate['id']}').hide();</script>";
	                	}
	                ?>   --> 
	            </td>
	            <!-- end of room rates -->
				

				<!-- show extension container -->
				<td class='<?php if($destination_index==$total_destinations){ echo "extension_container"; } ?>' style="display:none;">
					<?php 
						$ext_cost = $roomrate['ext_cost']==NULL ? 0 : $roomrate['ext_cost'];
						$ext_profit = $roomrate['ext_profit']==NULL ? 0: $roomrate['ext_profit'];
						$price_ext = 0;
						
						if($ext_cost==0 && $ext_profit==0){
							$ext_cost = $roomrate['cost'];
							$ext_profit = $roomrate['profit'];
							$price_ext = $ext_cost + $ext_profit;
							echo "<b>{$price}</b> / ".ucwords(str_replace('/', ' / ', $roomrate['rate_type']));
						}else{
							$price_ext = intval($ext_cost)+intval($ext_profit);
							echo "<b>{$price_ext}</b> / ".ucwords(str_replace('/', ' / ', $roomrate['rate_type']));
						}
					?>
					<input type="hidden" name="extension_roomrate_cost[<?php echo $destination_index; ?>][]" value="<?php echo $ext_cost; ?>">
					<input type="hidden" name="extension_roomrate_profit[<?php echo $destination_index; ?>][]" value="<?php echo $ext_profit; ?>">
					<input type="hidden" name="extension_roomrate_price[<?php echo $destination_index; ?>][]" value="<?php echo $price_ext; ?>">
				</td>	
				<!-- end of show extension container -->



				<!-- input value qty -->
	            <td>
	            	<?php
	            		$this->load->model('hotel/hotelroomrate'); 
	            		$pax = $this->hotelroomrate->getSummaryHotelRoom($booking_id,$roomrate_desc,$hotel_id);
	            		$room_type = $roomrate['room_type'];
	            		$input_class_name = "";
	            		if($room_type=="child_without_bed" || $room_type=="child_with_bed"){
	            			$input_class_name = "child_overflow_counter total_overflow_counter";
	            		}else if($room_type=="child_half_twin"){
	            			$input_class_name = "pair_overflow_counter";
	            		}else if($room_type=="breakfast"){
	            			$input_class_name = "bfast_overflow_counter";
	            		}else if($room_type=="extra_bed"){
	            			$input_class_name = "extrabed_overflow_counter total_overflow_counter";
	            		}else{
	            			$input_class_name = "paxroom_overflow_counter total_overflow_counter";
	            		}
	            	?>
	            	<!-- roomrates metadata -->
	            	<!-- <input name="roomrate_id[<?php echo $destination_index; ?>][]" id="roomrate_id<?php echo $roomrate['id'];  ?>" type="hidden" value="<?php echo $roomrate['id']; ?>">
	            	<input name="roomrate_price[<?php echo $destination_index; ?>][]" id="roomrate_price<?php echo $roomrate['id'];  ?>" type="hidden" value="<?php echo $price; ?>">
	            	<input name="roomrate_pax[<?php echo $destination_index; ?>][]" id="roomrate_pax<?php echo $roomrate['id'];  ?>" type="hidden" value="<?php echo $roomrate['pax']; ?>">
	                
	                <input ind="<?php echo $destination_index; ?>" name="roomrate_quantity[<?php echo $destination_index; ?>][]"  rr-id = "<?php echo $roomrate['id'];  ?>" room-cost="<?php echo $roomrate['cost']; ?>" room-profit="<?php echo $roomrate['profit'];  ?>"  id="roomrate_qty<?php echo $roomrate['id'];  ?>" rr-desc="<?php echo $roomrate_desc; ?>" rate_id="<?php echo $roomrate['id']; ?>" class="hotel_package_user_num hotel_package_user_num_roomrates roomrate_qty form-control min-size-num <?php echo $input_class_name; ?>" type="text" placeholder="<?php echo ucwords(str_replace('/', ' / ', $roomrate['rate_type'])); ?>" onkeypress="return isNumberKey(event)">
	           		
	           		<input name="roomrate_rate_type[<?php echo $destination_index; ?>][]" id="roomrate_rate_type<?php echo $roomrate['id'];  ?>" type="hidden" value="<?php echo $roomrate['rate_type'] ?>">
					<input name="roomrate_desc[<?php echo $destination_index; ?>][]" type="hidden" value="<?php echo $roomrate_desc; ?>">
					<input name="roomrate_profit[<?php echo $destination_index; ?>][]" type="hidden" value="<?php echo $roomrate['profit']; ?>" >
					<input name="roomrate_cost[<?php echo $destination_index; ?>][]" type="hidden" value="<?php echo $roomrate['cost']; ?>" > -->

					<input name="roomrate_id[<?php echo $destination_index; ?>][]" id="roomrate_id<?php echo $roomrate['id'];  ?>" type="hidden" value="<?php echo $roomrate['id']; ?>">
	            	<input name="roomrate_price[<?php echo $destination_index; ?>][]" id="roomrate_price<?php echo $roomrate['id'];  ?>" type="hidden" value="<?php echo $price; ?>">
	            	<input class="roomrate_pax<?php echo $start_index;?>" name="roomrate_pax[<?php echo $destination_index; ?>][]" id="roomrate_pax<?php echo $roomrate['id'];  ?>" type="hidden" value="<?php echo $roomrate['pax']; ?>">
	                
	                <?php if($pax):
	                	$pax_value = intval(strip_tags($pax[0]->computation));
	                endif;
	                ?>
	                <input ind="<?php echo $start_index; ?>" value="<?php echo @$pax_value?>" name="roomrate_quantity[<?php echo $destination_index; ?>][]"  rr-id = "<?php echo $roomrate['id'];  ?>" room-cost="<?php echo $roomrate['cost']; ?>" room-profit="<?php echo $roomrate['profit'];  ?>"  id="roomrate_qty<?php echo $roomrate['id'];  ?>" rr-desc="<?php echo $roomrate_desc; ?>" rate_id="<?php echo $roomrate['id']; ?>" class="hotel_package_user_num hotel_package_user_num_roomrates roomrate_qty form-control min-size-num <?php echo $input_class_name; ?>" type="text" placeholder="<?php echo ucwords(str_replace('/', ' / ', $roomrate['rate_type'])); ?>" onkeypress="return isNumberKey(event)">
	           		<?php $pax_value = '';?>
	           		<input name="roomrate_rate_type[<?php echo $destination_index; ?>][]" id="roomrate_rate_type<?php echo $roomrate['id'];  ?>" type="hidden" value="<?php echo $roomrate['rate_type'] ?>">
					<input name="roomrate_desc[<?php echo $destination_index; ?>][]" type="hidden" value="<?php echo $roomrate_desc; ?>">
					<input name="roomrate_profit[<?php echo $destination_index; ?>][]" type="hidden" value="<?php echo $roomrate['profit']; ?>" >
					<input name="roomrate_cost[<?php echo $destination_index; ?>][]" type="hidden" value="<?php echo $roomrate['cost']; ?>" >

					<input type="hidden" value="<?php echo $roomrate['room_type']; ?>"  id="roomrate_type<?php echo $roomrate['id'];  ?>">

					<!-- roomrates surcharges -->
					<div id="rrate_surcharges_container<?php echo $roomrate['id']; ?>" class="rrate_surcharges_container">
					<?php
						#dump($roomrate['id']);

						#get roomrate surcharges ----------------------------------------------
			    		$this->load->model("surcharge/surchargestorage");
			    		$roomrate_surcharges = $this->surchargestorage->getRoomRateSurcharge_id($roomrate['id']);

			    		#insert surcharges into an array ----------------------------------------------
						$surcharges = array("hotelrooms"=>$roomrate_surcharges);
						$dates = array("return"=>$return_value,"depart"=>$depart_value);

						#check if the room has surcharges ----------------------------------------------
						$room_surcharges = array();

						#if the room has surcharges get the surcharge for the selected date range ----------------------------------------------s
						if($roomrate_surcharges){
				    		#get the hotel surcharges ----------------------------------------------
							$room_surcharges = $this->surchargestorage->processSurchargeHotelRooms($surcharges, $dates);
						}

						#count the room surcharges ----------------------------------------------
						$count_test = count($room_surcharges);

						//insert into array ----------------------------------------------
						$arr = array();
						
						if($count_test!=0){
							foreach ($room_surcharges as $categ) {
								foreach ($categ as $surcharge) {
									$arr[]=$surcharge;
								}		
							}
						}

						//final counter ----------------------------------------------
						$final_count = count($arr);



						#set if final checker count is set to zero ----------------------------------------------
						if($final_count!=0){
							//temporary container for all the surcharges
							$tmp_surchage_array = array();
							foreach($arr as $rr_surcharge){
								#dump($rr_surcharge);
								#get the price by adding the cost and profit ----------------------------------------------
								$price = (int)$rr_surcharge['cost'] + (int)$rr_surcharge['profit'];
								$surcharge_id = $rr_surcharge['id'];
								$rr_id = $roomrate['id'];

								echo "<input type='hidden' name='room_surcharge_cost[{$destination_index}][]' rate_type='{$rr_surcharge['rate_type']}' rate_id='{$roomrate['id']}' class='total_cost_elem' value='{$rr_surcharge['profit']}'>";
								echo "<input type='hidden' name='room_surcharge_profit[{$destination_index}][]' rate_type='{$rr_surcharge['rate_type']}' rate_id='{$roomrate['id']}'   class='total_profit_elem' value='{$rr_surcharge['cost']}'>";
								echo "<input type='hidden' name='room_surcharge_profit_total[{$destination_index}][]' rule='{$rr_surcharge['rule']}' rule_type='{$rr_surcharge['rule_type']}'  value='{$price}' rate_type='{$rr_surcharge['rate_type']}' room_id='{$rr_id}' rate_id='{$surcharge_id}' class='r_surcharge_inputs'>";
								echo "<input type='hidden' name='room_surcharge_description[{$destination_index}][]' id='room_surcharge_description{$surcharge_id}' class='description' value='{$rr_surcharge['description']}'  />";
								
								$tmp_surchage_array[] = $rr_surcharge['id']; 
							}
							

							#room surcharges ----------------------------------------------
							$roomrate_surcharge = implode(",", $tmp_surchage_array);

						}else{
							#room surcgarges ----------------------------------------------
							$roomrate_surcharge = 0;
						}
						#check if final checker count is set to zero ----------------------------------------------
					?>

					</div>
					<!-- end of roomrates surcharges -->
						
					<input type='hidden' name='roomrate_surcharges[<?php echo $destination_index; ?>][]' id='roomrate_surcharges<?php echo $roomrate['id'];  ?>' value='<?php echo $roomrate_surcharge;  ?>'>
					<!-- end of roomrates metadata -->
	            	<?php $start_index++;?>
	            </td>
	            <!-- end of input value qty -->
		
	       	

	        </tr>
			<!-- end of table row data -->
	
		<?php endif; ?>
        <?php endforeach; //dump($roomrates);
        ?>
        <?php endif; ?>
        	<!-- start of remark -->
	        <tr class='<?php if($destination_index==$total_destinations){ echo "extension_container"; } ?>' style='display:none;'>
	            <td>Remarks : </td>
	            <td colspan="3"><textarea name="exremarks" class="form-control" style="resize:none;" rows="3"></textarea></td>
	        </tr>
	        <!-- end of remark -->
    </table>
</div>
<!-- end of roomrates container -->


<!-- start of extension container -->
<?php
}
?>
<!-- end of extension container -->



<script>
	$(document).ready(function(){
		var row_span = "";
		var destination_id = $('.package_destination_container').attr("destination-id");
		var total_guests   = nanChecker($("#total_holder_all").val());
		var current_pax	   = getDestinationPax($('#roomrates_container'+destination_id));
		if(current_pax==total_guests && current_pax!=0){
			$("#days_overflow"+destination_id).val("").empty();
			console.log(current_pax);
		}else{
			$("#days_overflow"+destination_id).val("days_true");
			row_span+="<div>The entered pax does not match the total guest count!</div>";
			console.log(current_pax);
		}

		//check user inputs for num ----------------------------------------------
		$('.hotel_package_user_num_table').find('.hotel_package_user_num').each(function(i,e){
			$(e).blur(function(){
				//checkTotalGuestsCounter();
				surchargeGeneration("false");
			}).keyup(function(){
				var pax_room_total = 0;

				$('.package_destination_container').each(function(i,e){
					$("#supplmentary_errors"+destination_id).empty().hide();
					//error container ----------------------------------------------
					var row_span = "";	

					//set the destinaton id ----------------------------------------------
					var destination_id = $(e).attr("destination-id");
					
					//get the total guests ----------------------------------------------
					var total_guests   = nanChecker($("#total_holder_all").val());
					var child_count    = nanChecker($("[name='child_count']").val());
					var adult_count    = nanChecker($("[name='adult_count']").val());
					
					//current total pax of the selected destination ----------------------------------------------
					var current_pax	   = getDestinationPax($('#roomrates_container'+destination_id));


					//handlers to true ----------------------------------------------
					var bfast          = true;
					var extrabed       = true;
					var childo         = true;
					
					//total counters ----------------------------------------------
					var total_bfast    = 0;
					var total_extrabed = 0;
					var total_child    = 0;
					var total_adult	   = 0;

					//pax room checker ----------------------------------------------
					
					var destination_pax_room = 0;
					var pax_room_checker     = true;
					var pax_room_checker_pax = 0;

					//error counter ----------------------------------------------
					var error_counter = 0;

					//check if the number of people matches with  the input field value----------------------------------------------
					$(e).find('.bfast_overflow_counter').each(function(i,e){
						var tmp = nanChecker($.trim($(e).val()));
						if(tmp!=0){
							total_bfast+=tmp;
						}
					});	

					//check if the bed matches with the coutner ----------------------------------------------
					$(e).find('.extrabed_overflow_counter').each(function(i,e){
						var tmp = nanChecker($.trim($(e).val()));
						if(tmp!=0){
							total_extrabed+=tmp;
						}
					});	

					//check if the number of children matches with the total child ----------------------------------------------
					$(e).find('.child_overflow_counter').each(function(i,e){
						var tmp = nanChecker($.trim($(e).val()));
						if(tmp!=0){
							total_child+=tmp;
						}
					});	

					//check if the number of children is paired with an adult ----------------------------------------------
					$(e).find('.pair_overflow_counter').each(function(i,e){
						var tmp = nanChecker($.trim($(e).val()));
						var tmpc = nanChecker($.trim($(e).val()));
						if(current_pax!=total_guests){
							total_child+=tmpc;
							current_pax+=tmp;
						}
					});

					//check the pax overflow counter
					$(e).find('.paxroom_overflow_counter').each(function(i,e){
						var rate_id = $(e).attr("rate_id");
						var counterss = $(e).attr("ind");
						var tmp_val = $.trim($(e).val());
						var pax_num = $(".roomrate_pax"+counterss).val();
						// var pax_num = $("#roomrate_pax"+rate_id).val(); 
						var room_type = $.trim($("#roomrate_type"+rate_id).val());

						if(tmp_val!=0){
							total_adult+=nanChecker(tmp_val);

							if(room_type!="child_with_bed" && room_type!="child_without_bed" && room_type!="breakfast" && room_type!="extra_bed"){
								var pax_room=(tmp_val/pax_num);
								var rounded_room = Math.round(pax_room);
								// console.log(pax_num);
								// console.log(tmp_val);
								// console.log(pax_num);
								// console.log(pax_room);
								if(rounded_room==pax_room){
									destination_pax_room+=pax_room;
									pax_room_total+=pax_room;
								}else{
									$(e).val('').empty();
									pax_room_checker = false;
									pax_room_checker_pax = pax_num;												
								}	
							}

						}
					});
					
					//set the pax room ----------------------------------------------
					$("#room_count"+destination_id).val(destination_pax_room);

				

					//check if the total bfast matches with the number of total guests ----------------------------------------------
					if(total_bfast>total_guests && total_bfast!=0){
						$("#bfast_overflow"+destination_id).val("bfast_true");
						row_span+="<div>Breakfast count exceeds the number of guests entered.</div>";
					}else{
						$("#bfast_overflow"+destination_id).val("").empty();
					}

					//check if the total entered child matches with the bed count ----------------------------------------------
					if(total_child<child_count && total_child!=0 ){
						if(child_count!=0){
							$("#child_overflow"+destination_id).val("child_true");
							row_span+="<div>The number of child(ren) entered does not match with the child count.</div>";
						}else{
							$("#child_overflow"+destination_id).val("").empty();
						}
					}else{
						$("#child_overflow"+destination_id).val("").empty();
					}

					//check if current pax per destination matches with the total guest ----------------------------------------------
					if(current_pax==total_guests && current_pax!=0){
						$("#days_overflow"+destination_id).val("").empty();
					}else{
						$("#days_overflow"+destination_id).val("days_true");
						row_span+="<div>The entered pax does not match the total guest count!</div>";
					}

					//check if adult
					if(total_adult==adult_count){
						$("#adult_overflow"+destination_id).val("").empty();
					}else{
						$("#adult_overflow"+destination_id).val("adult_true");
						row_span+="<div>Total number of adults does not match the total adult count</div>";
					}

					if(total_child==child_count){
						$("#adult_overflow"+destination_id).val("").empty();
					}else{
						$("#adult_overflow"+destination_id).val("adult_true");
						row_span+="<div>Total number of child does not match the total adult count</div>";
					}

					if(!pax_room_checker){
						row_span+="<div>Minimum number of occupants : "+pax_room_checker_pax+"</div>";
					}

					if(row_span.length){
						$("#supplmentary_errors"+destination_id).empty().show().append(row_span);
						surchargeGeneration("false");
					}else{
						$("#supplmentary_errors"+destination_id).hide();
					}

					console.log("____________________________________");
					console.log("Adult count: "+total_adult+"/"+adult_count);
					console.log("Child count: "+total_child+"/"+child_count);
					console.log("Pax count: "+current_pax+"/"+total_guests);
					console.log("____________________________________");
				});
				
				//set the value of the room count to the pax room ----------------------------------------------
				$("#room_count").val(pax_room_total);

			});
		});
		//end of check user inputs for num ----------------------------------------------

	});	
</script>