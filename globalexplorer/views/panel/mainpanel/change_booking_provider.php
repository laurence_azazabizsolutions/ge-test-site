<div id="page-wrapper">
    <!-- /.row -->
    <div class="row">
        <br>
        <div class="col-md-4">
            <?php
                $img = base_url('../img/feat/default.png');
                if($package->getImagePath()!='') $img = base_url('../'.$package->getImagePath());
            ?>
            <img class="thumbnail img-responsive" src="<?php echo $img; ?>" alt="...">
            <form id="stepbooking_form" class="form-horizontal" role="form" method="post" action="<?php echo base_url('panel/step_hotel')?>">
                <input type="hidden" name="package_id" value="<?php echo $package->getId();?>" />
                <input type="hidden" id="package_days" value="<?php echo $package->getDays();?>" />
                <!-- <input class="form-control" type="hidden" name="infant_cnt" value="0" readonly/> -->
                <div class="form-group">
                    <label class="col-sm-4 control-label">Adult</label>
                    <div class="col-sm-8">
                        <select class="form-control count_val" name="adult_count">
                            <option value='' selected>Adults(12+ years)</option>
                            <option>0</option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                            <option>8</option>
                            <option>9</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Children</label>
                    <div class="col-sm-8">
                        <select class="form-control count_val" name="child_count">
                            <option value='' selected>Children(Below 12 years)</option>
                            <option>0</option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                            <option>8</option>
                            <option>9</option>
                        </select>
                    </div>
                </div>
                <div id="child_wrapper">

                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Guest</label>
                    <div class="col-sm-8">
                        <input name="total_holder" class="form-control" type="number" placeholder="Total" readonly/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Departure</label>
                    <div class="col-sm-8">
                        <input id="datetimepickerBookingDepart" name="depart_date" type="calendar" data-date-format="YYYY-MM-DD" class="form-control" placeholder="Departure" readonly/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Return</label>
                    <div class="col-sm-8">
                        <input id="datetimepickerBookingReturn" name="return_date" type="calendar" data-date-format="YYYY-MM-DD" class="form-control" placeholder="Return" readonly/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <th class="text-right"><h3>Total:</h3></th>
                                    <th><h3><em>P123456</em></h3></th>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-8">
            <h3><?php echo $package->getTitle();?> (Effective from <?php echo $package->getDateFrom();?> - <?php echo $package->getDateTo();?>)</h3>
            <strong>Package includes:</strong><br><br>
            <?php echo nl2br(htmlentities($package->getDescription())); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <form id="stephotel_form" class="form-horizontal" role="form" action="<?php echo base_url('panel/booking_change'); ?>" method="post">
                <div class="panel panel-orange">
                    <input type="hidden" name="packageid" value="<?php echo $packageid;?>" />
                    <input tpye="hidden" name="bookid_change" value="<?php echo $boookid;?>"/>
                    <input tpye="hidden" name="rf_to_change" value="<?php echo $rf_to;?>"/>
                    <input tpye="hidden" name="rf_from_change" value="<?php echo $rf_from;?>"/>
                    <input tpye="hidden" name="of_from_change" value="<?php echo $of_from;?>"/>
                    <input tpye="hidden" name="of_to_change" value="<?php echo $of_to;?>"/>
                    <input tpye="hidden" name="airportcode_change" value="<?php echo $airportcode;?>"/>
                    <!-- Default panel contents -->
                    <div class="panel-heading text-center"><strong>Package Hotel</strong></div>
                    <div id="rooms_div" class="panel-body">
                        <div class="form-group">
                            <label class="col-md-4 control-label">Hotel Choice :</label>
                            <div class="col-md-5">
                                <select class="form-control" name="package_hotel_choice">
                                    <option value=" ">- Select -</option>
                                    <?php if (isset($package_hotels)): ?>
                                        <?php foreach ($package_hotels as $package_hotel): ?>
                                            <option value="<?php echo $package_hotel['package_hotel_id']; ?>"><?php echo $package_hotel['name']; ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>

                        <!-- Table -->
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <tr>
                                    <th class="brown text-center" colspan="3">Package</th>
                                </tr>
                            </table>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <tr>  
                                    <th class="brown text-center" colspan="3">Rooms Only</th>
                                </tr>
                            </table>
                        </div>

                    </div>
                </div>

                <div class="panel panel-orange">
                    <!-- Default panel contents -->
                    <div class="panel-heading text-center"><strong>Optional Tours / Add Ons</strong></div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <tr>
                                    <th>Description</th>
                                    <th>Price</th>
                                    <th>Qty</th>
                                </tr>
                                <?php if(isset($addons)):?>
                                    <?php foreach ($addons as $addon): ?>    
                                        <tr>
                                            <td><?php echo $addon['description']; ?></td>
                                            <td>
                                                <?php $price = ($addon['cost'] + $addon['profit']); echo $price; ?> / <?php echo $addon['unit']; ?>
                                                <input name="addon_id[]" type="hidden" value="<?php echo $addon['id']; ?>">
                                                <input name="addon_price[]" type="hidden" value="<?php echo $price; ?>">
                                            </td>
                                            <td><input name="addon_quantity[]" class="form-control min-size-num" type="number"></td>
                                        </tr>
                                    <?php endforeach;?>
                                <?php endif;?>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="panel panel-orange">
                    <!-- Default panel contents -->
                    <div class="panel-heading text-center"><strong>Additional Hotel Arrangments</strong></div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <tr>
                                    <th></th>
                                    <th>Hotel Name</th>
                                    <th colspan="2">Remarks</th>
                                </tr>
                                <tr>
                                    <td>Hotel 2</td>
                                    <td>
                                        <select name="exhotel_name" class="form-control min-size-name">
                                            <option value=" ">- Select -</option>
                                        </select>
                                    </td>
                                    <td>
                                        <div class="input-group date" id="datetimepickerCheckIn">
                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                            <input name="exhotel_checkin" type="calendar" data-date-format="YYYY-MM-DD" class="form-control" readonly/>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="input-group date" id="datetimepickerCheckOut">
                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                            <input name="exhotel_checkout" type="calendar" data-date-format="YYYY-MM-DD" class="form-control" readonly/>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Remarks</td>
                                    <td colspan="3"><textarea name="exhotel_remarks" class="form-control" style="resize:none;" rows="3"></textarea></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <button class="btn btn-orange center-block" role="button">Continue</button>
                <br>
            </form>
        </div>
    </div>
</div>
<!-- /#page-wrapper -->
