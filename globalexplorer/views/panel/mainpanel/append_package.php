
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <h3>Summary</h3>
            <strong>Booked On:</strong> <?php echo date('d-M-Y',strtotime(@$data_summary[0]->BOOKDATE));?><br>
            <strong>Package:</strong> <?php echo @$data_summary[0]->PACNIGHTS - 1;?> nights<br>
            <strong>Extension:</strong>
            <?php
                $total_extended= @$data_summary[0]->BOOKEDEXTENDDAYS;
                echo $total_extended;
            ?>
            nights<br>
            <strong>Totals:</strong>
            <?php
                $totals= $total_extended + @$data_summary[0]->PACNIGHTS - 1;
                echo  $totals;
            ?>
            nights<br>
            <?php
                if (@$data_summary[0]->BOOKSTATUS == "ammended") {
                    @$data_summary[0]->BOOKSTATUS = "amended";
                }
            ?>
            <h4><strong>Status:</strong> <span id="action_append"><?php echo ucfirst(@$data_summary[0]->BOOKSTATUS);?></span></h4>
            <br>
            <div class="modal fade" data-toggle="modal" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false">
                <div class="modal-body" style="margin:auto">
                    <div class="progress">
                      <div class="progress-bar progress-bar-danger progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
                        <span class="sr-only">45% Complete</span>
                      </div>
                    </div>
                </div>
            </div>
            <?php
                $d=strtotime("-0 Days");
                $datetime1=date_create(date("Y-m-d", $d));
                $datetime2=date_create(@$data_summary[0]->PACKCHECKIN);
                $diff=date_diff($datetime1,$datetime2);

                $booked_id=@$data_summary[0]->BOOKID;
                $packages_id=@$data_summary[0]->packages_id;

                if ($show_hide == 0) {
                    if (isset($user) && get_class($user) == 'Agent') {
                        if (@$data_summary[0]->BOOKSTATUS == "confirmed") {
                            echo '<a href="'.base_url('panel/pdf/'.$booked_id).'" class="btn btn-orange btn-xs" target="_blank">Invoice</a> |
                             <a href="'.base_url('panel/step_booking/'.$packages_id.'/'.$booked_id).'" class="btn btn-orange btn-xs">Amend</a> |
                             <a href="'.base_url('panel/voucher/'.$booked_id).'" class="btn btn-orange btn-xs" target="_blank">Service Voucer</a> |
                             <a href="" class="btn btn-orange btn-xs" data-toggle="modal" data-target="#myModalPassengers">View Passengers</a>';
                            echo "<br><br>";
                        }
                        else if (@$data_summary[0]->BOOKSTATUS == "amended" || @$data_summary[0]->BOOKSTATUS == "pending" || @$data_summary[0]->BOOKSTATUS == "change hotel" || @$data_summary[0]->BOOKSTATUS == "change room type") {
                            if($diff->format("%R") == "+"){
                                echo '<a href="'.base_url('panel/step_booking/'.$packages_id.'/'.$booked_id).'" class="btn btn-orange btn-xs">Amend</a> |
                                <a href="'.base_url('panel/voucher/'.$booked_id).'" class="btn btn-orange btn-xs" target="_blank">Service Voucer</a> |
                                <a href="" class="btn btn-orange btn-xs" data-toggle="modal" data-target="#myModalPassengers">View Passengers</a>';
                                echo "<br><br>";
                            }
                        }else if (@$data_summary[0]->BOOKSTATUS == "rejected") {
                            echo '<a href="#" onclick="status_booking('.$booked_id.',4); return false;" class="btn btn-orange btn-xs">Back to Amend</a> |
                            <a href="" class="btn btn-orange btn-xs" data-toggle="modal" data-target="#myModalPassengers">View Passengers</a>';
                            echo "<br><br>";
                        }
                    }
                    else if (isset($user) && get_class($user) == 'Provider') {
                        if (@$data_summary[0]->BOOKSTATUS == "amended" || @$data_summary[0]->BOOKSTATUS == "pending") {
                            echo '<a href="#" onclick="status_booking('.$booked_id.',3); return false;" class="btn btn-orange btn-xs">Confirm</a> |
                            <a href="#" onclick="status_booking('.$booked_id.',2); return false;" class="btn btn-orange btn-xs">Reject</a> |
                            <a href="#" onclick="status_booking('.$booked_id.',1); return false;" class="btn btn-orange btn-xs">Cancel</a> |
                            <a href="'.base_url('panel/voucher/'.$booked_id).'" class="btn btn-orange btn-xs" target="_blank">Service Voucer</a> |
                            <a href="" class="btn btn-orange btn-xs" data-toggle="modal" data-target="#myModalPassengers">View Passengers</a>';
                            echo "<br><br>";
                        }
                        else if (@$data_summary[0]->BOOKSTATUS == "rejected") {
                            echo '<a href="#" onclick="status_booking('.$booked_id.',4); return false;" class="btn btn-orange btn-xs">Back to Amend</a> |
                            <a href="" class="btn btn-orange btn-xs" data-toggle="modal" data-target="#myModalPassengers">View Passengers</a>';
                            echo "<br><br>";
                        }
                    }
                    else if (isset($user) && get_class($user)== 'Admin') {
                        if (@$data_summary[0]->BOOKSTATUS == "confirmed") {
                            // if($diff->format("%R") == "+"){
                            //     echo '<a href="'.base_url('panel/step_booking/'.$packages_id.'/'.$booked_id).'" class="btn btn-orange btn-xs">Amend</a> | ';
                            // }
                            echo '<a href="#" onclick="status_booking('.$booked_id.',1); return false;" class="btn btn-orange btn-xs">Cancel</a> |
                            <a href="'.base_url('panel/pdf/'.$booked_id).'" class="btn btn-orange btn-xs" target="_blank">Invoice</a> |
                            <a href="'.base_url('panel/voucher/'.$booked_id).'" class="btn btn-orange btn-xs" target="_blank">Service Voucer</a> |
                            <a href="'.base_url('panel/step_booking/'.$packages_id.'/'.$booked_id).'" class="btn btn-orange btn-xs">Amend</a> |
                            <a href="" class="btn btn-orange btn-xs" data-toggle="modal" data-target="#myModalDiscount">Discount</a> |
                            <a href="" class="btn btn-orange btn-xs" data-toggle="modal" data-target="#myModalAdditionalC">Additional Charge</a> |
                            <a href="" class="btn btn-orange btn-xs" data-toggle="modal" data-target="#myModalResent">Resend Email</a> |
                            <a href="" class="btn btn-orange btn-xs" data-toggle="modal" data-target="#myModalPassengers">View Passengers</a> ';
                            echo "<br><br>";
                            // <a href="#" class="btn btn-orange btn-xs" data-toggle="modal" data-target="#myModalDiscount">Discount</a> |
                        }
                        else if (@$data_summary[0]->BOOKSTATUS == "amended" || @$data_summary[0]->BOOKSTATUS == "pending" || @$data_summary[0]->BOOKSTATUS == "change hotel" || @$data_summary[0]->BOOKSTATUS == "change room type") {
                            if($diff->format("%R") == "+"){
                                echo '<a href="'.base_url('panel/step_booking/'.$packages_id.'/'.$booked_id).'" class="btn btn-orange btn-xs">Amend</a> | ';
                            }
                            echo '<a href="#" onclick="status_booking('.$booked_id.',3); return false;" class="btn btn-orange btn-xs">Confirm</a> |
                            <a href="#" onclick="status_booking('.$booked_id.',2); return false;" class="btn btn-orange btn-xs">Reject</a> |
                            <a href="#" onclick="status_booking('.$booked_id.',1); return false;" class="btn btn-orange btn-xs">Cancel</a> |
                            <a href="'.base_url('panel/pdf/'.$booked_id).'" class="btn btn-orange btn-xs" target="_blank">Invoice</a> |
                            <a href="'.base_url('panel/voucher/'.$booked_id).'" class="btn btn-orange btn-xs" target="_blank">Service Voucer</a> |
                            <a href="" class="btn btn-orange btn-xs" data-toggle="modal" data-target="#myModalDiscount">Discount</a> |
                            <a href="" class="btn btn-orange btn-xs" data-toggle="modal" data-target="#myModalAdditionalC">Additional Charge</a> |
                            <a href="" class="btn btn-orange btn-xs" data-toggle="modal" data-target="#myModalResent">Resend Email</a> |
                            <a href="" class="btn btn-orange btn-xs" data-toggle="modal" data-target="#myModalPassengers">View Passengers</a>';
                            echo "<br><br>";
                            // <a href="#" class="btn btn-orange btn-xs" data-toggle="modal" data-target="#myModalDiscount">Discount</a> |
                        }
                        else if (@$data_summary[0]->BOOKSTATUS == "cancelled") {
                            echo '<a href="'.base_url('panel/pdf/'.$booked_id).'" class="btn btn-orange btn-xs" target="_blank">Invoice</a> |
                            <a href="'.base_url('panel/voucher/'.$booked_id).'" class="btn btn-orange btn-xs" target="_blank">Service Voucer</a> |
                            <a href="'.base_url('panel/step_booking/'.$packages_id.'/'.$booked_id).'" class="btn btn-orange btn-xs">Amend</a> |
                            <a href="" class="btn btn-orange btn-xs" data-toggle="modal" data-target="#myModalDiscount">Discount</a> |
                            <a href="" class="btn btn-orange btn-xs" data-toggle="modal" data-target="#myModalAdditionalC">Additional Charge</a> |
                            <a href="" class="btn btn-orange btn-xs" data-toggle="modal" data-target="#myModalResent">Resend Email</a> |
                            <a href="" class="btn btn-orange btn-xs" data-toggle="modal" data-target="#myModalPassengers">View Passengers</a>';
                            echo "<br><br>";
                        }else if (@$data_summary[0]->BOOKSTATUS == "rejected") {
                            echo '<a href="#" onclick="status_booking('.$booked_id.',4); return false;" class="btn btn-orange btn-xs">Back to Amend</a>';
                            echo "<br><br>";
                        }
                    }
                }
            ?>

            <div class="panel panel-orange">
                <div class="panel-heading">
                    <h3 class="panel-title">Flight and Traveller Details</h3>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>BOOKING DETAILS</th>
                                <th>DEPARTURE</th>
                                <th>RETURN</th>
                                <th>GUEST/S</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                  <strong>Booking No:</strong> <?php echo @$data_summary[0]->BOOKNO;?> <br>
                                  <strong>Tour Code:</strong> <?php echo @$data_summary[0]->PACTOURCODE;?><br>
                                </td>
                                <td>
                                  <strong> <?php echo @$data_summary[0]->BOOKDEPFROM;?>-<?php echo @$data_summary[0]->BOOKDEPTO; ?></strong> <br>
                                  <?php echo date('d-M-Y',strtotime(@$data_summary[0]->PACKCHECKIN)); ?> <br>
                                  <?php echo @$data_summary[0]->BOOKDEPARTDATE; ?>-<?php echo @$data_summary[0]->BOOKDEPARTADATE; ?> <br>
                                  <?php echo @$data_summary[0]->BOOKDEPARTFLIGHTNO; ?>
                                </td>
                                <td>
                                  <strong> <?php echo @$data_summary[0]->BOOKRETFROM;?>-<?php echo @$data_summary[0]->BOOKRETTO;?></strong> <br>
                                  <?php echo date('d-M-Y',strtotime(@$data_summary[0]->PACKCHECKOUT)); ?> <br>
                                  <?php echo @$data_summary[0]->BOOKRETURNDATE; ?>-<?php echo @$data_summary[0]->BOOKRETURNADATE; ?> <br>
                                  <?php echo @$data_summary[0]->BOOKRETURNFLIGHTNO; ?>
                                </td>
                                <td>
                                    <?php if (isset($guest_summary)): ?>
                                        <?php $counter=0; foreach ($guest_summary as $row): ?>
                                        <?php echo $counter+1;?>. <?php echo @$row->name;?> (<?php echo @$row->type;?>)<br>
                                        <?php $counter++; endforeach; ?>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Flight Remarks:</th>
                                <td colspan="3"><?php echo @$data_summary[0]->BOOKREMARKS;?></td>
                            </tr>
                            <tr>
                                <th>Booking Remarks:</th>
                                <td colspan="3"><em><?php echo @$data_summary[0]->PACKREMARKS;?></em></td>
                            </tr>
                            <!--<tr>
                                <th>Optional Hotel:</th>
                                <td colspan="3"><?php //echo @$data_summary[0]->PACKEXHOTELNAME;?></td>
                            </tr> -->
                            <?php
                            @$hotel_name=explode(',',$add_hotel_bookings[0]->ext_hotel_name);
                            if(@$hotel_name[0] != "" || @$hotel_name[1] != ""){ ?>
                            <tr>
                                <th>Addtional Hotel bookings:</th>
                                <td colspan="3"></td>
                            </tr>
                            <?php }?>
                        </tfoot>
                    </table>
                    <?php if(count(@$add_hotel_bookings[0]->ext_hotel_name)!=0 || count(@$add_hotel_bookings[0]->ext_hotel_chkin_chkout) != 0 || @$add_hotel_bookings[0]->ext_hotel_chkin_chkout != "" || @$add_hotel_bookings[0]->ext_hotel_name != ""){ ?>
                    <?php
                    $hotel_name=explode(',',$add_hotel_bookings[0]->ext_hotel_name);
                    $chkin_chkout=explode(',',$add_hotel_bookings[0]->ext_hotel_chkin_chkout);
                    $chkin_chkout_=explode(':',@$chkin_chkout[0]);
                    $chkin_chkouts_=explode(':',@$chkin_chkout[1]);
                    if(@$hotel_name[0] != "" || @$hotel_name[1] != ""){
                    ?>

                    <table class="table">
                        <thead>
                            <tr>
                                <th>Hotel</th>
                                <th>Check In Date</th>
                                <th>Check Out Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(!empty($hotel_name[0])){?>
                            <tr>
                                <td>
                                <?php if(isset($hotel_name[0])){
                                    echo  $hotel_name[0];} ?>
                                </td>
                                <td>
                                    <?php if(isset($chkin_chkout_[0])){
                                        echo  date('d-M-Y',strtotime($chkin_chkout_[0]));
                                    }?>
                                </td>
                                <td>
                                    <?php if(isset($chkin_chkout_[1])){
                                        echo  date('d-M-Y',strtotime($chkin_chkout_[1]));
                                    }?>
                                </td>
                            </tr>
                            <?php }?>
                            <?php if(!empty($hotel_name[1])){?>
                            <tr>
                                <td>
                                    <?php if(isset($hotel_name[1])){
                                        echo  $hotel_name[1];
                                    } ?>
                                </td>
                                <td>
                                    <?php if(isset($chkin_chkouts_[1])){
                                        echo  date('d-M-Y',strtotime($chkin_chkouts_[0]));
                                    }?>
                                </td>
                                <td>
                                    <?php if(isset($chkin_chkouts_[1])){
                                        echo  date('d-M-Y',strtotime($chkin_chkouts_[1]));
                                    }?>
                                </td>
                            </tr>
                            <?php }?>
                        </tbody>
                    </table>
                    <?php } }?>
                </div>
            </div>

            <div class="panel panel-orange">
                <div class="panel-heading">
                    <h3 class="panel-title">Package Hotel and Optional Tours</h3>
                </div>

                <div class="table-responsive ">
                    <table class="table" style="margin-bottom:20px;">
                        <thead>
                            <tr>
                                <th>CHECK IN</th>
                                <th>CHECK OUT</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td> <?php echo date('d-M-Y',strtotime(@$data_summary[0]->PACKCHECKIN));?></td>
                                <td> <?php echo date('d-M-Y',strtotime(@$data_summary[0]->PACKCHECKOUT));?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <p class="text-center"><strong>SUMMARY</strong></p>
                <div class="table-responsive ">
                    <table class="table">
                        <tr>
                            <th colspan="3" style="border-top:0px;">Package</th>
                        </tr>
                        <tr>
                            <td colspan="3" style="border-top:0px;"><?php echo @$data_summary[0]->PACTOURTITLE;?></td>
                        </tr>

                        <!-- body of summary -->
                        <?php
                            $total_grand = 0;

                            #package summary -------------------------------------------
                            if(count($package_summary)!=0){
                                echo "
                                <tr class='summary_package_container'>
                                    <th colspan='3' style='border-top:0px;'>Package Surcharges</th>
                                </tr>";

                                foreach ($package_summary as $destination) {
                                    $total_grand+=$destination["subtotal"];

                                    echo "
                                    <tr class='summary_package_container'>
                                        <td>{$destination['description']}</td>
                                        <td>{$destination['computation']}</td>
                                        <td>{$destination['subtotal']} SGD</td>
                                    </tr>";
                                }
                            }
                            #end of package summary ------------------------------------

                            #destinations ----------------------------------------------
                            if(count($destination_summary)!=0){
                                for($i=0;$i<count($destination_summary);$i++){
                                    $destination = $destination_summary[$i];
                                    #destination header ----------------------------------------------
                                    echo "
                                    <tr>
                                        <th colspan='3' style='border-top:0px;'>Hotel</th>
                                    </tr>
                                    <tr>
                                        <td colspan='3' style='border-top:0px;'>{$r_summary[$i][0]['hotel_name']}</td>
                                    </tr>";
                                    #end of destination header -------------------------

                                    #hotel surcharges ----------------------------------
                                    if(count($hotel_summary[$i])!=0){

                                        echo "
                                        <tr>
                                            <th class='text-left' colspan='3' style='border-top:0px;'>Hotel Surcharges</th>
                                        </tr>";

                                        foreach ($hotel_summary[$i] as $hotel) {
                                            $total_grand+=$hotel["subtotal"];
                                            echo "
                                            <tr>
                                                <td>{$hotel['description']}</td>
                                                <td>{$hotel['computation']}</td>
                                                <td>{$hotel['subtotal']} SGD</td>
                                            </tr>";
                                        }
                                    }
                                    #end of hotel surcharges ---------------------------

                                    #roomrate surcharges -------------------------------
                                    if(count($rs_summary[$i])!=0){
                                        echo "
                                        <tr>
                                            <th class='text-left' colspan='3' style='border-top:0px;'>
                                                <br>
                                                Roomrate Surcharges
                                            </th>
                                        </tr>";

                                        foreach ($rs_summary[$i] as $rs) {
                                            $total_grand+=$rs["subtotal"];
                                            echo "
                                             <tr>
                                                <td>{$rs['description']}</td>
                                                <td>{$rs['computation']}</td>
                                                <td>{$rs['subtotal']} SGD</td>
                                            </tr>";
                                        }
                                    }
                                    #end of roomrate surcharges ------------------------

                                    #rooms summaries -----------------------------------
                                    if(count($r_summary[$i])!=0){
                                        echo "
                                        <tr>
                                            <th class='text-left' colspan='3' style='border-top:0px;'>
                                                <br>
                                                Rooms
                                            </th>
                                        </tr>";

                                        foreach ($r_summary[$i] as $r) {
                                            $discount = $r['discount'];
                                            $additional = $r['additional'];
                                            $comp = $r['computation'];
                                            $comp_exp = explode(" ", $comp);
                                            $comp_val = $r['subtotal'] / intval(strip_tags($comp_exp[0]));
                                            $comp_type = ($comp_exp[count($comp_exp)-1]);


                                            $total_grand+=$r["subtotal"];
                                            echo "
                                             <tr>
                                                <td>{$r['description']}</td>
                                                <td>{$r['computation']} ({$comp_val} / pax / {$comp_type})</td>
                                                <td>{$r['subtotal']} SGD</td>
                                            </tr>";

                                            //dump($r_summary[$i]);
                                        }
                                    }
                                    #end of rooms summaries ----------------------------
                                }
                            }
                            #end of destinations ---------------------------------------


                            #addon summary ---------------------------------------------
                            if(count($addon_summary)!=0){
                                echo "
                                <tr>
                                    <th colspan='3' style='border-top:0px;'>
                                        <br>
                                        Addons
                                    </th>
                                </tr>";

                               foreach ($addon_summary as $destination) {
                                    $total_grand+=$destination["subtotal"];
                                    echo "
                                    <tr>
                                        <td>{$destination['description']}</td>
                                        <td>{$destination['computation']}</td>
                                        <td>{$destination['subtotal']} SGD</td>
                                    </tr>";
                                }
                            }
                            #end of addon summary --------------------------------------
                            //dump($extension_summary);
                            #extension summary -----------------------------------------
                            if(count($extension_summary)!=0){
                                echo "
                                <tr>
                                    <th colspan='3' style='border-top:0px;'>
                                        <br>
                                        Extensions
                                    </th>
                                </tr>";

                                $remark = "";
                                foreach ($extension_summary as $destination) {
                                    $total_grand+=$destination["subtotal"];
                                    $remark = $destination["remark"];
                                    echo "
                                    <tr>
                                        <td>{$destination['description']}</td>
                                        <td>{$destination['computation']}</td>
                                        <td>{$destination['subtotal']} SGD</td>
                                    </tr>";
                                }

                                echo "
                                <tr>
                                    <td><strong>Remarks</strong></td>
                                    <td colspan=2>{$remark}</td>
                                </tr>";
            
                            }
                            #end of extension summary ----------------------------------

                        ?>
                        <!-- end of summary -->

                        <!-- totals -->
                        <tfoot>
                            <?php if (isset($user) && get_class($user) <> 'Provider'): ?>
                                <tr>
                                    <td colspan="3" style="border-top:0px;"><br></td>
                                </tr>
                                <tr>
                                    <?php
                                    if($additional > 0)
                                    {
                                        echo '<th colspan="2" >Additional Charge</th>';
                                        echo '<th>'.$additional.' SGD</th>';
                                    }

                                    ?>
                                </tr>
                                <tr>
                                <?php
                                if($discount > 0)
                                {
                                    echo '<th colspan="2" >Discount</th>';
                                    echo '<th>'.$discount.' SGD</th>';
                                }

                                ?>
                                </tr>
                                <tr>
                                    <th colspan="2">TOTAL</th>
                                    <th><?php echo @$total_grand+$additional-$discount.' SGD';?></th>
                                </tr>
                            <?php endif; ?>
                        </tfoot>
                        <!-- totals-->
                    </table>
                </div>
            </div>

            <!-- Agent/Provider -->
            <?php if (isset($user) && get_class($user) <> 'Customer') { ?>
                <div class="panel panel-orange">
                    <div class="panel-heading">
                        <h3 class="panel-title">Agent Details</h3>
                    </div>
                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th width="200px">Company Name</th>
                                <td> <?php echo @$data_summary[0]->AGENTCOMPANYNAME;?></td>
                            </tr>
                            <tr>
                                <th>Staff Name</th>
                                <td> <?php
                                    if(@$data_summary[0]->agentdetailsname != NULL)
                                        echo @$data_summary[0]->agentdetailsname;
                                    else
                                        echo @$data_summary[0]->AGENTLNAME.', '.@$data_summary[0]->AGENTFNAME;
                                ?></td>
                            </tr>
                            <tr>
                                <th>Email Address</th>
                                <td> <?php echo @$data_summary[0]->AGENTEMAIL;?></td>
                            </tr>
                            <tr>
                                <th>Contact Number</th>
                                <td> <?php echo @$data_summary[0]->AGENTPHONE;?></td>
                            </tr>
                            <tr>
                                <th>Reference Number</th>
                                <td><?php
                                    if(@$data_summary[0]->agentdetailsref != NULL)
                                        echo @$data_summary[0]->agentdetailsref;
                                    else
                                        echo '0';
                                 ?></td>
                            </tr>
                        </table>
                    </div>
                </div>

                <?php if (get_class($user) <> 'Agent'){ ?>
                    <div class="panel panel-orange">
                        <div class="panel-heading">
                            <h3 class="panel-title">Provider Details</h3>
                        </div>
                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <th width="200px">Company Name</th>
                                    <td> <?php echo @$provider_summary[0]->COMNAME;?></td>
                                </tr>
                                <tr>
                                    <th>Staff Name</th>
                                    <td> <?php echo @$provider_summary[0]->providerlname.', '.@$provider_summary[0]->providerfname;?></td>
                                </tr>
                                <tr>
                                    <th>Email Address</th>
                                    <td> <?php echo @$provider_summary[0]->provideremail;?></td>
                                </tr>
                                <tr>
                                    <th>Contact Number</th>
                                    <td> <?php echo @$provider_summary[0]->providerphone;?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>
            <!-- /.table-responsive -->
        </div>

        <!-- myModalResent -->
        <div class="modal fade" id="myModalResent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class=" pull-right btn btn-danger btn-sm" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Resend Email To:</h4>
                    </div>
                    <form id="append_package_form" class="form-horizontal" role="form" method="post" action="<?php echo base_url('panel/resend/'.$booked_id); ?>">
                        <div class="modal-body">
                            <label style="display: block;"> <input type="checkbox" id="chkProvider" name="Provider" value="Provider"> Provider </label>
                            <label style="display: block;"> <input type="checkbox" id="chkAgent" name="Agent" value="Agent"> Agent </label>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-orange" id="btnChkResend" disabled="disabled">Send</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- myModalDiscount -->
        <div class="modal fade" id="myModalDiscount" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class=" pull-right btn btn-danger btn-sm" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Discount:</h4>
                    </div>
                    <form id="append_package_form" class="form-horizontal" role="form" method="post" action="<?php echo base_url('panel/getDiscount/'.$booked_id); ?>">
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">Amount</label>
                                <div class="col-xs-8">
                                    <input type="number" step="any" class="form-control" name="discount" placeholder="Default" onkeypress="return isNumberKey(event)"  value="<?php echo @$data_summary[0]->TOTALDISCOUNT; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer"><!--
                            <button type="close" class="btn btn-default" data-dismiss="modal">Close</button> -->
                            <button type="submit" class="btn btn-orange">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- myModalAdditionalC -->
        <div class="modal fade" id="myModalAdditionalC" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class=" pull-right btn btn-danger btn-sm" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Additional Charge:</h4>
                    </div>
                    <form id="append_package_form" class="form-horizontal" role="form" method="post" action="<?php echo base_url('panel/getAdditional/'.$booked_id); ?>">
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">Amount</label>
                                <div class="col-xs-8">
                                    <input type="number" step="any" class="form-control" name="additional" placeholder="Default" onkeypress="return isNumberKey(event)"  value="<?php echo @$data_summary[0]->TOTALADDITIONAL; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer"><!--
                            <button type="close" class="btn btn-default" data-dismiss="modal">Close</button> -->
                            <button type="submit" class="btn btn-orange">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- myModalPassengers -->
        <div class="modal fade" id="myModalPassengers" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class=" pull-right btn btn-danger btn-sm" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Passengers</h4>
                    </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <table class="table">
                                    <thead>
                                        <tr>
                                        <td><strong>Name</strong></td>
                                        <td><strong>Date of Birth</strong></td>
                                        </tr>
                                    </thead>
                                    <tbody>                                      
                                        <?php foreach($passengers as $p){?>
                                            <tr>
                                            <td><?php echo $p->name?></td>
                                            <td><?php echo date_format(date_create($p->date_of_birth),'d-m-Y')?></td>
                                            </tr>
                                        <?php } ?>          
                                    </tbody>
                                </table>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
