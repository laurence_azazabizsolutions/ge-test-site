<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Global Explorer</title>
 		<style type="text/css">
			h1,
			h4{
				font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
				font-weight: 500;
				line-height: 1.1;
				color: inherit;
			}
		</style>
	</head>
	<body style="color: #333333;">
		<table cellspacing="0" cellpadding="0" style="background-color: #ffffff; border: 1px solid transparent;  border-radius: 4px; -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05); box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05); border-color: #e95a3e;">
			<tr>
				<td style="padding: 10px 15px; border-bottom: 1px solid transparent; border-top-right-radius: 3px; border-top-left-radius: 3px; color: #fff; background-color: #e95a3e; border-color: #e95a3e;">
					<h1 style="margin-top: 0; margin-bottom: 0; font-size: 16px; color: inherit; text-align: center; ">
						Global Explorer
					</h1> 
				</td>
			</tr>
			<tr>
				
			</tr>
			<tr>
				<td style="padding: 15px;">
                	<strong>No. of Bookings for <i><?php echo  date("M-Y", strtotime("-1 month")). "</i> "?></strong>
                    <table style="width: 770px; margin-bottom: 20px; border-collapse: collapse;">
                    	<thead>
	                        <tr>
	                            <td style="padding: 8px; vertical-align: bottom; border: 1px solid #dddddd;"><strong>Booking Code</strong></td>
	                            <?php if(@$provider != 'provider'){?>
	                            <td style="padding: 8px; vertical-align: bottom; border: 1px solid #dddddd;"><strong>Price</strong></td>
	                            <?php }?>
	                            <td style="padding: 8px; vertical-align: bottom; border: 1px solid #dddddd;"><strong>Package(s)</strong></td>
	                            <td style="padding: 8px; vertical-align: bottom; border: 1px solid #dddddd;"><strong>Customer Name(s)</strong></td>
	                            <td style="padding: 8px; vertical-align: bottom; border: 1px solid #dddddd;"><strong>Invoice(s)</strong></td>
	                        </tr>
                    	</thead>
                    	<tbody>
                    		<?php
                    			// if($user_data != NULL || $user_data != ""){
                       //              foreach($user_data as $row){
                                    	//@$total_all_price += $row['total_price'];
                                    	//if(@$row['email'] == @$mail_checker){
                           // ?>
	                            			<tr>
					                            <td style="padding: 8px; vertical-align: top; border: 1px solid #dddddd;"><?php echo $code; ?></td>
					                            <?php if(@$provider != 'provider'){?>
					                            <td style="padding: 8px; vertical-align: top; border: 1px solid #dddddd;"><?php echo $total; ?></td>
					                            <?php }?>
					                            <td style="padding: 8px; vertical-align: top; border: 1px solid #dddddd;"><?php echo $title; ?></td>
					                            <td style="padding: 8px; vertical-align: top; border: 1px solid #dddddd;"><?php
					       //                      $cnt=count($cust);
												// for ($i=0; $i < $cnt; $i++) { 
												// 	for ($ii=0; $ii < count($cust[$i]); $ii++) {
												// 		if($cust[$i][$ii]->id == $row['id']){
												// 			if($ii < 1){
												// 				$pax_names = $cust[$i][$ii]->name;
												// 			}
												// 		$total_pax[] = $cust[$i][$ii]->name.'<br>';
												// 		$pax_name= $cust[$i][$ii]->name;
												// 		}
												// 	}
												// }
					                            echo $PAXNAME;
					                            //echo $pax_names;
					                            //echo ' ('.$numberOfpax.' pax)';
					                            echo ' ('.$count_pax.' pax)';
			                                    $key = '~!@#$%^&*()_+ hacker is not allowed this area!';
			                                    $id = base_convert($id, 35, 36); 
			                                    $data = mcrypt_encrypt(MCRYPT_BLOWFISH,$key, $id, 'ecb');
			                                    $data = bin2hex($data);
					                             ?></td>
					                            <td style="padding: 8px; vertical-align: top; border: 1px solid #dddddd;"><a href="<?php echo base_url('/home/pdf/'.$data);?>">Invoice</a></td>
					                        </tr>
<!--                             <?php
                                    	//}
                                //     }
                                // }
                             ?> -->
                    	</tbody>
                    	<tfoot>
                    		<tr>
                    			<td colspan="1" style="padding: 9px; vertical-align: top; border: 1px solid #dddddd;">
                    				<?php echo 'Bookings: '.$user_data;?>
                    			</td>
                    			<td colspan="1" style="padding: 8px; vertical-align: top; border: 1px solid #dddddd;">
                    				<?php echo number_format(@$total, 2, '.', ',');?>
                    			</td>
                    			<td colspan="3" style="padding: 8px; vertical-align: top; border: 1px solid #dddddd;">
                    			</td>
                    		</tr>
                    	</tfoot>
                    </table>
				</td>
			</tr>
		</table>
	</body>
</html>