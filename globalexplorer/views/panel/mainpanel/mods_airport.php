<div id="page-wrapper">
    <!-- /.row -->
    <div class="row">
        <br>
        <div class="col-lg-12">
            <!-- Nav tabs -->
            <ul class="nav nav-pills" role="tablist" id="mods_airport">
                <li><a href="#newairportcode" role="tab" data-toggle="tab">New</a></li>
                <li class="active"><a href="#airportcodes" role="tab" data-toggle="tab">Airport Codes</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane" id="newairportcode">
                    <br>
                    <form id="create_airportcode" class="form-horizontal" role="form" method="post" action="<?php echo base_url('panel/create_airportcode')?>">
                        <div class="alert" style="display: none;"></div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Airport/City</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" placeholder="Chiang Mai" name="airportcode">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Code</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" placeholder="CNX" name="code" maxlength="3">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-8 col-sm-offset-3">
                                <button type="submit" class="btn btn-orange">Add</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="tab-pane active" id="airportcodes">
                    <br>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Airport Code</th>
                                    <th>Code</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php if (isset($airportcodes)): ?>
                                <?php foreach ($airportcodes as $airportcode): ?>
                                    <?php $this->view('panel/mainpanel/append_airportcode', array('airportcode' => $airportcode));?>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- /#page-wrapper -->