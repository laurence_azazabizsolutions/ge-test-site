<?php
  include_once ('header.php');
?>

    <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">

      <!-- image -->
      <div class="carousel-inner">
        <div class="item active grad">
          <div class="carousel-fill" style="background-image:url('img/4.jpg');"></div>
        </div>
      </div>

      <!-- caption -->
      <div class="container carousel-select">
        <div class="carousel-caption">
          <h1>Changmai</h1>
          <p>Come! Indulge! Explore! Experience!</p>
          <br><br><br><br><br><br><br><br><br>
        </div>
      </div>
    </div><!-- /.carousel -->
    
    <!-- Content 
    ================================================== -->
    <div class="container-fluid container-fluid-overlay">

      <!-- destination -->
      <div class="container-fluid">
        <div class="container pax-dest">
          <div class="row">
            <div class="col-xs-3">
              <a href="details.php">
                <div class="pax-img">
                  <div class="pax-fill" style="background-image:url('img/feat/11.jpg');"></div>
                </div>
                <p class="pax-details">5 DAYS 4 NIGHTS CHIANGMAI CHIANGRAI (DAY TOUR CEI) (Valid to 31 March 2015)</p>
              </a>
            </div>
            <div class="col-xs-3">
              <a href="details.php">
                <div class="pax-img">
                  <div class="pax-fill" style="background-image:url('img/feat/22.jpg');"></div>
                </div>
                <p class="pax-details">3 DAY 2 NIGHTS CHIANGMAI TOUR (VALID TO 31 March 2015)Minimum 2 Paxs</p>
              </a>
            </div>
            <div class="col-xs-3">
              <a href="details.php">
                <div class="pax-img">
                  <div class="pax-fill" style="background-image:url('img/feat/33.jpg');"></div>
                </div>
                <p class="pax-details">4 DAYS 3 NIGHTS CHIANGMAI ELEPHANT CAMP + MIX HILLTRIBE (Valid to 31 March 2015)</p>
              </a>
            </div>
            <div class="col-xs-3">
              <a href="details.php">
                <div class="pax-img">
                  <div class="pax-fill" style="background-image:url('img/feat/44.jpg');"></div>
                </div>
                <p class="pax-details">3 DAY 2 NIGHTS CHIANGMAI CHIANGRAI TOUR (VALID TO 31 March 2015)</p>
              </a>
            </div>
            <div class="col-xs-3">
              <a href="details.php">
                <div class="pax-img">
                  <div class="pax-fill" style="background-image:url('img/feat/11.jpg');"></div>
                </div>
                <p class="pax-details">5 DAYS 4 NIGHTS CHIANGMAI CHIANGRAI (DAY TOUR CEI) (Valid to 31 March 2015)</p>
              </a>
            </div>
            <div class="col-xs-3">
              <a href="details.php">
                <div class="pax-img">
                  <div class="pax-fill" style="background-image:url('img/feat/22.jpg');"></div>
                </div>
                <p class="pax-details">3 DAY 2 NIGHTS CHIANGMAI TOUR (VALID TO 31 March 2015)Minimum 2 Paxs</p>
              </a>
            </div>
            <div class="col-xs-3">
              <a href="details.php">
                <div class="pax-img">
                  <div class="pax-fill" style="background-image:url('img/feat/33.jpg');"></div>
                </div>
                <p class="pax-details">4 DAYS 3 NIGHTS CHIANGMAI ELEPHANT CAMP + MIX HILLTRIBE (Valid to 31 March 2015)</p>
              </a>
            </div>
            <div class="col-xs-3">
              <a href="details.php">
                <div class="pax-img">
                  <div class="pax-fill" style="background-image:url('img/feat/44.jpg');"></div>
                </div>
                <p class="pax-details">3 DAY 2 NIGHTS CHIANGMAI CHIANGRAI TOUR (VALID TO 31 March 2015)</p>
              </a>
            </div>
          </div>
          <div class="row text-center">
            <ul class="pagination pagination-sm">
              <li><a href="#">&laquo;</a></li>
              <li class="disabled"><a href="#">1</a></li>
              <li class="active"><a href="#">2</a></li>
              <li><a href="#">3</a></li>
              <li><a href="#">4</a></li>
              <li><a href="#">5</a></li>
              <li><a href="#">&raquo;</a></li>
            </ul>
          </div>
        </div>
      </div>
      <!-- /destination -->

<?php
  include_once ('footer.php');
?>