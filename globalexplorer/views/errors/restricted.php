<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>iPrestege</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('css/bootstrap.min.css" rel="stylesheet'); ?>">
    <link href="<?php echo base_url('css/bootstrap-datetimepicker.min.css" rel="stylesheet'); ?>">
    <link href="<?php echo base_url('css/bootstrapValidator.css" rel="stylesheet'); ?>">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url('css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="<?php echo base_url('../../assets/js/ie-emulation-modes-warning.js'); ?>"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="<?php echo base_url('../../assets/js/ie10-viewport-bug-workaround.js'); ?>"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url('css/carousel.css'); ?>" rel="stylesheet">
  </head>

  <body style="background:#eef1f3;">
    <div class="container">
      <div class="row">
        <div class="featurette col-md-offset-3">
          <img class="featurette-image pull-left img-responsive" src="<?php echo base_url('img/stop.png'); ?>" width="118px" style="margin:0 20px 20px 0;">
          <h1 class="featurette-heading">STOP! <span class="text-muted">No entry.</span></h1>
          <p class="lead">
            <h3>Access denied. Sorry.</h3>
            Looks like you do not have the sufficient permission to access this page.<br><br>
            <small><em>Return to the <strong><a href="<?php echo base_url('home'); ?>">homepage</a></strong>, please.</em></small>
          </p>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
  </body>
</html>