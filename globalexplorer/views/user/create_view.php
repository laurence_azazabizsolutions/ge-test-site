<?php
  include_once ('header.php');
?>

            <div id="page-wrapper">
                <!-- /.row -->
                <div class="row">
                    <br>
                    <div class="col-md-8">
                        <h3>5 DAYS 4 NIGHTS CHIANGMAI CHIANGRAI (DAY TOUR CEI) (Valid to 31 March 2015)</h3>
                          <strong>Package includes:</strong><br><br>
                          Day 01: Arrival, Khantoke Dinner<br>
                          Day 02: Doi Suthep Temple, Home industry<br>
                          Day 03: CNX-CEI-CNX, Hot spring, White Temple, Golden Triangle, Maesai<br>
                          Day 04: Elephant at work, Human and snake fighting, Monkey show, Orchid and butterfly Farm, Chiangmai Orientation<br>
                          Day 05: Departure
                    </div>
                    <div class="col-md-4">
                        <img class="thumbnail img-responsive" src="../img/feat/11.jpg" alt="...">
                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Adult</label>
                                <div class="col-sm-8">
                                    <select class="form-control">
                                        <option>Above 12 years</option>
                                        <option>0</option>
                                        <option selected>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                        <option>6</option>
                                        <option>7</option>
                                        <option>8</option>
                                        <option>9</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Children</label>
                                <div class="col-sm-8">
                                    <select class="form-control">
                                        <option selected>Below 12 years</option>
                                        <option>0</option>
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                        <option>6</option>
                                        <option>7</option>
                                        <option>8</option>
                                        <option>9</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Total</label>
                                <div class="col-sm-8">
                                    <input class="form-control" type="text" placeholder="Total" disabled="disabled"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Departure</label>
                                <div class="col-sm-8">
                                    <div class="input-group date" id="datetimepickerDepart">
                                      <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                      <input class="form-control" type="text" placeholder="Departure" disabled="disabled"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Return</label>
                                <div class="col-sm-8">
                                    <div class="input-group date" id="datetimepickerReturn">
                                      <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                      <input class="form-control" type="calendar" placeholder="Return" disabled="disabled"/>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-8 col-sm-offset-4">
                                    <a href="create_bookhotel.php" class="btn btn-orange">Book</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /#page-wrapper -->

<?php
  include_once ('footer.php');
?>