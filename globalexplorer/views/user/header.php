<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">

        <title>Global Explorer</title>

        <!-- Bootstrap Core CSS -->
        <link href="<?php echo base_url('css/bootstrap.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('css/bootstrap-datetimepicker.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('css/bootstrapValidator.css'); ?>" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="<?php echo base_url('css/sb-admin-2.css'); ?>" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="<?php echo base_url('css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link href="<?php echo base_url('css/carousel.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('css/dashboard.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('css/typeahead_custom.css'); ?>" rel="stylesheet">     
    </head>

    <body>

        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">Global Explorer</a>
                </div>
                <!-- /.navbar-header -->

                <ul class="nav navbar-top-links navbar-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-envelope fa-fw"></i>  <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-nav dropdown-messages">
                            <li>
                                <a href="#">
                                    <div>
                                        <strong>John Smith</strong>
                                        <span class="pull-right text-muted">
                                            <em>Yesterday</em>
                                        </span>
                                    </div>
                                    <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <strong>John Smith</strong>
                                        <span class="pull-right text-muted">
                                            <em>Yesterday</em>
                                        </span>
                                    </div>
                                    <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <strong>John Smith</strong>
                                        <span class="pull-right text-muted">
                                            <em>Yesterday</em>
                                        </span>
                                    </div>
                                    <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a class="text-center" href="#">
                                    <strong>Read All Messages</strong>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </li>
                        </ul>
                        <!-- /.dropdown-messages -->
                    </li>
                    <!-- /.dropdown -->
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-tasks fa-fw"></i>  <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-nav dropdown-tasks">
                            <li>
                                <a href="#">
                                    <div>
                                        <p>
                                            <strong>Task 1</strong>
                                            <span class="pull-right text-muted">40% Complete</span>
                                        </p>
                                        <div class="progress progress-striped active">
                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                                <span class="sr-only">40% Complete (success)</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <p>
                                            <strong>Task 2</strong>
                                            <span class="pull-right text-muted">20% Complete</span>
                                        </p>
                                        <div class="progress progress-striped active">
                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                                <span class="sr-only">20% Complete</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <p>
                                            <strong>Task 3</strong>
                                            <span class="pull-right text-muted">60% Complete</span>
                                        </p>
                                        <div class="progress progress-striped active">
                                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                                <span class="sr-only">60% Complete (warning)</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <p>
                                            <strong>Task 4</strong>
                                            <span class="pull-right text-muted">80% Complete</span>
                                        </p>
                                        <div class="progress progress-striped active">
                                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                                <span class="sr-only">80% Complete (danger)</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a class="text-center" href="#">
                                    <strong>See All Tasks</strong>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </li>
                        </ul>
                        <!-- /.dropdown-tasks -->
                    </li>
                    <!-- /.dropdown -->
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-bell fa-fw"></i>  <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-nav dropdown-alerts">
                            <li>
                                <a href="#">
                                    <div>
                                        <i class="fa fa-comment fa-fw"></i> New Comment
                                        <span class="pull-right text-muted small">4 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                        <span class="pull-right text-muted small">12 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <i class="fa fa-envelope fa-fw"></i> Message Sent
                                        <span class="pull-right text-muted small">4 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <i class="fa fa-tasks fa-fw"></i> New Task
                                        <span class="pull-right text-muted small">4 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                        <span class="pull-right text-muted small">4 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a class="text-center" href="#">
                                    <strong>See All Alerts</strong>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </li>
                        </ul>
                        <!-- /.dropdown-alerts -->
                    </li>
                    <!-- /.dropdown -->
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-nav dropdown-user">
                            <li><a href="#" data-toggle="modal" data-target="#myModalProfile"><i class="fa fa-user fa-fw"></i> User Profile</a>
                            </li>
                            <li><a href="#" data-toggle="modal" data-target="#myModalSetting"><i class="fa fa-gear fa-fw"></i> Settings</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="../"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                            </li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                    <!-- /.dropdown -->
                </ul>
                <!-- /.navbar-top-links -->

                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                            <li>Admin Side</li>
                            <li class="active">
                                <a href="#"><i class="fa fa-book fa-fw"></i> Booking<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="pending.php">Pending</a>
                                    </li>
                                    <li>
                                        <a href="index.php">All</a>
                                    </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-plane fa-fw"></i> Packages<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="package_new.php">New</a>
                                    </li>
                                    <li>
                                        <a href="package.php">All</a>
                                    </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                            <li>
                                <a href="hotel2.php"><i class="fa fa-building fa-fw"></i> Hotel</a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-user fa-fw"></i> Users<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <!-- <a href="users.php">New</a> -->
                                        <a href="<?php echo base_url('admin/new_users')?>">New</a>
                                    </li>
                                    <li>
                                        <a href="#">Agents</a>
                                    </li>
                                    <li>
                                        <a href="#">Provider</a>
                                    </li>
                                    <li>
                                        <a href="#">Admin</a>
                                    </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-money fa-fw"></i> Payments<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="payment_agent.php">Agent</a>
                                    </li>
                                    <li>
                                        <a href="payment_provider.php">Provider</a>
                                    </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                            <li>
                                <a href="report.php"><i class="fa fa-folder-open fa-fw"></i> Report</a>
                            </li>
                            <li>Provider Side</li>
                            <li class="active">
                                <a href="index.php"><i class="fa fa-book fa-fw"></i> Booking</a>
                            </li>
                            <li>
                                <a href="payment_provider_2.php"><i class="fa fa-money fa-fw"></i> Pending Payments</a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-envelope fa-fw"></i> Contact</a>
                            </li>
                            <li>Agent Side</li>
                            <li class="active">
                                <a href="#"><i class="fa fa-book fa-fw"></i> Booking<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    <li>
                                        <a href="create.php">Create Bookings</a>
                                    </li>
                                    <li>
                                        <a href="index.php">List of Bookings</a>
                                    </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-envelope fa-fw"></i> Contact</a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.sidebar-collapse -->
                </div>
                <!-- /.navbar-static-side -->
            </nav>
            <!-- /Navigation -->

            <!-- myModalProfile -->
            <div class="modal fade" id="myModalProfile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class=" pull-right btn btn-danger btn-sm" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Profile:</h4>
                  </div>
                  <div class="modal-body">
                    <form class="form-horizontal" role="form">
                      <div class="form-group">
                        <label class="col-xs-4 control-label">Contact Name</label>
                        <div class="col-xs-8">
                          <input type="text" class="form-control" placeholder="John Doe">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-xs-4 control-label">Company Name</label>
                        <div class="col-xs-8">
                          <input type="text" class="form-control" placeholder="Company XYZ">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-xs-4 control-label">Phone</label>
                        <div class="col-xs-8">
                          <input type="text" class="form-control" placeholder="0123456">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-xs-4 control-label">Email</label>
                        <div class="col-xs-8">
                          <input type="email" class="form-control" placeholder="johndoe@domail.com">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-xs-4 control-label">Country</label>
                        <div class="col-xs-8">
                          <select class="form-control">
                            <option value="#">Select</option>
                            <option value="China">China</option>
                            <option value="Shanghai">Shanghai</option>
                            <option value="Beijing">Beijing</option>
                            <option value="Thailand">Thailand</option>
                            <option value="Chiangmai">Chiangmai</option>
                            <option value="Bangkok">Bangkok</option>
                            <option value="Khabi">Khabi</option>
                            <option value="Taiwan">Taiwan</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-xs-4 control-label">Username</label>
                        <div class="col-xs-8">
                          <input type="text" class="form-control" placeholder="john_doe">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-xs-4 control-label">Password</label>
                        <div class="col-xs-8">
                          <input type="password" class="form-control" placeholder="*******">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-xs-4 control-label">Repeat Password</label>
                        <div class="col-xs-8">
                          <input type="password" class="form-control" placeholder="*******">
                        </div>
                      </div>
                    </form>
                  </div>
                  <div class="modal-footer">
                    <a class="btn btn-orange" href="#" data-dismiss="modal" role="button">Update</a>
                  </div>
                </div>
              </div>
            </div>

            <!-- myModalSetting -->
            <div class="modal fade" id="myModalSetting" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-sm">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="pull-right btn btn-danger btn-sm" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Setting:</h4>
                  </div>
                  <div class="modal-body">
                    <form class="form-horizontal" role="form">
                      <div class="form-group">
                        <label class="col-xs-4 control-label">Promotional Message</label>
                        <div class="col-xs-8">
                          <textarea class="form-control" style="resize:none;" rows="3"></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-xs-4 control-label">Provider Message</label>
                        <div class="col-xs-8">
                          <textarea class="form-control" style="resize:none;" rows="3"></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-xs-4 control-label">Agent Message</label>
                        <div class="col-xs-8">
                          <textarea class="form-control" style="resize:none;" rows="3"></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-xs-4 control-label">Contact Form Emails</label>
                        <div class="col-xs-8">
                          <input type="text" class="form-control" placeholder="Email">
                        </div>
                      </div>
                    </form>
                  </div>
                  <div class="modal-footer">
                    <a class="btn btn-orange" href="#" data-dismiss="modal" role="button">Update</a>
                  </div>
                </div>
              </div>
            </div>