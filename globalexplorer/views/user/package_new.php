<?php
  include_once ('header.php');
?>

            <div id="page-wrapper">
                <!-- /.row -->
                <div class="row">
                    <br>
                    <div class="col-lg-12">
                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Code</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Number of Nights</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Destination</label>
                                <div class="col-sm-8">
                                    <select class="form-control">
                                        <option>-</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Provider</label>
                                <div class="col-sm-8">
                                    <select class="form-control">
                                        <option>-</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Picture</label>
                                <div class="col-sm-8">
                                    <input type="file">
                                    <p class="help-block">Only JPEG and PDF &lt; 500kb are allowed.</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Title</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Description</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" style="resize:none;" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Validity Date</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Terms and Conditions</label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" style="resize:none;" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-8 col-sm-offset-3">
                                    <button type="reset" class="btn btn-default">Save Packages</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /#page-wrapper -->

<?php
  include_once ('footer.php');
?>