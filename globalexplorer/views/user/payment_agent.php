<?php
  include_once ('header.php');
?>

            <div id="page-wrapper">
                <!-- /.row -->
                <div class="row">
                    <br>
                    <div class="col-lg-12">

                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Agent</label>
                                <div class="col-sm-8">
                                    <select class="form-control">
                                        <option>Agent Name List</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Provider</label>
                                <div class="col-sm-8">
                                    <select class="form-control">
                                        <option>Provider Name List</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Booking Status</label>
                                <div class="col-sm-8">
                                    <select class="form-control">
                                        <option>-</option>
                                        <option>Pending</option>
                                        <option selected>Confirmed</option>
                                        <option>Rejected</option>
                                        <option>Amended</option>
                                        <option>Cancelled</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Date</label>
                                <div class="col-sm-4">
                                    <select class="form-control">
                                        <option>Current Month</option>
                                    </select>
                                    <br>
                                </div>
                                <div class="col-sm-4">
                                    <select class="form-control">
                                        <option>Current Year</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-8 col-sm-offset-3">
                                    <button type="reset" class="btn btn-orange">Filter</button>
                                    <button type="reset" class="btn btn-default">Clear</button>
                                </div>
                            </div>
                        </form>

                        <!-- Nav tabs -->
                        <ul class="nav nav-pills" role="tablist">
                            <li class="active"><a href="#pendingpayments" role="tab" data-toggle="tab">Pending Payments</a></li>
                            <li><a href="#addcollection" role="tab" data-toggle="tab">Add Collection</a></li>
                            <li><a href="#viewcollection" role="tab" data-toggle="tab">View Collection</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane active" id="pendingpayments">
                                <h2>December 2011</h2>
                                <br>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Booking Code</th>
                                                <th>Details</th>
                                                <th>Flight</th>
                                                <th>Amount</th>
                                                <th>Booking Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="danger">
                                                <td>
                                                    <a href="voucher.php">GE01949</a> <br>
                                                    <a href="view.php">View</a>
                                                </td>
                                                <td>
                                                    <strong>Package:</strong> BKK001 <br>
                                                    <strong>Agent:</strong> INDEPENDENT TRAVELLER PTE LTD <br>
                                                    <strong>Provider:</strong> Jetview Travel International Co Ltd <br>
                                                    <strong>Hotel:</strong> Centre Point Soi 15 (Studio) <br>
                                                    <strong>Traveller:</strong> NG HWI MEOW AGNES MS , TAN CHIA HWI SARAH NICOLE MS (2 Pax)
                                                </td>
                                                <td>
                                                    <strong>Ori:</strong> 05-Dec-2011 <br>
                                                    14:25:00 hrs - 13:45:00 hrs <br>
                                                    <strong>Ret:</strong> 07-Dec-2011 <br>
                                                    16:35:00 hrs - 19:55:00 hr
                                                </td>
                                                <td>0</td>
                                                <td>Cancelled</td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <a href="voucher.php">GE01949</a> <br>
                                                    <a href="view.php">View</a>
                                                </td>
                                                <td>
                                                    <strong>Package:</strong> BKK001 <br>
                                                    <strong>Agent:</strong> INDEPENDENT TRAVELLER PTE LTD <br>
                                                    <strong>Provider:</strong> Jetview Travel International Co Ltd <br>
                                                    <strong>Hotel:</strong> Centre Point Soi 15 (Studio) <br>
                                                    <strong>Traveller:</strong> NG HWI MEOW AGNES MS , TAN CHIA HWI SARAH NICOLE MS (2 Pax)
                                                </td>
                                                <td>
                                                    <strong>Ori:</strong> 05-Dec-2011 <br>
                                                    14:25:00 hrs - 13:45:00 hrs <br>
                                                    <strong>Ret:</strong> 07-Dec-2011 <br>
                                                    16:35:00 hrs - 19:55:00 hr
                                                </td>
                                                <td>0</td>
                                                <td>Confirmed</td>
                                                <td>
                                                    <button type="reset" class="btn btn-default">Collect</button>
                                                </td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th></th>
                                                <th>Total: 2 Booking(s)</th>
                                                <th></th>
                                                <th>0</th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane" id="addcollection">
                                <h2>December 2011</h2>
                                <br>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Booking Code</th>
                                                <th>Details</th>
                                                <th>Flight</th>
                                                <th>Amount</th>
                                                <th>Booking Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="danger">
                                                <td>
                                                    <a href="voucher.php">GE01949</a> <br>
                                                    <a href="view.php">View</a>
                                                </td>
                                                <td>
                                                    <strong>Package:</strong> BKK001 <br>
                                                    <strong>Agent:</strong> INDEPENDENT TRAVELLER PTE LTD <br>
                                                    <strong>Provider:</strong> Jetview Travel International Co Ltd <br>
                                                    <strong>Hotel:</strong> Centre Point Soi 15 (Studio) <br>
                                                    <strong>Traveller:</strong> NG HWI MEOW AGNES MS , TAN CHIA HWI SARAH NICOLE MS (2 Pax)
                                                </td>
                                                <td>
                                                    <strong>Ori:</strong> 05-Dec-2011 <br>
                                                    14:25:00 hrs - 13:45:00 hrs <br>
                                                    <strong>Ret:</strong> 07-Dec-2011 <br>
                                                    16:35:00 hrs - 19:55:00 hr
                                                </td>
                                                <td>0</td>
                                                <td>Cancelled</td>
                                                <td>
                                                    <input type="checkbox">
                                                </td>
                                            </tr>
                                            <tr class="warning">
                                                <td>
                                                    <a href="voucher.php">GE01949</a> <br>
                                                    <a href="view.php">View</a>
                                                </td>
                                                <td>
                                                    <strong>Package:</strong> BKK001 <br>
                                                    <strong>Agent:</strong> INDEPENDENT TRAVELLER PTE LTD <br>
                                                    <strong>Provider:</strong> Jetview Travel International Co Ltd <br>
                                                    <strong>Hotel:</strong> Centre Point Soi 15 (Studio) <br>
                                                    <strong>Traveller:</strong> NG HWI MEOW AGNES MS , TAN CHIA HWI SARAH NICOLE MS (2 Pax)
                                                </td>
                                                <td>
                                                    <strong>Ori:</strong> 05-Dec-2011 <br>
                                                    14:25:00 hrs - 13:45:00 hrs <br>
                                                    <strong>Ret:</strong> 07-Dec-2011 <br>
                                                    16:35:00 hrs - 19:55:00 hr
                                                </td>
                                                <td>0</td>
                                                <td>Rejected</td>
                                                <td>
                                                    <input type="checkbox">
                                                </td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th></th>
                                                <th>Total: 2 Booking(s)</th>
                                                <th></th>
                                                <th>0</th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <button type="reset" class="btn btn-orange">Submit</button>
                            </div>
                            <div class="tab-pane" id="viewcollection">
                                <h2>Cheque number: uob 976843</h2>
                                <button type="reset" class="btn btn-default">Revoke</button>
                                <br><br>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Booking Code</th>
                                                <th>Details</th>
                                                <th>Flight</th>
                                                <th>Amount</th>
                                                <th>Date Collected</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <a href="voucher.php">GE01949</a> <br>
                                                    <a href="view.php">View</a>
                                                </td>
                                                <td>
                                                    <strong>Package:</strong> BKK001 <br>
                                                    <strong>Agent:</strong> INDEPENDENT TRAVELLER PTE LTD <br>
                                                    <strong>Provider:</strong> Jetview Travel International Co Ltd <br>
                                                    <strong>Hotel:</strong> Centre Point Soi 15 (Studio) <br>
                                                    <strong>Traveller:</strong> NG HWI MEOW AGNES MS , TAN CHIA HWI SARAH NICOLE MS (2 Pax)
                                                </td>
                                                <td>
                                                    <strong>Ori:</strong> 05-Dec-2011 <br>
                                                    14:25:00 hrs - 13:45:00 hrs <br>
                                                    <strong>Ret:</strong> 07-Dec-2011 <br>
                                                    16:35:00 hrs - 19:55:00 hr
                                                </td>
                                                <td>0</td>
                                                <td>2013-03-26</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <a href="voucher.php">GE01949</a> <br>
                                                    <a href="view.php">View</a>
                                                </td>
                                                <td>
                                                    <strong>Package:</strong> BKK001 <br>
                                                    <strong>Agent:</strong> INDEPENDENT TRAVELLER PTE LTD <br>
                                                    <strong>Provider:</strong> Jetview Travel International Co Ltd <br>
                                                    <strong>Hotel:</strong> Centre Point Soi 15 (Studio) <br>
                                                    <strong>Traveller:</strong> NG HWI MEOW AGNES MS , TAN CHIA HWI SARAH NICOLE MS (2 Pax)
                                                </td>
                                                <td>
                                                    <strong>Ori:</strong> 05-Dec-2011 <br>
                                                    14:25:00 hrs - 13:45:00 hrs <br>
                                                    <strong>Ret:</strong> 07-Dec-2011 <br>
                                                    16:35:00 hrs - 19:55:00 hr
                                                </td>
                                                <td>0</td>
                                                <td>2013-03-26</td>
                                            </tr>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th></th>
                                                <th>Total: 2 Booking(s)</th>
                                                <th></th>
                                                <th>0</th>
                                                <th></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /#page-wrapper -->

<?php
  include_once ('footer.php');
?>