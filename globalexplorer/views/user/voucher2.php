<?php
require('../fpdf.php');

class PDF extends FPDF
{

    // Page Head
    function Head($status)
    {
        // Logo
        $this->Image('../img/ge.jpg',10,6,30);
        // Arial bold 15
        $this->SetFont('Arial','B',20);
        // Move to the right
        $this->Cell(40);
        // Title
        $this->Cell(0,20,$status,1,1,'C');
        // Line break
        $this->Ln(4);
    }

    // Page footer
    function Footer()
    {
        // Position at 1.5 cm from bottom
        $this->SetY(-15);
        // Arial italic 8
        $this->SetFont('Arial','I',8);
        // Page number
        $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
    }

    // Page Devider
    function Devider($devider_title,$num)
    {
        // Arial 12
        $this->SetFont('Times','B',12);
        // Background color
        $this->SetFillColor(233,90,62);
        // Title
        $this->Cell(0,1,"",0,1,'L',true);
        // Line break
        $this->Ln(1);

        // Title
        $this->Cell(0,10,$devider_title[$num],1,1,'L');
        $this->Ln(10);
    }

    // Colored table
    function FancyTable($header, $data)
    {
        // Colors, line width and bold font
        $this->SetFillColor(255,0,0);
        $this->SetTextColor(255);
        $this->SetDrawColor(128,0,0);
        $this->SetLineWidth(.3);
        $this->SetFont('','B');

        // Header
        $booking_title = array('Booking No.', 'Tour Code', 'Tour Name');
        $w = array(30, 30, 0);
        for($i=0;$i<count($booking_title);$i++)
            $this->Cell($w[$i],7,$booking_title[$i],1,0,'C',true);
        $this->Ln();
        // Color and font restoration
        $this->SetFillColor(224,235,255);
        $this->SetTextColor(0);
        $this->SetFont('');
        // Data

        $fill = false;
        foreach($data as $row)
        {
            $this->Cell($w[0],6,$data[0],'LR',0,'C',$fill);
            $this->Cell($w[1],6,$data[1],'LR',0,'C',$fill);
            $this->MultiCell($w[2],6,$data[2],'LR',0,'L',$fill);
            $this->Ln();
            $fill = !$fill;
        }
        // Closing line
        $this->Cell(0,0,'','T');
        $this->Ln(5);
    }
}

// Instanciation of inherited class
$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Head('Service Voucher [Pending]');

$devider_title = array('Booking Details', 'Guest Details');

$pdf->Devider($devider_title,0);
$header = array('Country', 'Capital', 'Area (sq km)', 'Pop. (thousands)');
$data = array('Country', 'Capital', 'Area (sq km)Area (sq km)Area (sq km)Area (sq km)Area (sq km)Area (sq km)Area (sq km)Area (sq km)Area (sq km)Area (sq km)Area (sq km)');
$pdf->FancyTable($header,$data);


$pdf->Devider($devider_title,1);
$header = array('Country', 'Capital', 'Area (sq km)', 'Pop. (thousands)');
$data = array('Country', 'Capital', 'Area (sq km)');
$pdf->FancyTable($header,$data);


for($i=1;$i<=40;$i++)
    $pdf->Cell(0,10,'Printing line number '.$i,0,1);
$pdf->Output();
?>