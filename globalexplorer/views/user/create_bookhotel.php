<?php
  include_once ('header.php');
?>

            <div id="page-wrapper">
                <!-- /.row -->
                <div class="row">
                    <br>
                    <div class="col-md-4">
                        <img class="thumbnail img-responsive" src="../img/feat/11.jpg" alt="...">
                    </div>
                    <div class="col-md-8">
                        <h4 style="margin-top:0px;">5 DAYS 4 NIGHTS CHIANGMAI CHIANGRAI (DAY TOUR CEI) (Valid to 31 March 2015)</h4>
                          <strong>For 1 Adult, 0 Child, 0 Infant</strong><br>
                          Total: 1 pax<br><br>

                          <strong>Departing on 17-Jul-2014</strong><br>
                          <strong>Returning on 21-Jul-2014 </strong><br>
                          Package: 4 nights<br>
                          Extension: 0 nights<br>
                          Total: 4 nights <br><br><br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-orange">
                            <!-- Default panel contents -->
                            <div class="panel-heading"><strong>Package Hotel</strong></div>
                            <div class="panel-body">
                              <form class="form-inline text-center" role="form">
                                Hotel Choice :
                                <select class="form-control">
                                  <option>Hotel 1</option>
                                  <option>Hotel 2</option>
                                  <option>Hotel 3</option>
                                  <option>Hotel 4</option>
                                  <option>Hotel 5</option>
                                </select>
                              </form>
                              <br>

                              <!-- Table -->
                              <div class="table-responsive">
                                <table class="table table-striped table-bordered">
                                  <tr>
                                    <th class="brown text-center" colspan="3">Package</th>
                                  </tr>
                                  <tr>
                                    <td>Single</td><td>754 / Pax / Trip</td>
                                    <td>
                                      <input class="form-control min-size-num" type="number" placeholder="Pax">
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>Twin</td> <td>754 / Pax / Trip</td>
                                    <td>
                                      <input class="form-control" type="number" placeholder="Pax">
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>Child With Bed</td> <td>754 / Pax / Trip</td>
                                    <td>
                                      <input class="form-control" type="number" placeholder="Pax">
                                    </td>
                                  </tr>
                                </table>
                              </div>

                              <div class="table-responsive">
                                <table class="table table-striped table-bordered">
                                  <tr>
                                    <th class="brown text-center" colspan="3">Rooms Only</th>
                                  </tr>
                                  <tr>
                                    <td>Extension (Single)</td> <td>754 / Room / Night</td>
                                    <td>
                                      <form class="form-inline" role="form">
                                        <input class="form-control min-size-num" type="number" placeholder="Room"> &nbsp; X &nbsp; 
                                        <input class="form-control min-size-num" type="number" placeholder="Night">
                                      </form>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>Extension (Twin)</td> <td>754 / Room / Night</td>
                                    <td>
                                      <form class="form-inline" role="form">
                                        <input class="form-control min-size-num" type="number" placeholder="Room"> &nbsp; X &nbsp; 
                                        <input class="form-control min-size-num" type="number" placeholder="Night">
                                      </form>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td>Remarks : </td>
                                    <td colspan="2"><textarea class="form-control" style="resize:none;" rows="3"></textarea></td>
                                  </tr>
                                </table>
                              </div>

                            </div>
                        </div>

                        <div class="panel panel-orange">
                            <!-- Default panel contents -->
                            <div class="panel-heading"><strong>Optional Tours / Add Ons</strong></div>
                            <div class="panel-body">
                              <div class="table-responsive">
                                <table class="table table-striped table-bordered">
                                  <tr>
                                    <th>Description</th>
                                    <th>Price</th>
                                    <th>Qty</th>
                                  </tr>
                                  <tr>
                                    <td>AO WAN MAPLE(12:45)</td>
                                    <td>40 / Pax</td>
                                    <td><input class="form-control min-size-num" type="number"></td>
                                  </tr>
                                </table>
                              </div>
                            </div>
                        </div>

                        <a class="btn btn-orange" href="#" role="button">Continue</a>
                    </div>
                </div>
            </div>
            <!-- /#page-wrapper -->

<?php
  include_once ('footer.php');
?>