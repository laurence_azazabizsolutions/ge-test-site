        </div>
        <!-- /#wrapper -->

        <!-- jQuery Version 1.11.0 -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="<?php echo base_url('../js/bootstrap.min.js'); ?>"></script>

        <!-- Metis Menu Plugin JavaScript -->
        <script src="<?php echo base_url('../js/plugins/metisMenu/metisMenu.min.js'); ?>"></script>

        <!-- Custom Theme JavaScript -->
        <script src="<?php echo base_url('../js/sb-admin-2.js'); ?>"></script>

        <script src="http://eonasdan.github.io/bootstrap-datetimepicker/scripts/moment.js"></script>
        <script src="<?php echo base_url('../js/bootstrap-datetimepicker.min.js'); ?>"></script>
        <script src="<?php echo base_url('../js/typeahead.js-master/dist/typeahead.bundle.js'); ?>"></script>
        <script src="<?php echo base_url('../js/bootstrapValidator.js'); ?>"></script>
        <script type="text/javascript">
        var d = new Date();
        d.setDate(d.getDate() - 0);
        
        var substringMatcher = function(strs) {
          return function findMatches(q, cb) {
            var matches, substrRegex;
            matches = [];

            substrRegex = new RegExp(q, 'i');

            $.each(strs, function(i, str) {
              if (substrRegex.test(str)) {
                matches.push({ value: str });
              }
            });

            cb(matches);
          };
        };
        

          $(function () {
              $(function () {
                $('#datetimepickerDepart').datetimepicker({
                  minDate:d,
                  pickTime: false
                });
                $('#datetimepickerReturn').datetimepicker({
                  pickTime: false
                });
                $("#datetimepickerDepart").on("dp.change",function (e) {
                  $('#datetimepickerReturn').data("DateTimePicker").setMinDate(e.date);
                });
                $("#datetimepickerReturn").on("dp.change",function (e) {
                  $('#datetimepickerDepart').data("DateTimePicker").setMaxDate(e.date);
                });
            });
          });

          $(function () {
            //provides list of companies from the database.
            //this is just for the meantime
            //by Kirstin
            var companies = JSON.parse(jQuery('#json_value_dd').val());
            $('#typeahead').typeahead({
              hint: true,
              highlight: true,
              minLength: 1
            },
            {
                name: 'company_name',
                displayKey: 'value',
                source: substringMatcher(companies)
            });
            $('#datetimepickerDOB').datetimepicker({
              maxDate:d,
              pickTime: false
            });
          });

          $(document).ready(function() {
          $('#admin_register')
              .bootstrapValidator({
                  message: 'This value is not valid',
                  //live: 'submitted',
                  feedbackIcons: {
                      valid: 'glyphicon glyphicon-ok',
                      invalid: 'glyphicon glyphicon-remove',
                      validating: 'glyphicon glyphicon-refresh'
                  },
                  fields: {
                      salutation: {
                          message: 'The salutation is not valid',
                          validators: {
                              notEmpty: {
                                  message: 'The salutation is required and can\'t be empty'
                              }
                          }
                      },
                      user_type: {
                          message: 'The type is not valid',
                          validators: {
                              notEmpty: {
                                  message: 'Type is required and can\'t be empty'
                              }
                          }
                      },
                      firstname: {
                          message: 'The firstname is not valid',
                          validators: {
                              notEmpty: {
                                  message: 'The first name is required and can\'t be empty'
                              },
                              // stringLength: {
                              //     min: 6,
                              //     max: 30,
                              //     message: 'The firstname must be more than 6 and less than 30 characters long'
                              // },
                              /*remote: {
                                  url: 'remote.php',
                                  message: 'The username is not available'
                              },*/
                              regexp: {
                                  regexp: /^([a-z0-9]*[a-z]){2}[a-zA-Z0-9 '.-]*$/i,
                                  message: 'The first name can only consist of letters and some characters(. -\')'
                              }
                          }
                      },
                      middlename: {
                          message: 'The middlename is not valid',
                          validators: {
                              regexp: {
                                  regexp: /^[a-zA-Z0-9 '.-]*$/i,
                                  message: 'The middle name can only consist of letters and some characters(. -\')'
                              }
                          }
                      },
                      lastname: {
                          message: 'The lastname is not valid',
                          validators: {
                              notEmpty: {
                                  message: 'The lastname is required and can\'t be empty'
                              },
                              // stringLength: {
                              //     min: 6,
                              //     max: 30,
                              //     message: 'The lastname must be more than 6 and less than 30 characters long'
                              // },
                              /*remote: {
                                  url: 'remote.php',
                                  message: 'The username is not available'
                              },*/
                              regexp: {
                                  regexp: /^([a-z0-9]*[a-z]){2}[a-zA-Z0-9 '.-]*$/i,
                                  message: 'The middle name can only consist of letters and some characters(. -\')'
                              }
                          }
                      },
                      dob: {
                          message: 'The date is not valid',
                          validators: {
                              notEmpty: {
                                  message: 'The date of birth is required and can\'t be empty'
                              },
                            date: {
                              format: 'YYYY/MM/DD',
                              message: 'Invalid Date. Please follow the correct format YYYY-MM-DD.'
                            }
                          }
                      },
                      company: {
                          message: 'The company is not valid',
                          validators: {
                            // regexp: {
                            //   regexp: /^[a-zA-Z0-9 !@#$%^&*()'.-]*$/i,
                            //   message: 'Enter a valid company name'
                            // },
                            customCompany: {
                              message: 'Company is required (for non-customer users)',
                              check_only_for: 'admin,agent,provider,account,operator',
                              field: 'user_type'
                            }
                          }
                      },
                      phone_number: {
                          message: 'The phone number is not valid',
                          validators: {
                            notEmpty: {
                                  message: 'Phone number is required and can\'t be empty'
                              },
                              stringLength: {
                                  min: 6,
                                  message: 'Your phone number must be at least 6 characters short'
                              },
                              regexp: {
                                  regexp: /^[0-9 ()+\-]+$/i,
                                  message: 'Enter the correct number format'
                              }
                          }
                      },
                      email: {
                          validators: {
                              notEmpty: {
                                  message: 'The email address is required and can\'t be empty'
                              },
                              emailAddress: {
                                  message: 'The input is not a valid email address'
                              }
                          }
                      },
                      country: {
                          message: 'The country is not valid',
                          validators: {
                              notEmpty: {
                                  message: 'Country is required and can\'t be empty'
                              }
                          }
                      },
                      password: {
                          validators: {
                              notEmpty: {
                                  message: 'The password is required and can\'t be empty'
                              },
                              stringLength: {
                                  min: 8,
                                  message: 'your password must be at least 8 characters short'
                              },
                              regexp: {
                                  regexp: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/i,
                                  message: 'Your password must contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character'
                              }
                          }
                      },
                      confirm_password: {
                          validators: {
                              notEmpty: {
                                  message: 'The password is required and can\'t be empty'
                              },
                              identical: {
                              field: 'password',
                              message: 'The passwords do not match'
                          }
                          }
                      }
                  }
              });
              // .on('success.form.bv', function(e) {
              //     // Prevent submit form
              //     e.preventDefault();

              //     var $form     = $(e.target),
              //         validator = $form.data('bootstrapValidator');
              //     //$form.find('.alert').html('Thanks for signing up. Now you can sign in as ' + validator.getFieldElements('username').val()).show();
              // });
        });

        (function($) {
          $.fn.bootstrapValidator.validators.customCompany = {
              html5Attributes: {
                  message: 'message',
                  field: 'field',
                  check_only_for: 'check_only_for'
              },

              /**
               * Return true if the input value contains a valid US phone number only
               *
               * @param {BootstrapValidator} validator Validate plugin instance
               * @param {jQuery} $field Field element
               * @param {Object} options Consist of key:
               * - message: The invalid message
               * - field: The name of field that will be used to get selected country
               * - check_only_for : Coma separated ISO 3166 country codes // i.e. 'us,ca'
               * @returns {Boolean}
               */
              validate: function(validator, $field, options) {
                  var value = $field.val();
                  

                  var user_type = validator.getFieldElements(options.field);

                  if (user_type == null) {
                      return true;
                  }

                  var usertype = user_type.val().trim();

                  if(usertype == ''){
                      return true;
                  }
                  var check_list = options.check_only_for.toUpperCase().split(',')

                  if(check_list.indexOf(usertype.toUpperCase()) == -1) {
                      return true;
                  }
          
                  if(value == '') return false; 
                  else return (/^[a-zA-Z0-9 !@#$%^&*()'.-]*$/i).test(value);
              }
          }
      }(window.jQuery));
        </script>

    </body>
</html>