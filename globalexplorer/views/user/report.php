<?php
  include_once ('header.php');
?>

            <div id="page-wrapper">
                <!-- /.row -->
                <div class="row">
                    <br>
                    <div class="col-lg-12">

                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Agent</label>
                                <div class="col-sm-8">
                                    <select class="form-control">
                                        <option>Agent Name List</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Provider</label>
                                <div class="col-sm-8">
                                    <select class="form-control">
                                        <option>Provider Name List</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Departure Date</label>
                                <div class="col-sm-4">
                                    <select class="form-control">
                                        <option>Current Month</option>
                                    </select>
                                    <br>
                                </div>
                                <div class="col-sm-4">
                                    <select class="form-control">
                                        <option>Current Year</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Return Date</label>
                                <div class="col-sm-4">
                                    <select class="form-control">
                                        <option>Current Month</option>
                                    </select>
                                    <br>
                                </div>
                                <div class="col-sm-4">
                                    <select class="form-control">
                                        <option>Current Year</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Booking Status</label>
                                <div class="col-sm-8">
                                    <select class="form-control">
                                        <option>-</option>
                                        <option>Pending</option>
                                        <option selected>Confirmed</option>
                                        <option>Rejected</option>
                                        <option>Amended</option>
                                        <option>Cancelled</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-8 col-sm-offset-3">
                                    <button type="reset" class="btn btn-orange">Filter</button>
                                    <button type="reset" class="btn btn-default">Clear</button>
                                </div>
                            </div>
                        </form>

                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Booking Code</th>
                                        <th>Package</th>
                                        <th>Agent/Provider</th>
                                        <th>Pax</th>
                                        <th>Flight</th>
                                        <th>Hotel</th>
                                        <th>Cost</th>
                                        <th>Selling</th>
                                        <th>Profit</th>
                                        <th>Payment</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="danger">
                                        <td>
                                            <a href="#">GE00401</a> <br>
                                            <a href="#">View</a>
                                        </td>
                                        <td>BKK016</td>
                                        <td>
                                            <span class="fa fa-caret-right">&nbsp;</span> Auspicious Travel Pte Ltd <br>
                                            <span class="fa fa-caret-right">&nbsp;</span> Jetview Travel International Co Ltd
                                        </td>
                                        <td>4</td>
                                        <td>
                                            <strong>Ori:</strong> 05-Dec-2011 <br>
                                            14:25:00 hrs - 13:45:00 hrs <br>
                                            <strong>Ret:</strong> 07-Dec-2011 <br>
                                            16:35:00 hrs - 19:55:00 hr <br>
                                            <strong>Num of Days:</strong> 4
                                        </td>
                                        <td>DIAMOND CITY(STANDARD)</td>
                                        <td>840</td>
                                        <td>950</td>
                                        <td>110</td>
                                        <td>
                                            <strong>Agent:</strong> Not Paid <br>
                                            <strong>Provider:</strong> Not Paid
                                        </td>
                                        <td>Confirm</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="#">GE00401</a> <br>
                                            <a href="#">View</a>
                                        </td>
                                        <td>BKK016</td>
                                        <td>
                                            <span class="fa fa-caret-right">&nbsp;</span> Auspicious Travel Pte Ltd <br>
                                            <span class="fa fa-caret-right">&nbsp;</span> Jetview Travel International Co Ltd
                                        </td>
                                        <td>4</td>
                                        <td>
                                            <strong>Ori:</strong> 05-Dec-2011 <br>
                                            14:25:00 hrs - 13:45:00 hrs <br>
                                            <strong>Ret:</strong> 07-Dec-2011 <br>
                                            16:35:00 hrs - 19:55:00 hr <br>
                                            <strong>Num of Days:</strong> 4
                                        </td>
                                        <td>DIAMOND CITY(STANDARD)</td>
                                        <td>840</td>
                                        <td>950</td>
                                        <td>110</td>
                                        <td>
                                            <strong>Agent:</strong> Not Paid <br>
                                            <strong>Provider:</strong> Not Paid
                                        </td>
                                        <td>Confirm</td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th></th>
                                        <th>Total: 2 Booking(s)</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th>7982</th>
                                        <th>8782</th>
                                        <th>800</th>
                                        <th>
                                            0 <br>
                                            0
                                        </th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            <!-- /#page-wrapper -->

<?php
  include_once ('footer.php');
?>