<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Global Explorer</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/bootstrap-datetimepicker.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../../assets/js/ie-emulation-modes-warning.js"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../../assets/js/ie10-viewport-bug-workaround.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="../css/carousel.css" rel="stylesheet">
  </head>

  <body>


      

    <!-- Content 
    ================================================== -->
    
      <!-- contact -->
      <div class="container" style="border:0px solid red; max-width:730px;">
        <br>
        <img src="../img/ge.jpg" class="center-block img-responsive"> <br>
        <h4>Tour Name: **Special Promotion 3 Days 2 Nights Bangkok Free & Easy** (2014)</h4>
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>BOOKING DETAILS</th>
                        <th>DEPARTURE</th>
                        <th>RETURN</th>
                        <th>GUEST/S</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                          <strong>Booking No:</strong> 123 <br>
                          <strong>Tour Code:</strong> 123 <br>
                          <strong>Status:</strong> Confirm
                        </td>
                        <td>
                          <strong>SIN-BKK</strong> <br>
                          11-Aug-2014, 17:40 <br>
                          5J123
                        </td>
                        <td>
                          <strong>BKK-SIN</strong> <br>
                          11-Aug-2014, 17:40 <br>
                          5J123
                        </td>
                        <td>
                          <p>1. My Name (Adult)</p>
                          <p>2. Your Name (Adult)</p>
                          <p>3. Their Name (Adult)</p>
                          <p>4. His Name (Child)</p>
                          <p>5. Her Name (Infant)</p>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <hr class="featurette-divider">
        <h4>Hotel Details</h4>
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>CHECK IN</th>
                        <th>CHECK OUT</th>
                        <th>HOTEL NAME</th>
                        <th>ROOM/S</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>09-Aug-2014</td>
                        <td>11-Aug-2014</td>
                        <td>Pratunam City Hotel - Free Wifi</td>
                        <td>Twin (1)</td>
                    </tr>
                </tbody>
            </table>
        </div>
        
        <br><br><br>
        <p>
          <small>
            <strong>For Emergency:</strong><br>
            Office: 026 419 013 / 026 419 019<br>
            Telephone: 081 402 5354
            <small>
            <ul>
              <small>
                <li>Check out time is strictly before 12 noon. All additional charges will be bear by the passengers. If passengers are late for airport pick-up, they will have to make their own transfer.</li>
                <li>All rooms are guarantted on the day of arrival. In the case of a no-show, your room(s) will be released and you will be subjected to the terms and conditions of the Cancellation/No-Show Policy specified at the time you made the booking as well as noted in the Confirmation Email.</li>
                <li>All special request are subjected to availability upon arrival.</li>
                <li>This is a shared transfer service. As such, there may be multiple hotel pick-ups en route to Airport. All times are approximate and pick up will be provided within 30 minutes from the final pick up time confirmed. Road, traffic and weather conditions may affect the schedule. The pick up time from your hotel will be at least 3 and a half hours prior to your scheduled flight time. Please make sure that you wait at the hotel conceierge desk 15 minutes prior to your confirmed pick up time. Once at the Airport, the shuttle will make stops at different terminals as necessary. Drivers are not expected to know which terminal, so please make sure you check this information with the company you booked your flight with before boarding the shuttle for your transfer to the Airport.</li>
                <li>There is no charge for a maximum of 1 piece of medium sized luggage per person. Oversize or excess luggage may have restrictions and you will be responsible for the transfer and cost of any extra luggage.</li>
              </small>
            </ul></small>
            <small>This is a computer generated service voucher. No signature is required.</small>
          </small>
        </p>


      </div>
      <!-- /details -->

<?php
  include_once ('footer.php');
?>