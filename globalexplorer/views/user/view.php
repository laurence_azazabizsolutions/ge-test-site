<?php
  include_once ('header.php');
?>

            <div id="page-wrapper">
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <h3>Summary</h3>
                        <strong>Booked On:</strong> 03-May-2014 <br>
                        <strong>Package:</strong> 4 nights <br>
                        <strong>Extension:</strong> 0 nights <br>
                        <strong>Total:</strong> 4 nights <br>
                        If Confirm : 
                        <a href="#">Amend</a> |
                        <a href="#">Cancel</a> |
                        <a href="#">Discount</a> |
                        <a href="#">Custom Amend</a> |
                        <a href="invoice.php">Invoice</a> <br>
                        If Amended :
                        <a href="#">Confirm</a> |
                        <a href="#">Reject</a> |
                        <a href="#">Amend</a> |
                        <a href="#">Cancel</a> |
                        <a href="#">Custom Amend</a>

                        <br><br>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Flight and Traveller Details</h3>
                            </div>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>BOOKING DETAILS</th>
                                            <th>DEPARTURE</th>
                                            <th>RETURN</th>
                                            <th>GUEST/S</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                              <strong>Booking No:</strong> 123 <br>
                                              <strong>Tour Code:</strong> 123 <br>
                                              <strong>Status:</strong> Confirm
                                            </td>
                                            <td>
                                              <strong>SIN-BKK</strong> <br>
                                              17-Jul-2014, 17:40 <br>
                                              5J123
                                            </td>
                                            <td>
                                              <strong>BKK-SIN</strong> <br>
                                              21-Jul-2014, 17:40 <br>
                                              5J123
                                            </td>
                                            <td>
                                              <p>1. My Name (Adult)</p>
                                              <p>2. Your Name (Adult)</p>
                                              <p>3. Their Name (Adult)</p>
                                              <p>4. His Name (Child)</p>
                                              <p>5. Her Name (Infant)</p>
                                            </td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>REMARKS</th>
                                            <td colspan="3">This is just a sample remarks for the flight</td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Package Hotel and Optional Tours <small><a href="">Confirm Booking with this Hotel</a></small></h3>
                            </div>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>CHECK IN</th>
                                            <th>CHECK OUT</th>
                                            <th>HOTEL NAME</th>
                                            <th>ROOM</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>09-Aug-2014</td>
                                            <td>11-Aug-2014</td>
                                            <td>Pratunam City Hotel - Free Wifi</td>
                                            <td>
                                                <table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>ROOM CHARGE</th>
                                                            <th>QTY</th>
                                                            <th>TOTAL</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>Twin (56 / Pax)</td>
                                                            <td>2 PAX</td>
                                                            <td>112</td>
                                                        </tr>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th colspan="2">GRAND TOTAL</th>
                                                            <td>112</td>
                                                        </tr>


                                                    </tfoot>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>REMARKS</th>
                                            <td colspan="3">This is just a sample remarks for the hotel</td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>

                        <!-- Agent/Provider -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Agent Details</h3>
                            </div>
                            <div class="table-responsive">
                                <table class="table">
                                    <tr>
                                        <th width="200px">Company Name</th>
                                        <td>GE TRAVEL PTE LTD</td>
                                    </tr>
                                    <tr>
                                        <th>Staff Name</th>
                                        <td>Yuki</td>
                                    </tr>
                                    <tr>
                                        <th>Email Address</th>
                                        <td>admin@getravel.sg</td>
                                    </tr>
                                    <tr>
                                        <th>Contact Number</th>
                                        <td>96514250</td>
                                    </tr>
                                    <tr>
                                        <th>Reference Number</th>
                                        <td></td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Agent Details</h3>
                            </div>
                            <div class="table-responsive">
                                <table class="table">
                                    <tr>
                                        <th width="200px">Company Name</th>
                                        <td>Jetview Travel International Co Ltd</td>
                                    </tr>
                                    <tr>
                                        <th>Staff Name</th>
                                        <td>Chalermchai Sriwasumetharatsami</td>
                                    </tr>
                                    <tr>
                                        <th>Email Address</th>
                                        <td>jetviewboss@hotmail.com, jetview@asianet.co.th, jetviewpatty@hotmail.com,pat@jetviewtravel.com</td>
                                    </tr>
                                    <tr>
                                        <th>Contact Number</th>
                                        <td>081-4494222</td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                        <!-- /.table-responsive -->
                        <p>
                            <h3>5 DAYS 4 NIGHTS CHIANGMAI CHIANGRAI (DAY TOUR CEI) (Valid to 31 March 2015)</h3>
                            <strong>Package includes:</strong><br><br>
                            Day 01: Arrival, Khantoke Dinner<br>
                            Day 02: Doi Suthep Temple, Home industry<br>
                            Day 03: CNX-CEI-CNX, Hot spring, White Temple, Golden Triangle, Maesai<br>
                            Day 04: Elephant at work, Human and snake fighting, Monkey show, Orchid and butterfly Farm, Chiangmai Orientation<br>
                            Day 05: Departure
                        </p>
                    </div>
                </div>
            </div>
            <!-- /#page-wrapper -->

<?php
  include_once ('footer.php');
?>