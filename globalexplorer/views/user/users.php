<?php
  include('header.php');
?>

            <div id="page-wrapper">
                <!-- /.row -->
                <div class="row">
                    <br>
                    <div class="col-lg-12">
                        <!-- Nav tabs -->
                        <ul class="nav nav-pills" role="tablist">
                            <li class="active"><a href="#new" role="tab" data-toggle="tab">New</a></li>
                            <li><a href="#agents" role="tab" data-toggle="tab">Agents</a></li>
                            <li><a href="#providers" role="tab" data-toggle="tab">Providers</a></li>
                            <li><a href="#admins" role="tab" data-toggle="tab">Admins</a></li>
                            <li><a href="#accounts" role="tab" data-toggle="tab">Accounts</a></li>
                            <li><a href="#customers" role="tab" data-toggle="tab">Customers</a></li>
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane active" id="new">
                                <br>
                                <input type="hidden" value=' <?php echo $companies; ?> ' id="json_value_dd">
                                <form id="admin_register" class="form-horizontal" role="form" method="post" action="<?php echo base_url('admin/register')?>">
                                    <div class="alert alert-success" style="display: none;"></div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Salutation<div class="requiredfld"> *</div></label>
                                        <div class="col-sm-8">
                                            <?php echo $salutations ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Type<div class="requiredfld"> *</div></label>
                                        <div class="col-sm-8">
                                            <select class="form-control" name="user_type">
                                                <option value='customer'>Customer</option>
                                                <option value='agent'>Agent</option>
                                                <option value='provider'>Provider</option>
                                                <option value='account'>Account</option>
                                                <option value='operator'>Operator</option>
                                                <option value='admin'>Admin</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">First Name<div class="requiredfld"> *</div></label>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" name="firstname">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Middle Name</label>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" name="middlename">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Last Name<div class="requiredfld"> *</div></label>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" name="lastname">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Last Name<div class="requiredfld"> *</div></label>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" name="lastname">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Date of Birth<div class="requiredfld"> *</div></label>
                                        <div class="col-sm-8">
                                          <div class="input-group date" id="datetimepickerDOB">
                                            <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                            <input class="form-control" type="text" placeholder="e.g. 11/13/1987" name="dob"/>
                                          </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Company</label>
                                        <div class="col-sm-8">
                                          <input id="typeahead" type="text" class="form-control" placeholder="Company XYZ" name="company">
                                        </div>
                                      </div>
                                      <div class="form-group">
                                        <label class="col-sm-3 control-label">Phone<div class="requiredfld"> *</div></label>
                                        <div class="col-sm-8">
                                          <input type="text" class="form-control" placeholder="+65123456" name="phone_number">
                                        </div>
                                      </div>
                                      <div class="form-group">
                                        <label class="col-sm-3 control-label">Email<div class="requiredfld"> *</div></label>
                                        <div class="col-sm-8">
                                          <input type="email" class="form-control" placeholder="johndoe@domail.com" name="email">
                                        </div>
                                      </div>
                                      <div class="form-group">
                                        <label class="col-sm-3 control-label">Country<div class="requiredfld"> *</div></label>
                                        <div class="col-sm-8">
                                          <?php echo $countries ?>
                                        </div>
                                      </div>
                                      <div class="form-group">
                                        <label class="col-sm-3 control-label">Password<div class="requiredfld"> *</div></label>
                                        <div class="col-sm-8">
                                          <input type="password" class="form-control" placeholder="******" name="password">
                                        </div>
                                      </div>
                                      <div class="form-group">
                                        <label class="col-sm-3 control-label">Repeat Password<div class="requiredfld"> *</div></label>
                                        <div class="col-sm-8">
                                          <input type="password" class="form-control" placeholder="******" name="confirm_password">
                                        </div>
                                      </div>
                                    <div class="form-group">
                                        <div class="col-sm-8 col-sm-offset-3">
                                            <button class="btn btn-orange" type="submit">Submit New User</button>
<!--                                             <button type="reset" class="btn btn-orange">Save New User</button> -->
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane" id="agents">
                                <br>
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Action</th>
                                                <th>Username</th>
                                                <th>Contact Name</th>
                                                <th>Company Name</th>
                                                <th>Email</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <a href="#">Edit</a> |
                                                    <a href="#">Delete</a> |
                                                    <a href="#">Activate</a>
                                                </td>
                                                <td>dion</td>
                                                <td>dion</td>
                                                <td>cottoncare</td>
                                                <td>dontsendmeemails</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane" id="providers">
                                <br>
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Action</th>
                                                <th>Username</th>
                                                <th>Contact Name</th>
                                                <th>Company Name</th>
                                                <th>Email</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <a href="#">Edit</a> |
                                                    <a href="#">Delete</a> |
                                                    <a href="#">Activate</a>
                                                </td>
                                                <td>dion</td>
                                                <td>dion</td>
                                                <td>cottoncare</td>
                                                <td>dontsendmeemails</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane" id="admins">
                                <br>
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Action</th>
                                                <th>Username</th>
                                                <th>Contact Name</th>
                                                <th>Company Name</th>
                                                <th>Email</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <a href="#">Edit</a> |
                                                    <a href="#">Delete</a> |
                                                    <a href="#">Activate</a>
                                                </td>
                                                <td>dion</td>
                                                <td>dion</td>
                                                <td>cottoncare</td>
                                                <td>dontsendmeemails</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane" id="accounts">
                                <br>
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Action</th>
                                                <th>Username</th>
                                                <th>Contact Name</th>
                                                <th>Company Name</th>
                                                <th>Email</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <a href="#">Edit</a> |
                                                    <a href="#">Delete</a> |
                                                    <a href="#">Activate</a>
                                                </td>
                                                <td>dion</td>
                                                <td>dion</td>
                                                <td>cottoncare</td>
                                                <td>dontsendmeemails</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="tab-pane" id="customers">
                                <br>
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Action</th>
                                                <th>Username</th>
                                                <th>Contact Name</th>
                                                <th>Company Name</th>
                                                <th>Email</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <a href="#">Edit</a> |
                                                    <a href="#">Delete</a> |
                                                    <a href="#">Activate</a>
                                                </td>
                                                <td>dion</td>
                                                <td>dion</td>
                                                <td>cottoncare</td>
                                                <td>dontsendmeemails</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <!-- myModalHotel -->
                        <div class="modal fade" id="myModalHotel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                          <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="pull-right btn btn-danger btn-sm" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4 class="modal-title" id="myModalLabel">Hotel:</h4>
                              </div>
                              <div class="modal-body">
                                <form class="form-horizontal" role="form">
                                  <div class="form-group">
                                    <label class="col-xs-4 control-label">Hotel Name</label>
                                    <div class="col-xs-8">
                                      <input type="text" class="form-control">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-xs-4 control-label">Website</label>
                                    <div class="col-xs-8">
                                      <input type="url" class="form-control">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-xs-4 control-label">Phone</label>
                                    <div class="col-xs-8">
                                      <input type="text" class="form-control">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-xs-4 control-label">Email</label>
                                    <div class="col-xs-8">
                                      <input type="email" class="form-control">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-xs-4 control-label">Comments</label>
                                    <div class="col-xs-8">
                                      <input type="text" class="form-control">
                                    </div>
                                  </div>
                                </form>
                              </div>
                              <div class="modal-footer">
                                <a class="btn btn-success" href="#" data-dismiss="modal" role="button">Save</a>
                              </div>
                            </div>
                          </div>
                        </div>

                        <!-- myModalRoomRate -->
                        <div class="modal fade" id="myModalRoomRate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                          <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="pull-right btn btn-danger btn-sm" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4 class="modal-title" id="myModalLabel">Hotel Room Rate:</h4>
                              </div>
                              <div class="modal-body">
                                <form class="form-horizontal" role="form">
                                  <div class="form-group">
                                    <label class="col-xs-4 control-label">Rate Name</label>
                                    <div class="col-xs-8">
                                      <select class="form-control">
                                        <option>Single</option>
                                        <option>Twin</option>
                                        <option>Tripple</option>
                                        <option>Child Half Twin</option>
                                        <option>Child With Bed</option>
                                        <option>Child Without Bed</option>
                                        <option>Extension</option>
                                        <option>Extension (Single)</option>
                                        <option>Extension (Twin)</option>
                                        <option>Ex Bed</option>
                                        <option>Breakfast</option>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-xs-4 control-label">Rate Type</label>
                                    <div class="col-xs-8">
                                      <select class="form-control">
                                        <option>Pax / Night</option>
                                        <option>Adult / Night</option>
                                        <option>Child / Night</option>
                                        <option>Infant / Night</option>
                                        <option>Room / Night</option>
                                        <option>Pax / Trip</option>
                                        <option>Adult / Trip</option>
                                        <option>Child / Trip</option>
                                        <option>Infant / Trip</option>
                                        <option>Room / Trip</option>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-xs-4 control-label">Cost</label>
                                    <div class="col-xs-8">
                                      <input type="number" class="form-control">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-xs-4 control-label">Profit</label>
                                    <div class="col-xs-8">
                                      <input type="number" class="form-control">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-xs-4 control-label">Pax</label>
                                    <div class="col-xs-8">
                                      <input type="number" class="form-control">
                                    </div>
                                  </div>
                                </form>
                              </div>
                              <div class="modal-footer">
                                <a class="btn btn-success" href="#" data-dismiss="modal" role="button">Save</a>
                              </div>
                            </div>
                          </div>
                        </div>

                        <!-- myModalRoomRateSurcharge -->
                        <div class="modal fade" id="myModalRoomRateSurcharge" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                          <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="pull-right btn btn-danger btn-sm" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4 class="modal-title" id="myModalLabel">Hotel Room Rate Surcharge:</h4>
                              </div>
                              <div class="modal-body">
                                <form class="form-horizontal" role="form">
                                  <div class="form-group">
                                    <label class="col-xs-4 control-label">Description</label>
                                    <div class="col-xs-8">
                                      <input type="text" class="form-control">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-xs-4 control-label">Rule Type</label>
                                    <div class="col-xs-8">
                                      <select class="form-control">
                                        <option>Date Range</option>
                                        <option>Day of Week</option>
                                        <option>Agent</option>
                                        <option>Blockout</option>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-xs-4 control-label">Rule</label>
                                    <div class="col-xs-8">
                                      <select class="form-control">
                                        <option>From and To</option>
                                        <option>Mon - Sun</option>
                                        <option>List of Agent</option>
                                        <option>From and To</option>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-xs-4 control-label">Cost</label>
                                    <div class="col-xs-8">
                                      <input type="number" class="form-control">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-xs-4 control-label">Profit</label>
                                    <div class="col-xs-8">
                                      <input type="number" class="form-control">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-xs-4 control-label">Rate Type</label>
                                    <div class="col-xs-8">
                                      <select class="form-control">
                                        <option>Pax / Night</option>
                                        <option>Adult / Night</option>
                                        <option>Child / Night</option>
                                        <option>Infant / Night</option>
                                        <option>Room / Night</option>
                                        <option>Pax / Trip</option>
                                        <option>Adult / Trip</option>
                                        <option>Child / Trip</option>
                                        <option>Infant / Trip</option>
                                        <option>Room / Trip</option>
                                      </select>
                                    </div>
                                  </div>
                                </form>
                              </div>
                              <div class="modal-footer">
                                <a class="btn btn-success" href="#" data-dismiss="modal" role="button">Save</a>
                              </div>
                            </div>
                          </div>
                        </div>

                        <!-- myModalHotelSurcharge -->
                        <div class="modal fade" id="myModalHotelSurcharge" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                          <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="pull-right btn btn-danger btn-sm" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4 class="modal-title" id="myModalLabel">Hotel Surcharge:</h4>
                              </div>
                              <div class="modal-body">
                                <form class="form-horizontal" role="form">
                                  <div class="form-group">
                                    <label class="col-xs-4 control-label">Description</label>
                                    <div class="col-xs-8">
                                      <input type="text" class="form-control">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-xs-4 control-label">Rule Type</label>
                                    <div class="col-xs-8">
                                      <select class="form-control">
                                        <option>Date Range</option>
                                        <option>Day of Week</option>
                                        <option>Agent</option>
                                        <option>Blockout</option>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-xs-4 control-label">Rule</label>
                                    <div class="col-xs-8">
                                      <select class="form-control">
                                        <option>From and To</option>
                                        <option>Mon - Sun</option>
                                        <option>List of Agent</option>
                                        <option>From and To</option>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-xs-4 control-label">Cost</label>
                                    <div class="col-xs-8">
                                      <input type="number" class="form-control">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-xs-4 control-label">Profit</label>
                                    <div class="col-xs-8">
                                      <input type="number" class="form-control">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-xs-4 control-label">Rate Type</label>
                                    <div class="col-xs-8">
                                      <select class="form-control">
                                        <option>Pax / Night</option>
                                        <option>Adult / Night</option>
                                        <option>Child / Night</option>
                                        <option>Infant / Night</option>
                                        <option>Room / Night</option>
                                        <option>Pax / Trip</option>
                                        <option>Adult / Trip</option>
                                        <option>Child / Trip</option>
                                        <option>Infant / Trip</option>
                                        <option>Room / Trip</option>
                                      </select>
                                    </div>
                                  </div>
                                </form>
                              </div>
                              <div class="modal-footer">
                                <a class="btn btn-success" href="#" data-dismiss="modal" role="button">Save</a>
                              </div>
                            </div>
                          </div>
                        </div>

                        <!-- myModalOptional -->
                        <div class="modal fade" id="myModalOptional" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                          <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="pull-right btn btn-danger btn-sm" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4 class="modal-title" id="myModalLabel">Optional Tour / Add On:</h4>
                              </div>
                              <div class="modal-body">
                                <form class="form-horizontal" role="form">
                                  <div class="form-group">
                                    <label class="col-xs-4 control-label">Description</label>
                                    <div class="col-xs-8">
                                      <input type="text" class="form-control">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-xs-4 control-label">Unit</label>
                                    <div class="col-xs-8">
                                      <input type="text" class="form-control">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-xs-4 control-label">Cost</label>
                                    <div class="col-xs-8">
                                      <input type="number" class="form-control">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-xs-4 control-label">Profit</label>
                                    <div class="col-xs-8">
                                      <input type="number" class="form-control">
                                    </div>
                                  </div>
                                </form>
                              </div>
                              <div class="modal-footer">
                                <a class="btn btn-success" href="#" data-dismiss="modal" role="button">Save</a>
                              </div>
                            </div>
                          </div>
                        </div>

                        <!-- myModalSurcharge -->
                        <div class="modal fade" id="myModalSurcharge" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                          <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="pull-right btn btn-danger btn-sm" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4 class="modal-title" id="myModalLabel">Package Surcharge:</h4>
                              </div>
                              <div class="modal-body">
                                <form class="form-horizontal" role="form">
                                  <div class="form-group">
                                    <label class="col-xs-4 control-label">Description</label>
                                    <div class="col-xs-8">
                                      <input type="text" class="form-control">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-xs-4 control-label">Rule Type</label>
                                    <div class="col-xs-8">
                                      <select class="form-control">
                                        <option>Date Range</option>
                                        <option>Day of Week</option>
                                        <option>Agent</option>
                                        <option>Blockout</option>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-xs-4 control-label">Rule</label>
                                    <div class="col-xs-8">
                                      <select class="form-control">
                                        <option>From and To</option>
                                        <option>Mon - Sun</option>
                                        <option>List of Agent</option>
                                        <option>From and To</option>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-xs-4 control-label">Cost</label>
                                    <div class="col-xs-8">
                                      <input type="number" class="form-control">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-xs-4 control-label">Profit</label>
                                    <div class="col-xs-8">
                                      <input type="number" class="form-control">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-xs-4 control-label">Rate Type</label>
                                    <div class="col-xs-8">
                                      <select class="form-control">
                                        <option>Pax / Night</option>
                                        <option>Adult / Night</option>
                                        <option>Child / Night</option>
                                        <option>Infant / Night</option>
                                        <option>Room / Night</option>
                                        <option>Pax / Trip</option>
                                        <option>Adult / Trip</option>
                                        <option>Child / Trip</option>
                                        <option>Infant / Trip</option>
                                        <option>Room / Trip</option>
                                      </select>
                                    </div>
                                  </div>
                                </form>
                              </div>
                              <div class="modal-footer">
                                <a class="btn btn-success" href="#" data-dismiss="modal" role="button">Save</a>
                              </div>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /#page-wrapper -->

<?php
  include_once ('footer.php');
?>