<?php
  include_once ('header.php');
?>

            <div id="page-wrapper">
                <!-- /.row -->
                <div class="row">
                    <br>
                    <div class="col-lg-12">
                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label class="col-sm-3 control-label text-right">Destination</label>
                                <div class="col-sm-8">
                                    <select class="form-control">
                                        <option>-</option>
                                    </select>
                                </div>
                            </div>
                        </form>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Title</th>
                                        <th>Picture</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><a href="package_edit.php">Edit</a></td>
                                        <td>
                                            BKK015: 4 Days 3 Nights Bangkok / Pattaya tour (BKK - 2N + PTY - 1N) (Effective from 1 Nov 2013 - 31 Oct 2014)<br>
                                        </td>
                                        <td><img class="thumbnail" src="../img/feat/11.jpg" alt="..." width="150"></td>
                                    </tr>
                                    <tr>
                                        <td><a href="package_edit.php">Edit</a></td>
                                        <td>
                                            BKK015: 4 Days 3 Nights Bangkok / Pattaya tour (BKK - 2N + PTY - 1N) (Effective from 1 Nov 2013 - 31 Oct 2014)<br>
                                        </td>
                                        <td><img class="thumbnail" src="../img/feat/11.jpg" alt="..." width="150"></td>
                                    </tr>
                                    <tr>
                                        <td><a href="package_edit.php">Edit</a></td>
                                        <td>
                                            BKK015: 4 Days 3 Nights Bangkok / Pattaya tour (BKK - 2N + PTY - 1N) (Effective from 1 Nov 2013 - 31 Oct 2014)<br>
                                        </td>
                                        <td><img class="thumbnail" src="../img/feat/11.jpg" alt="..." width="150"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                </div>

                <div class="row text-center">
                    <ul class="pagination pagination-sm">
                        <li><a href="#">&laquo;</a></li>
                        <li class="disabled"><a href="#">1</a></li>
                        <li class="active"><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#">&raquo;</a></li>
                    </ul>
                </div>
            </div>
            <!-- /#page-wrapper -->

<?php
  include_once ('footer.php');
?>