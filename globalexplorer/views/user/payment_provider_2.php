<?php
  include_once ('header.php');
?>

            <div id="page-wrapper">
                <!-- /.row -->
                <div class="row">
                    <br>
                    <div class="col-lg-12">

                        <h2>December 2011</h2>
                        <br>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Booking Code</th>
                                        <th>Details</th>
                                        <th>Flight</th>
                                        <th>Amount</th>
                                        <th>Booking Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="danger">
                                        <td>
                                            <a href="voucher.php">GE01949</a> <br>
                                            <a href="view.php">View</a>
                                        </td>
                                        <td>
                                            <strong>Package:</strong> BKK001 <br>
                                            <strong>Agent:</strong> INDEPENDENT TRAVELLER PTE LTD <br>
                                            <strong>Provider:</strong> Jetview Travel International Co Ltd <br>
                                            <strong>Hotel:</strong> Centre Point Soi 15 (Studio) <br>
                                            <strong>Traveller:</strong> NG HWI MEOW AGNES MS , TAN CHIA HWI SARAH NICOLE MS (2 Pax)
                                        </td>
                                        <td>
                                            <strong>Ori:</strong> 05-Dec-2011 <br>
                                            14:25:00 hrs - 13:45:00 hrs <br>
                                            <strong>Ret:</strong> 07-Dec-2011 <br>
                                            16:35:00 hrs - 19:55:00 hr
                                        </td>
                                        <td>0</td>
                                        <td>Cancelled</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="voucher.php">GE01949</a> <br>
                                            <a href="view.php">View</a>
                                        </td>
                                        <td>
                                            <strong>Package:</strong> BKK001 <br>
                                            <strong>Agent:</strong> INDEPENDENT TRAVELLER PTE LTD <br>
                                            <strong>Provider:</strong> Jetview Travel International Co Ltd <br>
                                            <strong>Hotel:</strong> Centre Point Soi 15 (Studio) <br>
                                            <strong>Traveller:</strong> NG HWI MEOW AGNES MS , TAN CHIA HWI SARAH NICOLE MS (2 Pax)
                                        </td>
                                        <td>
                                            <strong>Ori:</strong> 05-Dec-2011 <br>
                                            14:25:00 hrs - 13:45:00 hrs <br>
                                            <strong>Ret:</strong> 07-Dec-2011 <br>
                                            16:35:00 hrs - 19:55:00 hr
                                        </td>
                                        <td>0</td>
                                        <td>Confirmed</td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th></th>
                                        <th>Total: 2 Booking(s)</th>
                                        <th></th>
                                        <th>0</th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            <!-- /#page-wrapper -->

<?php
  include_once ('footer.php');
?>