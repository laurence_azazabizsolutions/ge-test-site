<?php
  include_once ('header.php');
?>

            <div id="page-wrapper">
                <!-- /.row -->
                <div class="row">
                    <br>
                    <div class="col-lg-12">
                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Departure Month/Year</label>
                                <div class="col-sm-4">
                                    <select class="form-control">
                                        <option>Month</option>
                                    </select>
                                    <br>
                                </div>
                                <div class="col-sm-4">
                                    <select class="form-control">
                                        <option>Month</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Return Date</label>
                                <div class="col-sm-4">
                                    <select class="form-control">
                                        <option>Month</option>
                                    </select>
                                    <br>
                                </div>
                                <div class="col-sm-4">
                                    <select class="form-control">
                                        <option>Month</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Booking Status</label>
                                <div class="col-sm-8">
                                    <select class="form-control">
                                        <option>-</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Booking Code</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-8 col-sm-offset-3">
                                    <button type="button" class="btn btn-orange">Filter</button>
                                    <button type="reset" class="btn btn-default">Clear</button>
                                </div>
                            </div>
                        </form>

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Booking Code</th>
                                        <th>Details</th>
                                        <th>Flight</th>
                                        <th>Amount</th>
                                        <th>Payment</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <a href="voucher.php">GE01949</a> <br>
                                            <a href="view.php">View</a>
                                        </td>
                                        <td>
                                            <strong>Package:</strong> BKK015: 4 Days 3 Nights Bangkok / Pattaya tour (BKK - 2N + PTY - 1N) (Effective from 1 Nov 2013 - 31 Oct 2014)<br>
                                            <strong>Agent:</strong> spring holidays pte ltd<br>
                                            <strong>Provider:</strong> Jetview Travel International Co Ltd<br>
                                            <strong>Hotel:</strong> PRATUNAM CITY INN (Standard)<br>
                                            <strong>Traveller:</strong> Ms YAO XIAOYAN (1 Pax)<br>
                                        </td>
                                        <td>
                                            <strong>Ori:</strong> 27-Jul-2014<br>
                                            <i>2250 hrs - 0020 hrs</i><br>
                                            <strong>Ret:</strong> 30-Jul-2014<br>
                                            <i>2120 hrs - 0040 hrs</i>
                                        </td>
                                        <td>
                                            <strong>Cost:</strong> 235<br>
                                            <strong>Profit:</strong> 25<br>
                                            <strong>Price:</strong> 260
                                        </td>
                                        <td>
                                            <strong>Agent Paid:</strong> No<br>
                                            <strong>Provider Paid:</strong> No
                                        </td>
                                        <td>Confirmed</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="voucher.php">GE01949</a> <br>
                                            <a href="view.php">View</a>
                                        </td>
                                        <td>
                                            <strong>Package:</strong> BKK015: 4 Days 3 Nights Bangkok / Pattaya tour (BKK - 2N + PTY - 1N) (Effective from 1 Nov 2013 - 31 Oct 2014)<br>
                                            <strong>Agent:</strong> spring holidays pte ltd<br>
                                            <strong>Provider:</strong> Jetview Travel International Co Ltd<br>
                                            <strong>Hotel:</strong> PRATUNAM CITY INN (Standard)<br>
                                            <strong>Traveller:</strong> Ms YAO XIAOYAN (1 Pax)<br>
                                        </td>
                                        <td>
                                            <strong>Ori:</strong> 27-Jul-2014<br>
                                            <i>2250 hrs - 0020 hrs</i><br>
                                            <strong>Ret:</strong> 30-Jul-2014<br>
                                            <i>2120 hrs - 0040 hrs</i>
                                        </td>
                                        <td>
                                            <strong>Cost:</strong> 235<br>
                                            <strong>Profit:</strong> 25<br>
                                            <strong>Price:</strong> 260
                                        </td>
                                        <td>
                                            <strong>Agent Paid:</strong> No<br>
                                            <strong>Provider Paid:</strong> No
                                        </td>
                                        <td>Confirmed</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="voucher.php">GE01949</a> <br>
                                            <a href="view.php">View</a>
                                        </td>
                                        <td>
                                            <strong>Package:</strong> BKK015: 4 Days 3 Nights Bangkok / Pattaya tour (BKK - 2N + PTY - 1N) (Effective from 1 Nov 2013 - 31 Oct 2014)<br>
                                            <strong>Agent:</strong> spring holidays pte ltd<br>
                                            <strong>Provider:</strong> Jetview Travel International Co Ltd<br>
                                            <strong>Hotel:</strong> PRATUNAM CITY INN (Standard)<br>
                                            <strong>Traveller:</strong> Ms YAO XIAOYAN (1 Pax)<br>
                                        </td>
                                        <td>
                                            <strong>Ori:</strong> 27-Jul-2014<br>
                                            <i>2250 hrs - 0020 hrs</i><br>
                                            <strong>Ret:</strong> 30-Jul-2014<br>
                                            <i>2120 hrs - 0040 hrs</i>
                                        </td>
                                        <td>
                                            <strong>Cost:</strong> 235<br>
                                            <strong>Profit:</strong> 25<br>
                                            <strong>Price:</strong> 260
                                        </td>
                                        <td>
                                            <strong>Agent Paid:</strong> No<br>
                                            <strong>Provider Paid:</strong> No
                                        </td>
                                        <td>Confirmed</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->

                    </div>
                </div>

                <div class="row text-center">
                    <ul class="pagination pagination-sm">
                        <li><a href="#">&laquo;</a></li>
                        <li class="disabled"><a href="#">1</a></li>
                        <li class="active"><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#">&raquo;</a></li>
                    </ul>
                </div>
            </div>
            <!-- /#page-wrapper -->

<?php
  include_once ('footer.php');
?>