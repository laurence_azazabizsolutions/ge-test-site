<?php
  include_once ('header.php');
?>

            <div id="page-wrapper">
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <h3>Pendings Bookings</h3><!-- pending_data -->
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>Booking Code</th>
                                        <th>Details</th>
                                        <th>Flight</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <a href="voucher.php">GE01949</a> <br>
                                            <a href="view.php">View</a>
                                        </td>
                                        <td>
                                            <strong>Package:</strong> BKK110: **Special Promotion 3 Days 2 Nights Bangkok Free & Easy** (2014)<br>
                                            <strong>Agent:</strong> phoenixholiday<br>
                                            <strong>Provider:</strong> Jetview Travel International Co Ltd<br>
                                            <strong>Hotel:</strong> Centre Point Petchburi 15 - Free Wifi<br>
                                            <strong>Traveller:</strong> Ms TIONG KINCHOR (8 Pax)<br>
                                        </td>
                                        <td>
                                            <strong>Ori:</strong> 13-Oct-2014<br>
                                            <i>08:25 hrs - 09:50 hrs</i><br>
                                            <strong>Ret:</strong> 16-Oct-2014<br>
                                            <i>20:05 hrs - 23:30 hrs</i>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="voucher.php">GE01949</a> <br>
                                            <a href="view.php">View</a>
                                        </td>
                                        <td>
                                            <strong>Package:</strong> BKK110: **Special Promotion 3 Days 2 Nights Bangkok Free & Easy** (2014)<br>
                                            <strong>Agent:</strong> phoenixholiday<br>
                                            <strong>Provider:</strong> Jetview Travel International Co Ltd<br>
                                            <strong>Hotel:</strong> Centre Point Petchburi 15 - Free Wifi<br>
                                            <strong>Traveller:</strong> Ms TIONG KINCHOR (8 Pax)<br>
                                        </td>
                                        <td>
                                            <strong>Ori:</strong> 13-Oct-2014<br>
                                            <i>08:25 hrs - 09:50 hrs</i><br>
                                            <strong>Ret:</strong> 16-Oct-2014<br>
                                            <i>20:05 hrs - 23:30 hrs</i>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="voucher.php">GE01949</a> <br>
                                            <a href="view.php">View</a>
                                        </td>
                                        <td>
                                            <strong>Package:</strong> BKK110: **Special Promotion 3 Days 2 Nights Bangkok Free & Easy** (2014)<br>
                                            <strong>Agent:</strong> phoenixholiday<br>
                                            <strong>Provider:</strong> Jetview Travel International Co Ltd<br>
                                            <strong>Hotel:</strong> Centre Point Petchburi 15 - Free Wifi<br>
                                            <strong>Traveller:</strong> Ms TIONG KINCHOR (8 Pax)<br>
                                        </td>
                                        <td>
                                            <strong>Ori:</strong> 13-Oct-2014<br>
                                            <i>08:25 hrs - 09:50 hrs</i><br>
                                            <strong>Ret:</strong> 16-Oct-2014<br>
                                            <i>20:05 hrs - 23:30 hrs</i>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                </div>

                <div class="row text-center">
                    <ul class="pagination pagination-sm">
                        <li><a href="#">&laquo;</a></li>
                        <li class="disabled"><a href="#">1</a></li>
                        <li class="active"><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#">&raquo;</a></li>
                    </ul>
                </div>
            </div>
            <!-- /#page-wrapper -->

<?php
  include_once ('footer.php');
?>