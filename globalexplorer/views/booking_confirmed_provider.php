<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Global Explorer</title>
 		<style type="text/css">
			h1,
			h4{
				font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
				font-weight: 500;
				line-height: 1.1;
				color: inherit;
			}
		</style>
	</head>
	<body style="color: #333333;">
		<div style="padding-right: 15px; padding-left: 15px; margin-right: auto; margin-left: auto;">
			<br>
			<div style="margin-bottom: 20px; background-color: #ffffff; border: 1px solid transparent;  border-radius: 4px; -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05); box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05); border-color: #e95a3e;">
				<div style="padding: 10px 15px; border-bottom: 1px solid transparent; border-top-right-radius: 3px; border-top-left-radius: 3px; color: #fff; background-color: #e95a3e; border-color: #e95a3e;">
					<h1 style="margin-top: 0; margin-bottom: 0; font-size: 16px; color: inherit; text-align: center; ">
						Global Explorer
					</h1>
				</div>
				<div style="padding: 15px;">
					<h4 style="font-size: 18px; margin-top: 10px; margin-bottom: 10px;">
						Hey, <?php echo $mail_data[0]->providerfirstname.' '.$mail_data[0]->providerlastname;?>!
					</h4>
					<br>
					<p style="margin: 0 0 10px;">
						The <?php echo $mail_data[0]->packtitle;?>  on  <?php echo date_format(date_create($mail_data[0]->bookeddepart),'d-m-Y');?>  until  <?php echo date_format(date_create($mail_data[0]->bookedreturn),'d-m-Y');?><?php if($mail_data[0]->bookingextend > 0){ echo ' with ' . $mail_data[0]->bookingextend . ' night/s extension'; }; ?> has been confirmed.
					</p><br>
					Booking No.: <?php echo $mail_data[0]->bookedcode;?><br>
					<br>
						Cheers,
					<br>
						<?php echo $user_click[0]->userfname.' ,'.$user_click[0]->userlname;?>
				</div>
			</div>
		</div>
	</body>
</html>