<?php
  include_once ('header.php');
?>

    <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">

      <!-- images -->
      <div class="carousel-inner">
        <div class="item active grad">
          <div class="carousel-fill" style="background-image:url('img/1.jpg');"></div>
        </div>
        <div class="item grad">
          <div class="carousel-fill" style="background-image:url('img/2.jpg');"></div>
        </div>
        <div class="item grad">
          <div class="carousel-fill" style="background-image:url('img/3.jpg');"></div>
        </div>
      </div>

      <!-- caption -->
      <div class="container carousel-select">
        <div class="carousel-caption">
          <div class="form-group">
            <p>
              <label for="selectDestination" class="col-md-4 col-md-offset-2 control-label text-right"><span class="fa fa-plane"></span> Travelling To : </label>
              <div class="col-md-4">
                <select class="form-control" id="selectDestination">
                  <option value="#">---(auto load selected destination)</option>
                  <option value="destination.php">China</option>
                  <option value="destination.php">Shanghai</option>
                  <option value="destination.php">Beijing</option>
                  <option value="destination.php">Thailand</option>
                  <option value="destination.php">Chiangmai</option>
                  <option value="destination.php">Bangkok</option>
                  <option value="destination.php">Khabi</option>
                  <option value="destination.php">Taiwan</option>
                </select>
              </div>
            </p>
            <br><br><br><br><br><br><br><br><br><br>
          </div>
        </div>
      </div>

      <!-- arrow -->
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
    </div><!-- /.carousel -->


    <!-- Content 
    ================================================== -->
    <div class="container-fluid container-fluid-overlay">

      <!-- featured -->
      <div class="container-fluid">
        <div class="container pax-dest">
          <h5 class="text-shadow-white"><strong>FEATURED DESTINATIONS</strong></h5>
          <br>
          <div class="row">
            <div class="col-xs-3">
              <a href="details.php">
                <div class="pax-img">
                  <div class="pax-fill" style="background-image:url('img/feat/11.jpg');"></div>
                </div>
                <p class="pax-details">5 DAYS 4 NIGHTS CHIANGMAI CHIANGRAI (DAY TOUR CEI) (Valid to 31 March 2015)</p>
              </a>
            </div>
            <div class="col-xs-3">
              <a href="details.php">
                <div class="pax-img">
                  <div class="pax-fill" style="background-image:url('img/feat/22.jpg');"></div>
                </div>
                <p class="pax-details">3 DAY 2 NIGHTS CHIANGMAI TOUR (VALID TO 31 March 2015)Minimum 2 Paxs</p>
              </a>
            </div>
            <div class="col-xs-3">
              <a href="details.php">
                <div class="pax-img">
                  <div class="pax-fill" style="background-image:url('img/feat/33.jpg');"></div>
                </div>
                <p class="pax-details">4 DAYS 3 NIGHTS CHIANGMAI ELEPHANT CAMP + MIX HILLTRIBE (Valid to 31 March 2015)</p>
              </a>
            </div>
            <div class="col-xs-3">
              <a href="details.php">
                <div class="pax-img">
                  <div class="pax-fill" style="background-image:url('img/feat/44.jpg');"></div>
                </div>
                <p class="pax-details">3 DAY 2 NIGHTS CHIANGMAI CHIANGRAI TOUR (VALID TO 31 March 2015)</p>
              </a>
            </div>
          </div>
        </div>
      </div>
      <!-- /featured -->

<?php
  include_once ('footer.php');
?>