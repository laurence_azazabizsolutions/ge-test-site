<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="<?php echo base_url('img/icon.png'); ?>">
         <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="<?php echo base_url('js/bootstrap.min.js'); ?>"></script>
        <script src="<?php echo base_url('js/public/general.js'); ?>"></script>
        <script src="<?php echo base_url('js/public/booking.js'); ?>"></script>
        <script src="<?php echo base_url('../../assets/js/docs.min.js'); ?>"></script>
		<script src="http://eonasdan.github.io/bootstrap-datetimepicker/scripts/moment.js"></script>
		 <script type="text/javascript">
            //redirect to selected Destination
            $(document).ready( function() {
                $('#selectDestination').change( function() {
                    location.href = $(this).val();
                })
                 $('#myModalLogin').modal('show');
                
            });
        </script>
        <script src="<?php echo base_url('js/bootstrap-datetimepicker.min.js'); ?>"></script>
        <script src="<?php echo base_url('js/typeahead.js-master/dist/typeahead.bundle.js'); ?>"></script>
        <script src="<?php echo base_url('js/bootstrapValidator.js'); ?>"></script>
        <script src="<?php echo base_url('js/picker.js'); ?>"></script>
        <script src="<?php echo base_url('js/picker.date.js'); ?>"></script>
        <script src="<?php echo base_url('js/picker.date.setting.js'); ?>"></script>
        <title>Global Explorer</title>

        <!-- Bootstrap core CSS -->
        <link href="<?php echo base_url('css/bootstrap.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('css/bootstrap-datetimepicker.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('css/bootstrapValidator.css'); ?>" rel="stylesheet">

        <!-- date picker within modal -->
        <link href="<?php echo base_url('css/default.css'); ?>" rel="stylesheet" id="theme_base">
        <link href="<?php echo base_url('css/default.date.css'); ?>" rel="stylesheet" id="theme_base">


        <!-- Custom Fonts -->
        <link href="<?php echo base_url('css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Custom styles for this template -->
        <link href="<?php echo base_url('css/carousel.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('css/typeahead_custom.css'); ?>" rel="stylesheet">

    </head>

    <body>
        <!-- NAVBAR
        ================================================== -->
        <div class="navbar-wrapper">
            <div class="container">
                <div class="navbar navbar-inverse navbar-static-top" role="navigation">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="<?php echo base_url(); ?>">Global Explorer</a>
                        </div>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav navbar-right">
                                <li class="<?php if (isset($page) && $page == 'index'): ?>active<?php endif; ?>"><a href="<?php echo base_url(); ?>">HOME</a></li>
                                <li class="<?php if (isset($page) && $page == 'about'): ?>active<?php endif; ?>"><a href="<?php echo base_url('about'); ?>">ABOUT</a></li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">BROWSE <span class="caret"></span></a>
                                    <ul class="dropdown-menu dropdown-menu-nav" role="menu">
                                        <li class="active"><a href="#">Destination 1</a></li>
                                        <li><a href="#">Destination 2</a></li>
                                        <li><a href="#">Destination 3</a></li>
                                        <li class="divider"></li>
                                        <li class="dropdown-header">Nav header</li>
                                        <li><a href="#">Separated link</a></li>
                                        <li><a href="#">One more separated link</a></li>
                                    </ul>
                                </li>
                                <li class="<?php if (isset($page) && $page == 'contact'): ?>active<?php endif; ?>"><a href="<?php echo base_url('contact'); ?>">CONTACT</a></li>
                                <?php if (isset($login)): ?>
                                    <?php if (FALSE === $login): ?>
                                        <li><a href="" data-toggle="modal" data-target="#myModalLogin">LOGIN</a></li>
                                    <?php elseif (isset($usertype)): ?>
                                    	<?php if ($usertype instanceof Customer): ?>
                                        <li><a href="<?php echo base_url('access/logout'); ?>">LOGOUT</a></li>
                                        <?php else:?>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">SETTING <span class="caret"></span></a>
                                            <ul class="dropdown-menu dropdown-menu-nav" role="menu">
                                                <li><a href="<?php echo base_url('panel'); ?>">View Dashboard</a></li>
                                                <li class="divider"></li>
                                                <li><a href="<?php echo base_url('access/logout'); ?>">Logout</a></li>
                                            </ul>
                                        </li>
                                        <?php endif; ?>
                                    <?php else: ?>
                                    <li><a href="<?php echo base_url('access/logout'); ?>">LOGOUT</a></li>
                                    <?php endif; ?>
                                <?php else: ?>
                                    <li><a href="" data-toggle="modal" data-target="#myModalLogin">LOGIN</a></li>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- myModalLogin -->
        <div class="modal fade" id="myModalLogin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="pull-right btn btn-danger btn-sm" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Please Login:</h4>
                    </div>
                    <form id="login_form" class="form-horizontal" role="form" action="<?php echo base_url('change/login'); ?>" method="post">
                        <div class="modal-body">
                           <!--  <?php //if($error_message && isset($error_message)): ?>
                                <div class="alert alert-danger" ><?php //echo $error_message?></div>
                            <?php //endif; ?> -->
                            <div class="form-group">
                                <label class="col-xs-4 control-label">Email</label>
                                <div class="col-xs-8">
                                	<input type="hidden" class="form-control" placeholder="Email" name="id_wang" value="<?php echo $anuar;?>">
                                    <input type="email" class="form-control" placeholder="Email" name="email_login">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-4 control-label">Password</label>
                                <div class="col-xs-8">
                                    <input type="password" class="form-control" placeholder="Password" name="password_login">
                                </div>
                            </div>
                            <!-- <div class="form-group">
                                <a class="col-xs-offset-4 col-xs-8" href="#" data-dismiss="modal" data-toggle="modal" data-target="#myModalForget">Forgot password?</a>
                            </div> -->
                        </div>
                        <div class="modal-footer">
                           <!--  <strong>No Account yet? <a href="#" data-dismiss="modal" data-toggle="modal" data-target="#myModalSignUp" role="button">Sign Up Now!</a></strong> &nbsp; -->
                            <button class="btn btn-orange" type="submit">Login</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- myModalForget -->
        <div class="modal fade" id="myModalForget" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="pull-right btn btn-danger btn-sm" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Enter Your Email:</h4>
                    </div>
                    <form id="defaultFormForget" method="post" class="form-horizontal" role="form" action="#">
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="col-xs-4 control-label">Email</label>
                                <div class="col-xs-8">
                                    <input type="email" class="form-control" placeholder="Email Address" name="email">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <strong>No Account yet? <a href="#" data-dismiss="modal" data-toggle="modal" data-target="#myModalSignUp" role="button">Sign Up Now!</a></strong> &nbsp;
                            <button type="submit" class="btn btn-orange">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- myModalSignUp -->
        <div class="modal fade" id="myModalSignUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <input type="hidden" value=' <?php echo (isset($companies)) ? $companies : json_encode(array()); ?> ' id="json_value_dd">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class=" pull-right btn btn-danger btn-sm" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Create Account:</h4>
                        <div style="color:#333333;font-size:11px">Fields marked with * are required</div>
                    </div>
                    <form id="signup_form" class="form-horizontal" role="form" method="post" action="<?php echo base_url('login_changed/customer'); ?>">
                        <div class="modal-body">
                            <div class="container" style="width:100%;">
                                <div class="alert alert-danger" style="display: none;"></div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="col-xs-5 control-label">Salutation
                                                <div class="requiredfld">*</div>
                                            </label>
                                            <div class="col-xs-7">
                                                <select class="form-control" name="salutation">
                                                    <option value=" ">- Select -</option>
                                                    <option value="mr.">Mr.</option>
                                                    <option value="ms.">Ms.</option>
                                                    <option value="mrs.">Mrs.</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-xs-5 control-label">First Name
                                                <div class="requiredfld">*</div>
                                            </label>
                                            <div class="col-xs-7">
                                                <input type="text" class="form-control" placeholder="John" name="firstname">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-xs-5 control-label">Middle Name</label>
                                            <div class="col-xs-7">
                                                <input type="text" class="form-control" placeholder=" " name="middlename">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-xs-5 control-label">Last Name
                                                <div class="requiredfld">*</div>
                                            </label>
                                            <div class="col-xs-7">
                                                <input type="text" class="form-control" placeholder="Doe" name="lastname">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-xs-5 control-label">Company</label>
                                            <div class="col-xs-7">
                                                <input id="typeahead" type="text" class="form-control" placeholder="Company XYZ" name="company">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="col-xs-5 control-label">Phone
                                                <div class="requiredfld">*</div>
                                            </label>
                                            <div class="col-xs-7">
                                                <input type="text" class="form-control" placeholder="+65123456" name="phone_number">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-xs-5 control-label">Email
                                                <div class="requiredfld">*</div>
                                            </label>
                                            <div class="col-xs-7">
                                                <input type="email" class="form-control" placeholder="johndoe@domail.com" name="email">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-xs-5 control-label">Country
                                                <div class="requiredfld">*</div>
                                            </label>
                                            <div class="col-xs-7">
                                                <select name="country" class="form-control">
                                                    <?php if (isset($countries)): ?>
                                                        <?php foreach ($countries as $country): ?>
                                                            <option value="<?php echo $country['code']; ?>"><?php echo $country['country']; ?></option>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-xs-5 control-label">Password
                                                <div class="requiredfld">*</div>
                                            </label>
                                            <div class="col-xs-7">
                                                <input type="password" class="form-control" placeholder="******" name="password">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-xs-5 control-label">Repeat Password
                                                <div class="requiredfld">*</div>
                                            </label>
                                            <div class="col-xs-7">
                                                <input type="password" class="form-control" placeholder="******" name="confirm_password">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <strong>Already registered? <a href="#" data-dismiss="modal" data-toggle="modal" data-target="#myModalLogin" role="button">Login Now!</a></strong> &nbsp;
                            <button class="btn btn-orange" type="submit">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- myModalSuccess -->
        <div class="modal fade" id="myModalSuccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="pull-right btn btn-danger btn-xs" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <p class="modal-title" id="myModalLabel"><?php if(isset($confirmation_message)) echo $confirmation_message; ?></p>
                    </div>
                </div>
            </div>
        </div>
	
	<!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">

      <!-- images -->
      <div class="carousel-inner">
        <div class="item active grad">
          <div class="carousel-fill" style="background-image:url('<?php echo base_url('img/1.jpg'); ?>');"></div>
        </div>
        <div class="item grad">
          <div class="carousel-fill" style="background-image:url('<?php echo base_url('img/2.jpg'); ?>');"></div>
        </div>
        <div class="item grad">
          <div class="carousel-fill" style="background-image:url('<?php echo base_url('img/3.jpg'); ?>');"></div>
        </div>
      </div>

      <!-- caption -->
      <div class="container carousel-select">
        <div class="carousel-caption">
          <div class="form-group">
            <p>
              <label for="selectDestination" class="col-md-4 col-md-offset-2 control-label text-right"><span class="fa fa-plane"></span> Travelling To : </label>
              <div class="col-md-4">
                <select class="form-control" id="selectDestination">
                  <!-- <option value="#">---(auto load selected destination)</option> -->
                  <!-- <option value="#">China</option> -->
                  <option value="CN">Shanghai</option>
                  <option value="BJ">Beijing</option>
                  <!-- <option value="#">Thailand</option> -->
                  <option value="TC">Chiangmai</option>
                  <option value="TH">Bangkok</option>
                  <option value="TK">Krabi</option>
                  <option value="TW">Taiwan</option>
                </select>
              </div>
            </p>
            <br><br><br><br><br><br><br><br><br><br>
          </div>
        </div>
      </div>

      <!-- arrow -->
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
    </div><!-- /.carousel -->


    <!-- Content 
    ================================================== -->
    <div class="container-fluid container-fluid-overlay">

      <!-- featured -->
      <div class="container-fluid">
        <div class="container pax-dest">
          <h5 class="text-shadow-white"><strong>FEATURED DESTINATIONS</strong></h5>
          <br>
          <div class="row">
            <div class="col-xs-3">
              <a href="<?php echo base_url('details1'); ?>">
                <div class="pax-img">
                  <div class="pax-fill" style="background-image:url('<?php echo base_url('img/feat/11.jpg'); ?>');"></div>
                </div>
                <p class="pax-details">5 DAYS 4 NIGHTS CHIANGMAI CHIANGRAI (DAY TOUR CEI) (Valid to 31 March 2015)</p>
              </a>
            </div>
            <div class="col-xs-3">
              <a href="<?php echo base_url('details1'); ?>">
                <div class="pax-img">
                  <div class="pax-fill" style="background-image:url('<?php echo base_url('img/feat/22.jpg'); ?>');"></div>
                </div>
                <p class="pax-details">3 DAY 2 NIGHTS CHIANGMAI TOUR (VALID TO 31 March 2015)Minimum 2 Paxs</p>
              </a>
            </div>
            <div class="col-xs-3">
              <a href="<?php echo base_url('details1'); ?>">
                <div class="pax-img">
                  <div class="pax-fill" style="background-image:url('<?php echo base_url('img/feat/33.jpg'); ?>');"></div>
                </div>
                <p class="pax-details">4 DAYS 3 NIGHTS CHIANGMAI ELEPHANT CAMP + MIX HILLTRIBE (Valid to 31 March 2015)</p>
              </a>
            </div>
            <div class="col-xs-3">
              <a href="<?php echo base_url('details1'); ?>">
                <div class="pax-img">
                  <div class="pax-fill" style="background-image:url('<?php echo base_url('img/feat/44.jpg'); ?>');"></div>
                </div>
                <p class="pax-details">3 DAY 2 NIGHTS CHIANGMAI CHIANGRAI TOUR (VALID TO 31 March 2015)</p>
              </a>
            </div>
          </div>
        </div>
      </div>
      <!-- /featured -->



            <div class="container-fluid footer footer-one">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <h6>Global Explorer</h6>
                            <p class="lead">
                                Offering the most superior finishes and amenities in all  Asian Country.
                                <br><br>
                                We can fulfill any of your needs.
                            </p>
                            <br>
                        </div>
                        <div class="col-md-4">
                            <h6>Book Your Travel Now!</h6>
                            <p class="lead">
                                <span class="glyphicon glyphicon-home">&nbsp;</span> 53 Ubi Ave 1, Ubi Industrial Park #01-29, Singapore 408934<br>
                                <span class="glyphicon glyphicon-earphone">&nbsp;</span> +65 8451 6666<br>
                                <span class="glyphicon glyphicon-envelope">&nbsp;</span> <a href="mailto:edmundwan@hotmail.com">edmundwan@yahoo.com</a><br>
                                <span class="glyphicon glyphicon-envelope">&nbsp;</span> <a href="mailto:op.globalexplorer@gmail.com">op.globalexplorer@gmail.com</a>
                            </p>
                            <br>
                        </div>
                        <div class="col-md-4">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3988.8147299875236!2d103.846524!3d1.2851339999999998!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31da190b691fac67%3A0x74054e0264fbed72!2s150+S+Bridge+Rd%2C+Singapore+058727!5e0!3m2!1sen!2sph!4v1407378742324" width="100%" height="100" frameborder="0" style="border:0"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container-fluid footer footer-two">
                <div class="container">
                    <p>Copyright &copy; 2014 Global Explorer. All Rights Reserved.</p>
                </div>
            </div>
            <!-- /footer -->

        </div>
        <!-- /Content -->

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
    </body>
</html>