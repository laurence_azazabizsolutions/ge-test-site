<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Global Explorer</title>
 		<style type="text/css">
			h1,
			h4{
				font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
				font-weight: 500;
				line-height: 1.1;
				color: inherit;
			}
		</style>
	</head>
	<body style="color: #333333;">
		<div style="padding-right: 15px; padding-left: 15px; margin-right: auto; margin-left: auto;">
			<br>
			<div style="margin-bottom: 20px; background-color: #ffffff; border: 1px solid transparent;  border-radius: 4px; -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05); box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05); border-color: #e95a3e;">
				<div style="padding: 10px 15px; border-bottom: 1px solid transparent; border-top-right-radius: 3px; border-top-left-radius: 3px; color: #fff; background-color: #e95a3e; border-color: #e95a3e;">
					<h1 style="margin-top: 0; margin-bottom: 0; font-size: 16px; color: inherit; text-align: center; ">
						Global Explorer
					</h1>
				</div>
				<div style="padding: 15px;">
					<h4 style="font-size: 18px; margin-top: 10px; margin-bottom: 10px;">
						Hey, <?php echo $firstname.' '.$lastname; ?>!
					</h4>
					<p style="margin: 0 0 10px;">Were ready to activate your account. All we need to do is make sure this is your email address.</p>
					<br>
					<center>
						<a href="<?php echo $link; ?>" style="color: #C64E0A; text-decoration:none;">
							<button style="padding: 6px 12px; margin-bottom: 0; font-size: 14px; font-weight: normal; line-height: 1.428571429; white-space: nowrap; cursor: pointer; background-image: none; border: 1px solid transparent; border-radius: 4px; -webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; -o-user-select: none; user-select: none;color: #fff; background-color: #e95a3e; border-color: #e02908;">Verify Address</button>
						 </a>
					</center>
					<br>
					<p style="margin: 0 0 10px; display: block; margin-top: 5px; margin-bottom: 10px; color: #737373;">If you didnt create a Global Explorer account, just delete this email and everything will go back to the way it was.</p>
				</div>
			</div>
		</div>
	</body>
</html>