

	</div>
		<!-- /#wrapper -->

		<!-- removed the "../" for bootrstrap.min, metisMenu, sbAdmin, datetimepicker,typeahead, validator -->

		<!-- jQuery Version 1.11.0 -->
		<!--
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		-->


		<!-- Bootstrap Core JavaScript -->
		<script src="<?php echo base_url('js/bootstrap.min.js'); ?>"></script>

		<!-- Metis Menu Plugin JavaScript -->
		<script src="<?php echo base_url('js/plugins/metisMenu/metisMenu.min.js'); ?>"></script>

		<!-- Custom Theme JavaScript -->
		<script src="<?php echo base_url('js/sb-admin-2.js'); ?>"></script>
		<script src="<?php echo base_url('js/moment.min.js'); ?>"></script>
		<script src="<?php echo base_url('js/bootstrap-datetimepicker.min.js'); ?>"></script>
		<script src="<?php echo base_url('js/typeahead.js-master/dist/typeahead.bundle.js'); ?>"></script>
		<script src="<?php echo base_url('js/bootstrapValidator.js'); ?>"></script>
		<script src="<?php echo base_url('js/panel/booking.js'); ?>"></script>

		<script src="<?php echo base_url('js/bootbox.min.js'); ?>"></script>
		<script src="<?php echo base_url('js/text_function.js'); ?>"></script>
		<script type="text/javascript">
			//current date
			var d = new Date();
			d.setDate(d.getDate() - 0);

			function func_filter() {
				//alert('as');


	        	//e.preventDefault();
	        	//$A = $(this);
	        	$form 			= $('#form_filter_booking');
	        	$div_package 	= $('#package_holder');
	        	$div_pagination = $('.row.text-center');
	        	//$A.attr('disabled','disabled');

	        	var loader = "<?php echo base_url('./img/loader.gif');?>";
		 		var $element = '<center><img id="loader" height="50px" src="'+ loader +'"></center>';
				$form.find('.alert').addClass('alert-warning').css('padding','0').append($element).show();

        		$.post($form.attr('action'), $form.serialize(), function(result) {
        			return_arr 	= result.split('_:_');
        			links	 	= return_arr[0];
        			packages 	= return_arr[1];
        			$div_package.empty();
        			$div_package.append(packages);
        			$div_pagination.find('.pagination').remove();
        			$div_pagination.append(links);
        			//$A.removeAttr('disabled');

        			$form.find('.alert').removeClass('alert-warning').css('padding','').hide();
        			$form.find('.alert center').remove();

        			return true;
		    	});
			}

			function discount($id, $discount) {
				$('#myModalDiscount').modal('show').on("shown.bs.modal", function() {
					$(this).find("form").attr("action", $("html").attr("base-url") + "panel/getDiscountPayment/" + $id);
					$(this).find("[name='discount']").attr("value", $discount);
					$(this).find("[name='old_url']").attr("value", window.location);
				});
			}
			function difference($id,$difference) {
				$('#myModalDifference').modal('show').on("shown.bs.modal", function() {
					$(this).find("form").attr("action", $("html").attr("base-url") + "panel/getDiscountPayment/" + $id);
					$(this).find("[name='difference']").attr("value", $difference);
					$(this).find("[name='old_urls']").attr("value", window.location);
				});
			}

			//dynamic creating package destination
			var counter = 1;

			// append new destination on click
			$('#appendDestination').click(function () {
				$(this).attr("disabled","disabled"); //disable the add more btn
				var destinationsArray = []; //declare an empty destination array

				counter = $('.destinationFormGroup').length!=0 ? parseInt($('.destinationFormGroup').last().attr('counter'))+1 : 1;

				//loop through each destinations
				$("[name='destination[]']").each(function(i,e){
					var tmp = $.trim($(e).val());
					if(tmp.length!=0){
						destinationsArray.push(tmp);
					}
				});

				//send a post query
				$.post("/json/fetchDestinations",{'destinations':destinationsArray},function(data){

					//truncate
					var elems =  '<div class="form-group destinationFormGroup" counter="'+counter+'" id="destinationFromGroup' + counter +'" >';
	                    elems += '	<div class="col-sm-3 control-label" style="padding-top:5px;">';
	                    elems += '		<button onclick="removebutton(this)" counter="' + counter + '" class="btn btn-danger btn-xs" type="button"><span class="fa fa-minus"></span> Remove</button>';
	                    elems += '  </div>';
	                    elems += '	<div class="col-sm-5">';
	                    elems += '		<select class="form-control" name="destination[]">';
	                    elems += '			<option value=" ">- Select -</option>';
	                    elems += 			data;
	                    elems += '		</select>';
	                    elems += '	</div>';
	                    elems += '	<div class="col-sm-3">';
	                    elems += '		<input type="text" class="form-control" name="number_of_nights[]" placeholder="Night(s)" onkeypress="return isNumberKey(event)">';
	                    // elems += '		<input type="text" class="form-control" name="number_of_days[]" placeholder="Night(s)" onkeypress="return isNumberKey(event)">';
	                    elems += '	</div>';
	                    elems += '</div>';

	                //check return
	                if(data=="nein"){
                    	$("#appendDestination").hide();
                    }else{
                    	var additonalDestination = $('#appendFormDestination').append(elems);

                    	//number of days
                    	var option_days   = $('#appendFormDestination').find('[name="number_of_nights[]"]');
						$('#create_package_form, #update_package_form').bootstrapValidator('addField', option_days);

						//destination
						var option_destination   = $('#appendFormDestination').find('[name="destination[]"]');
						$('#create_package_form, #update_package_form').bootstrapValidator('addField', option_destination);

						counter++;
                    }

                    //remove disabled attribute
                 	$('#appendDestination').removeAttr("disabled");
				});
			});

			//to fetch hotels according to destination of package
			$('#show_hotels_btn').click(function () {
				//declare default select element
				var elems 		= '<option value=" ">- Select -</option>';
				var package_id 	= $('#package_id').val();
				var target_modal= $(this).attr('data-target');
				$form = $(target_modal).find('form');

				//send a post query
				$form.find('.alert').html('').hide();
				$.post("/panel/fetchHotels",{'package_id':package_id},function(result){
					//remove older select options (this is to prevent duplicate options)
					$('#addpackagehotels_form [name=hotel_choice] option').remove();
					if (result.alert_type == 'Success') {
						$.each(result.info, function(k, v) {
							if(v.id) elems += '<option value="'+ v.id +'">'+ v.name +'</option>';
						});
						$('#addpackagehotels_form [name=hotel_choice]').append(elems);
					} else $form.find('.alert').html(result.info).show();

					$(target_modal).modal('show');
				}, 'json');
			});

			//remove this destination
			function removebutton(e) {
				var counter = $(e).attr('counter');
				//alert(counter);
				//modified by kirstin because i've added the remove button on package edit
				var counter_destinations = $('.destinationFormGroup').length;
				if (counter_destinations !=1 ) {
					if ($('#destinationFromGroup' + counter).find('select.p_dest').length > 0) {
						if (confirm('This destination is saved in this package. Are you sure you want to delete this?'))
							$('#destinationFromGroup' + counter).remove();
					} else $('#destinationFromGroup' + counter).remove();
				}
			}

			// show / hide fields
			$(document).ready(function() {
				//DYNAMIC RATES
				$('.em-modal i').click(function(){
					$(this).parent().parents('.dynamic_emergency').remove();
				});

				//CHECK RATES
				$('#package_id_').change(function(){
					 var id = this.value;
					$.post(jsite_url('panel/package_hotel_2'),{'id':id}, function(data){
						if(data != ""){
							$('input[name="date_in"]').change(function(){
									var date = $(this).val();
									var added = moment(moment(date).add(parseInt(data - 1),'day')).format('YYYY-MM-DD');
									//$(this).val(added);
									var n = new Date(added);
									n.setDate(n.getDate());
									$('#datetimepickerValidDateCheckOut').data("DateTimePicker").setMinDate(n);
									$('#datetimepickerValidDateCheckOut').data("DateTimePicker").setDate(n);
									$('#datetimepickerValidDateCheckIn').blur();
							});

							if($('input[name="date_in"]').val()){
								var date = $('input[name="date_in"]').val();
								var added = moment(moment(date).add(parseInt(data - 1),'day')).format('YYYY-MM-DD');
								//$(this).val(added);
								var n = new Date(added);
								n.setDate(n.getDate());
								$('#datetimepickerValidDateCheckOut').data("DateTimePicker").setMinDate(n);
								$('#datetimepickerValidDateCheckOut').data("DateTimePicker").setDate(n);
								$('#datetimepickerValidDateCheckIn').blur();
							}else{
								$('input[name="date_in"]').val('');
								$('input[name="date_out"]').val('');
							}
						}
					});
				});

				$('#datetimepickerValidDateCheckIn').datetimepicker({
					minDate:d,
					pickTime: false
				});

				$('#datetimepickerValidDateCheckOut').datetimepicker({
					minDate:d,
					pickTime: false
				});

				//BATCH EDIT PACKAGE
				$('.price_list_').on('click',function(e){
					var link = $(this).attr('href');
					$.get(link , function( data ) {
						$( ".batch-wrap" ).prepend(data);
					});
					e.preventDefault();
				});

				$('.package_batch_edit').on('click',function(e){
					var link 	= 	$(this).attr('href');
					var range 	=	$(this).attr('id');
					// $.get(link , function( data ) {
					// 	$( ".batch-wrap" ).prepend(data);
					// });
					$.post(link, {date_range: range}, function(data){
				        $( ".batch-wrap" ).prepend(data);
				    });
					e.preventDefault();
				});

				$('.package_batch_add').on('click',function(e){
					var link 			= $(this).attr('href');
					$.get(link , function( data ) {
						$( ".batch-wrap" ).prepend(data);
					});
					e.preventDefault();
				});

				$('.package_batch_copy').on('click',function(e){
					var link = $(this).attr('href');
					$.get(link , function( data ) {
						$( ".batch-wrap" ).prepend(data);
					});
					e.preventDefault();
				});

				$('.batch-wrap').on('click','button#batch_close',function(e){
					$('.batch-wrap .batch-modal').remove();
					e.preventDefault();
				});



	            $('#chkProvider').click(function() {
	            	if ($("#chkProvider").is(":not(:checked)")){
	            		if ($("#chkAgent").is(":not(:checked)")){
					   		$('#btnChkResend').attr('disabled','disabled');
					   	}
					   	else{
					   		$('#btnChkResend').removeAttr('disabled');
					   	}
					}
					else {
					   $('#btnChkResend').removeAttr('disabled');
					}
	            });
	            $('#chkAgent').click(function() {
	            	if ($("#chkAgent").is(":not(:checked)")){
	            		if ($("#chkProvider").is(":not(:checked)")){
					   		$('#btnChkResend').attr('disabled','disabled');
					   	}
					   	else{
					   		$('#btnChkResend').removeAttr('disabled');
					   	}
					}
					else {
					   $('#btnChkResend').removeAttr('disabled');
					}
	            });



	            $('#chkCode').click(function() {
	            	if ($("#chkCode").is(":not(:checked)")){
					   //  $('tr').hide();
					   // $('tr.weekly').show();
					   $.get('panel/removeFromSession/code' , function( data ) {});
					   $('.hideCode').hide();
					}
					else {
						$.get('panel/addToSession/code' , function( data ) {});
						$('.hideCode').show();
					}
	            });
	            $('#chkDetails').click(function() {
	            	if ($("#chkDetails").is(":not(:checked)")){
	            		$.get('panel/removeFromSession/details' , function( data ) {});
					   $('.hideDetails').hide();
					}
					else {
						$.get('panel/addToSession/details' , function( data ) {});
						$('.hideDetails').show();
					}
	            });
	            $('#chkDetails').click(function() {
	            	if ($("#chkDetails").is(":not(:checked)")){
	            		$.get('panel/removeFromSession/details' , function( data ) {});
					   $('.hideDetails').hide();
					}
					else {
						$.get('panel/addToSession/details' , function( data ) {});
						$('.hideDetails').show();
					}
	            });
	            $('#chkFlight').click(function() {
	            	if ($("#chkFlight").is(":not(:checked)")){
	            		$.get('panel/removeFromSession/flight' , function( data ) {});
					   $('.hideFlight').hide();
					}
					else {
						$.get('panel/addToSession/flight' , function( data ) {});
						$('.hideFlight').show();
					}
	            });
	            $('#chkAmount').click(function() {
	            	if ($("#chkAmount").is(":not(:checked)")){
	            		$.get('panel/removeFromSession/amount' , function( data ) {});
					   $('.hideAmount').hide();
					}
					else {
						$.get('panel/addToSession/amount' , function( data ) {});
						$('.hideAmount').show();
					}
	            });
	            $('#chkPayment').click(function() {
	            	if ($("#chkPayment").is(":not(:checked)")){
	            		$.get('panel/removeFromSession/payment' , function( data ) {});
					   $('.hidePayment').hide();
					}
					else {
						$.get('panel/addToSession/payment' , function( data ) {});
						$('.hidePayment').show();
					}
	            });
	            $('#chkStatus').click(function() {
	            	if ($("#chkStatus").is(":not(:checked)")){
	            		$.get('panel/removeFromSession/status' , function( data ) {});
					   $('.hideStatus').hide();
					}
					else {
						$.get('panel/addToSession/status' , function( data ) {});
						$('.hideStatus').show();
					}
	            });
	            $('#chkPackage').click(function() {
	            	if ($("#chkPackage").is(":not(:checked)")){ $.get('panel/removeFromSession/package' , function( data ) {});$('.hidePackage').hide(); }
					else { $.get('panel/addToSession/package' , function( data ) {});
						$('.hidePackage').show(); }
	            });
	            $('#chkAgent').click(function() {
	            	if ($("#chkAgent").is(":not(:checked)")){ $.get('panel/removeFromSession/agent' , function( data ) {});$('.hideAgent').hide(); }
					else { $.get('panel/addToSession/agent' , function( data ) {});
						$('.hideAgent').show(); }
	            });
	            $('#chkProvider').click(function() {
	            	if ($("#chkProvider").is(":not(:checked)")){ $.get('panel/removeFromSession/provider' , function( data ) {});$('.hideProvider').hide(); }
					else { $.get('panel/addToSession/provider' , function( data ) {});
						$('.hideProvider').show(); }
	            });
	            $('#chkHotel').click(function() {
	            	if ($("#chkHotel").is(":not(:checked)")){ $.get('panel/removeFromSession/hotel' , function( data ) {});$('.hideHotel').hide(); }
					else { $.get('panel/addToSession/hotel' , function( data ) {});
						$('.hideHotel').show(); }
	            });
	            $('#chkTraveller').click(function() {
	            	if ($("#chkTraveller").is(":not(:checked)")){ $.get('panel/removeFromSession/traveller' , function( data ) {});$('.hideTraveller').hide(); }
					else { $.get('panel/addToSession/traveller' , function( data ) {});
						$('.hideTraveller').show(); }
	            });
	            $('#chkTraveller').click(function() {
	            	if ($("#chkTraveller").is(":not(:checked)")){ $.get('panel/removeFromSession/traveller' , function( data ) {});$('.hideTraveller').hide(); }
					else { $.get('panel/addToSession/traveller' , function( data ) {});
						$('.hideTraveller').show(); }
	            });
	            $('#chkOrigin').click(function() {
	            	if ($("#chkOrigin").is(":not(:checked)")){ $.get('panel/removeFromSession/origin' , function( data ) {});$('.hideOrigin').hide(); }
					else { $.get('panel/addToSession/origin' , function( data ) {});
						$('.hideOrigin').show(); }
	            });
	            $('#chkReturn').click(function() {
	            	if ($("#chkReturn").is(":not(:checked)")){ $.get('panel/removeFromSession/return' , function( data ) {});$('.hideReturn').hide(); }
					else { $.get('panel/addToSession/return' , function( data ) {});
						$('.hideReturn').show(); }
	            });
	            $('#chkCost').click(function() {
	            	if ($("#chkCost").is(":not(:checked)")){ $.get('panel/removeFromSession/cost' , function( data ) {});$('.hideCost').hide(); }
					else { $.get('panel/addToSession/cost' , function( data ) {});
						$('.hideCost').show(); }
	            });
	            $('#chkProfit').click(function() {
	            	if ($("#chkProfit").is(":not(:checked)")){ $.get('panel/removeFromSession/profit' , function( data ) {});$('.hideProfit').hide(); }
					else { $.get('panel/addToSession/profit' , function( data ) {});
						$('.hideProfit').show(); }
	            });
	            $('#chkPrice').click(function() {
	            	if ($("#chkPrice").is(":not(:checked)")){ $.get('panel/removeFromSession/price' , function( data ) {});$('.hidePrice').hide(); }
					else { $.get('panel/addToSession/price' , function( data ) {});
					$('.hidePrice').show(); }
	            });
	            $('#chkAgentPayment').click(function() {
	            	if ($("#chkAgentPayment").is(":not(:checked)")){ $.get('panel/removeFromSession/agent_pay' , function( data ) {});$('.hideAgentPayment').hide(); }
					else { $.get('panel/addToSession/agent_pay' , function( data ) {});
					$('.hideAgentPayment').show(); }
	            });
	            $('#chkProviderPayment').click(function() {
	            	if ($("#chkProviderPayment").is(":not(:checked)")){ $.get('panel/removeFromSession/provider_pay' , function( data ) {});$('.hideProviderPayment').hide(); }
					else { $.get('panel/addToSession/provider_pay' , function( data ) {});
					$('.hideProviderPayment').show(); }
	            });
	        });

			//Dynamic Emergency Phone Number
			var max_fields      = 25; //maximum input boxes allowed
		    var wrapper         = $(".emergency-area"); //Fields wrapper
		    var add_button      = $(".em-addmore"); //Add button ID

		    var x = 1; //initlal text box count
		    $(add_button).click(function(e){ //on add input button click
		        e.preventDefault();
		        if(x < max_fields){ //max input box allowed
		            x++; //text box increment
		            $(wrapper).append('<div class="form-group emergency-append"><div class="col-sm-3 col-sm-offset-3"><input type="text" class="form-control" placeholder="Jane Doe" name="emergency_name[]"></div><div class="col-sm-5 ge-sctop"><i class="fa fa-minus em-remove"></i><input type="text" class="form-control" placeholder="+65123456" name="emergency_phone_number[]"></div></div>'); //add input box
		        }
		    });

		    $(wrapper).on("click",".em-remove", function(e){ //user click on remove text
		        e.preventDefault(); $(this).parent().parent().remove(); x--;
		    });

			// payment radio btn
			$(document).ready(function() {
	            $('#rdnCash').click(function() {
	            	document.getElementById('txtCheque').disabled = true;
	            	document.getElementById('txtOther').disabled = true;
	            });
	            $('#rdnCheque').click(function() {
	            	document.getElementById('txtCheque').disabled = false;
	            	document.getElementById('txtOther').disabled = true;
	            });
	            $('#rdnOther').click(function() {
	            	document.getElementById('txtCheque').disabled = true;
	            	document.getElementById('txtOther').disabled = false;
	            });
	        });

			function ruleType(){
				var rType = document.getElementById('selRuleType').value;
				var rule = ["date_range", "day_of_week", "agent", "blackout"];

				for (i = 0; i < rule.length; i++) {
					if (rType==rule[i]) {
						if (i == "0") {
							document.getElementById(rule[i]+"_1").style.display = '';
							document.getElementById(rule[i]+"_2").style.display = '';
						}
						else if (i == "3") {
							document.getElementById(rule[i]+"_1").style.display = '';
							document.getElementById(rule[i]+"_2").style.display = '';
						}
						else {
							document.getElementById(rule[i]).style.display = '';
						}
					}
					else {
						if (i == "0") {
							document.getElementById(rule[i]+"_1").style.display = 'none';
							document.getElementById(rule[i]+"_2").style.display = 'none';
						}
						else if (i == "3") {
							document.getElementById(rule[i]+"_1").style.display = 'none';
							document.getElementById(rule[i]+"_2").style.display = 'none';
						}
						else {
							document.getElementById(rule[i]).style.display = 'none';
						}
					}
				}
			}
			function ruleType2(){
				var rType = document.getElementById('selRuleType2').value;
				var rule = ["date_range", "day_of_week", "agent", "blackout"];

				for (i = 0; i < rule.length; i++) {
					if (rType==rule[i]) {
						if (i == "0") {
							document.getElementById(rule[i]+"2_1").style.display = '';
							document.getElementById(rule[i]+"2_2").style.display = '';
						}
						else if (i == "3") {
							document.getElementById(rule[i]+"2_1").style.display = '';
							document.getElementById(rule[i]+"2_2").style.display = '';
						}
						else {
							document.getElementById(rule[i]+"2").style.display = '';
						}
					}
					else {
						if (i == "0") {
							document.getElementById(rule[i]+"2_1").style.display = 'none';
							document.getElementById(rule[i]+"2_2").style.display = 'none';
						}
						else if (i == "3") {
							document.getElementById(rule[i]+"2_1").style.display = 'none';
							document.getElementById(rule[i]+"2_2").style.display = 'none';
						}
						else {
							document.getElementById(rule[i]+"2").style.display = 'none';
						}
					}
				}
			}

			var substringMatcher = function(strs) {
				return function findMatches(q, cb) {
					var matches, substrRegex;
					matches = [];

					substrRegex = new RegExp(q, 'i');

					$.each(strs, function(i, str) {
						if (substrRegex.test(str)) {
							matches.push({ value: str });
						}
					});
					cb(matches);
				};
			};

			//date picker setting

			$(function () {
				for (i = 1; i <= 3; i++) {
					$('#datetimepickerDOB'+i).datetimepicker({
						pickTime: false
					});
				}
			});

			// alternative email validator
			function validateEmail(field) {
			    var regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,5}$/;
			    return (regex.test(field)) ? true : false;
			}
			function validateMultipleEmailsCommaSeparated(emailcntl, seperator) {
			    var value = emailcntl.value;
			    if (value != '') {
			        var result = value.split(seperator);
			        // alert(result.length);
			        for (var i = 0; i < result.length; i++) {
			        	var trim_result = result[i].trim();
			            if (trim_result != '') {
			                if (!validateEmail(trim_result)) {
			                    emailcntl.focus();
			                    alert('Please check, `' + trim_result + '` email addresses not valid!');
			                    return false;
			                }
			            }
			        }
			    }
			    return true;
			}

			$(function () {
				$('#datetimepickerDepart').datetimepicker({
					minDate:d,
					pickTime: false
				});

				$('#datetimepickerReturn').datetimepicker({
					pickTime: false
				});

				$("#datetimepickerDepart").on("dp.change",function (e) {

					// select new date
					var n = new Date(e.date);
					n.setDate(n.getDate() + 0);

					$('#datetimepickerReturn').data("DateTimePicker").setMinDate(n);
					$('#datetimepickerReturn').data("DateTimePicker").setDate(n);
				});

				//Additional Hotel Booking
				$('#datepicker_checkin2').datetimepicker({
					minDate:d,
					pickTime: false
				});

				$('#datepicker_checkout2').datetimepicker({
					pickTime: false
				});

				$("#datepicker_checkin2").on("dp.change",function (e) {

					// select new date
					var n = new Date(e.date);
					n.setDate(n.getDate() + 0);

					$('#datepicker_checkout2').data("DateTimePicker").setMinDate(n);
					$('#datepicker_checkout2').data("DateTimePicker").setDate(n);
				});

				$('#datepicker_checkin3').datetimepicker({
					minDate:d,
					pickTime: false
				});

				$('#datepicker_checkout3').datetimepicker({
					pickTime: false
				});

				$("#datepicker_checkin3").on("dp.change",function (e) {

					// select new date
					var n = new Date(e.date);
					n.setDate(n.getDate() + 0);

					$('#datepicker_checkout3').data("DateTimePicker").setMinDate(n);
					$('#datepicker_checkout3').data("DateTimePicker").setDate(n);
				});

				//create_booking part
				$('#datetimepickerCreateBookingDepart').datetimepicker({
					minDate:d,
					pickTime: false
				});
				$('#datetimepickerCreateBookingReturn').datetimepicker({
					pickTime: false
				});

				$("#datetimepickerCreateBookingDepart").on("dp.change",function (e) {
					day = parseInt($('#package_days').val(),10);
					// select new datef
					var n = new Date(e.date);
					n.setDate(n.getDate());

					$('#datetimepickerCreateBookingReturn').data("DateTimePicker").setMinDate(n);
					$('#datetimepickerCreateBookingReturn').data("DateTimePicker").setDate(n);

					$('#datetimepickerCreateBookingDepart').blur();
				});

				$("#datetimepickerCreateBookingReturn").on("dp.change",function (e) {
					$('#datetimepickerCreateBookingReturn').blur();
				});

				//step_booking/step_hotel part
				$('#datetimepickerBookingDepart').datetimepicker({
					minDate:d,
					pickTime: false
				});

				$('#datetimepickerBookingReturn').datetimepicker({
					pickTime: false
				});

				$("#datetimepickerBookingDepart").on("dp.change",function (e) {
					day = parseInt($('#package_days').val()-1,10);
					// select new date
					var n = new Date(e.date);
					n.setDate(n.getDate() + day);

					$('#datetimepickerBookingReturn').data("DateTimePicker").setMinDate(n);
					$('#datetimepickerBookingReturn').data("DateTimePicker").setDate(n);
					$('#stephotel_form').bootstrapValidator('revalidateField', 'depart_date');

					//update the package, hotel and room surcharges ----------------------------------------------
					updatePackageSurcharges();
					listHotelSurcharges();

					$("[name='package_hotel_choice[]']").each(function(i,e){
						var destination_id = $(e).attr("destination-id");
						updateRoomRatesContainers(destination_id);
					});
					$('#datetimepickerBookingDepart').blur();
				});

				$("#datetimepickerBookingReturn").on("dp.change",function (e) {
					$('#stephotel_form').bootstrapValidator('revalidateField', 'return_date');
					day = parseInt($('#package_days').val()-1,10);

					var r = new Date(e.date);
					var d = new Date($('#datetimepickerBookingDepart').data("DateTimePicker").date);

					var extension = Math.floor(((((r).getTime() - (d).getTime())/60/60/24)/1000)) - day;

					if(extension!=0 || extension>0){
						$('.extension_container:last').show();
						$('#label_extended_days_container_wrapper').show();
						$("#label_extended_days_container").empty().append(extension);
					}else{
						$('.extension_container').hide();
						$("#label_extended_days_container_wrapper").hide();
					}

					//set the extended days ----------------------------------------------
					$('#extended_days').val(extension);

					//updatet the package, hotel and room surcharges ----------------------------------------------
					updatePackageSurcharges();
					listHotelSurcharges();

					$("[name='package_hotel_choice[]']").each(function(i,e){
						var destination_id = $(e).attr("destination-id");
						updateRoomRatesContainers(destination_id);
					});
					$('#datetimepickerBookingReturn').blur();
				});

				$('#datetimepickerCheckIn').datetimepicker({
					minDate:d,
					pickTime: false
				});

				$('#datetimepickerCheckOut').datetimepicker({
					pickTime: false
				});

				$("#datetimepickerCheckIn").on("dp.change",function (e) {

					// select new date
					var n = new Date(e.date);
					n.setDate(n.getDate() + 3);

					$('#datetimepickerCheckOut').data("DateTimePicker").setMinDate(n);
					$('#datetimepickerCheckOut').data("DateTimePicker").setDate(n);
					$('#stephotel_form').bootstrapValidator('revalidateField', 'exhotel_checkin');
				});

				$("#datetimepickerCheckOut").on("dp.change",function (e) {
					$('#stephotel_form').bootstrapValidator('revalidateField', 'exhotel_checkout');
				});

				//step_detail part --- Date and Time
				$('#datetimepickerDepartTime').datetimepicker({
					minDate:d,
					pickTime: true
				});

				$('#datetimepickerReturnTime').datetimepicker({
					pickTime: true
				});

				$("#datetimepickerDepartTime").on("dp.change",function (e) {

					// select new date
					var n = new Date(e.date);
					n.setDate(n.getDate() + 3);

					$('#datetimepickerReturnTime').data("DateTimePicker").setMinDate(n);
					$('#datetimepickerReturnTime').data("DateTimePicker").setDate(n);
				});

				//step_detail part --- Time only
				$('.datetimepickerTimeOnly').datetimepicker({
					pickDate: false,
					pickTime: true
				});

				$('#datetimepickerValidDateFrom').datetimepicker({
					minDate:d,
					pickTime: false
				});

				$('#datetimepickerValidDateUntil').datetimepicker({
					pickTime: false
				});

				$("#datetimepickerValidDateFrom").on("dp.change",function (e) {

					// select new date
					var n = new Date(e.date);
					n.setDate(n.getDate());

					$('#datetimepickerValidDateUntil').data("DateTimePicker").setMinDate(n);
					$('#datetimepickerValidDateUntil').data("DateTimePicker").setDate(n);
					$('#create_package_form, #update_package_form').bootstrapValidator('revalidateField', 'valid_date_from');

					$('#datetimepickerValidDateFrom').blur();
				});

				//Check Rate
				$('#datetimepickerValidDateCheckIn, #datetimepickerValidDateCheckOut').datetimepicker({
					//minDate:d,
					pickTime: false
				});

				$("#datetimepickerValidDateCheckIn").on("dp.change",function (e) {

					// select new date
					var n = new Date(e.date);
					n.setDate(n.getDate());

					$('#datetimepickerValidDateCheckOut').data("DateTimePicker").setMinDate(n);
					$('#datetimepickerValidDateCheckOut').data("DateTimePicker").setDate(n);
					$('#datetimepickerValidDateCheckIn').blur();
				});

				$("#datetimepickerValidDateCheckOut").on("dp.change",function (e) {
					$('#datetimepickerValidDateCheckOut').blur();
				});

				//DOB
				$('#datetimepickerDOB').datetimepicker({
					maxDate:d,
					pickTime: false
				});

				$('#datetimepickerDOBAdult').datetimepicker({
	              maxDate:d,
	              pickTime: false
	            });

	            $('#datetimepickerDOBChild').datetimepicker({
	              maxDate:d,
	              pickTime: false
	            });

	            $('#datetimepickerDOBInfant').datetimepicker({
	              maxDate:d,
	              pickTime: false
	            });

				$('#datetimepickerDOB').change(function(){
					$('#admin_register').bootstrapValidator('revalidateField', 'dob');
				});

				var tmp_dt_picker = new Date();
				var d_dt_picker_final = tmp_dt_picker.setDate(tmp_dt_picker.getDate()-4745);
				var ddddd = new Date(d_dt_picker_final);

				$('.datetimepickerDOB_class').datetimepicker({
					maxDate : ddddd.getFullYear()+"-"+ (ddddd.getMonth()+1) +"-"+ddddd.getDate(),
					defaultDate : ddddd.getFullYear()+"-"+ (ddddd.getMonth()+1) +"-"+ddddd.getDate(),
					pickTime: false
				});

				var tmp_dt_picker_c = new Date();
				var d_dt_picker_final_c = tmp_dt_picker_c.setDate(tmp_dt_picker_c.getDate()-1095);
				var ddddd_c = new Date(d_dt_picker_final_c);

				$('.datetimepickerDOB_class_child').datetimepicker({
					maxDate : ddddd_c.getFullYear()+"-"+ (ddddd_c.getMonth()+1) +"-"+ddddd_c.getDate(),
					defaultDate : ddddd_c.getFullYear()+"-"+ (ddddd_c.getMonth()+1) +"-"+ddddd_c.getDate(),
					pickTime: false
				});


				var tmp_dt_picker_i = new Date();
				var d_dt_picker_final_i = tmp_dt_picker_i.setDate(tmp_dt_picker_i.getDate());
				var ddddd_i = new Date(d_dt_picker_final_i);


				$('.datetimepickerDOB_class_infant').datetimepicker({
					maxDate : ddddd_i.getFullYear()+"-"+ (ddddd_i.getMonth()+1) +"-"+ddddd_i.getDate(),
					defaultDate : ddddd_i.getFullYear()+"-"+ (ddddd_i.getMonth()+1) +"-"+ddddd_i.getDate(),
					pickTime: false
				});


				$('#datetimepickerMonthView').datetimepicker({
					maxDate:d,
					format: "yyyy",
					viewMode: "months",
					minViewMode: "months",
					pickTime: false
				});

				$('#datetimepickerMonthViewDate').datetimepicker({
					format: "yyyy",
					viewMode: "months",
					minViewMode: "months",
					pickTime: false
				});

				$('#datetimepickerMonthViewOrigin').datetimepicker({
					format: "yyyy",
					viewMode: "months",
					minViewMode: "months",
					pickTime: false
				});

				$('#datetimepickerMonthViewDepart').datetimepicker({
					format: "yyyy",
					viewMode: "months",
					minViewMode: "months",
					pickTime: false
				});

				$("#datetimepickerMonthViewOrigin").on("dp.change",function (e) {

					// select new date
					var n = new Date(e.date);
					n.setDate(n.getDate());

					$('#datetimepickerMonthViewDepart').data("DateTimePicker").setMinDate(n);
					$('#datetimepickerMonthViewDepart').data("DateTimePicker").setDate(n);
				});


				$('.datetimepickerMonthViewPendingPayment').datetimepicker({
					format: " yyyy",
					viewMode: "months",
					minViewMode: "months",
					pickTime: false
				});

				$(".datetimepickerMonthViewPendingPayment").on("dp.change",function (e) {
					// select new date
					var n = new Date(e.date);
					var month = new Array();
					month[0] = "January";
					month[1] = "February";
					month[2] = "March";
					month[3] = "April";
					month[4] = "May";
					month[5] = "June";
					month[6] = "July";
					month[7] = "August";
					month[8] = "September";
					month[9] = "October";
					month[10] = "November";
					month[11] = "December";
					var m = month[n.getMonth()];
					var y = n.getFullYear();

					var mo = new Array();
					mo[0] = "01";
					mo[1] = "02";
					mo[2] = "03";
					mo[3] = "04";
					mo[4] = "05";
					mo[5] = "06";
					mo[6] = "07";
					mo[7] = "08";
					mo[8] = "09";
					mo[9] = "10";
					mo[10] = "11";
					mo[11] = "12";
					var mm = mo[n.getMonth()];

					//alert(y + "-" + mm);
					$('.viewPendingDate').html(m + " " + y);
					$('.tr-hide').hide();
					$('.'+y + "-" + mm).show();
					$('.'+y + "" + mm + ":last").show();
					var cnt_tr = $('.tr-hide:visible').length;
					console.log(cnt_tr);

					if (cnt_tr==0) {
						$('.payment_tab_btn').hide();
					} else {
						$('.payment_tab_btn').show();
					}
				});
			});



			//provides list of companies from the database.
			//this is just for the meantime
			//by Kirstin
			// $(function () {
			// 	if(jQuery('#json_value_dd').length > 0) {
			// 		companies = JSON.parse(jQuery('#json_value_dd').val());
			// 		$('#typeahead_manage, #typeahead_edit, #typeahead_profile').typeahead({
			// 			hint: true,
			// 			highlight: true,
			// 			minLength: 1
			// 		},
			// 		{
			// 			name: 'company_name',
			// 			displayKey: 'value',
			// 			source: substringMatcher(companies)
			// 		});
			// 	}
			// 	// if(jQuery('#json_airport_codes').length > 0) {
			// 	// 	companies = JSON.parse(jQuery('#json_airport_codes').val());
			// 	// 	$('#typeahead_of_from, #typeahead_rf_from, #typeahead_of_to, #typeahead_rf_to').typeahead({
			// 	// 		hint: true,
			// 	// 		highlight: true,
			// 	// 		minLength: 1
			// 	// 	},
			// 	// 	{
			// 	// 		name: 'airport_name',
			// 	// 		displayKey: 'value',
			// 	// 		source: substringMatcher(companies)
			// 	// 	});
			// 	// }
			// });

			//bootstrapValidator
			$(document).ready(function() {
				// $('#stephotel_form :input').change(function(){
				// 	var select_hotel = $('.additional_select_hotel option:selected').attr('hotel-name');
				// 	var reg_hotel2    = $('#add_hotel_name2').val();
				// 	var reg_hotel3    = $('#add_hotel_name3').val();
				// 	if($.trim($('#add_hotel_name2').val()).length != 0 || $.trim($('#add_hotel_name2').val()).length != 0){
				// 		if(reg_hotel2 != select_hotel || reg_hotel2 != reg_hotel3){
				// 			$('#stepbook_submit').attr('disabled','disabled');
				// 		}else{
				// 			$('#stepbook_submit').removeAttr('disabled');
				// 		}
				// 	}else{
				// 		$('#stepbook_submit').removeAttr('disabled');
				// 	}

				// 	if($.trim($('#add_hotel_name3').val()).length != 0 || $.trim($('#add_hotel_name3').val()).length != 0){
				// 		if(reg_hotel3 != select_hotel || reg_hotel3 != reg_hotel2){
				// 			$('#stepbook_submit').attr('disabled','disabled');
				// 		}else{
				// 			$('#stepbook_submit').removeAttr('disabled');
				// 		}
				// 	}else{
				// 		$('#stepbook_submit').removeAttr('disabled');
				// 	}
				// });

				/*$('#add_hotel_name2').focusout(function(){
					var select_hotel = $('.additional_select_hotel option:selected').attr('hotel-name');
					var opt_hotel    = $('#add_hotel_name3').val();
					var reg_hotel    = $(this).val();
					if($.trim($('#add_hotel_name2').val()).length != 0 || $.trim($('#add_hotel_name2').val()).length != 0){
						if(reg_hotel == select_hotel || reg_hotel == opt_hotel){
							$(this).parent('td').addClass('has-feedback');
							$(this).parent('td').addClass('has-error');
							$(this).parent('td').find('i').removeClass('glyphicon-ok');
							$(this).parent('td').find('i').addClass('glyphicon-remove');
							$(this).parent('td').find('i').css('display','block');
							$('#stepbook_submit').attr('disabled','disabled');
						}else{
							$(this).parent('td').find('i').css('display','block');
							$(this).parent('td').removeClass('has-error');
							$(this).parent('td').addClass('has-feedback');
							$(this).parent('td').addClass('has-success');
							$(this).parent('td').find('i').removeClass('glyphicon-remove');
							$(this).parent('td').find('i').addClass('glyphicon-ok');
							$('#datepicker_checkin2').removeAttr('disabled');
							$('#datepicker_checkout2').removeAttr('disabled');
							$('#stepbook_submit').removeAttr('disabled');
						}
					}else{
						$(this).parent('td').removeClass('has-feedback');
						$(this).parent('td').removeClass('has-error');
						$(this).parent('td').find('i').css('display','none');
						$(this).parent('td').removeClass('has-success');
						$('#stepbook_submit').removeAttr('disabled');
					}
				});*/

				$('#add_hotel_name2').keyup(function(){
					var select_hotel = $('.additional_select_hotel option:selected').attr('hotel-name');
					var opt_hotel    = $('#add_hotel_name3').val();
					var reg_hotel    = $(this).val();
					if(reg_hotel != select_hotel || reg_hotel != opt_hotel){
						/*$(this).parent('td').removeClass('has-feedback');
						$(this).parent('td').removeClass('has-error');
						$(this).parent('td').find('i').css('display','none');
						$(this).parent('td').removeClass('has-success');*/
						if($.trim($('#add_hotel_name2').val()).length != 0 || $.trim($('#add_hotel_name2').val()).length != 0){
							$('#datepicker_checkin2').removeAttr('disabled');
							$('#datepicker_checkout2').removeAttr('disabled');
						}else{
							$('#datepicker_checkin2').attr('disabled','disabled');
							$('#datepicker_checkout2').attr('disabled','disabled');
							$('#datepicker_checkin2').val('');
							$('#datepicker_checkout2').val('');
						}
					}
				});

				$('#add_hotel_name3').keyup(function(){
					var select_hotel = $('.additional_select_hotel option:selected').attr('hotel-name');
					var opt_hotel    = $('#add_hotel_name2').val();
					var reg_hotel    = $(this).val();
					if(reg_hotel != select_hotel || reg_hotel != opt_hotel){
						$(this).parent('td').removeClass('has-feedback');
						$(this).parent('td').removeClass('has-error');
						$(this).parent('td').find('i').css('display','none');
						$(this).parent('td').removeClass('has-success');
						if($.trim($('#add_hotel_name3').val()).length != 0 || $.trim($('#add_hotel_name3').val()).length != 0){
							$('#datepicker_checkin3').removeAttr('disabled');
							$('#datepicker_checkout3').removeAttr('disabled');
						}else{
							$('#datepicker_checkin3').attr('disabled','disabled');
							$('#datepicker_checkout3').attr('disabled','disabled');
							$('#datepicker_checkin3').val('');
							$('#datepicker_checkout3').val('');
						}
					}
				});


				/*$('#add_hotel_name3').focusout(function(){
					var select_hotel = $('.additional_select_hotel option:selected').attr('hotel-name');
					var opt_hotel    = $('#add_hotel_name2').val();
					var reg_hotel    = $(this).val();
					if($.trim($('#add_hotel_name3').val()).length != 0 || $.trim($('#add_hotel_name3').val()).length != 0){
						if(reg_hotel == select_hotel || reg_hotel == opt_hotel){
							$(this).parent('td').addClass('has-feedback');
							$(this).parent('td').addClass('has-error');
							$(this).parent('td').find('i').removeClass('glyphicon-ok');
							$(this).parent('td').find('i').addClass('glyphicon-remove');
							$(this).parent('td').find('i').css('display','block');
							$('#stepbook_submit').attr('disabled','disabled');
						}else{
							$(this).parent('td').find('i').css('display','block');
							$(this).parent('td').removeClass('has-error');
							$(this).parent('td').addClass('has-feedback');
							$(this).parent('td').addClass('has-success');
							$(this).parent('td').find('i').removeClass('glyphicon-remove');
							$(this).parent('td').find('i').addClass('glyphicon-ok');
							$('#datepicker_checkin3').removeAttr('disabled');
							$('#datepicker_checkout3').removeAttr('disabled');
							$('#stepbook_submit').removeAttr('disabled');
						}
					}else{
						$(this).parent('td').removeClass('has-feedback');
						$(this).parent('td').removeClass('has-error');
						$(this).parent('td').find('i').css('display','none');
						$(this).parent('td').removeClass('has-success');
					}
				});*/



				/*$('#stepbook_submit').mouseenter(function(e){
					var datepicker_checkin2  = $('#datepicker_checkin2').val();
					var datepicker_checkout2 = $('#datepicker_checkout2').val();
					var datepicker_checkin3  = $('#datepicker_checkin3').val();
					var datepicker_checkout3 = $('#datepicker_checkout3').val();
					if($.trim(datepicker_checkin2).length != 0 || $.trim(datepicker_checkout2).length != 0){
						$('#add_hotel_name2').parent('td').addClass('has-feedback');
						$('#add_hotel_name2').parent('td').addClass('has-error');
						$('#add_hotel_name2').parent('td').find('i').removeClass('glyphicon-ok');
						$('#add_hotel_name2').parent('td').find('i').addClass('glyphicon-remove');
						$('#add_hotel_name2').parent('td').find('i').css('display','block');
						$('#datepicker_checkin2').removeAttr('disabled');
						$('#datepicker_checkout2').removeAttr('disabled');
					}else{
						$('#add_hotel_name2').parent('td').removeClass('has-feedback');
						$('#add_hotel_name2').parent('td').removeClass('has-error');
						$('#add_hotel_name2').parent('td').find('i').css('display','none');
						$('#add_hotel_name2').parent('td').removeClass('has-success');
					}

					if($.trim(datepicker_checkin3).length != 0 || $.trim(datepicker_checkout3).length != 0){
						$('#add_hotel_name3').parent('td').addClass('has-feedback');
						$('#add_hotel_name3').parent('td').addClass('has-error');
						$('#add_hotel_name3').parent('td').find('i').removeClass('glyphicon-ok');
						$('#add_hotel_name3').parent('td').find('i').addClass('glyphicon-remove');
						$('#add_hotel_name3').parent('td').find('i').css('display','block');
						$('#datepicker_checkin3').removeAttr('disabled');
						$('#datepicker_checkout3').removeAttr('disabled');
					}else{
						$('#add_hotel_name3').parent('td').removeClass('has-feedback');
						$('#add_hotel_name3').parent('td').removeClass('has-error');
						$('#add_hotel_name3').parent('td').find('i').css('display','none');
						$('#add_hotel_name3').parent('td').removeClass('has-success');
						$('#stepbook_submit').attr('disabled','disabled');
					}
				});*/



				$('#datepicker_checkout2').focusout(function(){
					if($.trim($('#add_hotel_name2').val()).length == 0){
						$('#datepicker_checkin2').val('');
						$('#datepicker_checkout2').val('');
						$('#datepicker_checkin2').attr('disabled','disabled');
						$('#datepicker_checkout2').attr('disabled','disabled');
					}
				});
				$('#datepicker_checkout3').focusout(function(){
					if($.trim($('#add_hotel_name3').val()).length == 0){
						$('#datepicker_checkin3').val('');
						$('#datepicker_checkout3').val('');
						$('#datepicker_checkin3').attr('disabled','disabled');
						$('#datepicker_checkout3').attr('disabled','disabled');
					}
				});
				$('#datepicker_checkin2').focusout(function(){
					if($.trim($('#add_hotel_name2').val()).length == 0){
						$('#datepicker_checkin2').val('');
						$('#datepicker_checkout2').val('');
					}
				});
				$('#datepicker_checkin3').focusout(function(){
					if($.trim($('#add_hotel_name3').val()).length == 0){
						$('#datepicker_checkin3').val('');
						$('#datepicker_checkout3').val('');
					}
				});

				//register or create user
				$('#contact_form')
				.bootstrapValidator({
					message: 'This value is not valid',
					//live: 'submitted',
					feedbackIcons: {
						valid: 'glyphicon glyphicon-ok',
						invalid: 'glyphicon glyphicon-remove',
						validating: 'glyphicon glyphicon-refresh'
					},
					fields: {
						email: {
							validators: {
								notEmpty: {
									message: 'Email Address is required and can\'t be empty'
								},
								emailAddress: {
									message: 'The input is not a valid email address'
								}
							}
						},
						subject: {
							message: 'The subject is not valid',
							validators: {
								notEmpty: {
									message: 'Subject is required and can\'t be empty'
								}
							}
						},
						message: {
							message: 'The message is not valid',
							validators: {
								notEmpty: {
									message: 'Your message is required and can\'t be empty'
								}
							}
						}
					}
				});

				//register or create user
				$('#admin_register')
				.bootstrapValidator({
					message: 'This value is not valid',
					//live: 'submitted',
					feedbackIcons: {
						valid: 'glyphicon glyphicon-ok',
						invalid: 'glyphicon glyphicon-remove',
						validating: 'glyphicon glyphicon-refresh'
					},
					fields: {
						salutation: {
							message: 'The salutation is not valid',
							validators: {
								notEmpty: {
									message: 'The salutation is required and can\'t be empty'
								}
							}
						},
						user_type: {
							message: 'The type is not valid',
							validators: {
								notEmpty: {
									message: 'Type is required and can\'t be empty'
								}
							}
						},
						firstname: {
							message: 'The firstname is not valid',
							validators: {
								notEmpty: {
									message: 'The first name is required and can\'t be empty'
								},
								regexp: {
									regexp: /^([a-z0-9]*[a-z]){2}[a-zA-Z0-9 '.-]*$/i,
									message: 'The first name can only consist of letters and some characters(. -\')'
								}
							}
						},
						middlename: {
							message: 'The middlename is not valid',
							validators: {
								regexp: {
									regexp: /^[a-zA-Z0-9 '.-]*$/i,
									message: 'The middle name can only consist of letters and some characters(. -\')'
								}
							}
						},
						lastname: {
							message: 'The lastname is not valid',
							validators: {
								notEmpty: {
									message: 'The lastname is required and can\'t be empty'
								},
								regexp: {
									regexp: /^([a-z0-9]*[a-z]){2}[a-zA-Z0-9 '.-]*$/i,
									message: 'The lastname can only consist of letters and some characters(. -\')'
								}
							}
						},
						username: {
							message: 'The lastname is not valid',
							validators: {
								notEmpty: {
									message: 'The username is required and can\'t be empty'
								},
								regexp: {
									regexp: /^([a-z0-9]*[a-z]){2}[a-zA-Z0-9 '.-]*$/i,
									message: 'The username can only consist of letters and some characters(. -\')'
								}
							}
						},
						dob: {
							message: 'The date is not valid',
							validators: {
								notEmpty: {
									message: 'The date of birth is required and can\'t be empty'
								},
								date: {
									format: 'YYYY-MM-DD',
									message: 'Invalid Date. Please follow the correct format (YYYY-MM-DD).'
								}
							}
						},
						company: {
							message: 'The company is not valid',
							validators: {
								customCompany: {
									message: 'Company is required (for non-customer users)',
									check_only_for: 'admin,agent,provider,account,operator',
									field: 'user_type'
								}
							}
						},
						phone_number: {
							message: 'The phone number is not valid',
							validators: {
								notEmpty: {
									message: 'Phone number is required and can\'t be empty'
								},
								stringLength: {
									min: 6,
									message: 'Your phone number must be at least 6 characters short'
								},
								regexp: {
									regexp: /^[0-9 ()+\-]+$/i,
									message: 'Enter the correct number format'
								}
							}
						},
						emergency_phone_number: {
							message: 'The emergency phone number is not valid',
							validators: {
								notEmpty: {
									message: 'Emergency Phone number is required and can\'t be empty'
								},
								stringLength: {
									min: 6,
									message: 'Your emergency phone number must be at least 6 characters short'
								},
								regexp: {
									regexp: /^[0-9 ()+\-]+$/i,
									message: 'Enter the correct number format'
								}
							}
						},
						email: {
							validators: {
								notEmpty: {
									message: 'The email address is required and can\'t be empty'
								},
								emailAddress: {
									message: 'The input is not a valid email address'
								}
							}
						},
						country: {
							message: 'The country is not valid',
							validators: {
								notEmpty: {
									message: 'Country is required and can\'t be empty'
								}
							}
						},
						password: {
							validators: {
								notEmpty: {
									message: 'The password is required and can\'t be empty'
								},
								stringLength: {
									min: 8,
									message: 'your password must be at least 8 characters short'
								},
								/*regexp: {
									regexp: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/i,
									message: 'Your password must contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character'
								}*/
							}
						},
						confirm_password: {
							validators: {
								notEmpty: {
									message: 'The password is required and can\'t be empty'
								},
								identical: {
									field: 'password',
									message: 'The passwords do not match'
								}
							}
						}
					}
				})
				.on('success.form.bv', function(e) {
					e.preventDefault();
					var $form = $(e.target);
					var bv = $form.data('bootstrapValidator');
					$.ajax({
				 	 	url: $form.attr('action'),
				 	 	data: $form.serialize(),
				 	 	type: 'POST',
						async : false,
				 	 	dataType: 'json',
					 	beforeSend: function(){
					 		loader = "<?php echo base_url('./img/loader.gif');?>";
					 		$form.find('button.btn').hide();
					 		$form.find('button').after('<img id="loader" height="50px" src="'+ loader +'">');
					 	},
					 	success: function(result){
					 		$form.find('button.btn').show();
					 		$form.find('#loader').remove();
							$('p#myModalLabel').html(result.message);
							$('#myModalSuccess').modal('show');
							if(result.alert_type == 'Success'){
								type_id = '#'+$form.find('[name=user_type]').val()+'s';
								$form.data('bootstrapValidator').resetForm(true);
								$(type_id+' table.table > tbody:last').append('<tr class="tr'+ result.userdata.user_id +'" user_id="'+ result.userdata.user_id +'" ><td class=".td'+ result.userdata.user_id +'"><a href="#" data-target="#myModalEditUser" class="edit_link">Edit</a> | <a href="#" class="'+ result.userdata.user_id +'DeLeTeactivateDeLeTe'+ result.userdata.company +'" onclick ="updateStatus(this);">Activate</a> | <a href="#" class="'+ result.userdata.user_id +'DeLeTedeleteDeLeTe'+ result.userdata.company +'" onclick ="updateStatus(this);">Delete</a>  </td> <td>'+result.userdata.lastname+', '+result.userdata.firstname+'</td> <td>'+result.userdata.company+'</td> <td>'+result.userdata.email+'</td></tr>');
								$('#accounts table.table > tbody:last').append('<tr class="'+ result.userdata.user_id +'" user_id="'+ result.userdata.user_id +'" ><td class=".td'+ result.userdata.user_id +'"><a href="#" data-target="#myModalEditUser" class="edit_link">Edit</a> | <a href="#" class="'+ result.userdata.user_id +'DeLeTeactivateDeLeTe'+ result.userdata.company +'" onclick ="updateStatus(this);">Activate</a> | <a href="#" class="'+ result.userdata.user_id +'DeLeTedeleteDeLeTe'+ result.userdata.company +'" onclick ="updateStatus(this);">Delete</a>  </td> <td>'+result.userdata.lastname+', '+result.userdata.firstname+'</td> <td>'+result.userdata.company+'</td> <td>'+result.userdata.email+'</td></tr>');
								$('a[href="'+type_id+'"]').tab('show');
								scrollUp();
							}
					  	}
					});
				});

				//for myModalProfile - OWN USER
				$('#profile_form')
				.bootstrapValidator({
					message: 'This value is not valid',
					feedbackIcons: {
						valid: 'glyphicon glyphicon-ok',
						invalid: 'glyphicon glyphicon-remove',
						validating: 'glyphicon glyphicon-refresh'
					},
					fields: {
						salutation: {
							message: 'The salutation is not valid',
							validators: {
								notEmpty: {
									message: 'The salutation is required and can\'t be empty'
								}
							}
						},
						firstname: {
							message: 'The firstname is not valid',
							validators: {
								notEmpty: {
									message: 'The first name is required and can\'t be empty'
								},
								regexp: {
									regexp: /^([a-z0-9]*[a-z]){2}[a-zA-Z0-9 '.-]*$/i,
									message: 'The first name can only consist of letters and some characters(. -\')'
								}
							}
						},
						middlename: {
							message: 'The middlename is not valid',
							validators: {
								regexp: {
									regexp: /^[a-zA-Z0-9 '.-]*$/i,
									message: 'The middle name can only consist of letters and some characters(. -\')'
								}
							}
						},
						lastname: {
							message: 'The lastname is not valid',
							validators: {
								notEmpty: {
									message: 'The lastname is required and can\'t be empty'
								},
								regexp: {
									regexp: /^([a-z0-9]*[a-z]){2}[a-zA-Z0-9 '.-]*$/i,
									message: 'The lastname can only consist of letters and some characters(. -\')'
								}
							}
						},
						username: {
							message: 'The lastname is not valid',
							validators: {
								notEmpty: {
									message: 'The username is required and can\'t be empty'
								},
								regexp: {
									regexp: /^([a-z0-9]*[a-z]){2}[a-zA-Z0-9 '.-]*$/i,
									message: 'The username can only consist of letters and some characters(. -\')'
								}
							}
						},
						dob: {
							message: 'The date is not valid',
							validators: {
								notEmpty: {
									message: 'The date of birth is required and can\'t be empty'
								},
								date: {
									format: 'YYYY-MM-DD',
									message: 'Invalid Date. Please follow the correct format (YYYY-MM-DD).'
								}
							}
						},
						company: {
							message: 'The company is not valid',
							validators: {
								notEmpty: {
									message: 'The date of birth is required and can\'t be empty'
								}
							}
						},
						// customCompany: {
						//   message: 'Company is required (for non-customer users)',
						//   check_only_for: 'admin,agent,provider,account,operator',
						//   field: 'user_type'
						// },
						phone_number: {
							message: 'The phone number is not valid',
							validators: {
								notEmpty: {
									message: 'Phone number is required and can\'t be empty'
								},
								stringLength: {
									min: 6,
									message: 'Your phone number must be at least 6 characters short'
								},
								regexp: {
									regexp: /^[0-9 ()+\-]+$/i,
									message: 'Enter the correct number format'
								}
							}
						},
						email: {
							validators: {
								notEmpty: {
									message: 'The email address is required and can\'t be empty'
								},
								emailAddress: {
									message: 'The input is not a valid email address'
								}
							}
						},
						country: {
							message: 'The country is not valid',
							validators: {
								notEmpty: {
									message: 'Country is required and can\'t be empty'
								}
							}
						},
						old_password: {
							validators: {
								stringLength: {
									min: 8,
									message: 'your password must be at least 8 characters short'
								}
								// regexp: {
								// 	regexp: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/i,
								// 	message: 'Your password must contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character'
								// }
							}
						},
						new_password: {
							validators: {
								stringLength: {
									min: 8,
									message: 'your password must be at least 8 characters short'
								},
								/*regexp: {
									regexp: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/i,
									message: 'Your password must contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character'
								}*/
							}
						},
						confirm_new_password: {
							validators: {
								identical: {
									field: 'new_password',
									message: 'The passwords do not match'
								}
							}
						}
					}
				})
				.on('success.form.bv', function(e) {
					e.preventDefault();
					var $form = $(e.target);
					var bv = $form.data('bootstrapValidator');
					console.log($form.find("[name='emergency_name[]']").length);
					$.post($form.attr('action'), $form.serialize(), function(result) {
						///  DEBUG console.log('log = '+result.message);
						$form.find('.alert').hide();
						oldClass = $form.find('.alert').attr('class');
						classes = oldClass.split(' ');
						removeThis = '';
						if(classes[1] !== '') removeThis = classes[1];

						if(result.alert_type == 'Success'){
							$form.find('.alert').removeClass(removeThis).addClass('alert-success').html('').hide();
							$form.data('bootstrapValidator').resetForm();
							$form.find('#edit_profile_button').show();
							$form.find('#update_profile_button').hide();
							$form.find('#cancel_update_button').hide();
							$form.find('.form-control').attr('disabled','disabled');
							$form.find('.form-control.tt-hint').show();
							$form.find('[type=password]').val('');
							$('#myModalProfile').modal('hide');
							$('p#myModalLabel').html(result.message);
							$('#myModalSuccess').modal('show');
							if(result.change && result.data.usertype != null) updateTableData(result.data);
						}else $form.find('.alert').removeClass('alert-success').addClass('alert-danger').html(result.message).show();
					}, 'json');
				});

				$('#dob_profile').change(function(){
					$('#profile_form').bootstrapValidator('revalidateField', 'dob');
				});

				//for myModalProfile - OTHER USER
				$('#edit_profile_form')
				.bootstrapValidator({
					message: 'This value is not valid',
					//live: 'submitted',
					feedbackIcons: {
						valid: 'glyphicon glyphicon-ok',
						invalid: 'glyphicon glyphicon-remove',
						validating: 'glyphicon glyphicon-refresh'
					},
					fields: {
						salutation: {
							message: 'The salutation is not valid',
							validators: {
								notEmpty: {
									message: 'The salutation is required and can\'t be empty'
								}
							}
						},
						user_type: {
							message: 'The type is not valid',
							validators: {
								notEmpty: {
									message: 'Type is required and can\'t be empty'
								}
							}
						},
						firstname: {
							message: 'The firstname is not valid',
							validators: {
								notEmpty: {
									message: 'The first name is required and can\'t be empty'
								},
								regexp: {
									regexp: /^([a-z0-9]*[a-z]){2}[a-zA-Z0-9 '.-]*$/i,
									message: 'The first name can only consist of letters and some characters(. -\')'
								}
							}
						},
						middlename: {
							message: 'The middlename is not valid',
							validators: {
								regexp: {
									regexp: /^[a-zA-Z0-9 '.-]*$/i,
									message: 'The middle name can only consist of letters and some characters(. -\')'
								}
							}
						},
						lastname: {
							message: 'The lastname is not valid',
							validators: {
								notEmpty: {
									message: 'The lastname is required and can\'t be empty'
								},
								regexp: {
									regexp: /^([a-z0-9]*[a-z]){2}[a-zA-Z0-9 '.-]*$/i,
									message: 'The lastname can only consist of letters and some characters(. -\')'
								}
							}
						},
						username: {
							message: 'The lastname is not valid',
							validators: {
								notEmpty: {
									message: 'The username is required and can\'t be empty'
								},
								regexp: {
									regexp: /^([a-z0-9]*[a-z]){2}[a-zA-Z0-9 '.-]*$/i,
									message: 'The username can only consist of letters and some characters(. -\')'
								}
							}
						},
						dob: {
							message: 'The date is not valid',
							validators: {
								notEmpty: {
									message: 'The date of birth is required and can\'t be empty'
								},
								date: {
									format: 'YYYY-MM-DD',
									message: 'Invalid Date. Please follow the correct format YYYY-MM-DD.'
								}
							}
						},
						company: {
							message: 'The company is not valid',
							validators: {
								customCompany: {
									message: 'Company is required (for non-customer users)',
									check_only_for: 'admin,agent,provider,account,operator',
									field: 'user_type'
								}
							}
						},
						phone_number: {
							message: 'The phone number is not valid',
							validators: {
								notEmpty: {
									message: 'Phone number is required and can\'t be empty'
								},
								stringLength: {
									min: 6,
									message: 'Your phone number must be at least 6 characters short'
								},
								regexp: {
									regexp: /^[0-9 ()+\-]+$/i,
									message: 'Enter the correct number format'
								}
							}
						},
						email: {
							validators: {
								notEmpty: {
									message: 'The email address is required and can\'t be empty'
								},
								emailAddress: {
									message: 'The input is not a valid email address'
								}
							}
						},
						country: {
							message: 'The country is not valid',
							validators: {
								notEmpty: {
									message: 'Country is required and can\'t be empty'
								}
							}
						}
					}
				})
				.on('success.form.bv', function(e) {
					e.preventDefault();
					var $form = $(e.target);
					var bv = $form.data('bootstrapValidator');

					$.post($form.attr('action'), $form.serialize(), function(result) {
						$form.find('.alert').hide();
						oldClass = $form.find('.alert').attr('class');
						classes = oldClass.split(' ');
						removeThis = '';
						if(classes[1] !== '') removeThis = classes[1];
						if(result.alert_type == 'Success'){
							$form.data('bootstrapValidator').resetForm(true);
							$form.find('.alert').removeClass(removeThis).addClass('alert-success').html('').hide();
							$('#myModalEditUser').modal('hide');
							$('p#myModalLabel').html(result.message);
							$('#myModalSuccess').modal('show');
							if(result.change && result.data.usertype != null) updateTableData(result.data);
						}else $form.find('.alert').removeClass(removeThis).addClass('alert-danger').html(result.message).show();
					}, 'json');
				});

				$('#dob_profile_edit').change(function(){
					$('#edit_profile_form').bootstrapValidator('revalidateField', 'dob');
				});

				function updateTableData(result){
					var user_tr = $('#'+result.usertype.toLowerCase()+'s table.table').find('tr[user_id='+result.id+']');
					var accounts_tr = $('#accounts table.table').find('tr[user_id='+result.id+']');

					$(user_tr).find('[name=name_td]').html(result.lastname+', '+result.firstname);
					$(user_tr).find('[name=company_td]').html(result.company);
					$(user_tr).find('[name=email_td]').html(result.email);
					$(accounts_tr).find('[name=name_td]').html(result.lastname+', '+result.firstname);
					$(accounts_tr).find('[name=company_td]').html(result.company);
					$(accounts_tr).find('[name=email_td]').html(result.email);
				}

				$('#myModalEditUser').on('hide.bs.modal', function() {
					$('#edit_profile_form').bootstrapValidator('resetForm', true);
				});

				function scrollUp(){
					$('html, body').animate({
						scrollTop: ($('.row').offset().top)
					},500);
				}

				//register/creater package
				$('#create_package_form')
				.bootstrapValidator({
					message: 'This value is not valid',
					feedbackIcons: {
						valid: 'glyphicon glyphicon-ok',
						invalid: 'glyphicon glyphicon-remove',
						validating: 'glyphicon glyphicon-refresh'
					},
					fields: {
						// package_code: {
						// 	message: 'Code is invalid',
						// 	validators: {
						// 		notEmpty: {
						// 			message: 'Code is required and can\'t be empty'
						// 		},
						// 		regexp: {
						// 			regexp: /^[a-zA-Z0-9 '.-]*$/i,
						// 			message: 'Enter a valid code.'
						// 		},
						// 	}
						// },
						"destination[]": {
							message: 'The Destination is not valid',
							validators: {
								notEmpty: {
									message: 'Destination is required and can\'t be empty'
								},
								// different: {
								// 	field: "destination[]",
								// 	message: "Duplicate destinations detected"
								// }
								// }
							}
						},
						"number_of_nights[]": {
							message: 'The type is not valid',
							validators: {
								notEmpty: {
									message: 'Number of Nights is required and can\'t be empty'
								},
								integer: {
									message: 'The value must not contain decimals or other special characters.'
								},
								greaterThan: {
								    value: 1,
								    message: 'Please enter a value greater than or equal to %s'
								}
							}
						},
						package_provider: {
							message: 'Provider is not valid',
							validators: {
								notEmpty: {
									message: 'Provider is required and can\'t be empty. Please choose one.'
								},
							}
						},
						userfile: {
							message: 'The file is not valid',
							validators: {
								file: {
			                        extension: 'jpg,jpeg,jpe,png',
			                        type: 'image/jpeg,image/png',
			                        maxSize: 1024 * 768,   // 2 MB
			                        message: 'The selected file is not allowed'
			                    }
							}
						},
						"file[]": {
							message: 'The file is not valid',
							validators: {
								file: {
			                        extension: 'pdf,xla,xlc,xlm,xls,xlt,xlw',
			                        type: 'application/pdf,application/vnd.ms-excel',
			                       	maxSize: 19531.25 * 1536,     // 4 MB
			                        message: 'The selected file is not allowed'
			                    }
							}
						},
						package_title: {
							message: 'The title is not valid',
							validators: {
								notEmpty: {
									message: 'Title is required and can\'t be empty'
								}
							}
						},
						package_description: {
							message: 'Description is not valid',
							validators: {
								notEmpty: {
									message: 'Description is required and can\'t be empty'
								},
							}
						},
						valid_date_from: {
							message: 'The date is not valid',
							validators: {
								notEmpty: {
									message: 'Valid Date is required and can\'t be empty'
								},
								date: {
									format: 'YYYY-MM-DD',
									message: 'Invalid Date. Please follow the correct format (YYYY-MM-DD).'
								}
							}
						},
						valid_date_until: {
							message: 'The date is not valid',
							validators: {
								notEmpty: {
									message: 'Valid Date is required and can\'t be empty'
								},
								date: {
									format: 'YYYY-MM-DD',
									message: 'Invalid Date. Please follow the correct format (YYYY-MM-DD).'
								}
							}
						}
					}
				})
				.on('success.form.bv', function(e) {
					e.preventDefault();
					var $form = $(e.target);
					var bv = $form.data('bootstrapValidator');
					var form = $form[0];
					var formData = new FormData(form);
					$.ajax({
					  url: $form.attr('action'),
					  data: formData,
					  processData: false,
					  contentType: false,
					  type: 'POST',
					  dataType: 'json',
					  success: function(result){
						$form.find('.alert').hide();
						if(result.alert_type == 'Success'){
							location.href = window.location.origin+'/panel/view_packages';
						}else{
							$("#createPackage_BTN").removeAttr("disabled");
							$form.find('.alert').removeClass('alert-success').addClass('alert-danger').html(result.message).show();
					  	}
					  },
					  error: function(x){
					  	// console.log(x.responseText);
					  	$("#createPackage_BTN").removeAttr("disabled");
					  }
					});
				});


				$('#update_package_form')
				.bootstrapValidator({
					message: 'This value is not valid',
					feedbackIcons: {
						valid: 'glyphicon glyphicon-ok',
						invalid: 'glyphicon glyphicon-remove',
						validating: 'glyphicon glyphicon-refresh'
					},
					fields: {
						"destination[]": {
							message: 'The Destination is not valid',
							validators: {
								notEmpty: {
									message: 'Destination is required and can\'t be empty'
								}
							}
						},
						"number_of_nights[]": {
							message: 'The type is not valid',
							validators: {
								notEmpty: {
									message: 'Number of days is required and can\'t be empty'
								},
								integer: {
									message: 'The value must not contain decimals or other special characters.'
								},
								greaterThan: {
								    value: 1,
								    message: 'Please enter a value greater than or equal to %s'
								}
							}
						},
						package_provider: {
							message: 'Provider is not valid',
							validators: {
								notEmpty: {
									message: 'Provider is required and can\'t be empty. Please choose one.'
								},
							}
						},
						userfile: {
							message: 'The file is not valid',
							validators: {
								file: {
			                        extension: 'jpg,jpeg,png',
			                        type: 'image/jpeg,image/png',
			                        maxSize: 1024 * 768,   // 2 MB
			                        message: 'The selected file is not allowed'
			                    }
							}
						},
						"fckshit[]": {
							message: 'The file is not valid',
							validators: {
								file: {
			                        extension: 'jpg,jpeg,png,pdf,xla,xlc,xlm,xls,xlt,xlw',
			                        type: 'image/jpeg,image/png,application/pdf,application/vnd.ms-excel',
			                        maxSize: 19531.25 * 1536,   // 2 MB
			                        message: 'The selected file is not allowed'
			                    }
							}
						},
						package_title: {
							message: 'The title is not valid',
							validators: {
								notEmpty: {
									message: 'Title is required and can\'t be empty'
								}
							}
						},
						package_description: {
							message: 'Description is not valid',
							validators: {
								notEmpty: {
									message: 'Description is required and can\'t be empty'
								},
							}
						},
						valid_date_from: {
							message: 'The date is not valid',
							validators: {
								notEmpty: {
									message: 'Valid Date is required and can\'t be empty'
								},
								date: {
									format: 'YYYY-MM-DD',
									message: 'Invalid Date. Please follow the correct format (YYYY-MM-DD).'
								}
							}
						},
						valid_date_until: {
							message: 'The date is not valid',
							validators: {
								notEmpty: {
									message: 'Valid Date is required and can\'t be empty'
								},
								date: {
									format: 'YYYY-MM-DD',
									message: 'Invalid Date. Please follow the correct format (YYYY-MM-DD).'
								}
							}
						}
					}
				});
				// .on('success.form.bv', function(e) {
				// 	e.preventDefault();
				// 	var $form = $(e.target);
				// 	var bv = $form.data('bootstrapValidator');
				// 	var serialized = $form.serialize();
				// 	$form.find('.alert').removeClass('alert-warning alert-success alert-danger').html('').hide();
				// 	$.ajax({
				// 	// 	url: $form.attr('action'),
				// 	// 	data: formData,
				// 	// 	processData: false,
				// 	// 	contentType: false,
				// 	// 	type: 'POST',
				// 	// 	dataType: 'json',
				// 	// 	success: function(result){
				// 	// 		$form.find('.alert').hide();
				// 	// 		if(result.alert_type == 'Success'){
				// 	// 			location.href = window.location.origin+'/panel/view_packages';
				// 	// 		}else{
				// 	// 			$("#createPackage_BTN").removeAttr("disabled");
				// 	// 			$form.find('.alert').removeClass('alert-success').addClass('alert-danger').html(result.message).show();
				// 	// 		}
				// 	// 	},
				// 	// 	error: function(x){
				// 	// 	// console.log(x.responseText);
				// 	// 	$("#createPackage_BTN").removeAttr("disabled");
				// 	// }


				// 	  	url: $form.attr('action'),
				// 	  	data: serialized,
				// 		processData: false,
				// 		contentType: false,
				// 	  	type: 'POST',
				// 	  	dataType: 'json',
				// 	  	success: function(result){
				// 		  	$("#package_edit_BTN").removeAttr("disabled");
				// 			$form.find('.alert').hide();
				// 			if(result.alert_type == 'Success'){
				// 				$form.find('.alert').removeClass('alert-danger').addClass('alert-success').html(result.message).show();
				// 				$form.data('bootstrapValidator').resetForm();
				// 				if(result.src_path){
				// 					var img = '<img height="100px" src="'+result.src_path+'"/>';
				// 					$form.find('[name=package_image]').val(result.db_path);
				// 					if($form.find('img').length > 0)
				// 						$form.find('img').replaceWith(img);
				// 					else
				// 						$form.find('[name=package_image]').before(img);
				// 				}
				// 			}else $form.find('.alert').removeClass('alert-success').addClass('alert-danger').html(result.message).show();
				// 	  	},
				// 	  	error : function(x){
				// 	  		$("#package_edit_BTN").removeAttr("disabled");
				// 	  	}
				// 	});
				// });

				//creating a hotel
				$('#create_hotel_form')
				.bootstrapValidator({
					message: 'This value is not valid',
					feedbackIcons: {
						valid: 'glyphicon glyphicon-ok',
						invalid: 'glyphicon glyphicon-remove',
						validating: 'glyphicon glyphicon-refresh'
					},
					fields: {
						hotel_name: {
							message: 'Hotel name is not valid',
							validators: {
								notEmpty: {
									message: 'Hotel name is required and can\'t be empty'
								}
							}
						},
						hotel_country: {
							message: 'The Country is not valid',
							validators: {
								notEmpty: {
									message: 'Country is required and can\'t be empty'
								}
							}
						},
						hotel_website: {
							message: 'The Website is not valid',
							validators: {
								uri: {
									message: 'The website address is not valid. Do not forgot the http:// or https://'
								}
							}
						},
						hotel_phone: {
							message: 'The phone number is not valid',
							validators: {
								regexp: {
									regexp: /^[0-9 ()+\-]+$/i,
									message: 'Enter the correct number format'
								}
							}
						},
						hotel_email: {
							message: 'The email is not valid',
							validators: {
								emailAddress: {
									message: 'The input is not a valid email address'
								}
							}
						}
					}
				})
				.on('success.form.bv', function(e) {
					e.preventDefault();
					var $form = $(e.target);
					var bv = $form.data('bootstrapValidator');
					$.post($form.attr('action'), $form.serialize(), function(result) {
						// resultObj = result.split('_:_');
						// alert_type = resultObj[0];
						// message = resultObj[1];
						// html_data = resultObj[2];

						// $form.find('.alert').hide();
						// oldClass = $form.find('.alert').attr('class');
						// classes = oldClass.split(' ');
						// removeThis = '';
						// if(classes[1] !== '') removeThis = classes[1];

						if(result.alert_type == 'Success'){
							// $form.data('bootstrapValidator').resetForm(true);
							location.href = window.location.origin+'/panel/view_hotels';
							// $form.find('.alert').removeClass(removeThis).addClass('alert-success').html('').hide();
							// $('#hotelTable.table > tbody:last').append(html_data);
							// $('#myModalHotel').modal('hide');
							// $('p#myModalLabel').html(message);
							// $('#myModalSuccess').modal('show');
						}else $form.find('.alert').removeClass('alert-success').addClass('alert-danger').html(result.message).show();
					}, 'json');
				});

				$('#create_destination')
				.bootstrapValidator({
					message: 'This value is not valid',
					feedbackIcons: {
						valid: 'glyphicon glyphicon-ok',
						invalid: 'glyphicon glyphicon-remove',
						validating: 'glyphicon glyphicon-refresh'
					},
					fields: {
						country: {
							validators: {
								callback: {
			                        message: 'Please fill at least one of these fields',
			                        callback: function(value, validator) {
			                            return checkGroupEmpty(value, validator);
			                        }
			                    }
							},
						},
						city: {
							validators: {
								callback: {
									message: 'Please fill at least one of these fields',
									callback: function(value, validator) {
			                            return checkGroupEmpty(value, validator);
		                        	}
		                        },
								regexp: {
									message: 'Enter a valid city name',
									regexp: /^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$/
								}
							}
						},
						code: {
							message: 'Code is not valid',
							validators: {
								notEmpty: {
									message: 'Code is required and can\'t be empty'
								},
								regexp: {
									regexp: /^[A-Z]{2,3}$/,
									message: 'Only Upper Case letters are allowed.'
								},
							}
						}
					}
				})
				.on('success.form.bv', function(e) {
					e.preventDefault();
					var $form = $(e.target);
					var bv = $form.data('bootstrapValidator');

					$form.find('.alert').removeClass('alert-warning alert-success alert-danger').html('').hide();
					loader = "<?php echo base_url('./img/loader.gif');?>";
			 		$element = '<center><img id="loader" height="50px" src="'+ loader +'"></center>';
					$form.find('.alert').addClass('alert-warning').css('padding','0').append($element).show();

					$.post($form.attr('action'), $form.serialize(), function(result) {
						$form.find('.alert center').remove();
						$form.find('.alert').removeClass('alert-warning').css('padding','');

						var	resultObj 	= result.split('_:_');
						var	alert_type  = resultObj[0];
						var	message 	= resultObj[1];

						if(alert_type == 'Success'){
							html_data = resultObj[2];
							$form.find('.alert').addClass('alert-success').html(message);
							$('#countries').find("table.table > tbody:last").append(html_data)
							$form.data('bootstrapValidator').resetForm();
						}else $form.find('.alert').addClass('alert-danger').html(message);
					});
				});

				$('#create_airportcode')
				.bootstrapValidator({
					message: 'This value is not valid',
					feedbackIcons: {
						valid: 'glyphicon glyphicon-ok',
						invalid: 'glyphicon glyphicon-remove',
						validating: 'glyphicon glyphicon-refresh'
					},
					fields: {
						airportcode: {
							validators: {
								notEmpty: {
									message: 'Code is required and can\'t be empty'
								},
								regexp: {
									message: 'Enter a valid city name',
									regexp: /^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$/
								}
							}
						},
						code: {
							validators: {
								notEmpty: {
									message: 'Code is required and can\'t be empty'
								},
								regexp: {
									regexp: /^[A-Z]{2,3}$/,
									message: 'Only Upper Case letters are allowed.'
								},
							}
						}
					}
				})
				.on('success.form.bv', function(e) {
					e.preventDefault();
					var $form = $(e.target);
					var bv = $form.data('bootstrapValidator');

					$form.find('.alert').removeClass('alert-warning alert-success alert-danger').html('').hide();
					loader = "<?php echo base_url('./img/loader.gif');?>";
			 		$element = '<center><img id="loader" height="50px" src="'+ loader +'"></center>';
					$form.find('.alert').addClass('alert-warning').css('padding','0').append($element).show();

					$.post($form.attr('action'), $form.serialize(), function(result) {
						$form.find('.alert center').remove();
						$form.find('.alert').removeClass('alert-warning').css('padding','');

						var	resultObj 	= result.split('_:_');
						var	alert_type  = resultObj[0];
						var	message 	= resultObj[1];

						if(alert_type == 'Success'){
							html_data = resultObj[2];
							$form.find('.alert').addClass('alert-success').html(message);
							$('#airportcodes').find("table.table > tbody:last").append(html_data)
							$form.data('bootstrapValidator').resetForm();
						}else $form.find('.alert').addClass('alert-danger').html(message);
					});
				});

				function checkGroupEmpty(value, validator) {
				    var $groups = $('#newdestination').find('.dest'),
				        total   = $groups.length;
				    // If all elements are empty, then return false
				    if ($groups.filter(function() {
				            return $(this).val() === '';
				        }).length === total){
				        return false;
				    } else {
				        // Update the status of this validator to all fields
				        $groups.each(function() {
				            validator.updateStatus($(this).attr('name'), validator.STATUS_VALID, 'callback');
				        });

				        // Otherwise, returns true
				        return true;
				    }
				}

				$('#myModalHotel').on('hide.bs.modal', function() {
					$('#addpackagehotels_form').bootstrapValidator('resetForm', true);
				});

				$('#addhotelroomrate_form')
				.bootstrapValidator({
					message: 'This value is not valid',
					feedbackIcons: {
						valid: 'glyphicon glyphicon-ok',
						invalid: 'glyphicon glyphicon-remove',
						validating: 'glyphicon glyphicon-refresh'
					},
					fields: {
						hotelroomrate_cost: {
							message: 'The Cost is not valid',
							validators: {
								notEmpty: {
									message: 'Cost is required and can\'t be empty'
								},
								greaterThan: {
								    value: 0,
								    message: 'Please enter a value greater than or equal to %s'
								}
							}
						},
						hotelroomrate_cost_ext: {
							message: 'The Cost is not valid',
							validators: {
								notEmpty: {
									message: 'Cost Ext. is required and can\'t be empty'
								},
								greaterThan: {
								    value: 0,
								    message: 'Please enter a value greater than or equal to %s'
								}
							}
						},
						hotelroomrate_profit: {
							message: 'The Profit is not valid',
							validators: {
								notEmpty: {
									message: 'Profit is required and can\'t be empty'
								},
								greaterThan: {
								    value: 0,
								    message: 'Please enter a value greater than or equal to %s'
								}
							}
						},
						hotelroomrate_profit_ext: {
							message: 'The Profit is not valid',
							validators: {
								notEmpty: {
									message: 'Profit Ext. is required and can\'t be empty'
								},
								greaterThan: {
								    value: 0,
								    message: 'Please enter a value greater than or equal to %s'
								}
							}
						},
						hotelroomrate_pax: {
							message: 'The Pax is not valid',
							validators: {
								// notEmpty: {
								// 	message: 'Pax is required and can\'t be empty'
								// },
								greaterThan: {
								    value: 1,
								    message: 'Please enter a value greater than or equal to %s'
								}
							}
						}
					}
				})
				.on('success.form.bv', function(e) {
					e.preventDefault();
					var $form = $(e.target);
					var bv = $form.data('bootstrapValidator');
					var form = $form[0];
					var formData = new FormData(form);
					package_hotel_id = $form.find('[name=parent_id]').val();
					act_type = $form.find('[name=act_type]').val();

					$.ajax({
			            url: $form.attr('action'),
			            data: formData,
			            processData: false,
			            contentType: false,
			            type: 'POST',
			            dataType: 'html',
			            success: function(result){
			            	resultObj = result.split('_:_');
							alert_type = resultObj[0];
							message = resultObj[1];
			                $form.find('.alert').hide();
			                if(alert_type == 'Success'){
								html_data = resultObj[2];
			                    $form.data('bootstrapValidator').resetForm(true);
			                    $('#myModalRoomRate').modal('hide');
			                	if(act_type == 'add')
			                		$('table#packagehotels_table').find('[packagehotelid='+package_hotel_id+']').find('#roomrate_table.table > tbody:last').append(html_data);
			                	else
			                		$('table#packagehotels_table').find('#roomrate_table.table tr[roomrateid='+package_hotel_id+']').replaceWith(html_data);
			                } else {
			                	$form.find('.alert').removeClass('alert-success').addClass('alert-warning').html(message).show();
			            	}
			            }
			        });
				});

				$('#add_hotelroomsurcharge_form')
				.bootstrapValidator({
					message: 'This value is not valid',
					feedbackIcons: {
						valid: 'glyphicon glyphicon-ok',
						invalid: 'glyphicon glyphicon-remove',
						validating: 'glyphicon glyphicon-refresh'
					},
					fields: {
						roomratesurcharge_description: {
							message: 'Description is not valid',
							validators: {
								notEmpty: {
									message: 'Description is required and can\'t be empty'
								}
							}
						},
						roomratesurcharge_ruletype: {
							message: 'Rule Type is not valid',
							validators: {
								notEmpty: {
									message: 'Rule Type is required and can\'t be empty'
								}
							}
						},
						roomratesurcharge_rule: {
							message: 'Rule is not valid',
							validators: {
								notEmpty: {
									message: 'Rule is required and can\'t be empty'
								}
							}
						},
						roomratesurcharge_rule_range_1: {
							message: 'Rule is not valid',
							validators: {
								notEmpty: {
									message: 'Date From is required and can\'t be empty'
								}
							}
						},
						roomratesurcharge_rule_range_2: {
							message: 'Rule is not valid',
							validators: {
								notEmpty: {
									message: 'Date To is required and can\'t be empty'
								}
							}
						},
						roomratesurcharge_rule_day: {
							message: 'Rule is not valid',
							validators: {
								notEmpty: {
									message: 'Day of Week is required and can\'t be empty'
								}
							}
						},
						roomratesurcharge_rule_agent: {
							message: 'Rule is not valid',
							validators: {
								notEmpty: {
									message: 'Agent Name is required and can\'t be empty'
								}
							}
						},
						roomratesurcharge_rule_out_1: {
							message: 'Rule is not valid',
							validators: {
								notEmpty: {
									message: 'Date From is required and can\'t be empty'
								}
							}
						},
						roomratesurcharge_rule_out_2: {
							message: 'Rule is not valid',
							validators: {
								notEmpty: {
									message: 'Date To is required and can\'t be empty'
								}
							}
						},
						roomratesurcharge_cost: {
							message: 'The cost is not valid',
							validators: {
								notEmpty: {
									message: 'Cost is required and can\'t be empty'
								},
								greaterThan: {
								    value: 0,
								    message: 'Please enter a value greater than or equal to %s'
								}
							}
						},
						roomratesurcharge_profit: {
							message: 'The profit is not valid',
							validators: {
								notEmpty: {
									message: 'Profit is required and can\'t be empty'
								},
								greaterThan: {
								    value: 0,
								    message: 'Please enter a value greater than or equal to %s'
								}
							}
						},

					}
				})
				.on('success.form.bv', function(e) {
					e.preventDefault();
					var $form = $(e.target);
					var bv = $form.data('bootstrapValidator');
					var form = $form[0];
					var formData = new FormData(form);
					var act_type = $form.find('[name=act_type]').val();
					$.ajax({
			            url: $form.attr('action'),
			            data: formData,
			            processData: false,
			            contentType: false,
			            type: 'POST',
			            dataType: 'html',
			            success: function(result){
			            	resultObj = result.split('_:_');
							alert_type = resultObj[0];
							message = resultObj[1];
			                $form.find('.alert').hide();
			                if(alert_type == 'Success'){
								html_data = resultObj[2];
			                    $form.data('bootstrapValidator').resetForm(true);
			                    $('#myModalRoomRateSurcharge').modal('hide');
			                	//roomrate_id on edit is it's id, not the hotelroom's id
			                	roomrate_id = $form.find('[name=parent_id]').val();
			                	if(act_type == 'add'){
			                		$('table#packagehotels_table').find('[roomrateId='+ roomrate_id +']').parents('tr[packageHotelId]').find('#roomsurcharge_table.table > tbody:last').append(html_data);
			                	}else{
									pckg_htl_id = $form.find('[name=pckg_htl_id]').val();
			                		$('table#packagehotels_table').find('tr[packagehotelid='+ pckg_htl_id +']').find('#roomsurcharge_table tr[roomsurchargeid='+roomrate_id+']').replaceWith(html_data);
			                	}

			                } else {
			                	// $form.find('.alert').removeClass('alert-success').addClass('alert-warning').html(message).show();
			                	$form.find('.alert').removeClass('alert-success').addClass('alert-warning').html(message).show();
			            	}
			            }
			        });
				});

				// revalidateField for Hotel Room Surcharge
				$('#range_from1').change(function(){
					var d = $("#range_from1").val();
					var res = d.replace(/-/g, ",");

					var $input = $('#range_to1').pickadate()
					var picker = $input.pickadate('picker');
					picker.set('min', res);
					$('#add_hotelroomsurcharge_form').bootstrapValidator('revalidateField', 'roomratesurcharge_rule_range_1');
				});
				$('#range_to1').change(function(){
					$('#add_hotelroomsurcharge_form').bootstrapValidator('revalidateField', 'roomratesurcharge_rule_range_2');
				});
				$('#blackout_from1').change(function(){
					var d = $("#blackout_from1").val();
					var res = d.replace(/-/g, ",");

					var $input = $('#blackout_to1').pickadate()
					var picker = $input.pickadate('picker');
					picker.set('min', res);
					$('#add_hotelroomsurcharge_form').bootstrapValidator('revalidateField', 'roomratesurcharge_rule_out_1');
				});
				$('#blackout_to1').change(function(){
					$('#add_hotelroomsurcharge_form').bootstrapValidator('revalidateField', 'roomratesurcharge_rule_out_2');
				});

				//package addons
				$('#myModalOptional').on('hide.bs.modal', function() {
					url_link = "<?php echo base_url('panel/add_packageaddons'); ?>";
					$form = $('#addaddon_form');
					$form.bootstrapValidator('resetForm', true);
					$form.attr('action',url_link);
					$form.find('[name=act_type]').val('add');
				});

				//get package details
				$('body').on('click', '.edit_addon_link', function(e) {
					e.preventDefault();
					var A = $(this);
					var target_modal = A.attr('data-target');
					$form = $(target_modal).find('form');

					var get_link = "<?php echo base_url('panel/get_addon'); ?>";
					var url_link = "<?php echo base_url('panel/update_addon'); ?>";
					var id = A.parents('tr').attr('addonid');

					$form.attr('action', url_link);
					// $form.find('[name=pckgsrchrg_id]').val(id);
					$form.find('[name=act_type]').val('edit');
					$form.find('[name=addon_id]').val(id);
					$.ajax({
						url: get_link,
						type: 'POST',
						async : false,
						data : 'id='+id,
						dataType: 'json',
						error: function(){
							$form.find('.alert').addClass('alert-danger').html('There seems to be an error. Please try again.').show();
							return false;
						},
						success: function(result){
							// $(target_modal).modal('show'); return false;
							$form.find('.alert').html('').hide();
							if(result.alert_type == 'Success'){
								$.each(result.info, function(k, v) {
									if(v) 	$form.find('[name='+k+'].form-control').val(v).show();
									else 	$form.find('[name='+k+'].form-control').val('');
								});
							}else
								$form.find('.alert').html(result.message).show();

							$(target_modal).modal('show');
						}
					});
				});

				//package addons
				$('#addaddon_form')
				.bootstrapValidator({
					message: 'This value is not valid',
					feedbackIcons: {
						valid: 'glyphicon glyphicon-ok',
						invalid: 'glyphicon glyphicon-remove',
						validating: 'glyphicon glyphicon-refresh'
					},
					fields: {
						addon_description: {
							message: 'Description is not valid',
							validators: {
								notEmpty: {
									message: 'Description is required and can\'t be empty'
								}
							}
						},
						addon_unit: {
							message: 'Unit is not valid',
							validators: {
								notEmpty: {
									message: 'Unit is required and can\'t be empty'
								}
							}
						},
						addon_cost: {
							message: 'Cost is not valid',
							validators: {
								notEmpty: {
									message: 'Cost is required and can\'t be empty'
								},
								greaterThan: {
								    value: 0,
								    message: 'Please enter a value greater than or equal to %s'
								}
							}
						},
						addon_profit: {
							message: 'Profit is not valid',
							validators: {
								notEmpty: {
									message: 'Profit is required and can\'t be empty'
								},
								greaterThan: {
								    value: 0,
								    message: 'Please enter a value greater than or equal to %s'
								}
							}
						},

					}
				})
				.on('success.form.bv', function(e) {
					e.preventDefault();
					var $form = $(e.target);
					var bv = $form.data('bootstrapValidator');
					var form = $form[0];
					var formData = new FormData(form);
					package_hotel_id = $form.find('[name=package_hotel_id]').val();
					act_type = $form.find('[name=act_type]').val();
					addon_id = $form.find('[name=addon_id]').val();
					$.ajax({
			            url: $form.attr('action'),
			            data: formData,
			            processData: false,
			            contentType: false,
			            type: 'POST',
			            dataType: 'html',
			            success: function(result){
			            	resultObj = result.split('_:_');
							alert_type = resultObj[0];
							message = resultObj[1];
			                $form.find('.alert').hide();
			                if(alert_type == 'Success'){
								html_data = resultObj[2];
			                    $form.data('bootstrapValidator').resetForm(true);
			                    $('#myModalOptional').modal('hide');
			                	if(act_type == 'add')
			                		$('#addons_table.table > tbody:last').append(html_data);
			                	else
			                		$('#addons_table.table').find('tr[addonid='+ addon_id +']').replaceWith(html_data);
			                } else {
			                	$form.find('.alert').removeClass('alert-success').addClass('alert-warning').html(message).show();
			            	}
			            }
			        });
				});

				//creating a hotel
				$('#edit_hotel_form')
				.bootstrapValidator({
					message: 'This value is not valid',
					feedbackIcons: {
						valid: 'glyphicon glyphicon-ok',
						invalid: 'glyphicon glyphicon-remove',
						validating: 'glyphicon glyphicon-refresh'
					},
					fields: {
						hotel_name: {
							message: 'Hotel name is not valid',
							validators: {
								notEmpty: {
									message: 'Hotel name is required and can\'t be empty'
								}
							}
						},
						hotel_country: {
							message: 'The Country is not valid',
							validators: {
								notEmpty: {
									message: 'Country is required and can\'t be empty'
								}
							}
						},
						hotel_website: {
							message: 'The Website is not valid',
							validators: {
								uri: {
									message: 'The website address is not valid. Do not forgot the http:// or https://'
								}
							}
						},
						hotel_phone: {
							message: 'The phone number is not valid',
							validators: {
								regexp: {
									regexp: /^[0-9 ()+\-]+$/i,
									message: 'Enter the correct number format'
								}
							}
						},
						hotel_email: {
							message: 'The email is not valid',
							validators: {
								emailAddress: {
									message: 'The input is not a valid email address'
								}
							}
						}
					}
				})
				.on('success.form.bv', function(e) {
					e.preventDefault();
					var $form = $(e.target);
					var bv = $form.data('bootstrapValidator');

					$.post($form.attr('action'), $form.serialize(), function(result) {
						resultObj 	= result.split('_:_');
						alert_type 	= resultObj[0];
						message 	= resultObj[1];
						hotel_id 	= resultObj[2];
						html_data 	= resultObj[3];

						$form.find('.alert').html('').hide();

						if(alert_type == 'Success'){
							$('#hotelTable').find('tr[hotel_id='+hotel_id+'] a.edit_link').parents('td').replaceWith(html_data);
							$form.data('bootstrapValidator').resetForm(true);
							$('#myModalEditHotel').modal('hide');
							$('p#myModalLabel').html(message);
							$('#myModalSuccess').modal('show');
						}else $form.find('.alert').removeClass('alert-success').addClass('alert-danger').html(message).show();
					}, 'html');
				});

				//adding a hotel surcharge
				$('#create_hotelsurcharge_form')
				.bootstrapValidator({
					message: 'This value is not valid',
					feedbackIcons: {
						valid: 'glyphicon glyphicon-ok',
						invalid: 'glyphicon glyphicon-remove',
						validating: 'glyphicon glyphicon-refresh'
					},
					fields: {
						hotelsurcharge_description: {
							message: 'Description is not valid',
							validators: {
								notEmpty: {
									message: 'Description is required and can\'t be empty'
								}
							}
						},
						hotelsurcharge_ruletype: {
							message: 'Rule Type is not valid',
							validators: {
								notEmpty: {
									message: 'Rule Type is required and can\'t be empty'
								}
							}
						},
						hotelsurcharge_rule: {
							message: 'Rule is not valid',
							validators: {
								notEmpty: {
									message: 'Rule is required and can\'t be empty'
								}
							}
						},
						hotelsurcharge_rule_range_1: {
							message: 'Rule is not valid',
							validators: {
								notEmpty: {
									message: 'Date From is required and can\'t be empty'
								}
							}
						},
						hotelsurcharge_rule_range_2: {
							message: 'Rule is not valid',
							validators: {
								notEmpty: {
									message: 'Date TO is required and can\'t be empty'
								}
							}
						},
						hotelsurcharge_rule_day: {
							message: 'Rule is not valid',
							validators: {
								notEmpty: {
									message: 'Day of Week is required and can\'t be empty'
								}
							}
						},
						hotelsurcharge_rule_agent: {
							message: 'Rule is not valid',
							validators: {
								notEmpty: {
									message: 'Agent Name is required and can\'t be empty'
								}
							}
						},
						hotelsurcharge_rule_out_1: {
							message: 'Rule is not valid',
							validators: {
								notEmpty: {
									message: 'Date From is required and can\'t be empty'
								}
							}
						},
						hotelsurcharge_rule_out_2: {
							message: 'Rule is not valid',
							validators: {
								notEmpty: {
									message: 'Date To is required and can\'t be empty'
								}
							}
						},
						hotelsurcharge_cost: {
							message: 'The cost is not valid',
							validators: {
								notEmpty: {
									message: 'Cost is required and can\'t be empty'
								},
								greaterThan: {
								    value: 0,
								    message: 'Please enter a value greater than or equal to %s'
								}
							}
						},
						hotelsurcharge_profit: {
							message: 'The profit is not valid',
							validators: {
								notEmpty: {
									message: 'Cost is required and can\'t be empty'
								},
								greaterThan: {
								    value: 0,
								    message: 'Please enter a value greater than or equal to %s'
								}
							}
						},
					}
				})
				.on('success.form.bv', function(e) {
					e.preventDefault();
					var $form 	= $(e.target);
					var bv 		= $form.data('bootstrapValidator');
					var hs_id 	= $form.find('[name=hotelsurcharge_id]').val();
					var h_id 	= $form.find('[name=hotel_id]').val();
					$.ajax({
						url: $form.attr('action'),
						type: 'POST',
						async : false,
						data : $form.serialize(),
						dataType: 'html',
						error: function(){
							$form.find('.alert').addClass('alert-danger').html('There seems to be an error. Please try again.').show();
							return false;
						},
						success: function(result){
							resultObj = result.split('_:_');
							alert_type = resultObj[0];
							html_data = resultObj[1];
							$form.find('.alert').hide();
							if(alert_type == 'Success'){
								if(hs_id != '')
									$('#hotelTable').find('tr[hotel_id='+ h_id +']').find('tr[hotelsurcharge_id='+ hs_id +']').replaceWith(html_data);
								else
									$('#hotelTable').find('tr[hotel_id='+ h_id +']').find('.table > tbody:last').append(html_data);
								$('#myModalHotelSurcharge').modal('hide');
							}else{
								$form.find('.alert').removeClass('alert-success').addClass('alert-danger').html(html_data).show();
							}
							return false;
						}
					});
				});

				//revalidateField for Hotel Surcharge
				$('#range_from').change(function(){
					var d = $("#range_from").val();
					var res = d.replace(/-/g, ",");

					var $input = $('#range_to').pickadate()
					var picker = $input.pickadate('picker');
					picker.set('min', res);
					$('#create_hotelsurcharge_form').bootstrapValidator('revalidateField', 'hotelsurcharge_rule_range_1');
				});
				$('#range_to').change(function(){
					$('#create_hotelsurcharge_form').bootstrapValidator('revalidateField', 'hotelsurcharge_rule_range_2');
				});
				$('#blackout_from').change(function(){
					var d = $("#blackout_from").val();
					var res = d.replace(/-/g, ",");

					var $input = $('#blackout_to').pickadate()
					var picker = $input.pickadate('picker');
					picker.set('min', res);
					$('#create_hotelsurcharge_form').bootstrapValidator('revalidateField', 'hotelsurcharge_rule_out_1');
				});
				$('#blackout_to').change(function(){
					$('#create_hotelsurcharge_form').bootstrapValidator('revalidateField', 'hotelsurcharge_rule_out_2');
				});



				//adding a hotel room rate surcharge
				$('#create_roomratesurcharge_form')
				.bootstrapValidator({
					message: 'This value is not valid',
					feedbackIcons: {
						valid: 'glyphicon glyphicon-ok',
						invalid: 'glyphicon glyphicon-remove',
						validating: 'glyphicon glyphicon-refresh'
					},
					fields: {
						roomratesurcharge_description: {
							message: 'Description is not valid',
							validators: {
								notEmpty: {
									message: 'Description is required and can\'t be empty'
								}
							}
						},
						roomratesurcharge_ruletype: {
							message: 'Rule Type is not valid',
							validators: {
								notEmpty: {
									message: 'Rule Type is required and can\'t be empty'
								}
							}
						},
						roomratesurcharge_rule: {
							message: 'Rule is not valid',
							validators: {
								notEmpty: {
									message: 'Rule is required and can\'t be empty'
								}
							}
						},
						roomratesurcharge_rule_range_1: {
							message: 'Rule is not valid',
							validators: {
								notEmpty: {
									message: 'Date From is required and can\'t be empty'
								}
							}
						},
						roomratesurcharge_rule_range_2: {
							message: 'Rule is not valid',
							validators: {
								notEmpty: {
									message: 'Date TO is required and can\'t be empty'
								}
							}
						},
						roomratesurcharge_rule_day: {
							message: 'Rule is not valid',
							validators: {
								notEmpty: {
									message: 'Day of Week is required and can\'t be empty'
								}
							}
						},
						roomratesurcharge_rule_agent: {
							message: 'Rule is not valid',
							validators: {
								notEmpty: {
									message: 'Agent Name is required and can\'t be empty'
								}
							}
						},
						roomratesurcharge_rule_out_1: {
							message: 'Rule is not valid',
							validators: {
								notEmpty: {
									message: 'Date From is required and can\'t be empty'
								}
							}
						},
						roomratesurcharge_rule_out_2: {
							message: 'Rule is not valid',
							validators: {
								notEmpty: {
									message: 'Date To is required and can\'t be empty'
								}
							}
						},
						roomratesurcharge_cost: {
							message: 'The cost is not valid',
							validators: {
								notEmpty: {
									message: 'Cost is required and can\'t be empty'
								},
								greaterThan: {
								    value: 0,
								    message: 'Please enter a value greater than or equal to %s'
								}
							}
						},
						roomratesurcharge_profit: {
							message: 'The profit is not valid',
							validators: {
								notEmpty: {
									message: 'Cost is required and can\'t be empty'
								},
								greaterThan: {
								    value: 0,
								    message: 'Please enter a value greater than or equal to %s'
								}
							}
						},
					}
				})
				.on('success.form.bv', function(e) {
					e.preventDefault();

                    var $form     = $(e.target),
                    validator = $form.data('bootstrapValidator');
                    $form.find('.alert').html('Thanks for signing up. Now you can sign in as ' + validator.getFieldElements('username').val()).show();
				});



				//package surcharge
				$('#myModalSurcharge').on('hide.bs.modal', function() {
					url_link = "<?php echo base_url('panel/create_packagesurcharge'); ?>";
					$form = $('#create_packagesurcharge_form');
					$form.bootstrapValidator('resetForm', true);
					$form.attr('action',url_link);
					$form.find('[name=act_type]').val('add');
				});


				$('body').on('click', '.edit_packagesurcharge_link', function(e) {
					e.preventDefault();
					var A = $(this);
					var target_modal = A.attr('data-target');
					$form = $(target_modal).find('form');

					var get_link = "<?php echo base_url('panel/get_packagesurcharge'); ?>";
					var url_link = "<?php echo base_url('panel/update_packagesurcharge'); ?>";
					var id = A.parents('tr').attr('packageSurchargeId');

					$form.attr('action', url_link);
					$form.find('[name=pckgsrchrg_id]').val(id);
					$form.find('[name=act_type]').val('edit');
					$.ajax({
						url: get_link,
						type: 'POST',
						async : false,
						data : 'id='+id,
						dataType: 'json',
						error: function(){
							$form.find('.alert').addClass('alert-danger').html('There seems to be an error. Please try again.').show();
							return false;
						},
						success: function(result){
							// $(target_modal).modal('show'); return false;
							$form.find('.alert').html('').hide();
							if(result.alert_type == 'Success'){
								$.each(result.info, function(k, v) {
									if(v) 	$form.find('[name='+k+'].form-control').val(v).show();
									else 	$form.find('[name='+k+'].form-control').val('');
								});
							ruleType2();
							}else
								$form.find('.alert').html(result.message).show();

							$(target_modal).modal('show');
						}
					});
				});

				//adding a package surcharge
				$('#create_packagesurcharge_form')
				.bootstrapValidator({
					message: 'This value is not valid',
					feedbackIcons: {
						valid: 'glyphicon glyphicon-ok',
						invalid: 'glyphicon glyphicon-remove',
						validating: 'glyphicon glyphicon-refresh'
					},
					fields: {
						packagesurcharge_description: {
							message: 'Description is not valid',
							validators: {
								notEmpty: {
									message: 'Description is required and can\'t be empty'
								}
							}
						},
						packagesurcharge_ruletype: {
							message: 'Rule Type is not valid',
							validators: {
								notEmpty: {
									message: 'Rule Type is required and can\'t be empty'
								}
							}
						},
						packagesurcharge_rule: {
							message: 'Rule is not valid',
							validators: {
								notEmpty: {
									message: 'Rule is required and can\'t be empty'
								}
							}
						},
						packagesurcharge_rule_range_1: {
							message: 'Rule is not valid',
							validators: {
								notEmpty: {
									message: 'Date From is required and can\'t be empty'
								}
							}
						},
						packagesurcharge_rule_range_2: {
							message: 'Rule is not valid',
							validators: {
								notEmpty: {
									message: 'Date TO is required and can\'t be empty'
								}
							}
						},
						packagesurcharge_rule_day: {
							message: 'Rule is not valid',
							validators: {
								notEmpty: {
									message: 'Day of Week is required and can\'t be empty'
								}
							}
						},
						packagesurcharge_rule_agent: {
							message: 'Rule is not valid',
							validators: {
								notEmpty: {
									message: 'Agent Name is required and can\'t be empty'
								}
							}
						},
						packagesurcharge_rule_out_1: {
							message: 'Rule is not valid',
							validators: {
								notEmpty: {
									message: 'Date From is required and can\'t be empty'
								}
							}
						},
						packagesurcharge_rule_out_2: {
							message: 'Rule is not valid',
							validators: {
								notEmpty: {
									message: 'Date To is required and can\'t be empty'
								}
							}
						},
						packagesurcharge_cost: {
							message: 'The cost is not valid',
							validators: {
								notEmpty: {
									message: 'Cost is required and can\'t be empty'
								},
								greaterThan: {
								    value: 0,
								    message: 'Please enter a value greater than or equal to %s'
								}
							}
						},
						packagesurcharge_profit: {
							message: 'The profit is not valid',
							validators: {
								notEmpty: {
									message: 'Profit is required and can\'t be empty'
								},
								greaterThan: {
								    value: 0,
								    message: 'Please enter a value greater than or equal to %s'
								}
							}
						}
					}
				})
				.on('success.form.bv', function(e) {
					e.preventDefault();
					var $form = $(e.target);
					var bv = $form.data('bootstrapValidator');
					var form = $form[0];
					var formData = new FormData(form);
					var id = $form.find('[name=pckgsrchrg_id]').val();
					var act_type = $form.find('[name=act_type]').val();
					$.ajax({
			            url: $form.attr('action'),
			            data: formData,
			            processData: false,
			            contentType: false,
			            type: 'POST',
			            dataType: 'html',
			            success: function(result){
			            	resultObj = result.split('_:_');
							alert_type = resultObj[0];
							message = resultObj[1];
			                $form.find('.alert').hide();
			                if(alert_type == 'Success'){
								html_data = resultObj[2];
			                    $form.data('bootstrapValidator').resetForm(true);
			                    $('#myModalSurcharge').modal('hide');
			                	if(act_type=='add')
			                		$('#surcharge_table.table > tbody:last').append(html_data);
			                	else
			                		$('#surcharge_table.table').find('tr[packagesurchargeid='+id+']').replaceWith(html_data);
			                } else {
			                	$form.find('.alert').removeClass('alert-success').addClass('alert-warning').html(message).show();
			            	}
			            }
			        });
				});

				//revalidateField for package surcharge
				$('#range_from2').change(function(){
					var d = $("#range_from2").val();
					var res = d.replace(/-/g, ",");

					var $input = $('#range_to2').pickadate()
					var picker = $input.pickadate('picker');
					picker.set('min', res);
					$('#create_packagesurcharge_form').bootstrapValidator('revalidateField', 'packagesurcharge_rule_range_1');
				});
				$('#range_to2').change(function(){
					$('#create_packagesurcharge_form').bootstrapValidator('revalidateField', 'packagesurcharge_rule_range_2');
				});
				$('#blackout_from2').change(function(){
					var d = $("#blackout_from2").val();
					var res = d.replace(/-/g, ",");

					var $input = $('#blackout_to2').pickadate()
					var picker = $input.pickadate('picker');
					picker.set('min', res);
					$('#create_packagesurcharge_form').bootstrapValidator('revalidateField', 'packagesurcharge_rule_out_1');
				});
				$('#blackout_to2').change(function(){
					$('#create_packagesurcharge_form').bootstrapValidator('revalidateField', 'packagesurcharge_rule_out_2');
				});


				//reset bootstrap validator when modal is closed
				$('#myModalEditHotel, #myModalHotel').on('hide.bs.modal', function() {
					$('#edit_hotel_form,#create_hotel_form').bootstrapValidator('resetForm', true);
				});

				if($('#myModalProfile #edit_profile_button').length >0){
					$('#edit_profile_button').click(function(e){
						e.preventDefault();
						var A = $(this);
						A.hide();
						form_parent = A.parents('form');
						$(form_parent).find('.form-control').removeAttr('disabled');
						$(form_parent).find('.form-control.tt-hint').hide();
						$(form_parent).find('#update_profile_button').show();
						$(form_parent).find('#cancel_update_button').show();
					});
				}

				if($('#myModalProfile #cancel_update_button').length >0){
					$('#cancel_update_button').click(function(e){
						e.preventDefault();
						var A = $(this);
						A.hide();
						form_parent = A.parents('form');
						$(form_parent).find('.form-control').attr('disabled','disabled');
						$(form_parent).find('.form-control.tt-hint').show();
						$(form_parent).find('#edit_profile_button').show();
						$(form_parent).find('#update_profile_button').hide();
						$(form_parent).find('#cancel_update_button').hide();
						$('#myModalProfile').modal('hide');
					});
				}

				//calling of hotel surcharge
				$('body').on('click', '[name=addHotelSurcharge_link]', function(e) {
					e.preventDefault();
					var A = $(this);
					hotel_id = A.parents('tr').attr('hotel_id');
					target_modal = '#myModalHotelSurcharge';
					$('#create_hotelsurcharge_form').bootstrapValidator('resetForm', true);
					$(target_modal).find('[name=hotelsurcharge_id]').val('');
					$(target_modal).find('.alert').hide();
					$(target_modal).modal('show');
					$(target_modal).find('[name=hotel_id]').val(hotel_id);
				});

				if($('[name=edit_surcharge_link]').length >0){
					$('body').on('click', '[name=edit_surcharge_link]', function(e) {
					// $('[name=edit_surcharge_link]').click(function(e){
						e.preventDefault();
						var A = $(this);
						hotelsurcharge_id = A.parents('tr').attr('hotelsurcharge_id');
						hotel_id = A.parents('table').parents('tr').attr('hotel_id');
						target_modal = '#myModalHotelSurcharge';
						$('#create_hotelsurcharge_form').bootstrapValidator('resetForm', true);
						$(target_modal).find('form').attr('action',"<?php echo base_url('panel/update_hotelsurcharge')?>");
						$(target_modal).find('[name=hotelsurcharge_id]').val(hotelsurcharge_id);
						$(target_modal).find('[name=hotel_id]').val(hotel_id);
						var form = $(target_modal).find('form');

						$.ajax({
							url: "<?php echo base_url('panel/get_hotelsurcharge'); ?>",
							type: 'POST',
							async : false,
							data : 'id='+hotelsurcharge_id,
							dataType: 'json',
							error: function(){
								form.find('.alert').html('There seems to be an error. Please try again.').show();
								return false;
							},
							success: function(result){
								form.find('.alert').html('').hide();
								if(result.alert_type == 'Success'){
									$.each(result.surcharge_info, function(k, v) {
										if(v) form.find('[name='+k+'].form-control').val(v).show();
										else form.find('[name='+k+'].form-control').val('');
										// console.log(k + ' is ' + v);
									});
									ruleType();
									$(target_modal).modal('show');
								}else{
									$('p#myModalLabel').html(result.message);
									$('#myModalSuccess').modal('show');
								}
							}
						});
					});
				}

				$('#myModalRoomRate').on('hide.bs.modal', function() {
					$form = $('#addhotelroomrate_form');
					$form.bootstrapValidator('resetForm', true);
					url_link = "<?php echo base_url('panel/add_hotelroomrate'); ?>";
					$form.attr('action',url_link);
					$form.find('[name=act_type]').val('add');
				});

				$('#myModalRoomRateSurcharge').on('hide.bs.modal', function() {
					url_link = "<?php echo base_url('panel/add_hotelroomsurcharge'); ?>";
					$form = $('#add_hotelroomsurcharge_form');
					$form.bootstrapValidator('resetForm', true);
					$form.attr('action',url_link);
					$form.find('[name=act_type]').val('add');
				});

				$('body').on('click', '.edit_roomrate_link', function(e) {
					e.preventDefault();
					var A = $(this);
					var target_modal = A.attr('data-target');
					var js_srchrge = false;

					if(target_modal == '#myModalRoomRate'){
						var attr_name = 'roomrateid';
						var url_link = "<?php echo base_url('panel/update_hotelroomrate'); ?>";
						var get_link = "<?php echo base_url('panel/get_hotelroomrate'); ?>";
					} else if(target_modal == '#myModalRoomRateSurcharge'){
						var attr_name = 'roomsurchargeid';
						var url_link = "<?php echo base_url('panel/update_hotelroomsurcharge'); ?>";
						var get_link = "<?php echo base_url('panel/get_hotelroomsurcharge'); ?>";
						var package_hotel_id = A.parents('tr').parents('tr').attr('packagehotelid');
						$('<input>').attr({
						    type: 'hidden',
						    name: 'pckg_htl_id',
						    value: package_hotel_id
						}).prependTo($(target_modal).find('form'));
						js_srchrge = true;
					}

					// $(target_modal).modal('show');
					var id = A.parents('tr').attr(attr_name);
					$form = $(target_modal).find('form');
					$form.attr('action', url_link);
					$form.find('[name=parent_id]').val(id);
					$form.find('[name=act_type]').val('edit');
					$.ajax({
						url: get_link,
						type: 'POST',
						async : false,
						data : 'id='+id,
						dataType: 'json',
						error: function(){
							$form.find('.alert').addClass('alert-danger').html('There seems to be an error. Please try again.').show();
							return false;
						},
						success: function(result){
							$form.find('.alert').html('').hide();
							if(result.alert_type == 'Success'){
								$.each(result.info, function(k, v) {
									if(v) 	{
										if((k == 'hotelroomrate_date_from' || k == 'hotelroomrate_date_to') && v=='0000-00-00'){
											$("#date_range").hide();
											$form.find('[name='+k+'].form-control').val(v).hide();
											console.log('Here:'+k+'-'+v)
										}
										else{
											$("#date_range").show();
											$form.find('[name='+k+'].form-control').val(v).show();
											console.log(v);
										}										
									}
									else 	$form.find('[name='+k+'].form-control').val('');

								});
								if(js_srchrge) ruleType();
							}else $form.find('.alert').html(result.message).show();
								$(target_modal).modal('show');
						}
					});
				});


				$('#myModalHotelSurcharge').on('hide.bs.modal', function() {
					$('#create_hotelsurcharge_form').bootstrapValidator('resetForm', true);
					url_link = "<?php echo base_url('panel/register_hotelsurcharge'); ?>";
					$('#create_hotelsurcharge_form').attr('action',url_link);
				});


				$('body').on('click', '.package_hotel_add', function() {
					var A = $(this);
					var modal = A.attr('data-target');
					$(modal).modal('show');
					if(modal == '#myModalRoomRateSurcharge')  id = A.parents('tr').attr('roomrateid');
					else id = A.parents('tr').attr('packagehotelid');
					// alert(package_hotel_id);
					$(modal).find('[name=parent_id]').val(id);
					$(modal).find('.alert').html('').hide();
				});

				$('body').on('click', '.edit_link', function() {
					$('.em-area').html('');
					var A = $(this);
					target_modal = A.attr('data-target');

					if(target_modal=='#myModalEditHotel'){
						url_link = "<?php echo base_url('panel/get_hotel'); ?>";
						insert_id = 'hotel_id';
					}else if(target_modal=='#myModalEditUser'){
						url_link = "<?php echo base_url('panel/get_user'); ?>";
						insert_id = 'user_id';
					} else return false;

					var id = A.parents('tr').attr(insert_id);
					var arraycom;
					$.ajax({
						url: url_link,
						type: 'POST',
						async : false,
						data : 'id='+id,
						dataType: 'json',
						success: function(result){
							//console.log(result);
							if(result.alert_type == 'Success'){
								var form = $(target_modal).find('form');
								var i = 0;
								form.find('.alert').html('').hide();
								form.find('[name='+insert_id+'].form-control').val(id);
								//console.log(result);

								$.each(result.user_info, function(k, v) {
									if(v){
											form.find('[name='+k+'].form-control').val(v);
										}
									else{
										form.find('[name='+k+'].form-control').val('');
									}
								});
								var e_name = result.user_info.emergency_name;
								var e_phone = result.user_info.emergency_phone_number;

								var e_combi = [];


								if(e_name != null){
								for(var i=0; i<e_name.length; i++){
									e_combi.push({name:e_name[i], phone:e_phone[i]});
								}
								for(var x = 0; x < e_combi.length ; x++){
									$('.em-area').append('<div class="dynamic_emergency"><div class="col-xs-6">'+
                                                '<div class="form-group">'+
                                                    '<label class="col-xs-4 control-label">Emergency Name</label>'+
                                                    '<div class="col-xs-8">'+
                                                        '<input type="text" class="form-control" name="emergency_name[]" value="'+e_combi[x].name+'">'+
                                                    '</div>'+
                                                '</div>'+
                                            '</div>'+
                                            '<div class="col-xs-6">'+
                                                '<div class="form-group">'+
                                                    '<label class="col-xs-4 control-label">Emergency Phone</label>'+
                                                    '<div class="col-xs-8 ge-sctop em-modal">'+
                                                        '<i class="fa fa-minus em-remove"></i><input type="text" class="form-control" name="emergency_phone_number[]" value="'+e_combi[x].phone+'">'+
                                                    '</div>'+
                                                '</div>'+
                                            '</div></div>');
								}

								$('.em-modal i').click(function(){
									$(this).parent().parents('.dynamic_emergency').remove();
								});

								}//END

								$(target_modal).modal('show');
							}else{
								$('p#myModalLabel').html(result.message);
								$('#myModalSuccess').modal('show');
							}
						}
					});
				});
			});

			//custom function for company field on registering user.
			//make mandatory when any user type other than customer is chosen
			(function($) {
				$.fn.bootstrapValidator.validators.customCompany = {
					html5Attributes: {
						message: 'message',
						field: 'field',
						check_only_for: 'check_only_for'
					},

					/**
					* Return true if the input value contains a valid US phone number only
					*
					* @param {BootstrapValidator} validator Validate plugin instance
					* @param {jQuery} $field Field element
					* @param {Object} options Consist of key:
					* - message: The invalid message
					* - field: The name of field that will be used to get selected country
					* - check_only_for : Coma separated ISO 3166 country codes // i.e. 'us,ca'
					* @returns {Boolean}
					*/
					validate: function(validator, $field, options) {
						var value = $field.val();


						var user_type = validator.getFieldElements(options.field);

						if (user_type == null) {
							return true;
						}

						var usertype = user_type.val().trim();

						if(usertype == ''){
							return true;
						}
						var check_list = options.check_only_for.toUpperCase().split(',')

						if(check_list.indexOf(usertype.toUpperCase()) == -1) {
							return true;
						}

						if(value == '') return false;
						else return (/^[a-zA-Z0-9 !@#$%^&*(),'.-]*$/i).test(value);
					}
				}
			}(window.jQuery));
			$(document).ready(function() {

			 	$('.give_permission').click(function() {
	        		agent_id = $(this).val();
	        		$panel_name = $('#permissions');
			  		checked = $(this).is(':checked');
			  		package_id = $panel_name.find('[name=package_hotel_id]').val();
			  		url_link = "<?php echo base_url('panel/update_permission'); ?>";
			  		$.ajax({
					  	url: url_link,
					  	data: 'package_id='+package_id+'&agent_id='+agent_id+'&checked='+checked,
					  	type: 'POST',
					  	dataType: 'json',
					  	success: function(result){
							$panel_name.find('.alert').hide();
							if(result.alert_type == 'Success'){
								$panel_name.find('.alert').removeClass('alert-danger').addClass('alert-success').html(result.message).show();
							}else $panel_name.find('.alert').removeClass('alert-success').addClass('alert-danger').html(result.message).show();
					  	}
					});
			 	});

			 	//check if the last hotel choice was changed ----------------------------------------------
			 	$("[name='package_hotel_choice[]']:last").change(function(){
			 		var package_hotel_id = $(this).val();
			 		var country_code = $(this).attr("destination-code");

			 		//link for the optopnal hotel name ----------------------------------------------
			        url_link1 = "<?php 	echo base_url('panel/getOptionalHotel'); ?>";

			        //dropdown for the extension hotel name and id ----------------------------------------------
			        $.ajax({
			            url: url_link1,
			            data: 'package_hotel_id='+package_hotel_id+"&country_code="+country_code,
			            type: 'POST',
			            dataType: 'html',
			            success: function(result){
			                $('[name=exhotel_name] option').remove();
			                $('[name=exhotel_name]').append(result);
			            },
			            error: function(x){
			            	//alert(x.responseText);
			            }
			        });
			 	});

			 	if($("[name='package_hotel_choice[]']").val() != ''){
			 		//hotel id and hotel-package relation id ----------------------------------------------
			        var package_hotel_id = $("[name='package_hotel_choice[]']").val();
			        var booking_id = $("[name='booking_id']").val();

			        //package id ----------------------------------------------
			       	var package_id= $("[name='package_hotel_choice[]']").attr('package-id');

			      	//url link ----------------------------------------------
			        url_link = "<?php echo base_url('panel/gethotelroom_packagehotelid'); ?>";

			       	//get hotel meta data  ----------------------------------------------
			        pkg_hotel_id = package_hotel_id.split(".");

			        //hotel id ----------------------------------------------
			        var pkg_htl_id = pkg_hotel_id[0];

			        //destination id ----------------------------------------------
			        var destination_id = $("[name='package_hotel_choice[]']").attr("destination-id");

			        //hotel name ----------------------------------------------
			        var pkg_hotel_name = $('#package_hotel_choice_name'+destination_id+'-'+pkg_htl_id).attr("hotel-name");

			        //destination index ----------------------------------------------
			        var destination_index = $("[name='package_hotel_choice[]']").attr("destination-index");

			        //total destinations ----------------------------------------------
			        var total_destinations = parseInt($("#total_destinations").val())-1;

			        //hide suplementary errors ----------------------------------------------
			      	$('#supplementary_errors'+destination_id).hide();

			      	//depart and return values ----------------------------------------------
			      	var depart_value = $("#datetimepickerBookingDepart").val();
			      	var return_value = $("#datetimepickerBookingReturn").val();

			      	//clear the error ----------------------------------------------
			      	$("#blackout_error_container"+destination_id).hide();

			        //set the package hotel name
			        $("#package_hotel_name"+destination_id).val(pkg_hotel_name);

			        //load the spinners ----------------------------------------------
			        $("#roomrates_container"+destination_id).empty();
			        $("#destination_roomrates_loader"+destination_id).show();


			        //ajax for fetching the something.. ----------------------------------------------
			        $.ajax({
			            url: url_link,
			            data: 'package_hotel_id='+package_hotel_id+"&package_id="+package_id+"&depart_value="+depart_value+"&return_value="+return_value+"&destination_index="+destination_index+"&total_destinations="+total_destinations+"&booking_id="+booking_id,
			            type: 'POST',
			            dataType: 'html',
			            success: function(result){
			            	//load the spinners ----------------------------------------------
			       			$("#destination_roomrates_loader"+destination_id).hide();

			                resultObj = result.split('_:_');
			                alert_type = resultObj[0];
			                html_data = resultObj[1];

			                //empty and append the roomrates container ----------------------------------------------
			                $("#roomrates_container"+destination_id).empty().append(html_data);

               				//get the extension ----------------------------------------------
			                var extension = parseInt($('#extended_days').val());

			                if(extension!=0){
			                	$('.extension_container').hide();
			                	$('.extension_container:last').show();
			                }

			                //list all the hotel surcharges ----------------------------------------------
			        		listHotelSurcharges();

			            }, error : function(x){
			            	alert(x.responseText);
			            }
			        });
				}

				//start of select hotel ----------------------------------------------
			 	$("[name='package_hotel_choice[]']").change(function() {
			 		//hotel id and hotel-package relation id ----------------------------------------------
			        var package_hotel_id = $(this).val();

			        //package id ----------------------------------------------
			       	var package_id= $(this).attr('package-id');
			       	var booking_id= $(this).attr('booking-id');

			      	//url link ----------------------------------------------
			        url_link = "<?php echo base_url('panel/gethotelroom_packagehotelid'); ?>";

			       	//get hotel meta data  ----------------------------------------------
			        pkg_hotel_id = package_hotel_id.split(".");

			        //hotel id ----------------------------------------------
			        var pkg_htl_id = pkg_hotel_id[0];

			        //destination id ----------------------------------------------
			        var destination_id = $(this).attr("destination-id");

			        //hotel name ----------------------------------------------
			        var pkg_hotel_name = $('#package_hotel_choice_name'+destination_id+'-'+pkg_htl_id).attr("hotel-name");

			        //destination index ----------------------------------------------
			        var destination_index = $(this).attr("destination-index");

			        //total destinations ----------------------------------------------
			        var total_destinations = parseInt($("#total_destinations").val())-1;

			        //hide suplementary errors ----------------------------------------------
			      	$('#supplementary_errors'+destination_id).hide();

			      	//depart and return values ----------------------------------------------
			      	var depart_value = $("#datetimepickerBookingDepart").val();
			      	var return_value = $("#datetimepickerBookingReturn").val();

			      	//clear the error ----------------------------------------------
			      	$("#blackout_error_container"+destination_id).hide();

			        //set the package hotel name
			        $("#package_hotel_name"+destination_id).val(pkg_hotel_name);

			        //load the spinners ----------------------------------------------
			        $("#roomrates_container"+destination_id).empty();
			        $("#destination_roomrates_loader"+destination_id).show();


			        //ajax for fetching the something.. ----------------------------------------------
			        $.ajax({
			            url: url_link,
			            data: 'package_hotel_id='+package_hotel_id+"&package_id="+package_id+"&depart_value="+depart_value+"&return_value="+return_value+"&destination_index="+destination_index+"&total_destinations="+total_destinations+"&booking_id="+booking_id,
			            type: 'POST',
			            dataType: 'html',
			            success: function(result){
			            	//load the spinners ----------------------------------------------
			       			$("#destination_roomrates_loader"+destination_id).hide();

			                resultObj = result.split('_:_');
			                alert_type = resultObj[0];
			                html_data = resultObj[1];

			                //empty and append the roomrates container ----------------------------------------------
			                $("#roomrates_container"+destination_id).empty().append(html_data);

               				//get the extension ----------------------------------------------
			                var extension = parseInt($('#extended_days').val());

			                if(extension!=0){
			                	$('.extension_container').hide();
			                	$('.extension_container:last').show();
			                }

			                //list all the hotel surcharges ----------------------------------------------
			        		listHotelSurcharges();

			            }, error : function(x){
			            	alert(x.responseText);
			            }
			        });
			    });


			//filter on create booking
	        $('#filter_package_btn').click(function(e){
	        	e.preventDefault();
	        	$A = $(this);
	        	$form 			= $(this).parents('form');
	        	$div_package 	= $('#package_holder');
	        	$div_pagination = $('.row.text-center');
	        	$A.attr('disabled','disabled');

	        	var loader = "<?php echo base_url('./img/loader.gif');?>";
		 		$element = '<center><img id="loader" height="50px" src="'+ loader +'"></center>';
				$form.find('.alert').addClass('alert-warning').css('padding','0').append($element).show();

        		$.post($form.attr('action'), $form.serialize(), function(result) {
        			return_arr 	= result.split('_:_');
        			links	 	= return_arr[0];
        			packages 	= return_arr[1];
        			$div_package.empty();
        			$div_package.append(packages);
        			$div_pagination.find('.pagination').remove();
        			$div_pagination.append(links);
        			$A.removeAttr('disabled');

        			$form.find('.alert').removeClass('alert-warning').css('padding','').hide();
        			$form.find('.alert center').remove();

        			return true;
		    	});
		    });

	        //filter on view_hotels
		    $('#filter_hotel_btn').click(function(e){
	        	e.preventDefault();
	        	$form = $(this).parents('form');
	        	$div_pagination = $('.row.text-center');
        		$.post($form.attr('action'), $form.serialize(), function(result) {
        			return_arr 	= result.split('_:_');
					$div_pagination.find('.pagination').remove();
        			$("#hotelTable > tbody > tr").remove();
        			if(return_arr.length == 2){
						links = return_arr[0];
        				html_data = return_arr[1];
        				$div_pagination.append(links);
        			}else{
        				html_data = return_arr[0];
        			}
        			$("#hotelTable > tbody").append(html_data);
		    	});
		    });

		    //filter on package_edit
		    $('[name=filter_hotel_slct]').change(function(e){
	        	e.preventDefault();
	        	var id = $(this).val();
	        	if(id){
					$('table#packagehotels_table').find('tr[packagehotelid]').hide();
					$('table#packagehotels_table').find('tr[packagehotelid='+ id +']').show();
	        	}else{
					$('table#packagehotels_table').find('tr[packagehotelid]').show();
	        	}
		    });
		});
	
		
	  //   $('[name=package_destination]').change(function(e){
   //      	e.preventDefault();
			// var code = $(this).val();
			// try{
	  //       	$div_package = $('#displays_package_data');
	  //       	$div_pagination = $('.row.text-center');
	  //       	url_link = "<?php echo base_url('panel/filter_view_packages_jqrys'); ?>";
	  //   		$.post(url_link,  {"destination": code} , function(result) {
	  //   			// return false;
	  //   			return_arr 	= result.split('_:_');
	  //   			links	 	= return_arr[0];
	  //   			packages 	= return_arr[1];
	  //   			$div_package.empty();
	  //   			$div_package.append(packages);
	  //   			$div_pagination.find('.pagination').remove();
	  //   			$div_pagination.append(links);
	  //   			return true;
		 //    	});
			// }catch(err){
			// 	bootbox.alert("Package not found!");
			// }
	  //   });

	    $('[name=cbxpackages]').change(function(e){
        	e.preventDefault();
			var code = $(this).val();
			try{
	        	$div_package = $('#id-displayspackagedata');
	        	$div_pagination = $('.row.text-center');
	        	url_link = "<?php echo base_url('panel/filter_view_packages_front_pages'); ?>";
	    		$.post(url_link,  {"destination": code} , function(result) {
	    			// return false;
	    			return_arr 	= result.split('_:_');
	    			links	 	= return_arr[0];
	    			packages 	= return_arr[1];
	    			$div_package.empty();
	    			$div_package.append(packages);
	    			$div_pagination.find('.pagination').remove();
	    			$div_pagination.append(links);
	    			return true;
		    	});
			}catch(err){
				bootbox.alert("Package not found!");
			}
	    });



		/*
		*Anuar
		*/

		</script>
		<script type="text/javascript">
	    	var jsite_url = function(inputUrl) {
                tempUrl = "<?php echo site_url('" + inputUrl +"'); ?>";
                return tempUrl;
            }

	        function delete_dest(code,name,image) {
				$('#myModalDeletePDFTitle').html(name+' file?');
	        	$('#myModalDeletePDFCode').val(code);
	        	$('#myModalDeletePDFFile').val(image);
	        	//$('#myModalDeletePDF').modal('show');
	        	bootbox.confirm("Do you really want to remove this file?", function(result){
	        		if(result){
	        			delete_dest_true();
	        		}
	        	});
	   	   }

	        function delete_dest_true() {
	       		 //	$('#myModalDeletePDF').modal('hide');
	        	var code = $('#myModalDeletePDFCode').val();
	        	var file = $('#myModalDeletePDFFile').val();
	        	//alert(code);
	        	$.post(jsite_url('panel/delete_destination'), {'id' : code,'imagename':file}, function(data) {
                    if(data == '1'){
                        $.post(jsite_url('panel/list_attachment'),function(data){
                        	$('#list_destination').empty().append(data);
                        	$('#success_id').hide();
                        	bootbox.alert("File was successfully removed!");
			            });
                    }else{
                        bootbox.dialog({
								message: "File was not successfully removed!",
								buttons: {
									main: {
										label: "Ok",
										className: "btn-orange",
										callback: function() {
											bootbox.hideAll();
										}
									}
								}
							});
                    }
                });
	        }

			if($("#permissions input.give_permission").filter(':not(:checked)').length === 0){
				$('#permitAll').prop('checked', true);
			}

	        //kirstin
	        $('#permitAll').click(function(){
				$panel_name = $('#permissions');
	        	var agent_ids = [];
				var package_id = $panel_name.find('[name=package_hotel_id]').val();
				var checked = $(this).is(':checked');
        		$panel_name.find('.alert').removeClass('alert-warning alert-success alert-danger').html('').hide();

				$("#permissions input.give_permission").each(function(){
					agent_ids.push($(this).val());
					$(this).prop('checked', checked);
				});

				loader = "<?php echo base_url('./img/loader.gif');?>";
		 		$element = '<center><img id="loader" height="50px" src="'+ loader +'"></center>';
				$panel_name.find('.alert').addClass('alert-warning').css('padding','0').append($element).show();

				$.post("/panel/update_permission_all",{'agent_ids':agent_ids,'package_id':package_id,'checked':checked},function(result) {
					$panel_name.find('.alert center').remove();

					if(result.alert_type == 'Success'){
						$panel_name.find('.alert').removeClass('alert-warning').addClass('alert-success').css('padding','').html(result.message).show();
					}else{
						$panel_name.find('.alert').removeClass('alert-warning').addClass('alert-danger').css('padding','').html(result.message).show();
    				}
				}, 'json');
			});

	        /*
	        *Anuar
	        */
			$(function(){
				$("#button_filter").click(function(){
					$.post(jsite_url('panel/filter_data'), $("#booking_data").serialize(), function(data){
						$('#fucker').hide();
						$('#data_list').empty().append(data);
					});
				});

				$("#attach_id").click(function(){
					alert($('[name=pdf_file]').val());
					$.post(jsite_url('panel/attachment_do_upload'),$("#attach_id").serialize(), function(data){
						$('#list_destination').empty().append(data);
					});
				});
				$("#reports_filter").click(function(){
					$.post(jsite_url('panel/filter_report'), $("#report_data").serialize(), function(data){
						$('#report_list').append(data);
					});
				});
				$("#addpackagehotels_button").click(function(){
					$.post(jsite_url('panel/add_packagehotels'), $("#addpackagehotels_form").serialize(), function(data){
						/*if(data == 'success'){
							bootbox.dialog({
								message: "Hotel is already activate",
								buttons: {
									main: {
										label: "Ok",
										className: "btn-orange",
										callback: function() {
											bootbox.hideAll();
										}
									}
								}
							});
							$('#fck').append(data);
						}else */
						if(data){
							$('#fck').append(data);
						}else{

							//bootbox.alert('Hotel is already on the list');
							bootbox.dialog({
								message: "Hotel is already on the list",
								buttons: {
									main: {
										label: "Ok",
										className: "btn-orange",
										callback: function() {
											bootbox.hideAll();
										}
									}
								}
							});
						}
					});
				});
			});
			$('#all').click(function() {
			    location.reload();
			});

			function status_booking(id,action){
				$('#pleaseWaitDialog').modal();
				$.post(jsite_url('panel/action_booking'),{'id':id,'action':action}, function(data){
					//alert(data);
					// if(data == 'cancelled'){
					// 	$('#action_append').empty().append(data);
					// 	bootbox.alert("Successfully cancelled!");
					// }else if(data == 'rejected'){
					// 	$('#action_append').empty().append(data);
					// 	bootbox.alert("Successfully rejected!");
					// }else if(data == 'confirmed'){
					// 	$('#action_append').empty().append(data);
					// 	bootbox.alert("Successfully confirmed!");
					// }
					// else{
					// 	bootbox.alert("Not successfully!");
					// };
					setTimeout(window.location.reload.bind(window.location), 250);
				});
			}
		</script>

		<!-- niko -->
		<script type="text/javascript">
		$('.showNo').removeClass().addClass('nav nav-second-level');
		</script>
		<script type="text/javascript">
			function clickME(value)
			{

				var id = $(value).attr('id');
			    var url = '<?php echo site_url("panel/collect")?>';
			    var val= $(value).val().split('IamADelimiter');
			    var tot = parseInt($('#totalBookings').text())-1;
			    var totAm = parseInt($('#totalAmount').text())-val[1];

			    var booking_id = $(value).attr("booking_id");


			   $('#loader'+val[0]).css('display','block');
			    $('#'+id).hide();

			  	$.post(url,{data:$(value).val(), booking_id: booking_id},function(result)
			    {
				    result = $.trim(result);
		       		$('#'+val[0]).hide();
			        $('#totalBookings').text('');
			        $('#totalAmount').text('');
			        $('#totalAmount').text(totAm);
			        $('#totalBookings').text(tot);
		       		$("#append_payment_collections").append(result);
			    })
			    .fail(function(x){
			   		//alert(x.responseText);
			    });
			}

		</script>
		<script type="text/javascript">
		/*
		function updateStatus(value)
		{
		    //$('td a').click(function() { fukc(this); return false; });
		    var val = $(value).attr('class').split('DeLeTe');
		    var loader = "<td colspan='4' class='td"+val[0]+"'><center><div class='"+val[0]+"' style='background:url(<?php echo base_url('./img/loader.gif');?>) no-repeat center;height:20px;width:35px;background-size:100% auto;'></div></center></td>";
		    var url = '<?php echo site_url("panel/update_user_status");?>';
		    var newVal = '';
		    var newInfo = '';
		    var edit = '<a href="#" class="edit_link" data-target="#myModalEditUser">Edit</a> | ';

		    $('.tr'+val[0]+' td').remove();
		    $('.tr'+val[0]).append(loader);
		    $.post(url,{value:$(value).attr('class')},function(result)
		    {
		        var ac = "activate";var dl="delete";var de="deactivate";

		        result = $.trim(result).split('DeLeTe');

		        if(result[7] === 'pending')
		        {
		            newVal = '<td>'+
		                        edit+
		                        '<a href="#" class="'+result[0]+'DeLeTe'+ac+'DeLeTe'+val[2]+'" onclick="updateStatus(this);">Activate</a> | '+
		                        '<a href="#" class="'+result[0]+'DeLeTe'+dl+'DeLeTe'+val[2]+'" onclick="updateStatus(this);">Delete</a>  '+
		                      '</td>';
		        }
		        else if(result[7] === 'active')
		        {
		            newVal = '<td>'+
		                        edit+
		                        '<a href="#" class="'+result[0]+'DeLeTe'+de+'DeLeTe'+val[2]+'" onclick="updateStatus(this);">Deactivate</a> | '+
		                        '<a href="#" class="'+result[0]+'DeLeTe'+dl+'DeLeTe'+val[2]+'" onclick="updateStatus(this);">Delete</a>  '+
		                      '</td>';
		        }
		        else if(result[7] === 'inactive')
		        {
		            newVal = '<td>'+
		                        edit+
		                        '<a href="#" class="'+result[0]+'DeLeTe'+ac+'DeLeTe'+val[2]+'" onclick="updateStatus(this);">Activate</a> | '+
		                        '<a href="#" class="'+result[0]+'DeLeTe'+dl+'DeLeTe'+val[2]+'" onclick="updateStatus(this);">Delete</a>  '+
		                      '</td>';
		        }
		        else if(result[7] === 'deleted')
		        {

			            // newVal = '<td>'+
			            //             edit+
			            //             '<a href="#" class="'+result[0]+'DeLeTe'+de+'DeLeTe'+val[2]+'" onclick="updateStatus(this);">Deactivate</a> | '+
			            //             '<a href="#" class="'+result[0]+'DeLeTe'+ac+'DeLeTe'+val[2]+'" onclick="updateStatus(this);">Activate</a>  '+
			            //           '</td>';

		        }
		        newInfo = '<td name="name_td">'+result[3]+', '+result[1]+'</td><td name="company_td">'+val[2]+'</td><td name="email_td">'+result[5]+'</td>';
		        setTimeout(function()
		        {
		            $('.tr'+val[0]+' .td'+val[0]).remove();
		            $('.tr'+val[0]).append(newVal+newInfo);
		        },750);
		        //$('#tr'+val[0]+' #td'+val[0]).remove();

		    });

		}
		niko
		*/
		function updateStatus(value){

		    $closest_tr = $(value).parents('tr');
		    var actn 	= $(value).html();
		    var newVal 	= '';
		    var user_id = $closest_tr.attr('user_id');
		    var loader 	= "<td colspan='4' class='td"+user_id+"'><center><div class='"+user_id+"' style='background:url(<?php echo base_url('./img/loader.gif');?>) no-repeat center;height:20px;width:35px;background-size:100% auto;'></div></center></td>";
		    var url 	= '<?php echo site_url("panel/update_user_status");?>';
		    var edit 	= '<a href="#" class="edit_link" data-target="#myModalEditUser">Edit</a> | ';
		    $cloned 	= $closest_tr.find('td:not(:first-child)').clone();

		    if(actn == 'Delete'){
				if (!confirm('Are you sure you want to delete this user?'))
	        		return false;
		    }

		    $closest_tr.find('td').remove();
		    $closest_tr.append(loader);

			$('#myModalSuccess').find('#myModalLabel').html('').hide();

		    $.post(url,{ 'user_id':user_id,'action':actn },function(result){
		        if (result === 'deleted') {
		            $closest_tr.remove();
		            $('#myModalSuccess').modal('show');
		            $('#myModalSuccess').find('#myModalLabel').html('You have deleted this user').show();
		        } else {
		        	if (result === 'pending') {
			            newVal = '<td>'+
			                        edit+
			                        '<a href="#" onclick="updateStatus(this);">Activate</a> | '+
			                        '<a href="#" onclick="updateStatus(this);">Delete</a>  '+
			                      '</td>';
			        } else if(result === 'active') {
			            newVal = '<td>'+
			                        edit+
			                        '<a href="#" onclick="updateStatus(this);">Deactivate</a> | '+
			                        '<a href="#" onclick="updateStatus(this);">Delete</a>  '+
			                      '</td>';
			        } else if (result === 'inactive') {
			            newVal = '<td>'+
			                        edit+
			                        '<a href="#" onclick="updateStatus(this);">Activate</a> | '+
			                        '<a href="#" onclick="updateStatus(this);">Delete</a>  '+
			                      '</td>';
			        }

			        setTimeout(function(){
			        	$closest_tr.find('td').remove();
			            $closest_tr.append(newVal);
			            $closest_tr.append($cloned);
			        },500);
		        }
		    });

		}
		//Destination
		$('body').on('click', '.edit_destination_link', function(e) {
			e.preventDefault();
			var A = $(this);
			var parent_tr = A.parents('tr');
			var code = parent_tr.find('[name=code_td]').html();
			var name = parent_tr.find('[name=name_td]').html();

			//creating elements to "replace" the old row
			$elems =  '';
			$elems += '<td name="btn_td"> <a class="save_edit btn btn-orange"><i class="fa fa-check fa-fw"></i> Save</a> <a class="cancel_edit btn btn-default"><i class="fa fa-times fa-fw"></i> Cancel</a>'
    		$elems += '<td name="name_td" class=""><input type="text" class="yopak form-control" name="country"  value="'+ name +'"></td>';
    		$elems += '<td name="code_td" class=""><input type="text" class="yopak form-control" name="code"  value="'+ code +'" maxlength="3"></td>';

    		parent_tr.find('td').hide();
    		parent_tr.append($elems);

    		if (($('#countries .has-error').length == 0)) $('#countries').find('.alert').removeClass('alert-warning alert-success alert-danger').html('').hide();
		});

		$('body').on('click', '.changeStatus', function(e) {
			e.preventDefault();
			$A 			  = $(this);
			$parent_tr 	  = $A.parents('tr');
			var code  = $parent_tr.attr('destcode');
			$.post(jsite_url('panel/edit_destination'),{'code':code}, function(result){
				bootbox.alert("Deactivate successfully..");
			});
			setTimeout(window.location.reload.bind(window.location), 250);
		});

		$('body').on('click', '.changeStatusActive', function(e) {
			e.preventDefault();
			$A 			  = $(this);
			$parent_tr 	  = $A.parents('tr');
			var code_active  = $parent_tr.attr('destcode');
			$.post(jsite_url('panel/edit_destination'),{'code_active':code_active}, function(result){
				bootbox.alert("Activate successfully..");
			});
			setTimeout(window.location.reload.bind(window.location), 250);
		});

		$('body').on('click', '.cancel_edit', function(e) {
			e.preventDefault();
			$tab_id = $('#countries');

			var A = $(this);
			var parent_tr = A.parents('tr');

    		parent_tr.find('td:not(:hidden)').remove();
    		parent_tr.find('td:hidden').show();

    		if (($('#countries .has-error').length == 0)) $('#countries').find('.alert').removeClass('alert-warning alert-success alert-danger').html('').hide();
		});

		$('#countries').on('blur', '.yopak', function(e) {
			$A = $(this);
			$parent_td = $A.parents('td');
			$parent_td.attr('class','');
			var attr = $A.attr('name');

			if (attr == 'country') {
				if($.trim($A.val())=='') $parent_td.attr('class','has-error');
				else $parent_td.attr('class','has-success');
			}
			if (attr == 'code') {
				if ( !(/^[A-Z]{2,3}$/.test($A.val())) ) $parent_td.attr('class','has-error');
				else $parent_td.attr('class','has-success');
			}
		});

		$('body').on('click', '.save_edit', function(e) {
			e.preventDefault();
			//default objects nga important to manipulate
			$tab_id = $('#countries');

			//works as a simple validator.. but gi.isa lang para dili heavy.
			if (($('#countries .has-error').length > 0)){
				$tab_id.find('.alert').addClass('alert-warning').css('padding','').html('All fields are required. Please follow correct format for Destination and Code.').show();
				return false;
			}

			//set default
			$tab_id.find('.alert').removeClass('alert-warning alert-success alert-danger').css('padding','').html('').hide();

			//default objects nga important to manipulate
			$A 			  = $(this);
			$parent_tr 	  = $A.parents('tr');
			$parent_td 	  = $A.parents('td');

			//get parameters to be submitted
			var old_code  = $parent_tr.attr('destcode');
			var country   = $parent_tr.find('[name=country]').val();
			var new_code  = $parent_tr.find('[name=code]').val();

			//what basically happens first before saving
			var loader 	  = "<?php echo base_url('../img/loader.gif');?>";
	 		$element 	  = '<center><img id="loader" height="40px" src="'+ loader +'"></center>';
			$parent_td.find('a').hide();
			$parent_td.append($element);

    		$.post('/panel/edit_destination', {'old_code':old_code,'new_code':new_code,'country':country}, function(result){
				if(result.alert_type == 'Success'){
					$parent_tr.find('td:not(:hidden)').remove();
    				$parent_tr.attr('destcode', result.info.code);
    				$parent_tr.find('td[name=name_td]').html(result.info.country);
    				$parent_tr.find('td[name=code_td]').html(result.info.code);
    				$parent_tr.find('td:hidden').show();
					$tab_id.find('.alert').addClass('alert-success').html(result.message);
				} else {
					$tab_id.find('.alert').addClass('alert-danger').html(result.message).show();
					$parent_td.find('center').remove();
					$parent_td.find('a').show();
				}
    		}, 'json');
		});

		//Destination

		//Airport
		$('body').on('click', '.edit_airportcode_link', function(e) {
			e.preventDefault();
			var A = $(this);
			var parent_tr = A.parents('tr');
			var code = parent_tr.find('[name=code_td]').html();
			var name = parent_tr.find('[name=name_td]').html();

			//creating elements to "replace" the old row
			$elems =  '';
			$elems += '<td name="btn_td"> <a class="save_edit_airportcode btn btn-orange"><i class="fa fa-check fa-fw"></i> Save</a> <a class="cancel_edit_airportcode btn btn-default"><i class="fa fa-times fa-fw"></i> Cancel</a>'
    		$elems += '<td name="name_td" class=""><input type="text" class="kayop form-control" name="name"  value="'+ name +'"></td>';
    		$elems += '<td name="code_td" class=""><input type="text" class="kayop form-control" name="code"  value="'+ code +'" maxlength="3"></td>';

    		parent_tr.find('td').hide();
    		parent_tr.append($elems);

    		if (($('#airportcodes .has-error').length == 0)) $('#airportcodes').find('.alert').removeClass('alert-warning alert-success alert-danger').html('').hide();
		});

		$('body').on('click', '.change_Status', function(e) {
			e.preventDefault();
			$A 			  = $(this);
			$parent_tr 	  = $A.parents('tr');
			var code  = $parent_tr.attr('airportcode');
				$.post(jsite_url('panel/edit_airportcode'),{'code':code}, function(result){
						bootbox.alert("Deactivate successfully..");
					});
				setTimeout(window.location.reload.bind(window.location), 250);
			});

		$('body').on('click', '.changeStatus_Active', function(e) {
			e.preventDefault();
			$A 			  = $(this);
			$parent_tr 	  = $A.parents('tr');
			var code_active  = $parent_tr.attr('airportcode');
				$.post(jsite_url('panel/edit_airportcode'),{'code_active':code_active}, function(result){
						bootbox.alert("Activate successfully..");
					});
				setTimeout(window.location.reload.bind(window.location), 250);
			});

		$('body').on('click', '.cancel_edit_airportcode', function(e) {
			e.preventDefault();
			$tab_id = $('#airportcodes');

			var A = $(this);
			var parent_tr = A.parents('tr');

    		parent_tr.find('td:not(:hidden)').remove();
    		parent_tr.find('td:hidden').show();

    		if (($('#airportcodes .has-error').length == 0)) $('#airportcodes').find('.alert').removeClass('alert-warning alert-success alert-danger').html('').hide();
		});

		$('#airportcodes').on('blur', '.kayop', function(e) {
			$A = $(this);
			$parent_td = $A.parents('td');
			$parent_td.attr('class','');
			var attr = $A.attr('name');

			if (attr == 'name') {
				if($.trim($A.val())=='') $parent_td.attr('class','has-error');
				else $parent_td.attr('class','has-success');
			}
			if (attr == 'code') {
				if ( !(/^[A-Z]{2,3}$/.test($A.val())) ) $parent_td.attr('class','has-error');
				else $parent_td.attr('class','has-success');
			}
		});

		$('body').on('click', '.save_edit_airportcode', function(e) {
			e.preventDefault();
			//default objects nga important to manipulate
			$tab_id = $('#airportcodes');

			//works as a simple validator.. but gi.isa lang para dili heavy.
			if (($('#airportcodes .has-error').length > 0)){
				$tab_id.find('.alert').addClass('alert-warning').css('padding','').html('All fields are required. Please follow correct format for Destination and Code.').show();
				return false;
			}

			//set default
			$tab_id.find('.alert').removeClass('alert-warning alert-success alert-danger').css('padding','').html('').hide();

			//default objects nga important to manipulate
			$A 			  = $(this);
			$parent_tr 	  = $A.parents('tr');
			$parent_td 	  = $A.parents('td');

			//get parameters to be submitted
			var old_code  = $parent_tr.attr('airportcode');
			var name   = $parent_tr.find('[name=name]').val();
			var new_code  = $parent_tr.find('[name=code]').val();

			//what basically happens first before saving
			var loader 	  = "<?php echo base_url('../img/loader.gif');?>";
	 		$element 	  = '<center><img id="loader" height="40px" src="'+ loader +'"></center>';
			$parent_td.find('a').hide();
			$parent_td.append($element);

    		$.post('/panel/edit_airportcode', {'old_code':old_code,'new_code':new_code,'name':name}, function(result){
				if(result.alert_type == 'Success'){
					$parent_tr.find('td:not(:hidden)').remove();
    				$parent_tr.attr('airportcode', result.info.code);
    				$parent_tr.find('td[name=name_td]').html(result.info.name);
    				$parent_tr.find('td[name=code_td]').html(result.info.code);
    				$parent_tr.find('td:hidden').show();
					$tab_id.find('.alert').addClass('alert-success').html(result.message);
				} else {
					$tab_id.find('.alert').addClass('alert-danger').html(result.message).show();
					$parent_td.find('center').remove();
					$parent_td.find('a').show();
				}
    		}, 'json');
		});

		$('.btn').click(function(){
			var total = 0;
			var allVals = [];
			var arr =[];
			    $("input[type=checkbox]:checked").each(function(index, element) {
		    		allVals.push($(this).val());
		    		var array = $(this).val().split("IamADelimiter");
				    	$.each(array,function(i){
						    arr=array[i];
						});
			    	total = total + parseFloat(arr);
			    });
		    	$('#result_collection').empty().append(total);
		    	$('#result_total_payment').val(total);
	    		$('#arr_res').val(allVals);
		});

		$('.btn_provdr').click(function(){
			var total = 0;
			var allVals = [];
			var arr =[];
			    $("input[type=checkbox]:checked").each(function(index, element) {
		    		allVals.push($(this).val());
		    		var array = $(this).val().split("IamADelimiter");
				    	$.each(array,function(i){
						    arr=array[i];
						});
			    	total = total + parseFloat(arr);
			    });
		    	$('#result_collection1').empty().append(total);
		    	$('#result_total_payment1').val(total);
	    		$('#chkCollected').val(allVals);

		});

		$('#package_id_').change(function(){
			 var id = this.value;
			$.post(jsite_url('panel/package_hotel_'),{'id':id}, function(data){
				if(data != ""){
				$('#hotel_name').empty().append(data);
				}
			});
		});

		$('#destination_').change(function(){
			 var code = this.value;
			$.post(jsite_url('panel/destination_'),{'code':code}, function(data){
				if(data != ""){
					$('#package_id_').empty().append(data);
				}
			});
		});

		function pay_provider(id){
			$.post(jsite_url('panel/pay_provider'),{'id':id}, function(data){
				if(data != ""){
					//bootbox.alert("Successfully paid");
					window.location.replace("<?php echo base_url('panel/payment?tab=add'); ?>");
				}
			});
		}

		$('#CheckAll').click(function(){
			if(this.checked) { // check select status
				$('input:visible').attr('checked','checked'); // check select status
				this.checked = true;  //select all checkboxes with class "checkbox1"
					var total = 0;
					var sum = 0;
					var allVals = [];
					var arr =[];
				    $("input[type=checkbox]:checked").each(function(index, element) {
			    		allVals.push($(this).val());
			    		var array = $(this).val().split("IamADelimiter");
					    	$.each(array,function(i){
							    arr=array[i];
							});
				    	total = isNaN(total) + parseFloat(arr);
				    	if(!isNaN(total)){
				    	sum = sum + total;//sum result without NaN value
				    	}
				    });
				    $('#result_collections').empty().append(sum);
		    		$('#result_total_payments').append(sum);
	    			$('#arr_res').val(allVals);
	    			$('#result_collection').hide();
	    			$('#result_total_payment').hide();
			}else{
				$('input:checkbox').each(function() { //loop through each checkbox
				this.checked = false; //deselect all checkboxes with class "checkbox1"
				//$(".sum").prop('disabled',true);
				});
			}

		});


		$('document').ready(function(){
			if($('.dest_view').length) {
				//catch on shown the tab event
				$(".dest_view").on('shown.bs.tab', function (e) {
					//save the latest tab; use cookies if you like 'em better:
					localStorage.setItem('lastTab_customer', $(e.target).attr('id'));
				});

				//get the last tab
				var lastTab = localStorage.getItem('lastTab_customer');
				if(lastTab!=undefined && $.trim(lastTab).length != 0){
					$("#"+lastTab).tab("show");
					$("#"+lastTab).click();
				}
				else {
					$(".dest_view").find("li:eq(0)").find("a").click();
				}
			}
		});

		$('document').ready(function(){
			if($('.dest_').length) {
				//catch on shown the tab event
				$(".dest_").on('shown.bs.tab', function (e) {
					//save the latest tab; use cookies if you like 'em better:
					localStorage.setItem('lastTab_customer', $(e.target).attr('id'));
				});

				//get the last tab
				var lastTab = localStorage.getItem('lastTab_customer');
				if(lastTab!=undefined && $.trim(lastTab).length != 0){
					$("#"+lastTab).tab("show");
					$("#"+lastTab).click();
				}
				else {
					$(".dest_").find("li:eq(0)").find("a").click();
				}
			}
		});

		function clearCookie()
		{
			localStorage.setItem('lastTab_customer', $('#first_code').val());
		}

		function changeCode(val)
		{
			window.location.href = "<?php echo base_url('panel/view_packagess'); ?>" + "/" + val;
		}

		function changeCodes(val)
		{
			window.location.href = "<?php echo base_url('panel/create_bookings'); ?>" + "/" + val;
		}

		function change_pkg_view($code) {
			//alert($code);
			$("[name='package_destinations']").find("option").removeAttr("selected");

			//$("[name='tab_select']").val($code);

			$("[name='package_destinations']").find("[value='"+$code+"']").attr("selected","selected");
			$("[name='package_destinations']").change();

			$('[name=package_destinations]').change(function(e){
	        	e.preventDefault();
				var code = $(this).val();
				//alert(code);
	        	$div_package = $('#display');
	        	$div_pagination = $('.row.text-center');
	        	url_link = "<?php echo base_url('panel/filter_view_packages_jqry'); ?>";
	    		$.post(url_link,  {"destination": code} , function(result) {
	    			// return false;
	    			return_arr 	= result.split('_:_');
	    			links	 	= return_arr[0];
	    			packages 	= return_arr[1];
	    			$div_package.empty();
	    			$div_package.append(packages);
	    			$div_pagination.find('.pagination').remove();
	    			$div_pagination.append(links);
	    			return true;
		    	});

		    });

			//$('.s').attr("selected","selected");
			// window.location.href="/panel/view_packages/#"+code;
		}

		function change_pkg_views($code,$data) {

			$("[name='package_destination']").find("option").removeAttr("selected");

			//$("[name='tab_select']").val($code);

			$("[name='package_destination']").find("[value='"+$code+"']").attr("selected","selected");
			$("[name='package_destination']").change();

			$('[name="package_destination"]').change(function(e){
	        	e.preventDefault();
				var code = $(this).val();
				try{
		        	$div_package = $('#displays_package_data');
		        	$div_pagination = $('.row.text-center');
		        	url_link = "<?php echo base_url('panel/filter_view_packages_jqrys'); ?>";
		    		$.post(url_link,  {"destination": code} , function(result) {
		    			//alert(code);
		    			return_arr 	= result.split('_:_');
		    			links	 	= return_arr[0];
		    			packages 	= return_arr[1];
		    			$div_package.empty();
		    			$div_package.append(packages);
		    			$div_pagination.find('.pagination').remove();
		    			$div_pagination.append(links);
		    			return true;
			    	});
				}catch(err){
					bootbox.alert("Package not found!");
				}
		    });

			//$('.s').attr("selected","selected");

		}

		function change_pkg_views_front_pages($code,$data) {
			var data_id = $.trim($($data).attr('id'));
			$("[name='cbxpackages']").val(data_id).trigger('change');
		}

		

		$('document').ready(function(){
			if($('.dest_front_pages').length) {
				//catch on shown the tab event
				$(".dest_front_pages").on('shown.bs.tab', function (e) {
					//save the latest tab; use cookies if you like 'em better:
					localStorage.setItem('lastTab_customers', $(e.target).attr('id'));
				});

				//get the last tab
				var lastTab = localStorage.getItem('lastTab_customers');
				if(lastTab!=undefined && $.trim(lastTab).length != 0){
					$("#"+lastTab).tab("show");
					$("#"+lastTab).click();
				}
				else {
					$(".dest_front_pages").find("li:eq(0)").find("a").click();
				}
			}

			// $('body').on('hidden.bs.modal', '.modal', function () {
  			// 	$(this).removeData('bs.modal');
			// }).on('shown.bs.modal', '.modal', function () {
  			// 	$(this).removeData('bs.modal');
			// });
		})

		$('document').ready(function(){
			if($('.price_').length) {
				//catch on shown the tab event
				$(".price_").on('shown.bs.tab', function (e) {
					//save the latest tab; use cookies if you like 'em better:
					localStorage.setItem('lastTab_customers', $(e.target).attr('id'));
				});

				//get the last tab
				var lastTab = localStorage.getItem('lastTab_customers');
				if(lastTab!=undefined && $.trim(lastTab).length != 0){
					$("#"+lastTab).tab("show");
					$("#"+lastTab).click();
				}
				else {
					$(".price_").find("li:eq(0)").find("a").click();
				}
			}

			// $('body').on('hidden.bs.modal', '.modal', function () {
  			// 	$(this).removeData('bs.modal');
			// }).on('shown.bs.modal', '.modal', function () {
  			// 	$(this).removeData('bs.modal');
			// });
		})

		function difference($id,$difference) {
			$('#myModalDifference').modal('show').on("shown.bs.modal", function() {
				$(this).find("form").attr("action", $("html").attr("base-url") + "panel/getDiscountPayment/" + $id);
				$(this).find("[name='difference']").attr("value", $difference);
				$(this).find("[name='old_urls']").attr("value", window.location);
			});
		}
		function formatAlphaNum(el) {
			el.value = el.value.replace(/[^0-9-+]/gi,'').replace(/([0-9])([0-9]{2})$/, '$1.$2').replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
		}

		$('.print-packages').click(function(){
			$.post(jsite_url('panel/Printpackages'),{'id':$('[name="htxtpackagesid"]').val()}, function(data){
				console.log(data);
			});
		});
		//Airport
		</script>
		<script src="<?php echo base_url('js/picker.js'); ?>"></script>
		<script src="<?php echo base_url('js/picker.date.js'); ?>"></script>
		<script src="<?php echo base_url('js/picker.date.setting.js'); ?>"></script>
		<script src="<?php echo base_url('js/panel/booking_lester.js'); ?> "></script>
		<script src="<?php echo base_url('js/panel/step_booking.js'); ?> "></script>
		<script src="<?php echo base_url('js/panel/general.js'); ?>"></script>
		<script src="<?php echo base_url('js/panel/bootstrap-table.js'); ?>"></script>

</body>
