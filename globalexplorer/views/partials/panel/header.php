<!DOCTYPE html>
<html lang="en" base-url="<?php echo base_url(); ?>">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="<?php echo base_url('img/icon.png'); ?>">

        <title>Global Explorer</title>

        <!-- Bootstrap Core CSS -->
        <link href="<?php echo base_url('css/bootstrap.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('css/bootstrap-datetimepicker.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('css/bootstrapValidator.css'); ?>" rel="stylesheet">

        <!-- date picker within modal -->
        <link href="<?php echo base_url('css/default.css'); ?>" rel="stylesheet" id="theme_base">
        <link href="<?php echo base_url('css/default.date.css'); ?>" rel="stylesheet" id="theme_base">

        <!-- Custom CSS -->
        <link href="<?php echo base_url('css/sb-admin-2.css'); ?>" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="<?php echo base_url('css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url('css/bootstrap-table.css'); ?>" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <![endif]
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>-->
        <script src="<?php echo base_url('js/panel/html5shiv.js'); ?>"></script>
        <script src="<?php echo base_url('js/panel/respond.js'); ?>"></script>
        <link href="<?php echo base_url('css/carousel.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('css/dashboard.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('css/typeahead_custom.css'); ?>" rel="stylesheet">
        <input type="hidden" value=' <?php echo (isset($companies)) ? $companies : json_encode(array()); ?> ' id="json_value_dd">
        <link href="<?php echo base_url('css/animate.css'); ?>" rel="stylesheet">
        <style type="text/css">
            body {
                height:100%;
            }
            .page_display {
                border-right:3px solid #e95a3e;
            }
            .showNo {
                display:none;
            }
            #page-wrapper {
                min-height: 765px;
            }
            #tags{
              float:left;
              border:1px solid #ccc;
              padding:5px;
              font-family:Arial;
            }
            #tags span.tag{
              cursor:pointer;
              display:block;
              float:left;
              color:#fff;
              background:#689;
              padding:5px;
              padding-right:25px;
              margin:4px;
            }
            #tags span.tag:hover{
              opacity:0.7;
            }
            #tags span.tag:after{
             position:absolute;
             content:"x";
             border:1px solid;
             padding:0 4px;
             margin:3px 0 10px 5px;
             font-size:10px;
            }
            #tags input{
              background:#eee;
              border:0;
              margin:4px;
              padding:7px;
              width:auto;
            }
        </style>
        <script src="<?php echo base_url('js/jquery-1.11.0.js'); ?>"></script>
    </head>

    <body>
        <div class="batch-wrap"></div>
        <input type="hidden" id="base_url" value="<?php echo base_url(); ?>">
        <div id="wrapper">
            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo base_url(); ?>">Global Explorer</a>
                </div>
                <!-- /.navbar-header -->


                <ul class="nav navbar-top-links navbar-right">
                    <!--
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-envelope fa-fw"></i>  <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-nav dropdown-messages">
                            <li>
                                <a href="#">
                                    <div>
                                        <strong>John Smith</strong>
                                        <span class="pull-right text-muted">
                                            <em>Yesterday</em>
                                        </span>
                                    </div>
                                    <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <strong>John Smith</strong>
                                        <span class="pull-right text-muted">
                                            <em>Yesterday</em>
                                        </span>
                                    </div>
                                    <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <strong>John Smith</strong>
                                        <span class="pull-right text-muted">
                                            <em>Yesterday</em>
                                        </span>
                                    </div>
                                    <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a class="text-center" href="#">
                                    <strong>Read All Messages</strong>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-tasks fa-fw"></i>  <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-nav dropdown-tasks">
                            <li>
                                <a href="#">
                                    <div>
                                        <p>
                                            <strong>Task 1</strong>
                                            <span class="pull-right text-muted">40% Complete</span>
                                        </p>
                                        <div class="progress progress-striped active">
                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                                <span class="sr-only">40% Complete (success)</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <p>
                                            <strong>Task 2</strong>
                                            <span class="pull-right text-muted">20% Complete</span>
                                        </p>
                                        <div class="progress progress-striped active">
                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                                <span class="sr-only">20% Complete</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <p>
                                            <strong>Task 3</strong>
                                            <span class="pull-right text-muted">60% Complete</span>
                                        </p>
                                        <div class="progress progress-striped active">
                                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                                <span class="sr-only">60% Complete (warning)</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <p>
                                            <strong>Task 4</strong>
                                            <span class="pull-right text-muted">80% Complete</span>
                                        </p>
                                        <div class="progress progress-striped active">
                                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                                <span class="sr-only">80% Complete (danger)</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a class="text-center" href="#">
                                    <strong>See All Tasks</strong>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-bell fa-fw"></i>  <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-nav dropdown-alerts">
                            <li>
                                <a href="#">
                                    <div>
                                        <i class="fa fa-comment fa-fw"></i> New Comment
                                        <span class="pull-right text-muted small">4 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                        <span class="pull-right text-muted small">12 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <i class="fa fa-envelope fa-fw"></i> Message Sent
                                        <span class="pull-right text-muted small">4 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <i class="fa fa-tasks fa-fw"></i> New Task
                                        <span class="pull-right text-muted small">4 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                        <span class="pull-right text-muted small">4 minutes ago</span>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a class="text-center" href="#">
                                    <strong>See All Alerts</strong>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </li>
                        </ul>
                    </li>
                    -->

                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-nav dropdown-user">
                            <li><a href="" data-toggle="modal" data-target="#myModalProfile"><i class="fa fa-user fa-fw"></i> User Profile</a>
                            </li>
                            <!-- <li><a href="" data-toggle="modal" data-target="#myModalSetting"><i class="fa fa-gear fa-fw"></i> Settings</a>
                            </li> -->
                            <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home fa-fw"></i> View Homepage</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="<?php echo base_url('access/logout'); ?>" onclick="clearCookie()"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                            </li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                    <!-- /.dropdown -->
                </ul>
                <!-- /.navbar-top-links -->
                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu">
                        <?php if (isset($user) && get_class($user)== 'Admin'): ?>
                            <li class="<?php if (isset($page) && $page == 'index' ||  $page == 'pending_bookings'): ?>active<?php endif; ?>">
                                <a href="#"><i class="fa fa-book fa-fw"></i> Booking<span class="fa arrow"></span></a>
                                <?php if($page !== 'index' && $page !== 'pending_bookings') $showNo = 'showNo'; else $showNo=''; ?>
                                <ul class="nav nav-second-level <?php echo $showNo;?>">
                                    <li>
                                        <a href="<?php echo base_url('panel/pending_bookings')?>" class=<?php if($page == 'pending_bookings'): ?> "page_display" <?php endif; ?>>Pending</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('panel/')?>" class=<?php if($page == 'index'): ?> "page_display" <?php endif; ?>>All</a>
                                    </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                            <li class="<?php if (isset($page) && $page == 'create_package' || $page == 'view_packages' ||  $page == 'package_edit'): ?>active<?php endif; ?>">
                                <a href="#"><i class="fa fa-plane fa-fw"></i> Packages<span class="fa arrow"></span></a>
                                <?php if($page !== 'create_package' && $page !== 'view_packages') $showNo = 'showNo'; else $showNo=''; ?>
                                <ul class="nav nav-second-level <?php echo $showNo;?>">
                                    <li>
                                        <a href="<?php echo base_url('panel/create_package')?>" class=<?php if($page == 'create_package'): ?> "page_display" <?php endif; ?>>New</a>
                                    </li>
                                    <li>
                                        <a onclick="clearCookie()"  href="<?php echo base_url('panel/clearSessions/view_packages')?>" class=<?php if($page == 'view_packages'): ?> "page_display" <?php endif; ?>>All</a>
                                    </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                            <li class="<?php if (isset($page) && $page == 'view_hotels'): ?>active<?php endif; ?>">
                                <a href="<?php echo base_url('panel/view_hotels')?>"><i class="fa fa-building fa-fw"></i> Hotels</a>
                            </li>
                            <li class="<?php if (isset($page) && $page == 'manage_user'): ?>active<?php endif; ?>">
                                <a href="<?php echo base_url('panel/manage_user')?>"><i class="fa fa-user fa-fw"></i> Users</a>
                            </li>

                            <li class="<?php if (isset($page) && $page == 'payment'): ?>active<?php endif; ?>">
                                <a href="<?php echo base_url('panel/payment')?>"><i class="fa fa-money fa-fw"></i> Payments</a>
                            </li>

                            <li class="<?php if (isset($page) && $page == 'attachment'): ?>active<?php endif; ?>">
                                <a href="<?php echo base_url('panel/attachment')?>"><i class="fa fa-file-pdf-o fa-fw"></i> Attachments</a>
                            </li>
                            <li class="<?php if (isset($page) && $page == 'report'): ?>active<?php endif; ?>">
                                <a href="<?php echo base_url('panel/report')?>"><i class="fa fa-folder-open fa-fw"></i> Reports</a>
                            </li>
                            <li class="<?php if (isset($page) && $page == 'mods_destination' || $page == 'mods_airport'): ?>active<?php endif; ?>">
                                <a href="#"><i class="fa fa-cogs fa-fw"></i> Mods<span class="fa arrow"></span></a>
                                <?php if($page !== 'mods_airport' && $page !== 'mods_airport') $showNo = 'showNo'; else $showNo=''; ?>
                                <ul class="nav nav-second-level <?php echo $showNo;?>">
                                    <li>
                                        <a href="<?php echo base_url('panel/mods_destination')?>" class=<?php if($page == 'mods_destination'): ?> "page_display" <?php endif; ?>>Destination</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('panel/mods_airport')?>" class=<?php if($page == 'mods_airport'): ?> "page_display" <?php endif; ?>>Airport</a>
                                    </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                            <li class="<?php if (isset($page) && $page == 'frontpages'): ?>active<?php endif; ?>">
                                <a href="<?php echo base_url('panel/frontpages')?>"><i class="fa fa-home fa-fw"></i> Front Pages</a>
                            </li>
                        <?php elseif (isset($user) && get_class($user)== 'Provider'): ?>
                            <li class="<?php if (isset($page) && $page == 'index' || $page == 'package'): ?>active<?php endif; ?>">
                                <a href="<?php echo base_url('panel/')?>"><i class="fa fa-book fa-fw"></i> Booking</a>
                            </li>
                            <li class="<?php if (isset($page) && $page == 'payment_pending'): ?>active<?php endif; ?>">
                                <a href="<?php echo base_url('panel/payment_pending')?>"><i class="fa fa-money fa-fw"></i> Pending Payments</a>
                            </li>
                            <li>
                                <a href="" data-toggle="modal" data-target="#myModalContact"><i class="fa fa-envelope fa-fw"></i> Contact</a>
                            </li>
                        <?php elseif (isset($user) && get_class($user)== 'Agent'): ?>
                            <li class="<?php if (isset($page) && $page == 'create_booking' ||  $page == 'step_booking' ||  $page == 'step_details' ||  $page == 'index' ||  $page == 'package'): ?>active<?php endif; ?>">
                                <a href="#"><i class="fa fa-book fa-fw"></i> Booking<span class="fa arrow"></span></a>
                                <?php if($page !== 'create_booking' && $page !== 'index') $showNo = 'showNo'; else $showNo=''; ?>
                                <ul class="nav nav-second-level <?php echo $showNo;?>">
                                    <li>
                                        <a onclick="clearCookie()" href="<?php echo base_url('panel/clearSession/create_booking')?>" class=<?php if($page == 'create_booking' ||  $page == 'step_booking' ||  $page == 'step_details'): ?> "page_display" <?php endif; ?>>Create Bookings</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('panel/')?>" class=<?php if($page == 'index'): ?> "page_display" <?php endif; ?>>List of Bookings</a>
                                    </li>
                                    <li>
                                        <a href="" data-toggle="modal" data-target="#myModalPrice">Print Contract Rates</a>
                                        <!-- <a href="<?php echo base_url('panel/package_price_list_');?>" class="price_list_">Print Contract Rates</a> -->
                                    </li>
                                </ul>
                                <!-- /.nav-second-level -->
                            </li>
                           <!--  <li class="<?php //if (isset($page) && $page == 'payment_pending'): ?>active<?php //endif; ?>">
                                <a href="<?php //echo base_url('panel/payment_pending')?>"><i class="fa fa-money fa-fw"></i> Pending Payments</a>
                            </li> -->
                            <li class="<?php if (isset($page) && $page == 'rate'): ?>active<?php endif; ?>">
                                <a href="<?php echo base_url('panel/rate')?>"><i class="fa fa-dollar fa-fw"></i> Check Rates</a>
                            </li>
                            <li>
                                <a href="" data-toggle="modal" data-target="#myModalAirportMap"><i class="fa fa-map-marker fa-fw"></i> Airport Map</a>
                            </li>
                            <li>
                                <a href="" data-toggle="modal" data-target="#myModalContact"><i class="fa fa-envelope fa-fw"></i> Contact</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('panel/apps')?>"><i class="fa fa-android"></i> Apps</a>
                            </li>
                        <?php elseif (isset($user) && get_class($user)== 'Customer'): ?>
                            <li class="<?php if (isset($page) && $page == 'index' || $page == 'package'): ?>active<?php endif; ?>">
                                <a href="<?php echo base_url('panel/')?>"><i class="fa fa-book fa-fw"></i> Booking</a>
                            </li>
                            <li>
                                <a href="" data-toggle="modal" data-target="#myModalContact"><i class="fa fa-envelope fa-fw"></i> Contact</a>
                            </li>
                        <?php endif;?>
                        </ul>
                    </div>
                    <!-- /.sidebar-collapse -->
                </div>
                <!-- /.navbar-static-side -->
            </nav>
            <!-- /Navigation -->

            <!-- myModalProfile -->
            <div class="modal fade" id="myModalProfile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class=" pull-right btn btn-danger btn-sm" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="myModalLabel">Profile:</h4>
                        </div>
                        <form id="profile_form" class="form-horizontal" role="form" method="post" action="<?php echo base_url('panel/update_user'); ?>">
                            <div class="modal-body">
                                <div class="container" style="width:100%;">
                                    <div class="alert alert-danger" style="display: none;"></div>
                                    <input type="hidden" class="form-control" name="user_id" disabled="disabled" value="<?php echo $user->getUser()->getId(); ?>">
                                    <input type="hidden" class="form-control" name="user_type" disabled="disabled" value="<?php echo get_class($user)?>">
                                    <input type="hidden" class="form-control" name="prof_form" value="1">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="col-xs-4 control-label">Salutation</label>
                                                <div class="col-xs-8">
                                                    <select class="form-control" name="salutation" disabled="disabled">
                                                        <?php if (isset($salutations)): ?>
                                                            <?php foreach ($salutations as $key => $salutation): ?>
                                                                <option value="<?php echo $key; ?>" <?php if($user->getUser()->getSalutation() && $user->getUser()->getSalutation() == $key): ?>selected="selected"<?php endif; ?>><?php echo $salutation; ?></option>
                                                            <?php endforeach; ?>
                                                        <?php endif; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-xs-4 control-label">First Name</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" name="firstname" disabled="disabled" value="<?php echo $user->getUser()->getFirstname(); ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-xs-4 control-label">Middle Name</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" name="middlename" disabled="disabled" value="<?php echo $user->getUser()->getMiddlename(); ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-xs-4 control-label">Last Name</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" name="lastname" disabled="disabled" value="<?php echo $user->getUser()->getLastname(); ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-xs-4 control-label">Username</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" name="username" disabled="disabled" value="<?php echo $user->getUser()->getUsername(); ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-xs-4 control-label">Date of Birth</label>
                                                <div class="col-xs-8">
                                                    <input id="dob_profile" class="pickadate_DOB form-control" type="text" name="dob" disabled="disabled" value="<?php echo $user->getUser()->getDateOfBirth(); ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-xs-4 control-label">Company</label>
                                                <div class="col-xs-8">
                                                    <input id="typeahead_profile" type="text" class="form-control" name="company" disabled="disabled" value="<?php echo $user->getUser()->getCompany(); ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="col-xs-4 control-label">Phone</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" name="phone_number"  disabled="disabled" value="<?php echo $user->getUser()->getPhone(); ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-xs-4 control-label">Email</label>
                                                <div class="col-xs-8">
                                                    <input type="email" class="form-control" name="email" disabled="disabled" value="<?php echo $user->getUser()->getEmail(); ?>">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-xs-4 control-label">Alternative Email</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" name="alternative_email" disabled="disabled" value="<?php echo $user->getUser()->getAlteremail(); ?>" onblur="validateMultipleEmailsCommaSeparated(this,',')" placeholder="Comma Separated">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-xs-4 control-label">Country</label>
                                                <div class="col-xs-8">
                                                    <select name="country" class="form-control" disabled="disabled">
                                                        <?php if (isset($countries_usr)): ?>
                                                            <?php foreach ($countries_usr as $code=>$country): ?>
                                                                <option value="<?php echo $code; ?>"<?php if($user->getUser()->getCountry() && $user->getUser()->getCountry() == $code): ?> selected="selected"<?php endif; ?>><?php echo $country; ?></option>
                                                            <?php endforeach; ?>
                                                        <?php endif; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-xs-4 control-label">Old Password</label>
                                                <div class="col-xs-8">
                                                    <input type="password" class="form-control" placeholder="******" name="old_password" disabled="disabled">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-xs-4 control-label">New Password</label>
                                                <div class="col-xs-8">
                                                    <input type="password" class="form-control" placeholder="******" name="new_password" disabled="disabled">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-xs-4 control-label">Confirm New Password</label>
                                                <div class="col-xs-8">
                                                    <input type="password" class="form-control" placeholder="******" name="confirm_new_password" disabled="disabled">
                                                </div>
                                            </div>
                                        </div>
                                        <hr/>
                                        <div class="col-sm-12" style="border-top:1px solid #EBEBEB; padding-top:15px;">
                                        <?php
                                            $em_names  = json_decode($user->getUser()->getEmergencyname());
                                            $em_phones = json_decode($user->getUser()->getEmergencyphone());
                                            for($i = 0; $i < count($em_names) ; $i++){
                                        ?>
                                        <div class="dynamic_emergency">
                                            <div class="col-xs-6">
                                                <div class="form-group">
                                                    <label class="col-xs-4 control-label">Emergency Name</label>
                                                    <div class="col-xs-8">
                                                        <input type="text" class="form-control" name="emergency_name[]"  disabled="disabled" value="<?php echo $em_names[$i]; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="form-group">
                                                    <label class="col-xs-4 control-label">Emergency Phone</label>
                                                    <div class="col-xs-8 ge-sctop em-modal">
                                                        <i class="fa fa-minus em-remove"></i>
                                                        <input type="text" class="form-control" name="emergency_phone_number[]"  disabled="disabled" value="<?php echo $em_phones[$i]; ?>">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                         <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button id="edit_profile_button" class="btn btn-orange">Edit</button>
                                <button id="update_profile_button" type="submit" class="btn btn-orange" style="display:none;">Save</button>
                                <button id="cancel_update_button" class="btn btn-default" style="display:none;">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- myModalEditUser -->
            <div class="modal fade" id="myModalEditUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class=" pull-right btn btn-danger btn-sm" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="myModalLabel">Profile:</h4>
                        </div>
                        <form id="edit_profile_form" class="form-horizontal" role="form" method="post" action="<?php echo base_url('panel/update_user'); ?>">
                            <div class="modal-body">
                                <div class="container" style="width:100%;">
                                    <div class="alert alert-danger" style="display: none;"></div>
                                    <input type="hidden" class="form-control" name="user_id" value="">
                                    <input type="hidden" class="form-control" name="prof_form" value="2">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="col-xs-4 control-label">Salutation</label>
                                                <div class="col-xs-8">
                                                    <select class="form-control" name="salutation">
                                                        <?php if (isset($salutations)): ?>
                                                        <?php foreach ($salutations as $key => $salutation): ?>
                                                        <option value="<?php echo $key; ?>"><?php echo $salutation; ?></option>
                                                        <?php endforeach; ?>
                                                        <?php endif; ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <?php if(strtolower(get_class($user)) == 'admin'): ?>
                                                <div class="form-group">
                                                    <label class="col-xs-4 control-label">Type</label>
                                                    <div class="col-xs-8">
                                                        <select class="form-control" name="user_type">
                                                            <option value='customer'>Customer</option>
                                                            <option value='agent'>Agent</option>
                                                            <option value='provider'>Provider</option>
                                                            <option value='account'>Account</option>
                                                            <option value='admin'>Admin</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                            <div class="form-group">
                                                <label class="col-xs-4 control-label">First Name</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" name="firstname">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-xs-4 control-label">Middle Name</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" name="middlename">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-xs-4 control-label">Last Name</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" name="lastname">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-xs-4 control-label">Username</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" name="username">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="col-xs-4 control-label">Date of Birth</label>
                                                <div class="col-xs-8">
                                                    <input id="dob_profile_edit" class="pickadate_DOB form-control" type="text" name="dob">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-xs-4 control-label">Company</label>
                                                <div class="col-xs-8">
                                                    <input id="typeahead_manage" type="text" class="form-control" name="company">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-xs-4 control-label">Phone</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" name="phone_number">
                                                </div>
                                            </div>
                                            <!-- <div class="form-group">
                                                <label class="col-xs-4 control-label">Emergency Phone</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" name="emergency_phone_number">
                                                </div>
                                            </div> -->
                                            <div class="form-group">
                                                <label class="col-xs-4 control-label">Email</label>
                                                <div class="col-xs-8">
                                                    <input type="email" class="form-control" name="email">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-xs-4 control-label">Alternative Email</label>
                                                <div class="col-xs-8">
                                                    <input type="text" class="form-control" name="alternative_email" onblur="validateMultipleEmailsCommaSeparated(this,',')" placeholder="Comma Separated">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-xs-4 control-label">Country</label>
                                                <div class="col-xs-8">
                                                    <select name="country" class="form-control">
                                                        <?php if (isset($countries_usr)): ?>
                                                            <?php foreach ($countries_usr as $code=>$country): ?>
                                                                <option value="<?php echo $code; ?>"><?php echo $country; ?></option>
                                                            <?php endforeach; ?>
                                                        <?php endif; ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <hr/>
                                        <div class="col-sm-12 em-area" style="border-top:1px solid #EBEBEB; padding-top:15px;">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <td class="text-center">
                                                <input class="col-xs-2" class="grant_all_permissions" name="grant_all_permissions" type="checkbox"/><strong>Grant permission for all existing packages</strong>
                                            </td>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button id="update_profile_button" type="submit" class="btn btn-orange">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- myModalSetting -->
            <div class="modal fade" id="myModalSetting" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="pull-right btn btn-danger btn-sm" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="myModalLabel">Setting:</h4>
                        </div>
                        <form class="form-horizontal" role="form">
                            <div class="modal-body">
                                <div class="form-group">
                                    <label class="col-xs-4 control-label">Promotional Message</label>
                                    <div class="col-xs-8">
                                        <textarea class="form-control" style="resize:none;" rows="3"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-4 control-label">Provider Message</label>
                                    <div class="col-xs-8">
                                        <textarea class="form-control" style="resize:none;" rows="3"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-4 control-label">Agent Message</label>
                                    <div class="col-xs-8">
                                        <textarea class="form-control" style="resize:none;" rows="3"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-4 control-label">Contact Form Emails</label>
                                    <div class="col-xs-8">
                                        <input type="text" class="form-control" placeholder="Email">
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <a class="btn btn-orange" href="#" data-dismiss="modal" role="button">Update</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- myModalSuccess -->
            <div class="modal fade" id="myModalSuccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-body">
                            <button type="button" class="pull-right btn btn-danger btn-xs" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <p class="modal-title" id="myModalLabel">&nbsp;</p>
                        </div>
                    </div>
                </div>
            </div>

            <!-- myModalContact -->
            <div class="modal fade" id="myModalContact" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="pull-right btn btn-danger btn-sm" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="myModalLabel">Contact:</h4>
                        </div>
                        <form id="contact_form" class="form-horizontal" role="form" method="post" action="<?php echo base_url('panel/send_mail'); ?>">
                            <div class="modal-body">
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">Subject</label>
                                    <div class="col-xs-9">
                                        <input type="hidden" name="email[]" value="<?php echo $user->getUser()->getEmail();?>"></input>
                                        <input type="hidden" name="email[]" value="<?php echo $user->getUser()->getFirstname();?>"></input>
                                        <input name="email[]" type="text" class="form-control" name="subject" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">Message</label>
                                    <div class="col-xs-9">
                                        <textarea  name="email[]" class="form-control" style="resize:none;" rows="3" name="message" required></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input class="btn btn-orange"  type="submit" />
                                <!-- <a class="btn btn-orange" href="#" data-dismiss="modal" role="button" type="submit">Submit</a> -->
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- myModalPrice -->
            <div class="modal fade" id="myModalPrice" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="pull-right btn btn-danger btn-sm" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="myModalLabel">Download Price List:</h4>
                        </div>
                        <div class="modal-body">
                            <ul>
                                <?php
                                    $tmp=array();
                                    if(count(@$csv)!=0){
                                    foreach (@$csv as $key){ ?>
                                        <a href="<?php echo base_url('uploads/package_prices/'.@$key->PACKLINK); ?>" target="_blank"><li><?php echo @$key->PACKTITLE; ?></li></a>
                                <?php }} ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <!-- myModalAirportMap -->
            <div class="modal fade" id="myModalAirportMap" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="pull-right btn btn-danger btn-sm" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="myModalLabel">Attachment(s):</h4>
                        </div>
                        <div class="modal-body">
                            <ul>
                                <?php
                                    if(@$airport_map != ""):
                                    foreach($airport_map as $key){ ?>
                                        <a href="<?php echo base_url('uploads/destination_pdf/'.$key->pdf_name); ?>" target="_blank"><li><?php echo $key->title; ?></li></a>
                                <?php }
                                   endif;
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <!-- myModalPricelist -->
            <div class="modal fade" id="myModalPricelist" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="pull-right btn btn-danger btn-sm" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="myModalLabel">Print price list </h4>
                        </div>
                        <form id="contact_form" class="form-horizontal" role="form" method="post" action="<?php echo base_url('panel/price_pdf'); ?>">
                            <div class="modal-body">
                                <div class="form-group">
                                    <label class="col-xs-3 control-label">Package</label>
                                    <div class="col-xs-9">
                                        <input name="package_id" type="text" class="form-control" name="subject" required>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input class="btn btn-orange"  type="submit" value="Print pdf"/>
                                <!-- <a class="btn btn-orange" href="#" data-dismiss="modal" role="button" type="submit">Submit</a> -->
                            </div>
                        </form>
                    </div>
                </div>
            </div>