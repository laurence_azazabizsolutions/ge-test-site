            <!-- footer -->
            <div class="container-fluid footer footer-one">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="col-md-6">
                                <h6>Global Explorer</h6>
                                <p class="lead">
                                Offering the most superior finishes and amenities in all  Asian Country.
                                <br><br>
                                We can fulfill any of your needs.
                            </p>
                            <br>
                            </div>
                            <div class="col-md-3">
                                <img src="<?php echo base_url('img/google_play.jpg'); ?>" style="height:60%;width:50%">
                                <p>Download on <a href="https://play.google.com/store/apps/details?id=com.azaza.ge&hl=en">Google Play</a></p>
                            </div>
                            <div class="col-md-3">
                                <img src="<?php echo base_url('img/itunes.jpg'); ?>" style="height:60%;width:50%">
                                <p>Download on <a href="https://itunes.apple.com/ph/app/ge-agent-assistant/id998828920?mt=8">iTunes</a></p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <h6>Book Your Travel Now!</h6>
                            <p class="lead">
                                <span class="glyphicon glyphicon-home">&nbsp;</span> 53 Ubi Ave 1, Ubi Industrial Park #01-29, Singapore 408934<br>
                                <span class="glyphicon glyphicon-earphone">&nbsp;</span> +65 8451 6666<br>
                                <span class="glyphicon glyphicon-envelope">&nbsp;</span> <a href="mailto:edmundwan@hotmail.com">edmundwan@yahoo.com</a><br>
                                <span class="glyphicon glyphicon-envelope">&nbsp;</span> <a href="mailto:op.globalexplorer@gmail.com">op.globalexplorer@gmail.com</a>
                            </p>
                            <br>
                        </div>
                    </div>
                </div>
            </div> 

            <div class="container-fluid footer footer-two">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-6">
                            <p>Copyright &copy; 2014 Global Explorer. All Rights Reserved.</p>
                        </div>
                        <div class="col-xs-6 text-right">
                            <p>Powered by Azaza.</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /footer -->

        </div>
        <!-- /Content -->

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="<?php echo base_url('js/bootstrap.min.js'); ?>"></script>
        <script src="<?php echo base_url('js/public/general.js'); ?>"></script>
        <script src="<?php echo base_url('js/public/booking.js'); ?>"></script>
        <script type="text/javascript">
            //redirect to selected Destination
            $(document).ready( function() {
                $('#selectDestination').change( function() {
                    var browse = document.getElementById("selectDestination").value;

                    if (browse != '') {
                        location.href = $(this).val();
                    };
                });
            });
        </script>
        <script src="<?php echo base_url('js/moment.min.js'); ?>"></script>
        <script src="<?php echo base_url('js/bootstrap-datetimepicker.min.js'); ?>"></script>
        <script src="<?php echo base_url('js/typeahead.js-master/dist/typeahead.bundle.js'); ?>"></script>
        <script src="<?php echo base_url('js/bootstrapValidator.js'); ?>"></script>
        <script src="<?php echo base_url('js/picker.js'); ?>"></script>
        <script src="<?php echo base_url('js/picker.date.js'); ?>"></script>
        <script src="<?php echo base_url('js/picker.date.setting.js'); ?>"></script>
        <script src="<?php echo base_url('js/text_function.js'); ?>"></script>
        <script src="<?php echo base_url('js/panel/booking_lester.js'); ?>"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                //check if the last hotel choice was changed ----------------------------------------------
                $("[name='package_hotel_choice[]']:last").change(function(){
                    var package_hotel_id = $(this).val();
                    var country_code = $(this).attr("destination-code");
                    
                    //link for the optopnal hotel name ----------------------------------------------
                    url_link1 = "<?php  echo base_url('panel/getOptionalHotel'); ?>";

                    //dropdown for the extension hotel name and id ----------------------------------------------
                    $.ajax({
                        url: url_link1,
                        data: 'package_hotel_id='+package_hotel_id+"&country_code="+country_code,
                        type: 'POST',
                        dataType: 'html',
                        success: function(result){
                            $('[name=exhotel_name] option').remove();
                            $('[name=exhotel_name]').append(result);
                        },
                        error: function(x){
                            //alert(x.responseText);
                        }
                    });
                });

                //start of select hotel ----------------------------------------------
                $("[name='package_hotel_choice[]']").change(function() {
                    //hotel id and hotel-package relation id ----------------------------------------------
                    var package_hotel_id = $(this).val();

                    //package id ----------------------------------------------
                    var package_id= $(this).attr('package-id');
                    
                    //url link ----------------------------------------------
                    url_link = "<?php echo base_url('panel/gethotelroom_packagehotelid'); ?>";
                    
                    //get hotel meta data  ----------------------------------------------
                    pkg_hotel_id = package_hotel_id.split(".");

                    //hotel id ----------------------------------------------
                    var pkg_htl_id = pkg_hotel_id[0];

                    //destination id ----------------------------------------------
                    var destination_id = $(this).attr("destination-id");

                    //hotel name ----------------------------------------------
                    var pkg_hotel_name = $('#package_hotel_choice_name'+destination_id+'-'+pkg_htl_id).attr("hotel-name");
                    
                    //destination index ----------------------------------------------
                    var destination_index = $(this).attr("destination-index");

                    //total destinations ----------------------------------------------
                    var total_destinations = parseInt($("#total_destinations").val())-1;

                    //hide suplementary errors ----------------------------------------------
                    $('#supplementary_errors'+destination_id).hide();

                    //depart and return values ----------------------------------------------
                    var depart_value = $("#datetimepickerBookingDepart").val();
                    var return_value = $("#datetimepickerBookingReturn").val();

                    //clear the error ----------------------------------------------
                    $("#blackout_error_container"+destination_id).hide();

                    //set the package hotel name
                    $("#package_hotel_name"+destination_id).val(pkg_hotel_name);
                    
                    //load the spinners ----------------------------------------------
                    $("#roomrates_container"+destination_id).empty();
                    $("#destination_roomrates_loader"+destination_id).show();


                    //ajax for fetching the something.. ----------------------------------------------
                    $.ajax({
                        url: url_link,
                        data: 'package_hotel_id='+package_hotel_id+"&package_id="+package_id+"&depart_value="+depart_value+"&return_value="+return_value+"&destination_index="+destination_index+"&total_destinations="+total_destinations,
                        type: 'POST',
                        dataType: 'html',
                        success: function(result){
                            //load the spinners ----------------------------------------------
                            $("#destination_roomrates_loader"+destination_id).hide();

                            resultObj = result.split('_:_'); 
                            alert_type = resultObj[0]; 
                            html_data = resultObj[1]; 
                            
                            //empty and append the roomrates container ----------------------------------------------
                            $("#roomrates_container"+destination_id).empty().append(html_data);
               
                            //get the extension ----------------------------------------------
                            var extension = parseInt($('#extended_days').val());

                            if(extension!=0){
                                $('.extension_container').hide();
                                $('.extension_container:last').show();
                            }
                            
                            //list all the hotel surcharges ----------------------------------------------
                            listHotelSurcharges();
                         
                        }, error : function(x){
                            alert(x.responseText);
                        }
                    });  
                });
            });
        </script>
    </body>
</html>