<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="<?php echo base_url('img/icon.png'); ?>">

        <title>Global Explorer</title>

        <!-- Bootstrap core CSS -->
        <link href="<?php echo base_url('css/bootstrap.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('css/bootstrap-datetimepicker.min.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('css/bootstrapValidator.css'); ?>" rel="stylesheet">

        <!-- date picker within modal -->
        <link href="<?php echo base_url('css/default.css'); ?>" rel="stylesheet" id="theme_base">
        <link href="<?php echo base_url('css/default.date.css'); ?>" rel="stylesheet" id="theme_base">


        <!-- Custom Fonts -->
        <link href="<?php echo base_url('css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css">
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Custom styles for this template -->
        <link href="<?php echo base_url('css/carousel.css'); ?>" rel="stylesheet">
        <link href="<?php echo base_url('css/typeahead_custom.css'); ?>" rel="stylesheet">
        <script type="text/javascript"  src="<?php echo base_url('js/jquery-1.11.0.js'); ?>"></script>
    </head>

    <body>

        <!-- NAVBAR
        ================================================== -->
        <div class="navbar-wrapper">
            <div class="container">
                <div class="navbar navbar-inverse navbar-static-top" role="navigation">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="<?php echo base_url(); ?>">Global Explorer</a>
                        </div>
                        <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav navbar-right">
                                <li class="<?php if (isset($page) && $page == 'index'): ?>active<?php endif; ?>"><a href="<?php echo base_url(); ?>">HOME</a></li>
                                <li class="<?php if (isset($page) && $page == 'about'): ?>active<?php endif; ?>"><a href="<?php echo base_url('about'); ?>">ABOUT</a></li>
                                <li class="dropdown <?php if (isset($page) && $page == 'browse' || $page == 'details1' || $page == 'details2'): ?>active<?php endif; ?>">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">BROWSE <span class="caret"></span></a>
                                    <ul class="dropdown-menu dropdown-menu-nav" role="menu">
                                        <?php if (isset($countries)): ?>
                                        <?php foreach ($countries as $country): ?>
                                        <li><a href="<?php echo base_url('browse/'.$country['code']); ?>"><?php echo $country['country']; ?></a></li>
                                        <?php endforeach; ?>
                                        <?php endif; ?>
                                    </ul>
                                </li>
                                <li class="<?php if (isset($page) && $page == 'contact'): ?>active<?php endif; ?>"><a href="<?php echo base_url('contact'); ?>">CONTACT</a></li>
                                <?php if (isset($login)): ?>
                                    <?php if (FALSE === $login): ?>
                                        <li><a href="" data-toggle="modal" data-target="#myModalLogin">LOGIN</a></li>
                                    <?php elseif (isset($usertype)): ?>
                                        <?php if ($usertype instanceof Customer): ?>
                                           <li><a href="<?php echo base_url('access/logout'); ?>">LOGOUT</a></li> -->
                                        <?php else:?>
                                            <li class="dropdown">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">SETTING <span class="caret"></span></a>
                                                <ul class="dropdown-menu dropdown-menu-nav" role="menu">
                                                    <li><a href="<?php echo base_url('panel'); ?>">View Dashboard</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="<?php echo base_url('access/logout'); ?>">Logout</a></li>
                                                </ul>
                                            </li>
                                        <?php endif; ?>
                                    <?php else: ?>
                                        <li><a href="<?php echo base_url('access/logout'); ?>">LOGOUT</a></li>
                                    <?php endif; ?>
                                <?php else: ?>
                                    <li><a href="" data-toggle="modal" data-target="#myModalLogin">LOGIN</a></li>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- myModalLogin -->
        <div class="modal fade" id="myModalLogin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="pull-right btn btn-danger btn-sm" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Please Login:</h4>
                    </div>
                    <form id="login_form" class="form-horizontal" role="form" action="<?php echo base_url('access/login'); ?>" method="post">
                        <div class="modal-body">
                            <?php if($error_message && isset($error_message)): ?>
                                <div class="alert alert-danger" ><?php echo $error_message?></div>
                            <?php endif; ?>
                            <div class="form-group">
                                <label class="col-xs-4 control-label">Email</label>
                                <div class="col-xs-8">
                                    <input type="text" class="form-control" placeholder="Email" name="email_login">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-4 control-label">Password</label>
                                <div class="col-xs-8">
                                    <input type="password" class="form-control" placeholder="Password" name="password_login">
                                </div>
                            </div>
                            <div class="form-group">
                                <a class="col-xs-offset-4 col-xs-8" href="#" data-dismiss="modal" data-toggle="modal" data-target="#myModalForget">Forgot password?</a>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <strong>New User? <a href="#" data-toggle="modal" data-target="#myModalSignUp">Register Now!</a></strong> &nbsp;
                            <button class="btn btn-orange" type="submit">Login</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- myModalForget -->
        <div class="modal fade" id="myModalForget" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="pull-right btn btn-danger btn-sm" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Enter Your Email:</h4>
                    </div>
                    <form id="defaultFormForget" method="post" class="form-horizontal" role="form" action="<?php echo base_url('home/forgot_password'); ?>">
                        <div class="modal-body">
                            <div class="alert" style="display: none;"></div>
                            <div class="form-group">
                                <label class="col-xs-4 control-label">Email</label>
                                <div class="col-xs-8">
                                    <input type="email" class="form-control" placeholder="Email Address" name="email">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <!-- <strong>No Account yet? <a href="#" data-dismiss="modal" data-toggle="modal" data-target="#myModalSignUp" role="button">Sign Up Now!</a></strong> &nbsp; -->
                            <button type="submit" class="btn btn-orange">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- myModalSignUp -->
        <div class="modal fade" id="myModalSignUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class=" pull-right btn btn-danger btn-sm" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel">Create Account:</h4>
                        <div style="color:#333333;font-size:11px">Fields marked with * are required</div>
                    </div>
                    <form class="form-horizontal" role="form" method="post" action="<?php echo base_url('register/register_user'); ?>">
                        <div class="modal-body">
                            <div class="container" style="width:100%;">
                                <div class="alert alert-danger" style="display: none;"></div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="col-xs-5 control-label">Salutation
                                                <div class="requiredfld">*</div>
                                            </label>
                                            <div class="col-xs-7">
                                                <select class="form-control" name="salutation">
                                                    <option value=" ">- Select -</option>
                                                    <option value="mr.">Mr.</option>
                                                    <option value="ms.">Ms.</option>
                                                    <option value="mrs.">Mrs.</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-xs-5 control-label">First Name
                                                <div class="requiredfld">*</div>
                                            </label>
                                            <div class="col-xs-7">
                                                <input type="text" class="form-control" placeholder="John" name="firstname">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-xs-5 control-label">Middle Name</label>
                                            <div class="col-xs-7">
                                                <input type="text" class="form-control" placeholder=" " name="middlename">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-xs-5 control-label">Last Name
                                                <div class="requiredfld">*</div>
                                            </label>
                                            <div class="col-xs-7">
                                                <input type="text" class="form-control" placeholder="Doe" name="lastname">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-xs-5 control-label">Company</label>
                                            <div class="col-xs-7">
                                                <input id="typeahead" type="text" class="form-control" placeholder="Company XYZ" name="company">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="col-xs-5 control-label">Phone
                                                <div class="requiredfld">*</div>
                                            </label>
                                            <div class="col-xs-7">
                                                <input type="text" class="form-control" placeholder="+65123456" name="phone">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-xs-5 control-label">Email
                                                <div class="requiredfld">*</div>
                                            </label>
                                            <div class="col-xs-7">
                                                <input type="email" class="form-control" placeholder="johndoe@domail.com" name="email">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-xs-5 control-label">Country
                                                <div class="requiredfld">*</div>
                                            </label>
                                            <div class="col-xs-7">
                                                <select name="country" class="form-control">
                                                    <option value=" ">- Select -</option>
                                                    <?php if (isset($countries_usr)): ?>
                                                        <?php foreach ($countries_usr as $code=>$country): ?>
                                                            <option value="<?php echo $code; ?>"><?php echo $country; ?></option>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-xs-5 control-label">Password
                                                <div class="requiredfld">*</div>
                                            </label>
                                            <div class="col-xs-7">
                                                <input type="password" class="form-control" placeholder="******" name="password">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-xs-5 control-label">Repeat Password
                                                <div class="requiredfld">*</div>
                                            </label>
                                            <div class="col-xs-7">
                                                <input type="password" class="form-control" placeholder="******" name="confirm_password">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <strong>Already registered? <a href="#" data-dismiss="modal" data-toggle="modal" data-target="#myModalLogin" role="button">Login Now!</a></strong> &nbsp;
                            <button class="btn btn-orange" type="submit">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- myModalSuccess -->
        <div class="modal fade" id="myModalSuccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="pull-right btn btn-danger btn-xs" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <p class="modal-title" id="myModalLabel"><?php if(isset($confirmation_message)) echo $confirmation_message; ?></p>
                    </div>
                </div>
            </div>
        </div>
