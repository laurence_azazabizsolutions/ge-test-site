<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>Global Explorer</title>
 		<style type="text/css">
			h1,
			h4{
				font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
				font-weight: 500;
				line-height: 1.1;
				color: inherit;
			}
		</style>
	</head>
	<body style="color: #333333;">
		<table cellspacing="0" cellpadding="0" style="background-color: #ffffff; border: 1px solid transparent;  border-radius: 4px; -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05); box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05); border-color: #e95a3e;">
			<tr>
				<td style="padding: 10px 15px; border-bottom: 1px solid transparent; border-top-right-radius: 3px; border-top-left-radius: 3px; color: #fff; background-color: #e95a3e; border-color: #e95a3e;">
					<h1 style="margin-top: 0; margin-bottom: 0; font-size: 16px; color: inherit; text-align: center; ">
						Global Explorer
					</h1> 
				</td>
			</tr>
			<tr>
				<td style="padding: 15px;">
					<table style="width: 770px; margin-bottom: 20px; border-collapse: collapse;">
						<tr>
							<td><h4 style="font-size: 18px; margin-top: 10px; margin-bottom: 10px;"><?php echo $recipient; ?>:</h4></td>
						</tr>
						<tr>
							<td>
								<p style="margin: 0;">
									<?php if($ge_booking_status === 'amended'){ echo 'The booking is amended'; } ?><?php if($ge_booking_status === 'pending'){ echo 'A new booking has been created'; } ?> for <strong><?php echo $package[0]->getTitle();?><?php if($step_booking["extended_days"] > 0){ echo ' + '.$step_booking["extended_days"].' Night/s extension';} ?></strong>
									With <?php echo $step_booking['adult_count'];?> adult/s, <?php echo $step_booking['child_count'];?> child/ren, <?php echo $step_booking['infant_cnt'];?> infant/s on <?php echo date_format(date_create($step_booking['depart_date']),'d-m-Y');?> until <?php echo date_format(date_create($step_booking['return_date']),'d-m-Y');?>.
								</p>
							</td>
							
						</tr>
					</table>

                    <?php 
                        echo @$booking_summary_provider[0];
                        //echo @$booking_summary_provider[1];
                    ?>

					<center>
						<a href="<?php echo $confirm.'/'.$recipient_id; ?>" style="color: #C64E0A; text-decoration:none;">
							<button style="padding: 6px 12px; margin-bottom: 0; font-size: 14px; font-weight: normal; line-height: 1.428571429; white-space: nowrap; cursor: pointer; background-image: none; border: 1px solid transparent; border-radius: 4px; -webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; -o-user-select: none; user-select: none;color: #fff; background-color: #e95a3e; border-color: #e02908;">Confirm</button>
						</a>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<!-- <a href="<?php echo $hotel.'/'.$recipient_id; ?>" style="color: #C64E0A; text-decoration:none;">
							<button style="padding: 6px 12px; margin-bottom: 0; font-size: 14px; font-weight: normal; line-height: 1.428571429; white-space: nowrap; cursor: pointer; background-image: none; border: 1px solid transparent; border-radius: 4px; -webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; -o-user-select: none; user-select: none;color: #fff; background-color: #e95a3e; border-color: #e02908;">Change Hotel</button>
						 </a>
						 <a href="<?php echo $room.'/'.$recipient_id; ?>" style="color: #C64E0A; text-decoration:none;">
							<button style="padding: 6px 12px; margin-bottom: 0; font-size: 14px; font-weight: normal; line-height: 1.428571429; white-space: nowrap; cursor: pointer; background-image: none; border: 1px solid transparent; border-radius: 4px; -webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; -o-user-select: none; user-select: none;color: #fff; background-color: #e95a3e; border-color: #e02908;">Change Room</button>
						 </a>
						 <a href="<?php echo $reject.'/'.$recipient_id; ?>" style="color: #C64E0A; text-decoration:none;">
							<button style="padding: 6px 12px; margin-bottom: 0; font-size: 14px; font-weight: normal; line-height: 1.428571429; white-space: nowrap; cursor: pointer; background-image: none; border: 1px solid transparent; border-radius: 4px; -webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; -o-user-select: none; user-select: none;color: #fff; background-color: #e95a3e; border-color: #e02908;">Reject</button>
						 </a> -->
						 <a href="<?php echo $cancel.'/'.$recipient_id; ?>" style="color: #C64E0A; text-decoration:none;">
							<button style="padding: 6px 12px; margin-bottom: 0; font-size: 14px; font-weight: normal; line-height: 1.428571429; white-space: nowrap; cursor: pointer; background-image: none; border: 1px solid transparent; border-radius: 4px; -webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; -o-user-select: none; user-select: none;color: #fff; background-color: #e95a3e; border-color: #e02908;">Cancel</button>
						 </a>
					</center>
				</td>
			</tr>
		</table>
	</body>
</html>