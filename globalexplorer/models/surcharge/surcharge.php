<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

get_instance()->load->iface('storeiteminterface');
get_instance()->load->iface('capturestateinterface');

/**
 * @author Kirstin Rubica
 *
 */
class Surcharge extends CI_Model implements StoreItemInterface, CaptureStateInterface {
	
	/**
	 * @var string
	 */
	const RULE_DATE_RANGE = 'date_range';
	
	/**
	 * @var string
	 */
	const RULE_DAY_OF_WEEK = 'day_of_week';
	
	/**
	 * @var string
	 */
	const RULE_AGENT = 'agent';

	/**
	 * @var string
	 */
	const RULE_BLACKOUT = 'blackout';

	/**
	 * @var string
	 */
	const RATE_PAX_NIGHT = 'pax/night';
	
	/**
	 * @var string
	 */
	const RATE_ADULT_NIGHT = 'adult/night';
	
	/**
	 * @var string
	 */
	const RATE_CHILD_NIGHT = 'child/night';
	
	/**
	 * @var string
	 */
	const RATE_INFANT_NIGHT = 'infant/night';
	
	/**
	 * @var string
	 */
	const RATE_ROOM_NIGHT = 'room/night';
	
	/**
	 * @var string
	 */
	const RATE_PAX_TRIP = 'pax/trip';
	
	/**
	 * @var string
	 */
	const RATE_ADULT_TRIP = 'adult/trip';
	
	/**
	 * @var string
	 */
	const RATE_CHILD_TRIP = 'child/trip';
	
	/**
	 * @var string
	 */
	const RATE_INFANT_TRIP = 'infant/trip';
	
	/**
	 * @var string
	 */
	const RATE_ROOM_TRIP = 'room/trip';

	/**
	 * @var integer
	 */
	private $id;
	
	/**
	 * @var string
	 */
	private $description;
	
	/**
	 * @var string
	 */
	private $ruletype;
	
	/**
	 * @var string
	 */
	private $rule;
	
	/**
	 * @var string
	 */
	private $cost;
	
	/**
	 * @var string
	 */
	private $profit;
	
	/**
	 * @var string
	 */
	private $ratetype;
	
	/**
	 * @var string
	 */
	private $status;
	
	/**
	 * @var Hotel|HotelRoomRate|Package
	 */
	private $surchargeObj;
	
	/**
	 * Constructor
	 */
	public function __construct() {
		parent::__construct();
	}
	
	/**
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}
	
	/**
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}
	
	/**
	 * @param string $description
	 */
	public function setDescription($description) {
		$this->description = $this->checkString($description);
	}
	
	/**
	 * @return multitype:NULL 
	 */
	public function getRuletypes() {
		return array(
			$this::RULE_AGENT,
			$this::RULE_BLACKOUT,
			$this::RULE_DATE_RANGE,
			$this::RULE_DAY_OF_WEEK,
		);
	}
	
	/**
	 * @return string
	 */
	public function getRuletype() {
		return $this->ruletype;
	}
	
	/**
	 * @param string $ruletype
	 */
	public function setRuletype($ruletype) {
		$this->ruletype = $this->checkString($ruletype);
	}
	
	/**
	 * @return string
	 */
	public function getRule() {
		return $this->rule;
	}
	
	/**
	 * @param string $rule
	 */
	public function setRule($rule) {
		$this->rule = $this->checkString($rule);
	}
	
	/**
	 * @return string
	 */
	public function getCost() {
		return $this->cost;
	}
	
	/**
	 * @param string $cost
	 */
	public function setCost($cost) {
		$this->cost = $this->checkPrice($cost);
	}
	
	/**
	 * @return string
	 */
	public function getProfit() {
		return $this->profit;
	}
	
	/**
	 * @param string $profit
	 */
	public function setProfit($profit) {
		$this->profit = $this->checkPrice($profit);
	}
	
	/**
	 * @return Ambigous <NULL, string>
	 */
	public function getPrice() {
		$price = NULL;
		if ($this->getCost() && $this->getProfit()) {
			$price = bcadd($this->getCost(), $this->getProfit(), 2);
		}
		return $price;
	}
	
	/**
	 * @return multitype:NULL 
	 */
	public function getRatetypes() {
		return array(
			$this::RATE_ADULT_NIGHT,
			$this::RATE_ADULT_TRIP,
			$this::RATE_CHILD_NIGHT,
			$this::RATE_CHILD_TRIP,
			$this::RATE_INFANT_NIGHT,
			$this::RATE_INFANT_TRIP,
			$this::RATE_PAX_NIGHT,
			$this::RATE_PAX_TRIP,
			$this::RATE_ROOM_NIGHT,
			$this::RATE_ROOM_TRIP,
		);
	}
	
	/**
	 * @return string
	 */
	public function getRatetype() {
		return $this->ratetype;
	}
	
	/**
	 * @param string $ratetype
	 */
	public function setRatetype($ratetype) {
		$this->ratetype = $this->checkString($ratetype);
	}

	/**
	 * @return string
	 */
	public function getStatus() {
		return $this->status;
	}
	
	/**
	 * @param string $status
	 */
	public function setStatus($status) {
		$this->status = $this->checkString($status);
	}

	/**
	 * @return Hotel|HotelRoomRate|Package
	 */
	public function getSurchargeOf(){
		return (!empty($this->surchargeObj)) ? $this->surchargeObj : false;
	}

	/**
	 * @param Hotel|HotelRoomRate|Package $obj
	 */
	public function setSurchargeOf($obj){
		if($obj instanceof Hotel || $obj instanceof Package || $obj instanceof HotelRoomRate){
			$this->surchargeObj = $obj;
		}
	}
	
	
	/**
	 * @param integer $input
	 * @return Ambigous <NULL, integer>
	 */
	private function isInteger($input) {
		return (is_int($input)) ? $input : null;
	}
	
	/**
	 * @param string $price
	 * @return NULL|mixed
	 */
	private function checkPrice($price) {
		if (!is_numeric($price) || $price < 0 || strlen(substr(strrchr($price, "."), 1)) > 2)
			return null;
		else
			return str_replace(',', '', number_format($price, 2));
	}
	
	/**
	 * @param string $input
	 * @return Ambigous <NULL, string>
	 */
	private function checkString($input) {
		return (is_string($input)) ? $input : null;
	}

	/* (non-PHPdoc)
	 * @see StoreItemInterface::prepared()
	 */
	public function prepared() {
		if ($this->description && is_null($this->id)) {
			return true;
		}
		return false;
	}
	
	/* (non-PHPdoc)
	 * @see StoreItemInterface::stored()
	 */
	public function stored() {
		if ($this->id) {
			return true;
		}
		return false;
	}

	/* (non-PHPdoc)
	 * @see CaptureStateInterface::captureCurrentState()
	 */
	public function captureCurrentState() {
		$this->prevState = clone $this;		
	}

	/* (non-PHPdoc)
	 * @see CaptureStateInterface::prevState()
	 */
	public function prevState() {
		return (!empty($this->prevState)) ? $this->prevState : false;
	}
}