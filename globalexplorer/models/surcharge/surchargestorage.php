<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

get_instance()->load->iface('storageinterface');
get_instance()->load->iface('idmanipulatorinterface');

class SurchargeStorage extends CI_Model implements StorageInterface, IdManipulatorInterface {
	
	private $dbName;
	private $retrievalSurchargeOf;
	
	const DB_SURCHARGE_HOTEL = 'hotel_surcharges';
	const DB_SURCHARGE_PACKAGE = 'package_surcharges';
	// const DB_SURCHARGE_HOTEL_ROOM = 'hotel_room_surcharges';
	const DB_SURCHARGE_HOTEL_ROOM = 'hotelroom_surcharges';
	
	public function __construct() {
		$this->load->database();
	}
	
	public function getDbName() {
		return $this->dbName;
	}
	
	public function setDbName($dbName) {
		if ($dbName == $this::DB_SURCHARGE_HOTEL || $dbName == $this::DB_SURCHARGE_HOTEL_ROOM || $dbName == $this::DB_SURCHARGE_PACKAGE) {
			$this->dbName = $dbName;
		}
	}
	
	public function getRetrievalSurchargeOf() {
		return $this->retrievalSurchargeOf;
	}
	
	public function setRetrievalSurchargeOf(SurchargeAwareInterface $obj) {
		if ($obj instanceof SurchargeAwareInterface) {
			$this->retrievalSurchargeOf = $obj;
		}
	}
	
	public function setId($obj, $id) {
		if ($obj instanceof Surcharge) {
			setprivateid($obj, 'Surcharge', $id);
		}
	}
	
	public function getSurchargeByObject(SurchargeAwareInterface $object) {
		if (!$object instanceof SurchargeAwareInterface && !$this->getDbName()) {
			return false;
		}
		
		$result = $this->db->get_where($this->getDbName(), array(
			$this->surchargeDbTableIdField($this->getDbName()) => $object->getId(),
		))->result_array();
		
		$surchargeCollection = array();
		if (count($result) > 0) {
			foreach ($result as $per_result) {
				$surcharge = $this->getById(intval($per_result['id']));
				$object->addSurcharge($surcharge);
				$surchargeCollection[] = $surcharge;
			}
		}
		
		return $surchargeCollection;
	}
	
	public function getById($id) {
		if ($this->getDbName()) {
			$this->load->model('surcharge/surcharge');
			$results = $this->db->get_where($this->getDbName(), array(
				'id' => $id
			))->result_array();
			if (count($results) == 1) {
				$surcharge = new Surcharge();
				$surcharge->setDescription($results[0]['description']);
				$surcharge->setRuletype($results[0]['rule_type']);
				$surcharge->setRule($results[0]['rule']);
				$surcharge->setCost($results[0]['cost']);
				$surcharge->setProfit($results[0]['profit']);
				$surcharge->setRatetype($results[0]['rate_type']);
				$this->setId($surcharge, $id);
				$surcharge->captureCurrentState();
				return $surcharge;
			}
		}
		return false;
	}
	

	public function getByInformation(array $info) {
		if (!$this->getDbName())
			return;
		
		$surchargeInfo = array(
			$this->surchargeDbTableIdField($this->getDbName()) => false,
			'description' => false,
			'rule_type' => false,
			'rule' => false,
			'cost' => false,
			'profit' => false,
			'rate_type' => false,
			'status' => false
		);
		foreach ($info as $key => $infoValue) {
			if (in_array($key, array_keys($surchargeInfo)))
				$surchargeInfo[$key] = $infoValue;
		}
		foreach ($surchargeInfo as $key => $infoValue) {
			if (false === $infoValue)
				unset($surchargeInfo[$key]);
		}
		$collection = array();
		$results = $this->db->get_where($this->getDbName(), $surchargeInfo)->result_array();
		if (count($results > 0)) {
			$this->load->model('surcharge/surcharge');
			foreach ($results as $result) {
				$surcharge = new Surcharge();
				$surcharge->setDescription($result['description']);
				$surcharge->setRuletype($result['rule_type']);
				//kirstin added this
				$rule = $result['rule'];
				if($surcharge->getRuletype() == 'agent'){
					$agent = $this->db->get_where('users',array('id'=>$result['rule']))->row();
					$agent_name = $agent->lastname.', '.$agent->firstname;
				$rule = $agent_name;
				}
				$surcharge->setRule($rule);
				$surcharge->setCost($result['cost']);
				$surcharge->setProfit($result['profit']);
				$surcharge->setRatetype($result['rate_type']);
				$surcharge->setStatus($result['status']);
				$this->setId($surcharge, $result['id']);
				$surcharge->captureCurrentState();
				$collection[] = clone $surcharge;
			}
		}
		return $collection;
	}
	
	public function store($surcharge) {
		
		// Check if Surcharge Object
		if (!$surcharge instanceof Surcharge) {
			return;
		}
		
		// Pass surchargeOf
		$surchargeOf = $surcharge->getSurchargeOf();
		if (!$surchargeOf) {
			return;
		}
		
		// Set appropriate DB
		switch (strtolower(get_class($surchargeOf))) {
			case 'hotel':
				$this->setDbName($this::DB_SURCHARGE_HOTEL);
				break;
			case 'package':
				$this->setDbName($this::DB_SURCHARGE_PACKAGE);
				break;
			case 'hotelroomrate':
				$this->setDbName($this::DB_SURCHARGE_HOTEL_ROOM);
				break;
			default:
				return;
		}
		
		// Store
		if ($surcharge->prepared()) {
			$timestamp = time();
			if ($this->surchargeDbTableIdField($this->getDbName())) {
				$this->db->insert($this->getDbName(), array(
					$this->surchargeDbTableIdField($this->getDbName()) => $surchargeOf->getId(),
					'description' => $surcharge->getDescription(),
					'rule_type' => $surcharge->getRuletype(),
					'rule' => $surcharge->getRule(),
					'cost' => $surcharge->getCost(),
					'profit' => $surcharge->getProfit(),
					'rate_type' => $surcharge->getRatetype(),
					'status' => 'active',
					'timestamp' => $timestamp
				));
				$insert_id = $this->db->insert_id();
				$surcharge->captureCurrentState();
				$this->setId($surcharge, $insert_id);
			}
		}
		
		// Edit
		else if ($surcharge->stored()) {
			$prevSurcharge   = $surcharge->prevState();
			$updateSurcharge = array();
			if (($surcharge->getDescription() != $prevSurcharge->getDescription()) && ($surcharge->getDescription())) {
				$updateSurcharge['description'] = $surcharge->getDescription();
			}
			if (($surcharge->getRuletype() != $prevSurcharge->getRuletype()) && ($surcharge->getRuletype())) {
				$updateSurcharge['rule_type'] = $surcharge->getRuletype();
			}
			if (($surcharge->getRule() != $prevSurcharge->getRule()) && ($surcharge->getRule())) {
				$updateSurcharge['rule'] = $surcharge->getRule();
			}
			if (($surcharge->getCost() != $prevSurcharge->getCost()) && ($surcharge->getCost())) {
				$updateSurcharge['cost'] = $surcharge->getCost();
			}
			if (($surcharge->getProfit() != $prevSurcharge->getProfit()) && ($surcharge->getProfit())) {
				$updateSurcharge['profit'] = $surcharge->getProfit();
			}
			if (($surcharge->getRatetype() != $prevSurcharge->getRatetype()) && ($surcharge->getRatetype())) {
				$updateSurcharge['rate_type'] = $surcharge->getRatetype();
			}
			
			if (count($updateSurcharge) > 0) {
				$this->db->where('id', $surcharge->getId());
				$this->db->update($this->getDbName(), $updateSurcharge);
			}
		}
	}
	
	public function batchStore(array $objArray) {
		foreach ($objArray as $obj) {
			if (!$obj instanceof Surcharge)
				continue;
			if ($obj->getSurchargeOf() instanceof SurchargeAwareInterface) {
				$this->store($obj);
			}
		}
	}
	
	public function remove($surcharge) {
		if ($surcharge instanceof Surcharge) {
			
		}
	}
	
	private function surchargeDbTableIdField($dbName) {
		$fieldId = false;
		switch ($dbName) {
			case $this::DB_SURCHARGE_HOTEL:
				$fieldId = 'hotel_id';
				break;
			case $this::DB_SURCHARGE_HOTEL_ROOM:
				$fieldId = 'package_hotel_id';
				break;
			case $this::DB_SURCHARGE_PACKAGE:
				$fieldId = 'package_id';
				break;
		}
		return $fieldId;
	}

	public function add_packagesurcharge($arrdata){
    	 	$this->db->trans_start();
            $this->db->insert('package_surcharges',$arrdata);
            $id = $this->db->insert_id();
     		$this->db->trans_commit();
            if($this->db->trans_status()) return $id;
            else return "Package surcharge is not saved";   
	}

	//to update room surcharge
	public function getSurcharge_id($type, $id, $check=FALSE){
		if($type == 'package'){
			$table_name = 'package_surcharges';
		}elseif($type == 'hotel'){
			$table_name = 'hotel_surcharges';
		}else{
			$table_name = 'hotelroom_surcharges';
		}
		
		return $this->db->get_where($table_name, array('id'=>$id,"status"=>"active"))->row();
	}

	public function edit_HotelRoomSurcharge($data){
		$roomSurchToUpdate = array();
		$prev = $data->prevState;
		
		if (($data->description != $prev->description) && ($data->description)) {
			$roomSurchToUpdate['description'] = $data->description;
		}

		if (($data->rule_type != $prev->rule_type) && ($data->rule_type)) {
			$roomSurchToUpdate['rule_type'] = $data->rule_type;
		}

		if (($data->rule != $prev->rule) && ($data->rule)) {
			$roomSurchToUpdate['rule'] = $data->rule;
		}

		if (($data->cost != $prev->cost) && ($data->cost)) {
			$roomSurchToUpdate['cost'] = $data->cost;
		}

		if (($data->profit != $prev->profit) && ($data->profit)) {
			$roomSurchToUpdate['profit'] = $data->profit;
		}

		if (($data->rate_type != $prev->rate_type) && ($data->rate_type)) {
			$roomSurchToUpdate['rate_type'] = $data->rate_type;
		}

		// dump(count($roomSurchToUpdate));
		// dump($roomSurchToUpdate);
		// dump($data);
		if (count($roomSurchToUpdate) > 0) {
			$this->db->where('id', $data->id);
			$this->db->update('hotelroom_surcharges', $roomSurchToUpdate);
		}
	}

	/*
	*get surcharge
	*/
	public function getSurchargeByType_id($type, $id, $check=FALSE){
		if($type == 'package'){
			$table_name = 'package_surcharges';
			$id_name = 'package_id';
		}elseif($type == 'hotel'){
			$table_name = 'hotel_surcharges';
			$id_name = 'hotel_id';
		}else{
			$table_name = 'hotelroom_surcharges';
			$id_name = 'hotelroom_id';
		}
		
		// $not = array('rule_type' => 'blackout');
		// $this->db->where($not);
		$results = $this->db->get_where($table_name, array($id_name=>$id,"status"=>"active"));
		$data = array();
		if($results->num_rows > 0){
			$data = $results->result_array();
			if($check) $data = $this->getRuleIfAgent($data);
		}
		return $data;
	}
	/*
	*end of get surcharge
	*/

	/*
	*custom fuction to get surcharge of each room rate
	*/
	function getRoomRateSurcharge_id($roomrate_id){
		$this->db->where(array("hotelroom_id"=>$roomrate_id,"status"=>"active")); #get hotelroom surcharge by id and status active
		$q = $this->db->get("hotelroom_surcharges");
		$surcharges = $q->result_array();
		$tmp_surcharge = array();
		if(count($surcharges)!=0){
			foreach ($surcharges as $surcharge) {
				$tmp_surcharge[] = array(
					"id" => $surcharge['id'],
					"profit" => $surcharge['profit'],
					"cost" => $surcharge['cost'],
					"rate_type" => $surcharge['rate_type'],
					"rule_type" => $surcharge["rule_type"],
					"rule" => $surcharge["rule"],
					"description" => $surcharge["description"],
					"hotelroom_id" => $surcharge["hotelroom_id"]
				);
			}
			
		}else{
				$tmp_surcharge=false;
		}
		return $tmp_surcharge;
	}
	/*
	*end of custom function gto get surcharge of each room rate
	*/


	/*
	*get hotel room information ----------------------------------------------
	*/
	public function getHotelRoomInformation($room_id){
		#perform query ----------------------------------------------
		$this->db->select("*");
		$this->db->where(array(
							//"id"=>$room_id,
							"package_hotel_id"=>$room_id
						));
		$q = $this->db->get("hotelroom_rates");

		#return result ----------------------------------------------
		return $q->row();
	}
	/*
	*end of get hotel room information ----------------------------------------------
	*/

	/*
	*get addon information ----------------------------------------------
	*/
	public function getAddonInformation($addon_id){
		
	}
	/*
	*end of get addon information
	*/

	public function processSurcharge($surcharges=array(),$dates=array()){
		#$ctgrs = array('packages','hotels','hotelrooms');
		$ctgrs = array('packages');
		$new = array();
		foreach($ctgrs as $ctgry){

			foreach($surcharges[$ctgry] as $surcharge){
				$multiplier = 1;
				$insert = false;
				//$ctgry == 'hotels' && 	
				if($surcharge['rule_type'] == 'day_of_week'){
					$date_s = $this->dateRange($dates['depart'], $dates['return']);
					$insert = in_array($surcharge['rule'], $date_s);
				}elseif($surcharge['rule_type'] == 'agent'){
					$user_id = $this->session->userdata['id'];
					if($user_id === $surcharge['rule']){
						$surcharge['agent_name'] = $this->getAgentName($surcharge['rule']);
						$insert = true;
					}
				}elseif($surcharge['rule_type'] == 'date_range'){
					$new_rule = explode(' to ', $surcharge['rule']);
					if(!($dates['depart'] >= $new_rule[1] || $new_rule[0] >= $dates['return'] || $new_rule[1] <= $new_rule[0] || $dates['return'] <= $dates['depart'])){
						$insert = true;
						$start = $new_rule[0] <= $dates['depart'] ? $dates['depart'] : $new_rule[0];
						$end = $new_rule[1] <= $dates['return'] ? $new_rule[1] : $dates['return'];
						
						$datetime1 = new DateTime($start);
						$datetime2 = new DateTime($end);
						$diff      = $datetime1->diff($datetime2);
						
						$surcharge['isect_from'] = $start;
						$surcharge['isect_to']	 = $end;
						$surcharge['isect_days'] = $diff->days;
					}
				}else{
					$new_rule = explode(' to ', $surcharge['rule']);
					$insert = ($dates['return'] >= $new_rule[0] && $dates['depart'] <= $new_rule[1]);							
					$surcharge['type'] = $ctgry;
					$multiplier = 0;
				}
				$surcharge['table'] = substr($ctgry, 0, -1);
				if($insert){ 
					$new[$surcharge['rule_type']][] = $surcharge;
				}
			}
		}
		return $new;

	}


	public function processSurchargePackage($surcharges=array(),$dates=array()){
		$ctgrs = array('packages');
		$new = array();
		foreach($ctgrs as $ctgry){

			foreach($surcharges[$ctgry] as $surcharge){
				$multiplier = 1;
				$insert = false;
				//$ctgry == 'hotels' && 	
				if($surcharge['rule_type'] == 'day_of_week'){
					$date_s = $this->dateRange($dates['depart'], $dates['return']);
					$insert = in_array($surcharge['rule'], $date_s);
				}elseif($surcharge['rule_type'] == 'agent'){
					$user_id = $this->session->userdata['id'];
					if($user_id === $surcharge['rule']){
						$surcharge['agent_name'] = $this->getAgentName($surcharge['rule']);
						$insert = true;
					}
				}elseif($surcharge['rule_type'] == 'date_range'){
					$new_rule = explode(' to ', $surcharge['rule']);
					if(!($dates['depart'] > $new_rule[1] || $new_rule[0] > $dates['return'] || $new_rule[1] < $new_rule[0] || $dates['return'] < $dates['depart'])){
						$insert = true;
						$start = $new_rule[0] < $dates['depart'] ? $dates['depart'] : $new_rule[0];
						$end = $new_rule[1] < $dates['return'] ? $new_rule[1] : $dates['return'];
						
						$datetime1 = new DateTime($start);
						$datetime2 = new DateTime($end);
						$diff      = $datetime1->diff($datetime2);
						
						$surcharge['isect_from'] = $start;
						$surcharge['isect_to']	 = $end;
						$surcharge['isect_days'] = $diff->days;
					}
				}else{
					$new_rule = explode(' to ', $surcharge['rule']);
					$insert = ($dates['return'] >= $new_rule[0] && $dates['depart'] <= $new_rule[1]);							
					$surcharge['type'] = $ctgry;
					$multiplier = 0;
				}
				$surcharge['table'] = substr($ctgry, 0, -1);
				if($insert){ 
					$new[$surcharge['rule_type']][] = $surcharge;
				}
			}
		}
		return $new;

	}

	public function processSurchargeHotels($surcharges=array(),$dates=array()){
		#$ctgrs = array('packages','hotels','hotelrooms');
		$ctgrs = array('hotels');
		$new = array();
		foreach($ctgrs as $ctgry){

			foreach($surcharges[$ctgry] as $surcharge){
				$multiplier = 1;
				$insert = false;
				//$ctgry == 'hotels' && 	
				if($surcharge['rule_type'] == 'day_of_week'){
					$date_s = $this->dateRange($dates['depart'], $dates['return']);
					$insert = in_array($surcharge['rule'], $date_s);
				}elseif($surcharge['rule_type'] == 'agent'){
					$user_id = $this->session->userdata['id'];
					if($user_id === $surcharge['rule']){
						$surcharge['agent_name'] = $this->getAgentName($surcharge['rule']);
						$insert = true;
					}
				}elseif($surcharge['rule_type'] == 'date_range'){
					$new_rule = explode(' to ', $surcharge['rule']);
					if(!($dates['depart'] > $new_rule[1] || $new_rule[0] > $dates['return'] || $new_rule[1] < $new_rule[0] || $dates['return'] < $dates['depart'])){
						$insert = true;
						$start = $new_rule[0] < $dates['depart'] ? $dates['depart'] : $new_rule[0];
						$end = $new_rule[1] < $dates['return'] ? $new_rule[1] : $dates['return'];
						
						$datetime1 = new DateTime($start);
						$datetime2 = new DateTime($end);
						$diff      = $datetime1->diff($datetime2);
						
						$surcharge['isect_from'] = $start;
						$surcharge['isect_to']	 = $end;
						$surcharge['isect_days'] = $diff->days;
					}
				}else{
					$new_rule = explode(' to ', $surcharge['rule']);
					$insert = ($dates['return'] >= $new_rule[0] && $dates['depart'] <= $new_rule[1]);							
					$surcharge['type'] = $ctgry;
					$multiplier = 0;
				}
				$surcharge['table'] = substr($ctgry, 0, -1);
				if($insert){ 
					$new[$surcharge['rule_type']][] = $surcharge;
				}
			}
		}
		return $new;

	}

	public function processSurchargeHotelRooms($surcharges=array(),$dates=array()){

		#$ctgrs = array('packages','hotels','hotelrooms');
		$ctgrs = array('hotelrooms');
		$new = array();
		foreach($ctgrs as $ctgry){

			foreach($surcharges[$ctgry] as $surcharge){
				$multiplier = 1;
				$insert = false;
				//$ctgry == 'hotels' && 	
				if($surcharge['rule_type'] == 'day_of_week'){
					$date_s = $this->dateRange($dates['depart'], $dates['return']);
					$insert = in_array($surcharge['rule'], $date_s);
				}elseif($surcharge['rule_type'] == 'agent'){
					$user_id = $this->session->userdata['id'];
					if($user_id === $surcharge['rule']){
						$surcharge['agent_name'] = $this->getAgentName($surcharge['rule']);
						$insert = true;
					}
				}elseif($surcharge['rule_type'] == 'date_range'){
					$new_rule = explode(' to ', $surcharge['rule']);
					if(!($dates['depart'] > $new_rule[1] || $new_rule[0] > $dates['return'] || $new_rule[1] < $new_rule[0] || $dates['return'] < $dates['depart'])){
						$insert = true;
						$start = $new_rule[0] < $dates['depart'] ? $dates['depart'] : $new_rule[0];
						$end = $new_rule[1] < $dates['return'] ? $new_rule[1] : $dates['return'];
						
						$datetime1 = new DateTime($start);
						$datetime2 = new DateTime($end);
						$diff      = $datetime1->diff($datetime2);
						
						$surcharge['isect_from'] = $start;
						$surcharge['isect_to']	 = $end;
						$surcharge['isect_days'] = $diff->days;
					}
				}else{
					$new_rule = explode(' to ', $surcharge['rule']);
					$insert = ($dates['return'] >= $new_rule[0] && $dates['depart'] <= $new_rule[1]);							
					$surcharge['type'] = $ctgry;
					$multiplier = 0;
				}
				$surcharge['table'] = substr($ctgry, 0, -1);
				if($insert){ 
					$new[$surcharge['rule_type']][] = $surcharge;
				}
			}
		}
		//dump($new);
		return $new;

	}
	

	public function dateRange($first, $last, $step = '+1 day', $format = 'l' ) {
	    $dates = array();
	    $current = strtotime($first);
	    $last = strtotime($last);
		
	    while( $current <= $last ) {	
	        $dates[] = date($format, $current);
	        $current = strtotime($step, $current);
	    }
	    return $dates;
	}


	/**
	* accepts id of user/agent
	* returns name of agent only
	**/
	public function getAgentName($id){
		$agent = $this->db->get_where('users',array('id'=>$id))->result_array();
		$agent_name = $agent[0]['lastname'].', '.$agent[0]['firstname'];
		return $agent_name;
	}



	
	/**
	*checks if any of the surcharges in the array has agent type
	*retrieves name of agent.
	*returns array but with name of agent as rule.
	**/
	public function getRuleIfAgent($surcharges = array()){
		$new_surcharges = array();
		foreach($surcharges as $surcharge){
			if($surcharge['rule_type'] == 'agent'){
				$agent = $this->db->get_where('users',array('id'=>$surcharge['rule']))->result_array();
		    	$agent_name = $agent[0]['lastname'].', '.$agent[0]['firstname'];
				$surcharge['rule'] = $agent_name;
			}
			$new_surcharges[] = $surcharge;
		}
		return $new_surcharges;
	}



	/*
	*lester padul code here ----------------------------------------------
	*/
	public function getSurchargeInformation($type, $id){
		$this->db->select("*");
		$this->db->where(array("id"=>$id));
		$q = "";
		if($type=="package"){
			$q=$this->db->get("package_surcharges");
		}else if($type=="hotel"){
			$q=$this->db->get("hotel_surcharges");
		}else if($type=="rooms"){
			$q=$this->db->get("hotelroom_surcharges");
		}

		return $q->row();
	}















}