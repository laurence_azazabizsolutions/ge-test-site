<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Bookingstorage extends CI_Model {

	public function __construct() {
		$this->load->database();
	}

	/* (non-PHPdoc)
	 * @see IdManipulatorInterface::setId()
	 */
	public function setId($hotel, $id) {
		setprivateid($hotel, 'Hotel', $id);
	}

	public function getUser_Id($id) {
		$this->db->select("*");
		$q = $this->db->get_where("bookings", array("code"=>$id))->result_array();
		return $q;
	}

	/*
	*get booking information
	*/
	public function getBookingInformation($booking_id){
		$arr  = array();
		$this->db->select("bk.*, pkg_hotel.hotel_id, hotel.name as exhotel_name");
		$this->db->join("package_hotels as pkg_hotel","pkg_hotel.id = bk.exhotel_id", "left");
		$this->db->join("hotels as hotel", "hotel.id = pkg_hotel.hotel_id", "left");
		$arr =  $this->db->get_where("bookings as bk", array("bk.id"=>$booking_id))->result_array();
		$arr[0]["people_count"] = $this->getBookingPeopleCount($booking_id);
		$arr[0]["hotels"] = $this->getBookingHotels($booking_id);
		$arr[0]["addons"] = $this->getBookingAddons($booking_id);
		return $arr;
	}

	/*
	*get booking addons
	*/
	public function getBookingAddons($booking_id){
		$arr = array();
		return $this->db->get_where("applied_addons", array("booking_id"=>$booking_id))->result_array();
	}


	/*
	*get people counter
	*/
	public function getBookingPeopleCount($booking_id){
		$arr = array();

		$q = $this->db->get_where("traveller_details", array("type"=>"adult", "booking_id"=>$booking_id));
		$arr["adult_cnt"] = count($q->result_array());

		$qq = $this->db->get_where("traveller_details", array("type"=>"child", "booking_id"=>$booking_id));
		$arr["child_cnt"] = count($qq->result_array());

		$qqq = $this->db->get_where("traveller_details", array("type"=>"infant", "booking_id"=>$booking_id));
		$arr["infant_cnt"] = count($qqq->result_array());

		return $arr;
	}

	/*
	*get booking hotels
	*/
	public function getBookingHotels($booking_id){
		$this->db->select("package_hotel_id, remarks");
		$q = $this->db->get_where("booked_hotels", array("booking_id"=>$booking_id))->result_array();
		#$q[0]["traveller_details"] =  $this->getTravellerDetails($booking_id);
		return $q;
	}



	/*
	*fetch get traveller details
	*/
	public function getTravellerDetails($booking_id){
		$arr = array();

		$q = $this->db->get_where("traveller_details", array("type"=>"adult", "booking_id"=>$booking_id));
		$arr["adult_cnt"] = $q->result_array();

		$qq = $this->db->get_where("traveller_details", array("type"=>"child", "booking_id"=>$booking_id));
		$arr["child_cnt"] = $qq->result_array();

		$qqq = $this->db->get_where("traveller_details", array("type"=>"infant", "booking_id"=>$booking_id));
		$arr["infant_cnt"] = $qqq->result_array();

		return $arr;
	}



	/*
	*
	*Start
	*This function addBooking insert into database
	*
	*/
	public function addBooking($booking){
    	 	$this->db->trans_start();
    	 	$this->db->insert('bookings',$booking);
            $data['id']= $this->db->insert_id();
            $this->db->trans_commit();
			if ($this->db->trans_status() === true) {
				return  $data;
			} else {
				return false;
			}
	}
	/*
	*
	*End
	*This function addBooking insert into database
	*

	/*
	*
	*Start
	*This function addBookedhotels insert into database
	*
	*/
	public function addBookHotels($booking){
    	 	$this->db->trans_start();
            $this->db->insert('booked_hotels',$booking);
            $data['id']= $this->db->insert_id();
     		$this->db->trans_commit();
            if($this->db->trans_status()) return  $data;
	}
	/*
	*
	*End
	*This function addBookedhotels insert into database
	*
	*/

	/*
	*
	*Start
	*This function addBookHotelrooms insert into database
	*
	*/
	public function addBookHotelrooms($data_roomid,$data_arr,$data_room){

    	 	$this->db->trans_start();
				$save_arr['hotelroom_id'] =$data_roomid;//set as array
				$save_arr['booked_hotel_id'] = $data_arr;
				$save_arr['quantity'] = $data_room;
				$data=$this->db->insert('booked_hotelrooms',$save_arr);
     		$data= $this->db->insert_id();
     		$this->db->trans_commit();
            	if($this->db->trans_status()) return  $data;
	}
	/*
	*
	*End
	*This function addBookHotelrooms insert into database
	*
	*/

	/*Start
	*This function addappliedhotelroomsurcharges insert into database
	*
	*/
	public function addappliedhotelroomsurcharges($tmp_arr,$data_arr,$data_room){
    	 	$this->db->trans_start();
    	 	$booking['hotelroom_surcharge_id']=$tmp_arr;
    	 	$booking['booked_hotel_id'] = $data_arr;
    	 	$booking['quantity'] = $data_room;
            $data=$this->db->insert('applied_hotelroom_surcharges',$booking);
     		$data= $this->db->insert_id();
     		$this->db->trans_commit();
            if($this->db->trans_status()) return  $data;
	}
	/*
	*
	*End
	*This function addappliedhotelroomsurcharges insert into database
	*
	*/


	/*Start
	*This function addappliedhotelsurcharges insert into database
	*
	*/
	public function addappliedhotelsurcharges($tmp_arr,$data_arr,$data_room){
    	 	$this->db->trans_start();
    	 	$booking['hotel_surcharges_id']=$tmp_arr;
    	 	$booking['booked_hotel_id'] = $data_arr;
    	 	$booking['quantity'] = $data_room;
            $data=$this->db->insert('applied_hotel_surcharges',$booking);
     		$data= $this->db->insert_id();
     		$this->db->trans_commit();
            if($this->db->trans_status()) return  $data;
	}
	/*
	*
	*End
	*This function addappliedhotelsurcharges insert into database
	*
	*/

	/*Start
	*This function applied_addons insert into database
	*
	*/
	public function addappliedaddons($booking){
    	 	$this->db->trans_start();
            $data=$this->db->insert('applied_addons',$booking);
     		$data= $this->db->insert_id();
     		$this->db->trans_commit();
            if($this->db->trans_status()) return  $data;
	}
	/*
	*
	*End
	*This function applied_addons insert into database
	*
	*/

	/*Start
	*This function addpayments insert into database
	*
	*/
	public function addpayments($booking){
    	 	$this->db->trans_start();
            $data=$this->db->insert('payments',$booking);
     		$data= $this->db->insert_id();
     		$this->db->trans_commit();
            if($this->db->trans_status()) return  $data;
	}
	/*
	*
	*End
	*This function addpayments insert into database
	*
	*/

	/*Start
	*This function addappliedpackagesurcharges insert into database
	*
	*/
	public function addappliedpackagesurcharges($tmp_arr,$data_arr,$data_room){
    	 	$this->db->trans_start();
    	 	$booking['package_surcharge_id']=$tmp_arr;
    	 	$booking['booking_id'] = $data_arr;
    	 	$booking['quantity'] = $data_room;
            $data=$this->db->insert('applied_package_surcharges',$booking);
     		$data= $this->db->insert_id();
     		$this->db->trans_commit();
            if($this->db->trans_status()) return  $data;
	}
	/*
	*
	*End
	*This function addappliedpackagesurcharges insert into database
	*
	*/

	/*Start
	*This function addtravellerdetails insert into database
	*
	*/
	public function addtravellerdetails($booking){
    	 	$this->db->trans_start();
            $this->db->insert('traveller_details',$booking);
            $data= $this->db->insert_id();
     		$this->db->trans_commit();
            if($this->db->trans_status()) return  $data;
	}
	/*
	*
	*End
	*This function addtravellerdetails insert into database
	*
	*/

	/*Start
	*This function getRefnum insert into database
	*
	*/
	public function getRefnum(){
		$q = $this->db->get_where('reference_number', array('id'=>1));
		return $q->result();
	}
	/*
	*
	*End
	*This function getRefnum insert into database
	*
	*/

	/*Start
	*This function changeBooking select into database
	*
	*/
	public function changeBooking($id){
			 $this->db->where('id',$id);
 			$q = $this->db->get('bookings');
			return $q->result();
	}
	/*
	*
	*End
	*This function changeBooking select into database
	*
	*/


	/*
	*lester padul codes here...
	*insert the summary generated from step 1 booking
	*/
	public function insertSummaryBookings($arr,$booking_id){
		$tmp = array();
		#echo "booking id : {$booking_id} summary_data :  <hr>";
		#loop through the destinations ----------------------------------------------
		$remark = @$arr["bd_ext_remark"];
		$time = time();

		#check if not null package surcharges summary
		#echo "<h3>Package Surcharge summary</h3>";
		//package surcharges
		for($i=0; $i<count($arr["bd_pkg_sur_description"]);$i++){
			$description = $arr["bd_pkg_sur_description"][$i];
			$computation =  $this->chopFirstXGroup($arr["bd_pkg_sur_computation"][$i]);
			$subtotal = $arr["bd_pkg_sur_subtotal"][$i];
			$type = "0package_surcharge";
			if(!empty($description) && $subtotal!="0"){
				$tmp[] = array(
					"booking_id" => $booking_id,
					"destination_id" => "0",
					"hotel_id" => "0",
					"description" => $description,
					"computation" => $computation,
					"subtotal" => floatVal($subtotal),
					"type" => $type,
					"remark"=> "",
					"timestamp"=> $time
				);
				#echo "description : {$description} / computation : {$computation} / subtotal : {$subtotal} <br>";
			}
		}

		#check if not null - extension
		#echo "<h3>Extension summary</h3>";
		//extension summary
		$hotel_id = $arr["bd_destination_hotel"][count($arr["bd_destination_hotel"])-1];
		for($i=0; $i<count($arr["bd_ext_description"]);$i++){
			$description = $arr["bd_ext_description"][$i];
			$computation =  $this->chopFirstXGroup($arr["bd_ext_computation"][$i]);
			$subtotal = $arr["bd_ext_subtotal"][$i];
			$type = "5extension";
			if(!empty($description) && $subtotal!="0"){
				$tmp[] = array(
					"booking_id" => $booking_id,
					"destination_id" => "0",
					"hotel_id" => $hotel_id,
					"description" => $description,
					"computation" => $computation,
					"subtotal" => floatVal($subtotal),
					"type" => $type,
					"remark"=> "{$remark}",
					"timestamp"=> $time
				);
				#echo "description : {$description} / computation : {$computation} / subtotal : {$subtotal} <br>";
			}
		}

		#check if not null - summary
		#echo "<h3>Addon summary</h3>";
		//addon summary

		for($i=0; $i<count($arr["bd_addon_description"]);$i++){
			$description = $arr["bd_addon_description"][$i];
			$computation =  $this->chopFirstXGroup($arr["bd_addon_computation"][$i]);
			$subtotal = $arr["bd_addon_subtotal"][$i];
			$type = "4addon";
			if(!empty($description) && $subtotal!="0"){
				$tmp[] = array(
					"booking_id" => $booking_id,
					"destination_id" => "0",
					"hotel_id" => "0",
					"description" => $description,
					"computation" => $computation,
					"subtotal" => floatVal($subtotal),
					"type" => $type,
					"remark"=> "",
					"timestamp"=> $time
				);
				#echo "description : {$description} / computation : {$computation} / subtotal : {$subtotal} <br>";
			}
		}

		#destination looper
		for($i=0;$i<count($arr["destination_id"]);$i++){
			$destination_id = $arr["destination_id"][$i];
			$hotel_id = $arr["bd_destination_hotel"][$i];
			$h_id = explode(".", $hotel_id);
			$hotel_id= $h_id[0];
			#echo "<h3>Destination id : {$destination_id}</h3>";

			#echo "<h3>Hotel Surcharges</h3>";
			for($ii=0;$ii<count($arr["bd_hotel_sur_description"][$i]);$ii++){
				$description = $arr["bd_hotel_sur_description"][$i][$ii];
				$computation =  $this->chopFirstXGroup($arr["bd_hotel_sur_computation"][$i][$ii]);
				$subtotal = $arr["bd_hotel_sur_subtotal"][$i][$ii];
				$type = "1hotel_surcharge";
				if(!empty($description) && $subtotal!="0"){
					$tmp[] = array(
						"booking_id" => $booking_id,
						"destination_id" => $destination_id,
						"hotel_id" => $hotel_id,
						"description" => $description,
						"computation" => $computation,
						"subtotal" =>  floatVal($subtotal),
						"type" => $type,
						"remark"=> "",
						"timestamp"=> $time
					);
					#echo "description : {$description} / computation : {$computation} / subtotal : {$subtotal} <br>";
				}
			}

			#echo "<h3>Room Surcharges</h3>";
			for($ii=0;$ii<count($arr["bd_rs_description"][$i]);$ii++){
				$description = $arr["bd_rs_description"][$i][$ii];
				$computation =  $this->chopFirstXGroup($arr["bd_rs_computation"][$i][$ii]);
				$subtotal = $arr["bd_rs_subtotal"][$i][$ii];
				$type = "2room_surcharge";
				if(!empty($description) && $subtotal!="0"){
					$tmp[] = array(
						"booking_id" => $booking_id,
						"destination_id" => $destination_id,
						"hotel_id" => $hotel_id,
						"description" => $description,
						"computation" => $computation,
						"subtotal" =>  floatVal($subtotal),
						"type" => $type,
						"remark"=> "",
						"timestamp"=> $time
					);
					#echo "description : {$description} / computation : {$computation} / subtotal : {$subtotal} <br>";
				}
			}


			#echo "<h3>Room Payments</h3>";
			for($ii=0;$ii<count($arr["bd_room_description"][$i]);$ii++){
				$description = $arr["bd_room_description"][$i][$ii];
				$computation = $this->chopFirstXGroup($arr["bd_room_computation"][$i][$ii]);
				$subtotal = $arr["bd_room_subtotal"][$i][$ii];
				$type = "3room_payment";
				if(!empty($description) && $subtotal!="0"){
					$tmp[] = array(
						"booking_id" => $booking_id,
						"destination_id" => $destination_id,
						"hotel_id" => $hotel_id,
						"description" => $description,
						"computation" => $computation,
						"subtotal" =>  floatVal($subtotal),
						"type" => $type,
						"remark"=> "",
						"timestamp"=> $time
					);
					#echo "description : {$description} / computation : {$computation} / subtotal : {$subtotal} <br>";
				}
			}

		}

		$this->db->insert_batch("summary_data",$tmp);
	}
	/*
	*end of insert the summary generated from step 1 booking
	*/

	/*
	*generator of computation for the summary table
	*/
	function chopFirstXGroup($str){
		// return $str;
		$findX = strpos($str, "x");
		$new_string = substr($str, $findX+1);
		return trim($new_string);
	}
	/*
	*end of generator of computation for the summary table
	*/

	/*
	*get specific summary data information
	*/
	function getSummaryInformation($array){
		$this->db->select("summary_data.*,hotels.name as hotel_name, bookings.total_discount as discount,bookings.total_additional as additional");
		$this->db->join("hotels", "summary_data.hotel_id = hotels.id", "LEFT");
		$this->db->join("bookings", "summary_data.booking_id = bookings.id", "LEFT");
		$this->db->where($array);
		return $this->db->get("summary_data")->result_array();
	}

	/*
	*end of get specific summary data information
	*/

	/*
	*get destinations
	*/
	function getSummaryDestinations($booking_id){
		$this->db->select("summary_data.destination_id, package_destinations.country_code, countries.country, package_destinations.nights");
		$this->db->from("summary_data");
		$this->db->join("package_destinations","summary_data.destination_id=package_destinations.id", "LEFT");
		$this->db->join("countries","countries.code=package_destinations.country_code", "LEFT");
		$this->db->where("booking_id",$booking_id);
		$this->db->where("destination_id !=","0");
		$this->db->order_by("destination_id", "ASC");
		$this->db->group_by("destination_id");
		return $this->db->get()->result_array();
	}
	/*
	*end of get destinations
	*/

	function getPackagesDescription($id){
		$this->db->select('packages.title,packages.tnc,package_description.description,package_duration.date_from,package_duration.date_to');
		$this->db->join("packages","packages.id=package_description.package_id", "LEFT");
		$this->db->join("package_duration","package_duration.package_id=package_description.package_id", "LEFT");
		$this->db->where('package_description.package_id',$id);
		$q = $this->db->get('package_description');
		return $q->result();
	}


}