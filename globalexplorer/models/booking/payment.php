<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Payment extends CI_Model{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	function insert_payment($values)
	{
		$q = $this->db->insert_batch('payments',$values);
		if($q)
			return true;
		else
			return false;
	}
}
