<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

get_instance()->load->iface('storeiteminterface');
get_instance()->load->iface('capturestateinterface');

class Booking extends MY_Model implements StoreItemInterface, CaptureStateInterface {
	//MODEL SETTINGS
	protected $_table_name = 'bookings';
	protected $_order_by = 'id asc';

	//general info
	private $id;
	private $user;
	private $package_id;
	private $code;
	private $departure_date;
	private $return_date;

	//[origin] flight details
	private $of_num;
	private $of_from;
	private $of_to;
	private $of_departuretime;
	private $of_arrivaltime;
	private $of_remarks;

	//[return] flight details
	private $rf_num;
	private $rf_from;
	private $rf_to;
	private $rf_departuretime;
	private $rf_arrivaltime;
	private $rf_remarks;

	//hotel details
	private $checkin_date;
	private $checkout_date;
	private $hotel_remarks;
	private $hotel_status;

	private $total_cost;
	private $total_profit;
	private $total_price;
	private $status;
	private $confirmation_remarks;
	private $date_booked;
	
	//applied/booked
	private $addon;
	private $hotel;
	private $hotelroomrate;
	private $surcharge;
	private $traveller;
	private $prevState;
	
	public function __construct() {
		parent::__construct();
		$this->addon  = array();
		$this->hotel  = array();
		$this->hotelrate  = array();
		$this->traveller  = array();
		$this->surcharge = array();
	}
	
	public function getId() {
		return $this->id;
	}

	public function getPackageId() {
		return $this->package_id;
	}

	public function setPackageId($package_id) {
		$this->package_id = $package_id;
	}
	
	public function getUser() {
		return $this->user;
	}
	
	public function setUser($user) {
		if(isset($user) && ($user instanceof Admin || $user instanceof Customer))
			$this->user = $user;
	}
	
	public function getCode() {
		return $this->code;
	}
	
	public function setCode($code) {
		$this->code = $this->checkAlphaNum($code);
	}
	
	public function getDateRange() {
		return array(
			'departure_date' => $this->departure_date,
			'return_date' => $this->return_date
		);
	}
	
	public function setDateRange($departure_date, $return_date) {
		if ($this->checkValidRange($departure_date, $return_date) !== NULL) {
			$this->departure_date = $departure_date;
			$this->return_date   = $return_date;
		} else {
			$this->departure_date = null;
			$this->return_date   = null;
		}
	}
	
	public function getRangeDiff() {
		return $this->checkValidRange($this->departure_date, $this->return_date);
	}
	
	//flight details
	//originating flight
	public function getOfNum(){
		return $this->of_num;
	}

	public function getOfFrom(){
		return $this->of_from;
	}

	public function getOfTo(){
		return $this->of_to;
	}

	public function getOfDepartureTime(){
		return $this->of_departuretime;
	}

	public function getOfArrivalTime(){
		return $this->of_arrivaltime;
	}

	public function getOfRemarks(){
		return $this->$of_remarks;
	}

	//returning flight
	public function getRfNum(){
		return $this->rf_num;
	}

	public function getRfFrom(){
		return $this->rf_from;
	}

	public function getRfTo(){
		return $this->rf_to;
	}

	public function getRfDepartureTime(){
		return $this->rf_departuretime;
	}

	public function getRfArrivalTime(){
		return $this->rf_arrivaltime;
	}

	public function getRfRemarks(){
		return $this->rf_remarks;
	}

	public function setRoundTrip($trip){
		$this->of_num = $this->checkString($trip['of_num']);
		$this->of_from = $this->checkString($trip['of_from']);
		$this->of_to = $this->checkString($trip['of_to']);
		$this->of_departuretime = $this->checkString($trip['of_departuretime']);
		$this->of_arrivaltime = $this->checkString($trip['of_arrivaltime']);
		$this->of_remarks = $this->checkString($trip['of_remarks']);
		
		$this->rf_num = $this->checkAlphaNum($trip['rf_num']);
		$this->rf_from = $this->checkString($trip['rf_from']);
		$this->rf_to = $this->checkString($trip['rf_to']);
		$this->rf_departuretime = $this->checkString($trip['rf_departuretime']);
		$this->rf_arrivaltime = $this->checkString($trip['rf_arrivaltime']);
		$this->rf_remarks = $this->checkString($trip['rf_remarks']);
	}
	//end flight details	
	
	public function getSurcharge() {
		return $this->surcharge;
	}
	
	public function addSurcharge(Surcharge $surcharge) {
		if ($surcharge instanceof Surcharge) {
			$this->surcharge[] = $surcharge;
		}
	}
	
	public function getAddon() {
		return $this->addon;
	}
	
	public function addAddon(Addon $addon) {
		if ($addon instanceof Addon) {
			$this->addon[] = $addon;
		}
	}

	public function getHotel() {
		return $this->hotel;
	}
	
	public function addHotel(Hotel $hotel) {
		if ($hotel instanceof Hotel) {
			$this->hotel[] = $hotel;
		}
	}

	public function getHotelRoomRate() {
		return $this->hotelroomrate;
	}
	
	public function addHotelRoomRate(HotelRoomRate $hotelroomrate) {
		if ($hotelroomrate instanceof HotelRoomRate) {
			$this->hotelroomrate[] = $hotelroomrate;
		}
	}

	private function checkString($input) {
		return (is_string($input)) ? $input : null;
	}
	
	private function checkURL($url) {
		return (preg_match('/^(http\:\/\/|https\:\/\/)?([a-z0-9][a-z0-9\-]*\.)+[a-z0-9][a-z0-9\-]*((?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&;%@\.\w_]*)#?(?:[\.\!\/\\\w]*))?$/i', $url)) ? $url : null;
	}
	
	private function checkEmail($email) {
		return (filter_var($email, FILTER_VALIDATE_EMAIL)) ? $email : null;
	}
	
	private function checkPhone($phone) {
		return (preg_match('/^[0-9 ()+\-]+$/i', $phone)) ? $phone : null;
	}

	private function checkAlphaNum($code) {
		return (preg_match('/^[^\W_]+$/', $code)) ? $code : null;
	}

	private function checkValidRange($dateFrom, $dateTo) {
		if ($this->checkDate($dateFrom) && $this->checkDate($dateTo)) {
			$datetime1 = new DateTime($dateFrom);
			$datetime2 = new DateTime($dateTo);
			$diff      = $datetime2->diff($datetime1);
			return ($diff->days > 0 && $diff->invert == 1) ? $diff->days : null;
		} else
			return null;
	}
	
	public function prepared() {
		if ($this->name && is_null($this->id)) {
			return true;
		}
		return false;
	}
	
	public function stored() {
		if ($this->id) {
			return true;
		}
		return false;
	}
	
	/* (non-PHPdoc)
	 * @see CaptureStateInterface::captureCurrentState()
	 */
	public function captureCurrentState() {
		$this->prevState = clone $this;
	}

	/* (non-PHPdoc)
	 * @see CaptureStateInterface::prevState()
	 */
	public function prevState() {
		return (!empty($this->prevState)) ? $this->prevState : false;
	}

	//date of birth
	public function getNumberTodsAndInfs($children){
		// $infant_count = $toddler_count = 0;
		$data = $inf = $tod = array();
		if(count($children) > 0 && is_array($children)){
			foreach($children as $child){
				$bday = new DateTime($child);
				$today = new DateTime(date('Y-m-d'));
				$diff = $today->diff($bday); 
				if($diff->y < 13){
					if($diff->y < 3){
						$inf[] = $child;
					} else {
						$tod[] = $child;
					} 
				} else return false;
			}
			$data = array('infant_data'=>$inf,'toddler_data'=>$tod);
		}
		return $data;
	}
	
	public function processTravellers($travellers){
		$traveller = array();
		if(!empty($travellers['adult_name']) && isset($travellers['adult_name'])){	
			$cnt = 0;
			foreach($travellers['adult_name'] as $k=>$adult){
				$newdata = array();
				$cnt++;
				if($adult != ''){
					$newdata['name'] = $adult;
					$newdata['dob'] = $travellers['adult_dob'][$k];
					$newdata['ppno'] = $travellers['adult_ppno'][$k];
				}
				$traveller['adult'][] = $newdata;
			}
			$traveller['adult_count'] = $cnt;
			unset($travellers['adult_name']);
			unset($travellers['adult_dob']);
			unset($travellers['adult_ppno']);
		}
		if(!empty($travellers['infant_name']) && isset($travellers['infant_name'])){	
			$cnt = 0;
			foreach($travellers['infant_name'] as $k=>$infant){
				$newdata = array();
				$cnt++;
				if($infant != ''){
					$newdata['name'] = $infant;
					$newdata['dob'] = $travellers['infant_dob'][$k];
					$newdata['ppno'] = $travellers['infant_ppno'][$k];
				}
				$traveller['infant'][] = $newdata;
			}
			$traveller['infant_count'] = $cnt;
			unset($travellers['infant_name']);
			unset($travellers['infant_dob']);
			unset($travellers['infant_ppno']);
		}
		if(!empty($travellers['toddler_name']) && isset($travellers['toddler_name'])){	
			$cnt = 0;
			foreach($travellers['toddler_name'] as $k=>$toddler){
				$newdata = array();
				$cnt++;
				if($toddler != ''){
					$newdata['name'] = $toddler;
					$newdata['dob'] = $travellers['toddler_dob'][$k];
					$newdata['ppno'] = $travellers['toddler_ppno'][$k];
				}
				$traveller['toddler'][] = $newdata;
			}
			$traveller['toddler_count'] = $cnt;
			unset($travellers['toddler_name']);
			unset($travellers['toddler_dob']);
			unset($travellers['toddler_ppno']);
		}
		
		return array('flight'=>$travellers, 'traveller'=>$traveller);
	}

	public function processFlight($flight) {
		
		if(!empty($flight['flight_of_from']))
			$flight['flight_of_from'] = $this->getCodeOfAirportCodes_id($flight['flight_of_from']);
		if(!empty($flight['flight_rf_from']))
			$flight['flight_rf_from'] = $this->getCodeOfAirportCodes_id($flight['flight_rf_from']);
		if(!empty($flight['flight_of_to']))
			$flight['flight_of_to'] = $this->getCodeOfAirportCodes_id($flight['flight_of_to']);
		if(!empty($flight['flight_rf_to']))
			$flight['flight_rf_to'] = $this->getCodeOfAirportCodes_id($flight['flight_rf_to']);
		
		return $flight;
	}

	public function getCodeOfAirportCodes_id($id) {
		$this->db->select('code');
		$result = $this->db->get_where('airportcodes', array('id' => $id))->row();
		// $result = $this->db->get();
		return ($result->code) ? ($result->code) : 'DEF';
	}




	// PAGINATION BABY! STARTS
	function my_pagination($tbl_name,$per_page,$num_links,$chckStatus='',$user_id='') {
		$this->load->database();
		if ($this->uri->segment(2) === FALSE) {
		      $page_view = 'index';
		} else {
		      $page_view = $this->uri->segment(2);
		}

		if ($chckStatus == NULL) 
		{
			if($user_id == NULL)
			{
				$query = $this->db->get($tbl_name)->num_rows();
			}
			else
			{
				$query = $this->db->select('*')->from($tbl_name)->where('user_id',$user_id)->get()->num_rows();
			}		
		} 
		else 
		{
			$query = $this->db->select('*')->from($tbl_name)->where('status','pending')->get()->num_rows();
		}
		//$query = $this->db->select('*')->from($tbl_name)->where('user_id',$user_id)->get()->num_rows();
		$this->load->library('pagination');
		$config['base_url'] = base_url('panel/'.$page_view);
		$config['per_page'] = $per_page;
		$config['num_links'] = $num_links;
		$config['total_rows'] = $query;
		$config['full_tag_open'] = "<ul class='pagination pagination-sm'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";
		/*print_r($config['total_rows']);
		echo "<br/>";*/
		

		return $this->pagination->initialize($config);
	}

	//Pending Payments
	public function getAllPendingPayments($date_range= false, $where_array = false) {
		$this->db->select('bookings.id AS BOOKID,
			bookings.code AS BOOKINGCODE,
			packages.title AS PACKAGENAME,
			ACOMP.id AS COMPANYNAME_ID,
			ACOMP.name AS COMPANYNAME,
			PCOMP.id AS COMPANYNAMEP_ID,
			PCOMP.name AS COMPANYNAMEP,
			hotels.name AS HOTELNAME,
			bookings.departure_date AS BOOKINGDEPARTUREDATE,
			bookings.of_departuretime AS OFDEPARTTIME,
			bookings.of_arrivaltime AS OFARRIVALTIME,
			bookings.return_date AS BOOKINGRETURNDATE,
			bookings.rf_departuretime AS RFDEPARTTIME,
			bookings.rf_arrivaltime AS RFARRIVALTIME,
			bookings.total_cost AS BOOKINGCOST,
			bookings.total_profit AS BOOKINGPROFIT,
			bookings.total_price AS BOOKINGPRICE,
			bookings.total_discount AS BOOKINGDISCOUNT,
			bookings.total_additional AS BOOKINGADDITIONAL,
			bookings.total_difference AS BOOKINGDIFFERENCE,
			bookings.status AS BOOKINGSTATUS,
			traveller_details.name AS TRAVELLLERNAME,
            count(bookings.code) AS PAX');
        //$this->db->from('bookings');
		$this->db->join('payments'							,'payments.booking_id=bookings.id'						,'LEFT');
		$this->db->join('payment_type'						,'payment_type.id=payments.payment_type_id'				,'LEFT');
        $this->db->join('packages'                          ,'packages.id = bookings.package_id'                    ,'LEFT');
        $this->db->join('company_designations AS ACOMP_DEST','ACOMP_DEST.user_id = bookings.user_id'  				,'LEFT');
        $this->db->join('companies AS ACOMP'                ,'ACOMP.id = ACOMP_DEST.company_id'       				,'LEFT');
        $this->db->join('company_designations AS PCOMP_DEST','PCOMP_DEST.user_id = packages.provider_id'            ,'LEFT');
        $this->db->join('companies AS PCOMP'                ,'PCOMP.id = PCOMP_DEST.company_id'                     ,'LEFT');
        $this->db->join('booked_hotels'                 	,'booked_hotels.booking_id = bookings.id'           	,'LEFT');
        $this->db->join('package_hotels'                 	,'package_hotels.id = booked_hotels.package_hotel_id'   ,'LEFT');
        $this->db->join('hotels'                 			,'hotels.id = package_hotels.hotel_id'           		,'LEFT');
        $this->db->join('traveller_details'                 ,'traveller_details.booking_id = bookings.id'           ,'LEFT');

	    $this->db->where('bookings.id IN (SELECT MAX(bookings.id) FROM bookings GROUP BY bookings.code)');

		if($where_array){
			if(array_key_exists('ACOMP.id', $where_array)){
				$this->db->where('ACOMP.id',$where_array["ACOMP.id"]);
				$this->db->like($where_array);
			}
			else{
				$this->db->like($where_array);
			}
			
		}
		
		if($date_range){
			$this->db->where("bookings.departure_date between {$date_range}");
		}

	    $this->db->where('payments.date is NULL');
	    $this->db->group_by('traveller_details.booking_id');
	    
	    $q = $this->db->get("bookings");
	    return $q->result();
	}

	//Checking the payment types
	public function getAllPaymentRecords_collection() {
	    $this->db->select('payment_type.id AS PID,
            payment_type.type AS PTYPE,
            payment_type.description AS PDESC,
            payment_type.total_amount AS PTOT,
            payments.status AS PAYSTAT,
            payments.date AS PAYDATE');
        //$this->db->from('payment_type');
	    $this->db->join('payments'	,'payments.payment_type_id = payment_type.id'	,'LEFT');
	    $this->db->group_by('payment_type.id');

	    $q = $this->db->get("payment_type");
	    return $q->result();
	}

	//Gather data from payment type
	public function getAllCollectionPaymentData($date_range= false, $pid) {
		$this->db->select('bookings.id AS BOOKID,
            comp.name AS COMPANYNAME,
            packages.title AS PACKAGENAME,
            companies.name AS COMPANYNAMEP,
            bookings.code AS BOOKINGCODE,
            bookings.departure_date AS BOOKINGDEPARTUREDATE,
            bookings.return_date AS BOOKINGRETURNDATE,
            bookings.of_departuretime AS OFDEPARTTIME,
            bookings.of_arrivaltime AS OFARRIVALTIME,
            bookings.rf_departuretime AS RFDEPARTTIME,
            bookings.rf_arrivaltime AS RFARRIVALTIME,
            bookings.total_cost AS BOOKINGCOST,
            bookings.total_difference AS BOOKINGDIFFERENCE,
            bookings.total_profit AS BOOKINGPROFIT,
            bookings.total_price AS BOOKINGPRICE,
            bookings.total_discount AS BOOKINGDISCOUNT,
            bookings.total_additional AS BOOKINGADDITIONAL,
            bookings.status AS BOOKINGSTATUS,
            traveller_details.name AS TRAVELLLERNAME,
            payments.status AS PSTAT,
            payments.date AS PDATE,
            count(bookings.code) AS PAX');
        //$this->db->from('bookings');
		$this->db->join('payments'							,'payments.booking_id=bookings.id'						,'LEFT');
		//$this->db->join('payment_type'						,'payment_type.id=payments.payment_type_id'				,'LEFT');
        $this->db->join('packages'                          ,'packages.id = bookings.package_id'                    ,'LEFT');
        $this->db->join('company_designations'              ,'company_designations.user_id = packages.provider_id'  ,'LEFT');
        $this->db->join('companies'                         ,'companies.id = company_designations.company_id'       ,'LEFT');
        $this->db->join('company_designations AS comp_des'  ,'comp_des.user_id = bookings.user_id'                  ,'LEFT');
        $this->db->join('companies AS comp'                 ,'comp.id = comp_des.company_id'                        ,'LEFT');
        $this->db->join('traveller_details'                 ,'traveller_details.booking_id = bookings.id'           ,'LEFT');

		if($date_range){
			$this->db->where("bookings.departure_date between {$date_range}");
		}

	    $this->db->where('payments.payment_type_id', $pid);
	    $this->db->group_by('traveller_details.booking_id');

	    $q = $this->db->get("bookings");
	    return $q->result();
	}



	public function getAllPaymentRecords($start=false, $limit=false,$date_range= false, $search_term = false, $where_array = false) {
		$this->db->select('bookings.id AS BOOKID,
            comp.id AS COMPANYNAME_ID,
            comp.name AS COMPANYNAME,
            packages.title AS PACKAGENAME,
            companies.id AS COMPANYNAMEP_ID,
            companies.name AS COMPANYNAMEP,
            bookings.code AS BOOKINGCODE,
            bookings.departure_date AS BOOKINGDEPARTUREDATE,
            bookings.return_date AS BOOKINGRETURNDATE,
            bookings.of_departuretime AS OFDEPARTTIME,
            bookings.of_arrivaltime AS OFARRIVALTIME,
            bookings.rf_departuretime AS RFDEPARTTIME,
            bookings.rf_arrivaltime AS RFARRIVALTIME,
            bookings.total_cost AS BOOKINGCOST,
            bookings.total_profit AS BOOKINGPROFIT,
            bookings.total_price AS BOOKINGPRICE,
            bookings.status AS BOOKINGSTATUS,
            traveller_details.name AS TRAVELLLERNAME');
        //$this->db->from('bookings');
		$this->db->join('payments'							,'payments.booking_id=bookings.id'						,'LEFT');
		$this->db->join('payment_type'						,'payment_type.id=payments.payment_type_id'				,'LEFT');
        $this->db->join('packages'                          ,'packages.id = bookings.package_id'                    ,'LEFT');
        $this->db->join('company_designations'              ,'company_designations.user_id = packages.provider_id'  ,'LEFT');
        $this->db->join('companies'                         ,'companies.id = company_designations.company_id'       ,'LEFT');
        $this->db->join('company_designations AS comp_des'  ,'comp_des.user_id = bookings.user_id'                  ,'LEFT');
        $this->db->join('companies AS comp'                 ,'comp.id = comp_des.company_id'                        ,'LEFT');
        $this->db->join('traveller_details'                 ,'traveller_details.booking_id = bookings.id'           ,'LEFT');

	    $this->db->where('bookings.id IN (SELECT MAX(bookings.id) FROM bookings GROUP BY bookings.code)');

		if($where_array){
			$this->db->where($where_array);
		}
		
		if($date_range){
			$this->db->where("bookings.departure_date between {$date_range}");
		}

	    $this->db->where('payments.date is NULL');
	    $this->db->group_by('traveller_details.booking_id');
	    
	    if($limit){
	    	$q = $this->db->get("bookings",$limit, $start);
	    }else{
	    	$q = $this->db->get("bookings");
	    }
	    return $q->result();
	}

	public function getAllPaymentRecords_collection_data($start=false, $limit=false,$date_range= false, $search_term = false, $where_array = false, $pid) {
		$this->db->select('bookings.id AS BOOKID,
            comp.name AS COMPANYNAME,
            packages.title AS PACKAGENAME,
            companies.name AS COMPANYNAMEP,
            bookings.code AS BOOKINGCODE,
            bookings.departure_date AS BOOKINGDEPARTUREDATE,
            bookings.return_date AS BOOKINGRETURNDATE,
            bookings.of_departuretime AS OFDEPARTTIME,
            bookings.of_arrivaltime AS OFARRIVALTIME,
            bookings.rf_departuretime AS RFDEPARTTIME,
            bookings.rf_arrivaltime AS RFARRIVALTIME,
            bookings.total_cost AS BOOKINGCOST,
            bookings.total_profit AS BOOKINGPROFIT,
            bookings.total_price AS BOOKINGPRICE,
            bookings.status AS BOOKINGSTATUS,
            traveller_details.name AS TRAVELLLERNAME,
            payments.status AS PSTAT,
            payments.date AS PDATE');
        //$this->db->from('bookings');
		$this->db->join('payments'							,'payments.booking_id=bookings.id'						,'LEFT');
		//$this->db->join('payment_type'						,'payment_type.id=payments.payment_type_id'				,'LEFT');
        $this->db->join('packages'                          ,'packages.id = bookings.package_id'                    ,'LEFT');
        $this->db->join('company_designations'              ,'company_designations.user_id = packages.provider_id'  ,'LEFT');
        $this->db->join('companies'                         ,'companies.id = company_designations.company_id'       ,'LEFT');
        $this->db->join('company_designations AS comp_des'  ,'comp_des.user_id = bookings.user_id'                  ,'LEFT');
        $this->db->join('companies AS comp'                 ,'comp.id = comp_des.company_id'                        ,'LEFT');
        $this->db->join('traveller_details'                 ,'traveller_details.booking_id = bookings.id'           ,'LEFT');


		if($where_array){
			$this->db->where($where_array);
		}
		
		if($date_range){
			$this->db->where("bookings.departure_date between {$date_range}");
			//dump($date_range);
		}

	    //$this->db->where('bookings.id IN (SELECT MAX(bookings.id) FROM bookings GROUP BY bookings.code)');
	    //$this->db->where('payments.status = "collected"');
	    $this->db->where('payments.payment_type_id', $pid);
	    $this->db->group_by('traveller_details.booking_id');
	    
	    if($limit){
	    	$q = $this->db->get("bookings",$limit, $start);
	    }else{
	    	$q = $this->db->get("bookings");
	    }

	    //dump($q->result());
	    return $q->result();
	}

	public function getAllPaymentRecords_agent($userid) {
		$this->db->select('bookings.id AS BOOKID,
			comp.name AS COMPANYNAME,
			packages.title AS PACKAGENAME,
			companies.name AS COMPANYNAMEP,
			bookings.code AS BOOKINGCODE,
			bookings.departure_date AS BOOKINGDEPARTUREDATE,
			bookings.return_date AS BOOKINGRETURNDATE,
			bookings.of_departuretime AS OFDEPARTTIME,
			bookings.of_arrivaltime AS OFARRIVALTIME,
			bookings.rf_departuretime AS RFDEPARTTIME,
			bookings.rf_arrivaltime AS RFARRIVALTIME,
			bookings.total_cost AS BOOKINGCOST,
			bookings.total_profit AS BOOKINGPROFIT,
			bookings.total_price AS BOOKINGPRICE,
			bookings.status AS BOOKINGSTATUS,
			traveller_details.name AS TRAVELLLERNAME,
			payments.status AS PSTAT');
		$this->db->from('bookings');
		$this->db->join('packages'							,'packages.id = bookings.package_id'					,'LEFT');
		$this->db->join('company_designations'				,'company_designations.user_id = packages.provider_id'	,'LEFT');
		$this->db->join('companies'							,'companies.id = company_designations.company_id'		,'LEFT');
		$this->db->join('company_designations AS comp_des'	,'comp_des.user_id = bookings.user_id'					,'LEFT');
		$this->db->join('companies AS comp'					,'comp.id = comp_des.company_id'						,'LEFT');
		$this->db->join('traveller_details'					,'traveller_details.booking_id = bookings.id'			,'LEFT');
		$this->db->join('payments'							,'payments.booking_id = bookings.id'					,'LEFT');
		$this->db->join('payment_type'						,'payment_type.id = payments.payment_type_id'			,'LEFT');

		$this->db->where('bookings.id IN (SELECT MAX(bookings.id) FROM bookings GROUP BY bookings.code)');
	    $this->db->where('bookings.user_id ="'.$userid.'"');
	    $this->db->where('payments.status is NULL');
		$this->db->group_by('traveller_details.booking_id');

		$q = $this->db->get();
	    return $q->result();
	}
	
	public function getAllPaymentRecords_provider($userid) {
		$this->db->select('bookings.id AS BOOKID,
			comp.name AS COMPANYNAME,
			packages.title AS PACKAGENAME,
			companies.name AS COMPANYNAMEP,
			bookings.code AS BOOKINGCODE,
			bookings.departure_date AS BOOKINGDEPARTUREDATE,
			bookings.return_date AS BOOKINGRETURNDATE,
			bookings.of_departuretime AS OFDEPARTTIME,
			bookings.of_arrivaltime AS OFARRIVALTIME,
			bookings.rf_departuretime AS RFDEPARTTIME,
			bookings.rf_arrivaltime AS RFARRIVALTIME,
			bookings.total_cost AS BOOKINGCOST,
			bookings.total_profit AS BOOKINGPROFIT,
			bookings.total_price AS BOOKINGPRICE,
			bookings.status AS BOOKINGSTATUS,
			traveller_details.name AS TRAVELLLERNAME,
			payments.status AS PSTAT');
		$this->db->from('bookings');
		$this->db->join('packages'							,'packages.id = bookings.package_id'					,'LEFT');
		$this->db->join('company_designations'				,'company_designations.user_id = packages.provider_id'	,'LEFT');
		$this->db->join('companies'							,'companies.id = company_designations.company_id'		,'LEFT');
		$this->db->join('company_designations AS comp_des'	,'comp_des.user_id = bookings.user_id'					,'LEFT');
		$this->db->join('companies AS comp'					,'comp.id = comp_des.company_id'						,'LEFT');
		$this->db->join('traveller_details'					,'traveller_details.booking_id = bookings.id'			,'LEFT');
		$this->db->join('payments'							,'payments.booking_id = bookings.id'					,'LEFT');
		//$this->db->join('payment_type'						,'payment_type.id = payments.payment_type_id'			,'LEFT');

		$this->db->where('bookings.id IN (SELECT MAX(bookings.id) FROM bookings GROUP BY bookings.code)');
	    $this->db->where('packages.provider_id ="'.$userid.'"');
	    $this->db->where('payments.status is NULL');
		$this->db->group_by('traveller_details.booking_id');

		$q = $this->db->get();
	    return $q->result();
	}
	
	public function getAllBookingRecords_page($start = false, $limit = false, $search_array = false, $filter = false) {
		//$tbl_name = 'bookings';
		$this->db->select('bookings.id AS BOOKID,
			comp.name AS COMPANYNAME,
			packages.title AS PACKAGENAME,
			companies.name AS COMPANYNAMEP,
			bookings.code AS BOOKINGCODE,
			bookings.departure_date AS BOOKINGDEPARTUREDATE,
			bookings.return_date AS BOOKINGRETURNDATE,
			bookings.of_departuretime AS OFDEPARTTIME,
			bookings.of_arrivaltime AS OFARRIVALTIME,
			bookings.rf_departuretime AS RFDEPARTTIME,
			bookings.rf_arrivaltime AS RFARRIVALTIME,
			bookings.total_cost AS BOOKINGCOST,
			bookings.total_profit AS BOOKINGPROFIT,
			bookings.total_price AS BOOKINGPRICE,
			bookings.total_discount AS BOOKINGDISCOUNT,
			bookings.total_additional AS BOOKINGADDITIONAL,
			bookings.status AS BOOKINGSTATUS,
			traveller_details.name AS TRAVELLLERNAME,
			payments.status AS PSTAT,
            count(bookings.code) AS PAX');
		//$this->db->from('bookings');
		$this->db->join('packages'							,'packages.id = bookings.package_id'					,'LEFT');
		$this->db->join('company_designations'				,'company_designations.user_id = packages.provider_id'	,'LEFT');
		$this->db->join('companies'							,'companies.id = company_designations.company_id'		,'LEFT');
		$this->db->join('company_designations AS comp_des'	,'comp_des.user_id = bookings.user_id'					,'LEFT');
		$this->db->join('companies AS comp'					,'comp.id = comp_des.company_id'						,'LEFT');
		$this->db->join('traveller_details'					,'traveller_details.booking_id = bookings.id'			,'LEFT');
		$this->db->join('payments'							,'payments.booking_id = bookings.id'					,'LEFT');
		//$this->db->join('payment_type'						,'payment_type.id = payments.payment_type_id'			,'LEFT');

		if($filter){
			$this->db->like($filter);
		}
		if($search_array){
			$this->db->where($search_array);
		}

		$this->db->where('bookings.id IN (SELECT MAX(bookings.id) FROM bookings GROUP BY bookings.code)');
		$this->db->group_by('traveller_details.booking_id');
		$this->db->order_by('bookings.code','DESC');

		if($limit){
			$q = $this->db->get('bookings',$limit,$start);
		}else{
			$q = $this->db->get("bookings");
		}
		
	    return $q->result();
	}

	public function getAllBookingRecords_page_report($start = FALSE, $per_page = FALSE, $search_array = false, $filter_arr = false) {
		$this->db->select('bookings.id AS BOOKID,
            comp.name AS COMPANYNAME,
            packages.title AS PACKAGENAME,
            companies.name AS COMPANYNAMEP,
            bookings.code AS BOOKINGCODE,
            bookings.departure_date AS BOOKINGDEPARTUREDATE,
            bookings.return_date AS BOOKINGRETURNDATE,
            bookings.of_departuretime AS OFDEPARTTIME,
            bookings.of_arrivaltime AS OFARRIVALTIME,
            bookings.rf_departuretime AS RFDEPARTTIME,
            bookings.rf_arrivaltime AS RFARRIVALTIME,
            bookings.total_cost AS BOOKINGCOST,
            bookings.total_profit AS BOOKINGPROFIT,
            bookings.total_price AS BOOKINGPRICE,
            bookings.total_discount AS BOOKINGDISCOUNT,
            bookings.status AS BOOKINGSTATUS,
            traveller_details.name AS TRAVELLLERNAME,
            payments.status AS PSTAT,
            count(bookings.code) AS PAX');
        //$this->db->from('bookings');
        $this->db->join('packages'                          ,'packages.id = bookings.package_id'                    ,'LEFT');
        $this->db->join('company_designations'              ,'company_designations.user_id = packages.provider_id'  ,'LEFT');
        $this->db->join('companies'                         ,'companies.id = company_designations.company_id'       ,'LEFT');
        $this->db->join('company_designations AS comp_des'  ,'comp_des.user_id = bookings.user_id'                  ,'LEFT');
        $this->db->join('companies AS comp'                 ,'comp.id = comp_des.company_id'                        ,'LEFT');
        $this->db->join('traveller_details'                 ,'traveller_details.booking_id = bookings.id'           ,'LEFT');
        $this->db->join('payments'                          ,'payments.booking_id=bookings.id'                      ,'LEFT');
        $this->db->join('payment_type'                      ,'payment_type.id=payments.payment_type_id'             ,'LEFT');


		if($search_array){
			$this->db->like($search_array);
		}
		if ($filter_arr) {
			$this->db->where($filter_arr);
		}

	    $this->db->where('bookings.id IN (SELECT MAX(bookings.id) FROM bookings GROUP BY bookings.code)');
	    $this->db->group_by('traveller_details.booking_id');
		$this->db->order_by('bookings.code','ASC');
	    
	    if($per_page){
	    	$q = $this->db->get('bookings',$per_page,$start);
	    }else{
	    	$q = $this->db->get('bookings');
	    }
	    return $q->result();
	}

	public function getAllBookingRecords_page_pending() {
		//$tbl_name = 'bookings';
		$this->db->select('bookings.id AS BOOKID,
			comp.name AS COMPANYNAME,
			packages.title AS PACKAGENAME,
			companies.name AS COMPANYNAMEP,
			bookings.code AS BOOKINGCODE,
			bookings.departure_date AS BOOKINGDEPARTUREDATE,
			bookings.return_date AS BOOKINGRETURNDATE,
			bookings.of_departuretime AS OFDEPARTTIME,
			bookings.of_arrivaltime AS OFARRIVALTIME,
			bookings.rf_departuretime AS RFDEPARTTIME,
			bookings.rf_arrivaltime AS RFARRIVALTIME,
			bookings.total_cost AS BOOKINGCOST,
			bookings.total_profit AS BOOKINGPROFIT,
			bookings.total_price AS BOOKINGPRICE,
			bookings.status AS BOOKINGSTATUS,
			traveller_details.name AS TRAVELLLERNAME,
			payments.status AS PSTAT,
            count(bookings.code) AS PAX');
		//$this->db->from('bookings');
		$this->db->join('packages'							,'packages.id = bookings.package_id'					,'LEFT');
		$this->db->join('company_designations'				,'company_designations.user_id = packages.provider_id'	,'LEFT');
		$this->db->join('companies'							,'companies.id = company_designations.company_id'		,'LEFT');
		$this->db->join('company_designations AS comp_des'	,'comp_des.user_id = bookings.user_id'					,'LEFT');
		$this->db->join('companies AS comp'					,'comp.id = comp_des.company_id'						,'LEFT');
		$this->db->join('traveller_details'					,'traveller_details.booking_id = bookings.id'			,'LEFT');
		$this->db->join('payments'							,'payments.booking_id = bookings.id'					,'LEFT');
		//$this->db->join('payment_type'						,'payment_type.id = payments.payment_type_id'			,'LEFT');
		$this->db->where('bookings.status','pending');
		$this->db->where('bookings.id IN (SELECT MAX(bookings.id) FROM bookings GROUP BY bookings.code)');
		$this->db->group_by('traveller_details.booking_id');
		$this->db->order_by('bookings.code','DESC');
		$q = $this->db->get("bookings");
	    return $q->result();
	}

	public function getAllBookingRecords_pending() {
		$tbl_name = 'bookings';
		$per_page = 10;
		$num_links = 5;
		$chckStatus = "pending";
		$this->my_pagination($tbl_name,$per_page,$num_links,$chckStatus);

		$this->db->select('bookings.id AS BOOKID,
			comp.name AS COMPANYNAME,
			packages.title AS PACKAGENAME,
			companies.name AS COMPANYNAMEP,
			bookings.code AS BOOKINGCODE,
			bookings.departure_date AS BOOKINGDEPARTUREDATE,
			bookings.return_date AS BOOKINGRETURNDATE,
			bookings.of_departuretime AS OFDEPARTTIME,
			bookings.of_arrivaltime AS OFARRIVALTIME,
			bookings.rf_departuretime AS RFDEPARTTIME,
			bookings.rf_arrivaltime AS RFARRIVALTIME,
			bookings.total_cost,
			bookings.total_profit,
			bookings.total_price,
			bookings.status AS BOOKINGSTATUS,
			traveller_details.name AS TRAVELLLERNAME');
		//$this->db->from('bookings');
		$this->db->join('packages'							,'packages.id = bookings.package_id'					,'LEFT');
		$this->db->join('company_designations'				,'company_designations.user_id = packages.provider_id'	,'LEFT');
		$this->db->join('companies'							,'companies.id = company_designations.company_id'		,'LEFT');
		$this->db->join('company_designations AS comp_des'	,'comp_des.user_id = bookings.user_id'					,'LEFT');
		$this->db->join('companies AS comp'					,'comp.id = comp_des.company_id'						,'LEFT');
		$this->db->join('traveller_details'					,'traveller_details.booking_id = bookings.id'			,'LEFT');



	    $this->db->where('bookings.id IN (SELECT MAX(bookings.id) FROM bookings GROUP BY bookings.code)');
	   // $this->db->where('bookings.status ="pending"');
	    $this->db->where('bookings.status',$chckStatus);
	    $this->db->group_by('traveller_details.booking_id');
		$q = $this->db->get('bookings',$per_page,$this->uri->segment(3));
	    return $q->result();
	}

	/*
	*
	*End getAllBookingRecords function into database globalexplorer_db
	*
	*/


	public function getAllBookingRecordsAgent_page($userid, $start = false , $limit = false, $search_array = false, $filter = false) {
		$this->db->select('bookings.id AS BOOKID,
			comp.name AS COMPANYNAME,
			packages.title AS PACKAGENAME,
			companies.name AS COMPANYNAMEP,
			bookings.code AS BOOKINGCODE,
			bookings.departure_date AS BOOKINGDEPARTUREDATE,
			bookings.return_date AS BOOKINGRETURNDATE,
			bookings.of_departuretime AS OFDEPARTTIME,
			bookings.of_arrivaltime AS OFARRIVALTIME,
			bookings.rf_departuretime AS RFDEPARTTIME,
			bookings.rf_arrivaltime AS RFARRIVALTIME,
			bookings.total_cost AS BOOKINGCOST,
			bookings.total_profit AS BOOKINGPROFIT,
			bookings.total_price AS BOOKINGPRICE,
			bookings.total_discount AS BOOKINGDISCOUNT,
			bookings.total_additional AS BOOKINGADDITIONAL,
			bookings.status AS BOOKINGSTATUS,
			traveller_details.name AS TRAVELLLERNAME,
			payments.status AS PSTAT,
            count(bookings.code) AS PAX');
		//$this->db->from('bookings');
		$this->db->join('packages'							,'packages.id = bookings.package_id'					,'LEFT');
		$this->db->join('company_designations'				,'company_designations.user_id = packages.provider_id'	,'LEFT');
		$this->db->join('companies'							,'companies.id = company_designations.company_id'		,'LEFT');
		$this->db->join('company_designations AS comp_des'	,'comp_des.user_id = bookings.user_id'					,'LEFT');
		$this->db->join('companies AS comp'					,'comp.id = comp_des.company_id'						,'LEFT');
		$this->db->join('traveller_details'					,'traveller_details.booking_id = bookings.id'			,'LEFT');
		$this->db->join('payments'							,'payments.booking_id = bookings.id'					,'LEFT');
		$this->db->join('payment_type'						,'payment_type.id = payments.payment_type_id'			,'LEFT');

	    if($filter){
	    	$this->db->like($filter);
	    }
	    if($search_array){
	    	$this->db->where($search_array);
	    }

	    $this->db->where('bookings.id IN (SELECT MAX(bookings.id) FROM bookings GROUP BY bookings.code)');
	    $this->db->where('bookings.user_id ="'.$userid.'"');
		$this->db->group_by('traveller_details.booking_id');
		$this->db->order_by('bookings.code','DESC');

	    if($limit){
	    	$q = $this->db->get('bookings',$limit, $start);
	    }else{
	    	$q = $this->db->get('bookings');
	    }
	    return $q->result();
	}

	/*
	*
	*End getAllBookingRecordsAgent function into database globalexplorer_db
	*
	*/

	/*
	*
	*Start getAllBookingRecordsProvider_page function into database globalexplorer_db
	*
	*/

	public function getAllBookingRecordsProvider_page($userid, $start = false, $limit = false, $search_array = false, $filter = false) {
	    $this->db->select('bookings.id AS BOOKID,
			comp.name AS COMPANYNAME,
			packages.title AS PACKAGENAME,
			companies.name AS COMPANYNAMEP,
			bookings.code AS BOOKINGCODE,
			bookings.departure_date AS BOOKINGDEPARTUREDATE,
			bookings.return_date AS BOOKINGRETURNDATE,
			bookings.of_departuretime AS OFDEPARTTIME,
			bookings.of_arrivaltime AS OFARRIVALTIME,
			bookings.rf_departuretime AS RFDEPARTTIME,
			bookings.rf_arrivaltime AS RFARRIVALTIME,
			bookings.total_cost AS BOOKINGCOST,
			bookings.total_profit AS BOOKINGPROFIT,
			bookings.total_price AS BOOKINGPRICE,
			bookings.total_discount AS BOOKINGDISCOUNT,
			bookings.total_additional AS BOOKINGADDITIONAL,
			bookings.status AS BOOKINGSTATUS,
			traveller_details.name AS TRAVELLLERNAME,
			payments.status AS PSTAT,
            count(bookings.code) AS PAX');
		//$this->db->from('bookings');
		$this->db->join('packages'							,'packages.id = bookings.package_id'					,'LEFT');
		$this->db->join('company_designations'				,'company_designations.user_id = packages.provider_id'	,'LEFT');
		$this->db->join('companies'							,'companies.id = company_designations.company_id'		,'LEFT');
		$this->db->join('company_designations AS comp_des'	,'comp_des.user_id = bookings.user_id'					,'LEFT');
		$this->db->join('companies AS comp'					,'comp.id = comp_des.company_id'						,'LEFT');
		$this->db->join('traveller_details'					,'traveller_details.booking_id = bookings.id'			,'LEFT');
		$this->db->join('payments'							,'payments.booking_id = bookings.id'					,'LEFT');
		//$this->db->join('payment_type'						,'payment_type.id = payments.payment_type_id'			,'LEFT');

		if($filter){
			$this->db->like($filter);
		}
		if($search_array){
			$this->db->where($search_array);
		}

		$this->db->where('bookings.id IN (SELECT MAX(bookings.id) FROM bookings GROUP BY bookings.code)');
	    $this->db->where('packages.provider_id ="'.$userid.'"');
		$this->db->group_by('traveller_details.booking_id');
		$this->db->order_by('bookings.code','DESC');

		if($limit){
			$q = $this->db->get('bookings',$limit,$start);
		}else{
			$q = $this->db->get("bookings");
		}

	    return $q->result();
	}
	/*
	*
	*End getAllBookingRecordsProvider_page function into database globalexplorer_db
	*
	*/

	/*
	*
	*Start getAllBookinghotelRecord function into database globalexplorer_db
	*
	*/
	public function getAllBookinghotelRecord($booked_id,$search_array = false) {
	
		$this->db->select('hotels.name AS hotels_name');
		$this->db->join('booked_hotels','booked_hotels.booking_id = bookings.id','LEFT');
		$this->db->join('package_hotels','package_hotels.id = booked_hotels.package_hotel_id','LEFT');
		$this->db->join('hotels','hotels.id=package_hotels.hotel_id','LEFT');
	    $this->db->where('bookings.id',$booked_id);
	    //$this->db->where('bookings.status','pending');

	    #check if the search array is not false
	    if($search_array){
	    	$this->db->where($search_array);
	    }

		$q = $this->db->get('bookings');
	    return $q->result();
	}

	/*
	*
	*End getAllBookingRecordsProvider function into database globalexplorer_db
	*
	*/


	/*
	*
	*Start getAllBookingRecords function into database globalexplorer_db
	*
	*/
	public function getAllBookingRecords_filter($data) {
		$tbl_name = 'bookings';
		$per_page = 10;
		$num_links = 5;
		$this->my_pagination($tbl_name,$per_page,$num_links);
		$this->db->select(' bookings.id AS BOOKID,
							bookings.package_id AS PACKAGEID,
							bookings.rf_arrivaltime  AS ARRIVALTIME,
							bookings.of_arrivaltime AS  RETURNTIME,
							bookings.code AS BOOKINGCODE, 
							bookings.departure_date AS BOOKINGDEPARTUREDATE, 
							bookings.return_date AS BOOKINGRETURNDATE, 
							bookings.total_cost AS BOOKINGCOST, 
							bookings.total_profit AS BOOKINGPROFIT, 
							bookings.total_price AS BOOKINGPRICE, 
							bookings.status AS BOOKINGSTATUS,
							packages.title AS PACKAGENAME,
							packages.provider_id AS PROVIDERID,
							users.firstname AS PROVIDERFNAME,
							users.lastname AS PROVIDERLNAME,
							package_hotels.package_id AS PACKAGEID,
							hotels.name AS BOOKINGHOTELNAME,
							traveller_details.booking_id AS TRAVELLLERID,
							traveller_details.name AS TRAVELLLERNAME,
							uagent.firstname AS AGENTFNAME,
							uagent.lastname AS AGENTLNAME');
		//$this->db->from('bookings');
		$this->db->join('traveller_details','traveller_details.booking_id=bookings.id','LEFT');
		$this->db->join('packages','packages.id=bookings.package_id','LEFT');
		$this->db->join('package_hotels','package_hotels.package_id=packages.id','LEFT');
		$this->db->join('hotels','hotels.id=bookings.hotel_id','LEFT');
		$this->db->join('users','users.id=packages.provider_id','LEFT');
		$this->db->join('users AS uagent','uagent.id=bookings.user_id','LEFT');
		if($data['code'] != ''){
			
			$this->db->like('bookings.code',$data['code']);
		}
		if($data['status'] != ''){
			
			$this->db->like('bookings.status',$data['status']);
		}
		if($data['origin'] != ''){
			
			$this->db->like('bookings.departure_date',date('Y-m',strtotime($data['origin'])));
		}
		if($data['return'] != ''){
			
			$this->db->like('bookings.return_date',date('Y-m',strtotime($data['return'])));
		}
	    $this->db->group_by('bookings.id');
		$q = $this->db->get('bookings',$per_page,$this->uri->segment(3));
	    return $q->result();
	}
	/*
	*
	*End getAllBookingRecords function into database globalexplorer_db
	*
	*/
	/*
	*
	*Start getAllBookingRecords function into database globalexplorer_db
	*
	*/
	public function getAllBookingRecords_report($data) {
		$this->db->select(' bookings.id AS BOOKID,
							bookings.package_id AS PACKAGEID,
							bookings.rf_arrivaltime  AS ARRIVALTIME,
							bookings.of_arrivaltime AS  RETURNTIME,
							bookings.code AS BOOKINGCODE, 
							bookings.departure_date AS BOOKINGDEPARTUREDATE, 
							bookings.return_date AS BOOKINGRETURNDATE, 
							bookings.total_cost AS BOOKINGCOST, 
							bookings.total_profit AS BOOKINGPROFIT, 
							bookings.total_price AS BOOKINGPRICE, 
							bookings.status AS BOOKINGSTATUS,
							packages.title AS PACKAGENAME,
							packages.provider_id AS PROVIDERID,
							users.firstname AS PROVIDERFNAME,
							users.lastname AS PROVIDERLNAME,
							package_hotels.package_id AS PACKAGEID,
							hotels.name AS BOOKINGHOTELNAME,
							traveller_details.booking_id AS TRAVELLLERID,
							traveller_details.name AS TRAVELLLERNAME,
							uagent.firstname AS AGENTFNAME,
							uagent.lastname AS AGENTLNAME,
							uagent.id');
		$this->db->from('bookings');
		$this->db->join('traveller_details','traveller_details.booking_id=bookings.id','LEFT');
		$this->db->join('packages','packages.id=bookings.package_id','LEFT');
		$this->db->join('users','users.id=packages.provider_id','LEFT');
		$this->db->join('package_hotels','package_hotels.package_id=packages.id','LEFT');
		$this->db->join('hotels','hotels.id=bookings.hotel_id','LEFT');
		$this->db->join('users AS uagent','uagent.id=bookings.user_id','LEFT');
		if($data['agent_id'] != ''){
			$this->db->like('uagent.id',$data['agent_id']);
		}
		if($data['provider_id'] != ''){
			
			$this->db->like('packages.provider_id',$data['provider_id']);
		}
		if($data['depart'] != ''){
			
			$this->db->like('bookings.departure_date',date('Y-m',strtotime($data['depart'])));
		}
		if($data['return'] != ''){
			
			$this->db->like('bookings.return_date',date('Y-m',strtotime($data['return'])));
		}
		if($data['book_status'] != ''){
			
			$this->db->like('bookings.status',$data['book_status']);
		}
	    $this->db->group_by('bookings.id');
		$q = $this->db->get();
	    return $q->result();
	}
	/*
	*
	*End getAllBookingRecords function into database globalexplorer_db
	*
	*/
	public function filterReport($post_data=array(),$limit=0,$offset=0){
		$this->db->select(' bookings.id AS BOOKID,
							bookings.package_id AS PACKAGEID,
							bookings.rf_arrivaltime  AS ARRIVALTIME,
							bookings.of_arrivaltime AS  RETURNTIME,
							bookings.code AS BOOKINGCODE, 
							bookings.departure_date AS BOOKINGDEPARTUREDATE, 
							bookings.return_date AS BOOKINGRETURNDATE, 
							bookings.total_cost AS BOOKINGCOST, 
							bookings.total_profit AS BOOKINGPROFIT, 
							bookings.total_price AS BOOKINGPRICE, 
							bookings.status AS BOOKINGSTATUS,
							packages.title AS PACKAGENAME,
							packages.provider_id AS PROVIDERID,
							users.firstname AS PROVIDERFNAME,
							users.lastname AS PROVIDERLNAME,
							package_hotels.package_id AS PACKAGEID,
							hotels.name AS BOOKINGHOTELNAME,
							traveller_details.booking_id AS TRAVELLLERID,
							traveller_details.name AS TRAVELLLERNAME,
							uagent.firstname AS AGENTFNAME,
							uagent.lastname AS AGENTLNAME,
							uagent.id');
		$this->db->from("bookings");
		$this->db->join('traveller_details','traveller_details.booking_id=bookings.id','LEFT');
		$this->db->join('packages','packages.id=bookings.package_id','LEFT');
		$this->db->join('users','users.id=packages.provider_id','LEFT');
		$this->db->join('package_hotels','package_hotels.package_id=packages.id','LEFT');
		$this->db->join('hotels','hotels.id=bookings.hotel_id','LEFT');
		$this->db->join('users AS uagent','uagent.id=bookings.user_id','LEFT');
		$this->db->group_by('bookings.id');
		if($limit>0) 	$this->db->limit($limit);
		if($offset>0) 	$this->db->offset($offset);
	 	
	 	foreach($post_data as $key => $post){
	 		if (trim($post)!=''){
	 			if ($key == 'agent_id'){
					$this->db->like('uagent.id',$post);
	 			}
	 			if ($key == 'provider_id'){
					$this->db->like('packages.provider_id',$post);
	 			}
	 			if ($key == 'depart')
	 				$this->db->where(array('bookings.departure_date <=' => $post));
	 			if ($key == 'return')
	 				$this->db->where(array('bookings.return_date <=' => $post));
	 			if ($key == 'book_status') {
	 				$this->db->like('bookings.status',$post);
	 			}
	 		}
	 	}

		return $this->db->get();
	}

	public function getReport($where=array(), $limit=0, $offset=0){
		$this->db->select(' bookings.id AS BOOKID,
							bookings.package_id AS PACKAGEID,
							bookings.rf_arrivaltime  AS ARRIVALTIME,
							bookings.of_arrivaltime AS  RETURNTIME,
							bookings.code AS BOOKINGCODE, 
							bookings.departure_date AS BOOKINGDEPARTUREDATE, 
							bookings.return_date AS BOOKINGRETURNDATE, 
							bookings.total_cost AS BOOKINGCOST, 
							bookings.total_profit AS BOOKINGPROFIT, 
							bookings.total_price AS BOOKINGPRICE, 
							bookings.status AS BOOKINGSTATUS,
							packages.title AS PACKAGENAME,
							packages.provider_id AS PROVIDERID,
							users.firstname AS PROVIDERFNAME,
							users.lastname AS PROVIDERLNAME,
							package_hotels.package_id AS PACKAGEID,
							hotels.name AS BOOKINGHOTELNAME,
							traveller_details.booking_id AS TRAVELLLERID,
							traveller_details.name AS TRAVELLLERNAME,
							uagent.firstname AS AGENTFNAME,
							uagent.lastname AS AGENTLNAME,
							uagent.id');
		$this->db->from("bookings");
		$this->db->join('traveller_details','traveller_details.booking_id=bookings.id','LEFT');
		$this->db->join('packages','packages.id=bookings.package_id','LEFT');
		$this->db->join('users','users.id=packages.provider_id','LEFT');
		$this->db->join('package_hotels','package_hotels.package_id=packages.id','LEFT');
		$this->db->join('hotels','hotels.id=bookings.hotel_id','LEFT');
		$this->db->join('users AS uagent','uagent.id=bookings.user_id','LEFT');
		$this->db->group_by('bookings.id');
		if($limit>0) 		$this->db->limit($limit);
		if($offset>0) 		$this->db->offset($offset);
		if(!empty($where)) 	$this->db->where($where);
		return $this->db->get();
	}
	/*
	*
	*Start getAllBookingRecordsAgent function into database globalexplorer_db
	*
	*/
	public function getAllBookingRecordsAgent_filter($data,$userid) {
		$this->db->select('bookings.id AS BOOKID,
						bookings.package_id AS PACKAGEID,
						bookings.rf_arrivaltime  AS ARRIVALTIME,
						bookings.of_arrivaltime AS  RETURNTIME,
						bookings.code AS BOOKINGCODE, 
						bookings.departure_date AS BOOKINGDEPARTUREDATE, 
						bookings.return_date AS BOOKINGRETURNDATE, 
						bookings.total_cost AS BOOKINGCOST, 
						bookings.total_profit AS BOOKINGPROFIT, 
						bookings.total_price AS BOOKINGPRICE, 
						bookings.status AS BOOKINGSTATUS,
						packages.title AS PACKAGENAME,
						packages.provider_id AS PROVIDERID,
						users.firstname AS PROVIDERFNAME,
						users.lastname AS PROVIDERLNAME,
						package_hotels.package_id AS PACKAGEID,
						hotels.name AS BOOKINGHOTELNAME,
						traveller_details.booking_id AS TRAVELLLERID,
						traveller_details.name AS TRAVELLLERNAME,
						uagent.firstname AS AGENTFNAME,
						uagent.lastname AS AGENTLNAME,
						bookings.package_id AS BOKPERID,
						package_permissions.package_id AS PACPERID,
						package_permissions.user_id AS PACPERUSERID,
						userper.firstname AS USERFNAME');
		$this->db->from('bookings');
		$this->db->join('package_permissions','package_permissions.package_id=bookings.package_id','LEFT');
		$this->db->join('users AS userper','userper.id=package_permissions.user_id','LEFT');
		$this->db->join('traveller_details','traveller_details.booking_id=bookings.id','LEFT');
		$this->db->join('packages','packages.id=bookings.package_id','LEFT');
		$this->db->join('package_hotels','package_hotels.package_id=packages.id','LEFT');
		$this->db->join('hotels','hotels.id=bookings.hotel_id','LEFT');
		$this->db->join('users','users.id=packages.provider_id','LEFT');
		$this->db->join('users AS uagent','uagent.id=bookings.user_id','LEFT');
    
	    if($data['code'] != ''){
			$this->db->like('bookings.code',$data['code']);
		}
		if($data['status'] != ''){
			$this->db->like('bookings.status',$data['status']);
		}
		if($data['origin'] != ''){
			$this->db->like('bookings.departure_date',date('Y-m',strtotime($data['origin'])));
		}
		if($data['return'] != ''){
			$this->db->like('bookings.return_date',date('Y-m',strtotime($data['return'])));
		}
	    $this->db->where('package_permissions.user_id ="'.$userid.'"');
	    $this->db->group_by('bookings.id');
		$q = $this->db->get();
	    return $q->result();
	}

	/*
	*
	*End getAllBookingRecordsAgent function into database globalexplorer_db
	*
	*/

	/*
	*
	*Start getAllBookingRecordsProvider function into database globalexplorer_db
	*
	*/

	public function getAllBookingRecordsProvider_filter($data,$userid){
		$this->db->select('bookings.id AS BOOKID,
						bookings.package_id AS PACKAGEID,
						bookings.rf_arrivaltime  AS ARRIVALTIME,
						bookings.of_arrivaltime AS  RETURNTIME,
						bookings.code AS BOOKINGCODE, 
						bookings.departure_date AS BOOKINGDEPARTUREDATE, 
						bookings.return_date AS BOOKINGRETURNDATE, 
						bookings.total_cost AS BOOKINGCOST, 
						bookings.total_profit AS BOOKINGPROFIT, 
						bookings.total_price AS BOOKINGPRICE, 
						bookings.status AS BOOKINGSTATUS,
						packages.title AS PACKAGENAME,
						packages.provider_id AS PROVIDERID,
						users.firstname AS PROVIDERFNAME,
						users.lastname AS PROVIDERLNAME,
						package_hotels.package_id AS PACKAGEID,
						hotels.name AS BOOKINGHOTELNAME,
						traveller_details.booking_id AS TRAVELLLERID,
						traveller_details.name AS TRAVELLLERNAME,
						uagent.firstname AS AGENTFNAME,
						uagent.lastname AS AGENTLNAME,
						packages.provider_id AS PACPROID');
		$this->db->from('bookings');
		$this->db->join('traveller_details','traveller_details.booking_id=bookings.id','LEFT');
		$this->db->join('packages','packages.id=bookings.package_id','LEFT');
		$this->db->join('package_hotels','package_hotels.package_id=packages.id','LEFT');
		$this->db->join('hotels','hotels.id=bookings.hotel_id','LEFT');
		$this->db->join('users','users.id=packages.provider_id','LEFT');
		$this->db->join('users AS uagent','uagent.id=bookings.user_id','LEFT');
		$this->db->join('users AS uprovider','uprovider.id=packages.provider_id','LEFT');
		 if($data['code'] != ''){
			
			$this->db->like('bookings.code',$data['code']);
		}
		if($data['status'] != ''){
			
			$this->db->like('bookings.status',$data['status']);
		}
		if($data['origin'] != ''){
			
			$this->db->like('bookings.departure_date',date('Y-m',strtotime($data['origin'])));
		}
		if($data['return'] != ''){
			
			$this->db->like('bookings.return_date',date('Y-m',strtotime($data['return'])));
		} 
	    $this->db->where('packages.provider_id ="'.$userid.'"');
	    $this->db->group_by('bookings.id');
		$q = $this->db->get();
	    return $q->result();
	}

	/*
	*
	*End getAllBookingRecordsProvider function into database globalexplorer_db
	*
	*/


	/*
	*
	*Start getBybooked function into database globalexplorer_db
	*
	*/
	public function getBybooked($arraydata){
		$this->db->select('bookings.id AS BOOKID,
							bookings.package_id AS PACKAGEID,
							bookings.rf_arrivaltime  AS ARRIVALTIME,
							bookings.of_arrivaltime AS  RETURNTIME,
							bookings.code AS BOOKINGCODE, 
							bookings.departure_date AS BOOKINGDEPARTUREDATE, 
							bookings.return_date AS BOOKINGRETURNDATE, 
							bookings.total_cost AS BOOKINGCOST, 
							bookings.total_profit AS BOOKINGPROFIT, 
							bookings.total_price AS BOOKINGPRICE, 
							bookings.status AS BOOKINGSTATUS,
							packages.title AS PACKAGENAME,
							packages.provider_id AS PROVIDERID,
							users.firstname AS PROVIDERNAME,
							package_hotels.package_id AS PACKAGEID,
							hotels.name AS HOTELSNAME,
							traveller_details.booking_id AS TRAVELLLERID,
							uagent.firstname AS AGENTNAME');
		$this->db->join('users AS uagent','uagent.id=bookings.user_id','LEFT');
		$this->db->join('traveller_details','traveller_details.booking_id=bookings.id','LEFT');
		$this->db->join('packages','packages.id=bookings.package_id','LEFT');
		$this->db->join('package_hotels','package_hotels.package_id=packages.id','LEFT');
		$this->db->join('hotels','hotels.id=package_hotels.hotel_id','LEFT');
		$this->db->join('users','users.id=packages.provider_id','LEFT');

		if($arraydata['code'] != ''){
			
			$this->db->like('bookings.code',$arraydata['code']);
		}
		if($arraydata['status'] != ''){
			
			$this->db->like('bookings.status',$arraydata['status']);
		}
		if($arraydata['origin'] != ''){
			
			$this->db->like('bookings.departure_date',date('Y-m',strtotime($arraydata['origin'])));
		}
		if($arraydata['return'] != ''){
			
			$this->db->like('bookings.return_date',date('Y-m',strtotime($arraydata['return'])));
		}
		$this->db->group_by('bookings.id');
		$q = $this->db->get('bookings');
	    return $q->result();
	}
	/*
	*
	*End getBybooked function into database globalexplorer_db
	*
	*/

	/*
	*
	*Start getBysummary function into database globalexplorer_db
	*
	*/
	public function getBysummary($data){
	  	$this->db->select(' bookings.id AS BOOKID,
	       	bookings.code AS BOOKNO,
	       	bookings.of_departuretime AS BOOKDEPARTDATE,
	       	bookings.of_arrivaltime AS BOOKDEPARTADATEARRIVAL,
	       	bookings.Of_num AS BOOKDEPARTFLIGHTNO,
	       	bookings.total_price AS TOTALPRICE,
	       	bookings.total_discount AS TOTALDISCOUNT,
	       	bookings.total_additional AS TOTALADDITIONAL,
	       	bookings.agent_name AS agentdetailsname,
	       	bookings.agent_ref AS agentdetailsref,
	       	bookings.extended_days AS BOOKEDEXTENDDAYS,
	       	bookings.package_id AS packages_id,
	      	departurefrom.code AS BOOKDEPFROM,
	       	departureto.code AS BOOKDEPTO,
	       	bookings.rf_departuretime AS BOOKRETURNDATE,
	       	bookings.rf_arrivaltime AS BOOKRETURNADATEARRIVAL,
	       	bookings.rf_num AS BOOKRETURNFLIGHTNO,
	       	returnfrom.code AS BOOKRETFROM,
	       	returnto.code AS BOOKRETTO,
	       	bookings.status AS BOOKSTATUS,
	       	bookings.rf_remarks AS BOOKREMARKS,      
	       	bookings.date_booked AS BOOKDATE,
	       	packages.days AS PACNIGHTS,
	       	packages.code AS PACTOURCODE,
	       	packages.title AS PACTOURTITLE,
	       	traveller_details.name AS TRAVELNAME,
	       	traveller_details.type AS TRAVELTYPE,
	       	booked_hotels.checkin_date AS PACKCHECKIN,
	       	booked_hotels.checkout_date AS PACKCHECKOUT,
	       	booked_hotels.remarks AS PACKREMARKS,
	       	hotels.name AS PACKHOTELNAME,
	       	exHotels.name AS PACKEXHOTELNAME,
	       	users.firstname AS AGENTFNAME,
	       	users.lastname AS AGENTLNAME,
	       	users.email AS AGENTEMAIL,
	       	users.phone AS AGENTPHONE,
	       	companies.name AS AGENTCOMPANYNAME,
	       	PROVIDERUSER.firstname AS PROVIDERFNAME,
	       	PROVIDERUSER.lastname AS PROVIDERLNAME,
	       	PROVIDERUSER.email AS PROVIDEREMAIL,
	       	PROVIDERUSER.phone AS PROVIDERPHONE');
	  	$this->db->join('packages','packages.id=bookings.package_id','LEFT');
	 	$this->db->join('airportcodes AS returnfrom','returnfrom.id=bookings.rf_from','LEFT');
	  	$this->db->join('airportcodes AS returnto','returnto.id=bookings.rf_to','LEFT');
	  	$this->db->join('airportcodes AS departurefrom','departurefrom.id=bookings.of_from','LEFT');
	  	$this->db->join('airportcodes AS departureto','departureto.id=bookings.of_to','LEFT');
	  	$this->db->join('traveller_details','traveller_details.booking_id=bookings.id','LEFT');
	  	$this->db->join('booked_hotels','booked_hotels.booking_id=bookings.id','LEFT');
	  	$this->db->join('package_hotels','package_hotels.id=booked_hotels.package_hotel_id','LEFT');
	  	$this->db->join('hotels','hotels.id=bookings.hotel_id','LEFT');
	  	$this->db->join('package_hotels AS exPACKHotels','exPACKHotels.id=bookings.exhotel_id','LEFT');
	  	$this->db->join('hotels AS exHotels','exHotels.id=exPACKHotels.hotel_id','LEFT');
	  	$this->db->join('users','users.id=bookings.user_id','LEFT');
	  	$this->db->join('company_designations','company_designations.user_id=users.id','LEFT');
	  	$this->db->join('companies','companies.id=company_designations.company_id','LEFT');
	  	$this->db->join('packages AS PROVIDERDETAILS','PROVIDERDETAILS.provider_id=bookings.package_id','LEFT');
	  	$this->db->join('users AS PROVIDERUSER','PROVIDERUSER.id=bookings.package_id','LEFT');
	  	$this->db->join('packages AS PROVIDERPAC','PROVIDERPAC.id=bookings.package_id','LEFT');
	  	$this->db->where('bookings.id',$data);
	  	$q = $this->db->get('bookings');
     	return $q->result();
 	}
	/*
	*
	*End getBysummary function into database globalexplorer_db
	*
	*/

	/*
	*
	*Start getBysummary function into database globalexplorer_db
	*
	*/
	public function getByGuest($data){
		$this->db->select('bookings.id,
							traveller_details.name AS name,
							traveller_details.type AS type,
							traveller_details.passport_no,
							traveller_details.date_of_birth,
							count(bookings.code) AS PAX');
		$this->db->join('traveller_details','traveller_details.booking_id=bookings.id','LEFT');
		$this->db->where('bookings.id',$data);
		$q = $this->db->get('bookings');
	    return $q->result();
	}
	/*
	*
	*End getBysummary function into database globalexplorer_db
	*
	*/

	/*
	*
	*Start getBysummary function into database globalexplorer_db
	*
	*/
	public function getByGuestPackage($data){
		$this->db->select('bookings.id,
							traveller_details.name AS name,
							traveller_details.type AS type,
							traveller_details.passport_no,
							traveller_details.date_of_birth');
		$this->db->join('traveller_details','traveller_details.booking_id=bookings.id','LEFT');
		$this->db->where('bookings.id',$data);
		$q = $this->db->get('bookings');
	    return $q->result();
	}
	/*
	*
	*End getBysummary function into database globalexplorer_db
	*
	*/

	/*
	*
	*Start getBysummary function into database globalexplorer_db
	*
	*/
	public function getByGuestVoucher($data){
		$this->db->select('bookings.id,
							traveller_details.name AS name,
							traveller_details.type AS type,
							traveller_details.passport_no,
							traveller_details.date_of_birth');
		$this->db->join('traveller_details','traveller_details.booking_id=bookings.id','LEFT');
		$this->db->where('bookings.id',$data);
		$q = $this->db->get('bookings');
	    return $q->result();
	}
	/*
	*
	*End getBysummary function into database globalexplorer_db
	*
	*/

	/*
	*
	*Start getBysummary function into database globalexplorer_db
	*
	*/
	public function getBysummaryPackage($data){
		$this->db->select(' packages.title AS PACTITLE,
							package_description.description AS PACDES,
							users.phone AS PROVIDERPHONE,
							users.emergency_phone_number AS PROVIDEREMERGENCYNUMBER,
							users.emergency_name AS PROVIDEREMERGENCYNAME,
							package_duration.date_from AS PACDATE,
							package_duration.date_to AS PACTO');
		$this->db->join('packages','packages.id=bookings.package_id','LEFT');
		$this->db->join('users','users.id=packages.provider_id','LEFT');
		$this->db->join('package_description','package_description.package_id=bookings.package_id','LEFT');
		$this->db->join('package_duration','package_duration.package_id=bookings.package_id','LEFT');
		$this->db->where('bookings.id',$data);
		$q = $this->db->get('bookings');
	    return $q->result();
	}
	/*
	*
	*End getBysummary function into database globalexplorer_db
	*
	*/

	/*
	*
	*Start getBysummary function into database globalexplorer_db
	*
	*/
	public function getBysummaryPackageAddHotelBooking($data){
		$this->db->select('*');
		$this->db->where('booked_hotels.booking_id',$data);
		$q = $this->db->get('booked_hotels');
	    return $q->result();
	}
	/*
	*
	*End getBysummary function into database globalexplorer_db
	*
	*/

	/*
	*
	*Start getBysummary function into database globalexplorer_db
	*
	*/
	public function getBysummaryCompany($data){
		$this->db->select(' companies.name AS COMNAME,
							bookings.status AS bookstatus,
							users.lastname AS providerlname,
							users.firstname AS providerfname,
							users.email AS provideremail,
							users.phone AS providerphone');
		$this->db->join('packages','packages.id=bookings.package_id','LEFT');
		$this->db->join('users','users.id=packages.provider_id','LEFT');
		$this->db->join('company_designations','company_designations.user_id=users.id','LEFT');
		$this->db->join('companies','companies.id=company_designations.company_id','LEFT');
		$this->db->where('bookings.id',$data);
		$q = $this->db->get('bookings');
	    return $q->result();
	}
	public function getBysummaryCompanyAgent($data){
		$this->db->select('companies.name AS COMPANYNAME');
		$this->db->join('company_designations'	,'company_designations.user_id=bookings.user_id'	,'LEFT');
		$this->db->join('companies'				,'companies.id=company_designations.company_id'		,'LEFT');
		$this->db->where('bookings.id',$data);
		$q = $this->db->get('bookings');
	    return $q->result();
	}
	/*
	*
	*End getBysummary function into database globalexplorer_db
	*
	*/

	/*
	*
	*Start getBysummaryRoom function into database globalexplorer_db
	*
	*/

	public function getBysummaryRoomtype($data){
		$this->db->select(' bookings.id AS bookingid,
							bookings.code AS bookingcode,
							booked_hotels.booking_id AS bookedhotelsid,
							booked_hotelrooms.booked_hotel_id AS bookhotelroomsid,
							hotelroom_rates.room_type AS roomtype,
							hotelroom_rates.rate_type AS ratetype,
							hotelroom_rates.pax AS roompax,
							hotelroom_rates.cost AS roomcost,
							hotelroom_rates.profit AS roomprofit,
							hotelroom_rates.description AS roomdescription,
							booked_hotelrooms.quantity AS paxquantity');
		$this->db->join('booked_hotels','booked_hotels.booking_id=bookings.id','LEFT');
		$this->db->join('booked_hotelrooms','booked_hotelrooms.booked_hotel_id=booked_hotels.id','LEFT');
		$this->db->join('hotelroom_rates','hotelroom_rates.id=booked_hotelrooms.hotelroom_id','LEFT');
		$this->db->where('bookings.id',$data);
		$this->db->order_by('booked_hotelrooms.booked_hotel_id');
		$q = $this->db->get('bookings');
	    return $q->result();
	}
	/*
	*
	*End getBysummaryRoom function into database globalexplorer_db
	*
	*/
	/*
	*
	*Start getBysummaryRoom function into database globalexplorer_db
	*
	*/

	public function getBysummaryAddons($data){
		$this->db->select(' addons.description AS addonsdesc,
							addons.unit AS addonsunit,
							addons.cost AS addonscost,
							addons.profit AS addonsprofit,
							applied_addons.quantity AS appaddonsqty');
		$this->db->join('applied_addons','applied_addons.booking_id=bookings.id','LEFT');
		$this->db->join('addons','addons.id=applied_addons.addon_id','LEFT');
		$this->db->where('bookings.id',$data);
		$this->db->order_by('applied_addons.addon_id');
		$q = $this->db->get('bookings');
	    return $q->result();
	}
	/*
	*
	*End getBysummaryRoom function into database globalexplorer_db
	*
	*/

	/*
	*
	*Start bookingemailchecker function into database globalexplorer_db
	*
	*/
	public function bookingemailchecker($data){
		$this->db->select('bookings.id, bookings.package_id');
		$this->db->from('bookings');
		$this->db->where('bookings.id',$data);
		$this->db->where('bookings.status','pending');
		$q = $this->db->get();
	    return $q->result();
	}
	/*
	*
	*End bookingemailchecker function into database globalexplorer_db
	*
	*/

	/*
	*
	*Start bookingemailchecker function into database globalexplorer_db
	*
	*/
	public function bookingGetemail($data){
		$this->db->select(' bookings.id, 
							bookings.package_id,
							packages.admin_id,
							packages.provider_id,
							users.email AS adminemail,
							userprovider.email AS provideremail');
		$this->db->from('bookings');
		$this->db->join('packages','packages.id=bookings.package_id','LEFT');
		$this->db->join('users','users.id=packages.admin_id','LEFT');
		$this->db->join('users as userprovider','userprovider.id= packages.provider_id','LEFT');
		$this->db->where('bookings.id',$data);
		$q = $this->db->get();
	    return $q->result();
	}
	/*
	*
	*End bookingemailchecker function into database globalexplorer_db
	*
	*/
	/*
	*
	*Start bookingemailchecker function into database globalexplorer_db
	*
	*/
	public function getByreject($data){
		$this->db->select('	users.firstname AS userfname,
							users.lastname AS userlname,
							users.type AS userstatus,
							users.email AS useremail
							');
		$this->db->where('users.id',$data);
		$q = $this->db->get('users');
	    return $q->result();
	}
	/*
	*
	*End bookingemailchecker function into database globalexplorer_db
	*
	*/

	/*
	*
	*Start bookingemailchecker function into database globalexplorer_db
	*
	*/
	public function getByCancellation($data){
		$this->db->select('	bookings.id AS bookedid,
							bookings.code AS bookedcode,
							bookings.user_id AS bookeduserid,
							bookings.extended_days as bookingextend,
							users.firstname AS userfirstname,
							users.lastname AS userlastname,
							users.type AS agenttype,
							users.email AS agentemail,
							users.alternative_email AS agent_alter_email,
							packages.title AS packtitle,
							useradmin.firstname AS adminfirstname,
							useradmin.lastname AS adminlastname,
							useradmin.type AS admintype,
							useradmin.email AS adminemail,
							useradmin.alternative_email AS admin_alter_email,
							bookings.departure_date AS bookeddepart,
							bookings.return_date AS bookedreturn,
							userprovider.type AS providertype,
							userprovider.email AS provideremail,
							userprovider.alternative_email AS provider_alter_email,
							userprovider.firstname AS providerfirstname,
							userprovider.lastname AS providerlastname');
		$this->db->join('users','users.id=bookings.user_id','LEFT');
		$this->db->join('packages','packages.id=bookings.package_id','LEFT');
		$this->db->join('users as useradmin','useradmin.id=packages.admin_id','LEFT');
		$this->db->join('users as userprovider','userprovider.id=packages.provider_id','LEFT');
		$this->db->where('bookings.id',$data);
		$q = $this->db->get('bookings');
	    return $q->result();
	}
	/*
	*
	*End bookingemailchecker function into database globalexplorer_db
	*
	*/

	function report_print_query( $sql )
	{
		$query = $this->db->query( $sql );
		echo $this->db->_error_message();
		return $query->result();
	}

	function getBysummaryCheckRatePackageHotel($id, $id_hotel = FALSE){
		$this->db->select('package_hotels.id AS PACK_HOTEL_ID,
			package_hotels.package_id AS PACK_ID,
			package_hotels.hotel_id AS HOTEL_ID,
			hotels.name AS HOTEL_NAME');
		$this->db->join('hotels','hotels.id=package_hotels.hotel_id','LEFT');
		$this->db->where('package_hotels.package_id',$id);
		$this->db->where('package_hotels.status','active');

		if ($id_hotel) {
			$this->db->where('package_hotels.hotel_id',$id_hotel);
		}

		$this->db->order_by('hotels.name ASC');

		$q = $this->db->get('package_hotels');
	    return $q->result();
	}

	function getBysummaryCheckRateHotelRoom($id){
		$this->db->select('hotelroom_rates.id AS HRM_ID,
			hotelroom_rates.package_hotel_id AS HRM_PACK_ID,
			hotelroom_rates.room_type AS HRM_ROOM_TYPE,
			hotelroom_rates.description HRM_DESC,
			hotelroom_rates.rate_type AS HRM_RATE_TYPE,
			hotelroom_rates.cost AS HRM_COST,
			hotelroom_rates.profit AS HRM_PROFIT,
			hotelroom_rates.ext_cost AS HRM_EXT_COST,
			hotelroom_rates.ext_profit AS HRM_EXT_PROFIT');
		$this->db->where('hotelroom_rates.package_hotel_id',$id);
		$this->db->where('hotelroom_rates.status','active');

		$q = $this->db->get('hotelroom_rates');
	    return $q->result();
	}

	function getBysummaryPricelist($id,$hotel_name_id,$in,$out){
		$this->db->select(' packages.title AS packagestitle,
							hotels.name AS hotelname,
							hotelroom_rates.room_type AS hotelroomtype,
							hotelroom_rates.description AS hoteldescription,
							hotelroom_rates.rate_type AS hotelratetype,
							hotelroom_rates.cost AS hotelcost,
							hotelroom_rates.ext_cost AS hotelext_cost');
		$this->db->join('package_hotels','package_hotels.package_id=packages.id','LEFT');
		$this->db->join('hotels','hotels.id=package_hotels.hotel_id','LEFT');
		$this->db->join('hotelroom_rates','hotelroom_rates.package_hotel_id=package_hotels.id','LEFT');
		$this->db->where('packages.id',$id);
		if($hotel_name_id != 0){
			$this->db->where('hotels.id',$hotel_name_id);
		}
		$this->db->order_by('packages.title');
		$this->db->order_by('hotels.name','asc');
		$q = $this->db->get('packages');
	    return $q->result();
	}
	function getBypackageIdPermission($id){
		$this->db->select(' packages.id as id,
							packages.title AS title,
							users.firstname as user_name
							');
		$this->db->join('package_permissions','package_permissions.package_id=packages.id','LEFT');
		$this->db->join('users','users.id=package_permissions.user_id','LEFT');
		$this->db->where('users.id',$id);
		$this->db->order_by('packages.id','asc');
		$this->db->group_by('packages.id','asc');
		$q = $this->db->get('packages');
	    return $q->result();
	}

	function getBysummaryPackagesId($data = null){
		$this->db->select('packages.*,package_destinations.country_code,countries.country');
		$this->db->join('package_destinations'	,'package_destinations.package_id = packages.id'		,'LEFT');
		$this->db->join('countries'	,'countries.code = package_destinations.country_code'				,'LEFT');

		$this->db->join('package_permissions'	,'package_permissions.package_id = packages.id'			,'LEFT');
		$this->db->join('users'					,'users.id = package_permissions.user_id'				,'LEFT');
		$this->db->where("users.id",$data);
		
		$this->db->group_by('package_destinations.country_code');
		$q = $this->db->get('packages');
	    return $q->result();
	}

	function getBysummaryPricelist_($data,$packageId){
		$this->db->select('packages.id AS PACKID,
			packages.title AS PACKTITLE,
			packages.image_price AS PACKLINK');

		$this->db->join('package_permissions'	,'package_permissions.package_id = packages.id'		,'LEFT');
		$this->db->join('users'					,'users.id = package_permissions.user_id'			,'LEFT');

		$this->db->where('packages.image_price <> ""');
		$this->db->where("users.id",$data);
		$this->db->where("packages.id",$packageId);
		$this->db->order_by('packages.id','asc');
		$this->db->group_by('packages.id');
		$q = $this->db->get('packages');
	    return $q->result();
	}

	function getBysummaryPricelistcsv($data){
		$this->db->select('packages.id AS PACKID,
			packages.title AS PACKTITLE,
			packages.image_price AS PACKLINK');

		$this->db->join('package_permissions'	,'package_permissions.package_id = packages.id'		,'LEFT');
		$this->db->join('users'					,'users.id = package_permissions.user_id'			,'LEFT');

		$this->db->where('packages.image_price <> ""');
		$this->db->where("users.id",$data);
		$this->db->order_by('packages.id','asc');
		$this->db->group_by('packages.id');
		$q = $this->db->get('packages');
	    return $q->result();
	}


	function updateStatusBooking($id,$action){
			$this->db->trans_start();
			if($action=='confirmed'){
				$this->db->where('bookings.id',$id);
            	$data=$this->db->update('bookings',array('status' =>$action,'date_confirmed'=>date('Y-m-d'))); 
			}
			else{
				$this->db->where('bookings.id',$id);
            	$data=$this->db->update('bookings',array('status' =>$action)); 
			}
            // $this->db->where('bookings.id',$id);
            // $data=$this->db->update('bookings',array('status' =>$action)); 
            $this->db->trans_commit();
			if ($this->db->trans_status() === true) {
				return  $data;
			} else {
				return false;
			} 
	}

	function getBypackageIdprice(){
		$this->db->select('packages.id AS packid,packages.title AS packtitle');
		$q = $this->db->get('packages');
	    return $q->result();
	}

	/*
	*lester padul code
	*get the data summary..
	*/
	public function getDataSummaries($booking_id,$type){
		$this->db->select("*");
		$this->db->where("booking_id", $booking_id);
		$this->db->where("type", $type);
		$this->db->from("summary_data");
		$q = $this->db->get();
		return $q->result();
	}/*
	*lester padul code
	*get the data summary..
	*/
	public function getDataSummariesProvider($booking_id,$type){
		$this->db->select("*");
		$this->db->where("booking_id", $booking_id);
		$this->db->where("type", $type);
		$this->db->from("summary_data");
		$q = $this->db->get();
		return $q->result();
	}

	public function getByagentreport(){
		$this->db->select("companies.id AS id, companies.name AS compname");
		$this->db->from("users");
		$this->db->join('company_designations'	,'company_designations.user_id = users.id'			,'LEFT');
		$this->db->join('companies'				,'companies.id = company_designations.company_id'	,'LEFT');
		$this->db->where("users.type",'agent');
		$this->db->where("users.status",'active');
		$this->db->group_by("companies.name");
		$this->db->order_by("companies.name", "ASC");
		$q = $this->db->get();
		return $q->result();
	}
	public function getByproviderreport(){
		$this->db->select("companies.id AS id, companies.name AS compname");
		$this->db->from("users");
		$this->db->join('company_designations'	,'company_designations.user_id = users.id'			,'LEFT');
		$this->db->join('companies'				,'companies.id = company_designations.company_id'	,'LEFT');
		$this->db->where("users.type",'provider');
		$this->db->where("users.status",'active');
		$this->db->group_by("companies.name");
		$this->db->order_by("companies.name", "ASC");
		$q = $this->db->get();
		return $q->result();
	}

	public function getEmailadmin(){
		$this->db->select("users.email AS adminemail,users.alternative_email AS alteremail,users.id AS adminid,users.firstname AS adminfname,users.lastname AS adminlname");
		$this->db->from("users");
		$this->db->where("type",'admin');
		$this->db->where("status",'active');
		$q = $this->db->get();
		return $q->result();
	}

	public function getEmailadmin_company(){
		$this->db->select("users.email AS adminemail,users.alternative_email AS alteremail,users.id AS adminid,users.firstname AS adminfname,users.lastname AS adminlname");
		$this->db->from("users");
		$this->db->join('company_designations','users.id=company_designations.user_id','LEFT');
		//$this->db->join('companies','companies.id=company_designations.provider_id','LEFT');
		$this->db->where("users.type",'admin');
		$q = $this->db->get();
		return $q->result();
	}

	public function getEmailprovider($id){
		$this->db->select("users.email AS provideremail,users.alternative_email AS alteremail,packages.provider_id AS providerid,users.firstname AS providerfname,users.lastname AS providerlname");
		$this->db->from("bookings");
		$this->db->join('packages','bookings.package_id = packages.id','LEFT');
		$this->db->join('users','users.id=packages.provider_id','LEFT');
		$this->db->where("users.type",'provider');
		$this->db->where("users.status",'active');
		$this->db->where("bookings.id",$id);
		$q = $this->db->get();
		return $q->result();
	}

	public function getEmailagent($id){
		$this->db->select("users.email AS agentemail,users.alternative_email AS alteremail,users.id AS agentid,users.firstname AS agentfname,users.lastname AS agentlname");
		$this->db->from("users");
		$this->db->join('bookings','bookings.user_id = users.id','LEFT');
		$this->db->where("type",'agent');
		$this->db->where("bookings.id",$id);
		$this->db->where("users.status",'active');
		$q = $this->db->get();
		return $q->result();
	}
	public function getEmailcustomer($id){
		$this->db->select("users.email AS customeremail,users.id AS customerid,users.firstname AS customerfname,users.lastname AS customerlname");
		$this->db->from("users");
		$this->db->join('bookings','bookings.user_id = users.id','LEFT');
		$this->db->where("type",'customer');
		$this->db->where("bookings.id",$id);
		$q = $this->db->get();
		return $q->result();
	}
	public function getBookingEveryDay(){
		$date = date("Y-m-d");
		$this->db->select(" bookings.id as bid, 
							bookings.package_id as packages_id, 
							bookings.code as booking_code,
							bookings.status booking_status, 
							bookings.date_booked as booking_date,
							companies.name as company_name");
		$this->db->from("bookings");
		$this->db->join('company_designations',		'company_designations.user_id = bookings.user_id',	'LEFT');
		$this->db->join('companies',				'companies.id = company_designations.company_id',	'LEFT');
	    $this->db->where('bookings.id IN (SELECT MAX(bookings.id) FROM bookings GROUP BY bookings.code)');
		$this->db->where("bookings.date_booked",substr($date,0,10));
		$this->db->order_by("bookings.status");
		$q = $this->db->get();
		return $q->result();
	}

	public function getBookingMonthlys($userid){
		$fromDate = date("Y-m-d", strtotime("-1 month"));
		$this->db->select("bookings.id,
			bookings.user_id,
			bookings.departure_date,
			bookings.package_id,
			bookings.code,
			bookings.total_price,
			bookings.total_discount,
			users.firstname,
			users.lastname,
			users.email,
			users.alternative_email,
			companies.name,
			packages.title,
			bookings.departure_date,
			traveller_details.name AS PAXNAME,
			count(bookings.code) as numberPax");
		$this->db->join('users'								,'users.id = bookings.user_id','LEFT');
	    $this->db->join('company_designations'				,'company_designations.user_id = users.id','LEFT');
	    $this->db->join('companies'							,'companies.id = company_designations.company_id','LEFT');
	    $this->db->join('packages'							,'packages.id = bookings.package_id','LEFT');
	    $this->db->join('traveller_details'                 ,'traveller_details.booking_id = bookings.id','LEFT');
	    $this->db->where('bookings.user_id',$userid);
	    $this->db->where('bookings.status'					,'confirmed');
	    $this->db->where('bookings.id IN (SELECT MAX(bookings.id) FROM bookings GROUP BY bookings.code)');
	    $this->db->where('bookings.departure_date LIKE "'.substr($fromDate,0,7).'%"');
		//$this->db->order_by("bookings.id");
		$this->db->group_by("bookings.code");
		return $this->db->get("bookings")->result_array();
	}
	public function getBookingMonthlysProvider($userid){
		$fromDate = date("Y-m-d", strtotime("-1 month"));
		$this->db->select("bookings.id,
		bookings.package_id,
		bookings.departure_date,
		bookings.code,
		bookings.total_price,
		packages.title,
		packages.provider_id,
		users.alternative_email,
		users.email,
		users.firstname,
		company_designations.user_id,
		company_designations.company_id,
		companies.name");
	    $this->db->join('packages','packages.id = bookings.package_id','LEFT');
	    $this->db->join('users','users.id = packages.provider_id','LEFT');
	    $this->db->join('company_designations','company_designations.user_id = users.id','LEFT');
	    $this->db->join('companies','companies.id = company_designations.company_id','LEFT');
	    $this->db->where('bookings.user_id',$userid);
	    $this->db->where('bookings.status','confirmed');
	    $this->db->where('bookings.departure_date LIKE "'.substr($fromDate,0,7).'%"');
		$this->db->order_by("bookings.id");
		$this->db->group_by("bookings.code");
		return $this->db->get("bookings")->result_array();
	}


	public function getBookingMonthlyRecipients(){
		$fromDate = date("Y-m-d", strtotime("-1 month"));
		$this->db->select("bookings.id,
			bookings.user_id,
			bookings.departure_date,
			bookings.package_id,
			bookings.code,
			bookings.total_price,
			users.firstname,
			users.lastname,
			users.email,
			users.alternative_email,
			companies.name");
		$this->db->join('packages'							,'packages.id = bookings.package_id','LEFT');
		$this->db->join('traveller_details'                 ,'traveller_details.booking_id = bookings.id','LEFT');
	    $this->db->join('users'								,'users.id = bookings.user_id','LEFT');
	    $this->db->join('company_designations'				,'company_designations.user_id = users.id','LEFT');
	    $this->db->join('companies'							,'companies.id = company_designations.company_id','LEFT');
	    $this->db->where('bookings.status'					,'confirmed');
	    $this->db->where('bookings.departure_date LIKE "'.substr($fromDate,0,7).'%"');
	    $this->db->where('users.status'						,'active');
		$this->db->order_by("bookings.id");
		$this->db->order_by("users.email");
		$this->db->group_by("bookings.user_id");
		return $this->db->get("bookings")->result_array();
	}

	public function getBookingMonthlyRecipientsProviders(){
		$fromDate = date("Y-m-d", strtotime("-1 month"));
		$this->db->select("bookings.id,
			bookings.user_id,
			bookings.package_id,
			bookings.departure_date,
			packages.provider_id,
			users.alternative_email,
			users.email,
			users.firstname,
			userprovider.alternative_email as provideralternativeemail,
			userprovider.email as provideremail,
			userprovider.firstname as providerfirstname,
			company_designations.user_id as comapanyuser_id,
			company_designations.company_id,
			companies.name as com_name");
	    $this->db->join('users','users.id = bookings.user_id','LEFT');
 		$this->db->join('packages','packages.id = bookings.package_id','LEFT');
	    $this->db->join('users as userprovider','userprovider.id = packages.provider_id','LEFT');
	    $this->db->join('company_designations','company_designations.user_id','LEFT');
	    $this->db->join('companies','companies.id = company_designations.company_id','LEFT');
	    $this->db->where('bookings.departure_date LIKE "'.substr($fromDate,0,7).'%"');
		$this->db->order_by("bookings.id");
		$this->db->group_by("bookings.user_id");
		return $this->db->get("bookings")->result_array();
	}

	public function getBookingCheckerAccount($booking_id){
		$this->db->select("bookings.id,
						   bookings.user_id ");
		$this->db->join('users','users.id=bookings.user_id','LEFT');
		$this->db->where("bookings.id", $booking_id);
		$this->db->where("users.type",'customer');
		return $this->db->get("bookings")->result_array();
	}
	public function getBookingCheckerAccountAgent($booking_id){
		$this->db->select("bookings.id,
						   bookings.user_id ");
		$this->db->join('users','users.id=bookings.user_id','LEFT');
		$this->db->where("bookings.id", $booking_id);
		$this->db->where("users.type",'agent');
		return $this->db->get("bookings")->result_array();
	}

	public function getBookingCode($booking_id){
		$this->db->select("bookings.code,bookings.status");
		$this->db->where("bookings.id", $booking_id);
		return $this->db->get("bookings")->result_array();
	}

	public function addDiscount($booking_id, $discount){
		$this->db->set("bookings.total_discount", $discount);
		$this->db->where("bookings.id", $booking_id);
		return $this->db->update("bookings");
	}
	public function addAdditionalCharge($booking_id, $additional){
		$this->db->set("bookings.total_additional", $additional);
		$this->db->where("bookings.id", $booking_id);
		return $this->db->update("bookings");
	}
	public function addDifference($booking_id, $difference){
		$this->db->set("bookings.total_difference", $difference);
		$this->db->where("bookings.id", $booking_id);
		return $this->db->update("bookings");
	}

	public function getBookingIDs($booking_code){
		$this->db->select("bookings.id");
		$this->db->where("bookings.code",$booking_code);
		$this->db->order_by("bookings.id", "DESC");
		return $this->db->get("bookings",2,0)->result_array();
	}
	public function addPaymenttype($data_payment){
    	 	$this->db->trans_start();
    	 	$this->db->insert('payment_type',$data_payment);
            $data['id']= $this->db->insert_id();
            $this->db->trans_commit();
			if ($this->db->trans_status() === true) {
				return  $data;
			} else {
				return false;
			} 
	}

	public function payment_provider($id,$paymentid){
			$this->db->trans_start();
            $this->db->where('booking_id',$id);
            $data=$this->db->update('payments',array('payment_type_id' =>$paymentid, 'status' =>'paid', 'date'=>date('Y-m-d h:i:s'))); 
            $this->db->trans_commit();
			if ($this->db->trans_status() === true) {
				return  $data;
			} else {
				return false;
			} 
	}

	public function package_hotel_($id){
		$this->db->select("package_hotels.package_id,hotels.name,hotels.id,packages.days");
		$this->db->join('hotels','hotels.id=package_hotels.hotel_id','LEFT');
		$this->db->join('packages','packages.id=package_hotels.package_id','LEFT');
		$this->db->where("package_hotels.package_id",$id);
		$this->db->where("hotels.status",'active');
		return $this->db->get("package_hotels")->result();
	}
	public function package_hotel_mobile(){
		$this->db->select("packages.id as package_id,packages.title as package_title,packages.days");
		$this->db->join('hotels','hotels.id=package_hotels.hotel_id','LEFT');
		$this->db->join('packages','packages.id=package_hotels.package_id','LEFT');
		$this->db->group_by("packages.title");
		return $this->db->get("package_hotels")->result();
	}
	public function package_hotel_mobile_($id){
		$this->db->select("hotels.name,hotels.id");
		$this->db->join('hotels','hotels.id=package_hotels.hotel_id','LEFT');
		$this->db->join('packages','packages.id=package_hotels.package_id','LEFT');
		$this->db->where("package_hotels.package_id",$id);
		return $this->db->get("package_hotels")->result();
	}

	public function getBysummaryPdfairport(){
		$this->db->select('countries.country,destination_pdf.pdf_name,destination_pdf.title');
		$this->db->join('countries','countries.code= destination_pdf.destination','CROSS');
		$this->db->order_by('destination_pdf.id','asc');
		$q = $this->db->get('destination_pdf');
	    return $q->result();
	}

	public function getBysummaryPdfairport_sort($dCode){
		$this->db->select('countries.country,destination_pdf.pdf_name,destination_pdf.title');
		$this->db->join('countries','countries.code= destination_pdf.destination','CROSS');
		$this->db->where('destination_pdf.destination',$dCode);
		$this->db->order_by('destination_pdf.id','asc');
		$q = $this->db->get('destination_pdf');
	    return $q->result();
	}
	public function getByuserAgentandProvider(){
		$this->db->select(' users.id as uid,
							users.firstname,
							users.lastname,
							users.type,
							company_designations.id as cdid,
							companies.id as cid,
							companies.name as cname
							');
		$this->db->join('company_designations','company_designations.user_id = users.id','LEFT');
		$this->db->join('companies','companies.id = company_designations.company_id','LEFT');
		$this->db->where('users.status','active');
		$this->db->where('users.type','agent');
		$this->db->or_where('users.type','provider');
		$this->db->group_by('companies.name');
		$this->db->group_by('companies.name','ASC');
		$q = $this->db->get('users');
	    return $q->result();
	}

	public function getHotelName($code){
		$booking_code = explode('GE',$code);

		if($booking_code[1] <= 100173){
	        $this->db->select('*');
	        $this->db->from('bookings book'); 
	        $this->db->join('booked_hotels bookh', 'bookh.id=book.id', 'left');
	        $this->db->join('package_hotels pack', 'pack.id=bookh.package_hotel_id', 'left');
	        $this->db->join('hotels hot', 'hot.id=pack.hotel_id', 'left');
	        $this->db->where('book.code',$code);  
            $this->db->order_by('bookh.id', 'desc');
       		$this->db->limit(1);	   
	        $query = $this->db->get(); 
	        return $query->result_array();
		}else{
			$this->db->select('*');
	        $this->db->from('bookings book'); 
	        $this->db->join('booked_hotels bookh', 'bookh.id=book.id', 'left');
	        $this->db->join('hotels hot', 'hot.id=bookh.package_hotel_id', 'left');
	        $this->db->where('book.code',$code);   
	        $this->db->order_by('bookh.id', 'desc');
	        $this->db->limit(1);
	        $query = $this->db->get(); 


	        foreach ($query->result_array() as $row) {
	        	if ($row['name'] === NULL) {
	        		$this->db->select('*');
			        $this->db->from('bookings book'); 
			        $this->db->join('booked_hotels bookh', 'bookh.id=book.id', 'left');
			        $this->db->join('package_hotels pack', 'pack.id=bookh.package_hotel_id', 'left');
			        $this->db->join('hotels hot', 'hot.id=pack.hotel_id', 'left');
			        $this->db->where('book.code',$code);  
		            $this->db->order_by('bookh.id', 'desc');
		       		$this->db->limit(1);	   
			        $query1 = $this->db->get(); 
			        return $query1->result_array();
	        	}
	        	else{
					return $query->result_array();
	        	}
			}

        
        }
        /*if($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
	        $this->db->select('*');
	        $this->db->from('bookings book'); 
	        $this->db->join('booked_hotels bookh', 'bookh.id=book.id', 'left');
	        $this->db->join('package_hotels pack', 'pack.id=bookh.package_hotel_id', 'left');
	        $this->db->join('hotels hot', 'hot.id=pack.hotel_id', 'left');
	        $this->db->where('book.code',$code);  
            $this->db->order_by('bookh.id', 'desc');
       		$this->db->limit(1);	   
	        $query = $this->db->get(); 
	        return $query->result_array();
        }*/
	}
	public function get_booking_info($booking_id){
		$this->db->select("*");
		$this->db->from("traveller_details");
		$this->db->where('booking_id',$booking_id);
		$query = $this->db->get();

		return $query->result();
	}

	public function get_room_info($booking_id){
		$this->db->select("*");
		$this->db->from("summary_data");
		$this->db->where('booking_id',$booking_id);
		$this->db->where('type','3room_payment');
		$query = $this->db->get();

		return $query->result();
	}

	public function get_airportcode($id){
		$this->db->select("code");
		$this->db->from("airportcodes");
		$this->db->where('id',$id);
		$query = $this->db->get();

		return $query->result();
	}

	public function get_distinct(){
		$date = date('Y-m-d');
		$this->db->select("distinct(user_id),package_id,id");
		$this->db->from("bookings");
		$this->db->where(array('departure_date =' => date('Y-m-d',strtotime($date."+ 2 days")), 'status' => 'confirmed'));
		$query = $this->db->get();

		return $query->result();
	}

	public function getPassengers($booking_id){
		return $this->db->get_where('traveller_details',array('booking_id'=>$booking_id))->result();
	}

	public function getSummaryDestinations($booking_id){
		$this->db->select("summary_data.destination_id, package_destinations.country_code, countries.country, package_destinations.nights");
		$this->db->from("summary_data");
		$this->db->join("package_destinations","summary_data.destination_id=package_destinations.id", "LEFT");
		$this->db->join("countries","countries.code=package_destinations.country_code", "LEFT");
		$this->db->where("booking_id",$booking_id);
		$this->db->where("destination_id !=","0");
		$this->db->order_by("destination_id", "ASC");
		$this->db->group_by("destination_id");
		return $this->db->get()->result_array();
	}

	public function getSummaryInformation($array){
		$this->db->select("summary_data.*,hotels.name as hotel_name, bookings.total_discount as discount");
		$this->db->join("hotels", "summary_data.hotel_id = hotels.id", "LEFT");
		$this->db->join("bookings", "summary_data.booking_id = bookings.id", "LEFT");
		$this->db->where($array);
		return $this->db->get("summary_data")->result_array();
	}

	public function getAgentandCompany($user_id){
		$this->db->select(' users.firstname as fname,
							users.lastname as lname,
							companies.name as cname
							');
		$this->db->join('company_designations','company_designations.user_id = users.id','LEFT');
		$this->db->join('companies','companies.id = company_designations.company_id','LEFT');
		$this->db->where('users.status','active');
		$this->db->where('users.type','agent');
		$this->db->where('users.id',$user_id);
		$q = $this->db->get('users');
	    return $q->result();
	}



}
