<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

get_instance()->load->iface('capturestateinterface');
get_instance()->load->iface('storeiteminterface');

/**
 * @author Kirstin Rubica
 *
 */
class Package extends CI_Model implements CaptureStateInterface, StoreItemInterface {
	
	/**
	 * @var integer
	 */
	private $id;
	
	/**
	 * @var Admin
	 */
	private $admin;
	
	/**
	 * @var Provider
	 */
	private $provider;
	
	/**
	 * @var array|HotelRoomRate,
	 */
	private $hotelRoomRates;
	
	/**
	 * @var string
	 */
	private $code;
	
	/**
	 * @var string
	 */
	private $title;
	
	/**
	 * @var string
	 */
	private $description;
	
	/**
	 * @var string
	 */
	private $termsAndConditions;

	/**
	 * @var string
	 */
	private $imagePath;
	
	/**
	 * @var string
	 */
	private $dateFrom;
	
	/**
	 * @var string
	 */
	private $dateTo;
	
	/**
	 * @var string
	 */
	private $days;
	
	/**
	 * @var string
	 * total number of nights
	 */
	private $nights;

	/**
	 * @var string
	 */

	private $status;

	/**
	 * @var string
	 */

	private $getImage_price;
	
	/**
	 * @var string
	 */
	private $surcharge;
	
	/**
	 * @var string
	 */
	private $addons;
	
	/**
	 * @var string
	 */
	private $permission;
	
	/**
	 * @var string
	 */
	private $prevState;

	/**
	 * @var array
	 */
	private $destinations;

	/**
	 * 
	 */
	public function __construct() {
		parent::__construct();
		$this->hotelRoomRates	= array();
		$this->surcharge		= array();
		$this->addons			= array();
		$this->destinations		= array();
	}
	
	/**
	 * 
	 */
	public function getId() {
		return $this->id;
	}
	
	/**
	 * @return Admin
	 */
	public function getAdmin() {
		return $this->admin;
	}
	
	/**
	 * @param Admin $admin
	 */
	public function setAdmin(Admin $admin) {
		if ($admin instanceof Admin) {
			if ($admin->getUser()->stored()) {
				$this->admin = $admin;
			}
		}
	}
	
	/**
	 * @return Provider
	 */
	public function getProvider() {
		return $this->provider;
	}
	
	/**
	 * @param Provider $provider
	 */
	public function setProvider(Provider $provider) {
		if ($provider instanceof Provider) {
			if ($provider->getUser()->stored()) {
				$this->provider = $provider;
			}
		}
	}
	
	/**
	 * @param string $searchByName
	 */
	public function getHotel($searchByName = NULL) {
		
	}
	
	/**
	 * 
	 */
	public function getCode() {
		return $this->code;
	}


	/**
	 * @param string $code
	 */
	public function setCode($code) {
		$this->code = $this->checkString($code);
	}
	
	/**
	 * @return string
	 */
	public function getTitle() {
		return $this->title;
	}
	
	/**
	 * @param string $title
	 */
	public function setTitle($title) {
		$this->title = $this->checkString($title);
	}
	
	/**
	 * 
	 */
	public function getDescription() {
		return $this->description;
	}

	
	
	/**
	 * @param string $description
	 */
	public function setDescription($description) {
		$this->description = $this->checkString($description);
	}
	/**
	 * 
	 */
	public function getImage_price() {
		return $this->image_price;
	}
	/**
	 * 
	 */
	public function setImage_price($image_price) {
		return $this->image_price = $this->checkString($image_price);
	}
	
	/**
	 * 
	 */
	public function getTermsAndConditions() {
		return $this->termsAndConditions;
	}
	
	/**
	 * @param string $termsAndConditions
	 */
	public function setTermsAndConditions($termsAndConditions) {
		$this->termsAndConditions = $this->checkString($termsAndConditions);
	}


	/**
	 * 
	 */
	public function getImagePath() {
		return $this->imagePath;
	}
	
	/**
	 * @param string $image_path
	 */
	public function setImagePath($image_path) {
		$this->imagePath = $this->checkString($image_path);
	}

	/**
	 * @return Ambigous <NULL, string>
	 */
	public function getDateFrom() {
		return $this->dateFrom;
	}
	
	/**
	 * @return Ambigous <NULL, string>
	 */
	public function getDateTo() {
		return $this->dateTo;
	}
	
	/**
	 * 
	 */
	public function getDays() {
		return (NULL !== $this->nights) ? ($this->nights + 1) : null;
	}
	
	/**
	 * @param string $days
	 */
	public function setDays($days) {
		$this->days = (NULL !== $days) ? ($days) : null;
	}
	
	public function getNights() {
		return $this->nights;
	}
	
	/**
	 * @param string $nights
	 */
	public function setNights($nights) {
		$this->nights = $nights;
	}


	/**
	 * 
	 */
	public function getStatus() {
		return $this->status;
	}
	
	/**
	 * @param string $status
	 */
	public function setStatus($status) {
		$this->status = $this->checkString($status);
	}
	
	
	/**
	 * @return multitype:Ambigous <NULL, string> 
	 */
	public function getDateRange() {
		return array(
			'dateFrom' => $this->dateFrom,
			'dateTo' => $this->dateTo
		);
	}
	
	/**
	 * @param string $dateFrom
	 * @param string $dateTo
	 */
	public function setDateRange($dateFrom, $dateTo) {
		$this->dateFrom = $dateFrom;
		$this->dateTo   = $dateTo;
	}

	/**
	 * 
	 */
	public function getRangeDiff() {
		return $this->checkValidRange($this->dateFrom, $this->dateTo);
	}
	
	/**
	 * @return Ambigous <multitype:, Surcharge>
	 */
	public function getSurcharge() {
		return $this->surcharge;
	}
	
	/**
	 * @param Surcharge $surcharge
	 */
	public function addSurcharge(Surcharge $surcharge) {
		if ($surcharge instanceof Surcharge) {
			$this->surcharge[] = $surcharge;
		}
	}
	
	/**
	 * @return Ambigous <multitype:, Addon>
	 */
	public function getAddons() {
		return $this->addons;
	}
	
	/**
	 * @param Addon $addon
	 */
	public function addAddon(Addon $addon) {
		if ($addon instanceof Addon) {
			$addon->setPackage($this);
			$this->addons[] = $addon;
		}
	}

	/**
	 * @return Ambigous <multitype:, Addon>
	 */
	public function getDestinations() {
		return $this->destinations;
	}
	
	public function clearDestinations() {
		return $this->destinations = array();
	}

	/**
	 * @param Addon $addon
	 */
	public function addDestination(Destination $destination) {
		if ($destination instanceof Destination) {
			$destination->setPackage($this);
			$this->destinations[] = $destination;
		}
	}

	public function hasDestination(){
		return (count($this->destinations) > 0) ? true : false;
	}

	/**
	 * @param HotelRoomRate $hotelroomrate
	 */
	public function addHotelRoomRate(HotelRoomRate $hotelroomrate) {
		if ($hotelroomrate instanceof HotelRoomRate) {
			$hotelroomrate->setPackage($this);
			$this->hotelRoomRates[] = $hotelroomrate;
		}
	}

	/**
	 * @param string $searchByHotelName
	 */
	public function removeHotelRoomRate($searchByHotelName = NULL) {
		
	}
	
	/* (non-PHPdoc)
	 * @see CaptureStateInterface::captureCurrentState()
	 */
	public function captureCurrentState() {
		if ($this->stored()) {
			$this->prevState = clone $this;
		}
	}
	
	/* (non-PHPdoc)
	 * @see CaptureStateInterface::prevState()
	 */
	public function prevState() {
		return (!empty($this->prevState)) ? $this->prevState : false;
	}
	
	/* (non-PHPdoc)
	 * @see StoreItemInterface::prepared()
	 */
	public function prepared() {
		// 	$this->provider instanceof Provider,$this->description,
		// 	$this->dateFrom,$this->dateTo,$this->days,is_null($this->id)));die;
		if ($this->title
			&& $this->hasDestination()
			&& $this->admin instanceof Admin
			&& $this->provider instanceof Provider
			&& $this->description
			&& $this->dateFrom
			&& $this->dateTo
			&& $this->nights
			&& $this->status
			&& is_null($this->id)){
				return true;
		}
		return false;
	}
	
	/* (non-PHPdoc)
	 * @see StoreItemInterface::stored()
	 */
	public function stored() {
		return ($this->id) ? true : false;
	}
	
	/**
	 * @param string $days
	 * @return Ambigous <NULL, string>
	 */
	private function checkDays($days) {
		return ($this->isInteger($days) && $days > 0) ? $days : null;
	}
	
	/**
	 * @param string $input
	 * @return boolean
	 */
	private function isInteger($input) {
		return (ctype_digit(strval($input)));
	}
	
	/**
	 * @param string $input
	 * @return Ambigous <NULL, string>
	 */
	private function checkString($input) {
		//return (is_string($input)) ? $input : null;
		return $input;
	}
	
	/**
	 * @param string $date
	 * @return Ambigous <NULL, string>
	 */
	private function checkDate($date) {
		return (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $date) > 0) ? $date : null;
	}
	
	/**
	 * @param string $dateFrom
	 * @param string $dateTo
	 * @return NULL
	 */
	public function checkValidRange($dateFrom, $dateTo) {
		if ($this->checkDate($dateFrom) && $this->checkDate($dateTo)) {
			$datetime1 = new DateTime($dateFrom);
			$datetime2 = new DateTime($dateTo);
			$diff      = $datetime2->diff($datetime1);

			return ($diff->days > 0 && $diff->invert == 1) ? $diff->days : null;
		} else
			return null;
	}
}