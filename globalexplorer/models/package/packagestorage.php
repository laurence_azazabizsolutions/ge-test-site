<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

get_instance()->load->iface('storageinterface');
get_instance()->load->iface('idmanipulatorinterface');

/**
 * @author Joshua Paylaga
 *
 */
class PackageStorage extends CI_Model implements StorageInterface, IdManipulatorInterface {

	/**
	 *
	 */
	protected $limit;
	protected $offset;

	public function __construct() {
		$this->load->database();
	}

	public function getLimit(){
		return $this->limit;
	}

	public function setLimit($limit = 0){
		$this->limit = ($limit > 0) ? $limit : 0;
	}

	public function getOffset(){
		return $this->offset;
	}

	public function setOffset($offset = 0){
		$this->offset = ($offset > 0) ? $offset : 0;
	}

	public function getImage_price($id){
		$this->db->select('image_price');
		$this->db->from('packages');
		$this->db->where('id',$id);
		$q=$this->db->get();
		return $q->result();
	}
	/* (non-PHPdoc)
	 * @see StorageInterface::getById()
	 */
	public function getById($id) {
		// Execute the mothafucking query
		$this->db->select("
			packages.*,
			packages.image_price as 'priceimage',
			package_destinations.id as 'destinationid',
			package_destinations.country_code as 'destinationcode',
			package_destinations.nights as 'destinationnights',
			package_destinations.status as 'destinationstatus',
			package_duration.date_from,
			package_duration.date_to,
			package_description.description as 'packagedescription',
			addons.id as 'addonid',
			addons.description as 'addondescription',
			addons.unit,
			addons.cost,
			addons.profit");
		$this->db->from("packages");
		$this->db->join("package_duration"		,"packages.id = package_duration.package_id"	,"left");
		$this->db->join("package_description"	,"packages.id = package_description.package_id"	,"left");
		$this->db->join("package_destinations"	,"packages.id = package_destinations.package_id","left");
		$this->db->join("addons"				,"packages.id = addons.package_id"				,"left");
		$this->db->where("packages.id",$id);
		$this->db->order_by("packages.title", "asc");
		return $this->castArrayToPackage($this->db->get()->result_array());
	}

	public function getByDays($id) {
		// Execute the mothafucking query
		$this->db->select("
			days");
		$this->db->from("packages");
		$this->db->where("id",$id);
		return $this->db->get()->result();
	}

	/* (non-PHPdoc)
	 * @see StorageInterface::getByInformation()
	 */
	public function getByInformation(array $info) {
		// Setup possible query info
		$packageInfo = array(
			'id'			=> false,
			'admin_id' 		=> false,
			'provider_id' 	=> false,
			'code' 			=> false,
			'title' 		=> false,
			'days' 			=> false,
			'date_from' 	=> false,
			'date_to' 		=> false,
			'status' 		=> false,
			'description' 	=> false,
			'country_code' 	=> false,
		);

		// assign values
		foreach ($info as $key => $infoValue) {
			if (in_array($key, array_keys($packageInfo))) {
				$packageInfo[$key] = $infoValue;
			}
		}

		// remove unassigned indexes
		foreach ($packageInfo as $key => $infoValue) {
			if (false === $infoValue) {
				unset($packageInfo[$key]);
			}
		}

		// create final array
		$finalPackageInfo = array();
		if (isset($packageInfo['id'])) 			$finalPackageInfo['packages.id'] = $packageInfo['id'];
		if (isset($packageInfo['admin_id'])) 	$finalPackageInfo['packages.admin_id'] = $packageInfo['admin_id'];
		if (isset($packageInfo['provider_id'])) $finalPackageInfo['packages.provider_id'] = $packageInfo['provider_id'];
		if (isset($packageInfo['code'])) 		$finalPackageInfo['packages.code'] = $packageInfo['code'];
		if (isset($packageInfo['title'])) 		$finalPackageInfo['packages.title'] = $packageInfo['title'];
		if (isset($packageInfo['days'])) 		$finalPackageInfo['packages.days'] = $packageInfo['days'];
		if (isset($packageInfo['status'])) 		$finalPackageInfo['packages.status'] = $packageInfo['status'];
		if (isset($packageInfo['date_from'])) 	$finalPackageInfo['package_duration.date_from'] = $packageInfo['date_from'];
		if (isset($packageInfo['date_to'])) 	$finalPackageInfo['package_duration.date_to'] = $packageInfo['date_to'];
		if (isset($packageInfo['description'])) $finalPackageInfo['package_description.description'] = $packageInfo['description'];
		if (isset($packageInfo['country_code']) && $packageInfo['country_code'] != ' ') $finalPackageInfo['package_destinations.country_code'] = $packageInfo['country_code'];
		// Execute the mothafucking query
		$this->db->select("package_destinations.*, packages.*, package_duration.date_from, package_duration.date_to, package_description.description as 'packagedescription', addons.id as 'addonid', addons.description as 'addondescription', addons.unit, addons.cost, addons.profit");
		$this->db->from("packages");
		$this->db->join("package_duration"		,"packages.id = package_duration.package_id"	,"left");
		$this->db->join("package_description"	,"packages.id = package_description.package_id"	,"left");
		$this->db->join("package_destinations"	,"packages.id = package_destinations.package_id","left");
		$this->db->join("addons"				,"packages.id = addons.package_id"				,"left");
		$this->db->where($finalPackageInfo);

		if (isset($packageInfo['country_code']) && $packageInfo['country_code'] != ' ')
			$this->db->where('package_destinations.status','active');

		// $this->db->order_by("packages.title", "asc");

		//$order by id;
		$this->db->order_by("packages.id", "asc");
		// $this->db->get();
		// echo $this->db->last_query(); die;
		return $this->castArrayToPackage($this->db->get()->result_array());
	}



	/* (non-PHPdoc)
	 * @see StorageInterface::store()
	 */
	public function store($package,$array_pricename=false) {

		if (!$package instanceof Package) {
			return;
		}

		$timestamp = time();

		if ($package->prepared()) {

			//check if the code entered by the creator already exists within the database
			if ($package->prepared() || $package->stored()) {
				$this->db->from('packages');
				$this->db->where('code', $package->getCode());
				$duplicates = $this->db->count_all_results();
				if ($duplicates > 0)
					return;
			}

			// Insert packages table
			$this->db->insert('packages', array(
				'admin_id' 		=> $package->getAdmin()->getUser()->getId(),
				'provider_id' 	=> $package->getProvider()->getUser()->getId(),
				'code' 			=> $package->getCode(),
				'tnc' 			=> $package->getTermsAndConditions(),
				'title' 		=> $package->getTitle(),
				'image_path' 	=> $package->getImagePath(),
				'image_price' 	=> $array_pricename,
				'days' 			=> $package->getDays(), //summary of all the days
				'status' 		=> $package->getStatus(),
				'timestamp' 	=> $timestamp
			));

			//get the package id
			$package_id = $this->db->insert_id();

			// Insert package_description table
			$this->db->insert('package_description', array(
				'package_id' 	=> $package_id,
				'description' 	=> $package->getDescription(),
				'timestamp' 	=> $timestamp
			));

			// Insert package_duration table
			$this->db->insert('package_duration', array(
				'package_id' 	=> $package_id,
				'date_from' 	=> $package->getDateFrom(),
				'date_to' 		=> $package->getDateTo(),
				'timestamp' 	=> $timestamp
			));

			$this->setId($package, $package_id);
			$package->captureCurrentState();

			if($package->hasDestination()){
				$this->load->model('package/destination');
				$destinationStorage = new Destination();
				$destinationStorage->setDestinationsInactive($package->getId());
				$destinationStorage->batchStore($package->getDestinations());
			}

		}

		// Edit Package object
		else if ($package->stored()) {
			$previousState = $package->prevState();
			if ($previousState instanceof Package) {


				// package table
				$packageToUpdate = array();
				// Provider
				if (($package->getProvider() != $previousState->getProvider()) && ($package->getProvider())) {
					$packageToUpdate['provider_id'] = $package->getProvider()->getUser()->getId();
				}

				// }

				// Code
				if (($package->getCode() != $previousState->getCode()) && ($package->getCode())) {
					$packageToUpdate['code'] = $package->getCode();
				}

				// Title
				if (($package->getTitle() != $previousState->getTitle()) && ($package->getTitle())) {
					$packageToUpdate['title'] = $package->getTitle();
				}

				// Image path
				if (($package->getImagePath() != $previousState->getImagePath()) && ($package->getImagePath())) {
					$packageToUpdate['image_path'] = $package->getImagePath();
				}

				// Days
				if (($package->getDays() != $previousState->getDays()) && ($package->getDays())) {
					$packageToUpdate['days'] = $package->getDays();
				}

				// Terms and Condition
				if (($package->getTermsAndConditions() != $previousState->getTermsAndConditions()) && ($package->getTermsAndConditions())) {
					$packageToUpdate['tnc'] = $package->getTermsAndConditions();
				}

				// Status
				if (($package->getStatus() != $previousState->getStatus()) && ($package->getStatus())) {
					$packageToUpdate['status'] = $package->getStatus();
				}

				// Image_price
				if ($array_pricename != "") {
					$packageToUpdate['image_price'] = $array_pricename;
				}
				// Update packages table
				if (count($packageToUpdate) > 0) {
					$this->db->where('id', $package->getId());
					$this->db->update('packages',$packageToUpdate);
				}

				// package_description table
				$packageDescriptionToUpdate = array();
				if (($package->getDescription() != $previousState->getDescription()) && ($package->getDescription())) {
					$packageDescriptionToUpdate['description'] = $package->getDescription();
				}

				// Update package_description table
				if (count($packageDescriptionToUpdate) > 0) {
					$this->db->where('package_id', $package->getId());
					$this->db->update('package_description', $packageDescriptionToUpdate);
				}

				// package_duration table
				$packageDurationToUpdate = array();
				if (count(array_diff($package->getDateRange(), $package->prevState()->getDateRange())) > 0) {
					$packageDurationToUpdate['date_from'] = $package->getDateFrom();
					$packageDurationToUpdate['date_to'] = $package->getDateTo();
				}

				// Update package_duration table
				if (count($packageDurationToUpdate) > 0) {
					$this->db->where('package_id', $package->getId());
					$this->db->update('package_duration', $packageDurationToUpdate);
				}

				if($package->hasDestination()){
					$this->load->model('package/destination');
					$destinationStorage = new Destination();
					$destinationStorage->setDestinationsInactive($package->getId());
					$destinationStorage->batchStore($package->getDestinations());
				}

				// Check Addons
				$this->addonInsertUpdate($package, time());

				$package->captureCurrentState();

			}
		}
	}




	/* (non-PHPdoc)
	 * @see StorageInterface::batchStore()
	 */
	public function batchStore(array $packages) {
		foreach ($packages as $package) {
			if ($package instanceof Package) {
				$this->store($package);
			}
		}
	}

	/* (non-PHPdoc)
	 * @see StorageInterface::remove()
	 */
	public function remove($package) {

	}

	/* (non-PHPdoc)
	 * @see IdManipulatorInterface::setId()
	 */
	public function setId($package, $id) {
		setprivateid($package, 'Package', $id);
	}

	/**
	 * @param array $dbResults
	 */
	private function castArrayToPackage(array $dbResults, $isMultiple = true) {
		$packageTable = array();

		// 1st level arrays
		foreach ($dbResults as $result) {
			//dump_exit($result['days']);

			// Check for duplicates
			$hasNoDuplicate = true;
			foreach ($packageTable as $perPackage) {
				if ($perPackage['id'] == $result['id']) {
					$hasNoDuplicate = false;
					break;
				}
			}

			// Assign information if no duplicate
			if ($hasNoDuplicate) {

				$tmpPackageTable = array(
					'id'			=> $result['id'],
					'admin_id'		=> $result['admin_id'],
					'provider_id'	=> $result['provider_id'],
					'description'	=> $result['packagedescription'],
					'code' 			=> $result['code'],
					'title' 		=> $result['title'],
					'image_path' 	=> $result['image_path'],
					'days' 			=> $result['days'],
					'tnc' 			=> $result['tnc'],
					'status'		=> $result['status'],
					'timestamp' 	=> $result['timestamp'],
					'durations'		=> array(),
					'destination'	=> array(),
					'addons'		=> array(),
				);

				$packageTable[] = $tmpPackageTable;
			}
		}

		// Durations
		foreach ($packageTable as &$insertPackageDuration) {
			foreach ($dbResults as $travResult) {
				if ($insertPackageDuration['id'] == $travResult['id']) {
					$insertPackageDuration['durations'][] = array(
						'date_from' => $travResult['date_from'],
						'date_to'	=> $travResult['date_to'],
					);
				}
			}
		}

		// Clean durations
		foreach ($packageTable as &$dirtyPackageDuration) {
			$durationsSubject = &$dirtyPackageDuration['durations'];
			$noDuplicatesStack = array();
			foreach ($durationsSubject as $skey => $subject) {
				$hasNoDuplicate = true;
				foreach ($noDuplicatesStack as $ckey => $compare) {
					if (($subject['date_from'] == $compare['date_from']) && ($subject['date_to'] == $compare['date_to'])) {
						$hasNoDuplicate = false;
						break;
					}
				}
				if ($hasNoDuplicate) {
					$noDuplicatesStack[] = $subject;
				}
			}
			$durationsSubject = $noDuplicatesStack;
		}

		// Destinations
		foreach ($packageTable as &$insertPackageDestinations) {
			foreach ($dbResults as $travResult) {
				if ($insertPackageDestinations['id'] == $travResult['id']) {
					if (!empty($travResult['destinationid'])) {
						$insertPackageDestinations['destination'][] = array(
							'id' 		  => $travResult['destinationid'],
							'country_code'=> $travResult['destinationcode'],
							'nights'	  => $travResult['destinationnights'],
							'status'	  => $travResult['destinationstatus'],
						);
					}
				}
			}
		}

		// Clean destinations
		foreach ($packageTable as &$dirtyPackageDestinations) {
			$destinationSubject = &$dirtyPackageDestinations['destination'];
			$noDuplicatesStack = array();
			foreach ($destinationSubject as $skey => $subject) {
				$hasNoDuplicate = true;
				foreach ($noDuplicatesStack as $ckey => $compare) {
					if (($subject['country_code'] == $compare['country_code']) && ($subject['nights'] == $compare['nights'])) {
						$hasNoDuplicate = false;
						break;
					}
				}
				if ($hasNoDuplicate) {
					$noDuplicatesStack[] = $subject;
				}
			}
			$destinationSubject = $noDuplicatesStack;
		}

		// Addons
		foreach ($packageTable as &$insertPackageAddons) {
			foreach ($dbResults as $travResult) {
				if ($insertPackageAddons['id'] == $travResult['id']) {
					if (!empty($travResult['addondescription'])) {
						$insertPackageAddons['addons'][] = array(
							'id'			=> $travResult['addonid'],
							'description' 	=> $travResult['addondescription'],
							'unit'			=> $travResult['unit'],
							'cost'			=> $travResult['cost'],
							'profit'		=> $travResult['profit'],
						);
					}
				}
			}
		}

		// Clean addons
		foreach ($packageTable as &$dirtyPackageAddons) {
			$addonsSubject = &$dirtyPackageAddons['addons'];
			$noDuplicatesStack = array();
			foreach ($addonsSubject as $skey => $subject) {
				$hasNoDuplicate = true;
				foreach ($noDuplicatesStack as $ckey => $compare) {
					if (($subject['description'] == $compare['description'])
						&& ($subject['unit'] == $compare['unit'])
						&& ($subject['cost'] == $compare['cost'])
						&& ($subject['profit'] == $compare['profit'])) {
						$hasNoDuplicate = false;
						break;
					}
				}
				if ($hasNoDuplicate) {
					$noDuplicatesStack[] = $subject;
				}
			}
			$addonsSubject = $noDuplicatesStack;
		}

		// Pass to Package Object
		$this->load->model('package/addon');
		$this->load->model('package/package');
		// $this->load->model('package/package_destination');
		$this->load->model('user/userstorage');
		$this->userstorage->includePending(true);
		$packageCollection = array();
		foreach ($packageTable as $perPackageTable) {
			$admin 		= $this->userstorage->getById($perPackageTable['admin_id']);
			$provider 	= $this->userstorage->getById($perPackageTable['provider_id']);
			if ($admin && $provider) {
				$package = new Package();


				foreach ($perPackageTable['durations'] as $finalDuration) {
					$package->setDateRange($finalDuration['date_from'], $finalDuration['date_to']);
				}

				$total_nights = 0;
				foreach ($perPackageTable['destination'] as $finalDestination) {
					$this->load->model('package/destination');
					$destination = new Destination();
					$destination->setCountryCode($finalDestination['country_code']);
					$destination->setNights($finalDestination['nights']);
					$destination->setStatus($finalDestination['status']);
					setprivateid($destination, 'Destination', $finalDestination['id']);
					$total_nights += $finalDestination['nights'];

					$package->addDestination($destination);
				}

				foreach ($perPackageTable['addons'] as $finalAddon) {
					$addon = new Addon();
					$addon->setCost($finalAddon['cost']);
					$addon->setDescription($finalAddon['description']);
					$addon->setProfit($finalAddon['profit']);
					$addon->setUnit($finalAddon['unit']);
					setprivateid($addon, 'Addon', $finalAddon['id']);

					$package->addAddon($addon);
				}

				$package->setAdmin($admin);
				$package->setProvider($provider);
				$package->setCode($perPackageTable['code']);
				$package->setNights($total_nights);
				$package->setDays($total_nights+1);
				$package->setTitle($perPackageTable['title']);
				$package->setDescription($perPackageTable['description']);
				$package->setImagePath($perPackageTable['image_path']);
				$package->setStatus($perPackageTable['status']);
				$package->setTermsAndConditions($perPackageTable['tnc']);

				setprivateid($package, 'Package', $perPackageTable['id']);
				$packageCollection[] = $package;
			}
		}

		if ($isMultiple) {
			return $packageCollection;
		} else {
			if (isset($packageCollection[0])) {
				return $packageCollection[0];
			}
		}

		return false;
	}

	/**
	 * Takes care of addons CRUD
	 *
	 * @param Package $package
	 * @param integer $timestamp
	 * @param string $package_id
	 */
	private function addonInsertUpdate(Package $package, $timestamp, $package_id = NULL) {

		// Security
		if (!$package_id) {
			if ($package->stored()) {
				$package_id = $package->getId();
			} else {
				return;
			}
		}

		$dbAddons = $this->db->get_where('addons', array('package_id' => $package_id))->result_array();

		// List all ids in db
		$dbIds = array();
		foreach ($dbAddons as $dbAddon) {
			$dbIds[] = $dbAddon['id'];
		}

		// Separate all addons for insert
		$toInsertObjects = array();
		$toUpdateObjects = array();

		// Insert update loop
		foreach ($package->getAddons() as $addon) {

			// For inserts
			if ($addon->prepared()) {

				// Check for any duplicates
				$hasNoDuplicate = true;
				foreach ($dbAddons as $dbAddon) {
					if ($addon->getDescription() == $dbAddon['description']
						&& $addon->getUnit() == $dbAddon['unit']
						&& $addon->getCost() == $dbAddon['cost']
						&& $addon->getProfit() == $dbAddon['profit']) {
						$hasNoDuplicate = false;
						break;
					}
				}

				// Separate object
				if ($hasNoDuplicate) {
					$toInsertObjects[] = $addon;
				}
			}

			// For updates
			else if ($addon->stored()) {

				// For update
				if (in_array($addon->getId(), $dbIds)) {
					foreach ($dbAddons as $dbAddon) {
						if ($addon->getDescription() != $dbAddon['description']
							|| $addon->getUnit() != $dbAddon['unit']
							|| $addon->getCost() != $dbAddon['cost']
							|| $addon->getProfit() != $dbAddon['profit']) {
							$toUpdateObjects = $addon;
						}
					}
				}
			}
		}
		// end of insert update loop

		// Insert addons
		$insertAddons = array();
		foreach ($toInsertObjects as $addon) {
			$insertAddons[] = array(
					'package_id'	=> ($package_id) ? $package_id : $package->getId(),
					'description'	=> $addon->getDescription(),
					'unit'			=> $addon->getUnit(),
					'cost'			=> $addon->getCost(),
					'profit'		=> $addon->getProfit(),
					'timestamp'		=> $timestamp,
			);
		}
		if (count($insertAddons) > 0) {
			/* $this->db->insert_batch('addons', $insertAddons); */
		}

		// Update addons
		$updateAddons = array();
		foreach ($toUpdateObjects as $addon) {
			$updateAddons[] = array(
					'package_id'	=> ($package_id) ? $package_id : $package->getId(),
					'description'	=> $addon->getDescription(),
					'unit'			=> $addon->getUnit(),
					'cost'			=> $addon->getCost(),
					'profit'		=> $addon->getProfit(),
					'timestamp'		=> $timestamp,
			);
		}
		if (count($updateAddons) > 0) {
			$this->db->insert_batch('addons', $insertAddons);
		}
	}

	private function hotelRoomRateManager() {

	}

	public function getAndRearrageAllData($package_hotels) {
		$this->load->model('hotel/hotelroomrate');
		$this->load->model('surcharge/surchargestorage');
		$package_hotels_new = array();

		foreach($package_hotels as $package_hotel){
			$roomsurcharge = $roomsurcharges = array();
			$roomperhotel = $roomsurchargesperhotel = array();

			$roomsperhotel = $this->hotelroomrate->getHotelRoomRate_packageHotelId($package_hotel['package_hotel_id']);
			foreach($roomsperhotel as $roomperhotel){
				$roomsurcharge[] = $this->surchargestorage->getSurchargeByType_id('hotelroom', $roomperhotel['id'], TRUE);
			}

			for($i = 0, $n = count($roomsurcharge); $i < $n; $i++) {
				$roomsurcharges = array_merge($roomsurcharges, $roomsurcharge[$i]);
			}

			// die;
			// $roomsurcharges = $this->surchargestorage->getSurchargeByType_id('hotelroom', $package_hotel['package_hotel_id'], TRUE);

			if(!empty($roomsperhotel))
				$package_hotel['roomrates'] = $roomsperhotel;
			if(!empty($roomsurcharges))
				$package_hotel['roomsurcharges'] = $roomsurcharges;

			$package_hotels_new[$package_hotel['package_hotel_id']] = $package_hotel;
		}

		return $package_hotels_new;
	}
	public function getHotelrates($id){
		$this->db->select('package_hotels.id,
						   hotelroom_rates . *');
		$this->db->from('package_hotels');
		$this->db->join('hotelroom_rates','hotelroom_rates.package_hotel_id = package_hotels.id','LEFT');
		$this->db->where('package_hotels.id',$id);
		$this->db->where('hotelroom_rates.status','active');
		$q = $this->db->get()->result_array();
		return $q;
	}

	public function getHotelrates1($id,$date_from,$date_to){
		$this->db->select('package_hotels.id,
						   hotelroom_rates . *');
		$this->db->from('package_hotels');
		$this->db->join('hotelroom_rates','hotelroom_rates.package_hotel_id = package_hotels.id','LEFT');
		$this->db->where('package_hotels.id',$id);
		$this->db->where('hotelroom_rates.status','active');
		$this->db->where('hotelroom_rates.date_from',$date_from);
		$this->db->where('hotelroom_rates.date_to',$date_to);
		$q = $this->db->get()->result_array();
		return $q;
	}

	public function addPermission($arrdata){
	 	$this->db->trans_start();
        $this->db->insert('package_permissions',$arrdata);
 		$this->db->trans_commit();
        if($this->db->trans_status()) return true;
        else return "Permission is not saved";
	}

	public function deletePermission($arrdata){
	 	$this->db->trans_start();
	 	$this->db->where('package_id',$arrdata['package_id']);
	 	$this->db->where('user_id',$arrdata['user_id']);
        $this->db->delete('package_permissions');
 		$this->db->trans_commit();
        if($this->db->trans_status()) return true;
        else return "Permission is not saved";
	}

	public function addPermission_all($arrdata){
	 	$this->db->trans_start();
        $this->db->insert('package_permissions',$arrdata);
 		$this->db->trans_commit();
        if($this->db->trans_status()) return true;
        else return "Permission is not saved";
	}

	public function deletePermission_all($arrdata){
	 	$this->db->trans_start();
	 	$this->db->where('package_id',$arrdata['package_id']);
	 	$this->db->where('user_id',$arrdata['user_id']);
        $this->db->delete('package_permissions');
 		$this->db->trans_commit();
        if($this->db->trans_status()) return true;
        else return "Permission is not saved";
	}

	public function getPackages($where=array(), $limit=0, $offset=0){
		$this->db->select("packages.*, package_duration.date_from, package_duration.date_to, package_description.description as 'packagedescription', countries.country");
		$this->db->from("packages");
		$this->db->join("package_duration"		,"packages.id = package_duration.package_id"	,"LEFT");
		$this->db->join("package_description"	,"packages.id = package_description.package_id"	,"LEFT");
		$this->db->join("package_destinations"	,"packages.id = package_destinations.package_id","LEFT");
		$this->db->join("countries"				,'package_destinations.country_code=countries.code',"LEFT");
		$this->db->order_by("packages.id", "id");
		$this->db->group_by("packages.id");

		if($limit>0) 		$this->db->limit($limit);
		if($offset>0) 		$this->db->offset($offset);
		if(!empty($where)) 	$this->db->where($where);
		// if()
		return $this->db->get();
	}

	public function getPackages_agent($where=array(), $limit=0, $offset=0, $userID=NULL){
		$this->db->select("packages.*, package_duration.date_from, package_duration.date_to, package_description.description as 'packagedescription', countries.country as 'code'");
		$this->db->from("packages");
		$this->db->join("package_duration"		,"packages.id = package_duration.package_id"			,"LEFT");
		$this->db->join("package_description"	,"packages.id = package_description.package_id"			,"LEFT");
		$this->db->join("package_permissions"	,'packages.id = package_permissions.package_id'			,"LEFT");
		$this->db->join("package_destinations"	,'package_destinations.package_id=packages.id'			,"LEFT");
		$this->db->join("countries"				,'package_destinations.country_code=countries.code'		,"LEFT");
		$this->db->join("users"					,'users.id=package_permissions.user_id'					,"LEFT");
		$this->db->where("package_permissions.package_id = packages.id");
		$this->db->where("package_permissions.user_id", $userID);
		$this->db->group_by("packages.id");
		$this->db->order_by("packages.id", "ASC");
		if($limit>0) 		$this->db->limit($limit);
		if($offset>0) 		$this->db->offset($offset);
		return $this->db->get();
	}

	public function filterPackage($post_data=array(),$limit=0,$offset=0){

  $this->db->select("packages.*, package_duration.date_from, package_duration.date_to, package_description.description as 'packagedescription'");
  $this->db->from("packages");
  $this->db->join("package_duration"  ,"packages.id = package_duration.package_id" ,"left");
  $this->db->join("package_description" ,"packages.id = package_description.package_id" ,"left");
  $this->db->join("package_permissions" ,"packages.id = package_permissions.package_id"   ,"LEFT");
  $this->db->join("package_destinations" ,"packages.id = package_destinations.package_id","left");
  $this->db->join('users'      ,'users.id=package_permissions.user_id'     ,'LEFT');
  $this->db->order_by("packages.id", "id");
  $this->db->group_by("packages.id");
  if($limit>0)  $this->db->limit($limit);
  if($offset>0)  $this->db->offset($offset);
   foreach($post_data as $key => $post){
    if (trim($post)!=''){
     if ($key == 'hotel_choice'){
     $this->db->join("package_hotels" ,"packages.id = package_hotels.package_id" ,"left");
     $this->db->join("hotels"   ,"package_hotels.hotel_id = hotels.id"  ,"left");
      $this->db->where('hotels.id', $post);
     }
     if($key == 'destination') {
      if($this->session->userdata('type') !== 'agent') {
        $this->db->where('package_destinations.country_code', $post);
        $this->db->where('package_destinations.status', 'active');
      }else{
      	$this->db->where('package_permissions.package_id = packages.id');
      	$this->db->where('package_permissions.user_id'  ,$this->session->userdata('id'));
        $this->db->where('package_destinations.country_code', $post);
        $this->db->where('package_destinations.status', 'active');

      }
     }
     if ($key == 'depart_date')
      $this->db->where(array('package_duration.date_from <=' => $post));
     if ($key == 'return_date')
      $this->db->where(array('package_duration.date_from <=' => $post));
    }
   }

  return $this->db->get();
 }

	public function filterPackageFrontPages($post_data=array()){

		$this->db->select("packages.*, package_duration.date_from, package_duration.date_to, package_description.description as 'packagedescription'");
		$this->db->from("packages");
		$this->db->join("package_duration"		,"packages.id = package_duration.package_id"	,"left");
		$this->db->join("package_description"	,"packages.id = package_description.package_id"	,"left");
		$this->db->join("package_destinations"	,"packages.id = package_destinations.package_id","left");
		$this->db->order_by("packages.id", "id");
		$this->db->group_by("packages.id");
	 	foreach($post_data as $key => $post){
	 		if (trim($post)!=''){
	 			if ($key == 'hotel_choice'){
					$this->db->join("package_hotels" ,"packages.id = package_hotels.package_id"	,"left");
					$this->db->join("hotels"		 ,"package_hotels.hotel_id = hotels.id"		,"left");
	 				$this->db->where('hotels.id', $post);
	 			}

	 			if ($key == 'destination') {
	 				$this->db->where('package_destinations.country_code', $post);
	 				$this->db->where('package_destinations.status', 'active');
	 			}
	 			if ($key == 'depart_date')
	 				$this->db->where(array('package_duration.date_from <=' => $post));
	 			if ($key == 'return_date')
	 				$this->db->where(array('package_duration.date_from <=' => $post));
	 		}
	 	}

		return $this->db->get();
	}

	public function filterPackage_agent($userID=NULL, $post_data=array(), $limit=0, $offset=0){
		$this->db->select("packages.*, package_duration.date_from, package_duration.date_to, package_description.description as 'packagedescription', countries.country");
		$this->db->from("packages");
		$this->db->join("package_duration"		,"packages.id = package_duration.package_id"			,"LEFT");
		$this->db->join("package_description"	,"packages.id = package_description.package_id"			,"LEFT");
		$this->db->join("package_permissions"	,"packages.id = package_permissions.package_id"			,"LEFT");
		$this->db->join("package_destinations"	,"package_destinations.package_id=packages.id"			,"LEFT");
		$this->db->join("countries"				,"package_destinations.country_code=countries.code"		,"LEFT");
		$this->db->join("users"					,"users.id=package_permissions.user_id"					,"LEFT");
		$this->db->join("package_hotels" 		,"packages.id = package_hotels.package_id"				,"LEFT");
		$this->db->join("hotels"		 		,"package_hotels.hotel_id = hotels.id"					,"LEFT");
		//KIRSTIN HERE, CHANGE LATER


	 	foreach ($post_data as $key => $post) {
	 		if (trim($post)!='') {

	 			if ($key == 'hotel_choice') {
	 				$this->db->where('hotels.id', $post);
	 			}

	 			if ($key == 'destination') {
	 				$this->db->where('package_destinations.country_code', $post);
	 				$this->db->where('package_destinations.status', 'active');
	 			}

	 			if ($key == 'depart_date')
	 				$this->db->where('package_duration.date_from <=',$post);

	 			if ($key == 'return_date')
	 				$this->db->where('package_duration.date_from <=',$post);

	 		}
	 	}
		$this->db->where("package_permissions.package_id = packages.id");
		$this->db->where("package_permissions.user_id", $userID);
		$this->db->order_by("packages.id", "id");
	 	$this->db->group_by('packages.id');
		//echo $this->db->get()->num_rows; die;
		if($limit>0) 		$this->db->limit($limit);
		if($offset>0) 		$this->db->offset($offset);
		return $this->db->get();
	}

	public function filterPackageAgentFrontPages($userID=NULL, $post_data=array()){
		$this->db->select("packages.*, package_duration.date_from, package_duration.date_to, package_description.description as 'packagedescription', countries.country");
		$this->db->from("packages");
		$this->db->join("package_duration"		,"packages.id = package_duration.package_id"			,"LEFT");
		$this->db->join("package_description"	,"packages.id = package_description.package_id"			,"LEFT");
		$this->db->join("package_permissions"	,"packages.id = package_permissions.package_id"			,"LEFT");
		$this->db->join("package_destinations"	,"package_destinations.package_id=packages.id"			,"LEFT");
		$this->db->join("countries"				,"package_destinations.country_code=countries.code"		,"LEFT");
		$this->db->join("users"					,"users.id=package_permissions.user_id"					,"LEFT");
		$this->db->join("package_hotels" 		,"packages.id = package_hotels.package_id"				,"LEFT");
		$this->db->join("hotels"		 		,"package_hotels.hotel_id = hotels.id"					,"LEFT");
		//KIRSTIN HERE, CHANGE LATER


	 	foreach ($post_data as $key => $post) {
	 		if (trim($post)!='') {

	 			if ($key == 'hotel_choice') {
	 				$this->db->where('hotels.id', $post);
	 			}

	 			if ($key == 'destination') {
	 				$this->db->where('package_destinations.country_code', $post);
	 				$this->db->where('package_destinations.status', 'active');
	 			}

	 			if ($key == 'depart_date')
	 				$this->db->where('package_duration.date_from <=',$post);

	 			if ($key == 'return_date')
	 				$this->db->where('package_duration.date_from <=',$post);

	 		}
	 	}
		$this->db->where("package_permissions.package_id = packages.id");
		$this->db->where("package_permissions.user_id", $userID);
		$this->db->order_by("packages.id", "id");
	 	$this->db->group_by('packages.id');
		return $this->db->get();
	}

	public function getRefNum_package(){
		return $this->db->get_where('reference_number', array('id'=>2))->row();
	}

	public function setRefNum_package(){
		$this->db->set('code','code+1',false);
        $this->db->where('id',2);
        $this->db->update('reference_number');
	}

	/*
	*lester padul code
	*/
	public function getDestinations($package_id){
		$this->db->select("*");
		$this->db->from("package_destinations");
		$this->db->join("countries","countries.code=package_destinations.country_code","left");
		$this->db->where("package_id",$package_id);
		$this->db->where("package_destinations.status","active");
		$q = $this->db->get()->result_array();
		return $q;
	}
	/*
	*end of lester padul code
	*/

	/*
	*lester padul code
	*/
	public function getPackagesFrontPages($package_id){
		$this->db->select("packages.*,package_duration.date_from,package_duration.date_to");
		$this->db->from("packages");
		$this->db->join('package_duration','package_duration.package_id=packages.id');
		$this->db->where('set_image','1');
		$q = $this->db->get()->result_array();
		return $q;
	}
	/*
	*end of lester padul code
	*/

	public function getDestinationPackages($destination_code){
		$date = date('Y-m-d');
		$this->db->select("packages.*");
		$this->db->from("packages");
		$this->db->join('package_destinations','packages.id=package_destinations.package_id');
		$this->db->join('package_duration','package_duration.package_id=packages.id');
		$this->db->where('package_destinations.country_code',$destination_code);
		$this->db->where('packages.status','active');
		$this->db->where('package_duration.date_from <=',$date);
		$this->db->where('package_duration.date_to >=',$date);
		$q = $this->db->get()->result();
		return $q;

	}

}