<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

get_instance()->load->iface('storeiteminterface');

/**
 * @author Kirstin Rubica
 *
 */
class Destination extends CI_Model implements StoreItemInterface{


	/**
	 * @var string
	 */
	private $id;

	/**
	 * @var string
	 */
	private $package;

	/**
	 * @var string
	 */
	private $country_code;

	/**
	 * @var string
	 */
	private $nights;

	/**
	 * @var string
	 */
	private $status;

	/**
	 * @var string
	 */
	private $prevState;

	/**
	 *
	 */
	public function __construct() {
		parent::__construct();
	}

	public function setId($obj, $id) {
		if ($obj instanceof Destination) {
			setprivateid($obj, 'Destination', $id);
		}
	}

	/**
	 *
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @return Package
	 */
	public function getPackage() {
		return $this->package;
	}

	/**
	 * @param Package $package
	 */
	public function setPackage(Package $package) {
		if ($package instanceof Package) {
			$this->package = $package;
		}
	}

	/**
	 *
	 */
	public function getCountryCode($code = false) {
		if($code){
			$this->db->select('country');
			$cntry = $this->db->get_where('countries', array('code'=>$this->country))->row();
			return $cntry->country;
		}else return $this->country_code;
	}

	/**
	 * @param string $description
	 */
	public function setCountryCode($country_code) {
		$this->country_code = $this->checkString($country_code);
	}

	/**
	 *
	 */
	public function getNights() {
		return $this->nights;
	}

	/**
	 * @param string $nights
	 */
	public function setNights($nights) {
		$this->nights = ($this->isInteger($nights)) ? $nights : null;
	}

	/**
	 *
	 */
	public function getStatus() {
		return $this->status;
	}

	/**
	 * @param string $status
	 */
	public function setStatus($status) {
		$this->status = $this->checkString($status);
	}


	/* (non-PHPdoc)
	 * @see StoreItemInterface::prepared()
	*/
	public function prepared() {
		// TODO Auto-generated method stub
		if ($this->getPackage()
			&& $this->getCountryCode()
			&& $this->getNights()
			&& is_null($this->id)) {
			return true;
		}
		return false;
	}

	/* (non-PHPdoc)
	 * @see StoreItemInterface::stored()
	*/
	public function stored() {
		// TODO Auto-generated method stub
		return ($this->id) ? true : false;
	}

	/**
	 * @param string $input
	 * @return boolean
	 */
	private function isInteger($input) {
		return (ctype_digit(strval($input)));
	}

	/**
	 * @param string $input
	 * @return Ambigous <NULL, string>
	 */
	private function checkString($input) {
		return (is_string($input)) ? $input : null;
	}

	public function batchStore(array $objArray) {
		foreach ($objArray as $obj) {
			if ($obj instanceof Destination)
				$this->store($obj);
		}
	}

	/* (non-PHPdoc)
	 * @see CaptureStateInterface::captureCurrentState()
	 */
	public function captureCurrentState() {
		$this->prevState = clone $this;
	}

	public function prevState() {
		return (!empty($this->prevState)) ? $this->prevState : false;
	}

	public function store($destination) {
		// Check if Destination Object
		if (!$destination instanceof Destination) {
			return;
		}

		//if mandatory fields have values
		if ($destination->prepared()) {

			$this->db->insert('package_destinations', array(
				'package_id' 	=> $destination->getPackage()->getId(),
				'country_code' 	=> $destination->getCountryCode(),
				'nights' 		=> $destination->getNights(),
				'status' 		=> $destination->getStatus(),
			));

			$insert_id = $this->db->insert_id();
			$destination->captureCurrentState();
			$this->setId($destination, $insert_id);
		} else if ($destination->stored()) {

			/**
			* to compare updated values to its original state.
			* if there are any changes, a new key and value
			* is inserted to updateDestination
			*
			* change prevdestination here because the previous prev
			* destination doesn't capture the changes made when
			* setting the destinations to inactive
			**/

			// $prevDestination   = $destination->prevState();
			$prevDestination   = $this->getById($destination->getId());
			$updateDestination = array();

			// $this->db->get_where()
			if (($destination->getCountryCode() != $prevDestination->getCountryCode()) && ($destination->getCountryCode())) {
				$updateDestination['country_code'] = $destination->getCountryCode();
			}
			if (($destination->getNights() != $prevDestination->getNights()) && ($destination->getNights())) {
				$updateDestination['nights'] = $destination->getNights();
			}
			if (($destination->getStatus() != $prevDestination->getStatus()) && ($destination->getStatus())) {
				$updateDestination['status'] = $destination->getStatus();
			}

			if (count($updateDestination) > 0) {
				$this->db->where('id', $destination->getId());
				$this->db->update('package_destinations', $updateDestination);
			}
		}
	}

	public function getById($id){
		$result = $this->db->get_where('package_destinations', array('id' => $id))->row();
		if (count($result) == 1) {
			$this->load->model('package/packagestorage');
			$package = $this->packagestorage->getById($result->package_id);

			$destination = new Destination();
			$destination->setCountryCode($result->country_code);
			$destination->setPackage($package[0]);
			$destination->setNights($result->nights);
			$destination->setStatus($result->status);
			$this->setId($destination, $id);
			$destination->captureCurrentState();

			return $destination;
		}
		return false;
	}

	public function setDestinationsInactive($package_id) {
		$ids = $this->checkForDestination($package_id);
		if(!empty($ids))
			$this->db->where_not_in('package_destinations.id', $ids);

		$this->db->where('package_destinations.package_id', $package_id);
		$this->db->update('package_destinations', array('status'=>'inactive'));
	}

	public function checkForDestination($package_id){
		// $this->db->distinct();
		$this->db->select('package_destinations.id');
	  	$this->db->from('package_hotels');
	  	$this->db->join('hotels','hotels.id=package_hotels.hotel_id','LEFT');
	  	$this->db->join('package_destinations','package_destinations.package_id=package_hotels.package_id','LEFT');
	  	$this->db->where("`hotels`.`country_code` = `package_destinations`.`country_code`",'',false);
	  	$this->db->where('package_destinations.package_id',$package_id);
		$res_arr = $this->db->get();

		$return = array();
		if ($res_arr->num_rows() > 0) {
			foreach ($res_arr->result_array() as $result) {
				$return[] = $result['id'];
			}
		}
		return $return;
	}

	public function getActiveDestinations(){
		$this->db->select('countries.code, countries.country');
		$this->db->from('package_destinations');
		$this->db->join('packages' ,'package_destinations.package_id = packages.id','LEFT');
		$this->db->join('countries','package_destinations.country_code = countries.code','LEFT');
		$this->db->group_by('country');
		$this->db->where('package_destinations.status','active');

		return $this->db->get()->result_array();
	}



	public function getActiveBookingDestinations($userID_agent){
		$this->db->select('package_destinations.country_code AS code,
							package_destinations.status,
							countries.country');
		$this->db->from('packages');
		$this->db->join('package_duration'			,'packages.id = package_duration.package_id'			,'LEFT');
		$this->db->join('package_destinations'		,'packages.id = package_destinations.package_id'		,'LEFT');
		$this->db->join('package_permissions'		,'packages.id = package_permissions.package_id'			,'LEFT');
		$this->db->join('users'						,'users.id=package_permissions.user_id'					,'LEFT');
		$this->db->join('countries'					,'package_destinations.country_code=countries.code'		,'LEFT');
		$this->db->where('package_permissions.package_id = packages.id');
		$this->db->where('package_permissions.user_id'		,$userID_agent);
		$this->db->where('package_destinations.status'		,'active');
		$this->db->group_by('countries.country');
		$this->db->order_by('countries.country'				,'ASC');

		return $this->db->get()->result_array();
	}

	public function getActiveBookingDestinationsFrontPages(){
		$this->db->select('package_destinations.country_code AS code,
							package_destinations.status,
							countries.country');
		$this->db->from('packages');
		$this->db->join('package_duration'			,'packages.id = package_duration.package_id'			,'LEFT');
		$this->db->join('package_destinations'		,'packages.id = package_destinations.package_id'		,'LEFT');
		$this->db->join('package_permissions'		,'packages.id = package_permissions.package_id'			,'LEFT');
		$this->db->join('users'						,'users.id=package_permissions.user_id'					,'LEFT');
		$this->db->join('countries'					,'package_destinations.country_code=countries.code'		,'LEFT');
		$this->db->where('package_permissions.package_id = packages.id');
		$this->db->where('package_destinations.status'		,'active');
		$this->db->group_by('countries.country');
		$this->db->order_by('countries.country'				,'ASC');

		return $this->db->get()->result_array();
	}

	public function getActiveBookingHotels($userID_agent){
		$this->db->select('package_hotels.package_id, hotels.id, hotels.name');
		$this->db->from('packages');
		$this->db->join('package_hotels'			,'packages.id = package_hotels.package_id'			,'LEFT');
		$this->db->join('hotels'					,'package_hotels.hotel_id = hotels.id'				,'LEFT');
		$this->db->join('package_permissions'		,'packages.id = package_permissions.package_id'		,'LEFT');
		$this->db->join('users'						,'users.id=package_permissions.user_id'				,'LEFT');
		$this->db->where('package_permissions.package_id = packages.id');
		$this->db->where('package_permissions.user_id'		,$userID_agent);
		$this->db->where('hotels.status'		,'active');
		$this->db->group_by('hotels.name');
		$this->db->order_by('hotels.name'				,'ASC');

		return $this->db->get()->result_array();
	}
}