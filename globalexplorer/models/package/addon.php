<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

get_instance()->load->iface('storeiteminterface');

/**
 * @author Joshua Paylaga
 *
 */
class Addon extends CI_Model implements StoreItemInterface{
	
	/**
	 * @var string
	 */
	const RATE_PAX_NIGHT = 'pax/night';
	
	/**
	 * @var string
	 */
	const RATE_ADULT_NIGHT = 'adult/night';
	
	/**
	 * @var string
	 */
	const RATE_CHILD_NIGHT = 'child/night';
	
	/**
	 * @var string
	 */
	const RATE_INFANT_NIGHT = 'infant/night';
	
	/**
	 * @var string
	 */
	const RATE_ROOM_NIGHT = 'room/night';
	
	/**
	 * @var string
	 */
	const RATE_PAX_TRIP = 'pax/trip';
	
	/**
	 * @var string
	 */
	const RATE_ADULT_TRIP = 'adult/trip';
	
	/**
	 * @var string
	 */
	const RATE_CHILD_TRIP = 'child/trip';
	
	/**
	 * @var string
	 */
	const RATE_INFANT_TRIP = 'infant/trip';
	
	/**
	 * @var string
	 */
	const RATE_ROOM_TRIP = 'room/trip';
	
	/**
	 * @var string
	 */
	private $id;
	
	/**
	 * @var string
	 */
	private $package;
	
	/**
	 * @var string
	 */
	private $description;
	
	/**
	 * @var string
	 */
	private $unit;
	
	/**
	 * @var string
	 */
	private $cost;
	
	/**
	 * @var string
	 */
	private $profit;
	
	/**
	 * 
	 */
	public function __construct() {
		parent::__construct();
	}
	
	/**
	 * 
	 */
	public function getId() {
		return $this->id;
	}
	
	/**
	 * @return Package
	 */
	public function getPackage() {
		return $this->package;
	}
	
	/**
	 * @param Package $package
	 */
	public function setPackage(Package $package) {
		if ($package instanceof Package) {
			$this->package = $package;
		}
	}
	
	/**
	 * 
	 */
	public function getDescription() {
		return $this->description;
	}
	
	/**
	 * @param string $description
	 */
	public function setDescription($description) {
		$this->description = $this->checkString($description);
	}
	
	/**
	 * 
	 */
	public function getUnit() {
		return $this->unit;
	}
	
	/**
	 * @param string $unit
	 */
	public function setUnit($unit) {
		$this->unit = $this->checkString($unit);
	}
	
	/**
	 * 
	 */
	public function getCost() {
		return $this->cost;
	}
	
	/**
	 * @param string $cost
	 */
	public function setCost($cost) {
		$this->cost = $this->checkPrice($cost);
	}
	
	/**
	 * @return Ambigous <NULL, mixed>
	 */
	public function getProfit() {
		return $this->profit;
	}
	
	/**
	 * @param string $profit
	 */
	public function setProfit($profit) {
		$this->profit = $this->checkPrice($profit);
	}
	
	/* (non-PHPdoc)
	 * @see StoreItemInterface::prepared()
	*/
	public function prepared() {
		// TODO Auto-generated method stub
		if ($this->getPackage()
			&& $this->getDescription()
			&& $this->getUnit()
			&& $this->getCost()
			&& $this->getProfit()
			&& is_null($this->id)) {
			return true;
		}
		return false;
	}
	
	/* (non-PHPdoc)
	 * @see StoreItemInterface::stored()
	*/
	public function stored() {
		// TODO Auto-generated method stub
		return ($this->id) ? true : false;
	}
	
	/**
	 * @param string $input
	 * @return boolean
	 */
	private function isInteger($input) {
		return (ctype_digit(strval($input)));
	}
	
	/**
	 * @param string $price
	 * @return NULL|mixed
	 */
	private function checkPrice($price) {
		if (!is_numeric($price) || $price < 0 || strlen(substr(strrchr($price, "."), 1)) > 2)
			return null;
		else
			return str_replace(',', '', number_format($price, 2));
	}
	
	/**
	 * @param string $input
	 * @return Ambigous <NULL, string>
	 */
	private function checkString($input) {
		return (is_string($input)) ? $input : null;
	}

	public function storepackageaddons($addon){
		//store
		if(!isset($addon->id)){
			$result = $this->db->get_where('addons', array('package_id'=>$addon['package_id'],'description'=>$addon['description']));
	        $inDatabase = (bool)$result->num_rows();
	        if (!$inDatabase){
	    	 	$this->db->trans_start();
	    	 	$addon['timestamp'] = time();
		            $this->db->insert('addons', $addon);
		            $id = $this->db->insert_id();
	     		$this->db->trans_commit();
	            if($this->db->trans_status()) return $id;
	            else return "Add On is not saved";
	        } else return "This Add On has already been added to this package";
		}
		//update
		else{
			$addonToUpdate = array();
    		$prev = $addon->prevState;

			if (($addon->description != $prev->description) && ($addon->description)) {
				$addonToUpdate['description'] = $addon->description;
			}

			if (($addon->unit != $prev->unit) && ($addon->unit)) {
				$addonToUpdate['unit'] = $addon->unit;
			}

			if (($addon->cost != $prev->cost) && ($addon->cost)) {
				$addonToUpdate['cost'] = $addon->cost;
			}

			if (($addon->profit != $prev->profit) && ($addon->profit)) {
				$addonToUpdate['profit'] = $addon->profit;
			}

			if (count($addonToUpdate) > 0) {
				$this->db->where('id', $addon->id);
				$this->db->update('addons', $addonToUpdate);
			}		

		}
	}

	public function getAddons_packageId($package_id){
		$results = $this->db->get_where('addons', array('package_id'=>$package_id, 'status !='=>'inactive'));
		return ($results->num_rows > 0) ? $results->result_array() : array();
	}

	public function getAddons_Id($addon_id){
		$data = array();
		$result = $this->db->get_where('addons', array('id'=>$addon_id))->row();
		return $result;
	}

	public function getAddons_ID_QTY($addon_id,$booking_id){
		return $this->db->get_where("applied_addons", array("booking_id"=>$booking_id, "addon_id"=>$addon_id))->row();
	}

	public function processAddons($hotelrooms=array()){	
		$prices = array();
		$total = $cost = $profit = 0;
		if(isset($hotelrooms['addon_quantity']) && !empty($hotelrooms['addon_quantity'])){
			foreach($hotelrooms['addon_quantity'] as $k => $qty){
				$id = $hotelrooms['addon_id'][$k];
				if($qty != '' && $id != ''){
					$addon = $this->getAddons_Id($id);
					if(!empty($addon)){
						$price = $addon->cost + $addon->profit;
						$title = ucfirst($addon->description);
						$unit = ucwords(str_replace('/', ' / ', $addon->unit));
						$addon_price = $price * $qty;
						$addon_cost = $addon->cost * $qty;
						$addon_profit = $addon->profit * $qty;
						
						$prices[] = array(
							'id'=>$id
							,'title'=>$title
							,'price' => $price
							,'unit'=>$unit
							,'quantity'=>$qty
							,'computed' => $addon_price
						);
						
						$total += $addon_price;
						$cost += $addon_cost;
						$profit += $addon_profit;
					}
				}
			}
			return array('details'=>$prices,'total'=>$total,'cost'=>$cost,'profit'=>$profit);
		} else return array();

	}
}