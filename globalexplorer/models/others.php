<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Others extends CI_Model{
	
	public function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function getDestinationByCode($code){
		if(!$code) return false;

		$result = $this->db->get_where('countries', array('code'=>$code));

		if ($result->num_rows() != 0)
			return $result->row();
		else return false;

	}
	public function storeDestination($data, $code = ''){
		if ($code == '') {

			$return = $this->db->get_where('countries', array('country'=>$data['country'],'status'=>'active'));
			if ($return->num_rows() > 0) return "Country already exists in database";
			
			$return = $this->db->get_where('countries', array('code'=>$data['code'],'status'=>'active'));
			if ($return->num_rows() > 0) return "Code already exists in database";

			$return = $this->db->get_where('countries', 
				array(
					'code'	 =>$data['code'],
				  	'country'=>$data['country'],
				    'status' =>'inactive'
				)
			);
			if ($return->num_rows() > 0) {
				return "country and code already exist in database. Please activate on next tab";
			}
			//if not all of the above heehee, store
			$this->db->insert('countries', $data);
			return $data['code'];
		} else {
			$this->db->where('code', $code);
			if($data == 'deactivate'){
				$this->db->update('status', $data);
			}else if($data == 'active'){
				$this->db->update('status', $data);
			}else
				$this->db->update('countries', $data);
			return $code['code'];
		}
	}

	public function storeDestinationAirportcode($data, $code = ''){
		if ($code == '') {

			$return = $this->db->get_where('airportcodes', array('name'=>$data['name'],'status'=>'active'));
			if ($return->num_rows() > 0) return "name already exists in database";
			
			$return = $this->db->get_where('airportcodes', array('code'=>$data['code'],'status'=>'active'));
			if ($return->num_rows() > 0) return "Code already exists in database";

			$return = $this->db->get_where('airportcodes', 
				array(
					'code'	 =>$data['code'],
				  	'name'=>$data['name'],
				    'status' =>'inactive'
				)
			);
			if ($return->num_rows() > 0) {
				return "name and code already exist in database. Please activate on next tab";
			}
			//if not all of the above heehee, store
			$this->db->insert('airportcodes', $data);
			return $data['id'];
		} else {
			$this->db->where('id', $code);
			if($data == 'deactivate'){
				$this->db->update('status', $data);
			}else if($data == 'active'){
				$this->db->update('status', $data);
			}else
				$this->db->update('airportcodes', $data);
			return $code['id'];
		}
	}

	public function getAirportCodeById($id){
		if(!$id) return false;

		$result = $this->db->get_where('airportcodes', array('id'=>$id));

		if ($result->num_rows() != 0)
			return $result->row();
		else return false;

	}

	public function storeAirportcode($data){

		$return = $this->db->get_where('airportcodes', array('name'=>$data['name'],'status'=>'active'));
		if ($return->num_rows() > 0) return "Airport/City already exists in database";
		
		$return = $this->db->get_where('airportcodes', array('code'=>$data['code'],'status'=>'active'));
		if ($return->num_rows() > 0) return "Code already exists in database";

		$return = $this->db->get_where('airportcodes', 
			array(
				'code'	 =>$data['code'],
			  	'name'	 =>$data['name'],
			    'status' =>'inactive'
			)
		);
		if ($return->num_rows() > 0) {
			return "Airport/City and Code already exist in database. Please activate on next tab";
		}
		//if not all of the above heehee, store
		$this->db->insert('airportcodes', $data);
		return $this->db->insert_id();
	}

}
