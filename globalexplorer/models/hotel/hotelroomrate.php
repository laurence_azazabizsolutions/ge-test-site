<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class HotelRoomRate extends CI_Model {
	
	/**
	 * @var string
	 */
	const ROOM_SINGLE = 'single';
	
	/**
	 * @var string
	 */
	const ROOM_TWIN	= 'twin';
	
	/**
	 * @var string
	 */
	const ROOM_TRIPLE = 'triple';

	/**
	 * @var string
	 */
	const ROOM_CHILD_H_TWIN = 'child_half_twin';

	/**
	 * @var string
	 */
	const ROOM_CHILD_W_BED = 'child_with_bed';
	
	/**
	 * @var string
	 */
	const ROOM_CHILD_WO_BED = 'child_without_bed';
	
	/**
	 * @var string
	 */
	const ROOM_EXT_SINGLE = 'extension_(single)';
	
	/**
	 * @var string
	 */
	const ROOM_EXT_TWIN = 'extension_(twin)';
	
	/**
	 * @var string
	 */
	const ROOM_EXT_TRIPLE = 'extension_(triple)';
	
	/**
	 * @var string
	 */
	const ROOM_EXTRA_BED = 'extra_bed';
	
	/**
	 * @var string
	 */
	const ROOM_BREAKFAST = 'breakfast';
	
	/**
	 * @var string
	 */
	const ROOM_EXT = 'extension';
	
	/**
	 * @var string
	 */
	const RATE_PAX_NIGHT = 'pax/night';
	
	/**
	 * @var string
	 */
	const RATE_ADULT_NIGHT = 'adult/night';
	
	/**
	 * @var string
	 */
	const RATE_CHILD_NIGHT = 'child/night';
	
	/**
	 * @var string
	 */
	const RATE_INFANT_NIGHT = 'infant/night';
	
	/**
	 * @var string
	 */
	const RATE_ROOM_NIGHT = 'room/night';
	
	/**
	 * @var string
	 */
	const RATE_PAX_TRIP = 'pax/trip';
	
	/**
	 * @var string
	 */
	const RATE_ADULT_TRIP = 'adult/trip';
	
	/**
	 * @var string
	 */
	const RATE_CHILD_TRIP = 'child/trip';
	
	/**
	 * @var string
	 */
	const RATE_INFANT_TRIP = 'infant/trip';
	
	/**
	 * @var string
	 */
	const RATE_ROOM_TRIP = 'room/trip';
	
	private $id;
	
	private $roomRateMap;
	
	private $package;
	
	private $hotel;
	
	public function __construct() {
		$this->roomRateMap = array(
			$this::ROOM_SINGLE 			=> NULL,
			$this::ROOM_TWIN 			=> NULL,
			$this::ROOM_TRIPLE 			=> NULL,
			$this::ROOM_CHILD_H_TWIN 	=> NULL,
			$this::ROOM_CHILD_W_BED 	=> NULL,
			$this::ROOM_CHILD_WO_BED 	=> NULL,
			$this::ROOM_EXT_SINGLE 		=> NULL,
			$this::ROOM_EXT_TWIN 		=> NULL,
			$this::ROOM_EXT_TRIPLE 		=> NULL,
			$this::ROOM_EXTRA_BED 		=> NULL,
			$this::ROOM_BREAKFAST 		=> NULL,
			$this::ROOM_EXT 			=> NULL,
		);
	}
	
	public function setPackage(Package $package) {
		if ($package instanceof Package) {
			$this->package = $package;
		}
	}
	
	public function getPackage() {
		return $this->package;
	}
	
	public function setHotel(Hotel $hotel) {
		if ($hotel instanceof Hotel) {
			if ($hotel->stored()) {
				$this->hotel = $hotel;
			}
		}
	}
	
	public function getHotel() {
		return $this->hotel;
	}
	
	public function defineRateByRoom($roomType, $roomRateType, $cost, $profit, $pax) {
		if (array_key_exists($roomType, $this->roomRateMap)) {
			if ($roomRateType === $this::RATE_ADULT_NIGHT ||
				$roomRateType === $this::RATE_ADULT_TRIP ||
				$roomRateType === $this::RATE_CHILD_NIGHT ||
				$roomRateType === $this::RATE_CHILD_TRIP ||
				$roomRateType === $this::RATE_INFANT_NIGHT ||
				$roomRateType === $this::RATE_INFANT_TRIP ||
				$roomRateType === $this::RATE_PAX_NIGHT ||
				$roomRateType === $this::RATE_PAX_TRIP ||
				$roomRateType === $this::RATE_ROOM_NIGHT ||
				$roomRateType === $this::RATE_ROOM_TRIP) {
				if (is_numeric($cost) && is_numeric($profit) && is_numeric($pax)) {
					$this->roomRateMap[$roomType] = array(
							'rate_type'	=> $roomRateType,
							'cost' 		=> (string) $cost,
							'profit' 	=> (string) $profit,
							'pax' 		=> (string) $pax,
					);
				}
			}
		}
	}
	
	public function getRateByRoom($roomType) {
		if (array_key_exists($roomType, $this->roomRateMap)) {
			return $this->roomRateMap[$roomType];
		}
		return false;
	}
	/*
	*
	*Start 
	*This function storehotelroomrate insert into database 
	*
	*/
	public function storehotelroomrate($data){
    	if(isset($data->id) && $data->id != 0){

    		$roomToUpdate = array();
    		$prev = $data->prevState;
    		
    		if (($data->room_type != $prev->room_type) && ($data->room_type)) {
				$roomToUpdate['room_type'] = $data->room_type;
			}

			if (($data->description != $prev->description) && ($data->description)) {
				$roomToUpdate['description'] = $data->description;
			}

			if (($data->rate_type != $prev->rate_type) && ($data->rate_type)) {
				$roomToUpdate['rate_type'] = $data->rate_type;
			}

			if (($data->pax != $prev->pax) && ($data->pax)) {
				$roomToUpdate['pax'] = $data->pax;
			}

			if (($data->cost != $prev->cost) && ($data->cost)) {
				$roomToUpdate['cost'] = $data->cost;
			}

			if (($data->profit != $prev->profit) && ($data->profit)) {
				$roomToUpdate['profit'] = $data->profit;
			}

			if (($data->ext_cost != $prev->ext_cost) && ($data->ext_cost)) {
				$roomToUpdate['ext_cost'] = $data->ext_cost;
			}

			if (($data->ext_profit != $prev->ext_profit) && ($data->ext_profit)) {
				$roomToUpdate['ext_profit'] = $data->ext_profit;
			}

			if (count($roomToUpdate) > 0) {
				$this->db->where('id', $data->id);
				$this->db->update('hotelroom_rates', $roomToUpdate);
			}
    	}else{
	    	$return = "Hotel room rate is not saved";

    	 	$this->db->trans_start();
            	$this->db->insert('hotelroom_rates', $data);
            	$id = $this->db->insert_id();
     		$this->db->trans_commit();
        	
        	if($this->db->trans_status()) $return = $id;
        	
        	return $return;
    	}
	}
	/*
	*
	*End 
	*This function storehotelroomrate insert into database 
	*
	*/
	public function getHotelRoomRate_packageHotelIdAgent($package_id, $package_hotel_id, $separate = FALSE){
        $data = array();
        //$result = $this->db->get_where('hotelroom_rates', array('package_hotel_id'=>$package_hotel_id, "status"=>"active"));
	    $this->db->select('*');
		$this->db->from('hotelroom_rates');
		$this->db->join('package_hotels', 'package_hotels.id = hotelroom_rates.package_hotel_id');
		$this->db->where(array('package_hotels.hotel_id'=>$package_hotel_id,'package_hotels.package_id'=>$package_id, "hotelroom_rates.status"=>"active"));

		$result = $this->db->get();

        if($result->num_rows > 0){
	        if($separate){
	        	foreach($result->result_array() as $roomrate){
	        		if (strpos($roomrate['room_type'], 'extension') === FALSE) 
						$data['standard'][] = $roomrate;
					else $data['ex'][] = $roomrate;
	        	}
	        } else $data = $result->result_array();
		}


    	return $data;
	}

	public function getHotelRoomRate_packageHotelIdAgent1($package_id, $package_hotel_id, $separate = FALSE){
        $data = array();
        //$result = $this->db->get_where('hotelroom_rates', array('package_hotel_id'=>$package_hotel_id, "status"=>"active"));
	    $this->db->select('*, hotelroom_rates.id as hr_id');
		$this->db->from('hotelroom_rates');
		$this->db->join('package_hotels', 'package_hotels.id = hotelroom_rates.package_hotel_id');
		$this->db->where(array('package_hotels.hotel_id'=>$package_hotel_id,'package_hotels.package_id'=>$package_id, "hotelroom_rates.status"=>"active"));
		$this->db->where('hotelroom_rates.date_from','0000-00-00');
		$this->db->where('hotelroom_rates.date_to','0000-00-00');


		$result = $this->db->get();

        if($result->num_rows > 0){
	        if($separate){
	        	foreach($result->result_array() as $roomrate){
	        		if (strpos($roomrate['room_type'], 'extension') === FALSE) 
						$data['standard'][] = $roomrate;
					else $data['ex'][] = $roomrate;
	        	}
	        } else $data = $result->result_array();
		}
    	return $data;
	}

	public function getHotelRoomRateRange($package_hotel_id,$room_type,$description,$rate_type,$depart_value){
        $data = array();
        //$result = $this->db->get_where('hotelroom_rates', array('package_hotel_id'=>$package_hotel_id, "status"=>"active"));
	    $this->db->select('*');
		$this->db->from('hotelroom_rates');
		$this->db->where('package_hotel_id',$package_hotel_id);
		$this->db->where('room_type',$room_type);
		$this->db->where('description',$description);
		$this->db->where('rate_type',$rate_type);
		$this->db->where('date_from <=',$depart_value);
		$this->db->where('date_to >=',$depart_value);

		$result = $this->db->get()->result_array();
		return $result;
	}


	public function getHotelRoomRateRange1($package_hotel_id,$room_type,$description,$rate_type){
       
	    $this->db->select('*');
		$this->db->from('hotelroom_rates');
		$this->db->where('package_hotel_id',$package_hotel_id);
		$this->db->where('room_type',$room_type);
		$this->db->where('description',$description);
		$this->db->where('rate_type',$rate_type);

		$result = $this->db->get()->result_array();
		return $result;
	}

	public function getHotelRoomRate_packageHotelId($package_hotel_id, $separate = FALSE){
        $data = array();
        $result = $this->db->get_where('hotelroom_rates', array('package_hotel_id'=>$package_hotel_id, "status"=>"active"));

        if($result->num_rows > 0){
	        if($separate){
	        	foreach($result->result_array() as $roomrate){
	        		if (strpos($roomrate['room_type'], 'extension') === FALSE) 
						$data['standard'][] = $roomrate;
					else $data['ex'][] = $roomrate;
	        	}
	        } else $data = $result->result_array();
		}
    	return $data;
	}

	public function getHotelRoomRate_packageHotelId1($package_hotel_id, $separate = FALSE){
        $data = array();
       	$this->db->select('date_from,date_to');
       	$this->db->group_by(array('date_from','date_to'));
        $result = $this->db->get_where('hotelroom_rates', array('package_hotel_id'=>$package_hotel_id, "status"=>"active"));
        $data = $result->result_array();
    	return $data;
	}
	
	public function getHotelRoomRate_Id($roomRateId){
		$data = array();
		$result = $this->db->get_where('hotelroom_rates', array('id'=>$roomRateId))->row();
		return $result;
	}

	public function processPrices($booking=array(), $hotelrooms=array()){	
		$prices = array();
		$total = $cost = $profit = 0;


		if(isset($hotelrooms['roomrate_quantity']) && !empty($hotelrooms['roomrate_quantity'])){
			foreach($hotelrooms['roomrate_quantity'] as $k => $qty){
				$price = 0;
				$id = $hotelrooms['roomrate_id'][$k];
				if($qty != '' && $id != ''){
					$cnt = 1;
					$room = $this->getHotelRoomRate_Id($id);
					
					$package_days = $booking['total_days'] - $booking['extended_days'];
	    			$package_nights = $package_days - 1;
	    			$extended_nights = $booking['extended_days'];
					$rateType_arr = explode('/', $room->rate_type);
					$unit_one = $rateType_arr[0];
					$unit_two = $rateType_arr[1];
					
					if($unit_two == 'night'){
						$nights = $package_nights;
						if(strpos($room->room_type, 'extension') === TRUE)
							$nights = $extended_nights;
					} else $nights = 1;
					if($unit_one == 'pax')
						$cnt = $room->pax;
					elseif($unit_one == 'adult')
						$cnt = $booking['child_date']['adult_count'];
					elseif($unit_one == 'child')
						if($booking['child_date']['toddler_count'] > $room->pax)
							$cnt = $room->pax;
						else $cnt = $booking['child_date']['toddler_count'];
					elseif($unit_one == 'infant')
						$cnt = $booking['child_date']['infant_count'];
					
					$price = $room->cost + $room->profit;
					$cnt_nyt_qty = $cnt * $nights * $qty;
					$room_price = $price * $cnt_nyt_qty;
					$room_cost = $room->cost * $cnt_nyt_qty;
					$room_profit = $room->profit * $cnt_nyt_qty;
					$description = (trim($room->description)!='') ? ucfirst($room->description).' - ' : '';
					$roomtype = $description . ucwords(str_replace('_', ' ', $room->room_type));
					$ratetype = ucwords(str_replace('/', ' / ', $room->rate_type));
					
					$prices[] = array(
						'id'=> $id
						,'room_type'=>$roomtype
						,'quantity'=>$qty
						,'price' => $price
						,'computed' => $room_price
						,'rate_type'=>$ratetype
						,'pax'=>$room->pax
					);

					$total += $room_price;
					$cost += $room_cost;
					$profit += $room_profit;
				}
			}
			return array('details'=>$prices,'total'=>$total,'cost'=>$cost,'profit'=>$profit);
		} else return array();
	}



	
	/*
	*
	*Start 
	*This function hotelroomsurcharge insert into database 
	*
	*/

	public function hotelroomsurcharge($roomsurcharge){
	 	$this->db->trans_start();
        $this->db->insert('hotelroom_surcharges',$roomsurcharge);
        $id = $this->db->insert_id();
 		$this->db->trans_commit();
        
        if($this->db->trans_status()) return $id;
        else return "Hotel room surcharge is not saved";   
	}
	/*
	*
	*End 
	*This function hotelroomsurcharge insert into database 
	*
	*/

	public function getSummaryHotelRoom($booking_id,$description,$hotel_id){
		$q = $this->db->get_where('summary_data', array('booking_id'=>$booking_id,'description'=>$description,'hotel_id'=>$hotel_id));
		return $q->result();

	}
}