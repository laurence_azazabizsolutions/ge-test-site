<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

get_instance()->load->iface('storeiteminterface');
get_instance()->load->iface('capturestateinterface');
get_instance()->load->iface('surchargeawareinterface');

/**
 * @author Joshua Paylaga
 *
 */
class Hotel extends MY_Model implements StoreItemInterface,
										CaptureStateInterface,
										SurchargeAwareInterface {
	// MODEL SETTING
	protected $_table_name = 'hotels';
	protected $_order_by = 'id asc';	
	/**
	 * @var integer
	 */
	private $id;
	
	/**
	 * @var string
	 */
	private $name;
	
	/**
	 * @var string
	 */
	private $description;
	
	/**
	 * @var string
	 */
	private $location;
	
	/**
	 * @var string
	 */
	private $website;
	
	/**
	 * @var string
	 */
	private $phone;
	
	/**
	 * @var string
	 */
	private $email;
	
	/**
	 * @var string
	 */
	private $status;

	/**
	 * @var array
	 */
	private $surcharges;
	
	/**
	 * @var string
	 */
	private $country;
	
	/**
	 * @var Hotel
	 */
	private $prevState;
	
	/**
	 * Constructor
	 */
	public function __construct() {
		parent::__construct();
		$this->surcharges  = array();
	}
	
	/**
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}
	
	/**
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}
	
	/**
	 * @param string $name
	 */
	public function setName($name) {
		$this->name = $this->checkString($name);
	}
	
	/**
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}
	
	/**
	 * @param string $description
	 */
	public function setDescription($description) {
		$this->description = $this->checkString($description);
	}
	
	/**
	 * @return string
	 */
	public function getLocation() {
		return $this->location;
	}
	
	/**
	 * @param string $location
	 */
	public function setLocation($location) {
		$this->location = $this->checkString($location);
	}
	
	/**
	 * @return string
	 */
	public function getWebsite() {
		return $this->website;
	}
	
	/**
	 * @param string $website
	 */
	public function setWebsite($website) {
		$this->website = $this->checkURL($website);
	}
	
	/**
	 * @return string
	 */
	public function getPhone() {
		return $this->phone;
	}
	
	/**
	 * @param string $phone
	 */
	public function setPhone($phone) {
		$this->phone = $this->checkPhone($phone);
	}
	
	/**
	 * @return string
	 */
	public function getEmail() {
		return $this->email;
	}
	
	/**
	 * @param string $email
	 */
	public function setEmail($email) {
		$this->email = $this->checkEmail($email);
	}

	/**
	 * @return string
	 */
	public function getStatus() {
		return $this->status;
	}
	
	/**
	 * @param string $status
	 */
	public function setStatus($status) {
		$this->status = $this->checkString($status);
	}

	/**
	 * @return string
	 */
	public function getCountry($code = false) {
		if($code){
			$this->db->select('country');
			$cntry = $this->db->get_where('countries', array('code'=>$this->country))->row();
			return $cntry->country;
		}
		return $this->country;
	}
	
	/**
	 * @param string $country
	 */
	public function setCountry($country) {
		$this->country = $this->checkString($country);
	}
	
	/* (non-PHPdoc)
	 * @see SurchargeAwareInterface::addSurcharge()
	 */
	public function addSurcharge(Surcharge $surcharge) {
		if ($surcharge instanceof Surcharge) {
			$surcharge->setSurchargeOf($this);
			$this->surcharges[] = $surcharge;
		}
	}
	
	/* (non-PHPdoc)
	 * @see SurchargeAwareInterface::getSurcharges()
	 */
	public function getSurcharges() {
		return $this->surcharges;
	}
	
	/* (non-PHPdoc)
	 * @see SurchargeAwareInterface::hasSurcharge()
	 */
	public function hasSurcharge() {
		return (count($this->surcharges) > 0) ? true : false;
	}
	
	/* (non-PHPdoc)
	 * @see SurchargeAwareInterface::popSurcharge()
	 */
	public function popSurcharge() {
		return array_pop($this->surcharges);
	}
	
	/* (non-PHPdoc)
	 * @see SurchargeAwareInterface::removeSurcharge()
	 */
	public function removeSurcharge($pos) {
		if (array_key_exists($pos, $this->surcharges)) {
			unset($this->surcharges[$pos]);
			$this->surcharges = array_values($this->surcharges);
			return true;
		}
		return false;
	}
	
	public function prepared() {
		if ($this->name && is_null($this->id)) {
			return true;
		}
		return false;
	}
	
	/* (non-PHPdoc)
	 * @see StoreItemInterface::stored()
	 */
	public function stored() {
		if ($this->id) {
			return true;
		}
		return false;
	}
	
	/* (non-PHPdoc)
	 * @see CaptureStateInterface::captureCurrentState()
	 */
	public function captureCurrentState() {
		$this->prevState = clone $this;
	}
	
	/* (non-PHPdoc)
	 * @see CaptureStateInterface::prevState()
	 */
	public function prevState() {
		return (!empty($this->prevState)) ? $this->prevState : false;
	}
	
	/**
	 * @param string $input
	 * @return Ambigous <NULL, string>
	 */
	private function checkString($input) {
		return (is_string($input)) ? $input : null;
	}
	
	/**
	 * @param string $url
	 * @return Ambigous <NULL, string>
	 */
	private function checkURL($url) {
		return (preg_match('/^(http\:\/\/|https\:\/\/)?([a-z0-9][a-z0-9\-]*\.)+[a-z0-9][a-z0-9\-]*((?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&;%@\.\w_]*)#?(?:[\.\!\/\\\w]*))?$/i', $url)) ? $url : null;
	}
	
	/**
	 * @param string $email
	 * @return Ambigous <NULL, string>
	 */
	private function checkEmail($email) {
		return (filter_var($email, FILTER_VALIDATE_EMAIL)) ? $email : null;
	}
	
	/**
	 * @param string $phone
	 * @return Ambigous <NULL, string>
	 */
	private function checkPhone($phone) {
		return (preg_match('/^[0-9 ()+\-]+$/i', $phone)) ? $phone : null;
	}
}