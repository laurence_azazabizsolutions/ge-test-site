<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

get_instance()->load->iface('storageinterface');
get_instance()->load->iface('idmanipulatorinterface');

class HotelStorage extends CI_Model implements StorageInterface, IdManipulatorInterface {
	
	//boolean
	private $includeDeleted;

	public function __construct() {
		$this->load->database();
		$this->includeDeleted = false;
		$this->infoWhereIn 	  = false;
		$this->infoWhereInKey = '';
		$this->infoWhereInArr = array();
	}
	
	/* (non-PHPdoc)
	 * @see IdManipulatorInterface::setId()
	 */
	public function setId($hotel, $id) {
		setprivateid($hotel, 'Hotel', $id);
	}
	
	/* (non-PHPdoc)
	 * @see StorageInterface::getById()
	 */
	public function getById($id) {
		$this->load->model('hotel/hotel');
		
		if (false == $this->includeDeleted)
			$this->db->where_not_in('status', 'deleted');
		
		$results = $this->db->get_where('hotels', array('id' => $id))->result_array();

		if (count($results) == 1) {
			$hotel = new Hotel();
			$hotel->setName($results[0]['name']);
			$hotel->setDescription($results[0]['description']);
			$hotel->setLocation($results[0]['location']);
			$hotel->setCountry($results[0]['country_code']);
			$hotel->setWebsite($results[0]['website']);
			$hotel->setPhone($results[0]['phone']);
			$hotel->setEmail($results[0]['email']);
			$hotel->setStatus($results[0]['status']);
			$this->setId($hotel, $id);
			$this->setSurchargeByHotelId($id, $hotel);
			$hotel->captureCurrentState();
			
			return clone $hotel;
		}
		
		return false;
	}
	
	/* (non-PHPdoc)
	 * @see StorageInterface::getByInformation()
	 */
	public function getByInformation(array $info, $order=TRUE, $limit=0, $offset=0) {
		
		$hotelInfo = array(
			'name' 			=> false,
			'description' 	=> false,
			'location' 		=> false,
			// 'country' 		=> false,
			'website' 		=> false,
			'phone' 		=> false,
			'status' 		=> false,
		);

		foreach ($info as $key => $infoValue) {
			if (in_array($key, array_keys($hotelInfo))) {
				$hotelInfo[$key] = $infoValue;
			}
		}
		
		foreach ($hotelInfo as $key => $infoValue) {
			if (false === $infoValue) {
				unset($hotelInfo[$key]);
			}
		}
		
		$this->db->where($hotelInfo);
		
		if($this->infoWhereIn) 
		 	$this->db->where_in($this->infoWhereInKey, $this->infoWhereInArr);
		
		$collection = array();
		if (false == $this->includeDeleted)
			$this->db->where_not_in('status', 'inactive');

		if ($order) 	$this->db->order_by('name', 'ASC');
		if ($limit>0) 	$this->db->limit($limit);
		if ($offset>0) 	$this->db->offset($offset);
		
		$results = $this->db->get('hotels');
		
		if ($results->num_rows() > 0) {
			$this->load->model('hotel/hotel');
			$results = $results->result_array();
			foreach ($results as $result) {
				$hotel = new Hotel();
				$hotel->setName($result['name']);
				$hotel->setDescription($result['description']);
				$hotel->setLocation($result['location']);
				$hotel->setCountry($result['country_code']);
				$hotel->setWebsite($result['website']);
				$hotel->setPhone($result['phone']);
				$hotel->setEmail($result['email']);
				$hotel->setStatus($result['status']);
				$this->setId($hotel, $result['id']);
				$this->setSurchargeByHotelId($result['id'], $hotel);
				$hotel->captureCurrentState();
				$collection[] = clone $hotel;
			}
		}
		return $collection;
	}
	
	/* (non-PHPdoc)
	 * @see StorageInterface::store()
	 */
	public function store($hotel) {
		if (!$hotel instanceof Hotel) {
			return;
		}
		
		// Insert
		if ($hotel->prepared()) {
			
			// Check if hotel already exists
			if ($hotel->prepared() || $hotel->stored()) {
				$this->db->from('hotels');
				$this->db->where('name', 		 $hotel->getName());
				$this->db->where('location', 	 $hotel->getLocation());
				// $this->db->where('country_code', $countryCode);
				$countHotels = $this->db->count_all_results();
				if ($countHotels > 0)
					return;
			}
			
			// Insert hotels table
			$timestamp = time();
			$this->db->insert('hotels', array(
				'name' 			=> $hotel->getName(),
				'location' 		=> $hotel->getLocation(),
				'website' 		=> $hotel->getWebsite(),
				'phone' 		=> $hotel->getPhone(),
				'email' 		=> $hotel->getEmail(),
				'description' 	=> $hotel->getDescription(),
				'status' 		=> $hotel->getStatus(),
				'country_code' 	=> $hotel->getCountry(),
				'timestamp' 	=> $timestamp,
			));
			$insert_id = $this->db->insert_id();
			
			// Reconfigure Hotels object
			$hotel->captureCurrentState();
			$this->setId($hotel, $insert_id);
			
			// Store hotel surcharges
			if ($hotel->hasSurcharge()) {
				$this->load->model('surcharge/surchargestorage');
				$surchargestorage = new SurchargeStorage();
				$surchargestorage->batchStore($hotel->getSurcharges());
			}
		}
		
		// Update
		else if ($hotel->stored()) {
			$prevHotel = $hotel->prevState();
			$updateHotel = array();
			
			// Name
			if (($hotel->getName() != $prevHotel->getName()) && ($hotel->getName())) {
				$updateHotel['name'] = $hotel->getName();
			}
			
			// Location
			if ($hotel->getLocation() != $prevHotel->getLocation()) {
				$updateHotel['location'] = $hotel->getLocation();
			}
			
			// Website
			if ($hotel->getWebsite() != $prevHotel->getWebsite()) {
				$updateHotel['website'] = $hotel->getWebsite();
			}
			
			// Phone
			if ($hotel->getPhone() != $prevHotel->getPhone()) {
				$updateHotel['phone'] = $hotel->getPhone();
			}
			
			// Email
			if ($hotel->getEmail() != $prevHotel->getEmail()) {
				$updateHotel['email'] = $hotel->getEmail();
			}
			
			// Description
			if ($hotel->getDescription() != $prevHotel->getDescription()) {
				$updateHotel['description'] = $hotel->getDescription();
			}
			
			if (($hotel->getStatus() != $prevHotel->getStatus()) && ($hotel->getStatus())) {
				$updateHotel['status'] = $hotel->getStatus();
			}
			// Country_code
			if (($hotel->getCountry() != $prevHotel->getCountry()) && ($hotel->getCountry())) {
				$updateHotel['country_code'] = $hotel->getCountry();
			}
			
			// Update hotels table
			if (count($updateHotel) > 0) {
				$this->db->where('id', $hotel->getId());
				$this->db->update('hotels', $updateHotel);
				$hotel->captureCurrentState();
			}
			
			// Update hotel surcharges
			if ($hotel->hasSurcharge()) {
				$this->load->model('surcharge/surchargestorage');
				$surchargestorage = new SurchargeStorage();
				$surchargestorage->batchStore($hotel->getSurcharges());
			}
		}
	}
	
	/* (non-PHPdoc)
	 * @see StorageInterface::batchStore()
	 */
	public function batchStore(array $objArray) {
		
		foreach ($objArray as $obj) {
			if ($obj instanceof Hotel) {
				$this->store($obj);
			}
		}
	}
	
	public function remove($hotel) {
		if ($hotel instanceof Hotel) {
			if ($hotel->stored()) {
				$this->db->where('id', $hotel->getId());
				$this->db->update('hotels', array(
					'status' => 'deleted',
				));
				setprivateid($hotel, get_class($hotel), null);
			}
		}
	}

	/**
	 * @param boolean $decision
	 */
	public function includeDeleted($decision) {
		$this->includeDeleted = (is_bool($decision)) ? $decision : false;
	}

	/**
	 * @param boolean $decision
	 */
	public function infoWhereIn($decision) {
		$this->infoWhereIn = (is_bool($decision)) ? $decision : false;
	}

	public function infoWhereInKey($key = '') {
		$this->infoWhereInKey = ($key!='') ? $key : NULL;
	}

	public function infoWhereInArr($arr = array()) {
		$this->infoWhereInArr = (!empty($arr)) ? $arr : array();
	}
	
	private function setSurchargeByHotelId($hotel_id, Hotel $hotel) {
		if ($hotel->stored()) {
			$this->load->model('surcharge/surchargestorage');
			$surchargestorage = new SurchargeStorage();
			$surchargestorage->setDbName($surchargestorage::DB_SURCHARGE_HOTEL);
			$surcharges = $surchargestorage->getByInformation(array(
					'hotel_id' => $hotel_id,
			));
				
			foreach ($surcharges as $surcharge) {
				$hotel->addSurcharge($surcharge);
			}
		}
	}
	/*
	*
	*Start 
	*This function storePackage insert into database 
	*
	*/
	public function storePackagehotels($package_id,$hotel_choice){
		$result = $this->db->get_where('package_hotels', array('package_id'=>$package_id,'hotel_id'=>$hotel_choice));
        $inDatabase = (bool)$result->num_rows();
        if (!$inDatabase){
    	 	$this->db->trans_start();
            $var=array(
                'package_id'=>$package_id,
                'hotel_id'=>$hotel_choice
            );
            $this->db->insert('package_hotels', $var);
            $id = $this->db->insert_id();
     		$this->db->trans_commit();
            if($this->db->trans_status()) return $id;
            else return "Hotel is not saved";
        } else return "This hotel has already been added to this package";
	} 
	/*
	*
	*End 
	*This function storePackage insert into database 
	*
	*/

	/*
	*
	*Start 
	*This function storePackage insert into database 
	*
	*/
	public function storePackagehotelsChk($package_id,$hotel_choice){
		$result = $this->db->get_where('package_hotels', array('package_id'=>$package_id,'hotel_id'=>$hotel_choice,'status'=>'inactive'));
        $inDatabase = (bool)$result->num_rows();
        if ($inDatabase){
        	$this->db->where('package_id',$package_id);
        	$this->db->where('hotel_id',$hotel_choice);
			$this->db->update('package_hotels',array('status'=>'active'));
			$this->db->select('*');
			$this->db->from('package_hotels');
	        $this->db->where('package_id',$package_id);
        	$this->db->where('hotel_id',$hotel_choice);
        	$q=$this->db->get();
	        if($q->num_rows > 0) 
        	return $q->row();
        } else return false;
	} 
	/*
	*
	*End 
	*This function storePackage insert into database 
	*
	*/
	public function getPackageHotelById($package_hotel_id){
		$data = array();
		$this->db->select('package_hotels.id AS package_hotel_id,
			countries.country AS country,hotels.*');
        $this->db->from('package_hotels');
        $this->db->join('hotels','hotels.id=package_hotels.hotel_id','LEFT');
        $this->db->join('countries','countries.code=hotels.country_code','LEFT');
        $this->db->where('package_hotels.id',$package_hotel_id);
        $q=$this->db->get();
        if($q->num_rows > 0) 
        	return $q->row();
       	else return array();
	}

	




	/*
	*
	*Start 
	*This function getHotelsByPackage query into database 
	*
	*/
	public function getHotelsByPackage($package_id, $order=TRUE){
       	$this->db->select('package_hotels.id AS package_hotel_id, package_hotels.hotel_id,
			countries.country AS country,hotels.*');
		
		if($order) $this->db->order_by('name', 'ASC');
        
        $this->db->from('package_hotels');
        $this->db->join('packages','packages.id=package_hotels.package_id','LEFT');
        $this->db->join('hotels','hotels.id=package_hotels.hotel_id','LEFT');
        $this->db->join('countries','countries.code=hotels.country_code','LEFT');
        $this->db->where('package_hotels.package_id',$package_id);
        $this->db->where("package_hotels.status", "active");

        $q=$this->db->get();
        // dump($q->result_array());die;
        return $q->result_array();
	}
	/*
	*
	*End 
	*This function getHotelsByPackage query into database 
	*
	*/


	/*
	*fetch get hotels by package except x hotel id 
	*/
	public function getHotelsByPackage_except($package_id,$hotel_id){
		$this->db->select('package_hotels.id AS package_hotel_id, package_hotels.hotel_id,
			countries.country AS country,
			hotels.*');
        $this->db->from('package_hotels');
        $this->db->join('packages','packages.id=package_hotels.package_id','LEFT');
        $this->db->join('hotels','hotels.id=package_hotels.hotel_id','LEFT');
        $this->db->join('countries','countries.code=packages.destination','LEFT');
        $this->db->where('package_hotels.package_id',$package_id);
        $this->db->where("package_hotels.id !=",$hotel_id);
        $q=$this->db->get();
        return $q->result_array();
	}
	/*	
	*end of fetch hotels by package except x hotel id
	*/





	public function getHotelOfPackageExcept_this($package_hotel_id){
       	$data = array();

       	$this->db->select('hotel_id,package_id');
        $this->db->from('package_hotels');
        $this->db->where('package_hotels.id',$package_hotel_id);
        $result=$this->db->get()->result_array();

        if(count($result)> 0) {
	        $this->db->select('package_hotels.id AS package_hotel_id, name as hotel_name');
	        $this->db->from('package_hotels');
	        $this->db->join('hotels','hotels.id=package_hotels.hotel_id','LEFT');;
	        $this->db->where('package_hotels.hotel_id !=',$result[0]['hotel_id']);
	        $this->db->where('package_hotels.package_id =',$result[0]['package_id']);
	        $result1=$this->db->get()->result_array();

	        if(count($result1)> 0) $data = $result1;
		}

       	return $data;
	}
	public function getHotelOfPackageHotel($package_hotel_id){
		$this->db->select('hotel_id AS hotel_choice_id');
		$result = $this->db->get_where('package_hotels', array('id' => $package_hotel_id))->row();
		return $result->hotel_choice_id;
	}

	public function getNameofPackageHotel($package_hotel_id){
		$this->db->select('hotels.name as hotel_name');
		$this->db->from('hotels');
		$this->db->join('package_hotels','hotels.id=package_hotels.hotel_id','LEFT');
		$this->db->where(array('package_hotels.id' => $package_hotel_id));
		$result = $this->db->get()->row();

		return (!empty($result)) ? $result->hotel_name : '';
		// return $result->hotel_choice_id;
	}


	public function addDestinationLink($data){
		$this->db->trans_start();
        $this->db->insert('destination_pdf',$data);
 		$this->db->trans_commit();
        if($this->db->trans_status()) return true;
        else return "Hotel room rate is not saved";
	}

	public function getDestinationLink($destination_name){
    	$this->db->select('
			countries.country as Country,
    		destination_pdf.id AS did,
    		destination_pdf.destination AS des,
    		destination_pdf.pdf_name AS pdfname,
    		destination_pdf.title AS title');
    	$this->db->from('destination_pdf');
    	$this->db->join('countries','countries.code=destination_pdf.destination','CROSS');
    	$this->db->where('destination_pdf.destination',$destination_name);
		$res=$this->db->get();
		return $res->result();
	}

	public function getDestination_Link(){

    	$this->db->select('
			countries.country as Country,
    		destination_pdf.id AS did,
    		destination_pdf.destination AS des,
    		destination_pdf.pdf_name AS pdfname,
    		destination_pdf.title AS title');
    	$this->db->from('destination_pdf');
    	$this->db->join('countries','countries.code=destination_pdf.destination','CROSS');
		$res=$this->db->get();
		return $res->result();
	}

	public function getDestinationLink_crossjoin(){
    	$this->db->select('countries.code AS CCODE,
			    		  packages.title AS title,
			    		  packages.image_path AS image,
						  packages.destination AS PDEST');
    	$this->db->from('countries');
    	$this->db->join('packages','packages.destination=countries.code','left');
    	$this->db->where('packages.destination <> ""');
    	$this->db->group_by('packages.destination');
		$res=$this->db->get();
		return $res->result();
	}


	public function getDestinationLink_display($data){
    	$this->db->select('countries.code AS CCODE,
					packages.destination AS PDEST,
					packages.id AS PID,
					packages.code AS PCODE,
					packages.image_path AS PIMAGE,
					packages.title AS PTITLE,
					package_description.description AS PDESC,
					package_duration.date_from AS PFROM,
					package_duration.date_to AS PTO');
    	$this->db->from('countries');
    	$this->db->join('packages','packages.destination=countries.code','left');
    	$this->db->join('package_description','package_description.package_id=packages.id','left');
    	$this->db->join('package_duration','package_duration.package_id=packages.id','left');
    	$this->db->where('countries.code ="'.$data.'"');
		$res=$this->db->get();
		return $res->result();

	}
	public function getDestinationLink_pdf($data){
    	$this->db->select('countries.code AS CCODE,
					destination_pdf.destination AS DCOUNTRY,
					destination_pdf.title AS DTITLE,
					destination_pdf.pdf_name AS DNAME');
    	$this->db->from('countries');
    	$this->db->join('destination_pdf','destination_pdf.destination=countries.code','left');
    	$this->db->where('countries.code ="'.$data.'" && destination_pdf.destination <> ""');
		$res=$this->db->get();
		return $res->result();

	}
	public function deleteDestinationLink($id){
		$this->db->trans_start();
        $this->db->delete('destination_pdf', array('id' => $id));
 		$this->db->trans_commit();
        if($this->db->trans_status()) return true;
        else return "Hotel room rate is not saved";
	}

	/*
	* fetch the hotels by package id and country code
	*/
	public function getHotelsByPackageAndCode($code,$p_id){
		$this->db->select("hotels.*, package_hotels.*");
		$this->db->from("package_hotels");
		$this->db->join("hotels","hotels.id = package_hotels.hotel_id","LEFT");
		$this->db->where(array("hotels.country_code"=>$code,"package_hotels.package_id"=>$p_id));
		#$this->db->where(array("package_hotels.package_id"=>$p_id));
		$this->db->order_by("name","asc");
		return $this->db->get()->result_array();

	}
	/*
	* end of fetch the hotels by package id and country code
	*/

}