<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

get_instance()->load->iface('storageinterface');
get_instance()->load->iface('sensitiveuserdatamanipulatorinterface');

/**
 * 
 * @author Joshua Paylaga
 *
 */
class UserStorage extends CI_Model implements StorageInterface, SensitiveUserDataManipulatorInterface {
	
	/**
	 * @var string
	 */
	private $cipher;
	
	/**
	 * @var boolean
	 */
	private $includePending;

	/**
	 * @var boolean
	 */
	private $includeInactive;

	/**
	 * @var boolean
	 */
	private $includeAll;
	
	/**
	 * @var number
	 */
	private $limit;
	
	/**
	 * @var number
	 */
	private $offset;
	
	/**
	 * @var boolean
	 */
	private $countMode;
	
	/**
	 * Constructor.
	 */
	public function __construct() {
		parent::__construct();
		$this->load->database();
		$this->cipher        	= $this->db->escape($this->config->item('encryption_key'));
		$this->includePending	= false;
		$this->includeInactive	= false;
		$this->includeAll		= false;
		$this->countMode		= false;
		$this->load->model('user/userfactory');
	}
	
	/* (non-PHPdoc)
	 * @see StorageInterface::getById()
	 */
	public function getById($id) {
		$this->load->model('user/user');
		$queryArray['users.id'] = $id;
		
		$whereNotIn = array('deleted');
		
		if (false == $this->includeAll)
			$whereNotIn = array();

		if (false == $this->includePending)
			$whereNotIn[] = 'pending';

		if (false == $this->includeInactive)
			$whereNotIn[] = 'inactive';

		$this->db->select("users.*, companies.name as 'company'");
		$this->db->from("users");
		$this->db->join("company_designations", "users.id = company_designations.user_id", "left");
		$this->db->join("companies", "company_designations.company_id = companies.id", "left");
		$this->db->where($queryArray);
		$this->db->where_not_in('status', $whereNotIn);
		if ($this->hasLimit()) {
			$this->db->limit($this->limit, $this->offset);
		}
		
		$result = $this->db->get()->result_array();
		
		if (count($result) === 1) {
			$this->removeFetchLimit();
			return $this->translateDataToTypedClass($result[0]);
		}
		
		return false;
	}
	
	public function getByUser($username){
		$this->db->select("email");
		$this->db->from("users");
		$this->db->where('username',$username);
		$this->db->or_where('email',$username);
		return $this->db->get()->result();
	}
	public function getAllPackages(){
		$this->db->select("id");
		$this->db->from("packages");
		$query = $this->db->get();

		return $query->result();
	}

	public function count_packages(){
		$this->db->select("id");
		$this->db->from("packages");
		$query = $this->db->get();

		return $query->num_rows();
	}

	public function count_particular_packages($id){
		$this->db->select("*");
		$this->db->from("package_permissions");
		$this->db->where("user_id",$id);
		$query = $this->db->get();

		return $query->num_rows();
	}

	public function compare_packages($package_id,$userId){
		$this->db->select("*");
		$this->db->from("package_permissions");
		$this->db->where('package_id',$package_id);
		$this->db->where('user_id',$userId);
		$this->db->limit(1);
		$query = $this->db->get();

        if($query -> num_rows() == 1){
           	return TRUE;

        }else{
          	return FALSE;
        }

	}

	

	public function grant_packages($package_id,$userId){
		$data = array(
			'package_id' 	=> $package_id,
			'user_id'		=> $userId,
			);
		$this->db->trans_start();
		$this->db->insert('package_permissions', $data);
 		$this->db->trans_commit();


        if($this->db->trans_status()) return true;
        else return "Permission is not saved";
	}
	/* (non-PHPdoc)
	 * @see StorageInterface::getByInformation()
	 */
	public function getByInformation(array $personalInfo, $start = false, $limit = false) {
		// Set fields
		$infoFields = array(
			'firstname' 	=> false,
			'middlename' 	=> false,
			'lastname' 		=> false,
			'username' 		=> false,
			'email' 		=> false,
			'alternative_email' 		=> false,
			'phone' 		=> false,
			'company' 		=> false,
			'country' 		=> false,
			'type' 			=> false,
			'salutation' 	=> false,
			'date_of_birth' => false,
			'status' 		=> false
		);
		
		$whereNotIn = array('deleted');
		

		// To include pending activation users
		if (false == $this->includePending) {
			$whereNotIn[] = 'pending';
		}

		if (false == $this->includeInactive) {
			$whereNotIn[] = 'inactive';
		}

		if (true == $this->includeAll)
			$whereNotIn = array();
		
		// dump($whereNotIn);die;
		// Set initialized fields with values
		foreach ($personalInfo as $infoField => $infoValue) {
			if (in_array($infoField, array_keys($infoFields))) {
				$infoFields[$infoField] = $infoValue;
			}
		}
		
		// Clear fields with no values
		foreach ($infoFields as $infoField => $infoValue) {
			if ($infoValue == false) {
				unset($infoFields[$infoField]);
			}
		}
		
		// Rename fields for joining table
		$fieldsWithNewKeys = array();
		foreach ($infoFields as $fieldKey => $fieldValue) {
			$fieldsWithNewKeys[($fieldKey == 'company') ? 'companies.name' : 'users.' . $fieldKey] = $fieldValue;
		}
		
		// CI active record
		$this->db->select("users.*, companies.name as 'company'");
		$this->db->from("users");
		$this->db->join("company_designations", "users.id = company_designations.user_id", "left");
		$this->db->join("companies", "company_designations.company_id = companies.id", "left");
		$this->db->where($fieldsWithNewKeys);
		if(!empty($whereNotIn))
			$this->db->where_not_in('status', $whereNotIn);
		$this->db->order_by("users.lastname", "asc");

		/*if ($this->hasLimit()) {
			$this->db->limit($this->limit, $this->offset);
		}*/

		if($limit){
			$this->db->limit($limit,$start);
		}

		// Return number of results instead when in count mode
		if ($this->countMode) {
			return $this->db->count_all_results();
		}
		
		// Return fetched users in the database
		$results = $this->db->get()->result_array();

		//dump_exit($results);
		$typedUsersCollection = array();
		foreach ($results as $result) {
			$typedUsersCollection[] = $this->translateDataToTypedClass($result);
		}

		if ($this->hasLimit()) {
			$this->removeFetchLimit();
		}
		// dump($typedUsersCollection);
		// die;
		return $typedUsersCollection;
	}
	
	/* (non-PHPdoc)
	 * @see StorageInterface::store()
	 */
	public function store($user) {
		if (!$user instanceof TypedUserInterface)
			return;
		
		$regtime      = time();
		$userInstance = $user->getUser();
		
		// if ($userInstance->prepared() || $userInstance->stored()) {
		if ($userInstance->prepared()) {
			$this->db->from('users');
			$this->db->where('email', $userInstance->getEmail());
			$countEmails = $this->db->count_all_results();
			if ($countEmails > 0) return;
		}
		
		if ($userInstance->prepared()) {
			
			$userToInsert = array(
				'firstname' => $userInstance->getFirstname(),
				'middlename' => ($userInstance->getMiddlename()) ? $userInstance->getMiddlename() : null,
				'lastname' => $userInstance->getLastname(),
				'email' => $userInstance->getEmail(),
				'username' => $userInstance->getUsername(),
				'alternative_email' => $userInstance->getAlteremail(),
				'phone' => $userInstance->getPhone(),
				'emergency_phone_number' => $userInstance->getEmergencyphone(),
				'emergency_name' => $userInstance->getEmergencyname(),
				'type' => strtolower(get_class($user)),
				'regtime' => $regtime,
				'country' => $userInstance->getCountry(),
				'salutation' => $userInstance->getSalutation(),
				'date_of_birth' => $userInstance->getDateOfBirth(),
				'status' => 'pending'
			);

			
			// $toHash = '';
			// foreach ($userToInsert as $userInfo) {
			// 	$toHash .= $userInfo;
			// }
			// $toHash = sha1($toHash);
			// $this->db->insert('master_users', array(
			// 	'hash' => $toHash,
			// 	'timestamp' => $regtime
			// ));
			// $masteruserid = $this->db->insert_id();
			
			// $userToInsert['master_user_id'] = $masteruserid;
			
			$this->db->insert('users', $userToInsert);
			$userId = $this->db->insert_id();
			
			$this->setId($userInstance, $userId);
			$userInstance->captureCurrentState();
			if ($userInstance->getCompany()) {
				$this->assignCompanyFromDatabase($userId, $userInstance->getCompany(), $regtime);
			}
			
			$this->db->set('password', "AES_ENCRYPT({$this->db->escape($userInstance->getPassword() . $userInstance->getId())},{$this->cipher})", false);
			$this->db->set('regtime', $regtime);
			$this->db->insert('passwords');
		}
		
		else if ($userInstance->stored()) {

			$previousState = $userInstance->prevState();
			if ($previousState instanceof User) {
				$userToUpdate = array();

				if (($userInstance->getFirstname() != $previousState->getFirstname()) && ($userInstance->getFirstname())) {
					$userToUpdate['firstname'] = $userInstance->getFirstname();
				}
				//if (($userInstance->getMiddlename() != $previousState->getMiddlename()) && ($userInstance->getMiddlename())) {
				if ($userInstance->getMiddlename() != $previousState->getMiddlename()) {
					$userToUpdate['middlename'] = $userInstance->getMiddlename();
				}
				if (($userInstance->getLastname() != $previousState->getLastname()) && ($userInstance->getLastname())) {
					$userToUpdate['lastname'] = $userInstance->getLastname();
				}
				if (($userInstance->getUsername() != $previousState->getUsername()) && ($userInstance->getUsername())) {
					$userToUpdate['username'] = $userInstance->getUsername();
				}
				if (($userInstance->getEmail() != $previousState->getEmail()) && ($userInstance->getEmail())) {
					$userToUpdate['email'] = $userInstance->getEmail();
				}
				if (($userInstance->getAlteremail() != $previousState->getAlteremail()) && ($userInstance->getAlteremail())) {
					$userToUpdate['alternative_email'] = $userInstance->getAlteremail();
				}
				if (($userInstance->getPhone() != $previousState->getPhone()) && ($userInstance->getPhone())) {
					$userToUpdate['phone'] = $userInstance->getPhone();
				}
				if (($userInstance->getEmergencyphone() != $previousState->getEmergencyphone()) && ($userInstance->getEmergencyphone())) {
					$userToUpdate['emergency_phone_number'] = $userInstance->getEmergencyphone();
				}
				if (($userInstance->getEmergencyname() != $previousState->getEmergencyname()) && ($userInstance->getEmergencyname())) {
					$userToUpdate['emergency_name'] = $userInstance->getEmergencyname();
				}
				if (($userInstance->getCompany() != $previousState->getCompany()) && ($userInstance->getCompany())) {
					if (!$previousState->getCompany()) {
						$this->assignCompanyFromDatabase($userInstance->getId(), $userInstance->getCompany(), $regtime);
					} else {
						$this->assignCompanyFromDatabase($userInstance->getId(), $userInstance->getCompany(), $regtime, true);
					}
				}
				if (($userInstance->getCountry() != $previousState->getCountry()) && ($userInstance->getCountry())) {
					$userToUpdate['country'] = $userInstance->getCountry();
				}
				
				if (($userInstance->getDateOfBirth() != $previousState->getDateOfBirth()) && ($userInstance->getDateOfBirth())) {
					$userToUpdate['date_of_birth'] = $userInstance->getDateOfBirth();
				}

				if (($userInstance->getStatus() != $previousState->getStatus()) && ($userInstance->getStatus())) {
					$userToUpdate['status'] = $userInstance->getStatus();
				}

				if (($userInstance->getPassword() != $previousState->getPassword()) && ($userInstance->getPassword())) {
					$this->db->set('password', "AES_ENCRYPT({$this->db->escape($userInstance->getPassword() . $userInstance->getId())},{$this->cipher})", false);
					$this->db->where("SUBSTR(CAST(AES_DECRYPT(password, {$this->cipher}) as CHAR),41) =", $userInstance->getId());
					$this->db->update('passwords');
				}
				if (count($userToUpdate) > 0) {
					$this->db->where('id', $userInstance->getId());
					$this->db->update('users', $userToUpdate);
					$userInstance->captureCurrentState();
				}
			}
		}
	}
	
	/* (non-PHPdoc)
	 * @see StorageInterface::batchStore()
	 */
	public function batchStore(array $users) {
		foreach ($users as $user) {
			if ($user instanceof TypedUserInterface) {
				$this->store($user);
			}
		}
	}
	
	/* (non-PHPdoc)
	 * @see StorageInterface::remove()
	 */
	public function remove($user) {
		if ($user instanceof TypedUserInterface) {
			if ($user->getUser()->stored()) {
				$this->db->where('id', $user->getUser()->getId());
				$this->db->update('users', array('status' => 'deleted'));
				$userObj = $user->getUser();
				setprivateid($userObj, get_class($userObj), null);
			}
		}
	}
	
	/* (non-PHPdoc)
	 * @see IdManipulatorInterface::setId()
	 */
	public function setId($user, $id) {
		setprivateid($user, 'User', $id);
	}
	
	/* (non-PHPdoc)
	 * @see SensitiveUserDataManipulatorInterface::setStoredPassword()
	 */
	public function setStoredPassword(User $user, $storedPassword) {
		$ref     = new ReflectionClass('User');
		$passref = $ref->getProperty('password');
		$passref->setAccessible(true);
		$passref->setValue($user, $storedPassword);
	}
	
	/**
	 * @param number $id
	 */
	public function removeById($id) {
		$user = $this->getById($id);
		$this->remove($user);
	}
	
	/**
	 * @param array $personalInfo
	 */
	public function removeByInformation(array $personalInfo) {
		$users = $this->getByInformation($personalInfo);
		foreach ($users as $user) {
			$this->remove($user);
		}
	}
	
	/**
	 * @param number $limit
	 * @param number $offset
	 */
	public function fetchLimit($limit, $offset = NULL) {
		$this->limit = $limit;
		$this->offset = $offset;
	}
	
	/**
	 * 
	 */
	public function removeFetchLimit() {
		$this->limit = NULL;
		$this->offset = NULL;
	}
	
	/**
	 * @return boolean
	 */
	public function hasLimit() {
		return ($this->limit) ? true : false;
	}
	
	/**
	 * @param boolean $countMode
	 */
	public function countMode($countMode) {
		$this->countMode = $countMode;
	}
	
	/**
	 * @param boolean $decision
	 */
	public function includePending($decision) {
		$this->includePending = (is_bool($decision)) ? $decision : false;
	}

	/**
	 * @param boolean $decision
	 */
	public function includeInactive($decision) {
		$this->includeInactive = (is_bool($decision)) ? $decision : false;
	}

	/**
	 * @param boolean $decision
	 */
	public function includeAll($decision) {
		$this->includeAll = (is_bool($decision)) ? $decision : false;
	}
	
	/**
	 * @param array $result
	 * @return NULL
	 */
	private function translateDataToTypedClass(array $result) {
		$typedUser = null;
		if (is_array($result)) {
			$this->load->model('user/user');
			$this->setId($this->user, $result['id']);
			$this->user->setFirstname($result['firstname']);
			if ($result['middlename'])
				$this->user->setMiddlename($result['middlename']);
			$this->user->setLastname($result['lastname']);
			$this->user->setUsername($result['username']);
			$this->user->setPhone($result['phone']);
			$this->user->setEmergencyphone($result['emergency_phone_number']);
			$this->user->setEmergencyname($result['emergency_name']);
			$this->user->setEmail($result['email']);
			$this->user->setAlteremail($result['alternative_email']);
			$this->user->setCompany($result['company']);
			$this->user->setCountry($result['country']);
			$this->user->setSalutation($result['salutation']);
			$this->user->setDateOfBirth($result['date_of_birth']);
			$this->user->setStatus($result['status']);
			$qstring = "SELECT SUBSTR(CAST(AES_DECRYPT(password, {$this->cipher}) as CHAR),1,40) as 'password'
						FROM `passwords`
						WHERE SUBSTR(CAST(AES_DECRYPT(password, {$this->cipher}) as CHAR),41) = ?
						ORDER BY `regtime` DESC LIMIT 1";
			$result_password = $this->db->query($qstring, $result['id'])->result_array();
			if (count($result_password) === 1) {
				$this->setStoredPassword($this->user, $result_password[0]['password']);
			}


			$this->userfactory->setUser(clone $this->user);
			$typedUser = $this->userfactory->buildInstance($result['type']);
			$typedUser->getUser()->captureCurrentState();
			unset($this->user);
		}
		
		return $typedUser;
	}
	
	/**
	 * @param unknown $userId
	 * @param unknown $companyName
	 * @param unknown $timestamp
	 * @param string $forUpdate
	 */
	private function assignCompanyFromDatabase($userId, $companyName, $timestamp, $forUpdate = false) {
		$existingCompany = $this->db->get_where('companies', array(
			'name' => $companyName
		))->result_array();
		if (count($existingCompany) <= 0) {
			$insertCompany = array(
				'name' => $companyName,
				'timestamp' => $timestamp
			);
			$this->db->insert('companies', $insertCompany);
			$insertCompany['id'] = $this->db->insert_id();
			$existingCompany[0]  = $insertCompany;
		}
		$designationInsert = array(
			'user_id' => $userId,
			'company_id' => $existingCompany[0]['id'],
			'timestamp' => $timestamp
		);
		if ($forUpdate) {
			$designationUpdate = array(
				'company_id' => $designationInsert['company_id'],
				'timestamp' => $timestamp
			);
			$this->db->where('user_id', $userId);
			$this->db->update('company_designations', $designationUpdate);
		} else {
			$this->db->insert('company_designations', $designationInsert);
		}
	}
	
	public function updateUserStatus_byId($id,$status){
		$statusArr = array('pending','active','inactive','deleted');
		if (in_array($status, $statusArr)) {
			$this->db->where('id',$id);
			$this->db->update('users',array('status'=>$status));
			return true;
		} else return false;
	}

	//original, by Niko
	public function updateThenReturnUser_byId($id,$status)
	{
		$this->db->where('id',$id);
		$q=$this->db->update('users',$status);
		if($q)
		{
			$this->db->where('id',$id);
			$this->db->select('*');
			$q = $this->db->get('users');
			return $q->result();
		}
		else
			echo 'bababa';

	}

	public function storeAgent($userToInsert,$company,$password) {
		
		$regtime      = time();
			
		$this->db->insert('users', $userToInsert);
		$userId = $this->db->insert_id();

		if ($company) {
			$this->assignCompanyFromDatabase($userId, $this->user->getCompany(), $regtime);
		}
		
		$this->db->set('password', "AES_ENCRYPT({$this->db->escape($password . $userId)},{$this->cipher})", false);
		$this->db->set('regtime', $regtime);
		$this->db->insert('passwords');

		return $userId;

	}

	public function updatePassword($user){
		$userInstance = $user->getUser();
		$regtime      = time();
		$this->db->set('password', "AES_ENCRYPT({$this->db->escape($userInstance->getPassword() . $userInstance->getId())},{$this->cipher})", false);
		$this->db->set('regtime', $regtime);
		$this->db->where('id',$userInstance->getId());
		$this->db->update('passwords');
	}
}