<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

get_instance()->load->iface('typeduserinterface');

/**
 * @author Joshua Paylaga
 *
 */
class Customer extends MY_Model implements TypedUserInterface {
	// MODEL SETTING
	protected $_table_name = 'traveller_details';
	protected $_order_by = 'id asc';
	/**
	 * @var User
	 */
	private $user;
	
	/**
	 * Constructor
	 */
	public function __construct() {
		parent::__construct();
	}
	
	/* (non-PHPdoc)
	 * @see TypedUserInterface::getUser()
	 */
	public function getUser() {
		return $this->user;
	}
	
	/* (non-PHPdoc)
	 * @see TypedUserInterface::setUser()
	 */
	public function setUser(User $user) {
		if ($user instanceof User) {
			$this->user = $user;
		} //$user instanceof User
	}
}

/* End of file customer.php */
/* Location: ./application/models/customer.php */