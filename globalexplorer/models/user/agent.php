<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

get_instance()->load->iface('typeduserinterface');

/**
 * @author Joshua Paylaga
 *
 */
class Agent extends CI_Model implements TypedUserInterface {
	
	/**
	 * @var User
	 */
	private $user;
	
	/**
	 * Constructor
	 */
	public function __construct() {
		parent::__construct();
	}
	
	/* (non-PHPdoc)
	 * @see TypedUserInterface::getUser()
	 */
	public function getUser() {
		return $this->user;
	}
	
	/* (non-PHPdoc)
	 * @see TypedUserInterface::setUser()
	 */
	public function setUser(User $user) {
		if ($user instanceof User) {
			$this->user = $user;
		} //$user instanceof User
	}
}

/* End of file agent.php */
/* Location: ./application/models/agent.php */