<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

get_instance()->load->iface('capturestateinterface');
get_instance()->load->iface('storeiteminterface');

/**
 * @author Joshua Paylaga
 *
 */
class User extends MY_Model implements CaptureStateInterface, StoreItemInterface {
	// MODEL SETTING
	protected $_table_name = 'users';
	protected $_order_by = 'id asc';

	/**
	 * @var integer
	 */
	private $id;
	
	/**
	 * @var string
	 */
	private $firstname;
	
	/**
	 * @var string
	 */
	private $middlename;
	
	/**
	 * @var string
	 */
	private $lastname;
	
	/**
	 * @var string
	 */
	private $company;
	
	/**
	 * @var string
	 */
	private $password;
	
	/**
	 * @var string
	 */
	private $phone;
	
	/**
	 * @var string
	 */
	private $em_phone;

	/**
	 * @var string
	 */
	private $em_name;
	
	/**
	 * @var string
	 */
	private $email;

	/**
	 * @var string
	 */
	private $username;

	/**
	 * @var string
	 */
	private $alteremail;
	
	/**
	 * @var string
	 */
	private $country;
	
	/**
	 * @var string
	 */
	private $salutation;
	
	/**
	 * @var string
	 */
	private $dateOfBirth;
	
	/**
	 * @var string
	 */
	private $status;
	
	/**
	 * @var User
	 */
	private $prevState;
	
	/**
	 * @var string
	 */
	const SALUTATION_MR = 'mr.';
	
	/**
	 * @var string
	 */
	const SALUTATION_MS = 'ms.';
	
	/**
	 * @var string
	 */
	const SALUTATION_MRS = 'mrs.';
	
	/**
	 * Controller
	 */
	public function __construct() {
		parent::__construct();
	}
	
	/**
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}
	
	/**
	 * @return string
	 */
	public function getFirstname() {
		return $this->firstname;
	}
	
	/**
	 * @param string $firstname
	 */
	public function setFirstname($firstname) {
		$this->firstname = $this->checkString($firstname);
	}
	
	/**
	 * @return string
	 */
	public function getMiddlename() {
		return $this->middlename;
	}
	
	/**
	 * @param string $middlename
	 */
	public function setMiddlename($middlename) {
		$this->middlename = $this->checkString($middlename);
	}
	
	/**
	 * @return string
	 */
	public function getLastname() {
		return $this->lastname;
	}
	
	/**
	 * @param string $lastname
	 */
	public function setLastname($lastname) {
		$this->lastname = $this->checkString($lastname);
	}

	/**
	 * @return string
	 */
	public function getUsername() {
		return $this->username;
	}
	
	/**
	 * @param string $lastname
	 */
	public function setUsername($username) {
		$this->username = $this->checkString($username);
	}
	
	/**
	 * @return string
	 */
	public function getCompany() {
		return $this->company;
	}
	
	/**
	 * @param string $company
	 */
	public function setCompany($company) {
		$this->company = $this->checkString($company);
	}
	
	/**
	 * @return Ambigous <NULL, string, string>
	 */
	public function getPassword() {
		return $this->password;
	}
	
	/**
	 * @param string $password
	 */
	public function setPassword($password) {
		$str            = $this->checkString($password);
		$this->password = ($str) ? hashmonster($password) : null;
	}
	
	/**
	 * @return string
	 */
	public function getPhone() {
		return $this->phone;
	}
	
	/**
	 * @param string $phone
	 */
	public function setPhone($phone) {
		$this->phone = $this->checkString($phone);
	}

	/**
	 * @return string
	 */
	public function getEmergencyphone() {
		return $this->em_phone;
	}
	
	/**
	 * @param string $phone
	 */
	public function setEmergencyphone($em_phone) {
		$this->em_phone = $this->checkString($em_phone);
	}

	/**
	 * @return string
	 */
	public function getEmergencyname() {
		return $this->em_name;
	}
	
	/**
	 * @param string $phone
	 */
	public function setEmergencyname($em_name) {
		$this->em_name = $this->checkString($em_name);
	}
	
	/**
	 * @return string
	 */
	public function getEmail() {
		return $this->email;
	}
	/**
	 * @param string $email
	 */
	public function setEmail($email) {
		$email = $this->checkString($email);
		$this->load->helper('email');
		if (valid_email($email)) {
			$this->email = $email;
		}
	}

	/**
	 * @return string
	 */
	public function getAlteremail() {
		return $this->alteremail;
	}
	
	/**
	 * @param string $email
	 */
	public function setAlteremail($alteremail) {
		$this->alteremail = $alteremail;
	}

	
	/**
	 * @return Ambigous <NULL, boolean>
	 */
	public function getCountry($code = false) {
		// dump($this->country);die;
		// dump($code);die;
		if ($code && $this->stored()) {
			$countries = $this->getAllCountries();
			$return = ($countries[$this->country]) ? $countries[$this->country] : null;
		}else {
			$return = $this->country;
		}
		return $return;
	}
	
	/**
	 * @param string $salutation
	 */
	public function setSalutation($salutation) {
		if ($salutation === $this::SALUTATION_MR || $salutation === $this::SALUTATION_MS || $salutation === $this::SALUTATION_MRS) {
			$this->salutation = $salutation;
		}
	}
	
	/**
	 * @return string
	 */
	public function getSalutation() {
		return $this->salutation;
	}
	
	/**
	 * @param string $date
	 */
	public function setDateOfBirth($date) {
		$this->dateOfBirth = (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $date) > 0) ? $date : null;
	}
	
	/**
	 * @return Ambigous <NULL, string>
	 */
	public function getDateOfBirth() {
		return $this->dateOfBirth;
	}
	
	/**
	 * @param string $countryCode
	 */
	public function setCountry($countryCode) {
		$this->country = ($countryCode) ? strtoupper($countryCode) : null;
	}
		
	public function getStatus(){
		return $this->status;
	}

	public function setStatus($status){
		$this->status = $this->checkStatus($status);
	}
	
	/* (non-PHPdoc)
	 * @see CaptureStateInterface::prevState()
	 */
	public function prevState() {
		return ($this->prevState instanceof User) ? $this->prevState : false;
	}
	
	/* (non-PHPdoc)
	 * @see StoreItemInterface::prepared()
	 */
	public function prepared() {
		if ($this->firstname
			&& $this->lastname
			&& $this->password
			&& $this->phone
			&& $this->em_phone
			&& $this->em_name
			&& $this->email
			&& $this->username
			&& $this->country
			&& $this->salutation
			//&& $this->status
			&& is_null($this->id)) {
				return true;
		}
		return false;
	}
	
	/* (non-PHPdoc)
	 * @see StoreItemInterface::stored()
	 */
	public function stored() {
		return ($this->id) ? true : false;
	}
	
	/* (non-PHPdoc)
	 * @see CaptureStateInterface::captureCurrentState()
	 */
	public function captureCurrentState() {
		if ($this->stored()) {
			$this->prevState = clone $this;
		}
	}
	
	/**
	 * @param string $input
	 * @return Ambigous <NULL, string>
	 */
	private function checkString($input) {
		return (is_string($input)) ? $input : null;
	}

	private function checkStatus($input) {
		return (preg_match('/(pending|active|inactive|deleted)/i',$input) ? $input : null);
	}

	public function getAllCountries(){
		return $country = array(
			// "" => "- Select -",
			"AF" => "Afghanistan",
			"AL" => "Albania",
			"DZ" => "Algeria",
			"AS" => "American Samoa",
			"AD" => "Andorra",
			"AO" => "Angola",
			"AI" => "Anguilla",
			"AQ" => "Antarctica",
			"AG" => "Antigua and Barbuda",
			"AR" => "Argentina",
			"AM" => "Armenia",
			"AW" => "Aruba",
			"AU" => "Australia",
			"AT" => "Austria",
			"AZ" => "Azerbaijan",
			"BS" => "Bahamas",
			"BH" => "Bahrain",
			"BD" => "Bangladesh",
			"BB" => "Barbados",
			"BY" => "Belarus",
			"BE" => "Belgium",
			"BZ" => "Belize",
			"BJ" => "Benin",
			"BM" => "Bermuda",
			"BT" => "Bhutan",
			"BO" => "Bolivia",
			"BA" => "Bosnia and Herzegowina",
			"BW" => "Botswana",
			"BV" => "Bouvet Island",
			"BR" => "Brazil",
			"IO" => "British Indian Ocean Territory",
			"BN" => "Brunei Darussalam",
			"BG" => "Bulgaria",
			"BF" => "Burkina Faso",
			"BI" => "Burundi",
			"KH" => "Cambodia",
			"CM" => "Cameroon",
			"CA" => "Canada",
			"CV" => "Cape Verde",
			"KY" => "Cayman Islands",
			"CF" => "Central African Republic",
			"TD" => "Chad",
			"CL" => "Chile",
			"CN" => "China",
			"CX" => "Christmas Island",
			"CC" => "Cocos (Keeling) Islands",
			"CO" => "Colombia",
			"KM" => "Comoros",
			"CG" => "Congo",
			"CK" => "Cook Islands",
			"CR" => "Costa Rica",
			"CI" => "Cote D'Ivoire",
			"HR" => "Croatia",
			"CU" => "Cuba",
			"CY" => "Cyprus",
			"CZ" => "Czech Republic",
			"DK" => "Denmark",
			"DJ" => "Djibouti",
			"DM" => "Dominica",
			"DO" => "Dominican Republic",
			"TL" => "East Timor",
			"EC" => "Ecuador",
			"EG" => "Egypt",
			"SV" => "El Salvador",
			"GQ" => "Equatorial Guinea",
			"ER" => "Eritrea",
			"EE" => "Estonia",
			"ET" => "Ethiopia",
			"FK" => "Falkland Islands (Malvinas)",
			"FO" => "Faroe Islands",
			"FJ" => "Fiji",
			"FI" => "Finland",
			"FR" => "France",
			"FX" => "France, Metropolitan",
			"GF" => "French Guiana",
			"PF" => "French Polynesia",
			"TF" => "French Southern Territories",
			"GA" => "Gabon",
			"GM" => "Gambia",
			"GE" => "Georgia",
			"DE" => "Germany",
			"GH" => "Ghana",
			"GI" => "Gibraltar",
			"GR" => "Greece",
			"GL" => "Greenland",
			"GD" => "Grenada",
			"GP" => "Guadeloupe",
			"GU" => "Guam",
			"GT" => "Guatemala",
			"GN" => "Guinea",
			"GW" => "Guinea-bissau",
			"GY" => "Guyana",
			"HT" => "Haiti",
			"HM" => "Heard and Mc Donald Islands",
			"HN" => "Honduras",
			"HK" => "Hong Kong",
			"HU" => "Hungary",
			"IS" => "Iceland",
			"IN" => "India",
			"ID" => "Indonesia",
			"IR" => "Iran (Islamic Republic of)",
			"IQ" => "Iraq",
			"IE" => "Ireland",
			"IL" => "Israel",
			"IT" => "Italy",
			"JM" => "Jamaica",
			"JP" => "Japan",
			"JO" => "Jordan",
			"KZ" => "Kazakhstan",
			"KE" => "Kenya",
			"KI" => "Kiribati",
			"KP" => "Korea, Democratic People's Republic of",
			"KR" => "Korea, Republic of",
			"KW" => "Kuwait",
			"KG" => "Kyrgyzstan",
			"LA" => "Lao People's Democratic Republic",
			"LV" => "Latvia",
			"LB" => "Lebanon",
			"LS" => "Lesotho",
			"LR" => "Liberia",
			"LY" => "Libyan Arab Jamahiriya",
			"LI" => "Liechtenstein",
			"LT" => "Lithuania",
			"LU" => "Luxembourg",
			"MO" => "Macau",
			"MK" => "Macedonia, The Former Yugoslav Republic of",
			"MG" => "Madagascar",
			"MW" => "Malawi",
			"MY" => "Malaysia",
			"MV" => "Maldives",
			"ML" => "Mali",
			"MT" => "Malta",
			"MH" => "Marshall Islands",
			"MQ" => "Martinique",
			"MR" => "Mauritania",
			"MU" => "Mauritius",
			"YT" => "Mayotte",
			"MX" => "Mexico",
			"FM" => "Micronesia, Federated States of",
			"MD" => "Moldova, Republic of",
			"MC" => "Monaco",
			"MN" => "Mongolia",
			"MS" => "Montserrat",
			"MA" => "Morocco",
			"MZ" => "Mozambique",
			"MM" => "Myanmar",
			"NA" => "Namibia",
			"NR" => "Nauru",
			"NP" => "Nepal",
			"NL" => "Netherlands",
			"AN" => "Netherlands Antilles",
			"NC" => "New Caledonia",
			"NZ" => "New Zealand",
			"NI" => "Nicaragua",
			"NE" => "Niger",
			"NG" => "Nigeria",
			"NU" => "Niue",
			"NF" => "Norfolk Island",
			"MP" => "Northern Mariana Islands",
			"NO" => "Norway",
			"OM" => "Oman",
			"PK" => "Pakistan",
			"PW" => "Palau",
			"PA" => "Panama",
			"PG" => "Papua New Guinea",
			"PY" => "Paraguay",
			"PE" => "Peru",
			"PH" => "Philippines",
			"PN" => "Pitcairn",
			"PL" => "Poland",
			"PT" => "Portugal",
			"PR" => "Puerto Rico",
			"QA" => "Qatar",
			"RE" => "Reunion",
			"RO" => "Romania",
			"RU" => "Russian Federation",
			"RW" => "Rwanda",
			"KN" => "Saint Kitts and Nevis",
			"LC" => "Saint Lucia",
			"VC" => "Saint Vincent and the Grenadines",
			"WS" => "Samoa",
			"SM" => "San Marino",
			"ST" => "Sao Tome and Principe",
			"SA" => "Saudi Arabia",
			"SN" => "Senegal",
			"SC" => "Seychelles",
			"SL" => "Sierra Leone",
			"SG" => "Singapore",
			"SK" => "Slovakia (Slovak Republic)",
			"SI" => "Slovenia",
			"SB" => "Solomon Islands",
			"SO" => "Somalia",
			"ZA" => "South Africa",
			"GS" => "South Georgia and the South Sandwich Islands",
			"ES" => "Spain",
			"LK" => "Sri Lanka",
			"SH" => "St. Helena",
			"PM" => "St. Pierre and Miquelon",
			"SD" => "Sudan",
			"SR" => "Suriname",
			"SJ" => "Svalbard and Jan Mayen Islands",
			"SZ" => "Swaziland",
			"SE" => "Sweden",
			"CH" => "Switzerland",
			"SY" => "Syrian Arab Republic",
			"TW" => "Taiwan",
			"TJ" => "Tajikistan",
			"TZ" => "Tanzania, United Republic of",
			"TH" => "Thailand",
			"TG" => "Togo",
			"TK" => "Tokelau",
			"TO" => "Tonga",
			"TT" => "Trinidad and Tobago",
			"TN" => "Tunisia",
			"TR" => "Turkey",
			"TM" => "Turkmenistan",
			"TC" => "Turks and Caicos Islands",
			"TV" => "Tuvalu",
			"UG" => "Uganda",
			"UA" => "Ukraine",
			"AE" => "United Arab Emirates",
			"GB" => "United Kingdom",
			"US" => "United States",
			"UM" => "United States Minor Outlying Islands",
			"UY" => "Uruguay",
			"UZ" => "Uzbekistan",
			"VU" => "Vanuatu",
			"VA" => "Vatican City State (Holy See)",
			"VE" => "Venezuela",
			"VN" => "Viet Nam",
			"VG" => "Virgin Islands (British)",
			"VI" => "Virgin Islands (U.S.)",
			"WF" => "Wallis and Futuna Islands",
			"EH" => "Western Sahara",
			"YE" => "Yemen",
			"RS" => "Serbia",
			"CD" => "The Democratic Republic of Congo",
			"ZM" => "Zambia",
			"ZW" => "Zimbabwe",
			"JE" => "Jersey",
			"BL" => "St. Barthelemy",
			"XU" => "St. Eustatius",
			"XC" => "Canary Islands",
			"ME" => "Montenegro"
		);
	}
}