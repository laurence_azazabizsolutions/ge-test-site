<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

get_instance()->load->iface('sensitiveuserdatamanipulatorinterface');

/**
 * @author Joshua Paylaga
 *
 */
class UserPersistence extends CI_Model implements SensitiveUserDataManipulatorInterface {
	
	/**
	 * @var TypedUserInterface
	 */
	private $user;
	
	/**
	 * Constructor. Load the persisted user if there is any.
	 */
	public function __construct() {
		parent::__construct();
		//$this->load->library('session');
		$this->load->model('user/user', 'u');
		$this->load->model('user/userfactory');
		if ($this->session->userdata('id')) {
			$this->setId($this->u, (int) $this->session->userdata('id'));
			$this->u->setFirstname($this->session->userdata('firstname'));
			$this->u->setMiddlename($this->session->userdata('middlename'));
			$this->u->setLastname($this->session->userdata('lastname'));
			$this->u->setUsername($this->session->userdata('username'));
			$this->u->setCompany($this->session->userdata('company'));
			$this->u->setCountry($this->session->userdata('country'));
			$this->u->setPhone($this->session->userdata('phone'));
			$this->u->setEmergencyphone($this->session->userdata('emergency_phone_number'));
			$this->u->setEmergencyname($this->session->userdata('emergency_name'));
			$this->u->setEmail($this->session->userdata('email'));
			$this->u->setAlteremail($this->session->userdata('alternative_email'));
			$this->u->setSalutation($this->session->userdata('salutation'));
			$this->u->setDateOfBirth($this->session->userdata('dateofbirth'));
			$this->u->setStatus($this->session->userdata('status'));
			$this->setStoredPassword($this->u, $this->session->userdata('password'));
			$this->userfactory->setUser($this->u);
			$this->user = $this->userfactory->buildInstance($this->session->userdata('type'));
		}
	}
	
	/**
	 * Persist a TypedUserInterface object.
	 * 
	 * @param TypedUserInterface $user
	 */
	public function persistUser(TypedUserInterface $user) {
		//dump_exit($user);
		if ($user instanceof TypedUserInterface) {
			if ($user->getUser()->stored()) {
				$this->user = $user;
				// dump($user);
				// die;
				$this->session->set_userdata(array(
					'id' => $user->getUser()->getId(),
					'firstname' => $user->getUser()->getFirstname(),
					'middlename' => $user->getUser()->getMiddlename(),
					'lastname' => $user->getUser()->getLastname(),
					'username' => $user->getUser()->getUsername(),
					'company' => $user->getUser()->getCompany(),
					'phone' => $user->getUser()->getPhone(),
					'emergency_phone_number' => $user->getUser()->getEmergencyphone(),
					'emergency_name' => $user->getUser()->getEmergencyname(),
					'country' => $user->getUser()->getCountry(),
					'email' => $user->getUser()->getEmail(),
					'alternative_email' => $user->getUser()->getAlteremail(),
					'password' => $user->getUser()->getPassword(),
					'salutation' => $user->getUser()->getSalutation(),
					'dateofbirth' => $user->getUser()->getDateOfBirth(),
					'status' => $user->getUser()->getStatus(),
					'type' => strtolower(get_class($user)),
				));
			//dump_exit($user->getUser()->getUsername());
			} //$user->getUser()->stored()
		} //$user instanceof TypedUserInterface

	}
	
	/**
	 * Get the persisted TypedUserInterface object.
	 * 
	 * @return Ambigous <NULL, TypedUserInterface>
	 */
	public function getPersistedUser() {
		return $this->user;
	}

	public function getUserType($userId){
		$this->db->select("type");
		$this->db->from("users");
		$this->db->where('id',$userId);
		$this->db->limit(1);
		return $this->db->get()->result();

	}
	
	/**
	 * Stop persisting the user.
	 */
	public function releaseUser() {
		$this->session->sess_destroy();
		$this->user = null;
	}
	
	/* (non-PHPdoc)
	 * @see IdManipulatorInterface::setId()
	 */
	public function setId($user, $id) {
		setprivateid($user, 'User', $id);
	}
	
	/* (non-PHPdoc)
	 * @see SensitiveUserDataManipulatorInterface::setStoredPassword()
	 */
	public function setStoredPassword(User $user, $storedPassword) {
		if ($user instanceof User) {
			$ref     = new ReflectionClass('User');
			$passref = $ref->getProperty('password');
			$passref->setAccessible(true);
			$passref->setValue($user, $storedPassword);
		} //$user instanceof User
	}
}

/* End of file userpersistence.php */
/* Location: ./application/models/userpersistence.php */