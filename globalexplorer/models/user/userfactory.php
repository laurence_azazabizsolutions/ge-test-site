<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @author Joshua Paylaga
 *
 */
class UserFactory extends CI_Model {
	
	/**
	 * @var User
	 */
	private $user;
	
	/**
	 * @var string
	 */
	private $types;
	
	/**
	 * Constructor
	 */
	public function __construct() {
		parent::__construct();
		$this->types = array(
			'customer',
			'agent',
			'provider',
			'admin'
		);
	}
	
	/**
	 * @return Ambigous <NULL, User>
	 */
	public function getUser() {
		return $this->user;
	}
	
	/**
	 * @param User $user
	 */
	public function setUser(User $user) {
		if ($user instanceof User) {
			$this->user = ($user->prepared() || $user->stored()) ? $user : null;
		}
	}
	
	/**
	 * @param string $type
	 * @return string|boolean
	 */
	public function buildInstance($type) {
		if ($this->user instanceof User) {
			if (in_array(strtolower($type), $this->types)) {
				$loadType = 'user/' . $type;
				$this->load->model($loadType);
				$returnInstance = clone $this->$type;
				$returnInstance->setUser($this->user);
				unset($this->$type);
				
				return $returnInstance;
			}
		}
		
		return false;
	}
	
	/**
	 * @return Ambigous <boolean, Customer>
	 */
	public function customer() {
		return $this->buildInstance('customer');
	}
	
	/**
	 * @return Ambigous <boolean, Agent>
	 */
	public function agent() {
		return $this->buildInstance('agent');
	}
	
	/**
	 * @return Ambigous <boolean, Provider>
	 */
	public function provider() {
		return $this->buildInstance('provider');
	}
	
	/**
	 * @return Ambigous <boolean, Admin>
	 */
	public function admin() {
		return $this->buildInstance('admin');
	}
}