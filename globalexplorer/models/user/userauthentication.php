<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

get_instance()->load->iface('authenticationinterface');

class UserAuthentication extends CI_Model implements AuthenticationInterface {
	
	private $storedUser;
	
	public function getStatus($email) {
		$this->load->model('user/userstorage');
		$this->userstorage->includeAll(true);
		$user = $this->userstorage->getByInformation(array(
			'email' => $email,
		));
		if (count($user) == 1) {
			$status = $user[0]->getUser()->getStatus();
			return $status;
		}
		return false;
	}

	/* (non-PHPdoc)
	 * @see AuthenticationInterface::setStored()
	 */
	public function setStored($stored) {
		$this->load->model('user/userstorage');
		// $this->userstorage->includePending(true); // Temporary
		$user = $this->userstorage->getByInformation(array(
			'email' => $stored,
		));
		if (count($user) == 1) {
			$this->storedUser = $user[0];
			return true;
		}
		return false;
	}

	/* (non-PHPdoc)
	 * @see AuthenticationInterface::verify()
	 */
	public function verify($password) {
		if ($this->getStoredUser()) {
			$input_password = hashmonster($password);
			
			$stored_password = $this->getStoredUser()->getUser()->getPassword();
			
			if ($stored_password === $input_password) {
				return true;
			}
		}
		
		return false;
	}
	
	public function getStoredUser() {
		return $this->storedUser;
	}
}