<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "home";
$route['404_override'] = 'errors/page_not_found';

$route['panel'] = 'panel/mainpanel';
$route['panel/(:any)'] = 'panel/mainpanel/$1';

//user
$route['panel/register'] = 'panel/panelregister/register_user';
$route['panel/get_user'] = 'panel/panelregister/get_user_details';
$route['panel/update_user'] = 'panel/panelregister/update_user';
//hotel
$route['panel/register_hotel'] = 'panel/panelregister/register_hotel';
$route['panel/get_hotel'] = 'panel/panelregister/get_hotel_details';
$route['panel/update_hotel'] = 'panel/panelregister/update_hotel';
$route['panel/register_hotelsurcharge'] = 'panel/panelregister/register_hotelsurcharge';
$route['panel/get_hotelsurcharge'] = 'panel/panelregister/get_hotelsurcharge_details';
$route['panel/update_hotelsurcharge'] = 'panel/panelregister/update_hotelsurcharge';
//package
$route['panel/register_package'] 	= 'panel/panelregister/register_package';
$route['panel/update_package'] 		= 'panel/panelregister/update_package';
$route['panel/add_packagehotels'] 	= 'panel/panelregister/add_packagehotels';
//room rates
$route['panel/add_hotelroomrate'] 		= 'panel/panelregister/add_hotelroomrate';
$route['panel/get_hotelroomrate'] 		= 'panel/panelregister/get_hotelroomrate_details';
$route['panel/update_hotelroomrate'] 	= 'panel/panelregister/update_hotelroomrate';
//room surcharge
$route['panel/add_hotelroomsurcharge'] 		= 'panel/panelregister/add_hotelroomsurcharge';
$route['panel/get_hotelroomsurcharge'] 		= 'panel/panelregister/get_hotelroomsurcharge_details';
$route['panel/update_hotelroomsurcharge'] 	= 'panel/panelregister/update_hotelroomsurcharge';
//package surcharge
$route['panel/create_packagesurcharge'] = 'panel/panelregister/create_packagesurcharge';
$route['panel/get_packagesurcharge'] 	= 'panel/panelregister/get_packagesurcharge_details';
$route['panel/update_packagesurcharge'] = 'panel/panelregister/update_packagesurcharge';

$route['panel/add_packageaddons'] 	= 'panel/panelregister/add_packageaddons';
$route['panel/get_addon'] 			= 'panel/panelregister/get_addon_details';
$route['panel/update_addon'] 		= 'panel/panelregister/update_addon';

$route['panel/update_permission'] 	  = 'panel/panelregister/update_permission';
$route['panel/update_permission_all'] = 'panel/panelregister/update_permission_all';
//booking
// $route['panel/gethotelroom_packagehotelid'] = 'panel/panelregister/gethotelroom_packagehotelid';
$route['panel/create_destination'] = 'panel/panelregister/create_destination';
$route['panel/edit_destination']   = 'panel/panelregister/edit_destination';
$route['panel/create_airportcode'] = 'panel/panelregister/create_airportcode';
$route['panel/edit_airportcode']   = 'panel/panelregister/edit_airportcode';

#lester test
$route['panel/step_detailstest'] = "panel/lestertest/sampletest";
#end of lester test

$route['contact'] = 'home/contact';
$route['about'] = 'home/about';
$route['jsonencode'] = 'home/package_jsonencode';
$route['result_package_jsonencode'] = 'home/result_package_jsonencode';
// $route['browse'] = 'home/browse';
$route['send_mail'] = 'home/send_mail';
$route['details1/(:num)'] = 'home/details1/$1';
$route['details2/(:num)'] = 'home/details2/$1';
$route['details3/(:num)'] = 'home/details3/$1';
$route['booking'] = 'home/create_booking';

$route["json/(:any)"] = "panel/booking_json/$1";
$route["general_json/(:any)"] = "panel/general_json/$1";

$route["browse/(:any)"] = "home/browse/$1";
/* End of file routes.php */
/* Location: ./application/config/routes.php */