<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @author Anuar
 *
 */
class Change extends MY_Controller {
	
	private $headDataInclude = NULL;

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->setAllAccess(true);
		parent::__construct();
		
		//	send mail
		$this->load->library('My_PHPMailer');
		$this->load->helper('email');
		$this->mail = new PHPMailer();
		$this->mail->SMTPDebug = 0;
		$this->mail->isSMTP();																// Set mailer to use SMTP
		$this->mail->Host = 'cot.cottoncare.com.sg';										// Specify main and backup SMTP servers
		$this->mail->SMTPAuth = true;														// Enable SMTP authentication
		$this->mail->Username = 'no-reply@budgetmaid.com.sg';								// SMTP username
		$this->mail->Password = 'noreply@123';												// SMTP password
		$this->mail->SMTPSecure = 'tls';													// Enable TLS encryption, `ssl` also accepted
		$this->mail->Port = 587;															// TCP port to connect to

    	$this->mail->addBCC('delabahan@gmail.com');
    	$this->mail->addBCC('jude@azazabizsolutions.com');
	}
	
	/**
	 * Default Controller action
	 */
	public function index() {

		//$this->load->view('login_changed');
		redirect('errors/restricted_area');
	}

    public function confirm_booking($arg = null, $action='confirm') {
		if (!$arg) return;
		$id = $this->uri->segment(5,0);
		$this->load->model('user/userstorage');
		$this->load->model('booking/booking');
		$this->load->model('user/userauthentication');
		$this->load->model('user/userpersistence');
		$this->load->database();
		$user_data = $this->userstorage->getById($id);
		$result = $this->db->get_where('confirmation_booking', array('hash' => $arg))->result_array();
		if(empty($user_data)){
			/*$this->flash_message('This user doesn\'t exist', TRUE, '');
			return;*/
			redirect('errors/restricted_area');
		}
		if (count($result) == 1) {
			$email = $user_data->getUser()->getEmail();
			if ($this->isLoggedIn()){
				$user = $this->userpersistence->getPersistedUser();
				if (!($user->getUser()->getId() == $id)){
					$this->userpersistence->releaseUser();
					if ($this->userauthentication->setStored($email)){
						$user = $this->userauthentication->getStoredUser();
						$this->userpersistence->persistUser($user);
					} else{
						redirect('errors/restricted_area');
					}
				}
			}else{
				if ($this->userauthentication->setStored($email)) {
						$user = $this->userauthentication->getStoredUser();
					$this->userpersistence->persistUser($user);
				} else {
					redirect('errors/restricted_area');
				}
			}

			if(@$user){
				$booking = $this->db->get_where('bookings', array('id' => $result[0]['booking_id']))->result_array();
				if(@$booking[0]['status'] == 'pending' || @$booking[0]['status'] == 'ammended'){
					if ($action === 'confirm'){
						$this->db->where('id',$result[0]['booking_id']);	
						$this->db->update('bookings', array('status' => 'confirmed','date_confirmed'=>date('Y-m-d')));
						$this->booking_confirmed($result[0]['booking_id'],$this->userpersistence->getPersistedUser());
						redirect('panel/package/'.$result[0]['booking_id']);
					}elseif ($action === 'hotel'){
						$this->db->where('id',$result[0]['booking_id']);
						$this->db->update('bookings', array('status' => 'change hotel'));
						$this->booking_hotel_full($result[0]['booking_id'],$this->userpersistence->getPersistedUser());
						redirect('panel/package/'.$result[0]['booking_id']);
					}elseif ($action === 'room'){
						$this->db->where('id',$result[0]['booking_id']);
						$this->db->update('bookings', array('status' => 'change room type'));
						$this->booking_room_full($result[0]['booking_id'],$this->userpersistence->getPersistedUser());
						redirect('panel/package/'.$result[0]['booking_id']);
					}elseif ($action === 'reject'){
						$this->db->where('id',$result[0]['booking_id']);
						$this->db->update('bookings', array('status' => 'rejected'));
						$this->booking_reject($result[0]['booking_id'],$this->userpersistence->getPersistedUser());
						redirect('panel/package/'.$result[0]['booking_id']);
					}elseif ($action === 'cancel'){
						$this->db->where('id',$result[0]['booking_id']);
						$this->db->update('bookings', array('status' => 'cancelled'));
						$this->booking_cancellation($result[0]['booking_id'],$this->userpersistence->getPersistedUser());
						redirect('panel/package/'.$result[0]['booking_id']);
					}	
				}else{
					redirect('errors/restricted_area_');
				}
			}else {
				// Bad Email
				/*$this->flash_message($ERR, TRUE, '');*/
				redirect('errors/restricted_area');
			}
		} else {
			// Bad Email
			/*$this->flash_message('Booking doesn\'t exist', TRUE, '');*/
			redirect('errors/restricted_area');
		}
	}

	public function booking_hotel_full($data_mapping,$id){
	 	$this->load->model('booking/booking');
	 	$mail = $this->mail;
        $mail->setFrom('no-reply@ge.com.sg', 'Global Explorer');						// Sender
    	//$mail->addReplyTo('no-reply@globlaexplorer.com', 'Global Explorer');			// Sender's optional
	    $mail->WordWrap = 50;                                 							// Set word wrap to 50 characters
	    $mail->isHTML(true);                                  							// Set email format to HTML
	 	$data=$this->booking->getByCancellation($data_mapping);
	    $mail->Subject = ''.$data[0]->bookedcode.' [change hotel] Global Explorer Booking Notification';
	    $recipients=$this->booking->getEmailadmin();
		$data_res=$this->booking->getByreject($id->getUser()->getId());
		$data_result['user_click']=$this->booking->getByreject($id->getUser()->getId());
		$data_result['mail_data']=$this->booking->getByCancellation($data_mapping);

		if($data[0]->agenttype == 'agent'){
    		$mail->ClearAddresses();
			$mail->ClearCCs();
			if($data[0]->agent_alter_email != "" || $data[0]->agent_alter_email != NULL){
				$wang=explode(',',$data[0]->agent_alter_email);
	    		for ($i=0; $i < count($wang); $i++) {
		     		$mail->addAddress($wang[$i],$data[0]->userfirstname ." ". $data[0]->userlastname);
		     		//$mail->addAddress('delabahan@gmail.com',$data[0]->userfirstname ." ". $data[0]->userlastname);
				}
			} 
		    $mail->addAddress($data[0]->agentemail,$data[0]->userfirstname ." ". $data[0]->userlastname);
		    //$mail->addAddress('delabahan@gmail.com',$data[0]->userfirstname ." ". $data[0]->userlastname);
		    $mail->Body    = $this->load->view('booking_hotel_full_agent',$data_result,TRUE);
	    	if(!$mail->send()) return false;
		}

		if($data[0]->providertype == 'provider'){
			if($data[0]->provideremail != $data_res[0]->useremail){
	    		$mail->ClearAddresses();
				$mail->ClearCCs();
				if($data[0]->provider_alter_email != "" || $data[0]->provider_alter_email != NULL){
					$wang=explode(',',$data[0]->provider_alter_email);
		    		for ($i=0; $i < count($wang); $i++) {
			     		$mail->addCC($wang[$i],$data[0]->providerfirstname ." ". $data[0]->providerlastname);
			     		//$mail->addCC('delabahan@gmail.com',$data[0]->providerfirstname ." ". $data[0]->providerlastname);
					}
				} 
				$mail->addAddress($data[0]->provideremail,$data[0]->providerfirstname ." ". $data[0]->providerlastname);// Add a recipient (Provider)
			    //$mail->addAddress('delabahan@gmail.com',$data[0]->providerfirstname ." ". $data[0]->providerlastname);
			    $mail->Body    = $this->load->view('booking_hotel_full_provider',$data_result,TRUE);
			    if(!$mail->send()) return false;
			}
		}

		if($data[0]->admintype == 'admin'){
			if($data[0]->adminemail != $data_res[0]->useremail){
	    		$mail->ClearAddresses();
				$mail->ClearCCs();
				if($data[0]->admin_alter_email != "" || $data[0]->admin_alter_email != NULL){
					$wang=explode(',',$data[0]->admin_alter_email);
		    		for ($i=0; $i < count($wang); $i++) {
			     		$mail->addCC($wang[$i],$data[0]->adminfirstname ." ". $data[0]->adminlastname);
			     		//$mail->addCC('delabahan@gmail.com',$data[0]->adminlastname);
					}
				} 
				$mail->addAddress($data[0]->adminemail,$data[0]->adminfirstname ." ". $data[0]->adminlastname);// Add a recipient (Provider)
			    //$mail->addAddress('delabahan@gmail.com',$data[0]->adminfirstname ." ". $data[0]->adminlastname);
			    $mail->Body    = $this->load->view('booking_hotel_full_admin',$data_result,TRUE);
			    if(!$mail->send()) return false;
			}
		}

		if($recipients != NULL || $recipients != ""){
     		foreach($recipients as $row){
	    		$mail->ClearAddresses();
				$mail->ClearCCs();
				//if($row->adminemail != $data[0]->adminemail){
					$data_result['recipient'] = @$row->adminfname.' '.@$row->adminlname;
		     		if($row->alteremail != '' || $row->alteremail != NULL) {
				     	$alternative_email=explode(',',$row->alteremail);
				     	for ($i=0; $i < count($alternative_email); $i++) {
				     		$mail->addCC($alternative_email[$i],$row->adminfirstname ." ". $row->adminlastname);
				     		//$mail->addCC('delabahan@gmail.com',$row->adminfirstname ." ". $row->adminlastname);
						}
				    }
			    	$mail->addAddress($row->adminemail,@$row->adminfname ." ".@$row->adminlname);
			    	//$mail->addAddress('delabahan@gmail.com',@$row->adminfname ." ".@$row->adminlname);
			    	$mail->Body    = $this->load->view('booking_hotel_full_all_admin',$data_result,TRUE);
			    	if(!$mail->send()) return false;
			   // }
     		}
     	}
	    $mail->AltBody = 'Booking Fully Booked [Global Explorer]';
		return true;
    }

public function booking_room_full($data_mapping,$id){
	 	$this->load->model('booking/booking');
	 	$mail = $this->mail;
        $mail->setFrom('no-reply@ge.com.sg', 'Global Explorer');						// Sender
    	//$mail->addReplyTo('no-reply@globlaexplorer.com', 'Global Explorer');			// Sender's optional
	    $mail->WordWrap = 50;                                 							// Set word wrap to 50 characters
	    $mail->isHTML(true);                                  							// Set email format to HTML
	 	$data=$this->booking->getByCancellation($data_mapping);
	    $mail->Subject = ''.$data[0]->bookedcode.' [change room] Global Explorer Booking Notification';
	    $recipients=$this->booking->getEmailadmin();
		$data_res=$this->booking->getByreject($id->getUser()->getId());
		$data_result['user_click']=$this->booking->getByreject($id->getUser()->getId());
		$data_result['mail_data']=$this->booking->getByCancellation($data_mapping);

		if($data[0]->agenttype == 'agent'){
    		$mail->ClearAddresses();
			$mail->ClearCCs();
			if($data[0]->agent_alter_email != "" || $data[0]->agent_alter_email != NULL){
				$wang=explode(',',$data[0]->agent_alter_email);
	    		for ($i=0; $i < count($wang); $i++) {
		     		$mail->addAddress($wang[$i],$data[0]->userfirstname ." ". $data[0]->userlastname);
		     		//$mail->addAddress('delabahan@gmail.com',$data[0]->userfirstname ." ". $data[0]->userlastname);
				}
			} 
		    $mail->addAddress($data[0]->agentemail,$data[0]->userfirstname ." ". $data[0]->userlastname);
		    //$mail->addAddress('delabahan@gmail.com',$data[0]->userfirstname ." ". $data[0]->userlastname);
		    $mail->Body    = $this->load->view('booking_room_full_agent',$data_result,TRUE);
	    	if(!$mail->send()) return false;
		}

		if($data[0]->providertype == 'provider'){
			if($data[0]->provideremail != $data_res[0]->useremail){
	    		$mail->ClearAddresses();
				$mail->ClearCCs();
				if($data[0]->provider_alter_email != "" || $data[0]->provider_alter_email != NULL){
					$wang=explode(',',$data[0]->provider_alter_email);
		    		for ($i=0; $i < count($wang); $i++) {
			     		$mail->addCC($wang[$i],$data[0]->providerfirstname ." ". $data[0]->providerlastname);
			     		//$mail->addCC('delabahan@gmail.com',$data[0]->providerfirstname ." ". $data[0]->providerlastname);
					}
				} 
				$mail->addAddress($data[0]->provideremail,$data[0]->providerfirstname ." ". $data[0]->providerlastname);// Add a recipient (Provider)
			    //$mail->addAddress('delabahan@gmail.com',$data[0]->providerfirstname ." ". $data[0]->providerlastname);
			    $mail->Body    = $this->load->view('booking_room_full_provider',$data_result,TRUE);
			    if(!$mail->send()) return false;
			}
		}

		if($data[0]->admintype == 'admin'){
			if($data[0]->adminemail != $data_res[0]->useremail){
	    		$mail->ClearAddresses();
				$mail->ClearCCs();
				if($data[0]->admin_alter_email != "" || $data[0]->admin_alter_email != NULL){
					$wang=explode(',',$data[0]->admin_alter_email);
		    		for ($i=0; $i < count($wang); $i++) {
			     		$mail->addCC($wang[$i],$data[0]->adminfirstname ." ". $data[0]->adminlastname);
			     		//$mail->addCC('delabahan@gmail.com',$data[0]->adminlastname);
					}
				} 
				$mail->addAddress($data[0]->adminemail,$data[0]->adminfirstname ." ". $data[0]->adminlastname);// Add a recipient (Provider)
			    //$mail->addAddress('delabahan@gmail.com',$data[0]->adminfirstname ." ". $data[0]->adminlastname);
			    $mail->Body    = $this->load->view('booking_room_full_admin',$data_result,TRUE);
			    if(!$mail->send()) return false;
			}
		}

		if($recipients != NULL || $recipients != ""){
     		foreach($recipients as $row){
	    		$mail->ClearAddresses();
				$mail->ClearCCs();
				//if($row->adminemail != $data[0]->adminemail){
					$data_result['recipient'] = @$row->adminfname.' '.@$row->adminlname;
		     		if($row->alteremail != '' || $row->alteremail != NULL) {
				     	$alternative_email=explode(',',$row->alteremail);
				     	for ($i=0; $i < count($alternative_email); $i++) {
				     		$mail->addCC($alternative_email[$i],$row->adminfirstname ." ". $row->adminlastname);
				     		//$mail->addCC('delabahan@gmail.com',$row->adminfirstname ." ". $row->adminlastname);
						}
				    }
			    	$mail->addAddress($row->adminemail,@$row->adminfname ." ".@$row->adminlname);
			    	//$mail->addAddress('delabahan@gmail.com',"asdf");
			    	$mail->Body    = $this->load->view('booking_room_full_all_admin',$data_result,TRUE);
			    	if(!$mail->send()) return false;
			   // }
     		}
     	}
	    $mail->AltBody = 'Booking Fully Booked [Global Explorer]';
		return true;
    }

public function booking_reject($data_mapping,$id){
	 	$this->load->model('booking/booking');
	 	$mail = $this->mail;
        $mail->setFrom('no-reply@ge.com.sg', 'Global Explorer');						// Sender
    	//$mail->addReplyTo('no-reply@globlaexplorer.com', 'Global Explorer');			// Sender's optional
	    $mail->WordWrap = 50;                                 							// Set word wrap to 50 characters
	    $mail->isHTML(true);                                  							// Set email format to HTML
	 	$data=$this->booking->getByCancellation($data_mapping);
	    $mail->Subject = ''.$data[0]->bookedcode.' [rejected] Global Explorer Booking Notification';
	    $recipients=$this->booking->getEmailadmin();
		$data_res=$this->booking->getByreject($id->getUser()->getId());
		$data_result['user_click']=$this->booking->getByreject($id->getUser()->getId());
		$data_result['mail_data']=$this->booking->getByCancellation($data_mapping);

		if($data[0]->agenttype == 'agent'){
    		$mail->ClearAddresses();
			$mail->ClearCCs();
			if($data[0]->agent_alter_email != "" || $data[0]->agent_alter_email != NULL){
				$wang=explode(',',$data[0]->agent_alter_email);
	    		for ($i=0; $i < count($wang); $i++) {
		     		$mail->addAddress($wang[$i],$data[0]->userfirstname ." ". $data[0]->userlastname);
				}
			} 
		    $mail->addAddress($data[0]->agentemail,$data[0]->userfirstname ." ". $data[0]->userlastname);
		    $mail->Body    = $this->load->view('booking_rejected_agent',$data_result,TRUE);
	    	if(!$mail->send()) return false;
		}

		if($data[0]->providertype == 'provider'){
			if($data[0]->provideremail != $data_res[0]->useremail){
	    		$mail->ClearAddresses();
				$mail->ClearCCs();
				if($data[0]->provider_alter_email != "" || $data[0]->provider_alter_email != NULL){
					$wang=explode(',',$data[0]->provider_alter_email);
		    		for ($i=0; $i < count($wang); $i++) {
			     		$mail->addCC($wang[$i],$data[0]->providerfirstname ." ". $data[0]->providerlastname);
					}
				} 
				$mail->addAddress($data[0]->provideremail,$data[0]->providerfirstname ." ". $data[0]->providerlastname);// Add a recipient (Provider)
			    $mail->Body    = $this->load->view('booking_rejected_provider',$data_result,TRUE);
			    if(!$mail->send()) return false;
			}
		}

		if($data[0]->admintype == 'admin'){
			if($data[0]->adminemail != $data_res[0]->useremail){
	    		$mail->ClearAddresses();
				$mail->ClearCCs();
				if($data[0]->admin_alter_email != "" || $data[0]->admin_alter_email != NULL){
					$wang=explode(',',$data[0]->admin_alter_email);
		    		for ($i=0; $i < count($wang); $i++) {
			     		$mail->addCC($wang[$i],$data[0]->adminfirstname ." ". $data[0]->adminlastname);
					}
				} 
				$mail->addAddress($data[0]->adminemail,$data[0]->adminfirstname ." ". $data[0]->adminlastname);// Add a recipient (Provider)
			    $mail->Body    = $this->load->view('booking_rejected_admin',$data_result,TRUE);
			    if(!$mail->send()) return false;
			}
		}

		if($recipients != NULL || $recipients != ""){
     		foreach($recipients as $row){
	    		$mail->ClearAddresses();
				$mail->ClearCCs();
				//if($row->adminemail != $data[0]->adminemail){
					$data_result['recipient'] = @$row->adminfname.' '.@$row->adminlname;
		     		if($row->alteremail != '' || $row->alteremail != NULL) {
				     	$alternative_email=explode(',',$row->alteremail);
				     	for ($i=0; $i < count($alternative_email); $i++) {
				     		$mail->addCC($alternative_email[$i],$row->adminfirstname ." ". $row->adminlastname);
						}
				    }
			    	$mail->addAddress($row->adminemail,@$row->adminfname ." ".@$row->adminlname);
			    	$mail->Body    = $this->load->view('booking_rejected_all_admin',$data_result,TRUE);
			    	if(!$mail->send()) return false;
			   // }
     		}
     	}
	    $mail->AltBody = 'Booking Rejected [Global Explorer]';
		return true;
    }

	public function booking_confirmed($data_mapping,$id){
        $this->load->model('booking/booking');
	 	$mail = $this->mail;
        $mail->setFrom('no-reply@ge.com.sg', 'Global Explorer');						// Senter
    	//$mail->addReplyTo('no-reply@globlaexplorer.com', 'Global Explorer');			// Sender's optional
	    $mail->WordWrap = 50;                                 							// Set word wrap to 50 characters
	    $mail->isHTML(true);                                  							// Set email format to HTML
	 	$data=$this->booking->getByCancellation($data_mapping);
	    $mail->Subject = ''.$data[0]->bookedcode.' [confirmed] Global Explorer Booking Notification';
		$recipients=$this->booking->getEmailadmin();
		$data_res=$this->booking->getByreject($id->getUser()->getId());
		$data_result['user_click']=$this->booking->getByreject($id->getUser()->getId());
		$data_result['mail_data']=$this->booking->getByCancellation($data_mapping);
		
		if($data[0]->agenttype == 'agent'){
    		$mail->ClearAddresses();
			$mail->ClearCCs();
			if($data[0]->agent_alter_email != "" || $data[0]->agent_alter_email != NULL){
				$wang=explode(',',$data[0]->agent_alter_email);
	    		for ($i=0; $i < count($wang); $i++) {
		     		$mail->addCC($wang[$i],$data[0]->userfirstname ." ". $data[0]->userlastname);
				}
			} 
		    $mail->addAddress($data[0]->agentemail,$data[0]->userfirstname ." ". $data[0]->userlastname);
		    $mail->Body    = $this->load->view('booking_confirmed_agent',$data_result,TRUE);
		    if(!$mail->send()) return false;
		}

		if($data[0]->providertype == 'provider'){
			if($data[0]->provideremail != $data_res[0]->useremail){
	    		$mail->ClearAddresses();
				$mail->ClearCCs();
				if($data[0]->provider_alter_email != "" || $data[0]->provider_alter_email != NULL){
					$wang=explode(',',$data[0]->provider_alter_email);
		    		for ($i=0; $i < count($wang); $i++) {
			     		$mail->addCC($wang[$i],$data[0]->providerfirstname ." ". $data[0]->providerlastname);
					}
				} 
				$mail->addAddress($data[0]->provideremail,$data[0]->providerfirstname ." ". $data[0]->providerlastname);// Add a recipient (Provider)
			    $mail->Body    = $this->load->view('booking_confirmed_provider',$data_result,TRUE);
			    if(!$mail->send()) return false;
			}
		}

		if($data[0]->admintype == 'admin'){
			if($data[0]->adminemail != $data_res[0]->useremail){
	    		$mail->ClearAddresses();
				$mail->ClearCCs();
				if($data[0]->admin_alter_email != "" || $data[0]->admin_alter_email != NULL){
					$wang=explode(',',$data[0]->admin_alter_email);
		    		for ($i=0; $i < count($wang); $i++) {
			     		$mail->addCC($wang[$i],$data[0]->adminfirstname ." ". $data[0]->adminlastname);
					}
				} 
				$mail->addAddress($data[0]->adminemail,$data[0]->adminfirstname ." ". $data[0]->adminlastname);// Add a recipient (Provider)
			    $mail->Body    = $this->load->view('booking_confirmed_admin',$data_result,TRUE);
			    if(!$mail->send()) return false;
			}
		}

		if($recipients != NULL || $recipients != ""){
     		foreach($recipients as $row){
	    		$mail->ClearAddresses();
				$mail->ClearCCs();
				//if($row->adminemail != $data[0]->adminemail){
					$data_result['recipient'] = @$row->adminfname.' '.@$row->adminlname;
		     		if($row->alteremail != '' || $row->alteremail != NULL) {
				     	$alternative_email=explode(',',$row->alteremail);
				     	for ($i=0; $i < count($alternative_email); $i++) {
				     		$mail->addCC($alternative_email[$i],$row->adminfirstname ." ". $row->adminlastname);
						}
				    }
			    	$mail->addAddress($row->adminemail,@$row->adminfname ." ".@$row->adminlname);
			    	$mail->Body    = $this->load->view('booking_confirmed_all_admin',$data_result,TRUE);
			    	if(!$mail->send()) return false;
			    //}
     		}
     	}
     	$mail->AltBody = 'Booking Confirmed [Global Explorer]';
		return true;
    }

    public function booking_cancellation($data_mapping,$id){
        $this->load->model('booking/booking');
	 	$mail = $this->mail;
        $mail->setFrom('no-reply@ge.com.sg', 'Global Explorer');						// Sender
    	//$mail->addReplyTo('no-reply@globlaexplorer.com', 'Global Explorer');			// Sender's optional
	    $mail->WordWrap = 50;                                 							// Set word wrap to 50 characters
	    $mail->isHTML(true);                                  							// Set email format to HTML
	 	$data=$this->booking->getByCancellation($data_mapping);
	    $mail->Subject = ''.$data[0]->bookedcode.' [cancelled] Global Explorer Booking Notification';
	    $recipients=$this->booking->getEmailadmin();
		$data_res=$this->booking->getByreject($id->getUser()->getId());
		$data_result['user_click']=$this->booking->getByreject($id->getUser()->getId());
		$data_result['mail_data']=$this->booking->getByCancellation($data_mapping);

		if($data[0]->agenttype == 'agent'){
    		$mail->ClearAddresses();
			$mail->ClearCCs();
			if($data[0]->agent_alter_email != "" || $data[0]->agent_alter_email != NULL){
				$wang=explode(',',$data[0]->agent_alter_email);
	    		for ($i=0; $i < count($wang); $i++) {
		     		$mail->addCC($wang[$i],$data[0]->userfirstname ." ". $data[0]->userlastname);
				}
			}
		    $mail->addAddress($data[0]->agentemail,$data[0]->userfirstname ." ". $data[0]->userlastname);
		    $mail->Body    = $this->load->view('booking_cancellation_agent',$data_result,TRUE);
		    if(!$mail->send()) return false;
		}

		if($data[0]->providertype == 'provider'){
			if($data[0]->provideremail != $data_res[0]->useremail){
	    		$mail->ClearAddresses();
				$mail->ClearCCs();
				if($data[0]->provider_alter_email != "" || $data[0]->provider_alter_email != NULL){
					$wang=explode(',',$data[0]->provider_alter_email);
		    		for ($i=0; $i < count($wang); $i++) {
			     	$mail->addCC($wang[$i],$data[0]->providerfirstname ." ". $data[0]->providerlastname);
					}
				} 
				$mail->addAddress($data[0]->provideremail,$data[0]->providerfirstname ." ". $data[0]->providerlastname);// Add a recipient (Provider)
			    $mail->Body    = $this->load->view('booking_cancellation_provider',$data_result,TRUE);
			    if(!$mail->send()) return false;
			}
		}

		if($data[0]->admintype == 'admin'){
			if($data[0]->adminemail != $data_res[0]->useremail){
	    		$mail->ClearAddresses();
				$mail->ClearCCs();
				if($data[0]->admin_alter_email != "" || $data[0]->admin_alter_email != NULL){
					$wang=explode(',',$data[0]->admin_alter_email);
		    		for ($i=0; $i < count($wang); $i++) {
			     	$mail->addCC($wang[$i],$data[0]->adminfirstname ." ". $data[0]->adminlastname);
					}
				} 
				$mail->addAddress($data[0]->adminemail,$data[0]->adminfirstname ." ". $data[0]->adminlastname);// Add a recipient (Provider)
			    $mail->Body    = $this->load->view('booking_cancellation_admin',$data_result,TRUE);
			    if(!$mail->send()) return false;
			}
		}

		if($recipients != NULL || $recipients != ""){
     		foreach($recipients as $row){
	    		$mail->ClearAddresses();
				$mail->ClearCCs();
				//if($row->adminemail != $data[0]->adminemail){
					$data_result['recipient'] = @$row->adminfname.' '.@$row->adminlname;
		     		if($row->alteremail != '' || $row->alteremail != NULL) {
				     	$alternative_email=explode(',',$row->alteremail);
				     	for ($i=0; $i < count($alternative_email); $i++) {
				     		$mail->addCC($alternative_email[$i],$row->adminfirstname ." ". $row->adminlastname);
						}
				    }
			    	$mail->addAddress($row->adminemail,@$row->adminfname ." ". @$row->adminlname);
		    	$mail->Body    = $this->load->view('booking_cancellation_all_admin',$data_result,TRUE);
		    	if(!$mail->send()) return false;
		    	//}
     		}
     	}
     	$mail->AltBody = 'Booking Cancelled [Global Explorer]';
		return true;
    }
}