<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Dev extends CI_Controller {

	public function test() {
		echo "IT WORKSH!";
	}
	
	public function createadmin() {
		$this->load->model('user/user');
		$this->load->model('user/userfactory');
		$this->load->model('user/userstorage');
		
		$this->user->setSalutation('mr.');
		$this->user->setFirstname('Edmund');
		$this->user->setLastname('Wan');
		$this->user->setCompany('Global Explorer');
		$this->user->setEmail('edmund@globalexplorer.com.sg');
		$this->user->setPassword('qwerty12!');
		$this->user->setCountry('sg');
		$this->user->setPhone('123456789');
		$this->userfactory->setUser($this->user);
		$this->userstorage->store($this->userfactory->admin());
		
		echo "Admin created.";
	}
	
	public function createagent() {
		$this->load->model('user/user');
		$this->load->model('user/userfactory');
		$this->load->model('user/userstorage');
		
		// $this->user->setSalutation('ms.');
		// $this->user->setFirstname('Kirstin');
		// $this->user->setLastname('Rubica');
		// $this->user->setCompany('Global Explorer');
		// $this->user->setEmail('kirstinrubica@gmail.com');
		// $this->user->setPassword('qwerty12!');
		// $this->user->setCountry('ph');
		// $this->user->setPhone('123456789');
		
		$this->user->setSalutation('mr.');
		$this->user->setFirstname('Anuar');
		$this->user->setLastname('Delabahan');
		$this->user->setCompany('Google');
		$this->user->setEmail('anuar@azazabizsolutions.com');
		$this->user->setPassword('qwerty12!');
		$this->user->setCountry('ph');
		$this->user->setPhone('123456789');
		
		$this->userfactory->setUser($this->user);
		$this->userstorage->store($this->userfactory->agent());
		echo "Agent created.";
	}

	public function createprovider() {
		$this->load->model('user/user');
		$this->load->model('user/userfactory');
		$this->load->model('user/userstorage');
		
		$this->user->setSalutation('mr.');
		$this->user->setFirstname('Greeg');
		$this->user->setLastname('Gimongala');
		$this->user->setCompany('IBM');
		$this->user->setEmail('greeg@azazabizsolutions.com');
		$this->user->setPassword('qwerty12!');
		$this->user->setCountry('ph');
		$this->user->setPhone('123456789');

		// $this->user->setSalutation('mr.');
		// $this->user->setFirstname('Joshua');
		// $this->user->setLastname('Paylaga');
		// $this->user->setCompany('Facebook');
		// $this->user->setEmail('joshua@azazabizsolutions.com');
		// $this->user->setPassword('qwerty12!');
		// $this->user->setCountry('ph');
		// $this->user->setPhone('123456789');
		
		$this->userfactory->setUser($this->user);
		$this->userstorage->store($this->userfactory->provider());
		
		echo "Provider created.";
	}
	
	public function usertest() {
		$this->load->model('user/userstorage');
		$this->userstorage->includePending(true);
		$this->userstorage->removeByInformation(array('type' => 'admin'));
	}
	
	public function resetdbai() {
		$this->load->database();
		$results = $this->db->query("SHOW TABLES")->result_array();
		$tables  = array();
		foreach ($results as $result) {
			$table = $result['Tables_in_globalexplorer_db'];
			$this->db->query("ALTER TABLE {$table} AUTO_INCREMENT = 1");
		}
	}
	
	public function dbrecreate() {
		$this->load->database();
		$this->load->dbforge();
		$this->load->dbutil();
		
		$dbname = 'globalexplorer_db';
		
		if ($this->dbutil->database_exists($dbname)) {
			$this->dbforge->drop_database($dbname);
			echo "Database Dropped\n";
		}
		
		if ($this->dbforge->create_database($dbname)) {
			echo "Database Created\n";
			$this->db->query("use {$dbname}");
			$templine = '';
			$lines = file('../sql/globalexplorer_db.sql');
			echo "Importing .sql file\n";
			foreach ($lines as $line)
			{
				// Skip it if it's a comment
				if (substr($line, 0, 2) == '--' || $line == '')
				    continue;

				$templine .= $line;
				if (substr(trim($line), -1, 1) == ';')
				{
				    $this->db->query($templine);
				    $templine = '';
				}
			}
			
			$this->resetdbai();
			echo "Tables imported successfully\n";
		}
	}
}