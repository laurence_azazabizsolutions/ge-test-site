<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @author Joshua Paylaga
 *
 */
class Errors extends MY_Controller {

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->setAllAccess(true);
		parent::__construct();
	}
	
	/**
	 * Default Controller action
	 */
	public function index() {
		// $this->publicView();
	}
	
	/**
	 * ERROR 404
	 */
	public function page_not_found() {
		$this->load->view('errors/404');
	}
	
	
	/**
	 * LOGIN ERROR
	 */
	public function restricted_area() {
		$this->load->view('errors/restricted');
	}

	/**
	 * Confirmed ERROR
	 */
	public function restricted_area_() {
		$this->load->view('errors/mail_restricted');
	}

	/**
	 * Confirmed ERROR
	 */
	public function restricted_area_for_agent() {
		$this->load->view('errors/restricted');
	}

	/**
	 * Confirmed ERROR
	 */
	public function restricted_area_for_admin() {
		$this->load->view('errors/restricted');
	}

	/**
	 * Confirmed ERROR
	 */
	public function restricted_area_for_customer() {
		$this->load->view('errors/restricted');
	}/**
	 * Confirmed ERROR
	 */
	public function restricted_area_verification() {
		$this->load->view('errors/mail_restricted_verification');
	}
}