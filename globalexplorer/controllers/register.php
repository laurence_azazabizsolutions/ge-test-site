<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @author Joshua Paylaga
 *
 */
class Register extends MY_Controller {
	
	public function __construct() {
		$this->setPublicAccessible(true);
		parent::__construct();
		
		//	send mail
		$this->load->library('My_PHPMailer');
		$this->load->helper('email');
		$this->mail = new PHPMailer();
		$this->mail->SMTPDebug = 0;
		$this->mail->isSMTP();																// Set mailer to use SMTP
		$this->mail->Host = 'cot.cottoncare.com.sg';										// Specify main and backup SMTP servers
		$this->mail->SMTPAuth = true;														// Enable SMTP authentication
		$this->mail->Username = 'no-reply@budgetmaid.com.sg';								// SMTP username
		$this->mail->Password = 'noreply@123';												// SMTP password
		$this->mail->SMTPSecure = 'tls';													// Enable TLS encryption, `ssl` also accepted
		$this->mail->Port = 587;															// TCP port to connect to

    	$this->mail->addBCC('delabahan@gmail.com');
    	$this->mail->addBCC('jude@azazabizsolutions.com');
	}
	
	public function customer() {
		$this->load->database();
		$this->load->model('user/user');
		$this->load->model('user/userfactory');
		$this->load->model('user/userstorage');
		$this->load->helper('email');
		$userdata = $this->input->post();
		$email = $userdata['email'];
		if (valid_email($email))
		{
			if ($userdata['password'] == $userdata['confirm_password'] ) {
				$this->user->setSalutation( $userdata['salutation'] );
				$this->user->setFirstname( $userdata['firstname'] );
				$this->user->setMiddlename( $userdata['middlename'] );
				$this->user->setLastname( $userdata['lastname'] );
				$this->user->setCompany( $userdata['company'] );
				$this->user->setPhone( $userdata['phone'] );
				$this->user->setEmail( $email );
				//$this->user->setAlteremail( $userdata['alteremail'] );
				$this->user->setCountry( $userdata['country'] );
				$this->user->setPassword( $userdata['password'] );
				$this->userfactory->setUser( $this->user );
				$customer = $this->userfactory->customer();
				$match = $this->db->get_where('users', array(
					'email' => $email,
				));
				
				if ($match->num_rows() <= 0) {
					$this->userstorage->store($customer);
					$hash = null;
					$link = null;
					//if ($this->user->stored()) {
						// Create string to hash
						$toHash = sprintf("%d_%s_%s",$this->user->getId(),
													 $this->user->getEmail(),
													 $this->user->getPassword());
						// Use hashmonster to create hash
						$hash = hashmonster($toHash);
						$this->load->database();
						$this->db->insert('confirmation', array(
							'user_id' =>$this->user->getId(),
							'hash' => $hash,
							'timestamp' => time(),
						));
						
						// Create link
						$link = base_url('register/confirmation/' . $hash);
						$conc = '';
						if($this->email_confirmation($link, $customer)){
							$conc = ' An email verification was sent. Please check your email.';
						}

						echo json_encode(
							array(
								'alert_type'=>'Success',
								'message'=>'Register Successful.' . $conc
							)
						);
					/*}else{
						echo json_encode(
							array(
								'alert_type'=>'Error',
								'message'=>'Registration failed. Please try again.' . $conc
							)
						);
					}*/
					
				} else {
					echo json_encode(
						array(
							'alert_type'=>'Error',
							'message'=>'Email already exists.'
						)
					);
				}
			} else {
				echo json_encode(
					array(
						'alert_type'=>'Error',
						'message'=>'Passwords do not match'
					)
				);
			}
		}
	}
	
	public function confirmation($arg = null) {
		if (!$arg) return;
		$this->load->database();
		$result = $this->db->get_where('confirmation', array('hash' => $arg))->result_array();
		$result1 = $this->db->get_where('users', array('status' =>'pending','id'=>$result[0]['user_id']))->result_array();
		if (@$result1 != "") {
			if(@$result1[0]['status'] == 'pending'){
				$this->db->where('id',$result[0]['user_id']);
				$this->db->update('users', array('status' => 'active'));
				$this->flash_message('Successfully confirmed your email', TRUE, 'home/index');
			}else redirect('errors/restricted_area_verification');
		} else $this->flash_message('Could not verify your email. Please try again.', TRUE, 'home/index');
	}

	public function flash_message($message, $do_redirect=FALSE, $redirect_location=''){
		$this->session->set_flashdata('confirmation_message', $message);
		if($do_redirect) redirect($redirect_location);
	}


	public function email_confirmation($link='',$customer){
     	if(!$customer instanceof Customer) return;
	    $data['email'] = $customer->getUser()->getEmail();
	    $data['firstname'] = $customer->getUser()->getFirstname();
	    $data['lastname'] = $customer->getUser()->getLastname();
	    $data['link'] = $link;
        $mail = $this->mail;
		$mail->setFrom('no-reply@ge.com.sg', 'Global Explorer');						// Sender
	    $mail->addAddress($data['email'], $data['firstname'].' '.$data['lastname']);	// Add a recipient
	    $mail->WordWrap = 50;															// Set word wrap to 50 characters
	    $mail->isHTML(true);															// Set email format to HTML
	    $mail->Subject = 'Email Confirmation [Global Explorer]';
	    $mail->Body    = $this->load->view('email_confirmation',$data,TRUE);

    	$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

	    if(!$mail->send()) return false;
	    else return true;
    }

    public function register_user() {

		$this->load->model('user/user');
		$this->load->model('user/userfactory');
		$this->load->model('user/userstorage');

		$userdata = $this->input->post();
		$regtime      = time();
		
		if ($userdata['password'] == $userdata['confirm_password'] ) {
			$this->user->setSalutation( $userdata['salutation'] );
			$this->user->setFirstname( $userdata['firstname'] );
			$this->user->setMiddlename( $userdata['middlename'] );
			$this->user->setLastname( $userdata['lastname'] );
			$this->user->setCompany( $userdata['company'] );
			$this->user->setPhone( $userdata['phone'] );
			$this->user->setEmail( $userdata['email'] );
			$this->user->setUsername( $userdata['firstname'] );
			$this->user->setCountry( $userdata['country'] );
			$this->user->setPassword( $userdata['password'] );
			$this->userfactory->setUser( $this->user );
			$user = $this->userfactory->buildInstance( 'agent' );
			$match = $this->db->get_where('users', array(
				'email' =>$userdata['email'],
				));
			//match_user
			$match_user = $this->db->get_where('users', array(
				'username' =>$userdata['firstname'],
				));
			if ($match_user->num_rows() <= 0) {
				if ($match->num_rows() <= 0) {
					$userToInsert = array(
						'firstname' => $this->user->getFirstname(),
						'middlename' => ($this->user->getMiddlename()) ? $this->user->getMiddlename() : null,
						'lastname' => $this->user->getLastname(),
						'email' =>$this->user->getEmail(),
						'username' => $this->user->getUsername(),
						'phone' => $this->user->getPhone(),
						'type' => 'agent',
						'regtime' => $regtime,
						'country' => $this->user->getCountry(),
						'salutation' => $this->user->getSalutation(),
						'status' => 'pending'
					);
					$user_id = $this->userstorage->storeAgent($userToInsert,$userdata['company'],$this->user->getPassword());
					$conc = '';
					/*if ($this->user->stored()) {*/
					// Create string to hash
						$toHash = sprintf("%d_%s_%s",$this->user->getId(),
							$this->user->getEmail(),
							$this->user->getPassword());

					// Use hashmonster to create hash
						$hash = hashmonster($toHash);
						$this->load->database();
						$this->db->insert('confirmation', array(
							'user_id' => $user_id,
							'hash' => $hash,
							'timestamp' => time(),
							));

					// Create link
						$link = base_url('register/confirmation/' . $hash);

						if ($this->email_confirmation_agent($link,$userToInsert)) {
							$conc = ' An email verification was sent.';
						}

						$this->flash_message('Successfully registered. '.$conc, TRUE, 'home/index');
						/*} else {
						echo json_encode(
						array(
						'alert_type'=>'Error',
						'message'=>'Registration failed. Please try again.' . $conc
						)
						);
						}*/
						} else {
							$this->flash_message('Email already exists.', TRUE, 'home/index');
						}
					} else {
						$this->flash_message('Username already exists.', TRUE, 'home/index');
					}
				} else {
					$this->flash_message('Passwords do not match.', TRUE, 'home/index');
				}
	}


	public function email_confirmation_agent($link='',$agent){
	    $data['email'] = $agent['email'];
	    $data['firstname'] = $agent['firstname'];
	    $data['lastname'] = $agent['lastname'];
	    $data['link'] = $link;
        $mail = $this->mail;
		$mail->setFrom('no-reply@ge.com.sg', 'Global Explorer');						// Sender
	    $mail->addAddress($data['email'], $data['firstname'].' '.$data['lastname']);	// Add a recipient
	    $mail->WordWrap = 50;															// Set word wrap to 50 characters
	    $mail->isHTML(true);															// Set email format to HTML
	    $mail->Subject = 'Email Confirmation [Global Explorer]';
	    $mail->Body    = $this->load->view('email_confirmation',$data,TRUE);

    	$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

	    if(!$mail->send()) return false;
	    else return true;
    }

}