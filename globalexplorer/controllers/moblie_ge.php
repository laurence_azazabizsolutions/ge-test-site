<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @author Master Ariel
 *
 */
class Mobile_ge extends MY_Controller {

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->setAllAccess(true);
		parent::__construct();
	}

	public function index(){
		$this->load->model('booking/booking');
		$id = 1;
		$data['hotel_name_'] = $this->booking->package_hotel_($id);
		$this->load->view('panel/mainpanel/append_hotels_name',$data);
	}
}


	