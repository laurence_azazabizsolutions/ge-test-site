<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Access_mobile extends MY_Controller {
	
	public function __construct() {
		$this->setAllAccess(true);
		parent::__construct();
		header('Access-Control-Allow-Origin: *');
	}


	public function login2()
	{
		$this->load->model('user/userauthentication');
		$this->load->model('user/userstorage');
		if($this->input->post('encLogin_secret') === '0ff9346b4edc8dc033bff30762bc3c15d465d3f')
		{
			$uname=$this->input->post('chrUser_name');
			$username=$this->userstorage->getByUser($uname);
			if(@$username[0]->email){
				$email = $username[0]->email;
			}elseif(@$username[0]->email == $uname){
				$email = $uname;
			}else{
				die(json_encode('You have entered an invalid username'));
				//$this->flash_message('You have entered an invalid username', TRUE, '');
			}

			$password = $this->input->post('passUser_pass');

			if ($email && $password) {
				$status = $this->userauthentication->getStatus($email);

				if($status !== false){
					if ($status == 'active') {

						if ($this->userauthentication->setStored($email)) {

							if ($this->userauthentication->verify($password)) {
								$user = $this->userauthentication->getStoredUser();
								
								if ($user instanceof TypedUserInterface) {
									$this->userpersistence->persistUser($user);
									// dump_exit($user);
									//dump_exit($this->session->userdata('username'));
									if ($user instanceof Customer) {
										$this->userpersistence->releaseUser();
										//redirect('errors/restricted_area_for_customer');
									}else{
										$data = array(
											'status' =>$this->userauthentication->verify($password),
											'id'  => $this->session->userdata('id'),
											'firstname' => $this->session->userdata('firstname'),
											'lastname' => $this->session->userdata('lastname'),
											'type' => $this->session->userdata('type'),
											);
										die(json_encode($data));
									}
								}
							} else {
								// Bad Password
								die(json_encode('You have entered an invalid password'));
							}
						} else {
							// Bad Email
								//die(json_encode('You have entered an invalid password'));
						}
					} elseif ($status == 'pending') {
						//$this->flash_message('Pending Account: Please confirm this email', TRUE, '');
					} else {
						//$this->flash_message('Log in not allowed. User has been '. $status .'.', TRUE, '');
					}
				} else {
					//$this->flash_message('You have entered an invalid email', TRUE, '');
				}
			}
		}
		else
		{
			echo 'restrict !';
		}
	}
	// public function getPassword($user_id)
	// {
	// 	$id = (int)$user_id;
	// 	$this->db->where('id',$id);
	// 	$this->db->from('passwords');
	// 	$result = $this->db->get()->result();
	// 	return $result;

	// }
}