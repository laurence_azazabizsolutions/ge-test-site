<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	// NO MODEL AT THE MOMENT ------------------------------------
	// UPDATE AFTER DEADLINE

class Agent_mobile extends MY_Controller {
	
	public function __construct() {
		$this->setAllAccess(true);
		parent::__construct();
		$this->load->database();
		header('Access-Control-Allow-Origin: *');
		$this->load->model('booking/booking');
		$this->load->model('user/user');

		$this->load->library('My_PHPMailer');
		$this->load->helper('email');
		$this->mail = new PHPMailer();
		$this->mail->SMTPDebug = 0;
		$this->mail->isSMTP();																// Set mailer to use SMTP
		$this->mail->Host = 'cot.cottoncare.com.sg';										// Specify main and backup SMTP servers
		$this->mail->SMTPAuth = true;														// Enable SMTP authentication
		$this->mail->Username = 'no-reply@budgetmaid.com.sg';								// SMTP username
		$this->mail->Password = 'noreply@123';												// SMTP password
		$this->mail->SMTPSecure = 'tls';													// Enable TLS encryption, `ssl` also accepted
		$this->mail->Port = 587;

	}

	public function getBooking()
	{
		if($this->input->post('encLogin_secret') === '0ff9346b4edc8dc033bff30762bc3c15d465d3f')
		{
			$search_array = false;
			$filter_arr = false;
			$start = false;
			$per_page = 0;
			$userId = $this->input->post('id');
			$userType = $this->input->post('type');

			if($userType == 'admin'){
				//$data['selectall_data']  = $this->booking->getAllBookingRecords_page(false,false,$search_array,$filter_arr);
				$data['selectall_data']  = $this->booking->getAllBookingRecords_page($start,$per_page,$search_array,$filter_arr);
			}

			else if($userType == 'provider'){
				//$data['selectall_data']  = $this->booking->getAllBookingRecordsProvider_page($userId,false,false,$search_array,$filter_arr);
				$data['selectall_data']  = $this->booking->getAllBookingRecordsProvider_page($userId);
			}

			else if($userType == 'agent' || $userType === 'customer'){
				//$data['selectall_data']  = $this->booking->getAllBookingRecordsAgent_page($userId,$start,false,$search_array,$filter_arr);
				$data['selectall_data']  = $this->booking->getAllBookingRecordsAgent_page($userId);
				
			}

			$hotelname =  array();

			for ($index=0; $index < count($data['selectall_data']); $index++) { 
				$temp = array(
							'id' => $data['selectall_data'][$index]->BOOKID, 
							'hotelname' => $this->getHotelName2($data['selectall_data'][$index]->BOOKINGCODE), 
							'bookdate' => $this->getBookDate($data['selectall_data'][$index]->BOOKINGCODE) 
							);
				array_push($hotelname, $temp);
			}

			$data['hotels'] = $hotelname;

			// $data['selectall_data']->
			// dump($data['hotels']);
			die(json_encode($data));
		}
		else
		{
			echo 'restrict !';
		}
	}

	public function getHotelName2($bcode)
	{
		// if($this->input->post('encLogin_secret') === '0ff9346b4edc8dc033bff30762bc3c15d465d3f')
		// {
			$booking_id = $bcode;
			$result = $this->booking->getHotelName($booking_id);
			$hotelname = $result[0]['name'];
		    return $hotelname;
		// }
		// else
		// {
		// 	echo 'restrict !';
		// }
	}

	public function getBookDate($bcode)
	{
		// if($this->input->post('encLogin_secret') === '0ff9346b4edc8dc033bff30762bc3c15d465d3f')
		// {
			$booking_id = $bcode;
			$result = $this->booking->getHotelName($booking_id);
			$hotelname = $result[0]['date_booked'];
		    return $hotelname;
		// }
		// else
		// {
		// 	echo 'restrict !';
		// }
	}

	public function package($booking_id=false) {
		$this->load->model("booking/booking");

		$booking_id = $this->input->post('booking_id');
		//$checker = $this->booking->getBookingCheckerAccount(@$booking_id);
		//$checker_agent_account = $this->booking->getBookingCheckerAccountAgent(@$booking_id);
		$user_data = $this->userpersistence->getPersistedUser();


		//if(get_class($user_data) === 'Provider' || @$checker_agent_account[0]['user_id'] == $user_data->getUser()->getId() || @$checker[0]['user_id'] == $user_data->getUser()->getId()){
			$code = $this->booking->getBookingCode(@$booking_id);
			@$booking_ids = $this->booking->getBookingIDs($code[0]["code"]);

			$booking_summary = false;
			if(count($booking_ids)){
				for($i=0;$i<count($booking_ids);$i++){
					$booking_summary[] = $this->getSummaryView($booking_ids[$i]["id"],$i);
				}
			}

			$this->panelView(null, array(
				"booking_summary"=>$booking_summary
			));
		// }
		// else{
		// 	redirect('errors/restricted_area');
		// }
	}

	public function getAgents(){
		// if($this->input->post('encLogin_secret') === '0ff9346b4edc8dc033bff30762bc3c15d465d3f')
		// {
			$this->db->select(' id, firstname ');
			$this->db->from('users'); 
			$data['agent'] = $this->db->get()->result();

			die(json_encode($data));
		// }
		// else
		// {
		// 	echo 'restrict !';
		// }
	}

	public function getSummaryView($booking_id,$show_hide){
		$this->load->model('booking/booking');
		$this->load->model("booking/bookingstorage");
		$this->load->model('user/user');
		$countries_usr    = $this->user->getAllCountries();
		$summary_data     =$this->booking->getBysummary($booking_id);
		$gettotalnights   =$this->booking->getBysummaryPackage($booking_id);
		$summary_guest    =$this->booking->getByGuest($booking_id);
		$provider_package =$this->booking->getBysummaryCompany($booking_id);
		$summary_datas	  =$this->booking->getBysummaryPackage($booking_id);
		$summary_dataBooking =$this->booking->getBysummaryPackageAddHotelBooking($booking_id);


		#get the individual summaries ----------------------------------------------
		$package_summary   = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"type"=>"0package_surcharge"));
		$addon_summary     = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"type"=>"4addon"));
		$extension_summary = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"type"=>"5extension"));

        #fetch the destination IDs ----------------------------------------------
        $destination_summary = $this->bookingstorage->getSummaryDestinations($booking_id);

        for($i=0;$i<count($destination_summary);$i++){
			$destination     = $destination_summary[$i];
			#fetch the hotel, room surcharge, room summaries ----------------------------------------------
			$hotel_summary[] = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"destination_id"=>$destination["destination_id"],"type"=>"1hotel_surcharge"));
			$rs_summary[]    = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"destination_id"=>$destination["destination_id"],"type"=>"2room_surcharge"));
			$r_summary[]     = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"destination_id"=>$destination["destination_id"],"type"=>"3room_payment"));
        }

        $passengers 	=	$this->booking->getPassengers($booking_id);

		$user_data = $this->userpersistence->getPersistedUser();

		$data['countries_usr']        = $countries_usr;
		$data['user']                 =$user_data;
		$data['data_summary']         =$summary_data;
		$data['guest_summary']        =$summary_guest;
		$data['package_total_nights'] =$gettotalnights;
		$data['provider_summary']     =$provider_package;
		$data['package_summary']      =@$package_summary;
		$data['addon_summary']        =@$addon_summary;
		$data['extension_summary']    =@$extension_summary;
		$data['destination_summary']  =@$destination_summary;
		$data['hotel_summary']        =@$hotel_summary;
		$data['rs_summary']           =@$rs_summary;
		$data['r_summary']            =@$r_summary;
		$data['show_hide']            =@$show_hide;
		$data['package_summarys']	  =@$summary_datas;
		$data['add_hotel_bookings']	  =@$summary_dataBooking;
		$data['passengers']			  =@$passengers;

		// return $this->load->view("panel/mainpanel/append_package.php",$data,TRUE);
		die(json_encode($data));
	}

	public function availableCountries()
	{
		$this->load->model('booking/booking');
		$this->load->model('package/package');
		$this->load->model('package/destination');
		$this->load->model('package/packagestorage');
		$this->load->model('user/user');
		if($this->input->post('encLogin_secret') === '0ff9346b4edc8dc033bff30762bc3c15d465d3f')
		{
			$userId = $this->input->post('userid');

			$data['destinations'] = $this->destination->getActiveBookingDestinations($userId);
			
			die(json_encode($data));
		}
		else
		{
			echo 'restrict !';
		}
	}

	public function packageList()
	{

		$userId = $this->input->post('userid');
		$code = $this->input->post('c_code');

		$this->db->select(' packages.id,
							packages.code,
							packages.title,
							packages.image_path,
							packages.status,
							package_duration.timestamp,
							package_duration.date_from,
							package_duration.date_to,
							package_destinations.country_code
						');
		$this->db->from('packages');
		$this->db->join('package_duration', 'package_duration.package_id = packages.id');
		$this->db->join('package_destinations', 'package_destinations.package_id = packages.id');
		$this->db->join('package_permissions', 'package_permissions.package_id = packages.id');
		
		$this->db->where('package_destinations.country_code', $code);
		$this->db->where('packages.status', 'active');
		$this->db->where('package_permissions.user_id', $userId);
		$this->db->distinct('packages.title');
		$data['packages'] = $this->db->get()->result();

		die(json_encode($data));
		// dump_exit($packages);
	}

	public function packageDetails()
	{
		if($this->input->post('encLogin_secret') === '0ff9346b4edc8dc033bff30762bc3c15d465d3f')
		{
			$package_id = $this->input->post('package_id');

			$this->db->select(' packages.id,
								packages.code,
								packages.days,
								packages.title,
								packages.image_path,
								packages.status,
								package_duration.timestamp,
								package_description.description,
								package_duration.date_from,
								package_duration.date_to,
								package_destinations.country_code
							');
			$this->db->from('packages');
			$this->db->join('package_description', 'package_description.package_id = packages.id');
			$this->db->join('package_duration', 'package_duration.package_id = packages.id');
			$this->db->join('package_destinations', 'package_destinations.package_id = packages.id');
			$this->db->join('package_permissions', 'package_permissions.package_id = packages.id');
			
			$this->db->where('packages.id', $package_id);
			$this->db->where('packages.status', 'active');
			$this->db->distinct('packages.title');
			$data['packages'] = $this->db->get()->result();


			$this->db->select(' hotels.id,
								hotels.name,
								package_hotels.package_id,
								package_hotels.status
							');
			$this->db->from('hotels');
			$this->db->join('package_hotels', 'package_hotels.hotel_id = hotels.id');
			
			$this->db->where('package_hotels.package_id', $package_id);
			$this->db->distinct('hotels.name');
			$this->db->order_by('hotels.name', "ASC");
			$hotels = $this->db->get()->result();
			$data['hotels'] = $hotels;


			$this->db->select(' * ');
			$this->db->from('addons');
			
			$this->db->where('package_id', $package_id);
			$this->db->distinct('description');
			$this->db->order_by('id', "ASC");
			$addons = $this->db->get()->result();
			$data['addons'] = $addons;

			$this->db->select(' * ');
			$this->db->from('package_surcharges');
			
			$this->db->where('package_id', $package_id);
			$this->db->where('status', "Active");
			$this->db->distinct('description');
			$this->db->order_by('id', "ASC");
			$addons = $this->db->get()->result();
			$data['package_surcharges'] = $addons;


			$this->db->select(' 
								package_destinations.id,
								package_destinations.package_id,
								package_destinations.country_code,
								package_destinations.nights,
								package_destinations.status,
								countries.code,
								countries.country
							');
			$this->db->from('package_destinations');
			$this->db->join('countries', 'countries.code = package_destinations.country_code');

			$this->db->where('package_destinations.package_id', $package_id);
			$this->db->order_by('id', "ASC");
			$destination = $this->db->get()->result();
			$data['destination'] = $destination;

			die(json_encode($data));
		}
		else
		{
			echo 'restrict !';
		}
	}

	public function roomRates()
	{
		if($this->input->post('encLogin_secret') === '0ff9346b4edc8dc033bff30762bc3c15d465d3f')
		{
			$hotel_id = $this->input->post('hotel_id');
			$package_id = $this->input->post('package_id');

			$this->db->select(' * ');
			$this->db->from('package_hotels'); 
			$this->db->join('hotelroom_rates', 'hotelroom_rates.package_hotel_id = package_hotels.id');

			$this->db->where('package_hotels.package_id', $package_id);
			$this->db->where('hotelroom_rates.cost !=', "0.00");
			$this->db->where('package_hotels.status', "active");
			$this->db->where('package_hotels.hotel_id', $hotel_id);
			$data['roomrates'] = $this->db->get()->result();

			$hotelroomid = $data['roomrates'][0]->package_hotel_id;


			$this->db->select(' * ');
			$this->db->from('hotel_surcharges');
			$this->db->where('hotel_id', $hotel_id);
			$this->db->where('status', "active");
			$this->db->distinct('description');
			$this->db->order_by('id', "ASC");
			$addons = $this->db->get()->result();
			$data['hotel_surcharges'] = $addons;

			$this->db->select(' * ');
			$this->db->from('hotelroom_surcharges');
			$this->db->where('hotelroom_id', $hotelroomid);
			$this->db->where('status', "active");
			$this->db->distinct('description');
			$this->db->order_by('id', "ASC");
			$hrsurcharges = $this->db->get()->result();
			$data['hotelroom_surcharges'] = $hrsurcharges;

			die(json_encode($data));
		}
		else
		{
			echo 'restrict !';
		}
	}

	public function getCountries()
	{
		if($this->input->post('encLogin_secret') === '0ff9346b4edc8dc033bff30762bc3c15d465d3f')
		{
			$this->db->select(' * ');
			$this->db->from('airportcodes');
			$data['countries'] = $this->db->get()->result();

			die(json_encode($data));
		}
		else
		{
			echo 'restrict !';
		}
	}

	public function saveBooking()
	{
		if($this->input->post('encLogin_secret') === '0ff9346b4edc8dc033bff30762bc3c15d465d3f')
		{
			$summarydata = $this->input->post('summary_data');
			$des_id = $summarydata['des_id'];
			$user_id = $summarydata['userid'];
			$hotel_add = $summarydata['hotel_ext_addons'];
			$travellers = $summarydata['travellerdetails'];
			$package_name = $summarydata['package_name'];
			$adults = $summarydata['adults'];
			$children = $summarydata['children'];
			$infant = $summarydata['infant'];
			$extension = $summarydata['extension'];
			$depatureDate = $summarydata['depatureDate'];
			$returnDate = $summarydata['returnDate'];
			// dump($summarydata);
			// die(json_encode("YEY!"));

			//--------------------------------- SAVE BOOKING ---------------------------------

			$this->db->select(' id, code ');
			$this->db->from('bookings');
			$this->db->order_by('id','DESC');
			$this->db->limit(1);
			$data['bookingcode'] = $this->db->get()->result();

			$newbookingcode = str_replace("GE", "", $data['bookingcode'][0]->code);
			$newbookingcode = (int)$newbookingcode + 1;
			$newbookingcode = "GE".$newbookingcode;


			$bookings = array(
								'user_id' => $summarydata['userid'],
								'package_id' => $summarydata['package_id'],
								'code' => $newbookingcode,
								'departure_date' => $summarydata['depatureDate'],
								'return_date' => $summarydata['returnDate'],
								'extended_days' => $summarydata['extension'],
								'hotel_id' => 0,
								'of_num' => $summarydata['org_flight'],
								'of_from' => $summarydata['org_from'],
								'of_to' => $summarydata['org_to'],
								'of_departuretime' => $summarydata['org_depart'],
								'of_arrivaltime' => $summarydata['org_arrive'],
								'of_remarks' => $summarydata['flight_remarks'],
								'rf_num' => $summarydata['ret_flight'],
								'rf_from' => $summarydata['ret_from'],
								'rf_to' => $summarydata['ret_to'],
								'rf_departuretime' => $summarydata['ret_depart'],
								'rf_arrivaltime' => $summarydata['ret_arrive'],
								'rf_remarks' => $summarydata['flight_remarks'],
								'total_cost' => $summarydata['tot_cost'],
								'total_profit' => $summarydata['tot_profit'],
								'total_price' => $summarydata['subtotal'],
								'total_discount' => 0.00,
								'total_difference' => 0.00,
								'agent_name' => $summarydata['agentname'],
								'agent_ref' => $summarydata['refno'],
								'confirmation_remarks' => "Booking details has been sent to the Administrator and Provider. Please wait for confirmation.",
								'date_booked' => date("Y-m-d"),
								'timestamp' => "0000-00-00 00:00:00"
							); 
			
			$bookingsaved = $this->db->insert('bookings', $bookings);
			$this->db->select(' id, code ');
			$this->db->from('bookings');
			$this->db->order_by('id','DESC');
			$this->db->limit(1);
			$data['bookingcode'] = $this->db->get()->result();

			$bookingid = (int)$data['bookingcode'][0]->id;
			$booking_code = $data['bookingcode'][0]->code;


			//--------------------------------- SAVE HOTEL ---------------------------------

			$hotel = array(
								'booking_id' => $bookingid,
								'package_hotel_id' => $summarydata['hotel_id'],
								'checkin_date' => $summarydata['depatureDate'],
								'checkout_date' => $summarydata['returnDate'],
								'ext_hotel_name' => $summarydata['addhotel'][0]['name'].",".$summarydata['addhotel'][1]['name'],
								'ext_hotel_chkin_chkout' => $summarydata['addhotel'][0]['checkin'].":".$summarydata['addhotel'][0]['checkout'].",".$summarydata['addhotel'][1]['checkin'].":".$summarydata['addhotel'][1]['checkout'],
								'remarks' => $summarydata['addhotel_remarks']
							); 
			
			$hotelroomssaved = $this->db->insert('booked_hotels', $hotel);
			$this->db->select(' id ');
			$this->db->from('booked_hotels');
			$this->db->order_by('id','DESC');
			$this->db->limit(1);
			$data['bookhotels'] = $this->db->get()->result();

			$hotelid = (int)$data['bookhotels'][0]->id;


			//--------------------------------- SAVE HOTELROOM ---------------------------------

			// dump($summarydata['hotel_ext_addons']);
			$hid = $summarydata['hotel_id'];
			$book_hotel_id = $summarydata['hotel_id'];
			

			for ($i=0; $i < count($hotel_add); $i++) { 
				// dump("This is ".$i);
				// dump($hotel_add[$i]['type']);
				// dump();
				// dump($book_hotel_id);
				if($hotel_add[$i]['type'] == "3room_payment"){
					$hotelrooms = array(
										'hotelroom_id' => $hotel_add[$i]['rphid'],
										'booked_hotel_id' => $summarydata['hotel_id'],
										'quantity' => $hotel_add[$i]['quantity'],
									); 
					
					$hotelroomssaved = $this->db->insert('booked_hotelrooms', $hotelrooms);
				}
				else if($hotel_add[$i]['type'] == "0package_surcharge"){
					$package_surcharge = array(
										'booking_id' => $bookingid,
										'package_surcharge_id' => $hotel_add[$i]['id'],
										'quantity' => $hotel_add[$i]['quantity'],
									); 
					
					$package_surcharge_saved = $this->db->insert('applied_package_surcharges', $package_surcharge);
					// if($package_surcharge_saved){dump("saved pack surcharge");}
					// else{dump("not saved pack surcharge");}
				}
				else if($hotel_add[$i]['type'] == "1hotel_surcharge"){
					$hotel_surcharge = array(
										'booked_hotel_id' => $book_hotel_id,
										'hotel_surcharges_id' => $hotel_add[$i]['id'],
										'quantity' => $hotel_add[$i]['quantity'],
									); 
					
					$hotel_surcharge_saved = $this->db->insert('applied_hotel_surcharges', $hotel_surcharge);	
					// if($hotel_surcharge_saved){dump("saved hotel surcharge");}
					// else{dump("not saved hotel surcharge");}
				}
				else if($hotel_add[$i]['type'] == "2room_surcharge"){
					$hotelroom_surcharge = array(
											'booked_hotel_id' => $book_hotel_id,
											'hotelroom_surcharge_id' => $hotel_add[$i]['id'],
											'quantity' => $hotel_add[$i]['quantity'],
										); 
					
					$hotelroom_surcharge_saved = $this->db->insert('applied_hotelroom_surcharges', $hotelroom_surcharge);
					dump($book_hotel_id." - ".$hotel_add[$i]['id']." - ".$hotel_add[$i]['quantity']);	
					// if($hotelroom_surcharge_saved){dump("saved room surcharge");}
					// else{dump("not saved room surcharge");}
				}

				
				if($hotel_add[$i]['type'] != "3room_payment"){
					$hid = 0;
				}

				$summarydata = array(
									'booking_id' => $bookingid,
									'destination_id' => $des_id,
									'hotel_id' => $hid,
									'description' => $hotel_add[$i]['description'],
									'computation' => $hotel_add[$i]['computation'],
									'subtotal' => (int)$hotel_add[$i]['subtotal'],
									'type' => $hotel_add[$i]['type'],
									'timestamp' => time()
								);                        
				
				$summarydatasaved = $this->db->insert('summary_data', $summarydata);
			}


			//--------------------------------- SAVE TRAVELLERS ---------------------------------

			for ($i=0; $i < count($travellers); $i++) { 
				$travellerdetails = array(
									'booking_id' => $bookingid,
									'user_id' => $user_id,
									'name' => $travellers[$i]['name'],
									'passport_no' => $travellers[$i]['passno'],
									'date_of_birth' => $travellers[$i]['dob'],
									'type' => $travellers[$i]['type']
								); 
				
				$travellerdetailssaved = $this->db->insert('traveller_details', $travellerdetails);
			}
			$booking_status = 'pending';
			$booking_ids = $this->booking->getBookingIDs($booking_code);
			if(count($booking_ids)){
				for($i=0;$i<count($booking_ids);$i++){
					if($booking_status === 'amended')
					{
						$booking_summary[] = $this->getSummaryViewByEmail_agent($booking_ids[$i]["id"],$tracks);
						$booking_summary_provider[] = $this->getSummaryViewByEmail_provider($booking_ids[$i]["id"],$tracks);
					}
					else
					{
						$booking_summary[] = $this->getSummaryViewByEmail_agent($booking_ids[$i]["id"],'');
						$booking_summary_provider[] = $this->getSummaryViewByEmail_provider($booking_ids[$i]["id"],'');
					}
					
				}
			}

			$booking['ge_booking_code'] = $booking_code;
			$booking['ge_booking_status'] = $booking_status;
			$booking['booking_id_mapper'] = $bookingid;
			$booking['booking_summary'] = $booking_summary;
			$booking['booking_summary_provider'] = $booking_summary_provider;
			$booking['package_name'] = $package_name;
			$booking['adults'] = $adults;
			$booking['children'] = $children;
			$booking['infant'] = $infant;
			$booking['extension'] = $extension;
			$booking['depatureDate'] = $depatureDate;
			$booking['returnDate'] = $returnDate;
			$booking_stored=1;
			if($booking_stored){//una nga email
				$this->load->helper('string');
				$toHash = random_string('alpha', 10);
				$hash = hashmonster($toHash);
				$this->db->insert('confirmation_booking', array(
					'booking_id' => $bookingid,
					'hash' => $hash
				));
				$link = base_url('change/confirm_booking/'.$hash);
				if($this->booking_confirmation($link,$booking) > 0 ){
					die(json_encode("SUCCESS"));
				}
				else{
					die(json_encode("FAILED"));
				}
				if($this->booking_confirmation_provider($link,$booking) > 0){
					die(json_encode("SUCCESS"));
				}
				else{
					die(json_encode("FAILED"));
				}
				// redirect('panel/package/'.$return_result_id);
			}

			
		}
		else
		{
			echo 'restrict !';
		}
	}

	public function getSummaryViewByEmail_agent($booking_id,$track_changes){
		$data = $this->getSummaryViewByEmail($booking_id,$track_changes);
		return $this->load->view("panel/mainpanel/append_email.php",$data,TRUE);
	}

	public function getSummaryViewByEmail_provider($booking_id,$track_changes){
		$data = $this->getSummaryViewByEmail($booking_id,$track_changes);
		return $this->load->view("panel/mainpanel/append_email_provider.php",$data,TRUE);
	}

	public function getSummaryViewByEmail($booking_id,$tracks_change){
		$this->load->model('booking/booking');
		$this->load->model("booking/bookingstorage");
		$this->load->model('user/user');
		$countries_usr    = $this->user->getAllCountries();
		$summary_data     =$this->booking->getBysummary($booking_id);
		$gettotalnights   =$this->booking->getBysummaryPackage($booking_id);
		$summary_guest    =$this->booking->getByGuest($booking_id);
		$provider_package =$this->booking->getBysummaryCompany($booking_id);

		#get the individual summaries ----------------------------------------------
		$package_summary   = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"type"=>"0package_surcharge"));
		$addon_summary     = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"type"=>"4addon"));
		$extension_summary = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"type"=>"5extension"));

        #fetch the destination IDs ----------------------------------------------
        $destination_summary = $this->bookingstorage->getSummaryDestinations($booking_id);

        for($i=0;$i<count($destination_summary);$i++){
			$destination     = $destination_summary[$i];
			#fetch the hotel, room surcharge, room summaries ----------------------------------------------
			$hotel_summary[] = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"destination_id"=>$destination["destination_id"],"type"=>"1hotel_surcharge"));
			$rs_summary[]    = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"destination_id"=>$destination["destination_id"],"type"=>"2room_surcharge"));
			$r_summary[]     = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"destination_id"=>$destination["destination_id"],"type"=>"3room_payment"));
        }

		$user_data = $this->userpersistence->getPersistedUser();

		$data['countries_usr']        = $countries_usr;
		$data['user']                 =$user_data;
		$data['data_summary']         =$summary_data;
		$data['guest_summary']        =$summary_guest;
		$data['package_total_nights'] =$gettotalnights;
		$data['provider_summary']     =$provider_package;
		$data['package_summary']      =@$package_summary;
		$data['addon_summary']        =@$addon_summary;
		$data['extension_summary']    =@$extension_summary;
		$data['destination_summary']  =@$destination_summary;
		$data['hotel_summary']        =@$hotel_summary;
		$data['rs_summary']           =@$rs_summary;
		$data['r_summary']            =@$r_summary;
		$data['tracks']            	  =$tracks_change;

		$email_view_user = get_Class($data['user']);
		return $data;

		// dump($email_view_user);
		// die;
		//return $this->load->view("panel/mainpanel/append_email.php",$data,TRUE);
	}

	    public function booking_confirmation($link='', $booking){
	    if(!$link) return;
     	$mail_agent=$booking['booking_id_mapper'];
		$this->load->model('booking/booking');
		$recipients=$this->booking->getEmailadmin();
		$get_agent_email=$this->booking->getEmailagent($mail_agent);
		$company=$this->booking->getEmailagent($mail_agent);
		$company_info = $this->db->select('*')->from('companies')->join('company_designations', 'companies.id = company_designations.company_id')->where(array('company_designations.user_id'=>$company[0]->agentid))->get()->result();
     	$cnt = 0;
        $mail = $this->mail;
    	$mail->setFrom('op.globalexplorer@gmail.com', 'Global Explorer');						// Sender

	    $mail->WordWrap = 50;                                 							// Set word wrap to 50 characters
	    $mail->isHTML(true);
	   	if($booking['ge_booking_status'] == 'amended'){
	    $mail->Subject = ''.$booking['ge_booking_code'].' [amended] Global Explorer Booking Notification';
	    }else if($booking['ge_booking_status'] == 'pending'){
	    $mail->Subject = ''.$booking['ge_booking_code'].' [pending] Global Explorer Booking Notification';
	    }                               							// Set email format to HTML
	    if($get_agent_email != NULL || $get_agent_email != ""){
		   // foreach ($get_agent_email as $key) {
	    		$mail->ClearAddresses();
				$mail->ClearCCs();
				//$booking['recipient'] =$get_agent_email[0]->agentfname.' '.$get_agent_email[0]->agentlname;
				$booking['recipient'] = $company_info[0]->name;
	     		if($get_agent_email[0]->alteremail != "" || $get_agent_email[0]->alteremail != NULL) {
					$wang=explode(',',$get_agent_email[0]->alteremail);
		    		for ($i=0; $i < count($wang); $i++) {
			     		$mail->addCC($wang[$i],$booking['recipient']);
			     		//$mail->addCC('delabahan@gmail.com',$booking['recipient']);
					}
					// $mail->addCC('edmundwan@yahoo.com','Edmund Wan');
					// $mail->addCC('op.globalexplorer@gmail.com','Edmund Wan');
				}
	     		$mail->addAddress($get_agent_email[0]->agentemail,$booking['recipient']);
	     		// dump_exit($get_agent_email[0]->agentemail);
	     		// $mail->addAddress('edmundwan@yahoo.com','Edmund Wan');
	     		// $mail->addAddress('op.globalexplorer@gmail.com','Edmund Wan');
	     		//dump_exit($booking);
		    	$mail->Body    = $this->load->view('booking_confirmation',$booking,TRUE);

		
			    if($mail->send()) $cnt++;
			//}
		}
     	if($recipients != NULL || $recipients != ""){
     		foreach($recipients as $row){
	    		$mail->ClearAddresses();
				$mail->ClearCCs();
				$company_info = $this->db->select('*')->from('companies')->join('company_designations', 'companies.id = company_designations.company_id')->where(array('company_designations.user_id'=>$row->adminid))->get()->result();
	     		$booking['recipient'] = $company_info[0]->name;
	     		//$booking['recipient'] = $row->adminfname.' '.$row->adminlname;
	     		$booking['recipient_id'] = $row->adminid;
	     		if($row->alteremail != '' || $row->alteremail != NULL) {
			     	$alternative_email=explode(',',$row->alteremail);
		    		for ($i=0; $i < count($alternative_email); $i++) {
			     		$mail->addCC($alternative_email[$i],$booking['recipient']);
			     		//$mail->addCC('delabahan@gmail.com',$booking['recipient']);
					}
			    }
		    	$mail->addAddress($row->adminemail, $booking['recipient']);     		// Add a recipient (Admin)
		    	//$mail->addAddress('delabahan@gmail.com', $booking['recipient']);     		// Add a recipient (Admin)
	    		$mail->Body    = $this->load->view('booking_confirmation',$booking,TRUE);
		    	if($mail->send()) $cnt++;
     		}
     	}

		$mail->Body    = $this->load->view('booking_confirmation',$booking,TRUE);
    	$mail->AltBody = 'Booking Confirmation [Global Explorer]';
    	//if($mail->send());
     	return $cnt;
    }

    public function booking_confirmation_provider($link='', $booking){
	    if(!$link) return;
	    $booking['confirm'] = $link.'/confirm';
	    $booking['hotel'] = $link.'/hotel';
	    $booking['room'] = $link.'/room';
	    $booking['reject'] = $link.'/reject';
	    $booking['cancel'] = $link.'/cancel';
		$this->load->model('booking/booking');
        $mail_provider=$booking['booking_id_mapper'];
        //dump_exit($mail_provider);
		$recipients=$this->booking->getEmailadmin();
		$get_provider_email=$this->booking->getEmailprovider($mail_provider);
		$get_provider_company = $this->db->select('*')->from('companies')->join('company_designations', 'companies.id = company_designations.company_id')->where(array('company_designations.user_id'=>$get_provider_email[0]->providerid))->get()->result();
     	$cnt = 0;
     	//dump_exit($get_provider_company);

        $mail = $this->mail;
    	$mail->setFrom('op.globalexplorer@gmail.com', 'Global Explorer');						// Add a recipient

    	$mail->WordWrap = 50;                                 							// Set word wrap to 50 characters
	    $mail->isHTML(true);                                  							// Set email format to HTML
	    if($booking['ge_booking_status'] == 'amended'){
	    $mail->Subject = ''.$booking['ge_booking_code'].' [amended] Global Explorer Booking Notification';
	    }else if($booking['ge_booking_status'] == 'pending'){
	    $mail->Subject = ''.$booking['ge_booking_code'].' [pending] Global Explorer Booking Notification';
	    }
	    if($get_provider_email != NULL || $get_provider_email != ""){
		    //foreach ($get_provider_email as $key) {
	    		$mail->ClearAddresses();
				$mail->ClearCCs();
				$booking['recipient'] =	 $get_provider_company[0]->name;
	 			$booking['recipient_id'] = $get_provider_email[0]->providerid;
	     		if($get_provider_email[0]->alteremail != "" || $get_provider_email[0]->alteremail != NULL) {
					$wang=explode(',',$get_provider_email[0]->alteremail);
		    		for ($i=0; $i < count($wang); $i++) {
			     		$mail->addCC($wang[$i],$booking['recipient']);
			     		//$mail->addCC('delabahan@gmail.com',$booking['recipient']);
					}
				}
	     		$mail->addAddress($get_provider_email[0]->provideremail,$booking['recipient']);
	     		//$mail->addAddress('delabahan@gmail.com',$booking['recipient']);
		    	$mail->Body    = $this->load->view('booking_confirmation_provider',$booking,TRUE);
			    if($mail->send()) $cnt++;
			//}
		}

     	if($recipients != NULL || $recipients != ""){
     		foreach($recipients as $row){
	    		$mail->ClearAddresses();
				$mail->ClearCCs();
				$get_company = $this->db->select('*')->from('companies')->join('company_designations', 'companies.id = company_designations.company_id')->where(array('company_designations.user_id'=>$row->adminid))->get()->result();
	     		$booking['recipient'] = $get_company[0]->name;
	     		$booking['recipient_id'] = $row->adminid;
	     		if($row->alteremail != '' || $row->alteremail != NULL) {
			     	$alternative_email=explode(',',$row->alteremail);
			     	for ($i=0; $i < count($alternative_email); $i++) {
			     		$mail->addCC($alternative_email[$i],$booking['recipient']);
			     		//$mail->addCC('delabahan@gmail.com',$booking['recipient']);
					}
			    }
		    	$mail->addAddress($row->adminemail, $booking['recipient']);     		// Add a recipient (Admin)
		    	//$mail->addAddress('delabahan@gmail.com', $booking['recipient']);     		// Add a recipient (Admin)
	    		$mail->Body    = $this->load->view('booking_confirmation_provider',$booking,TRUE);
		    	if($mail->send()) $cnt++;
     		}
     	}
     	$mail->Body    = $this->load->view('booking_confirmation_provider',$booking,TRUE);
    	$mail->AltBody = 'Booking Confirmation [Global Explorer]';
		//if($mail->send());
     	return $cnt;
    }


	public function updateStatus()
	{
		if($this->input->post('encLogin_secret') === '0ff9346b4edc8dc033bff30762bc3c15d465d3f')
		{
			$id = $this->input->post('id');
			$status = $this->input->post('status');
			$user_id = $this->input->post('userid');
			// $id = 319;
			// $status = 'rejected';
			// $user_id = 78;
			$status_update = array(
								'status' => $status,
							); 
			
			$this->db->where('id', $id);
			$status_updatesaved = $this->db->update('bookings', $status_update);

			if($status == 'confirmed'){
				if($status_updatesaved){
					$resend=$this->booking_confirmed($id, $user_id);
					if($status_updatesaved == true)
    					die(json_encode("SUCCESS"));
	    			else
	    				die(json_encode("FAILED"));
				}
			}
			else{
				if($status_updatesaved){
					$resend=$this->booking_reject($id, $user_id);
					if($status_updatesaved == true)
    					die(json_encode("SUCCESS"));
	    			else
	    				die(json_encode("FAILED"));
				}
			}
			

			//die(json_encode("SUCCESS"));
		}
		else
		{
			echo 'restrict !';
		}
	}


	public function booking_reject($data_mapping,$id){
	 	$this->load->model('booking/booking');
	 	$mail = $this->mail;
        $mail->setFrom('no-reply@ge.com.sg', 'Global Explorer');						// Sender
    	//$mail->addReplyTo('no-reply@globlaexplorer.com', 'Global Explorer');			// Sender's optional
	    $mail->WordWrap = 50;                                 							// Set word wrap to 50 characters
	    $mail->isHTML(true);                                  							// Set email format to HTML
	 	$data=$this->booking->getByCancellation($data_mapping);
	    $mail->Subject = ''.$data[0]->bookedcode.' [rejected] Global Explorer Booking Notification';
	    $recipients=$this->booking->getEmailadmin();
		$data_res=$this->booking->getByreject($id);
		$data_result['user_click']=$this->booking->getByreject($id);
		$data_result['mail_data']=$this->booking->getByCancellation($data_mapping);

		if($data[0]->agenttype == 'agent'){
    		$mail->ClearAddresses();
			$mail->ClearCCs();
			if($data[0]->agent_alter_email != "" || $data[0]->agent_alter_email != NULL){
				$wang=explode(',',$data[0]->agent_alter_email);
	    		for ($i=0; $i < count($wang); $i++) {
		     		$mail->addAddress($wang[$i],$data[0]->userfirstname ." ". $data[0]->userlastname);
				}
			} 
		    $mail->addAddress($data[0]->agentemail,$data[0]->userfirstname ." ". $data[0]->userlastname);
		    $mail->Body    = $this->load->view('booking_rejected_agent',$data_result,TRUE);
	    	if(!$mail->send()) return false;
		}

		if($data[0]->providertype == 'provider'){
			if($data[0]->provideremail != $data_res[0]->useremail){
	    		$mail->ClearAddresses();
				$mail->ClearCCs();
				if($data[0]->provider_alter_email != "" || $data[0]->provider_alter_email != NULL){
					$wang=explode(',',$data[0]->provider_alter_email);
		    		for ($i=0; $i < count($wang); $i++) {
			     		$mail->addCC($wang[$i],$data[0]->providerfirstname ." ". $data[0]->providerlastname);
					}
				} 
				$mail->addAddress($data[0]->provideremail,$data[0]->providerfirstname ." ". $data[0]->providerlastname);// Add a recipient (Provider)
			    $mail->Body    = $this->load->view('booking_rejected_provider',$data_result,TRUE);
			    if(!$mail->send()) return false;
			}
		}

		if($data[0]->admintype == 'admin'){
			if($data[0]->adminemail != $data_res[0]->useremail){
	    		$mail->ClearAddresses();
				$mail->ClearCCs();
				if($data[0]->admin_alter_email != "" || $data[0]->admin_alter_email != NULL){
					$wang=explode(',',$data[0]->admin_alter_email);
		    		for ($i=0; $i < count($wang); $i++) {
			     		$mail->addCC($wang[$i],$data[0]->adminfirstname ." ". $data[0]->adminlastname);
					}
				} 
				$mail->addAddress($data[0]->adminemail,$data[0]->adminfirstname ." ". $data[0]->adminlastname);// Add a recipient (Provider)
			    $mail->Body    = $this->load->view('booking_rejected_admin',$data_result,TRUE);
			    if(!$mail->send()) return false;
			}
		}

		if($recipients != NULL || $recipients != ""){
     		foreach($recipients as $row){
	    		$mail->ClearAddresses();
				$mail->ClearCCs();
				//if($row->adminemail != $data[0]->adminemail){
					$data_result['recipient'] = @$row->adminfname.' '.@$row->adminlname;
		     		if($row->alteremail != '' || $row->alteremail != NULL) {
				     	$alternative_email=explode(',',$row->alteremail);
				     	for ($i=0; $i < count($alternative_email); $i++) {
				     		$mail->addCC($alternative_email[$i],$row->adminfname ." ". $row->adminlname);
						}
				    }
			    	$mail->addAddress($row->adminemail,@$row->adminfname ." ".@$row->adminlname);
			    	$mail->Body    = $this->load->view('booking_rejected_all_admin',$data_result,TRUE);
			    	if(!$mail->send()) return false;
			   // }
     		}
     	}
	    $mail->AltBody = 'Booking Rejected [Global Explorer]';
		return true;
    }

	public function booking_confirmed($data_mapping,$id){

        $this->load->model('booking/booking');
	 	$mail = $this->mail;
        $mail->setFrom('no-reply@ge.com.sg', 'Global Explorer');						// Senter
    	//$mail->addReplyTo('no-reply@globlaexplorer.com', 'Global Explorer');			// Sender's optional
	    $mail->WordWrap = 50;                                 							// Set word wrap to 50 characters
	    $mail->isHTML(true);                                  							// Set email format to HTML
	 	$data=$this->booking->getByCancellation($data_mapping);
	    $mail->Subject = ''.$data[0]->bookedcode.' [confirmed] Global Explorer Booking Notification';
		$recipients=$this->booking->getEmailadmin();
		$data_res=$this->booking->getByreject($id);
		$data_result['user_click']=$this->booking->getByreject($id);
		$data_result['mail_data']=$this->booking->getByCancellation($data_mapping);
		// dump($data);
		// dump($data_res);
		// dump($data_result);
		
		if($data[0]->agenttype == 'agent'){
    		$mail->ClearAddresses();
			$mail->ClearCCs();
			if($data[0]->agent_alter_email != "" || $data[0]->agent_alter_email != NULL){
				$wang=explode(',',$data[0]->agent_alter_email);
	    		for ($i=0; $i < count($wang); $i++) {
		     		$mail->addCC($wang[$i],$data[0]->userfirstname ." ". $data[0]->userlastname);
				}
			} 
		    $mail->addAddress($data[0]->agentemail,$data[0]->userfirstname ." ". $data[0]->userlastname);
		    $mail->Body    = $this->load->view('booking_confirmed_agent',$data_result,TRUE);
		    if(!$mail->send()) return false;
		}

		if($data[0]->providertype == 'provider'){
			if($data[0]->provideremail != $data_res[0]->useremail){
	    		$mail->ClearAddresses();
				$mail->ClearCCs();
				if($data[0]->provider_alter_email != "" || $data[0]->provider_alter_email != NULL){
					$wang=explode(',',$data[0]->provider_alter_email);
		    		for ($i=0; $i < count($wang); $i++) {
			     		$mail->addCC($wang[$i],$data[0]->providerfirstname ." ". $data[0]->providerlastname);
					}
				} 
				$mail->addAddress($data[0]->provideremail,$data[0]->providerfirstname ." ". $data[0]->providerlastname);// Add a recipient (Provider)
			    $mail->Body    = $this->load->view('booking_confirmed_provider',$data_result,TRUE);
			    if(!$mail->send()) return false;
			}
		}

		if($data[0]->admintype == 'admin'){
			if($data[0]->adminemail != $data_res[0]->useremail){
	    		$mail->ClearAddresses();
				$mail->ClearCCs();
				if($data[0]->admin_alter_email != "" || $data[0]->admin_alter_email != NULL){
					$wang=explode(',',$data[0]->admin_alter_email);
		    		for ($i=0; $i < count($wang); $i++) {
			     		$mail->addCC($wang[$i],$data[0]->adminfirstname ." ". $data[0]->adminlastname);
					}
				} 
				$mail->addAddress($data[0]->adminemail,$data[0]->adminfirstname ." ". $data[0]->adminlastname);// Add a recipient (Provider)
			    $mail->Body    = $this->load->view('booking_confirmed_admin',$data_result,TRUE);
			    if(!$mail->send()) return false;
			}
		}

		if($recipients != NULL || $recipients != ""){
     		foreach($recipients as $row){
	    		$mail->ClearAddresses();
				$mail->ClearCCs();
				//if($row->adminemail != $data[0]->adminemail){
					$data_result['recipient'] = @$row->adminfname.' '.@$row->adminlname;
		     		if($row->alteremail != '' || $row->alteremail != NULL) {
				     	$alternative_email=explode(',',$row->alteremail);
				     	for ($i=0; $i < count($alternative_email); $i++) {
				     		$mail->addCC($alternative_email[$i],$row->adminfname ." ". $row->adminlname);
						}
				    }
			    	$mail->addAddress($row->adminemail,@$row->adminfname ." ".@$row->adminlname);
			    	$mail->Body    = $this->load->view('booking_confirmed_all_admin',$data_result,TRUE);
			    	if(!$mail->send()) return false;
			    //}
     		}
     	}
     	$mail->AltBody = 'Booking Confirmed [Global Explorer]';
		return true;
    }



	public function getBookingPayments()
	{
		// if($this->input->post('encLogin_secret') === '0ff9346b4edc8dc033bff30762bc3c15d465d3f')
		// {
			$search_array = false;
			$filter_arr = false;
			$start = false;
			$per_page = 0;
			$userId = $this->input->post('id');
			$userType = $this->input->post('type');	
			// $userId = 78;
			// $userType = "admin";	

			if($userType == 'admin'){
				//$data['selectall_data']  = $this->booking->getAllBookingRecords_page(false,false,$search_array,$filter_arr);
				$data['selectall_data']  = $this->booking->getAllBookingRecords_page($start,$per_page,$search_array,$filter_arr);
			}

			else if($userType == 'provider'){
				//$data['selectall_data']  = $this->booking->getAllBookingRecordsProvider_page($userId,false,false,$search_array,$filter_arr);
				$data['selectall_data']  = $this->booking->getAllBookingRecordsProvider_page($userId);
			}

			else if($userType == 'agent' || $userType === 'customer'){
				//$data['selectall_data']  = $this->booking->getAllBookingRecordsAgent_page($userId,$start,false,$search_array,$filter_arr);
				$data['selectall_data']  = $this->booking->getAllBookingRecordsAgent_page($userId);
				
			}

			$hotelname =  array();

			for ($index=0; $index < count($data['selectall_data']); $index++) { 
				$temp = array(
							'id' => $data['selectall_data'][$index]->BOOKID, 
							'hotelname' => $this->getHotelName2($data['selectall_data'][$index]->BOOKINGCODE), 
							'bookdate' => $this->getBookDate($data['selectall_data'][$index]->BOOKINGCODE), 
							// 'company' => $this->getAgent($userId),
							'company' => $this->getAgent($data['selectall_data'][$index]->BOOKINGCODE),
							'provider' => $this->getProvider($data['selectall_data'][$index]->BOOKINGCODE),
							'passengers' =>  $this->getPassengers($data['selectall_data'][$index]->BOOKID),
							'payment_id' =>  $this->getPaymentId($data['selectall_data'][$index]->BOOKID),
							'payment_date' =>  $this->getPaymentDate($data['selectall_data'][$index]->BOOKID),
							'difference' =>  $this->getDifference($data['selectall_data'][$index]->BOOKID),
							'payment_type' =>  $this->getPaymentType($data['selectall_data'][$index]->BOOKID)
							);
				array_push($hotelname, $temp);
			}

			$data['hotels'] = $hotelname;

			$this->db->select(' * ');
			$this->db->from('companies');
			$this->db->join('company_designations', 'company_designations.company_id = companies.id');
			$this->db->join('users', 'users.id = company_designations.user_id');
			$this->db->where('users.type', "provider");
			$this->db->where('users.status', "active");

			$data['provider'] = $this->db->get()->result();


			$this->db->select(' 
								companies.id,
								companies.name,
								company_designations.*,
							');
			$this->db->from('companies');
			$this->db->join('company_designations', 'company_designations.company_id = companies.id');
			$this->db->join('users', 'users.id = company_designations.user_id');
			$this->db->where('users.type', "agent");
			$this->db->where('users.status', "active");
			$this->db->group_by('companies.name');
			
			$data['companies'] = $this->db->get()->result();


			// $data['selectall_data']->
			// dump($data['hotels']);
			die(json_encode($data));
			// dump($data['hotels']);
		// }
		// else
		// {
		// 	echo 'restrict !';
		// }
	}

	public function getAgent($bookingcode)
	{
		// if($this->input->post('encLogin_secret') === '0ff9346b4edc8dc033bff30762bc3c15d465d3f')
		// {
			$bcode = $bookingcode; 
			$this->db->select('*');
			$this->db->from('bookings');
			$this->db->join('company_designations', 'company_designations.user_id = bookings.user_id');
			$this->db->where('bookings.code', $bcode);
			$agent = $this->db->get()->result();
			$agent_id = $agent[0]->company_id;
			// dump($agent_id);
		 	return $agent_id;

		    // dump("yoh");
		// }
		// else
		// {
		// 	echo 'restrict !';
		// }
	}

	public function getProvider($bookingcode)
	{
		// if($this->input->post('encLogin_secret') === '0ff9346b4edc8dc033bff30762bc3c15d465d3f')
		// {
			$bcode = $bookingcode; 
			$this->db->select(' * ');
			$this->db->from('bookings');
			$this->db->join('packages', 'packages.id = bookings.package_id');
			$this->db->join('company_designations', 'company_designations.user_id = packages.provider_id');
			$this->db->where('bookings.code', $bcode);
			$provider = $this->db->get()->result();
			$provider_id = $provider[0]->company_id;
			// dump($provider_id);
		    return $provider_id;
		    // dump($provider_id);
		// }
		// else
		// {
		// 	echo 'restrict !';
		// }
	}

	public function getPaymentId($bookingcode)
	{
		// if($this->input->post('encLogin_secret') === '0ff9346b4edc8dc033bff30762bc3c15d465d3f')
		// {
			$bcode = $bookingcode; 
			$this->db->select(' * ');
			$this->db->from('payments');
			$this->db->where('booking_id', $bcode);
			$payments = $this->db->get()->result();
			// $payment_id = $payments[0]->payment_type_id;
			if($payments){
		    	return $payments[0]->payment_type_id;
		    	// dump($payments[0]->payment_type_id);							
		    }else{
		    	return 0;
		    }
			
		// }
		// else
		// {
		// 	echo 'restrict !';
		// }
	}

	public function getPaymentDate($bookingcode)
	{
		// if($this->input->post('encLogin_secret') === '0ff9346b4edc8dc033bff30762bc3c15d465d3f')
		// {
			$bcode = $bookingcode; 
			$this->db->select(' * ');
			$this->db->from('payments');
			$this->db->where('booking_id', $bcode);
			$payments = $this->db->get()->result();
			// $payment_id = $payments[0]->payment_type_id;
			if($payments){
		    	return $payments[0]->date;
		    	// dump($payments[0]->payment_type_id);							
		    }else{
		    	return 0;
		    }
			
		// }
		// else
		// {
		// 	echo 'restrict !';
		// }
	}

	public function getPaymentType($bookingcode)
	{
		// if($this->input->post('encLogin_secret') === '0ff9346b4edc8dc033bff30762bc3c15d465d3f')
		// {
			$bcode = $bookingcode; 
			$this->db->select(' * ');
			$this->db->from('payments');
			$this->db->join('payment_type', 'payment_type.id = payments.payment_type_id');
			$this->db->where('payments.booking_id', $bcode);
			$payments = $this->db->get()->result();
			// $payment_id = $payments[0]->payment_type_id;
			if($payments){
				$description = "";
				if($payments[0]->description != ""){
					$description = "[ ".$payments[0]->description." ]";
				}
				$payment_label = strtoupper($payments[0]->type).": ".number_format($payments[0]->total_amount)." SGD ".$description;
		    	return $payment_label;
		    	// dump($payments[0]->payment_type_id);							
		    }else{
		    	return 0;
		    }
			
		// }
		// else
		// {
		// 	echo 'restrict !';
		// }
	}

	public function getDifference($bookingcode)
	{
		// if($this->input->post('encLogin_secret') === '0ff9346b4edc8dc033bff30762bc3c15d465d3f')
		// {
			$bcode = $bookingcode; 
			$this->db->select(' * ');
			$this->db->from('bookings');
			$this->db->where('id', $bcode);
			$payments = $this->db->get()->result();
			// $payment_id = $payments[0]->payment_type_id;
			if($payments){
		    	return $payments[0]->total_difference;
		    	// dump($payments[0]->payment_type_id);							
		    }else{
		    	return 0;
		    }
			
		// }
		// else
		// {
		// 	echo 'restrict !';
		// }
	}	

	public function getPassengers($bookid)
	{
		// if($this->input->post('encLogin_secret') === '0ff9346b4edc8dc033bff30762bc3c15d465d3f')
		// {
			$temp = array();
			$bid = $bookid; 
			$this->db->select(' * ');
			$this->db->from('traveller_details');
			$this->db->where('booking_id', $bid);
			$provider = $this->db->get()->result();
		    $passengers = array();
			for ($index=0; $index < count($provider); $index++) { 
				$temp = array(
							'name' => $provider[$index]->name
							);
				array_push($passengers, $temp);
			}

		    // dump($passengers);
		    return $passengers;
		// }
		// else
		// {
		// 	echo 'restrict !';
		// }
	}


	public function updateDiscount()
	{
		if($this->input->post('encLogin_secret') === '0ff9346b4edc8dc033bff30762bc3c15d465d3f')
		{
			$id = $this->input->post('id');
			$discount_update = array(
								'total_discount' => $this->input->post('total_discount'),
							); 
			
			$this->db->where('id', $id);
			$discount_updatesaved = $this->db->update('bookings', $discount_update);

			die(json_encode("SUCCESS"));
		}
		else
		{
			echo 'restrict !';
		}
	}

	public function updateDifference()
	{
		if($this->input->post('encLogin_secret') === '0ff9346b4edc8dc033bff30762bc3c15d465d3f')
		{
			$id = $this->input->post('id');
			$difference_update = array(
								'total_difference' => $this->input->post('total_difference'),
							); 
			
			$this->db->where('id', $id);
			$difference_updatesaved = $this->db->update('bookings', $difference_update);

			die(json_encode("SUCCESS"));
		}
		else
		{
			echo 'restrict !';
		}
	}

	public function collectPayments()
	{
		if($this->input->post('encLogin_secret') === '0ff9346b4edc8dc033bff30762bc3c15d465d3f')
		{
			$paymenttype = array(
								'type' => $this->input->post('type'),
								'description' => $this->input->post('description'),
								'total_amount' => $this->input->post('total'),
							); 
			
			$paymenttype_saved = $this->db->insert('payment_type', $paymenttype);


			$this->db->select('*');
			$this->db->from('payment_type');
			$this->db->order_by('id','DESC');
			$this->db->limit(1);
			$data['paymenttype'] = $this->db->get()->result();

			$type = (int)$data['paymenttype'][0]->id;

			die(json_encode($type));

		}
		else
		{
			echo 'restrict !';
		}
	}


	public function paymentInfos()
	{
		if($this->input->post('encLogin_secret') === '0ff9346b4edc8dc033bff30762bc3c15d465d3f')
		{
			$paymenttype = array(
								'booking_id' => $this->input->post('booking_id'),
								'payment_type_id' => $this->input->post('payment_type_id'),
								'status ' => $this->input->post('status'),
							); 
			
			$paymenttype_saved = $this->db->insert('payments', $paymenttype);

			die("SUCCESS");

		}
		else
		{
			echo 'restrict !';
		}
	}

	public function updatePaymentInfos()
	{
		if($this->input->post('encLogin_secret') === '0ff9346b4edc8dc033bff30762bc3c15d465d3f')
		{
			$this->db->where('booking_id', $this->input->post('booking_id'));
			$this->db->delete('payments');

			$paymenttype = array(
								'booking_id' => $this->input->post('booking_id'),
								'payment_type_id' => $this->input->post('payment_type_id'),
								'status ' => $this->input->post('status'),
							); 
			
			$paymenttype_saved = $this->db->insert('payments', $paymenttype);

			die("SUCCESS");

		}
		else
		{
			echo 'restrict !';
		}
	}

	public function getActiveDestinations()
	{
		$this->load->model('package/destination');
	
		if($this->input->post('encLogin_secret') === '0ff9346b4edc8dc033bff30762bc3c15d465d3f')
		{

			$data['destinations'] = $this->destination->getActiveDestinations();	
			die(json_encode($data));
		}
		else
		{
			echo 'restrict !';
		}
	}

	public function getDestinationPackages(){

        $this->load->model('package/packagestorage');

        if($this->input->post('encLogin_secret') === '0ff9346b4edc8dc033bff30762bc3c15d465d3f')
		{
			$destination_code	=	$this->input->post('destination_code');
			$data['packages'] = $this->packagestorage->getDestinationPackages($destination_code);	
			die(json_encode($data));
		}

        else
		{
			echo 'restrict !';
		}

	}

	public function getPackageHotels(){

		$this->load->model('hotel/hotelstorage');

		if($this->input->post('encLogin_secret') === '0ff9346b4edc8dc033bff30762bc3c15d465d3f')
		{
			$package_id	=	$this->input->post('package_id');
			$data['package_hotels'] = $this->hotelstorage->getHotelsByPackage($package_id);	
			die(json_encode($data));
		}

        else
		{
			echo 'restrict !';
		}

	}

	public function checkAvailablePAckages(){
		$this->load->model('booking/booking');
        $this->load->model('package/packagestorage');
        $this->load->model('package/package');
        $this->load->model('hotel/hotel');
        $this->load->model('surcharge/surchargestorage');

        if($this->input->post('encLogin_secret') === '0ff9346b4edc8dc033bff30762bc3c15d465d3f'){
	        $packageid = intval(@$_POST['package_id']);
	        $hotel_name_id =  intval(@$_POST['hotel_name_id'])===0 ? false : intval(@$_POST['hotel_name_id']);
	        $datein =  @$_POST['date_in'];
	        $dateout =  @$_POST['date_out'];
	        $list_srchg = array();
	        $list_hotel= $this->booking->getBysummaryCheckRatePackageHotel($packageid, $hotel_name_id);
	        for ($i=0; $i < count($list_hotel); $i++) { 
	            $list_srchg[$i] = new stdClass();

	            $list_srchg[$i]->pkg = $this->surchargestorage->getSurchargeByType_id("package",$list_hotel[$i]->PACK_ID, TRUE);
	            $list_srchg[$i]->htl = $this->surchargestorage->getSurchargeByType_id("hotel",$list_hotel[$i]->HOTEL_ID, TRUE);

	            if ($datein != null && $dateout != null) {
	                $surcharges_pkg = array("packages"=>$list_srchg[$i]->pkg);
	                $surcharges_htl = array("hotels"=>$list_srchg[$i]->htl);

	                $dates = array("return"=>$dateout,"depart"=>$datein);

	                $list_srchg[$i]->pkg = $this->surchargestorage->processSurchargePackage($surcharges_pkg, $dates);
	                $list_srchg[$i]->htl = $this->surchargestorage->processSurchargeHotels($surcharges_htl, $dates);

	                $tmp_pkg_surchg = $this->surchargestorage->processSurchargePackage($surcharges_pkg, $dates);
	                $count_test_pkg = count($tmp_pkg_surchg);
	                $tmp_pkg_surchg_list = array();
	                if($count_test_pkg!=0){
	                    foreach ($tmp_pkg_surchg as $categ) {
	                        foreach ($categ as $surcharge) {
	                            $tmp_pkg_surchg_list[]=$surcharge;
	                        }       
	                    }
	                }
	                $list_srchg[$i]->pkg = $tmp_pkg_surchg_list;

	                $tmp_htl_surchg = $this->surchargestorage->processSurchargeHotels($surcharges_htl, $dates);
	                $count_test_htl = count($tmp_htl_surchg);
	                $tmp_htl_surchg_list = array();
	                if($count_test_htl!=0){
	                    foreach ($tmp_htl_surchg as $categ) {
	                        foreach ($categ as $surcharge) {
	                            $tmp_htl_surchg_list[]=$surcharge;
	                        }       
	                    }
	                }
	                $list_srchg[$i]->htl = $tmp_htl_surchg_list;

	            }

	            $list_hotel[$i]->hotelroom = $this->booking->getBysummaryCheckRateHotelRoom($list_hotel[$i]->PACK_HOTEL_ID);
	            $list_srchg[$i]->hrm = array();
	            for ($hrm=0; $hrm < count($list_hotel[$i]->hotelroom); $hrm++) { 
	                @$tmp_var = $list_hotel[$i]->hotelroom[$hrm];
	                
	                if ($datein && $dateout) {
	                    $tmp_hrm_list = $this->surchargestorage->getSurchargeByType_id("hotel",$tmp_var->HRM_ID, TRUE);
	                    
	                    $tmp_hrm = $this->surchargestorage->getRoomRateSurcharge_id($tmp_var->HRM_ID);
	                    if ($tmp_hrm !== FALSE) {
	                        $surcharges_hrm = array("hotelrooms"=>$tmp_hrm);
	                        $dates = array("return"=>$dateout,"depart"=>$datein);

	                        $tmp_hrm_surchg = $this->surchargestorage->processSurchargeHotelRooms($surcharges_hrm, $dates);

	                        $count_test = count($tmp_hrm_surchg);

	                        $tmp_hrm_surchg_rm = array();
	                        if($count_test!=0){
	                            foreach ($tmp_hrm_surchg as $categ) {
	                                foreach ($categ as $surcharge) {
	                                    $tmp_hrm_surchg_rm[]=$surcharge;
	                                }       
	                            }
	                        }
	                        $list_srchg[$i]->hrm[$hrm] = $tmp_hrm_surchg_rm;
	                    }
	                }
	                else {
	                    //$list_srchg[$i]->hrm[] = $this->surchargestorage->getSurchargeByType_id("hotelroom_surcharges",$tmp_var->HRM_ID, TRUE);
	                     $list_srchg[$i]->hrm[] = '';
	                }

	            }
	            //dump($list_srchg);
	            //dump($list_hotel);
	        }
	            //dump(array('ListHotel'=>$list_hotel,'ListSurcharges'=>$list_srchg));
	            //die(json_encode($data));
	            die(json_encode(array('ListHotel'=>$list_hotel,'ListSurcharges'=>$list_srchg)));
        }
        else{
        	
        	echo 'restrict !';
        }
	}
}