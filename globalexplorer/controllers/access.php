<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Access extends MY_Controller {
	
	public function __construct() {
		$this->setAllAccess(true);
		parent::__construct();
	}
	
	public function index() {
		
	}
	public function login_view() {
		$this->load->view('login_view');
	}	

	public function login() {

		if (!$this->isLoggedIn()) {
			$this->load->model('user/userauthentication');
			$this->load->model('user/userstorage');
			$this->load->library('form_validation');
			$this->form_validation->set_rules('email_login', 'Username', 'trim|required|min_length[5]|max_length[100]|xss_clean');

			if ($this->form_validation->run() == FALSE)
			{
				$this->flash_message('You have input an invalid username', TRUE, '');
			}

			$uname=$this->input->post('email_login');
			$username=$this->userstorage->getByUser($uname);
			if(@$username[0]->email){
				$email = $username[0]->email;
			}elseif(@$username[0]->email == $uname){
				$email = $uname;
			}else{
				$this->flash_message('You have entered an invalid username', TRUE, '');
			}

			$password = $this->input->post('password_login');

			if ($email && $password) {
				$status = $this->userauthentication->getStatus($email);

				if($status !== false){
					if ($status == 'active') {

						if ($this->userauthentication->setStored($email)) {

							if ($this->userauthentication->verify($password)) {
								$user = $this->userauthentication->getStoredUser();
								if ($user instanceof TypedUserInterface) {
									$this->userpersistence->persistUser($user);
									// dump_exit($user);
									//dump_exit($this->session->userdata('username'));
									if ($user instanceof Customer) {
										$this->userpersistence->releaseUser();
										redirect('errors/restricted_area_for_customer');
									}else{
										if($this->session->userdata('type')=="agent"){
											redirect('panel/create_booking');
										}else {
											redirect('panel');
										}
									}
								}
							} else {
								// Bad Password
								$this->flash_message('You have entered an invalid password', TRUE, '');
							}
						} else {
							// Bad Email
							$this->flash_message('You have entered an invalid email', TRUE, '');
						}
					} elseif ($status == 'pending') {
						$this->flash_message('Pending Account: Please confirm this email', TRUE, '');
					} else {
						$this->flash_message('Log in not allowed. User has been '. $status .'.', TRUE, '');
					}
				} else {
					$this->flash_message('You have entered an invalid email', TRUE, '');
				}
			} else {
							// Bad Input
				$this->flash_message('Input invalid', TRUE, '');
			}
		}
	}

	public function logout() {
		if ($this->isLoggedIn()) {
			$this->userpersistence->releaseUser();
			redirect('');
		} else {
			redirect('errors/restricted_area');
		}
	}
	
	public function flash_message($message, $do_redirect=FALSE, $redirect_location=''){
		$this->session->set_flashdata('flash_message', $message);
		if($do_redirect) redirect($redirect_location);
	}
}