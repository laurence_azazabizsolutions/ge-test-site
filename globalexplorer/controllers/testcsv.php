<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @author Kirstin Rubica
 *
 */
class TestCSV extends MY_Controller {
	
	function readCSV(){
        $this->load->library('csvreader');
        $results =   $this->csvreader->parse_file('files/niko.csv',true);
        echo "<pre>";
        print_r($results);
        echo "</pre>";
        die;
        $data['csvData'] =  $results;
        $this->load->view('view_csv', $data);  
	}

	function readExcel(){
		// Load the spreadsheet reader library
		$this->load->library('excel_reader');

		// Read the spreadsheet via a relative path to the document
		// for example $this->excel_reader->read('./uploads/file.xls');
		// echo base_url('Surcharge.xls');
		$this->excel_reader->read('files/niko.xls');
		//echo "we're in!";die;
		// Get the contents of the first worksheet
		$worksheet = $this->excel_reader->worksheets[0];
        echo "<pre>";
        print_r($worksheet);
        echo "</pre>";
	}

	public function testphpexcel(){

		//$file = 'files/format.xlsx';
		$file = 'files/Format New 10-20-2014.xls';

 
		//load the excel library
		$this->load->library('excel');
		 
		//read file from path
		$objPHPExcel = PHPExcel_IOFactory::load($file);
		
		//to use excel5 reader
		// $objReader = new PHPExcel_Reader_Excel5();
		
		//to list sheets from excel
		// $sheets = $objReader->listWorksheetNames($file); 
		//to activate through index of sheet
		// $objPHPExcel->setActiveSheetIndex(2);
		//to get title of active sheet
		// $title =  $objPHPExcel->getActiveSheet()->getTitle();
		// print_r($title);die;
		
		//get only the Cell Collection
		$cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();
		//extract to a PHP readable array format
		foreach ($cell_collection as $cell) {
		    $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
		    $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
		    //with richtext data
		    // $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
			//to remove richtext 
			$data_value = ($objPHPExcel->getActiveSheet()->getCell($cell)->getValue() instanceof PHPExcel_RichText) ?
    		$objPHPExcel->getActiveSheet()->getCell($cell)->getValue()->getPlainText() : $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
		
		    //header will/should be in row 1 only. of course this can be modified to suit your need.
		    /*if ($row == 1) {
		        $header[$row][$column] = $data_value;
		    } else {
		        $arr_data[$row][$column][] = $data_value;
		    }*/
		    $arr_data[$row][$column][] = $data_value;
		}
		
	/*	$sample = array('Niko','amaw','buang');
		if(in_array('Niko', $sample))
					echo 'bananan'; return false;
		echo "<pre>";
		//print_r($header);
		print_r($sample);
		echo "</pre>";return false;*/

		
		echo "<pre>";
		//print_r($header);
		print_r($arr_data);
		echo "</pre>";return false;

		foreach($arr_data as $column)
		{
			foreach($column as $row)
			{
				if(in_array('Niko', $row))
					echo 'bananan'; return false;
				foreach($row as $value)
				{
					if(in_array('Niko', $value))
					echo 'bananan'; return false;
				}
			}
		}
		die;

		//send the data in an array format
		$data['header'] = $header;
		$data['values'] = $arr_data;
	}
}
