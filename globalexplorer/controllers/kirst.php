<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Kirst extends CI_Controller {

	public function test(){

		$string = "Kirstin Dominique Acha Rubica1 \t Kirstin Dominique Acha Rubica2          \n Kirstin Dominique Acha Rubica3 \r Kirstin Dominique Acha Rubica4";
		// $string = preg_replace('/\s+/', ' ', $text);
		echo "<pre>";
		print_r($string);
		echo "</pre>";
		// echo trim($string);
		echo str_word_count($string,0);

		$str_rep = preg_replace("/\s\s+/", " ", $string);
		$str_trim = trim($str_rep, "\x00..\x1F");
		$str_arr = explode(' ', $str_trim);
		echo count($str_arr);
	}

	public function test2(){
		echo "<pre>";
		$text   = "\t\tThese are a few words :) ...  ";
		$binary = "\x09Example string\x0A";
		$hello  = "Hello World";
		var_dump($text, $binary, $hello);

		print "\n";

		$trimmed = trim($text);
		var_dump($trimmed);

		$trimmed = trim($text, " \t.");
		var_dump($trimmed);

		$trimmed = trim($hello, "Hdle");
		var_dump($trimmed);

		$trimmed = trim($hello, 'HdWr');
		var_dump($trimmed);

		// trim the ASCII control characters at the beginning and end of $binary
		// (from 0 to 31 inclusive)
		$clean = trim($binary, "\x00..\x1F");
		var_dump($clean);
		echo "</pre>";

	}

}