<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @author Joshua Paylaga
 *
 */
class MainPanel extends MY_Controller {

	/**
	 * Constructor
	 */
	public function __construct() {
		parent::__construct();
		$this->load->helper(array('form','url'));
		$this->load->database();

		//	send mail
		$this->load->library('My_PHPMailer');
		$this->load->helper('email');
		$this->mail = new PHPMailer();
		$this->mail->SMTPDebug = 0;
		$this->mail->isSMTP();																// Set mailer to use SMTP
		$this->mail->Host = 'cot.cottoncare.com.sg';										// Specify main and backup SMTP servers
		$this->mail->SMTPAuth = true;														// Enable SMTP authentication
		$this->mail->Username = 'no-reply@budgetmaid.com.sg';								// SMTP username
		$this->mail->Password = 'noreply@123';												// SMTP password
		$this->mail->SMTPSecure = 'tls';													// Enable TLS encryption, `ssl` also accepted
		$this->mail->Port = 587;															// TCP port to connect to

    	// $this->mail->addBCC('johncabs018@yahoo.com');
    	// $this->mail->addBCC('jude@azazabizsolutions.com');

		/*if ($this->userpersistence->getPersistedUser() instanceof Customer) {
			redirect('errors/restricted_area');
			$this->gethotelroom_packagehotelid();
			$this->getOptionalHotel();
		}*/
		if(count($this->session->userdata('fields'))<=1)
			{$this->session->set_userdata('fields',array(1,'code','details','flight','amount','payment','status','package','agent','hotel','traveller','origin','return','price','agent_pay','provider_pay'));}
		else
			{$this->session->set_userdata('fields',$this->session->userdata('fields'));}


	}


	/**
	 * Default Controller action
	 */
	public function index() {
		$userid=$this->session->userdata('id');
		$this->load->model('booking/booking');
		$this->load->model('user/user');
		$this->load->library("pagination");


		$search_array = false;
		$filter_arr = false;
		$start = false;
		$per_page = 10;


		$start_time = isset($_GET["origin"]) ? strtotime($this->input->get("origin",TRUE)) : "";
		$end_time = isset($_GET["return"]) ? strtotime($this->input->get("return",TRUE)) : "";
		$page_links = "";

		if(isset($_GET["per_page"])){
			if(!empty($_GET["per_page"])){
				$start = $this->input->get("per_page", TRUE);
			}else{
				$start = 0;
			}
		}else{
			$start = 0;
		}

		if(isset($_GET["booked_code"])){
			if(!empty($_GET["booked_code"])){
				//$search_array["bookings.code"] = $this->input->get("booked_code",TRUE);
				$filter_arr["bookings.code"] = $this->input->get("booked_code",TRUE);
			}
		}

		if(isset($_GET["booking_status"])){
			if(!empty($_GET["booking_status"])){
				$search_array["bookings.status"] = $this->input->get("booking_status",TRUE);
			}
		}

		if(isset($_GET["origin"])){
			if(!empty($_GET["origin"])){
				$start_time = date("Y-m-d 00:00:00",$start_time);
				$search_array["bookings.departure_date >="] = $start_time;
			}
		}

		if(isset($_GET["return"])){
			if(!empty($_GET["return"])){
				$end_time = date("Y-m-t 23:59:59",$end_time);
				$search_array["bookings.return_date <="] = $end_time;
			}
		}

		if(isset($_GET["a_companyname"])){
			if(!empty($_GET["a_companyname"])){
				$search_array["comp.name"] = $this->input->get("a_companyname",TRUE);
			}
		}

		if(isset($_GET["p_companyname"])){
			if(!empty($_GET["p_companyname"])){
				$search_array["companies.name"] = $this->input->get("p_companyname",TRUE);
			}
		}

		$countries_usr = $this->user->getAllCountries();
		$user_data = $this->userpersistence->getPersistedUser();


		$new_get = $_GET;
		unset($new_get["per_page"]);

		$config["base_url"]   = base_url("panel/")."?".http_build_query($new_get, '', "&"); #set the url link
		$config["per_page"]   = $per_page; # set the number of rows per page
		$config['num_links'] = 5;
		$config['page_query_string'] = TRUE;
		$config['full_tag_open'] = "<ul class='pagination pagination-sm'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";


		if(get_class($user_data) === 'Admin'){
			$select_alldata  = $this->booking->getAllBookingRecords_page(false,false,$search_array,$filter_arr);

			$config["total_rows"] = count($select_alldata); #count the total number of rows
			$this->pagination->initialize($config); #initialzie the library

			$select_alldata  = $this->booking->getAllBookingRecords_page($start,$per_page,$search_array,$filter_arr);
		}

		if(get_class($user_data) === 'Provider'){
			$select_alldata  = $this->booking->getAllBookingRecordsProvider_page($userid,false,false,$search_array,$filter_arr);

			$config["total_rows"] = count($select_alldata); #count the total number of rows
			$this->pagination->initialize($config); #initialzie the library

			$select_alldata  = $this->booking->getAllBookingRecordsProvider_page($userid,$start,$per_page,$search_array,$filter_arr);
		}

		if(get_class($user_data) === 'Agent' || get_class($user_data) === 'Customer'){
			$select_alldata  = $this->booking->getAllBookingRecordsAgent_page($userid,$start,false,$search_array,$filter_arr);

			$config["total_rows"] = count($select_alldata); #count the total number of rows
			$this->pagination->initialize($config); #initialzie the library

			$select_alldata  = $this->booking->getAllBookingRecordsAgent_page($userid,$start,$per_page,$search_array,$filter_arr);
		}

		for ($i=0; $i < count($select_alldata) ; $i++) {
			$select_alldata[$i]->hotel=$this->booking->getAllBookinghotelRecord($select_alldata[$i]->BOOKID);
		}

		$userID_agent = $user_data->getUser()->getId();
		$query=$this->booking->getBysummaryPricelistcsv($userID_agent);
		$res_agentprovider=$this->booking->getByuserAgentandProvider();
		$airport_map=$this->booking->getBysummaryPdfairport();
		$user_type = $this->userpersistence->getUserType($userID_agent);
		//dump_exit($user_type);
		$this->panelView(null, array(
				'salutations' => array(
						'mr.' => 'Mr.',
						'ms.' => 'Ms.',
						'mrs.' => 'Mrs.',
				),
				'countries_usr' => $countries_usr,
				'user' => $user_data,
				'booked_data'=>$select_alldata,
				'user_type'	=> $user_type,
				'result_agentprovider'=>$res_agentprovider,
				//'booked_hotel_data'=>$select_hotel,
				"page_links" => $this->pagination->create_links(),
				'csv'=>$query,
				'airport_map'=>$airport_map
		));

		
		
	}

	public function pending_bookingsss() {
		if ($this->userpersistence->getPersistedUser() instanceof Customer) {
			redirect('errors/restricted_area');
		}
		$this->load->model('booking/booking');
		$this->load->model('user/user');
		$countries_usr = $this->user->getAllCountries();
		// $this->db->order_by('country');
		// $countries = $this->db->get('countries')->result_array();
		$user_data = $this->userpersistence->getPersistedUser();
		$select_alldata  = $this->booking->getAllBookingRecords_pending();

		for ($i=0; $i < count($select_alldata) ; $i++) {
			$select_alldata[$i]->hotel=$this->booking->getAllBookinghotelRecord($select_alldata[$i]->BOOKID);
		}

		$this->panelView(null, array(
				'salutations' => array(
						'mr.' => 'Mr.',
						'ms.' => 'Ms.',
						'mrs.' => 'Mrs.',
				),
				'countries_usr' => $countries_usr,
				'user' => $user_data,
				'booked_data'=>$select_alldata
		));
	}

	public function pending_bookings() {
		if ($this->userpersistence->getPersistedUser() instanceof Customer) {
			redirect('errors/restricted_area');
		}

		$this->load->library("pagination");
		$this->load->model('booking/booking');
		$this->load->model('user/user');
		$search_array["bookings.status"] = "pending";
		$per_page = 10;
		$page_links = "";
		$new_get = $_GET;
		unset($new_get["per_page"]);

		$config["base_url"]   = base_url("panel/pending_bookings/")."?".http_build_query($new_get, '', "&"); #set the url link
		$config["per_page"]   = $per_page; # set the number of rows per page
		$config['num_links'] = 5;
		$config['page_query_string'] = TRUE;
		$config['full_tag_open'] = "<ul class='pagination pagination-sm'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";

		$countries_usr = $this->user->getAllCountries();
		// $this->db->order_by('country');
		// $countries = $this->db->get('countries')->result_array();
		$user_data = $this->userpersistence->getPersistedUser();
		/*$select_alldata  = $this->booking->getAllBookingRecords_pending();

		for ($i=0; $i < count($select_alldata) ; $i++) {
			$select_alldata[$i]->hotel=$this->booking->getAllBookinghotelRecord($select_alldata[$i]->BOOKID);
		}*/



		$select_allpending = $this->booking->getAllBookingRecords_page_pending();
		$config["total_rows"] = count($select_allpending); #count the total number of rows
		$this->pagination->initialize($config); #initialzie the library
		for ($i=0; $i < count($select_allpending) ; $i++) {
			$select_allpending[$i]->hotel=$this->booking->getAllBookinghotelRecord($select_allpending[$i]->BOOKID);
		}
		$this->panelView(null, array(
				'salutations' => array(
						'mr.' => 'Mr.',
						'ms.' => 'Ms.',
						'mrs.' => 'Mrs.',
				),
				'countries_usr' => $countries_usr,
				'user' => $user_data,
				'booked_data'=>$select_allpending,
				"page_links" => $this->pagination->create_links()
		));
	}

	public function packageBatchUpdate(){
		$id = $this->input->post('id'); //array of id
		$package_id = $this->input->post('packageid'); //array of id
        $room_type = $this->input->post('hotelroomrate_name'); //array of item name
        $description = $this->input->post('hotelroomrate_description'); //array or qty
        $rate_type = $this->input->post('hotelroomrate_type'); //array of price
        $cost = $this->input->post('hotelroomrate_cost'); // not array
        $profit = $this->input->post('hotelroomrate_profit'); //array or qty
        $ext_cost = $this->input->post('hotelroomrate_cost_ext'); //array of price
        $ext_profit = $this->input->post('hotelroomrate_profit_ext'); // not array
        //echo json_encode(array('status' => $id));
        switch ($this->input->post('act_type')) {
        	case 'edit':
        		$updateArray = array();
		        for($x = 0; $x < sizeof($id); $x++){
		        		if($room_type[$x] == 'twin')
					    	$pax = 2;
					    elseif($room_type[$x] == 'triple')
					    	$pax = 3;
					    elseif($room_type[$x] == 'quadruple')
					    	$pax = 4;
					    else
					    	$pax = 1;
			            $updateArray[] = array(
							'id'=>$id[$x],
			                'room_type' => $room_type[$x],
			                'description' => $description[$x],
			                'rate_type' => $rate_type[$x],
			                'cost' => $cost[$x],
			                'profit' => $profit[$x],
			                'ext_cost' => $ext_cost[$x],
			                'ext_profit' => $ext_profit[$x],
			                'pax' 	=> $pax
			            );
		      		}
            	$res = $this->db->update_batch('hotelroom_rates',$updateArray, 'id');

	           if(count($updateArray) > 0)
					echo json_encode(array('status' => true));
				else
					echo json_encode(array('status' => false));
        		break;

         	case 'add':
        		$data = array();
		        for($x = 0; $x < sizeof($room_type); $x++){
		        		if($room_type[$x] == 'twin')
					    	$pax = 2;
					    elseif($room_type[$x] == 'triple')
					    	$pax = 3;
					    elseif($room_type[$x] == 'quadruple')
					    	$pax = 4;
					    else
					    	$pax = 1;
			            $data[] = array(
							'package_hotel_id'=>$package_id,
			                'room_type' => $room_type[$x],
			                'description' => $description[$x],
			                'rate_type' => $rate_type[$x],
			                'cost' => $cost[$x],
			                'profit' => $profit[$x],
			                'ext_cost' => $ext_cost[$x],
			                'ext_profit' => $ext_profit[$x],
			                'status' => 'active',
			                'pax'	=> 	$pax,
			            );
		      		}
					$res = $this->db->insert_batch('hotelroom_rates', $data);
					if(count($res) > 0)
						echo json_encode(array('status' => true));
					else
						echo json_encode(array('status' => false));
        		break;
        }
	}

	public function package_batch_edit() {
		if ($this->userpersistence->getPersistedUser() instanceof Customer) {
			redirect('errors/restricted_area');
		}
		$package_id = $this->uri->segment(3, 0);
		$this->load->helper('url');
		$this->load->model('package/packagestorage');
		if(!$_POST)
		{
			if(!$package_id) redirect('panel/view_packages');
			$results['data'] = $this->packagestorage->getHotelrates($package_id);
			$this->load->view('panel/mainpanel/package_batch_edit',$results);
		}else{
			$dateArr = explode("/",$this->input->post('date_range'));
			$results['data'] = $this->packagestorage->getHotelrates1($package_id,$dateArr[0],$dateArr[1]);
			$this->load->view('panel/mainpanel/package_batch_edit',$results);
		}

		// if($_POST){
		// 	$dateArr = explode("/",$this->input->post('date_range'));
		// 	$results['data'] = $this->packagestorage->getHotelrates1($package_id,$dateArr[0],$dateArr[1]);
		// 	dump_exit($results['data']);
		// 	//$this->load->view('panel/mainpanel/package_batch_edit',$results);
		// }else{
			// $results['data'] = $this->packagestorage->getHotelrates($package_id);
			// $this->load->view('panel/mainpanel/package_batch_edit',$results);
		//}
	}

	public function package_batch_edit_view() {
		if ($this->userpersistence->getPersistedUser() instanceof Customer) {
			redirect('errors/restricted_area');
		}
		$package_id = $this->uri->segment(3, 0);
		$this->load->helper('url');
		if(!$package_id) redirect('panel/view_packages');
		$this->load->model('package/packagestorage');

		$results['data'] = $this->packagestorage->getHotelrates($package_id);
		$this->load->view('panel/mainpanel/package_batch_edit_view',$results);
	}

	public function package_batch_add() {
		if ($this->userpersistence->getPersistedUser() instanceof Customer) {
			redirect('errors/restricted_area');
		}
		$package_id ['packageid'] = $this->uri->segment(3, 0);
		$this->load->helper('url');
		if(!$package_id) redirect('panel/view_packages');
		$this->load->view('panel/mainpanel/package_batch_add',$package_id);
	}

	public function package_batch_copy() {

		$this->load->model('package/packagestorage');

		if ($this->userpersistence->getPersistedUser() instanceof Customer) {
			redirect('errors/restricted_area');
		}

		if($_POST){
			$package_hotel_id	=	$this->input->post('package_hotel_id');	
			$room_type 			=	$this->input->post('room_type');
			$description 		=	$this->input->post('description');
			$rate_type 			=	$this->input->post('rate_type')	;	
			$cost 				= 	$this->input->post('hotelroomrate_cost'); 
		    $profit 			= 	$this->input->post('hotelroomrate_profit');
		    $ext_cost 			= 	$this->input->post('hotelroomrate_cost_ext'); 
		    $ext_profit 		= 	$this->input->post('hotelroomrate_profit_ext'); 
		    $date_from 			= 	$this->input->post('hotelroomrate_date_from');
		    $date_to 			= 	$this->input->post('hotelroomrate_date_to');
		    
			$data = array();
	        for($x = 0; $x < sizeof($cost); $x++){
	        	if($room_type[$x] == 'twin')
			    	$pax = 2;
			    elseif($room_type[$x] == 'triple')
			    	$pax = 3;
			    elseif($room_type[$x] == 'quadruple')
			    	$pax = 4;
			    else
			    	$pax = 1;
	            $data[] = array(
					'package_hotel_id'		=>	$package_hotel_id,
	                'room_type' 			=> 	$room_type[$x],
	                'description' 			=> 	$description[$x],
	                'rate_type' 			=> 	$rate_type[$x],
	                'cost' 					=> 	$cost[$x],
	                'profit' 				=> 	$profit[$x],
	                'ext_cost' 				=> 	$ext_cost[$x],
	                'ext_profit' 			=> 	$ext_profit[$x],
	                'status'				=> 	'active',
	                'pax'					=> 	$pax,
	                'date_from'				=> 	$date_from,
	                'date_to'				=> 	$date_to
	            );
      		}
			$res = $this->db->insert_batch('hotelroom_rates', $data);
			if(count($res) > 0)
			{
				echo "<script>alert('Successfullly added.')</script>";
				// $results['data'] = $this->packagestorage->getHotelrates($package_id);
				// $this->session->set_flashdata('success', 'Successfully added new rates!');
				$this->package_edit();
			}
			else
				echo json_encode(array('status' => false));
		}


		else{
			$package_id = $this->uri->segment(3, 0);
			$this->load->helper('url');
			if(!$package_id) redirect('panel/view_packages');
			
			$results['data'] = $this->packagestorage->getHotelrates($package_id);
			$this->load->view('panel/mainpanel/package_batch_copy',$results);
		}
		
	}

	public function resend($booking_id=false) {

		$this->load->helper('string');
		$toHash = random_string('alpha', 10);
		$hash = hashmonster($toHash);
		$this->db->insert('confirmation_booking', array(
			'booking_id' => $booking_id,
			'hash' => $hash
		));
		$link = base_url('change/confirm_booking/'.$hash);

		$sum_provider = $this->input->POST('Provider');
		$sum_agent = $this->input->POST('Agent');

		$booking_summary = false;
	    $booking_summary['confirm'] = $link.'/confirm';
	    $booking_summary['reject'] = $link.'/reject';
	    $booking_summary['cancel'] = $link.'/cancel';

		$this->load->model("booking/booking");

        $mail_provider=$booking_id;
		$get_provider_email=$this->booking->getEmailprovider($mail_provider);
		$get_agent_email=$this->booking->getEmailagent($mail_provider);

     	$cnt = 0;

        $mail = $this->mail;
    	$mail->setFrom('no-reply@ge.com.sg', 'Global Explorer');						// Add a recipient

    	$mail->WordWrap = 50;                                 							// Set word wrap to 50 characters
	    $mail->isHTML(true);                                  							// Set email format to HTML

    	$code = $this->booking->getBookingCode(@$booking_id);
		@$booking_ids = $this->booking->getBookingIDs($code[0]["code"]);

		if(count($booking_ids)){
			for($i=0;$i<count($booking_ids);$i++){
				$booking_summary[] = $this->getSummaryViewas($booking_ids[$i]["id"],$i);
			}
		}

	    $mail->Subject = $code[0]['code'] .' ['. $code[0]['status'] .'] Global Explorer Booking Notification';


		if ($sum_provider === "Provider") {
			$mail->ClearAddresses();
			$mail->ClearCCs();

			$booking_summary['recipient'] =$get_provider_email[0]->providerfname.' '.$get_provider_email[0]->providerlname;
 		 	$booking_summary['recipient_id'] = $get_provider_email[0]->providerid;
     		if($get_provider_email[0]->alteremail != "" || $get_provider_email[0]->alteremail != NULL) {
				$wang=explode(',',$get_provider_email[0]->alteremail);
	    		for ($i=0; $i < count($wang); $i++) {
		     		$mail->addCC($wang[$i],$booking['recipient']);
				}
			}
			$mail->addAddress($get_provider_email[0]->provideremail,$booking_summary['recipient']);
    		$mail->Body    = $this->load->view('panel/mainpanel/resend_email_provider',$booking_summary,TRUE);
		}

		if ($sum_agent === "Agent") {
			$mail->ClearAddresses();
			$mail->ClearCCs();

			$booking_summary['recipient'] =$get_agent_email[0]->agentfname.' '.$get_agent_email[0]->agentlname;
     		if($get_agent_email[0]->alteremail != "" || $get_agent_email[0]->alteremail != NULL) {
				$wang=explode(',',$get_agent_email[0]->alteremail);
	    		for ($i=0; $i < count($wang); $i++) {
		     		$mail->addCC($wang[$i],$booking['recipient']);
				}
			}
     		$mail->addAddress($get_agent_email[0]->agentemail,$booking_summary['recipient']);

    		$mail->Body    = $this->load->view('panel/mainpanel/resend_email',$booking_summary,TRUE);
    		if($mail->send());
		}

     	//$mail->Body    = $this->load->view('booking_confirmation_provider',$booking,TRUE);
    	$mail->AltBody = 'Booking Confirmation [Global Explorer]';
	    if($mail->send());

     	redirect('panel/package/'.$booking_id);
	}

	public function getDiscount($booking_id=false) {
		$this->load->model("booking/booking");
		$discount = $this->input->post("discount");
		$result = $this->booking->addDiscount(@$booking_id, $discount);

     	redirect('panel/package/'.$booking_id);
	}

	public function getAdditional($booking_id=false) {
		$this->load->model("booking/booking");
		$additional = $this->input->post("additional");
		$result = $this->booking->addAdditionalCharge(@$booking_id, $additional);

     	redirect('panel/package/'.$booking_id);
	}

	public function getDiscountPayment($booking_id=false) {
		$this->load->model("booking/booking");
		if($this->input->post("discount")){
			$discount = $this->input->post("discount");
			$result = $this->booking->addDiscount(@$booking_id, $discount);
	     	redirect($_POST['old_url']);
		}else{
			$difference = $this->input->post("difference");
			$result = $this->booking->addDifference(@$booking_id, $difference);
	     	redirect($_POST['old_urls']);
		}
	}

	public function getSummaryViewas($booking_id,$show_hide){
		$this->load->model('booking/booking');
		$this->load->model("booking/bookingstorage");
		$this->load->model('user/user');
		$countries_usr    = $this->user->getAllCountries();
		$summary_data     =$this->booking->getBysummary($booking_id);
		$gettotalnights   =$this->booking->getBysummaryPackage($booking_id);
		$summary_guest    =$this->booking->getByGuest($booking_id);
		$provider_package =$this->booking->getBysummaryCompany($booking_id);


		#get the individual summaries ----------------------------------------------
		$package_summary   = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"type"=>"0package_surcharge"));
		$addon_summary     = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"type"=>"4addon"));
		$extension_summary = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"type"=>"5extension"));

        #fetch the destination IDs ----------------------------------------------
        $destination_summary = $this->bookingstorage->getSummaryDestinations($booking_id);

        for($i=0;$i<count($destination_summary);$i++){
			$destination     = $destination_summary[$i];
			#fetch the hotel, room surcharge, room summaries ----------------------------------------------
			$hotel_summary[] = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"destination_id"=>$destination["destination_id"],"type"=>"1hotel_surcharge"));
			$rs_summary[]    = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"destination_id"=>$destination["destination_id"],"type"=>"2room_surcharge"));
			$r_summary[]     = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"destination_id"=>$destination["destination_id"],"type"=>"3room_payment"));
        }

		$user_data = $this->userpersistence->getPersistedUser();

		$data['countries_usr']        = $countries_usr;
		$data['user']                 =$user_data;
		$data['data_summary']         =$summary_data;
		$data['guest_summary']        =$summary_guest;
		$data['package_total_nights'] =$gettotalnights;
		$data['provider_summary']     =$provider_package;
		$data['package_summary']      =@$package_summary;
		$data['addon_summary']        =@$addon_summary;
		$data['extension_summary']    =@$extension_summary;
		$data['destination_summary']  =@$destination_summary;
		$data['hotel_summary']        =@$hotel_summary;
		$data['rs_summary']           =@$rs_summary;
		$data['r_summary']            =@$r_summary;
		$data['show_hide']            =@$show_hide;

		return $this->load->view("panel/mainpanel/append_package.php",$data,TRUE);
	}

	public function package($booking_id=false) {
		$this->load->model("booking/booking");
		//$checker = $this->booking->getBookingCheckerAccount(@$booking_id);
		//$checker_agent_account = $this->booking->getBookingCheckerAccountAgent(@$booking_id);
		$user_data = $this->userpersistence->getPersistedUser();


		//if(get_class($user_data) === 'Provider' || @$checker_agent_account[0]['user_id'] == $user_data->getUser()->getId() || @$checker[0]['user_id'] == $user_data->getUser()->getId()){
			$code = $this->booking->getBookingCode(@$booking_id);
			@$booking_ids = $this->booking->getBookingIDs($code[0]["code"]);

			$booking_summary = false;
			if(count($booking_ids)){
				for($i=0;$i<count($booking_ids);$i++){
					$booking_summary[] = $this->getSummaryView($booking_ids[$i]["id"],$i);
				}
			}

			$this->panelView(null, array(
				"booking_summary"=>$booking_summary
			));
		// }
		// else{
		// 	redirect('errors/restricted_area');
		// }
	}

	public function getSummaryView($booking_id,$show_hide){
		$this->load->model('booking/booking');
		$this->load->model("booking/bookingstorage");
		$this->load->model('user/user');
		$countries_usr    = $this->user->getAllCountries();
		$summary_data     =$this->booking->getBysummary($booking_id);
		$gettotalnights   =$this->booking->getBysummaryPackage($booking_id);
		$summary_guest    =$this->booking->getByGuest($booking_id);
		$provider_package =$this->booking->getBysummaryCompany($booking_id);
		$summary_datas	  =$this->booking->getBysummaryPackage($booking_id);
		$summary_dataBooking =$this->booking->getBysummaryPackageAddHotelBooking($booking_id);


		#get the individual summaries ----------------------------------------------
		$package_summary   = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"type"=>"0package_surcharge"));
		$addon_summary     = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"type"=>"4addon"));
		$extension_summary = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"type"=>"5extension"));

        #fetch the destination IDs ----------------------------------------------
        $destination_summary = $this->bookingstorage->getSummaryDestinations($booking_id);

        for($i=0;$i<count($destination_summary);$i++){
			$destination     = $destination_summary[$i];
			#fetch the hotel, room surcharge, room summaries ----------------------------------------------
			$hotel_summary[] = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"destination_id"=>$destination["destination_id"],"type"=>"1hotel_surcharge"));
			$rs_summary[]    = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"destination_id"=>$destination["destination_id"],"type"=>"2room_surcharge"));
			$r_summary[]     = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"destination_id"=>$destination["destination_id"],"type"=>"3room_payment"));
        }

        $passengers 	=	$this->booking->getPassengers($booking_id);

		$user_data = $this->userpersistence->getPersistedUser();

		$data['countries_usr']        = $countries_usr;
		$data['user']                 =$user_data;
		$data['data_summary']         =$summary_data;
		$data['guest_summary']        =$summary_guest;
		$data['package_total_nights'] =$gettotalnights;
		$data['provider_summary']     =$provider_package;
		$data['package_summary']      =@$package_summary;
		$data['addon_summary']        =@$addon_summary;
		$data['extension_summary']    =@$extension_summary;
		$data['destination_summary']  =@$destination_summary;
		$data['hotel_summary']        =@$hotel_summary;
		$data['rs_summary']           =@$rs_summary;
		$data['r_summary']            =@$r_summary;
		$data['show_hide']            =@$show_hide;
		$data['package_summarys']	  =@$summary_datas;
		$data['add_hotel_bookings']	  =@$summary_dataBooking;
		$data['passengers']			  =@$passengers;

		return $this->load->view("panel/mainpanel/append_package.php",$data,TRUE);
	}

	public function getSummaryViewByEmail($booking_id,$tracks_change){
		$this->load->model('booking/booking');
		$this->load->model("booking/bookingstorage");
		$this->load->model('user/user');
		$countries_usr    = $this->user->getAllCountries();
		$summary_data     =$this->booking->getBysummary($booking_id);
		$gettotalnights   =$this->booking->getBysummaryPackage($booking_id);
		$summary_guest    =$this->booking->getByGuest($booking_id);
		$provider_package =$this->booking->getBysummaryCompany($booking_id);

		#get the individual summaries ----------------------------------------------
		$package_summary   = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"type"=>"0package_surcharge"));
		$addon_summary     = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"type"=>"4addon"));
		$extension_summary = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"type"=>"5extension"));

        #fetch the destination IDs ----------------------------------------------
        $destination_summary = $this->bookingstorage->getSummaryDestinations($booking_id);

        for($i=0;$i<count($destination_summary);$i++){
			$destination     = $destination_summary[$i];
			#fetch the hotel, room surcharge, room summaries ----------------------------------------------
			$hotel_summary[] = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"destination_id"=>$destination["destination_id"],"type"=>"1hotel_surcharge"));
			$rs_summary[]    = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"destination_id"=>$destination["destination_id"],"type"=>"2room_surcharge"));
			$r_summary[]     = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"destination_id"=>$destination["destination_id"],"type"=>"3room_payment"));
        }

		$user_data = $this->userpersistence->getPersistedUser();

		$data['countries_usr']        = $countries_usr;
		$data['user']                 =$user_data;
		$data['data_summary']         =$summary_data;
		$data['guest_summary']        =$summary_guest;
		$data['package_total_nights'] =$gettotalnights;
		$data['provider_summary']     =$provider_package;
		$data['package_summary']      =@$package_summary;
		$data['addon_summary']        =@$addon_summary;
		$data['extension_summary']    =@$extension_summary;
		$data['destination_summary']  =@$destination_summary;
		$data['hotel_summary']        =@$hotel_summary;
		$data['rs_summary']           =@$rs_summary;
		$data['r_summary']            =@$r_summary;
		$data['tracks']            	  =$tracks_change;

		$email_view_user = get_Class($data['user']);
		return $data;

		// dump($email_view_user);
		// die;
		//return $this->load->view("panel/mainpanel/append_email.php",$data,TRUE);
	}

	public function getSummaryViewByEmail_agent($booking_id,$track_changes){
		$data = $this->getSummaryViewByEmail($booking_id,$track_changes);
		return $this->load->view("panel/mainpanel/append_email.php",$data,TRUE);
	}

	public function getSummaryViewByEmail_provider($booking_id,$track_changes){
		$data = $this->getSummaryViewByEmail($booking_id,$track_changes);
		return $this->load->view("panel/mainpanel/append_email_provider.php",$data,TRUE);
	}

	public function manage_user() {
		if ($this->userpersistence->getPersistedUser() instanceof Customer) {
			redirect('errors/restricted_area');
		}
		$this->load->library("pagination");
		$this->load->model('user/user');
		$countries_usr = $this->user->getAllCountries();

		$this->load->helper('url');

		$user_data = $this->userpersistence->getPersistedUser();
		if(!$user_data instanceof Admin) redirect('panel/');

		$this->load->database();
		// $this->db->order_by('country');
		//$countries = $this->user->getAllCountries();
		// $countries = $this->db->get('countries')->result_array();
		$companies = $this->db->get('companies')->result_array();
		$all_companies = array();

		foreach($companies as $company){
			if(!in_array($company['name'], $all_companies)) {
				$all_companies[] = $company['name'];
			}
		}

		// load users
		$this->load->model('user/userstorage');
		$this->userstorage->includeInactive(true);
		$this->userstorage->includePending(true);

		$tab = @$_GET["tab"];
		$start = isset($_GET["per_page"]) ? (intVal($_GET["per_page"])==0 ? 0 : intval($_GET["per_page"]) ) : 0;
		$limit = 10;

		$customers		= $this->userstorage->getByInformation(array('type' => 'customer'));
		$agents			= $this->userstorage->getByInformation(array('type' => 'agent'));
		$providers		= $this->userstorage->getByInformation(array('type' => 'provider'));
		$admins			= $this->userstorage->getByInformation(array('type' => 'admin'));
		$accounts		= $this->userstorage->getByInformation(array());


		$config["per_page"]          = $limit; # set the number of rows per page
		$config['num_links'] 		 = 5;
		$config['page_query_string'] = TRUE;
		$config['full_tag_open']     = "<ul class='pagination pagination-sm'>";
		$config['full_tag_close']    ="</ul>";
		$config['num_tag_open']      = '<li>';
		$config['num_tag_close']     = '</li>';
		$config['cur_tag_open']      = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close']     = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open']     = "<li>";
		$config['next_tagl_close']   = "</li>";
		$config['prev_tag_open']     = "<li>";
		$config['prev_tagl_close']   = "</li>";
		$config['first_tag_open']    = "<li>";
		$config['first_tagl_close']  = "</li>";
		$config['last_tag_open']     = "<li>";
		$config['last_tagl_close']   = "</li>";

		unset($_GET["tab"]);

		/*
		* customers
		*/
		$config["total_rows"] = count($customers); #count the total number of rows
		$config["base_url"]   = base_url("panel/manage_user")."?tab=customers&".http_build_query($_GET, '', "&"); #set the url link
		$start_customer = count($customers)<$limit ? 0:$start;
		$this->pagination->initialize($config); #initialzie the library
		$p_link_customers = $this->pagination->create_links();


		/*
		*agents
		*/
		$config["total_rows"] = count($agents); #count the total number of rows
		$config["base_url"]   = base_url("panel/manage_user")."?tab=agents&".http_build_query($_GET, '', "&"); #set the url link
		$start_agent =count($agents)<$limit ? 0:$start;
		$this->pagination->initialize($config); #initialzie the library
		$p_link_agents = $this->pagination->create_links();

		/*
		*providers
		*/
		$config["total_rows"] = count($providers); #count the total number of rows
		$config["base_url"]   = base_url("panel/manage_user")."?tab=providers&".http_build_query($_GET, '', "&"); #set the url link
		$start_provider = count($providers)<$limit ? 0:$start;
		$this->pagination->initialize($config); #initialzie the library
		$p_link_providers = $this->pagination->create_links();

		/*
		*admins
		*/
		$config["total_rows"] = count($admins); #count the total number of rows
		$config["base_url"]   = base_url("panel/manage_user")."?tab=admins&".http_build_query($_GET, '', "&"); #set the url link
		$start_admin = count($admins)<$limit ? 0:$start;
		$this->pagination->initialize($config); #initialzie the library
		$p_link_admins = $this->pagination->create_links();

		/*
		*accounts
		*/
		$config["total_rows"] = count($accounts); #count the total number of rows
		$config["base_url"]   = base_url("panel/manage_user")."?tab=accounts&".http_build_query($_GET, '', "&"); #set the url link
		$start_account = count($accounts)<$limit ? 0:$start;
		$this->pagination->initialize($config); #initialzie the library
		$p_link_accounts = $this->pagination->create_links();



		$customers		= $this->userstorage->getByInformation(array('type' => 'customer'),$start_customer,$limit);
		$agents			= $this->userstorage->getByInformation(array('type' => 'agent'),$start_agent,$limit);
		$providers		= $this->userstorage->getByInformation(array('type' => 'provider'),$start_provider,$limit);
		$admins			= $this->userstorage->getByInformation(array('type' => 'admin'),$start_admin,$limit);
		$accounts		= $this->userstorage->getByInformation(array(),$start_account,$limit);


		$this->panelView(
			array(
				'agents'           => $agents,
				'customers'        => $customers,
				'providers'        => $providers,
				'admins'           => $admins,
				'accounts'         => $accounts,
				"p_link_accounts"  => $p_link_accounts,
				"p_link_admins"    => $p_link_admins,
				"p_link_providers" => $p_link_providers,
				"p_link_agents"    => $p_link_agents,
				"p_link_customers" => $p_link_customers,
				"tab"              => $tab
			),
			array(
				'salutations' => array(
						'mr.'  => 'Mr.',
						'ms.'  => 'Ms.',
						'mrs.' => 'Mrs.',
				),
				'user'          => $user_data,
				'countries_usr' => $countries_usr,
				'companies'     => json_encode($all_companies),
			)
		);
	}

	public function create_package() {
		if ($this->userpersistence->getPersistedUser() instanceof Customer) {
			redirect('errors/restricted_area');
		}
		$this->load->helper('form');
		$this->load->model('user/userstorage');
		$this->load->model('user/user');
		$countries_usr = $this->user->getAllCountries();

		$this->userstorage->includePending(true);
		$providers = $this->userstorage->getByInformation(array('type'=>'provider'));
		$this->db->order_by('country','asc');
		$countries = $this->db->get('countries')->result_array();
		$user_data = $this->userpersistence->getPersistedUser();
		$this->panelView(null, array(
				'salutations' => array(
						'mr.' => 'Mr.',
						'ms.' => 'Ms.',
						'mrs.' => 'Mrs.',
				),
				'countries' => $countries,
				'countries_usr' => $countries_usr,
				'providers' => $providers,
				'user' => $user_data
		));
	}

	public function view_packages() {
		$this->load->helper('url');
		if ($this->userpersistence->getPersistedUser() instanceof Customer) {
			redirect('errors/restricted_area');
		}
		$ref = (isset($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : FALSE;
		if($ref){
			$urlArr = parse_url($ref);
			$pathArr = explode('/', $urlArr['path']);

			if(@$pathArr[2] !== 'view_packages')
				$this->session->unset_userdata('filter_fields');
		}
		$this->load->model('package/package');
		$this->load->model('package/destination');
		$this->load->model('package/packagestorage');
		$this->load->model('user/user');

		// $post_data['asdf'] = 'bayot';
		$countries_usr = $this->user->getAllCountries();
		$user_data = $this->userpersistence->getPersistedUser();
		// $packages = $this->packagestorage->getPackages();
		$destinations = $this->destination->getActiveDestinations();

		//holds data from session
		if($this->session->userdata('filter_fields') === false)
		{
			$post_data = array('destination' => $destinations[0]['code']);
		}else
		{
			$post_data = $this->session->userdata('filter_fields');
		}

		$config['base_url'] = base_url('panel/view_packages');
		$config['per_page'] = 9;
		$config['num_links'] = 5;


		if(!is_bool($post_data) && array_filter($post_data)){
			$config['total_rows'] = $this->packagestorage->filterPackage($post_data)->num_rows();
		 	$packages = $this->packagestorage->filterPackage($post_data,$config['per_page'], $this->uri->segment(3))->result();

		}else{
			$result = $this->packagestorage->getPackages();
			if($result) {
				$config['total_rows'] = $result->num_rows();
				$packages = $this->packagestorage->getPackages(array(), $config['per_page'], $this->uri->segment(3))->result();
			} else {
				$config['total_rows'] = 0;
				$packages = array();
			}
		}

		$this->paginator($config);
		$this->panelView(null, array(
				'salutations' => array(
						'mr.' => 'Mr.',
						'ms.' => 'Ms.',
						'mrs.' => 'Mrs.',
				),
				'packages' => $packages,
				'destinations' => $destinations,
				'countries_usr' => $countries_usr,
				'user' => $user_data,
				'paging' =>  $this->pagination->create_links()
		));
	}

	public function view_packagess($code) {
		$this->load->helper('url');
		//dump_exit($code);
		if ($this->userpersistence->getPersistedUser() instanceof Customer) {
			redirect('errors/restricted_area');
		}
		$ref = (isset($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : FALSE;
		if($ref){
			$urlArr = parse_url($ref);
			$pathArr = explode('/', $urlArr['path']);

			if(@$pathArr[2] !== 'view_packages')
				$this->session->unset_userdata('filter_fields');
		}
		$this->load->model('package/package');
		$this->load->model('package/destination');
		$this->load->model('package/packagestorage');
		$this->load->model('user/user');

		// $post_data['asdf'] = 'bayot';
		$countries_usr = $this->user->getAllCountries();
		$user_data = $this->userpersistence->getPersistedUser();
		// $packages = $this->packagestorage->getPackages();
		$destinations = $this->destination->getActiveDestinations();

		//holds data from session
		// if($this->session->userdata('filter_fields') === false)
		// {
			$post_data = array('destination' => $code);
		// }else
		// {
		// 	$post_data = $this->session->userdata('filter_fields');
		// }

		$config['base_url'] = base_url('panel/view_packagess').'/'.$code;
		$config['per_page'] = 9;
		$config['num_links'] = 5;
		$config['uri_segment'] = 4;


		// if(!is_bool($post_data) && array_filter($post_data)){
			$config['total_rows'] = $this->packagestorage->filterPackage($post_data)->num_rows();
		 	$packages = $this->packagestorage->filterPackage($post_data,$config['per_page'], $this->uri->segment(4))->result();
		 	// dump_exit($packages);

		// }else{
		// 	$result = $this->packagestorage->getPackages();
		// 	if($result) {
		// 		$config['total_rows'] = $result->num_rows();
		// 		$packages = $this->packagestorage->getPackages(array(), $config['per_page'], $this->uri->segment(4))->result();
		// 	} else {
		// 		$config['total_rows'] = 0;
		// 		$packages = array();
		// 	}
		// }
		 	
		$this->session->set_userdata('filter_fields', $post_data);

		$this->paginator($config);
		$this->panelView(null, array(
				'salutations' => array(
						'mr.' => 'Mr.',
						'ms.' => 'Ms.',
						'mrs.' => 'Mrs.',
				),
				'packages' => $packages,
				'destinations' => $destinations,
				'countries_usr' => $countries_usr,
				'user' => $user_data,
				'paging' =>  $this->pagination->create_links()
		));
	}

	//function to filter from view_packages
	public function filter_view_packages_jqry(){
		$this->load->helper('url');
		$this->load->library('pagination');
		$this->load->model('package/packagestorage');
		$post_data = $this->input->post();
		// $post_data = array(
		// 	'destination' => $code
		// 	);
		// dump_exit($post_data);
		$config['base_url'] = base_url('panel/view_packages');
		$config['per_page'] = 9;
		$config['total_rows'] = $this->packagestorage->filterPackage($post_data)->num_rows();
		$config['num_links'] = 5;

		// .'?tab='.$post_data['destination'].'&'.http_build_query($_GET, '', "&")

		$this->paginator($config);
		$this->session->set_userdata('filter_fields', $post_data);

		$packages = $this->packagestorage->filterPackage($post_data,$config['per_page']);

		$data['packages'] = $packages->result();
		echo $this->pagination->create_links();
		//$this->load->view('panel/mainpanel/filtered_packages', $data, TRUE);
	}
	public function filter_view_packages_jqrys(){
		$this->load->helper('url');
		$this->load->library('pagination');
		$this->load->model('package/packagestorage');
		$post_data = $this->input->post();
		// $post_data = array(
		// 	'destination' => $code
		// 	);
		// dump_exit($post_data);
		$config['base_url'] = base_url('panel/create_booking');
		$config['per_page'] = 9;
		$config['total_rows'] = $this->packagestorage->filterPackage($post_data)->num_rows();
		$config['num_links'] = 5;
		$this->paginator($config);
		$this->session->set_userdata('filter_fields', $post_data);

		$packages = $this->packagestorage->filterPackage($post_data,$config['per_page']);
		$data['pack'] = $packages->result();
		echo $this->pagination->create_links().'_:_'.$this->load->view('panel/mainpanel/filtered_packages_booking', $data, TRUE);
	}

	public function package_edit() {
		if ($this->userpersistence->getPersistedUser() instanceof Customer) {
			redirect('errors/restricted_area');
		}
		$package_id = $this->uri->segment(3, 0);
		$this->load->helper('url');
		if(!$package_id) redirect('panel/view_packages');

		$package_hotels_new = $rawDest = array();
		$this->load->helper('form');
		$this->load->model('package/package');
		$this->load->model('package/addon');
		$this->load->model('package/packagestorage');
		$this->load->model('hotel/hotelstorage');
		$this->load->model('hotel/hotelroomrate');
		$this->load->model('surcharge/surchargestorage');
		$this->load->model('user/userstorage');
		$this->userstorage->includePending(true);

		$this->db->order_by('country','asc');
		$countries = $this->db->get('countries')->result_array();
		$this->load->model('user/user');
		$countries_usr = $this->user->getAllCountries();
		$providers = $this->userstorage->getByInformation(array('type'=>'provider'));
		$agents	= $this->userstorage->getByInformation(array('type' => 'agent'));
		$user_data = $this->userpersistence->getPersistedUser();
		$package= $this->packagestorage->getById($package_id);
		$Image_price= $this->packagestorage->getImage_price($package_id);

		$addons = $this->addon->getAddons_packageId($package_id);
		$hasbooking = ($this->db->get_where('bookings', array('package_id'=>$package_id))->num_rows() > 0);

		$surcharges = $this->surchargestorage->getSurchargeByType_id('package', $package_id, true);
		$package_hotels=$this->hotelstorage->getHotelsByPackage($package_id, true);
		$date_range	=	$this->hotelroomrate->getHotelRoomRate_packageHotelId1(148);
		//so far, getAllData can only get room rates and surcharges
		$package_hotels_new = $this->packagestorage->getAndRearrageAllData($package_hotels);

		foreach($agents as $agent){
			$result = $this->db->get_where('package_permissions', array('user_id'=>$agent->getUser()->getId(), 'package_id'=>$package_id))->row();
			if($result) $agent->getUser()->isPermitted = true;
			else $agent->getUser()->isPermitted = false;
		}

		$this->panelView(null, array(
				// 'surcharge' => clone $this->surcharge,
				'surcharges' => $surcharges,
				'salutations' => array(
						'mr.' => 'Mr.',
						'ms.' => 'Ms.',
						'mrs.' => 'Mrs.',
				),
				'providers' => $providers,
				'agents' => $agents,
				'package_hotels' => $package_hotels_new,
				'package' => @$package[0],
				'package_image_price' => $Image_price,
				'addons' => $addons,
				'countries_usr' => $countries_usr,
				'countries' => $countries,
				'user' => $user_data,
				'hasbooking' => $hasbooking,
				"package_id"=>$package_id
		));
	}


	public function fetchHotels(){
		$this->load->model('package/packagestorage');
		$this->load->model('hotel/hotelstorage');

		$package_id = $this->input->post('package_id');
		$package = $this->packagestorage->getById($package_id);
		$rawDest = $selected = array();

		if(!$package[0]->hasDestination()){
			echo json_encode(array(
				'alert_type'=>'Error',
				'info'		=>'Please select destination.'
			));
			return;
		}

		foreach($package[0]->getDestinations() as $destination){
			if(!in_array($destination->getCountryCode(), $rawDest) && $destination->getStatus()=='active')
				$rawDest[] = $destination->getCountryCode();
		}
		// $rawDest[] = 'KD';
		$this->hotelstorage->infoWhereIn(true);
		$this->hotelstorage->infoWhereInKey('country_code');
		$this->hotelstorage->infoWhereInArr($rawDest);

		$hotels = $this->hotelstorage->getByInformation(array(), TRUE, 0, 0);

		foreach($hotels as $hotel){
			$selected[] = array('id'=>$hotel->getId(), 'name'=>$hotel->getName());
		}
		if(count($selected) > 0){
			echo json_encode(array(
				'alert_type'=>'Success',
				'info'		=>$selected
			));
		} else {
			echo json_encode(array(
				'alert_type'=>'Error',
				'info'		=>'No Available Hotels'
			));
		}
	}

	public function view_hotels() {
		$this->load->model('user/user');
		$this->load->model('user/userstorage');
		$this->load->model('hotel/hotelstorage');
		$this->load->model('surcharge/surcharge');

		$this->userstorage->includePending(true); //remove this soon
		// $this->hotelstorage->includeDeleted(true);

		$this->db->order_by('country', 'ASC');
		$countries = $this->db->get('countries')->result_array();

		$config['base_url'] 	= base_url('panel/view_hotels');
		$config['total_rows'] 	= count($this->hotelstorage->getByInformation(array()));
		$config['per_page'] 	= 9;
		$config['num_links'] 	= 5;
		$this->paginator($config);

		$countries_usr 	= $this->user->getAllCountries();
		$user_data 		= $this->userpersistence->getPersistedUser();
		$agents			= $this->userstorage->getByInformation(array('type' => 'agent'));
		$hotels 		= $this->hotelstorage->getByInformation(array(), true, $config['per_page'], $this->uri->segment(3));
		$all_hotels 	= $this->hotelstorage->getByInformation(array());

		$this->panelView(array(
			'hotels' 	=> $hotels,
			'surcharge' => clone $this->surcharge,
			),
			array(
				'salutations' => array(
						'mr.' => 'Mr.',
						'ms.' => 'Ms.',
						'mrs.'=> 'Mrs.',
				),
				'countries_usr' => $countries_usr,
				'countries' 	=> $countries,
				'user' 			=> $user_data,
				'agents' 		=> $agents,
				'all_hotels' 	=> $all_hotels
			)
		);
	}

	//function to filter from view_hotels
	public function filter_hotels_jqry(){
		$this->load->model('hotel/hotelstorage');
		if($this->input->post('hotel_filter')){

			$data['hotel'] = $this->hotelstorage->getById($this->input->post('hotel_filter'));
			echo $this->load->view('panel/mainpanel/append_hotel', $data);

		}else{

			$config['base_url'] = base_url('panel/view_hotels');
			$config['total_rows'] = count($this->hotelstorage->getByInformation(array()));
			$config['per_page'] = 9;
			$config['num_links'] = 5;
			$this->paginator($config);

			$data['hotels'] = $this->hotelstorage->getByInformation(array(), true, $config['per_page'], $this->uri->segment(3));
			echo $this->pagination->create_links().'_:_'.$this->load->view('panel/mainpanel/append_hotels', $data);
		}
	}

	public function payment() {
		if ($this->userpersistence->getPersistedUser() instanceof Customer) {
			redirect('errors/restricted_area');
		}
		$this->load->model('booking/booking');
		$this->load->model('user/user');
		//$this->load->library("pagination");


		$search_array = false;
		$search_term  = false;
		$date_month   = date("F");
		$date_mon     = date("M");
		$date_m 	  = date("m");
		$date_year    = date("Y");
		$date_range = false;

		$tab = isset($_GET["tab"]) ? trim($_GET["tab"]) : "pending";

		if(isset($_GET["booked_code"])){
			if(!empty($_GET["booked_code"])){
				//$search_array["bookings.code"] = $this->input->get("booked_code",TRUE);
				$search_array["bookings.code"] = $_GET["booked_code"];
			}
		}

		if(isset($_GET["name"])){
			if(!empty($_GET["name"])){
				//$search_array["bookings.code"] = $this->input->get("booked_code",TRUE);
				$search_array["traveller_details.name"] = $_GET["name"];
			}
		}

		#check if status is set
		if(isset($_GET["status"])){
			if($_GET["status"]!="false"){
				$search_array["bookings.status"] = $_GET["status"];
			}
		}

		#check if origin is set
		if(isset($_GET["origin"])){
			if(!empty($_GET["origin"])){
				$strtotime = strtotime($_GET["origin"]);
				$date_month = date("F", $strtotime);
				$date_mon = date("M", $strtotime);
				$date_m = date("m", $strtotime);
				$date_year = date("Y", $strtotime);
				$start = date("Y-m-01", $strtotime);
				$end   = date("Y-m-t", $strtotime);
				$date_range = "'{$start}' and '{$end}'";
			}
		}

		#check if report agent id is set
		if(isset($_GET["report_agent_id"])){
			if(!empty($_GET["report_agent_id"])){
				$search_array["ACOMP.id"] = $_GET["report_agent_id"];
			}
		}

		#check if report provider is set
		if(isset($_GET["report_provider_id"])){
			if(!empty($_GET["report_provider_id"])){
				$search_array["PCOMP.id"] = $_GET["report_provider_id"];
			}
		}


		$countries_usr   = $this->user->getAllCountries();
		$user_data       = $this->userpersistence->getPersistedUser();
		//$select_alldata  = $this->booking->getAllPaymentRecords(false, false,$date_range, $search_term, $search_array);

		$select_allpending = $this->booking->getAllPendingPayments($date_range, $search_array);
		for ($i=0; $i < count($select_allpending) ; $i++) {
			$select_allpending[$i]->hotel=$this->booking->getAllBookinghotelRecord($select_allpending[$i]->BOOKID);
			//$hotel_name = $this->booking->getHotelName($select_allpending[$i]->BOOKINGCODE);
		}

		$payment_agent    = $this->booking->getByagentreport();
		$payment_provider = $this->booking->getByproviderreport();


		// foreach ($select_alldata as $key) {
		// 	$booking_id=$key->BOOKID;
		// }

		// $booked_id    = @$booking_id;
		// $select_hotel = $this->booking->getAllBookinghotelRecord($booked_id);

		$start = 0; #set the default start page to 1

		if(isset($_GET["per_page"])){
			$start = intVal($_GET["per_page"])==0 ? 0 : intVal($_GET["per_page"]);
		}

		//pass get array to a new variable
		$new_get = $_GET;
		$select_alldata_collection  = $this->booking->getAllPaymentRecords_collection();
		for ($i=0; $i < count($select_alldata_collection) ; $i++) {
			$select_alldata_collection[$i]->booked = $this->booking->getAllCollectionPaymentData($date_range, $select_alldata_collection[$i]->PID);

			//hotel
			for ($h=0; $h < count($select_alldata_collection[$i]->booked) ; $h++) {
				$select_alldata_collection[$i]->booked[$h]->hotel = $this->booking->getHotelName($select_alldata_collection[$i]->booked[$h]->BOOKINGCODE);
			}
			// for ($h=0; $h < count($select_alldata_collection[$i]->booked) ; $h++) {
			// 	$select_alldata_collection[$i]->booked[$h]->hotel = $this->booking->getAllBookinghotelRecord($select_alldata_collection[$i]->booked[$h]->BOOKID);
			// }
		}
		//dump_exit($select_allpending);
		$this->panelView(null, array(
				'salutations' => array(
						'mr.' => 'Mr.',
						'ms.' => 'Ms.',
						'mrs.' => 'Mrs.',
				),
				'countries_usr'					=> $countries_usr,
				'user'							=> $user_data,
				'booked_data'					=> $select_allpending,
				'booked_data_collection'		=> $select_alldata_collection,
				//'booked_hotel_data' =>$select_hotel,
				//"payment_page_links"			=> $this->pagination->create_links(),
				//"add_collection_page_links"		=> $this->pagination->create_links(),
				//"view_collection_page_links"	=> $this->pagination->create_links(),
				"start_page"					=> $start,
				"date_month"					=> $date_month,
				"date_mon"						=> $date_mon,
				"date_m"						=> $date_m,
				"date_year"						=> $date_year,
				"payment_agent"					=> $payment_agent,
				"payment_provider"				=> $payment_provider,
				"tab"							=> $tab
		));
	}

	public function payment_agent() {
		if ($this->userpersistence->getPersistedUser() instanceof Customer) {
			redirect('errors/restricted_area');
		}
		$this->load->model('booking/booking');
		$this->load->model('user/user');
		// $this->db->order_by('country');
		// $countries = $this->db->get('countries')->result_array();
		$countries_usr = $this->user->getAllCountries();
		$user_data = $this->userpersistence->getPersistedUser();
		$select_alldata  = $this->booking->getAllPaymentRecords();
		foreach ($select_alldata as $key) {
				$booking_id=$key->BOOKID;
			}
		$booked_id = $booking_id;
		$select_hotel  = $this->booking->getAllBookinghotelRecord($booked_id);
		//print_r($select_alldata);return false;
		$this->panelView(null, array(
				'salutations' => array(
						'mr.' => 'Mr.',
						'ms.' => 'Ms.',
						'mrs.' => 'Mrs.',
				),
				'countries_usr' => $countries_usr,
				'user' => $user_data,
				'booked_data'=>$select_alldata,
				'booked_hotel_data'=>$select_hotel
		));
	}

	public function payment_provider() {
		if ($this->userpersistence->getPersistedUser() instanceof Customer) {
			redirect('errors/restricted_area');
		}
		$this->load->model('user/user');
		$countries_usr = $this->user->getAllCountries();
		// $this->db->order_by('country');
		// $countries = $this->db->get('countries')->result_array();
		$user_data = $this->userpersistence->getPersistedUser();
		$this->panelView(null, array(
				'salutations' => array(
						'mr.' => 'Mr.',
						'ms.' => 'Ms.',
						'mrs.' => 'Mrs.',
				),
				'countries_usr' => $countries_usr,
				'user' => $user_data
		));
	}

	public function payment_pending() {
		if ($this->userpersistence->getPersistedUser() instanceof Customer) {
			redirect('errors/restricted_area');
		}
		$userid=$this->session->userdata('id');
		$usertype=$this->session->userdata('type');

		$this->load->model('booking/booking');
		$this->load->model('user/user');
		$countries_usr = $this->user->getAllCountries();
		// $this->db->order_by('country');
		// $countries = $this->db->get('countries')->result_array();
		$user_data = $this->userpersistence->getPersistedUser();

		if($usertype === 'agent') {
			$select_alldata  = $this->booking->getAllPaymentRecords_agent($userid);
		}
		else if($usertype === 'provider') {
			$select_alldata  = $this->booking->getAllPaymentRecords_provider($userid);
		}

		for ($i=0; $i < count($select_alldata) ; $i++) {
			$select_alldata[$i]->hotel=$this->booking->getAllBookinghotelRecord($select_alldata[$i]->BOOKID);
		}

		$userID_agent = $user_data->getUser()->getId();
		$query=$this->booking->getBysummaryPricelistcsv($userID_agent);
 		$airport_map=$this->booking->getBysummaryPdfairport();

		$date_month     = date("F");
		$date_m 	  = date("m");
		$date_year    = date("Y");

		$this->panelView(null, array(
				'salutations' => array(
						'mr.' => 'Mr.',
						'ms.' => 'Ms.',
						'mrs.' => 'Mrs.',
				),
				'date_month' => $date_month,
				'date_m' => $date_m,
				'date_year' => $date_year,
				'countries_usr' => $countries_usr,
				'user' => $user_data,
				'booked_data'=>$select_alldata,
				'csv'=>$query,
				'airport_map'=>$airport_map
		));
	}

	public function rate() {
		if ($this->userpersistence->getPersistedUser() instanceof Customer) {
			redirect('errors/restricted_area');
		}
		$this->load->model('package/packagestorage');
		$this->load->model('package/package');
		$this->load->model('package/destination');
		$this->load->model('hotel/hotel');
		$this->load->model('booking/booking');
		$this->load->model('user/user');
		$this->load->model('surcharge/surchargestorage');

		$countries_usr = $this->user->getAllCountries();
		$this->db->order_by('id','asc');
		$hotels = $this->db->get('hotels')->result_array();
		$user_data = $this->userpersistence->getPersistedUser();

		$packageid = $this->input->post('package_id');
		$hotel_name_id = $this->input->post('hotel_name_id');
		$datein = $this->input->post('date_in');
		$dateout = $this->input->post('date_out');
		$summary_data = $this->booking->getBysummaryPricelist($packageid,$hotel_name_id,$datein,$dateout);

		// $pkg_srchg = $this->surchargestorage->getSurchargeByType_id("package",$pkg_id, TRUE);
		// $hotel_srchg = $this->surchargestorage->getSurchargeByType_id("hotel",$hotel_id, TRUE);
		// $hotel_rm_srchg = $this->surchargestorage->getSurchargeByType_id("hotelroom",$hotel_rm_id, TRUE);

		$list_srchg = array();
		$list_hotel = $this->booking->getBysummaryCheckRatePackageHotel($packageid, $hotel_name_id);
		for ($i=0; $i < count($list_hotel); $i++) {
			$list_srchg[$i] = new stdClass();

			$list_srchg[$i]->pkg = $this->surchargestorage->getSurchargeByType_id("package",$list_hotel[$i]->PACK_ID, TRUE);
			$list_srchg[$i]->htl = $this->surchargestorage->getSurchargeByType_id("hotel",$list_hotel[$i]->PACK_HOTEL_ID, TRUE);

			if ($datein && $dateout) {
				$surcharges_pkg = array("packages"=>$list_srchg[$i]->pkg);
				$surcharges_htl = array("hotels"=>$list_srchg[$i]->htl);

				$dates = array("return"=>$dateout,"depart"=>$datein);

				// $list_srchg[$i]->pkg = $this->surchargestorage->processSurchargePackage($surcharges_pkg, $dates);
				// $list_srchg[$i]->htl = $this->surchargestorage->processSurchargeHotels($surcharges_htl, $dates);

				$tmp_pkg_surchg = $this->surchargestorage->processSurchargePackage($surcharges_pkg, $dates);
				$count_test_pkg = count($tmp_pkg_surchg);
				$tmp_pkg_surchg_list = array();
				if($count_test_pkg!=0){
					foreach ($tmp_pkg_surchg as $categ) {
						foreach ($categ as $surcharge) {
							$tmp_pkg_surchg_list[]=$surcharge;
						}
					}
				}
				$list_srchg[$i]->pkg = $tmp_pkg_surchg_list;

				$tmp_htl_surchg = $this->surchargestorage->processSurchargeHotels($surcharges_htl, $dates);
				$count_test_htl = count($tmp_htl_surchg);
				$tmp_htl_surchg_list = array();
				if($count_test_htl!=0){
					foreach ($tmp_htl_surchg as $categ) {
						foreach ($categ as $surcharge) {
							$tmp_htl_surchg_list[]=$surcharge;
						}
					}
				}
				$list_srchg[$i]->htl = $tmp_htl_surchg_list;

			}

			$list_hotel[$i]->hotelroom = $this->booking->getBysummaryCheckRateHotelRoom($list_hotel[$i]->PACK_HOTEL_ID);
			$list_srchg[$i]->hrm = array();
			for ($hrm=0; $hrm < count($list_hotel[$i]->hotelroom); $hrm++) {
				@$tmp_var = $list_hotel[$i]->hotelroom[$hrm];

				if ($datein && $dateout) {
					$tmp_hrm_list = $this->surchargestorage->getSurchargeByType_id("hotelroom",$tmp_var->HRM_ID, TRUE);

					$tmp_hrm = $this->surchargestorage->getRoomRateSurcharge_id($tmp_var->HRM_ID);
					if ($tmp_hrm !== FALSE) {
						$surcharges_hrm = array("hotelrooms"=>$tmp_hrm);
						$dates = array("return"=>$dateout,"depart"=>$datein);

						$tmp_hrm_surchg = $this->surchargestorage->processSurchargeHotelRooms($surcharges_hrm, $dates);

						$count_test = count($tmp_hrm_surchg);

						$tmp_hrm_surchg_rm = array();
						if($count_test!=0){
							foreach ($tmp_hrm_surchg as $categ) {
								foreach ($categ as $surcharge) {
									$tmp_hrm_surchg_rm[]=$surcharge;
								}
							}
						}
						$list_srchg[$i]->hrm[$hrm] = $tmp_hrm_surchg_rm;
					}
				}
				else {
					$list_srchg[$i]->hrm[] = $this->surchargestorage->getSurchargeByType_id("hotelroom",$tmp_var->HRM_ID, TRUE);
				}

			}
		}

		$userID_agent = $user_data->getUser()->getId();
		$query=$this->booking->getBysummaryPricelistcsv($userID_agent);
		$packages = $this->booking->getBypackageIdPermission($userID_agent);
		$destinations = $this->destination->getActiveDestinations();
		$airport_map=$this->booking->getBysummaryPdfairport();
		$this->panelView(null, array(
				'salutations' => array(
						'mr.' => 'Mr.',
						'ms.' => 'Ms.',
						'mrs.' => 'Mrs.',
				),
				'user' => $user_data,
				'countries_usr'=>$countries_usr,
				'hotels' => $hotels,
				'room_summary'=>$summary_data,
				'list_hotel'=>$list_hotel,
				'list_srchg'=>$list_srchg,
				'packages'=>$packages,
				'csv'=>$query,
				'airport_map'=>$airport_map,
				'destinations'=>$destinations
			)
		);
	}

	public function package_hotel_(){
		$this->load->model('booking/booking');
		$data['hotel_name_'] = $this->booking->package_hotel_($this->input->post('id'));
		$this->load->view('panel/mainpanel/append_hotels_name',$data);
	}

	public function package_hotel_2(){
		$this->load->model('booking/booking');
		$data['hotel_name_'] = $this->booking->package_hotel_($this->input->post('id'));
		echo json_decode($data['hotel_name_'][0]->days,true);
	}

	public function destination_(){
		$this->load->model('booking/booking');
		$this->load->model('package/packagestorage');
		$data['packages'] = $this->packagestorage->getDestinationPackages($this->input->post('code'));
		$this->load->view('panel/mainpanel/append_destination_rates',$data);
	}

	public function attachment() {
		if ($this->userpersistence->getPersistedUser() instanceof Customer) {
			redirect('errors/restricted_area');
		}
		// print_r($this->session->userdata);
		$this->load->model('hotel/hotelstorage');
		$this->load->model('user/user');
		$countries_usr = $this->user->getAllCountries();
		$this->db->order_by('country','asc');
		$countries = $this->db->get('countries')->result_array();
		$user_data = $this->userpersistence->getPersistedUser();
		$destination_link=$this->hotelstorage->getDestination_Link();
		$this->panelView(null, array(
				'salutations' => array(
						'mr.' => 'Mr.',
						'ms.' => 'Ms.',
						'mrs.' => 'Mrs.',
				),
				'countries' => $countries,
				'countries_usr' => $countries_usr,
				'countries' => $countries,
				'user' => $user_data,
				'link_destination'=>$destination_link,
		));
	}


	public function create_booking() {
		if ($this->userpersistence->getPersistedUser() instanceof Customer) {
			redirect('errors/restricted_area');
		}
		$ref = (isset($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : FALSE;
		if($ref){
			$urlArr = parse_url($ref);
			$pathArr = explode('/', $urlArr['path']);

			if(@$pathArr[2] !== 'create_booking')
				$this->session->unset_userdata('filter_fields');
		}

		$this->load->model('booking/booking');
		$this->load->model('package/package');
		$this->load->model('package/destination');
		$this->load->model('package/packagestorage');
		$this->load->model('user/user');
		$countries_usr = $this->user->getAllCountries();
		//holds data from session

		$user_data = $this->userpersistence->getPersistedUser();
		$userID_agent = $user_data->getUser()->getId();

		//contains data from form of last filter.
		$destinations = $this->destination->getActiveBookingDestinations($userID_agent);

		//dump_exit($destinations[0]['code']);
		if($this->session->userdata('filter_fields') === false)
		{
			$post_data = array('destination' => @$destinations[0]['code']);
		}else
		{
			$post_data = $this->session->userdata('filter_fields');
		}
		// echo "<pre>";
		// print_r($post_data);
		$hotels = $this->destination->getActiveBookingHotels($userID_agent);
		$config['base_url'] = base_url('panel/create_booking');
		$config['per_page'] = 9;
		$config['num_links'] = 5;

		//dump($userID_agent); die;

		if ($post_data) {
			$config['total_rows'] = $this->packagestorage->filterPackage_agent($userID_agent,$post_data)->num_rows();
		 	$packages = $this->packagestorage->filterPackage_agent($userID_agent,$post_data,$config['per_page'], $this->uri->segment(3));
		} else {
			$config['total_rows'] = $this->packagestorage->getPackages_agent(NULL, NULL, NULL, $userID_agent)->num_rows();
			$packages = $this->packagestorage->getPackages_agent(array(), 9, $this->uri->segment(3), $userID_agent);
		}
		//dump_exit($packages->result());

		$query=$this->booking->getBysummaryPricelistcsv($userID_agent);
		$airport_map=$this->booking->getBysummaryPdfairport();
		$this->paginator($config);
		$this->panelView(null, array(
				'salutations' => array(
						'mr.' => 'Mr.',
						'ms.' => 'Ms.',
						'mrs.' => 'Mrs.',
				),
				'user_id'	=> $userID_agent,
				'destinations' => $destinations,
				'hotels' => $hotels,
				'packages' => $packages->result(),
				'countries_usr' => $countries_usr,
				'user' => $user_data,
				'csv'=>$query,
				'airport_map'=>$airport_map,
				'first_code' => @$destinations[0]['code'],
				'paging' =>  $this->pagination->create_links()
		));
	}

	public function create_bookings($code) {
		if ($this->userpersistence->getPersistedUser() instanceof Customer) {
			redirect('errors/restricted_area');
		}
		$ref = (isset($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : FALSE;
		if($ref){
			$urlArr = parse_url($ref);
			$pathArr = explode('/', $urlArr['path']);

			if(@$pathArr[2] !== 'create_booking')
				$this->session->unset_userdata('filter_fields');
		}

		$this->load->model('booking/booking');
		$this->load->model('package/package');
		$this->load->model('package/destination');
		$this->load->model('package/packagestorage');
		$this->load->model('user/user');
		$countries_usr = $this->user->getAllCountries();
		//holds data from session

		$user_data = $this->userpersistence->getPersistedUser();
		$userID_agent = $user_data->getUser()->getId();

		//contains data from form of last filter.
		$destinations = $this->destination->getActiveBookingDestinations($userID_agent);

		//dump_exit($destinations[0]['code']);
		// if($this->session->userdata('filter_fields') === false)
		// {
		// 	$post_data = array('destination' => $destinations[0]['code']);
		// }else
		// {
		// 	$post_data = $this->session->userdata('filter_fields');
		// }
		$post_data = array('destination' => $code);
		// echo "<pre>";
		// print_r($post_data);
		$hotels = $this->destination->getActiveBookingHotels($userID_agent);
		$config['base_url'] = base_url('panel/create_bookings').'/'.$code;
		$config['per_page'] = 9;
		$config['num_links'] = 5;
		$config['uri_segment'] = 4;

		//dump($userID_agent); die;

		// if ($post_data) {
			$config['total_rows'] = $this->packagestorage->filterPackage_agent($userID_agent,$post_data)->num_rows();
		 	$packages = $this->packagestorage->filterPackage_agent($userID_agent,$post_data,$config['per_page'], $this->uri->segment(4));
		// } else {
		// 	$config['total_rows'] = $this->packagestorage->getPackages_agent(NULL, NULL, NULL, $userID_agent)->num_rows();
		// 	$packages = $this->packagestorage->getPackages_agent(array(), 9, $this->uri->segment(3), $userID_agent);
		// }
		//dump_exit($packages->result());

		$this->session->set_userdata('filter_fields', $post_data);
		$query=$this->booking->getBysummaryPricelistcsv($userID_agent);
		$airport_map=$this->booking->getBysummaryPdfairport();
		$this->paginator($config);
		$this->panelView(null, array(
				'salutations' => array(
						'mr.' => 'Mr.',
						'ms.' => 'Ms.',
						'mrs.' => 'Mrs.',
				),
				'user_id'	=> $userID_agent,
				'destinations' => $destinations,
				'hotels' => $hotels,
				'packages' => $packages->result(),
				'countries_usr' => $countries_usr,
				'user' => $user_data,
				'csv'=>$query,
				'airport_map'=>$airport_map,
				'first_code' => $destinations[0]['code'],
				'paging' =>  $this->pagination->create_links()
		));
	}

	//static pa, will make this dynamic soon...
	//for create_booking ra
	//kirstin
	public function clearSession(){
		//where to redirect
		$page = $this->uri->segment(3, '');
		$field = FALSE;

		if ($page == 'create_booking')
			$field = 'filter_fields';

		if ($field)
			$this->session->unset_userdata('filter_fields');

		redirect('panel/'.$page);
	}

	public function clearSessions(){
		//where to redirect
		$page = $this->uri->segment(3, '');
		$field = FALSE;

		if ($page == 'view_packages')
			$field = 'filter_fields';

		if ($field)
			$this->session->unset_userdata('filter_fields');

		redirect('panel/'.$page);
	}

	//function to filter from create_booking
	public function filter_packages_jqry(){
		$this->load->library('pagination');
		$this->load->model('package/packagestorage');
		$post_data = $this->input->post();
		$user_data = $this->userpersistence->getPersistedUser();
		$userID_agent = $user_data->getUser()->getId();

		$config['base_url'] = base_url('panel/create_booking');
		$config['per_page'] = 9;
		$config['total_rows'] = $this->packagestorage->filterPackage_agent($userID_agent, $post_data)->num_rows();
		$config['num_links'] = 5;

		$this->paginator($config);

		$this->session->set_userdata('filter_fields', $post_data);

		$packages = $this->packagestorage->filterPackage_agent($userID_agent, $post_data,$config['per_page']);
		$data['pack'] = $packages->result();
		echo $this->pagination->create_links().'_:_'.$this->load->view('panel/mainpanel/filtered_packages_booking', $data, TRUE);
	}

	// booking transation
	public function step_booking($package_id = false,$booking_id = false) {
		if ($this->userpersistence->getPersistedUser() instanceof Customer) {
			redirect('errors/restricted_area');
		}
		$this->load->model("booking/bookingstorage");

		if(!$package_id) return;

		#booking data
		$booking_data = null;
		if($booking_id){
			$booking_data = $this->bookingstorage->getBookingInformation($booking_id);

		}


		#load models ----------------------------------------------u+
		$this->load->model('booking/booking');
		$this->load->model('user/userstorage');
		$this->load->model('package/package');
		$this->load->model('package/packagestorage');
		$this->load->model('hotel/hotelstorage');
		$this->load->model('surcharge/surchargestorage');
		$this->load->model('package/addon');
		$this->load->model('user/user');

		#fetch package relevent information ----------------------------------------------
		$countries_usr = $this->user->getAllCountries();

		#$package_hotels = $this->hotelstorage->getHotelsByPackage($package_id);

		$this->db->order_by('country','asc');
		$this->userstorage->includePending(true);
		$countries = $this->db->get('countries')->result_array();
		$user_data = $this->userpersistence->getPersistedUser();
		$package = $this->packagestorage->getById($package_id);
		$days = $this->packagestorage->getByDays($package_id);
		$addons = $this->addon->getAddons_packageId($package_id);
		$destinations = $this->packagestorage->getDestinations($package_id);


		#get the package addons ----------------------------------------------
		if(count($addons)){
			for($i=0;$i<count($addons);$i++){
				$addon_book = $this->addon->getAddons_ID_QTY($addons[$i]["id"], $booking_id);
				$addons[$i]["user_qty"] = @$addon_book->quantity;
			}
		}

		#get the package hotels ----------------------------------------------
		$package_hotels = array();
		if(count($destinations)){
			foreach($destinations as $dest){
				$destination_id = $dest['id'];
				//$package_hotels[] = $this->hotelstorage->getHotelsByPackageAndCode($dest['code'],$dest["package_id"]);
				$package_hotels[] = $this->hotelstorage->getHotelsByPackage($dest["package_id"],TRUE);
			}
		}

		#get package surcharge ----------------------------------------------
		$pkg_srchg = $this->surchargestorage->getSurchargeByType_id("package",$package_id, TRUE);
		//dump($pkg_srchg);
		$pkg_surcharge_count = count($pkg_srchg);
		//dump($pkg_surcharge_count);
		$pkg_surcharge_price=0;
		$pkg_srchg_id = array();

		#get package surcharge info ----------------------------------------------
		if($pkg_surcharge_count!=0){
			foreach ($pkg_srchg as $pkg) {
				$pkg_surcharge_price += $pkg['cost']+$pkg['profit'];
				$pkg_srchg_id[] = array(
					'id' => $pkg['id'],
					'profit'=> $pkg['profit'],
					'cost' => $pkg['cost'],
					'rate_type' => $pkg['rate_type']
				);
			}
		}

		#set session package id ----------------------------------------------
		$this->session->set_userdata('package_id',$package_id);

		//dump($package[0]);
		

		#load the panel view with the pertinent data ----------------------------------------------
		$userID_agent = $user_data->getUser()->getId();
		$query=$this->booking->getBysummaryPricelistcsv($userID_agent);
		$airport_map=$this->booking->getBysummaryPdfairport_sort($destinations[0]['code']);
		$this->panelView(null, array(
				'salutations' => array(
						'mr.' => 'Mr.',
						'ms.' => 'Ms.',
						'mrs.' => 'Mrs.',
				)
				,'package_hotels' 	=> $package_hotels
				,'countries_usr' 		=> $countries_usr
				,'package' 			=> @$package[0]
				,'user' 			=> $user_data
				,'addons' => $addons
				,"package_surcharge" => $pkg_surcharge_price
				,"package_surcharge_id" => $pkg_srchg_id
				,"destinations" => $destinations
				,"booking_data" => $booking_data[0]
				,"pkg_srchg" => $pkg_srchg
				,'csv'=>$query,
				'airport_map'=>$airport_map,
				'days'			=>	$days,
				'package_id'	=>	$package_id
		));
	}

	public function step_hotel() {
		if ($this->userpersistence->getPersistedUser() instanceof Customer) {
			redirect('errors/restricted_area');
		}
		$olah = array();
		$data = $this->input->post();
		$this->load->model('user/user');
		$countries_usr = $this->user->getAllCountries();
		$total_days = $this->package->checkValidRange($data['depart_date'],$data['return_date']);
		if(isset($data['child_date']))
			$olah = $this->booking->getNumberTodsAndInfs($data['child_date']);

		$data['child_date'] = $olah;
		$data['total_days'] = $total_days;
		$data['package_days'] = $package[0]->getDays();
		$data['extended_days'] = 0;
		if($total_days > $package[0]->getDays())
			$data['extended_days'] = $total_days - $package[0]->getDays();

		$package_hotels = $this->hotelstorage->getHotelsByPackage($package_id);
		$addons = $this->addon->getAddons_packageId($package_id);
		// $this->db->order_by('country');
		// $countries = $this->db->get('countries')->result_array();
		$user_data = $this->userpersistence->getPersistedUser();
		$this->session->set_userdata('step_booking',$data);
		// print_r($data);die;
		$this->panelView(null, array(
			'salutations' => array(
					'mr.' => 'Mr.',
					'ms.' => 'Ms.',
					'mrs.' => 'Mrs.',
			),
			'package_hotels'=>$package_hotels,
			'package'=>$package[0],
			'addons' => $addons,
			'prev_details' => $this->session->userdata['step_booking'],
			'countries_usr' => $countries_usr,
			'user' => $user_data,
			'bookid'=>$this->input->post('book_id'),
		));
	}

	// ajax function get hotel room surcharges

	public function send_mail(){
		if ($this->userpersistence->getPersistedUser() instanceof Customer) {
			redirect('errors/restricted_area');
		}
		$post = $this->input->post('email');

		if($post){
		    $this->load->model('booking/booking');
			$sender_email = (valid_email($post[0])) ? $post[0] : null;
			$sender_name = trim($post[1]);
			$sender_subject = trim($post[2]);
			$sender_message = (trim($post[3]) != '') ? $post[3] : null;
			// if(null){$sender_name = trim($post[1]);
			if($sender_message && $sender_email){
		        $mail = $this->mail;
		    	$mail->setFrom('no-reply@ge.com.sg', $sender_name);						// Sender
		    	$mail->addReplyTo($sender_email, $sender_name);							// Sender's optional]
				$recipients=$this->booking->getEmailadmin();
				if($recipients != NULL || $recipients != ""){
					foreach($recipients as $row){
						$admin_email=$row->adminemail;
						$mail->addAddress($admin_email, 'Global Explorer');				// Add a recipient (Admin)
					}
				}
			    $mail->WordWrap = 50;													// Set word wrap to 50 characters
			    $mail->isHTML(true);													// Set email format to HTML
			    $mail->Subject = $sender_subject;
			    $mail->Body    = $sender_message;
			    // $mail->Body    = $this->load->view('booking_confirmation_sample',$data,TRUE);

		    	$mail->AltBody = $sender_message;


				//if done sending
				if($mail->send()){
					/*echo json_encode(array(
						'alert_type'=>'Success',
						'message'=>'Email Sent.'
					));*/
					redirect('panel');
				}else{
					echo json_encode(array(
						'alert_type'=>'Error',
						'message'=>'Unable to send your message. Please try again.'
					));
				}
			} else {
				echo json_encode(array(
					'alert_type'=>'Error',
					'message'=>'Unable to send mail. Please enter valid mail and/or message.'
				));
			}
		}
	}

	public function gethotelroom_packagehotelid(){
    	#initialize needed models ----------------------------------------------
    	$this->load->model('hotel/hotelroomrate');
    	$this->load->model("surcharge/surchargestorage");
    	$this->load->model("hotel/hotelstorage");



		$depart_value = trim(date('Y-m-d',strtotime(str_replace('-', ' ' , $_POST['depart_value']))));
		$return_value = trim($_POST['return_value']);
		$index = trim($_POST['destination_index']);
		$total_destinations = trim($_POST["total_destinations"]);

    	#get inputs through ajax ----------------------------------------------
    	$package_hotel_id = $this->input->post('package_hotel_id'); #get the input post

    	if($package_hotel_id==" "){

    	}

    	$package_hotel_ids = explode(".", $package_hotel_id); #expldoe the x.x format of hotel ids
    	$pkg_hotel_id = $package_hotel_ids[0];	#base primary key of hotel
    	$pkg_hotel_rel = (int) $package_hotel_ids[1]; #relationship of hotel and package

    	$package_id = $this->input->post("package_id"); #get package id



    	$exp = explode(".", $package_hotel_id); #explode the string with a '.' delimiter

    	$package_hotel_id = $exp[0]; #relating id between hotel and package
    	$package_hotel_id_rel = $exp[1]; #id of package independent of package//primary key


		//$package_hotel_rooms = $this->hotelroomrate->getHotelRoomRate_packageHotelIdAgent($package_id, $package_hotel_id_rel, TRUE); #fetch the hotel room rates based on the joining relationship of hotel and package
		$package_hotel_rooms = $this->hotelroomrate->getHotelRoomRate_packageHotelIdAgent1($package_id, $package_hotel_id_rel, TRUE);

		$hotel_surcharges = $this->surchargestorage->getSurchargeByType_id("hotel",$package_hotel_id,TRUE); #fetch surcharge of hotel


    	// print_r(count($package_hotel_rooms) > 0);
    	// if(count($package_hotel_rooms)){
    	$data["hotel_surcharges"] = $hotel_surcharges;
    	$data['depart_value'] = $depart_value;
    	$data['return_value'] = $return_value;
    	$data['destination_index'] = $index;
    	$data['total_destinations'] = $total_destinations;
    	$data['booking_id']	= trim($_POST['booking_id']);
    	$data['hotel_id'] = $pkg_hotel_id;
    	#dump_exit($package_hotel_rooms);

    	#check if packate hotel rooms exist
    	#check if package has hotel room
    	if(!empty($package_hotel_rooms)){
    		//$data['roomrates'] = $package_hotel_rooms;
    		foreach($package_hotel_rooms['standard'] as $r){
				$result	=	$this->hotelroomrate->getHotelRoomRateRange($r['package_hotel_id'],$r['room_type'],$r['description'],$r['rate_type'],$depart_value);
				if($result){
					foreach($result as $res){
						$rooms['standard'][] = $res;
					}
				}
				else{
					$rooms['standard'][] = $r;
				}
				
			}
			$data['roomrates']	=	$rooms;
    		echo 'view_:_'.$this->load->view('panel/mainpanel/booking_roomchoices', $data);
    	}
    	else{
    		$data["none_table"] = true; #pass this to check if there is a table
 			echo "blank_:_".$this->load->view('panel/mainpanel/booking_roomchoices', $data);
   		}
    }

    /*
    *ajax function get hotel room surcharges
    */

    /**
    * COLLECT/PAYMENTS MOTHAFUCKA! STARTS
    */
    public function collect() {
    	if ($this->userpersistence->getPersistedUser() instanceof Customer) {
			redirect('errors/restricted_area');
		}
    	$this->load->model('booking/payment');
    	$this->load->model("booking/booking");
    	$value = explode('IamADelimiter',$this->input->post('data'));
  		$booking_id = $this->input->post("booking_id");
    	$data = array('booking_id'=>$value[0],
    				  'status'=>'paid',
    				  'amountpaid'=>$value[1]
    		         );

    	$q = $this->payment->insert_payment($data);
    	if($q){
    		$search_array["bookings.id"] = $booking_id;
	  		$select_alldata  = $this->booking->getAllPaymentRecords(false, false,false, false, $search_array);
	  		$row = $select_alldata[0];
			foreach ($select_alldata as $key) {
				$booking_id=$key->BOOKID;
			}
			$booked_id    = @$booking_id;
			$booked_hotel_data = $this->booking->getAllBookinghotelRecord($booked_id);
			$tmp=array();
            foreach ($booked_hotel_data as $key){
                $tmp[]=$key->hotels_name;
            }
	  		echo "<tr>
                <td>
                    <a href=".base_url('panel/voucher/'.$row->BOOKID)." target=\"_blank\">".$row->BOOKINGCODE."</a> <br>
                    <a href=".base_url('panel/package/'.$row->BOOKID).">View</a>
                </td>
                <td>
                    <strong>Package:</strong> ".$row->PACKAGENAME."<br>
                    <strong>Agent:</strong> ".$row->COMPANYNAME."<br>
                    <strong>Provider:</strong> ".$row->COMPANYNAMEP."<br>
                    <strong>Hotel:</strong> ". implode(', ',$tmp)."<br>
                    <strong>Traveller:</strong> ".$row->TRAVELLLERNAME."
                </td>
                <td>
                    <strong>Ori:</strong> ".$row->BOOKINGDEPARTUREDATE."<br>
                    <i>".$row->OFDEPARTTIME."-".$row->OFARRIVALTIME." hrs</i> <br>
                    <strong>Ret:</strong> ".$row->BOOKINGRETURNDATE."<br>
                    <i>".$row->RFDEPARTTIME."-".$row->RFARRIVALTIME." hrs</i>
                </td>
                <td>".$row->BOOKINGCOST."</td>
                <td>".$row->PDATE."</td>
            </tr>";
	  	}
    	else{
    		echo 'wala na save';
    	}
    }
    /**
    * COLLECT/PAYMENTS MOTHAFUCKA! ENDS
    */

    public function update_user_status() {
    	if ($this->userpersistence->getPersistedUser() instanceof Customer) {
			redirect('errors/restricted_area');
		}
   		$this->load->model('user/userstorage');
   		$id 	= $this->input->post('user_id');
   		$action = $this->input->post('action');

   		switch($action){
   			case 'Delete':
   				$status = 'deleted';
   			break;
   			case 'Activate':
   				$status = 'active';
   			break;
   			case 'Deactivate';
   				$status = 'inactive';
   			break;
   		}
   		$result = $this->userstorage->updateUserStatus_byId($id,$status);
   		echo $status;
    }

    /*
    *append new optional hotel
    */
    public function getOptionalHotel(){
    	$this->load->model('hotel/hotelstorage');

    	#get inputs through ajax ----------------------------------------------
    	$package_hotel_id = $this->input->post('package_hotel_id'); #get the input post
    	$country_code = $this->input->post('country_code');

    	$package_hotel_ids = explode(".", $package_hotel_id); #expldoe the x.x format of hotel ids
    	$pkg_hotel_id = $package_hotel_ids[0];	#base primary key of hotel
    	$pkg_hotel_rel = (int) $package_hotel_ids[1]; #relationship of hotel and package

    	$result1 = "";

       	$this->db->select('hotel_id,package_id');
        $this->db->from('package_hotels');
        $this->db->where('package_hotels.id',$pkg_hotel_rel);
        $result=$this->db->get()->result_array();

        if(count($result)> 0) {
	        $this->db->select('package_hotels.id AS package_hotel_id, name as hotel_name');
	        $this->db->from('package_hotels');
	        $this->db->join('hotels','hotels.id=package_hotels.hotel_id','LEFT');;
	        $this->db->where('package_hotels.hotel_id !=',$result[0]['hotel_id']);
	        $this->db->where('package_hotels.package_id =',$result[0]['package_id']);
	        $this->db->where("hotels.country_code = ", $country_code);
	        $this->db->order_by("name", "ASC");
	        $result1=$this->db->get()->result_array();
		}


		if(count($result1)){
			echo "<option value=''>- Select -</option>";
			for($i=0;$i<count($result1);$i++){
				echo "<option value='{$result1[$i]['package_hotel_id']}' >{$result1[$i]['hotel_name']}</option>";
			}
		}
    }
    /*
    *append new optional hotel
    */

    /*
    *generate surcharges
    */
    public function getSurcharges(){
    	$package_hotel_id = $this->input->post('package_hotel_id');
    	$package_id = $this->input->post('package_id');
    	$dates['depart'] = $this->input->post('depart_date');
    	$dates['return'] = $this->input->post('return_date');

    	// if(!$package_hotel_id) return false;

    	$data = array();
    	$this->load->model('hotel/hotelstorage');
    	$this->load->model('surcharge/surchargestorage');

    	$hotel_id = $this->hotelstorage->getHotelOfPackageHotel($package_hotel_id);
		$surcharge['packages'] = $this->surchargestorage->getSurchargeByType_id('package', $package_id);
    	$surcharge['hotels'] = $this->surchargestorage->getSurchargeByType_id('hotel',$hotel_id);
    	$surcharge['hotelrooms'] = $this->surchargestorage->getSurchargeByType_id('hotelroom',$package_hotel_id);
    	$data['surcharges'] = $this->surchargestorage->processSurcharge($surcharge, $dates);

    	return $this->load->view('panel/mainpanel/booking_surcharges', $data, TRUE);
    }
    /*
    *generate surcharges
    */



    /*
    *redirected from panel/booking_json
    */
	public function step_details() {
		$this->load->database();
		$this->load->model('booking/booking');
		$this->load->model('package/package');
		$this->load->model('package/packagestorage');
		$this->load->model("booking/bookingstorage");
		$addon_model = $this->load->model("package/addon");

		$data["prev_details"]["booking_id"] = @$_POST["booking_id"];
		$data["prev_details"]["booking_code"] = @$_POST["booking_code"];

		$data["prev_details"]["package_id"] = $_POST['package_id'];

		$data["prev_details"]['package_days'] =  $_POST['package_days'];
		$data["prev_details"]["extended_days"] = $_POST["extended_days"];

		//track extended days
		if($this->input->post("extended_days", TRUE) !== $this->input->post("extended_days1", TRUE))
		{
			$data["prev_details"]["track_ext_days"] = "Extension Dates Changed";
		}

		$data["prev_details"]["infant_cnt"] = isset($_POST["infant_cnt"]) ?  $this->input->post("infant_cnt", TRUE):0;
		$data["prev_details"]["adult_count"] = isset($_POST["adult_count"]) ?  $this->input->post('adult_count', TRUE):0;
		$data["prev_details"]["child_count"] = isset($_POST["child_count"]) ?  $this->input->post('child_count', TRUE):0;
		$data["prev_details"]["total_holder"] = $_POST["total_holder"];

		//track pax number
		if($this->input->post("infant_cnt", TRUE) !== $this->input->post("infant_cnt1", TRUE) || $this->input->post('adult_count', TRUE) !== $this->input->post('adult_count1', TRUE) || $this->input->post('child_count', TRUE) !== $this->input->post('child_count1', TRUE) )
		{
			$data["prev_details"]["track_pax_num"] = "No. of Pax Changed";
		}

		$data["prev_details"]["depart_date"] =  $this->input->post("depart_date", TRUE);
		$data["prev_details"]["return_date"] =  $this->input->post("return_date", TRUE);

		//track travel dates
		if($this->input->post("depart_date", TRUE) !== $this->input->post("depart_date1", TRUE) || $this->input->post("return_date", TRUE) !== $this->input->post("return_date1", TRUE))
		{
			$data["prev_details"]["track_travel_dates"] = "Travel Dates Changed";
		}

		$data["prev_details"]["package_hotel_choice"] =  $_POST["package_hotel_choice"];
		#$data["prev_details"]["package_hotel_name"] = $_POST["package_hotel_name"];

		//track hotel
		if($this->input->post("hotelid", TRUE) !== $this->input->post("hotelid1", TRUE))
		{
			$data["prev_details"]["track_hotel"] = "Hotel Changed";
		}

		$data["prev_details"]["roomrate_id"] =  $_POST["roomrate_id"];
		#$data["prev_details"]["roomrate_price"] = $_POST['roomrate_price'];
		$data["prev_details"]["roomrate_pax"] =  $_POST['roomrate_pax'];
		#$data["prev_details"]["roomrate_quantity"] = $_POST['roomrate_quantity'];
		#$data["prev_details"]["roomrate_rate_type"] = $_POST['roomrate_rate_type'];
		$data["prev_details"]["roomrate_id_surcharges"] =  $_POST['roomrate_surcharges'];

		$data["prev_details"]["child_date"] = isset($_POST['child_date']) ?  $_POST['child_date']:0;

		$data["prev_details"]["roomrate_quantity"] =  $_POST["roomrate_quantity"];

		#package surcharge meta data
		$data["prev_details"]["package_surcharge_id"] = @$_POST["pkg_surcharge_id"];
		#$data["prev_details"]["pkg_surcharge_description"] = @$_POST['pkg_surcharge_description'];
		#$data["prev_details"]["pkg_surcharge_rule"] = @$_POST['pkg_surcharge_rule'];
		#$data["prev_details"]["pkg_surcharge_rule_type"] = @$_POST['pkg_surcharge_rule_type'];
		#$data["prev_details"]["pkg_surcharge_profit"] = @$_POST['pkg_surcharge_profit'];
		#$data["prev_details"]["pkg_surcharge_cost"] = @$_POST['pkg_surcharge_cost'];
		#$data["prev_details"]["pkg_surcharge_price"] = @$_POST['pkg_surcharge_price'];


		#hotel surcharge metadata
		$data["prev_details"]["hotel_surcharge_id"] = @$_POST["hotel_surcharge_id"];
		#$data["prev_details"]["hotel_surcharge_rule_type"] = @$_POST['hotel_surcharge_rule_type'];
		#$data["prev_details"]["hotel_surcharge_profit"] = @$_POST['hotel_surcharge_profit'];
		#$data["prev_details"]["hotel_surcharge_cost"] = @$_POST['hotel_surcharge_cost'];
		#$data["prev_details"]["hotel_surcharge_price"] = @$_POST['hotel_surcharge_price'];
		#$data["prev_details"]["hotel_surcharge_rule"] = @$_POST['hotel_surcharge_rule'];

		#get addon for the package ----------------------------------------------
		$data["prev_details"]["addon_id"] = isset($_POST["addon_id"]) ? $_POST["addon_id"]:0; #check if addon is present
		$data["prev_details"]["addon_quantity"] = isset($_POST["addon_quantity"]) ?  $this->input->post('addon_quantity', TRUE):0;
		#$data["prev_details"]["addon_desc"] = isset($_POST['addon_desc']) ? $_POST['addon_desc']: "";
		#$data["prev_details"]["addon_price"] = isset($_POST['addon_cost']) ? intVal($_POST['addon_cost'])+intVal($_POST['addon_profit']) : 0;
		#$data["prev_details"]["addon_unit"] = isset($_POST['addon_unit']) ? $_POST["addon_unit"]: "";

		#get total days,payments,cost,and profit ----------------------------------------------
		$data["prev_details"]["total_payment"] =  $_POST['package_total_payment'];
		$data["prev_details"]["total_cost"] = $_POST['package_total_cost'];
		$data["prev_details"]["total_profit"] = $_POST['package_total_profit'];
		$data["prev_details"]["total_days"] = (int)$_POST['package_days']+(int)$_POST['extended_days'];

		#extension remarks ----------------------------------------------
		$data["prev_details"]["exremarks"] = isset($_POST["exremarks"]) ? $this->input->post('exremarks',TRUE):"";

		#additional hotel information ----------------------------------------------
		$data["prev_details"]["exhotel_remarks"] = @$this->input->post('exhotel_remarks', TRUE);
		$data["prev_details"]["exhotel_name"] = @$_POST['exhotel_name'];
		$data["prev_details"]["exhotel_name_full"] = @$_POST['exhotel_name_full'];

		// $dataarr['a'] = array(@$_POST['add_hotel_name2'],@$_POST['add_hotel_name3']);
		// $data_arr = array(@$_POST['additional_checkin2'],@$_POST['additional_checkout2']);
		// $data_arr1 = array(@$_POST['additional_checkin3'],@$_POST['additional_checkout3']);
		// $dataarr['b'] = array($data_arr,$data_arr1);
		// dump(json_encode($dataarr['a']));
		// dump_exit(json_encode($dataarr['b']));
		$data["prev_details"]["exhotel_name_add"] = @$_POST['add_hotel_name2'].','.@$_POST['add_hotel_name3'];
		$data["prev_details"]["exhotel_chkin_chkout"] = @$_POST['additional_checkin2'].':'.@$_POST['additional_checkout2'].','.@$_POST['additional_checkin3'].':'.@$_POST['additional_checkout3'];


		// $data["prev_details"]["exhotel_name_add"] = json_encode(array(@$_POST['add_hotel_name2'],@$_POST['add_hotel_name3']));
		// $data["prev_details"]["exhotel_chkin_chkout"] = json_encode(array('FirstChck'=> array(@$_POST['additional_checkin2'],@$_POST['additional_checkout2']),'SecondChck'=>array(@$_POST['additional_checkin3'],@$_POST['additional_checkout3'])));
		// dump_exit($data["prev_details"]["exhotel_name_add"],$data["prev_details"]["exhotel_chkin_chkout"]);
		$data["prev_details"]["room_count"] = $_POST['room_count'];
		#$data['prev_details']['package_total_surcharge'] = $_POST['package_total_surcharge'];
		#$data['prev_details']['package_hotel_total_surcharge'] = $_POST['package_hotel_total_surcharge'];
		#$data['prev_details']['package_room_total_surcharges'] = $_POST['package_room_total_surcharges'];

		#destination block ----------------------------------------------
		$data["prev_details"]["destination_id"] = @$_POST["destination_id"];
		#$data["prev_details"]["destination_country"] = @$_POST["destination_country"];
		#$data["prev_details"]["destination_nights"] = @$_POST["destination_nights"];
		$data["prev_details"]["bd_destination_hotel"] = @$_POST["bd_destination_hotel"];

		#pkg surcharge summary
		$data["prev_details"]["bd_pkg_sur_description"] = @$_POST["bd_pkg_sur_description"];
		$data["prev_details"]["bd_pkg_sur_computation"] = @$_POST["bd_pkg_sur_computation"];
		$data["prev_details"]["bd_pkg_sur_subtotal"] = @$_POST["bd_pkg_sur_subtotal"];

		#destination summary
		$data["prev_details"]["bd_destination_id"] = @$_POST["bd_destination_id"];
		$data["prev_details"]["bd_destination_hotel"] = @$_POST["bd_destination_hotel"];

		#hotel summary
		$data["prev_details"]["bd_hotel_sur_description"] = @$_POST["bd_hotel_sur_description"];
		$data["prev_details"]["bd_hotel_sur_computation"] = @$_POST["bd_hotel_sur_computation"];
		$data["prev_details"]["bd_hotel_sur_subtotal"] = @$_POST["bd_hotel_sur_subtotal"];

		#room surcharge summary
		$data["prev_details"]["bd_rs_description"] = @$_POST["bd_rs_description"];
		$data["prev_details"]["bd_rs_computation"] = @$_POST["bd_rs_computation"];
		$data["prev_details"]["bd_rs_subtotal"] = @$_POST["bd_rs_subtotal"];

		#room summary
		$data["prev_details"]["bd_room_description"] = @$_POST["bd_room_description"];
		$data["prev_details"]["bd_room_computation"] = @$_POST["bd_room_computation"];
		$data["prev_details"]["bd_room_subtotal"] = @$_POST["bd_room_subtotal"];

		#addon summary
		$data["prev_details"]["bd_addon_description"] = @$_POST["bd_addon_description"];
		$data["prev_details"]["bd_addon_computation"] = @$_POST["bd_addon_computation"];
		$data["prev_details"]["bd_addon_subtotal"] = @$_POST["bd_addon_subtotal"];

		#extensions summary
		$data["prev_details"]["bd_ext_description"] = @$_POST["bd_ext_description"];
		$data["prev_details"]["bd_ext_computation"] = @$_POST["bd_ext_computation"];
		$data["prev_details"]["bd_ext_subtotal"] = @$_POST["bd_ext_subtotal"];

		$this->session->set_userdata('step_booking',$data['prev_details']);
		#end ofinitialize the needed data ----------------------------------------------


		#get countries ----------------------------------------------
		$this->db->order_by('country');
		$countries = $this->db->get('countries')->result_array();
		$this->db->order_by('name');
		$airportcodes = $this->db->get('airportcodes')->result_array();


		#set session package id ----------------------------------------------
		$wang=$this->session->userdata('step_booking');
		$this->session->set_userdata("package_id",$wang['package_id']);


		$traveller_details = false;
		$booking_data = false;
		#get traveller details
		if($data["prev_details"]["booking_id"]!=null){
			$traveller_details = $this->bookingstorage->getTravellerDetails($data["prev_details"]["booking_id"]);
			$booking_data = $this->bookingstorage->getBookingInformation($data["prev_details"]["booking_id"]);
		}

		#get package id ----------------------------------------------
		$package_id = $this->session->userdata('package_id');

		#get package information ----------------------------------------------
		$package = $this->packagestorage->getById($package_id);

		#get user data ----------------------------------------------
		$user_data = $this->userpersistence->getPersistedUser();
		$all_airportcodes = array();


		#concatenate the country code and id as per request ----------------------------------------------
		foreach($airportcodes as $airportcode){
			if(!in_array($airportcode['name'], $all_airportcodes)) {
				$all_airportcodes[$airportcode['code'].".".$airportcode['id']] = $airportcode['name'];
			}
		}

		$days = $this->packagestorage->getByDays($package_id);


		#panel view load ----------------------------------------------
		$userID_agent = $user_data->getUser()->getId();
		$query=$this->booking->getBysummaryPricelistcsv($userID_agent);
 		$airport_map=$this->booking->getBysummaryPdfairport();
		$this->panelView(null, array(
				'salutations' => array(
						'mr.' => 'Mr.',
						'ms.' => 'Ms.',
						'mrs.' => 'Mrs.',
				),
				'airportcodes' => $all_airportcodes,
				'prev_details' => $this->session->userdata('step_booking'),
				'countries' => $countries,
				'package'=>@$package[0],
				'user' => $user_data,
				'traveller_details' => $traveller_details,
				"booking_data" => $booking_data[0],
				"addon_model" => $addon_model,
				'csv'=>$query,
				'airport_map'=>$airport_map,
				'days'		=> $days
		));
		#end of panel view load ----------------------------------------------
	}

	public function change_details_agent() {
		if ($this->userpersistence->getPersistedUser() instanceof Customer) {
			redirect('errors/restricted_area');
		}
		// print_r($_POST);die;

		$this->load->database();
		$this->load->model('package/package');
		$this->load->model('package/packagestorage');

		$this->db->order_by('country');
		$countries = $this->db->get('countries')->result_array();
		$this->db->order_by('name');
		$airportcodes = $this->db->get('airportcodes')->result_array();

		$this->session->set_userdata('step_hotel',$_POST);
		$bookid=$this->input->post('booking_id');
		$package_id = $this->input->post('packageid');
		$package = $this->packagestorage->getById($package_id);
		$user_data = $this->userpersistence->getPersistedUser();
		$all_airportcodes = array();

		foreach($airportcodes as $airportcode){
			if(!in_array($airportcode['name'], $all_airportcodes)) {
				$all_airportcodes[$airportcode['id']] = $airportcode['name'];
			}
		}

		$this->panelView(null, array(
				'salutations' => array(
						'mr.' => 'Mr.',
						'ms.' => 'Ms.',
						'mrs.' => 'Mrs.',
				),
				'airportcodes' => $all_airportcodes,
				'prev_details' => $this->session->userdata['step_booking'],
				'countries' => $countries,
				'package'=>$package[0],
				'user' => $user_data,
				'booking_id'=>$bookid,
				'packageid'=>$package_id
		));
	}


	/*
	*
	*Start booking function
	*
	*/
	public function booking() {
		if ($this->userpersistence->getPersistedUser() instanceof Customer) {
			redirect('errors/restricted_area');
		}
		$this->load->library("security");
		
		$step_booking = unserialize(@$_POST["step_booking_serialized"]);
		unset($_POST['step_booking_serialized']);


		$new_tmp_array = array_merge($step_booking,$this->security->xss_clean($_POST));
		$this->session->set_userdata("step_detail", $new_tmp_array);

		//dump_exit($new_tmp_array);
		$this->load->model('booking/bookingstorage');
		$this->load->model('package/packagestorage');
		$this->load->model('package/package');
		$this->load->model('user/userstorage');
		$this->load->model('hotel/hotelstorage');
		$this->load->model('package/addon');
		$this->load->model('booking/booking');
		$this->load->model('hotel/hotelroomrate');
		$this->load->model('surcharge/surchargestorage');
		$booking_code_=array();
		$user_data = $this->userpersistence->getPersistedUser();
		$package_id = $this->session->userdata('package_id');
		$step_booking = $this->session->userdata('step_detail');
		$step_details = $this->session->userdata('step_detail');
		$package = $this->packagestorage->getById($package_id);
		$flight_of_from = $step_details['flight_of_from'];
		$of_from = explode('.',$flight_of_from);
		$of_from_code = $of_from['0']; 
		$of_from_id = $of_from['1'];
		$flight_rf_from = $step_details['flight_rf_from'];
		$rf_from = explode('.',$flight_rf_from);
		$rf_from_code = $rf_from['0']; 
		$rf_from_id = @$rf_from['1'];
		$flight_of_to = $step_details['flight_of_to'];
		$of_to = explode('.',$flight_of_to);
		$of_to_code = $of_to['0']; 
		$of_to_id = $of_to['1'];
		$flight_rf_to = @$step_details['flight_rf_to'];
		$rf_to = explode('.',$flight_rf_to);
		$rf_to_code = $rf_to['0']; 
		$rf_to_id = @$rf_to['1'];  
		//$wang=date('Ymd',strtotime($step_booking['depart_date']));
		$default_string='GE';
		$ref_num = $this->bookingstorage->getRefnum();
		//$bookingcode=$default_string.$wang.$ref_num[0]->code;
		$bookingcode=$default_string.$ref_num[0]->code;
		$booking['user_id'] = $user_data->getUser()->getId(); #get user id // current agent
		$booking['package_id'] = $package_id; #get selected package id
		$booking['departure_date'] = date('Y-m-d',strtotime(str_replace('-', ' ' , $step_booking['depart_date']))); #departure date from the dp
		$booking['return_date'] = date('Y-m-d',strtotime(str_replace('-', ' ' , $step_booking['return_date']))); #return date from the dp
		$booking['rf_num'] = $step_details['flight_rf_no'];
		$booking['rf_from'] = $rf_from_id;
		$booking['rf_to'] = $rf_to_id;
		$booking['rf_departuretime'] = date('H:i:s',strtotime($step_details['flight_rf_date_departure']));
		$booking['rf_arrivaltime'] = date('H:i:s',strtotime($step_details['flight_rf_date_arrival'])); 
		$booking['extended_days'] = $step_details['extended_days']; 
		$booking['rf_remarks'] = $step_details['flight_remarks'];
		$booking['of_num'] = $step_details['flight_of_no'];
		$booking['of_from'] = $of_from_id;
		$booking['of_to'] = $of_to_id;
		$booking['of_departuretime'] =  date('H:i:s',strtotime($step_details['flight_of_date_departure']));
		$booking['of_arrivaltime'] = date('H:i:s',strtotime($step_details['flight_of_date_arrival']));
		$booking['of_remarks'] = $step_details['flight_remarks'];
		$booking['total_cost'] = $step_booking['total_cost'];
		$booking['total_profit'] = $step_booking['total_profit'];
		$booking['total_price'] =  $step_booking['total_payment'];
		$booking['agent_name'] = $step_booking['agent_details_name'];
		$booking['agent_ref'] = $step_booking['agent_details_no'];
		$booking['status'] = 'pending';
		$booking['confirmation_remarks'] = 'Booking details has been sent to the Administrator and Provider. Please wait for confirmation.';
		$booking['date_booked'] = date('Y-m-d');
		$booking['timestamp'] = time();
		$hotelname = $step_booking['package_hotel_choice'];
		$amendedcode=$step_booking['booking_code'];
		$booking['exhotel_id']=$step_booking['exhotel_name'];
		if($step_booking['booking_code'] != NULL || $step_booking['booking_code'] != ''){
			$booking['code'] = $amendedcode; #amended code based on the algorithm
			$match=$this->bookingstorage->getUser_Id($booking['code']);
			$booking['user_id'] =$match[0]['user_id'];
			$booking_code_=$booking['code'];
			$booking['status'] = 'ammended';
			$result = $this->bookingstorage->addBooking($booking);
			$booking_status = 'amended';
			$return_result_id=$result['id'];
		}else{
			$booking['code'] = $bookingcode; #generate code based on the algorithm
			$booking['user_id'] = $user_data->getUser()->getId(); #get user id // current agent
			$booking_code_=$booking['code'];
			$this->db->set('code','code+1',false);
            $this->db->where('id',1);
            $this->db->update('reference_number'); 
			$result = $this->bookingstorage->addBooking($booking);
			$booking_status = 'pending';
			$return_result_id=$result['id'];
		}
		$admin = $package[0]->getAdmin();
		$provider = $package[0]->getProvider();
		$agent = $user_data;
		if($return_result_id != NULL && $return_result_id != '' && $return_result_id != 0)
		{
			/*
			*insert into the summary table
			*/
			$summary_data["destination_id"] = $step_details["destination_id"];
			$summary_data["bd_destination_hotel"] = $step_details["bd_destination_hotel"];
			$summary_data["bd_pkg_sur_description"] = $step_details["bd_pkg_sur_description"];
			$summary_data["bd_pkg_sur_computation"] = $step_details["bd_pkg_sur_computation"];
			$summary_data["bd_pkg_sur_subtotal"] = $step_details["bd_pkg_sur_subtotal"];
			$summary_data["bd_hotel_sur_description"] = $step_details["bd_hotel_sur_description"];
			$summary_data["bd_hotel_sur_computation"] = $step_details["bd_hotel_sur_computation"];
			$summary_data["bd_hotel_sur_subtotal"] = $step_details["bd_hotel_sur_subtotal"];
			$summary_data["bd_rs_description"] = $step_details["bd_rs_description"];
			$summary_data["bd_rs_computation"] = $step_details["bd_rs_computation"];
			$summary_data["bd_rs_subtotal"] = $step_details["bd_rs_subtotal"];
			$summary_data["bd_room_description"] = $step_details["bd_room_description"];
			$summary_data["bd_room_computation"] = $step_details["bd_room_computation"];
			$summary_data["bd_room_subtotal"] = $step_details["bd_room_subtotal"];
			$summary_data["bd_addon_description"] = $step_details["bd_addon_description"];
			$summary_data["bd_addon_computation"] = $step_details["bd_addon_computation"];
			$summary_data["bd_addon_subtotal"] = $step_details["bd_addon_subtotal"];
			$summary_data["bd_ext_description"] = $step_details["bd_ext_description"];
			$summary_data["bd_ext_computation"] = $step_details["bd_ext_computation"];
			$summary_data["bd_ext_subtotal"] = $step_details["bd_ext_subtotal"];
			$summary_data["bd_ext_remark"] = $step_details["exremarks"];
			$this->bookingstorage->insertSummaryBookings($summary_data,$return_result_id);
			/*
			*end of insert into the summary table
			*/

			//*Start add bookhotels*//
			foreach ($hotelname as $key) {
				$tmp_hotel = explode('.',$key);
				$hotelname_arr= $tmp_hotel['1'];
				$booking2['booking_id'] =$return_result_id;
				$booking2['package_hotel_id'] = $hotelname_arr;//set as default
				$booking2['checkin_date'] = date('Y-m-d',strtotime(str_replace('-', ' ' , $step_booking['depart_date'])));;
				$booking2['checkout_date'] = date('Y-m-d',strtotime(str_replace('-', ' ' , $step_booking['return_date'])));;
				$booking2['remarks'] = $step_booking['exhotel_remarks'];
				$booking2['ext_hotel_name'] = $step_booking['exhotel_name_add'];
				$booking2['ext_hotel_chkin_chkout'] = $step_booking['exhotel_chkin_chkout'];
				$booking2['status'] ='pending';
				$result2=$this->bookingstorage->addBookHotels($booking2);
			}
			//*End add bookhotels*//


			/*
			*Start insert database
			*/
			$count_room=count($step_booking['roomrate_quantity']);
			$room=$step_booking['roomrate_quantity'];
			$roomid=$step_booking['roomrate_id'];
			@$roomprice=$step_booking['roomrate_price'];
			$roompax=$step_booking['roomrate_pax'];
			$roomsurcharges=$step_booking['roomrate_id_surcharges'];
			$hotelsurcharges=$step_booking['hotel_surcharge_id'];
			$packagesurcharges=$step_booking['package_surcharge_id'];
			for($i=0;$i<count($room);$i++){
				if(!empty($room[$i])){
					for ($ii=0; $ii < count($roomid); $ii++){ 
						if($i == $ii){
							for ($iii=0; $iii < count($roomid[$ii]); $iii++){
								if(!empty($room[$ii][$iii])){
									$data_room=$room[$ii][$iii];
									$data_arr_=$return_result_id;
									$data_arr=$result2['id'];
									$data_roomid=$roomid[$ii][$iii];
									$data_roomsurcharges=$roomsurcharges[$ii][$iii];
									@$data_hotelsurcharges=$hotelsurcharges[$ii][$iii];
									@$data_packagesurcharges=$packagesurcharges[$ii][$iii];
									$this->bookingstorage->addBookHotelrooms($data_roomid,$data_arr,$data_room);
									if($data_hotelsurcharges != 0){
										$this->bookingstorage->addappliedhotelsurcharges($data_hotelsurcharges,$data_arr,$data_room);
									}
									if($data_packagesurcharges != 0){
										$this->bookingstorage->addappliedpackagesurcharges($data_packagesurcharges,$data_arr_,$data_room);
									}
									if($roomsurcharges[$ii][$iii] != 0){
										$tmp_arr=explode(",",$roomsurcharges[$ii][$iii]);
										foreach ($tmp_arr as $key) {
											$data_arr_=$key;
											$this->bookingstorage->addappliedhotelroomsurcharges($data_arr_,$data_arr,$data_room);
										}
									}
								}
							}
						}
					}
				}
			}
			/*
			*
			*End insert database
			*
			*/
			//*Start add addappliedaddons*//
			if($step_booking['addon_quantity'] != NULL && $step_booking['addon_quantity'] !=0 && $step_booking['addon_quantity'] !=''){
				foreach($step_booking['addon_quantity'] as $k => $qty){
					if($qty != '' && $step_booking['addon_id'][$k] != ''){	
						$appliedaddons['booking_id'] =$return_result_id;
						$appliedaddons['addon_id'] = $step_booking['addon_id'][$k]; //set as default
						$appliedaddons['quantity'] = $qty;
						$result6=$this->bookingstorage->addappliedaddons($appliedaddons);
					}
				}
			}
			//*End add addappliedaddons*//
			//*Start add addtravellerdetails*//
			if(@$step_details['adult_name'] != 0 && @$step_details['adult_name'] !=''){
				foreach($step_details['adult_name'] as $k => $qty){
					if($qty != '' && $step_details['adult_name'][$k] != ''){
						$travellerdetails['booking_id']=$return_result_id;
						$travellerdetails['user_id'] =$this->session->userdata('id');
						$travellerdetails['name'] = @$step_details['adult_name'][$k];
						$travellerdetails['date_of_birth'] = date('Y-m-d',strtotime(str_replace('-', ' ' ,@$step_details['adult_dob'][$k])));
						$travellerdetails['passport_no'] = @$step_details['adult_ppno'][$k];
						$travellerdetails['type'] ="Adult";
						$travellerdetails['status'] ='0';
						$this->bookingstorage->addtravellerdetails($travellerdetails);

						// if(@$step_details['adult_name'][$k] !== @$step_details['adult_name1'][$k] || @$step_details['adult_dob'][$k] !== @$step_details['adult_dob1'][$k] || @$step_details['adult_ppno'][$k] !== @$step_details['adult_ppno1'][$k])
						// {
						//	$tracks['passenger_details'] = @$step_details['adult_name'][$k];
						
					}
				}
		    }
			if(@$step_details['child_name'] != 0 && @$step_details['child_name'] !=''){
				foreach($step_details['child_name'] as $k => $qty){
					if($qty != '' && $step_details['child_name'][$k] != ''){
						$travellerdetails['booking_id']=$return_result_id;
						$travellerdetails['user_id'] =$this->session->userdata('id');
						$travellerdetails['name'] = @$step_details['child_name'][$k];
						$travellerdetails['passport_no'] = @$step_details['child_ppno'][$k];
						$travellerdetails['date_of_birth'] = date('Y-m-d',strtotime(str_replace('-', ' ' ,@$step_details['child_dob'][$k])));
						$travellerdetails['type'] ="Child";
						$travellerdetails['status'] ='0';
						$this->bookingstorage->addtravellerdetails($travellerdetails);
					}
				}
			}
			if(@$step_details['infant_name'] != 0 && @$step_details['infant_name'] !=''){
				foreach($step_details['infant_name'] as $k => $qty){
					if($qty != '' && $step_details['infant_name'][$k] != ''){
						$travellerdetails['booking_id']=$return_result_id;
						$travellerdetails['user_id'] =$this->session->userdata('id');
						$travellerdetails['name'] = @$step_details['infant_name'][$k];
						$travellerdetails['passport_no'] = @$step_details['infant_ppno'][$k];
						$travellerdetails['date_of_birth'] = date('Y-m-d',strtotime(str_replace('-', ' ' ,@$step_details['infant_dob'][$k])));
						$travellerdetails['type'] ="Infant";
						$travellerdetails['status'] ='0';
						$this->bookingstorage->addtravellerdetails($travellerdetails);
					}
				}
		    }
			/*}*/
			//*End add addtravellerdetails*//
			$conc = '';
			$booking = array();
			$tracks = array();
			$booking['step_booking'] = $step_booking;
			$booking['user'] = $user_data;
			$booking['package'] = $package;
			$booking['admin'] = $admin;
			$booking['provider'] = $provider;
			$booking_ids = $this->booking->getBookingIDs($booking_code_);
			$booking_summary = false;
			$booking_summary_provider = false;


			if(@$step_details['adult_name'] != 0 && @$step_details['adult_name'] !='')
			{
				foreach($step_details['adult_name'] as $x => $qty)
				{
					// dump_exit(@$step_details['adult_dob'][$x]);
					if(@$step_details['adult_name'][$x] !== @$step_details['adult_name1'][$x] 
						|| @$step_details['adult_dob'][$x] !== date('d-M-Y',strtotime(@$step_details['adult_dob1'][$x] ))
						|| @$step_details['adult_ppno'][$x] !== @$step_details['adult_ppno'][$x])
					{
						$track['passenger_detail'] = 'Passengers Details Changed';
					}
				}
			}

			if(@$step_details['child_name'] != 0 && @$step_details['child_name'] !='')
			{
				foreach($step_details['child_name'] as $x => $qty)
				{
					if(@$step_details['child_name'][$x] !== @$step_details['child_name1'][$x]
						|| @$step_details['child_dob'][$x] !== date('d-M-Y',strtotime(@$step_details['child_dob1'][$x]))
						|| @$step_details['child_ppno'][$x] !== @$step_details['child_ppno1'][$x])
					{
						$track['passenger_detail'] = 'Passengers Details Changed';
					}
				}
			}

			if(@$step_details['infant_name'] != 0 && @$step_details['infant_name'] !='')
			{
				foreach($step_details['infant_name'] as $k => $qty)
				{
					if(@$step_details['infant_name'][$x] !== @$step_details['infant_name1'][$x]
						|| @$step_details['infant_dob'][$x] !== date('d-M-Y',strtotime(@$step_details['infant_dob1'][$x]))
						|| @$step_details['infant_ppno'][$x] !== @$step_details['infant_ppno1'][$x])
					{
						$track['passenger_detail'] = 'Passengers Details Changed';
					}
				}
			}

			
			if($this->input->post('flight_of_date_departure') !== date ('H:i',strtotime($this->input->post('flight_of_date_departure1')))
				|| $this->input->post('flight_of_date_arrival') !== date ('H:i',strtotime($this->input->post('flight_of_date_arrival1')))
				|| $this->input->post('flight_rf_date_departure') !== date ('H:i',strtotime($this->input->post('flight_rf_date_departure1')))
				|| $this->input->post('flight_rf_date_arrival') !== date ('H:i',strtotime($this->input->post('flight_rf_date_arrival1'))) )
			{
				$track['flight_detail'] = 'Flight Details Changed';
			}

			if($this->input->post('flight_of_no') !== $this->input->post('flight_of_no1') 
				|| $this->input->post('flight_rf_no') !== $this->input->post('flight_rf_no1'))
			{
				$track['flight_detail'] = 'Flight Details Changed';
			}

			if($this->input->post('of_from') !== $this->input->post('of_from1') 
				|| $this->input->post('rf_from') !== $this->input->post('rf_from1')
				|| $this->input->post('of_to') !== $this->input->post('of_to1')
				|| $this->input->post('rf_to') !== $this->input->post('rf_to1') )
			{
				$track['flight_detail'] = 'Flight Details Changed';
			}

			if($this->input->post('flight_remarks') !== $this->input->post('flight_remarks1') )
			{
				$track['flight_detail'] = 'Flight Details Changed';
			}

			$tracks['num_pax'] = $step_booking['track_pax_num'];
			$tracks['ext_days'] = $step_booking['track_ext_days'];
			$tracks['travel_dates'] = $step_booking['track_travel_dates'];
			$tracks['hotel'] = $step_booking['track_hotel'];
			$tracks['passenger_detail'] = $track['passenger_detail'];
			$tracks['flight_detail'] = $track['flight_detail'];
				

			if(count($booking_ids)){
				for($i=0;$i<count($booking_ids);$i++){
					if($booking_status === 'amended')
					{
						$booking_summary[] = $this->getSummaryViewByEmail_agent($booking_ids[$i]["id"],$tracks);
						$booking_summary_provider[] = $this->getSummaryViewByEmail_provider($booking_ids[$i]["id"],$tracks);
					}
					else
					{
						$booking_summary[] = $this->getSummaryViewByEmail_agent($booking_ids[$i]["id"],'');
						$booking_summary_provider[] = $this->getSummaryViewByEmail_provider($booking_ids[$i]["id"],'');
					}
					
				}
			}
			$booking['ge_booking_code'] = $booking_code_;
			$booking['ge_booking_status'] = $booking_status;
			$booking['booking_id_mapper'] = $return_result_id;
			$booking['booking_summary'] = $booking_summary;
			$booking['booking_summary_provider'] = $booking_summary_provider;
			$booking_stored=1;
			if($booking_stored){//una nga email
				$this->load->helper('string');
				$toHash = random_string('alpha', 10);
				$hash = hashmonster($toHash);
				$this->db->insert('confirmation_booking', array(
					'booking_id' => $return_result_id,
					'hash' => $hash
				));
				$link = base_url('change/confirm_booking/'.$hash);
				if($this->booking_confirmation($link,$booking) > 0 ){
					$conc .= ' Booking details has been sent to the Agent and the Administrator. Please wait for confirmation.';
				}
				if($this->booking_confirmation_provider($link,$booking) > 0){
					$conc .= ' Booking details has been sent to the Provider. Please wait for confirmation.';
				}
				redirect('panel/package/'.$return_result_id);
			}
    		echo json_encode(array(
				'alert_type'=>'Success',
				'message'=>'Successfully added.'
		 	));
		}else{
    		echo json_encode(array(
		 		'alert_type'=>'Error',
		 		'message'=>$result
		 	));
    	}
	}

	/*
	*
	*End booking function
	*
	*/

    public function booking_confirmation_provider($link='', $booking){
	    if(!$link) return;
	    $booking['confirm'] = $link.'/confirm';
	    $booking['hotel'] = $link.'/hotel';
	    $booking['room'] = $link.'/room';
	    $booking['reject'] = $link.'/reject';
	    $booking['cancel'] = $link.'/cancel';
		$this->load->model('booking/booking');
        $mail_provider=$booking['booking_id_mapper'];
        //dump_exit($mail_provider);
		$recipients=$this->booking->getEmailadmin();
		$get_provider_email=$this->booking->getEmailprovider($mail_provider);
		$get_provider_company = $this->db->select('*')->from('companies')->join('company_designations', 'companies.id = company_designations.company_id')->where(array('company_designations.user_id'=>$get_provider_email[0]->providerid))->get()->result();
     	$cnt = 0;
     	//dump_exit($get_provider_company);

        $mail = $this->mail;
    	$mail->setFrom('op.globalexplorer@gmail.com', 'Global Explorer');						// Add a recipient

    	$mail->WordWrap = 50;                                 							// Set word wrap to 50 characters
	    $mail->isHTML(true);                                  							// Set email format to HTML
	    if($booking['ge_booking_status'] == 'amended'){
	    $mail->Subject = ''.$booking['ge_booking_code'].' [amended] Global Explorer Booking Notification';
	    }else if($booking['ge_booking_status'] == 'pending'){
	    $mail->Subject = ''.$booking['ge_booking_code'].' [pending] Global Explorer Booking Notification';
	    }
	    if($get_provider_email != NULL || $get_provider_email != ""){
		    //foreach ($get_provider_email as $key) {
	    		$mail->ClearAddresses();
				$mail->ClearCCs();
				$booking['recipient'] =	 $get_provider_company[0]->name;
	 			$booking['recipient_id'] = $get_provider_email[0]->providerid;
	     		if($get_provider_email[0]->alteremail != "" || $get_provider_email[0]->alteremail != NULL) {
					$wang=explode(',',$get_provider_email[0]->alteremail);
		    		for ($i=0; $i < count($wang); $i++) {
			     		$mail->addCC($wang[$i],$booking['recipient']);
			     		//$mail->addCC('delabahan@gmail.com',$booking['recipient']);
					}
				}
	     		$mail->addAddress($get_provider_email[0]->provideremail,$booking['recipient']);
	     		//$mail->addAddress('delabahan@gmail.com',$booking['recipient']);
		    	$mail->Body    = $this->load->view('booking_confirmation_provider',$booking,TRUE);
			    if($mail->send()) $cnt++;
			//}
		}

     	if($recipients != NULL || $recipients != ""){
     		foreach($recipients as $row){
	    		$mail->ClearAddresses();
				$mail->ClearCCs();
				$get_company = $this->db->select('*')->from('companies')->join('company_designations', 'companies.id = company_designations.company_id')->where(array('company_designations.user_id'=>$row->adminid))->get()->result();
	     		$booking['recipient'] = $get_company[0]->name;
	     		$booking['recipient_id'] = $row->adminid;
	     		if($row->alteremail != '' || $row->alteremail != NULL) {
			     	$alternative_email=explode(',',$row->alteremail);
			     	for ($i=0; $i < count($alternative_email); $i++) {
			     		$mail->addCC($alternative_email[$i],$booking['recipient']);
			     		//$mail->addCC('delabahan@gmail.com',$booking['recipient']);
					}
			    }
		    	$mail->addAddress($row->adminemail, $booking['recipient']);     		// Add a recipient (Admin)
		    	//$mail->addAddress('delabahan@gmail.com', $booking['recipient']);     		// Add a recipient (Admin)
	    		$mail->Body    = $this->load->view('booking_confirmation_provider',$booking,TRUE);
		    	if($mail->send()) $cnt++;
     		}
     	}
     	$mail->Body    = $this->load->view('booking_confirmation_provider',$booking,TRUE);
    	$mail->AltBody = 'Booking Confirmation [Global Explorer]';
		//if($mail->send());
     	return $cnt;
    }

    public function booking_confirmation($link='', $booking){
	    if(!$link) return;
     	$mail_agent=$booking['booking_id_mapper'];
		$this->load->model('booking/booking');
		$recipients=$this->booking->getEmailadmin();
		$get_agent_email=$this->booking->getEmailagent($mail_agent);
		$company=$this->booking->getEmailagent($mail_agent);
		$company_info = $this->db->select('*')->from('companies')->join('company_designations', 'companies.id = company_designations.company_id')->where(array('company_designations.user_id'=>$company[0]->agentid))->get()->result();
     	$cnt = 0;
        $mail = $this->mail;
    	$mail->setFrom('op.globalexplorer@gmail.com', 'Global Explorer');						// Sender

	    $mail->WordWrap = 50;                                 							// Set word wrap to 50 characters
	    $mail->isHTML(true);
	   	if($booking['ge_booking_status'] == 'amended'){
	    $mail->Subject = ''.$booking['ge_booking_code'].' [amended] Global Explorer Booking Notification';
	    }else if($booking['ge_booking_status'] == 'pending'){
	    $mail->Subject = ''.$booking['ge_booking_code'].' [pending] Global Explorer Booking Notification';
	    }                               							// Set email format to HTML
	    if($get_agent_email != NULL || $get_agent_email != ""){
		   // foreach ($get_agent_email as $key) {
	    		$mail->ClearAddresses();
				$mail->ClearCCs();
				//$booking['recipient'] =$get_agent_email[0]->agentfname.' '.$get_agent_email[0]->agentlname;
				$booking['recipient'] = $company_info[0]->name;
	     		if($get_agent_email[0]->alteremail != "" || $get_agent_email[0]->alteremail != NULL) {
					$wang=explode(',',$get_agent_email[0]->alteremail);
		    		for ($i=0; $i < count($wang); $i++) {
			     		$mail->addCC($wang[$i],$booking['recipient']);
			     		//$mail->addCC('delabahan@gmail.com',$booking['recipient']);
					}
					// $mail->addCC('edmundwan@yahoo.com','Edmund Wan');
					// $mail->addCC('op.globalexplorer@gmail.com','Edmund Wan');
				}
	     		$mail->addAddress($get_agent_email[0]->agentemail,$booking['recipient']);
	     		// dump_exit($get_agent_email[0]->agentemail);
	     		// $mail->addAddress('edmundwan@yahoo.com','Edmund Wan');
	     		// $mail->addAddress('op.globalexplorer@gmail.com','Edmund Wan');
	     		//dump_exit($booking);
		    	$mail->Body    = $this->load->view('booking_confirmation',$booking,TRUE);

		
			    if($mail->send()) $cnt++;
			//}
		}
     	if($recipients != NULL || $recipients != ""){
     		foreach($recipients as $row){
	    		$mail->ClearAddresses();
				$mail->ClearCCs();
				$company_info = $this->db->select('*')->from('companies')->join('company_designations', 'companies.id = company_designations.company_id')->where(array('company_designations.user_id'=>$row->adminid))->get()->result();
	     		$booking['recipient'] = $company_info[0]->name;
	     		//$booking['recipient'] = $row->adminfname.' '.$row->adminlname;
	     		$booking['recipient_id'] = $row->adminid;
	     		if($row->alteremail != '' || $row->alteremail != NULL) {
			     	$alternative_email=explode(',',$row->alteremail);
		    		for ($i=0; $i < count($alternative_email); $i++) {
			     		$mail->addCC($alternative_email[$i],$booking['recipient']);
			     		//$mail->addCC('delabahan@gmail.com',$booking['recipient']);
					}
			    }
		    	$mail->addAddress($row->adminemail, $booking['recipient']);     		// Add a recipient (Admin)
		    	//$mail->addAddress('delabahan@gmail.com', $booking['recipient']);     		// Add a recipient (Admin)
	    		$mail->Body    = $this->load->view('booking_confirmation',$booking,TRUE);
		    	if($mail->send()) $cnt++;
     		}
     	}

		$mail->Body    = $this->load->view('booking_confirmation',$booking,TRUE);
    	$mail->AltBody = 'Booking Confirmation [Global Explorer]';
    	//if($mail->send());
     	return $cnt;
    }

	public function voucher() {
		$booking_id=$this->uri->segment(3,0);
		$user_data = $this->userpersistence->getPersistedUser();
		$this->load->library('pdf');
		$this->load->model('booking/booking');
		$this->load->model("booking/bookingstorage");
		$summary_data['guest']=$this->booking->getByGuestVoucher($booking_id);
		$summary_data['invoice_pdf']=$this->booking->getBysummary($booking_id);
		$summary_data['provider_summary']=$this->booking->getBysummaryCompany($booking_id);
		$summary_data['room_summary']=$this->booking->getBysummaryRoomtype($booking_id);
		$summary_data['package_summarys']=$this->booking->getBysummaryPackage($booking_id);
		$summary_data['add_hotel_bookings']=$this->booking->getBysummaryPackageAddHotelBooking($booking_id);
		#get the individual summaries ----------------------------------------------
        $package_summary = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"type"=>"0package_surcharge"));
        $addon_summary = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"type"=>"4addon"));
        $extension_summary = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"type"=>"5extension"));

        #fetch the destination IDs ----------------------------------------------
        $destination_summary = $this->bookingstorage->getSummaryDestinations($booking_id);

        for($i=0;$i<count($destination_summary);$i++){
        	$destination = $destination_summary[$i];
        	#fetch the hotel, room surcharge, room summaries ----------------------------------------------
            $hotel_summary[] = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"destination_id"=>$destination["destination_id"],"type"=>"1hotel_surcharge"));
            $rs_summary[] = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"destination_id"=>$destination["destination_id"],"type"=>"2room_surcharge"));
            $r_summary[] = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"destination_id"=>$destination["destination_id"],"type"=>"3room_payment"));
        }

    	$summary_data['package_summary']=$package_summary;
    	$summary_data['addon_summary']=$addon_summary;
    	$summary_data['extension_summary']=$extension_summary;
    	$summary_data['destination_summary']=$destination_summary;
    	$summary_data['hotel_summary']=$hotel_summary;
    	$summary_data['rs_summary']=$rs_summary;
    	$summary_data['r_summary']=$r_summary;
    	$summary_data['user']=$user_data;
    	$pdf_user = get_Class($summary_data['user']);


		$this->pdf->set_paper( "letter", "portrait" );
		$this->pdf->load_view('panel/mainpanel/voucher_pdf_provider',$summary_data);
		$this->pdf->render();
		$this->pdf->stream('voucher.pdf',array('Attachment'=>0));
	}

	public function invoice_pdf() {
		if ($this->userpersistence->getPersistedUser() instanceof Customer) {
			redirect('errors/restricted_area');
		}
		$this->db->order_by('country');
		$countries = $this->db->get('countries')->result_array();
		$user_data = $this->userpersistence->getPersistedUser();
		$this->load->view('panel/mainpanel/invoice');
	}

	public function logout() {
		$this->userpersistence->releaseUser();
		redirect('');
	}
	/*
	*
	*Start do_upload function upload pdf link
	*
	*/
	function do_upload_attachment(){
		if ($this->userpersistence->getPersistedUser() instanceof Customer) {
			redirect('errors/restricted_area');
		}
		$this->load->model('hotel/hotelstorage');
		$link_pdf = array();
		$search_replace = array(' ','/','<','>');
		$link_pdf = array();
		$name1= str_replace($search_replace,"_",$_FILES['pdf_file']['name']);
		$name= time().$name1;
		$size = $_FILES['pdf_file']['size'];
		$tmp_name  = $_FILES['pdf_file']['tmp_name'];
		$allowed_ext = 'pdf|jpg|jpeg|docx';
		$max_size = '2000000000000';
		$destination ='uploads/destination_pdf/';
		if($this->file_upload($name,$size,$tmp_name,$allowed_ext,$max_size,$destination)){
			$link_pdf['destination'] = $this->input->post('destination');
			$link_pdf['title'] = $this->input->post('title');
			$link_pdf['pdf_name'] = $name;
			$this->hotelstorage->addDestinationLink($link_pdf);
			$array = array("status"=>"success", "msg"=> "File was successfully uploaded!");
			$this->session->set_flashdata($array);
			redirect('panel/attachment');
		}else{
			//$array = array("status"=>"error", "msg"=> "An error occured during file upload! Error:{$res}");
			$array = array("status"=>"error", "msg"=> "An error occured during file upload! Error");
			$this->session->set_flashdata($array);
			redirect('panel/attachment');
		}
	}

	/*
	*
	*End do_upload function upload pdf link
	*
	*/

	/*
	*
	*Start file_upload function upload pdf link
	*
	*/

	function file_upload($name,$size,$tmp_name,$allowed_ext,$max_size,$destination){
		$ext = $this->get_extention($name);
		if($this->set_and_check_ext($allowed_ext,$ext)){
			if($this->max_size($max_size,$size)){
				if($this->upload_path($tmp_name,$destination,$name)){
					return true;
				}
				else return false;
			}
			else return false;
		}
		else return false;
	}
	/*
	*
	*End file_upload function upload pdf link
	*
	*/

	/*
	*
	*Start get_extention function upload pdf link
	*
	*/
	function get_extention($name){
	 	return strtolower(substr($name, strrpos($name,'.',-1)+1));
	}

 	/*
	*
	*End get_extention function upload pdf link
	*
	*/
	/*
	*
	*Start max_size function upload pdf link
	*
	*/
	function max_size($max_size,$size){
	 	if($size < $max_size)
			return true;
		else
	   		return false;
	}
	/*
	*
	*End max_size function upload pdf link
	*
	*/

	/*
	*
	*Start upload_path function upload pdf link
	*
	*/
	function upload_path($tmp_name,$destination,$name){
		if(move_uploaded_file($tmp_name,$destination.$name))
			return true;
		else
			return false;
	}
	/*
	*
	*End upload_path function upload pdf link
	*
	*/

	/*
	*
	*Start set_and_check_ext function upload pdf link
	*
	*/
	function set_and_check_ext($allowed_ext,$ext){
	 	$get_allowed_ext = explode('|',$allowed_ext);
	 	if(in_array($ext,$get_allowed_ext))
	 		return true;
	 	else
	 		return false;
	}
	/*
	*
	*End set_and_check_ext function upload pdf link
	*
	*/

	/*
	*
	*Start delete_destination function upload pdf link
	*
	*/
	function delete_destination(){
		if ($this->userpersistence->getPersistedUser() instanceof Customer) {
			redirect('errors/restricted_area');
		}
		$this->load->model('hotel/hotelstorage');
		$deleted=$this->input->post('id');
		$imagename=$this->input->post('imagename');
		$filename = 'uploads/destination_pdf/'.$imagename;
		if (file_exists($filename)) {
			unlink('uploads/destination_pdf/'.$imagename);
			$this->hotelstorage->deleteDestinationLink($deleted);
			echo '1';
		}else{
			echo 'Error';
		}
	}
	/*
	*
	*End delete_destination function upload pdf link
	*
	*/
	public function report() {
		if ($this->userpersistence->getPersistedUser() instanceof Customer) {
			redirect('errors/restricted_area');
		}
		$this->load->model('user/user');
		$this->load->model('booking/booking');
		$this->load->library("pagination");


		$search_array = array(); #declare the search variable array
		$filter_arr = array();
		$start_time = isset($_GET["depart"]) ? strtotime($this->input->get("depart",TRUE)) : "";
		$end_time = isset($_GET["return"]) ? strtotime($this->input->get("return",TRUE)) : "";

		if(isset($_GET["booked_code"])){
			if(!empty($_GET["booked_code"])){
				//$search_array["bookings.code"] = $this->input->get("booked_code",TRUE);
				$search_array["bookings.code"] = $_GET["booked_code"];
			}
		}

		if(isset($_GET["name"])){
			if(!empty($_GET["name"])){
				//$search_array["bookings.code"] = $this->input->get("booked_code",TRUE);
				$search_array["traveller_details.name"] = $_GET["name"];
			}
		}

		if(isset($_GET["report_agent_id"])){
			if(!empty($_GET["report_agent_id"])){
				$search_array["comp.id"] = $_GET["report_agent_id"];
			}
		}

		if(isset($_GET["report_provider_id"])){
			if(!empty($_GET["report_provider_id"])){
				$search_array["companies.id"] = $_GET["report_provider_id"];
			}
		}

		// if(isset($_GET["depart"])){
		// 	if(!empty($_GET["depart"])){
		// 		$search_array["bookings.departure_date >= "] = date("Y-m-d 00:00:00",$start);
		// 	}
		// }

		// if(isset($_GET["return"])){
		// 	if(!empty($_GET["return"])){
		// 		$search_array["bookings.return_date <= "] = date("Y-m-t 23:59:59",$end);
		// 	}
		// }

		if(isset($_GET["depart"])){
			if(!empty($_GET["depart"])){
				$start_time = date("Y-m-d 00:00:00",$start_time);
				$filter_arr["bookings.departure_date >="] = $start_time;
			}
		}

		if(isset($_GET["return"])){
			if(!empty($_GET["return"])){
				$end_time = date("Y-m-t 23:59:59",$end_time);
				$filter_arr["bookings.return_date <="] = $end_time;
			}
		}

		if(isset($_GET["book_status"])){
			if(!empty($_GET["book_status"])){
				$search_array["bookings.status"] = $_GET["book_status"];
			}
		}


		$countries_usr   = $this->user->getAllCountries();
		$countries       = $this->db->get('countries')->result_array();
		$user_data       = $this->userpersistence->getPersistedUser();

		$report_agent    = $this->booking->getByagentreport();
		$report_provider = $this->booking->getByproviderreport();

		//$select_alldata  = $this->booking->getAllBookingRecords_page(false,false,$search_array);
		$select_alldata  = $this->booking->getAllBookingRecords_page_report(false,false,$search_array,$filter_arr);

		/*
		*start of pagination code
		*/
		$start = 0;

		if(isset($_GET["per_page"])){
			$start = intVal($_GET["per_page"]) == 0 ? 0 : intval($_GET["per_page"]);
		}

		$new_get = $_GET;
		unset($new_get["per_page"]);

		$config["base_url"]          = base_url("panel/report")."?".http_build_query($new_get, '', "&"); #set the url link
		$config["total_rows"]        = count($select_alldata); #count the total number of rows
		$config["per_page"]          = 10; # set the number of rows per page
		$config["num_links"]          = 5; # set the number of rows per page
		$config['page_query_string'] = TRUE;
		$config['full_tag_open']     = "<ul class='pagination pagination-sm'>";
		$config['full_tag_close']    ="</ul>";
		$config['num_tag_open']      = '<li>';
		$config['num_tag_close']     = '</li>';
		$config['cur_tag_open']      = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close']     = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open']     = "<li>";
		$config['next_tagl_close']   = "</li>";
		$config['prev_tag_open']     = "<li>";
		$config['prev_tagl_close']   = "</li>";
		$config['first_tag_open']    = "<li>";
		$config['first_tagl_close']  = "</li>";
		$config['last_tag_open']     = "<li>";
		$config['last_tagl_close']   = "</li>";

		$this->pagination->initialize($config); #initialzie the library

		/*
		* end of pagination code
		*/

		$select_alldata = $this->booking->getAllBookingRecords_page_report($start,$config["per_page"],$search_array,$filter_arr); #refetch all the data
		for ($i=0; $i < count($select_alldata) ; $i++) {
			$select_alldata[$i]->hotel=$this->booking->getAllBookinghotelRecord($select_alldata[$i]->BOOKID);
		}
		$allbookings = $this->booking->getAllBookingRecords_page_report();
		$this->panelView(null, array(
				'salutations' => array(
						'mr.' => 'Mr.',
						'ms.' => 'Ms.',
						'mrs.' => 'Mrs.',
				),
				'countries_usr' => $countries_usr,
				'user' => $user_data,
				'countries' => $countries,
				'report_agent' => $report_agent,
				'booked_data'=>$select_alldata,
				'all_booking' => $allbookings,
				'report_provider' => $report_provider,
				"page_links" =>$this->pagination->create_links()
		));
	}

	/*
	*disinfect the variables
	*/
	public function disinfect($str){
		$this->load->library("security");
		if(is_array($str)){
			if(count($str)){
				for($i=0;$i<count($str);$i++){
					if(is_array($str[$i])){
						for($ii=0;$ii<count($str[$i]);$ii++){
							$str[$i][$ii] = $this->security->xss_clean($str[$i][$ii]);
						}
					}else{
						$str[$i] = $this->security->xss_clean($str[$i]);
					}
				}
			}
			return $str;
		}else{
			return $this->security->xss_clean($str);
		}
	}
	/*
	*end of disinfect variables
	*/


	/*
	*
	*Start filter_data function upload pdf link
	*
	*/
	function filter_data(){
		$userid=$this->session->userdata('id');
		$this->load->model('booking/booking');
		$data['code']=$this->input->post('booked_code');
		$data['status']=$this->input->post('booking_status');
		$data['origin']=$this->input->post('origin');
		$data['return']=$this->input->post('return');
		$user_data = $this->userpersistence->getPersistedUser();
		if(get_class($user_data) === 'Admin'){
			$select_alldata  = $this->booking->getAllBookingRecords_filter($data);
			if($select_alldata != NULL){
				foreach ($select_alldata as $key) {
					$booking_id=$key->BOOKID;
				}
			}else{
				$booking_id="";
			}
        }
		if(get_class($user_data) === 'Agent'){
			$select_alldata  = $this->booking->getAllBookingRecordsAgent_filter($data,$userid);
			if($select_alldata != NULL){
				foreach ($select_alldata as $key) {
					$booking_id=$key->BOOKID;
				}
			}else{
				$booking_id="";
			}
		}
		if(get_class($user_data) === 'Provider'){
			$select_alldata  = $this->booking->getAllBookingRecordsProvider_filter($data,$userid);
			if($select_alldata != NULL){
				foreach ($select_alldata as $key) {
					$booking_id=$key->BOOKID;
				}
			}else{
				$booking_id="";
			}
		}
		$booked_id = $booking_id;
		$select_hotel  = $this->booking->getAllBookinghotelRecord($booked_id);
		$this->load->view('panel/mainpanel/append_booking_data',array(
			'booked_data'=>$select_alldata,
			'user' => $user_data,
			'booked_hotel_data'=>$select_hotel
		));
	}
	/*
	*
	*End filter_data function upload pdf link
	*
	*/

	/*
	*
	*Start filter_report function
	*/
	function filter_report(){
		$ref = (isset($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : FALSE;
		if($ref){
			$urlArr = parse_url($ref);
			$pathArr = explode('/', $urlArr['path']);

			if(@$pathArr[2] !== 'report')
				$this->session->unset_userdata('filter_fields');
		}
		/*$userid=$this->session->userdata('id');*/
		$this->load->model('booking/booking');
		$data['agent_id']=$this->input->post('report_agent_id');
		$data['provider_id']=$this->input->post('report_provider_id');
		$data['depart']=$this->input->post('depart');
		$data['return']=$this->input->post('return');
		$data['book_status']=$this->input->post('book_status');
		$user_data = $this->userpersistence->getPersistedUser();
		//holds data from session
		$post_data = $this->session->userdata('filter_fields');

		$config['base_url'] = base_url('panel/report');
		$config['per_page'] = 9;
		$config['num_links'] = 5;

		if(!is_bool($post_data) && array_filter($post_data)){
			$config['total_rows'] = $this->booking->filterReport($post_data)->num_rows();
		 	$select_alldata = $this->booking->filterReport($post_data,$config['per_page'], $this->uri->segment(3))->result();
		}else{
			$result = $this->booking->getReport();
			if($result) {
				$config['total_rows'] = $result->num_rows();
				$select_alldata = $this->booking->getReport(array(), $config['per_page'], $this->uri->segment(3))->result();
			} else {
				$config['total_rows'] = 0;
				$select_alldata = array();
			}
		}

		$select_alldata  = $this->booking->getAllBookingRecords_report($data);
		if($select_alldata != NULL){
			$booking_id=array();
			foreach ($select_alldata as $key) {
				$booking_id=$key->BOOKID;
			}
			$booked_id=$booking_id;
			$select_hotel  = $this->booking->getAllBookinghotelRecord($booked_id);
		}else{
			$select_hotel="";
		}
		$this->load->view('panel/mainpanel/append_report_data',array(
			'booked_data'=>$select_alldata,
			'user' => $user_data,
			'booked_hotel_data'=>$select_hotel
		));
	}
	/*
	*
	*End filter_report function
	*
	*/

	public function list_attachment() {
		if ($this->userpersistence->getPersistedUser() instanceof Customer) {
			redirect('errors/restricted_area');
		}
		$this->load->model('hotel/hotelstorage');
		$this->db->order_by('country','asc');
		$countries = $this->db->get('countries')->result_array();
		$user_data = $this->userpersistence->getPersistedUser();
		$destination_link=$this->hotelstorage->getDestination_Link();
		$this->load->view('panel/mainpanel/append_attachment',array(
				'countries' => $countries,
				'user' => $user_data,
				'link_destination'=>$destination_link
		));
	}

	public function list_destination() {
		if ($this->userpersistence->getPersistedUser() instanceof Customer) {
			redirect('errors/restricted_area');
		}
		$userid=$this->session->userdata('id');
		$this->db->order_by('country','asc');
		$this->load->model('booking/booking');
		$countries = $this->db->get('countries')->result_array();
		$user_data = $this->userpersistence->getPersistedUser();
		$select_alldata_agent  = $this->booking->getAllBookingRecordsAgent_page($userid);
		$select_alldata_provider  = $this->booking->getAllBookingRecordsProvider_page($userid);
		$select_alldata  = $this->booking->getAllBookingRecords_page();
		$this->load->view('panel/mainpanel/append_booking_data',array(
				'booked_data'=>$select_alldata,
				'booked_data_agent'=>$select_alldata_agent,
				'booked_data_provider'=>$select_alldata_provider,
				'user' => $user_data
		));
	}

	public function paginator($configArr=array()){

		$this->load->library('pagination');
		$config['total_rows'] = 0;
		$config['base_url'] = '';
		$config['per_page'] = 0;
		$config['num_links'] = 0;
		$config['full_tag_open'] = "<ul class='pagination pagination-sm'>";
		$config['full_tag_close'] ="</ul>";
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open'] = "<li>";
		$config['next_tagl_close'] = "</li>";
		$config['prev_tag_open'] = "<li>";
		$config['prev_tagl_close'] = "</li>";
		$config['first_tag_open'] = "<li>";
		$config['first_tagl_close'] = "</li>";
		$config['last_tag_open'] = "<li>";
		$config['last_tagl_close'] = "</li>";


		foreach($configArr as $c=>$conf){
			$config[$c] = $conf;
		}
		$this->pagination->initialize($config);
	}

	public function pdf(){
		$booking_id=$this->uri->segment(3,0);
		$this->load->library('pdf');
		$this->load->model('booking/booking');
		$this->load->model("booking/bookingstorage");
		$user_data = $this->userpersistence->getPersistedUser();
		$summary_data['guest']=$this->booking->getByGuest($booking_id);
		$summary_data['invoice_pdf']=$this->booking->getBysummary($booking_id);
		//$summary_data['provider_summary']=$this->booking->getBysummaryCompany($booking_id);
		$summary_data['agent_summary']=$this->booking->getBysummaryCompanyAgent($booking_id);
		$summary_data['room_summary']=$this->booking->getBysummaryRoomtype($booking_id);
        #load the booking storage model ----------------------------------------------

        #get the individual summaries ----------------------------------------------
        $package_summary = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"type"=>"0package_surcharge"));
        $addon_summary = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"type"=>"4addon"));
        $extension_summary = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"type"=>"5extension"));

        #fetch the destination IDs ----------------------------------------------
        $destination_summary = $this->bookingstorage->getSummaryDestinations($booking_id);

        for($i=0;$i<count($destination_summary);$i++){
        	$destination = $destination_summary[$i];
        	#fetch the hotel, room surcharge, room summaries ----------------------------------------------
            $hotel_summary[] = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"destination_id"=>$destination["destination_id"],"type"=>"1hotel_surcharge"));
            $rs_summary[] = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"destination_id"=>$destination["destination_id"],"type"=>"2room_surcharge"));
            $r_summary[] = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"destination_id"=>$destination["destination_id"],"type"=>"3room_payment"));
        }

    	$summary_data['package_summary']=$package_summary;
    	$summary_data['addon_summary']=$addon_summary;
    	$summary_data['extension_summary']=$extension_summary;
    	$summary_data['destination_summary']=$destination_summary;
    	$summary_data['hotel_summary']=$hotel_summary;
    	$summary_data['rs_summary']=$rs_summary;
    	$summary_data['r_summary']=$r_summary;
    	$summary_data['user']=$user_data;
		$this->pdf->set_paper( "letter", "portrait" );
		$this->pdf->load_view('panel/mainpanel/invoice_pdf',$summary_data);
		$this->pdf->render();
		$this->pdf->stream('invoice.pdf');
		//$this->pdf->stream('invoice.pdf',array('Attachment'=>0));
	}

	public function price_pdf(){
		if ($this->userpersistence->getPersistedUser() instanceof Customer) {
			redirect('errors/restricted_area');
		}
		$this->load->library('pdf');
		$this->load->model('booking/booking');
		$packageid=$this->input->post('package_id');
		$summary_data['room_summary']=$this->booking->getBysummaryPricelist($packageid);
		$this->pdf->set_paper( "legal", "landscape" );
		$this->pdf->load_view('panel/mainpanel/pricelist_pdf',$summary_data);
		$this->pdf->render();
		$this->pdf->stream('pricelist.pdf');
	}
	public function excel(){
		if ($this->userpersistence->getPersistedUser() instanceof Customer) {
			redirect('errors/restricted_area');
		}
		$this->load->view('panel/mainpanel/invoice_excel');
	}

	public function mods_destination(){
		if ($this->userpersistence->getPersistedUser() instanceof Customer) {
			redirect('errors/restricted_area');
		}
		$this->load->helper('form');

		$this->db->order_by('country','asc');
		$countries = $this->db->get('countries')->result();
		$this->load->model('user/user');
		$countries_usr = $this->user->getAllCountries();
		$user_data = $this->userpersistence->getPersistedUser();

		$this->panelView(null, array(
			'salutations' => array(
					'mr.' => 'Mr.',
					'ms.' => 'Ms.',
					'mrs.' => 'Mrs.',
			),
			'destinations' => $countries,
			'countries_usr' => $countries_usr,
			'countries' => $countries,
			'user' => $user_data,
		));
    }

	public function mods_airport(){
		if ($this->userpersistence->getPersistedUser() instanceof Customer) {
			redirect('errors/restricted_area');
		}
		$this->load->helper('form');

		$this->db->order_by('country','asc');
		$countries = $this->db->get('countries')->result();
		$this->load->model('user/user');
		$countries_usr = $this->user->getAllCountries();
		$user_data = $this->userpersistence->getPersistedUser();
		$this->db->order_by('name','asc');
		$airportcodes = $this->db->get('airportcodes')->result();

		$this->panelView(null, array(
			'salutations' => array(
					'mr.' => 'Mr.',
					'ms.' => 'Ms.',
					'mrs.' => 'Mrs.',
			),
			'airportcodes' => $airportcodes,
			'countries_usr' => $countries_usr,
			'countries' => $countries,
			'user' => $user_data,
		));
    }

    public function action_booking(){
    	if ($this->userpersistence->getPersistedUser() instanceof Customer) {
			redirect('errors/restricted_area');
		}
    	$this->load->model('booking/booking');
    	$this->load->model('user/userpersistence');
    	$id=$this->input->post('id');
    	$action=$this->input->post('action');
    	switch ($action) {
    		case '1':
    			$cancel="cancelled";
    			$res=$this->booking->updateStatusBooking($id,$cancel);
    			$resend=$this->booking_cancellation($id, $this->userpersistence->getPersistedUser());
    			if($res == true)
    				echo $cancel;
    			else
    				echo 'not cancelled';
    			break;
    		case '2':
    			$reject="rejected";
    			$res=$this->booking->updateStatusBooking($id,$reject);
    			$resend=$this->booking_reject($id, $this->userpersistence->getPersistedUser());
    			if($res == true)
    				echo $reject;
    			else
    				echo 'not rejected';
    			break;
    		case '3':
    			$confirm="confirmed";
    			$res=$this->booking->updateStatusBooking($id,$confirm);
    			$resend=$this->booking_confirmed($id, $this->userpersistence->getPersistedUser());
    			if($res == true) {
    				echo $confirm;

    			} else {
    				echo 'not confirmed';
    			}
    			break;
    		case '4':
    			$hotel="ammended";
    			$res=$this->booking->updateStatusBooking($id,$hotel);
    			$resend=$this->booking_hotel_full($id, $this->userpersistence->getPersistedUser());
    			if($res == true) {
    				echo $hotel;

    			} else {
    				echo 'not amended';
    			}
    			break;
    	}
		return $resend;
    }

	public function decrypt($encrypted_id, $key) {
	    $data = pack('H*', $encrypted_id); // Translate back to binary
	    $data = mcrypt_decrypt(MCRYPT_BLOWFISH, $key, $data, 'ecb');
	    $data = base_convert($data, 36, 35);
	    return $data;
	}

	public function agent_payment(){
		$this->load->model('booking/payment');
    	$this->load->model("booking/booking");
    	$data_payment['type']=$this->input->post('radPaymenttype');
    	if($data_payment['type'] == 'cash'){
    		$des="";
    	}else{
    		$des=$this->input->post('description');
    	}

    	$data_payment['description']=$des;
    	$data_payment['total_amount']=$this->input->post('result_total_payment');
    	$res=$this->booking->addPaymenttype($data_payment);
		$ex_data = explode(',',$this->input->post('arr_res'));

		for ($i=0; $i <count($ex_data) ; $i++) {
			$insert_payment=$ex_data[$i];
			$value = explode('IamADelimiter',$insert_payment);

			if($this->input->post('tabVal') === "pending") {
				$data=array(
	    		'booking_id'=>$value[0],
		    	'payment_type_id'=>$res['id'],
		    	'status'=>'collected'
	    		);
    			$this->db->insert('payments',$data);
			}
			else if($this->input->post('tabVal') === "view") {
				$this->booking->payment_provider($value[0],$res['id']);
			}
		}
		if($this->input->post('tabVal') === "pending") { redirect('panel/payment?tab=view'); }
		else if($this->input->post('tabVal') === "view") { redirect('panel/payment?tab=add'); }
	}

	public function getHotel(){
		$this->load->model("booking/booking");
		$hotelname = $this->booking->getHotelName('GE100186');
		foreach ($hotelname as $hn) {
			dump($hn['name']);
		}
	}

	public function Printpackages($id){
		$this->load->model('package/packagestorage');
		$this->load->library('pdf');
		$this->db->select('*');
		$this->db->from('packages');
		$this->db->join('package_duration', 'packages.id = package_duration.package_id', 'left');
		$this->db->join('package_description', 'packages.id = package_description.package_id', 'left');
		$this->db->where('packages.id', $id);
		$data['package'] = $this->db->get()->result();

		//generate pdf
		$this->pdf->set_paper( "letter", "portrait" );
		$this->pdf->load_view('panel/mainpanel/printpackages_pdf',$data);
		$this->pdf->render();
		$this->pdf->stream('package.pdf',array('Attachment'=>0));

		//$this->load->view('panel/mainpanel/printpackages_pdf',$data);
	}

	public function test(){
		$this->load->model("booking/booking");
		$admins = $this->booking->getEmailadmin();
		foreach($admins as $a){
			$get_provider_company = $this->db->select('*')->from('companies')->join('company_designations', 'companies.id = company_designations.company_id')->where(array('company_designations.user_id'=>$a->adminid))->get()->result();
			print_r($get_provider_company);
		}

	}

	public function addToSession($field){
		$array = $this->session->userdata('fields');
		if(!in_array($field, $array))
		{
			array_push($array, $field);
	        $this->session->set_userdata('fields',$array);
    	}
	}

	public function removeFromSession($field){
		$array = $this->session->userdata('fields');
		$new_session = array_diff($array, array($field));
		$this->session->set_userdata('fields',$new_session);
	}

	public function apps() {

		$this->load->model('user/user');

		if ($this->userpersistence->getPersistedUser() instanceof Customer) {
			redirect('errors/restricted_area');
		}

		$countries_usr = $this->user->getAllCountries();
		$user_data = $this->userpersistence->getPersistedUser();

		$this->panelView(null, array(
				'salutations' => array(
						'mr.' => 'Mr.',
						'ms.' => 'Ms.',
						'mrs.' => 'Mrs.',
				),
				'user' => $user_data,
				'countries_usr'=>$countries_usr
			)
		);
	}

	

}