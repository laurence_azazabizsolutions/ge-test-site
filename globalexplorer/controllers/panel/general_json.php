<?php 
class general_json extends MY_Controller{

	#constructor class
	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->helper("debugger_helper");
	}
	
	/*
	*remove hotel from package...
	*/
	public function removePackageHotelID(){
		#fetch the hotel id
		$package_hotel_id = $this->input->post("package_hotel_id");
			
		$update = array("status"=>"inactive");
		$this->db->where("id",$package_hotel_id);
		$this->db->update("package_hotels",$update);
	}


	/*
	* remove surcharge from the hotel room
	*/
	public function removeSurchargeHotelRooms(){
		#fetch the surcharge id
		$surcharge_id = $this->input->post("surcharge_id");

		$update = array("status"=>"inactive");
		$this->db->where("id",$surcharge_id);
		$this->db->update("hotelroom_surcharges",$update);
	}


	/*
	* remove the roomrate from the hotel package
	*/
	public function removePackageRoomRate(){
		#fetch the roomrate  id
		$roomrate_id = $this->input->post('roomrate_id');

		$update = array("status" => "inactive");
		$this->db->where("id",$roomrate_id);
		$this->db->update("hotelroom_rates",$update);
	}

	/*
	*multi-purpose element remove
	*/
	public function removeGeneralElement(){
		$table = $this->input->post('table');
		$id = $this->input->post("id");

		$update = array("status"=> "inactive");
		$this->db->where("id",$id);
		$this->db->update($table,$update);
	}

}