<?php 
class booking_json extends MY_Controller{
	
	function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->model('surcharge/surchargestorage');
		$this->load->helper("debugger_helper");
	}



	/*
	*ensures that the details are sent via session!
	*step_booking will contain the data from the step 2 "booking" sections!
	*step_detail will contain the data from the step 3 booking detaisl!
	*this function will be called through ajax!
	*/
	function ajax_step_details(){
		$step_booking = $this->session->userdata("step_booking");
		$this->session->set_userdata("step_detail",$_POST);
		$this->session->set_userdata("merged_booking_details", array_merge($this->session->userdata("step_booking"),$this->session->userdata("step_detail")));
	}


	/*
	*get other airportcodes
	*/
	function fetchDynamicAirportCodes(){
		#get passed code ----------------------------------------------
		$code = $_POST['getAirportCode_curr_code'];
		#get the id by delimiting the "." symbol ----------------------------------------------
		$id_tmp = explode(".",$code);
		$id = intval($id_tmp[1]);


		$this->db->select("*");
		$this->db->from("airportcodes");
		$this->db->where("id !=",$id);
		$q = $this->db->get();

		$num = $q->num_rows();

		if($num!=0){
			$results = $q->result_array();
			echo json_encode(array("error"=>false,"countries"=>$results));
		}else{
			echo json_encode(array("error"=>true,"countries"=>"ERROR"));
		}
	}


	/*
	*fetch package surcharges ----------------------------------------------
	*fetch all the surcharges of a particular package
	*/
	public function fetchPackageSurcharges(){

		$start_time = date('Y-m-d',strtotime(str_replace('-', ' ' , $_POST['depart_value'])));
		$end_time = date('Y-m-d',strtotime(str_replace('-', ' ' , $_POST['return_value'])));
		$package_id = $_POST['package_id'];


		#get package surcharges based on its ID ----------------------------------------------
		$package_surcharges = $this->surchargestorage->getSurchargeByType_id("package",$package_id, TRUE);
		

		#insert surcharges into an array ----------------------------------------------
		$surcharges = array("packages"=>$package_surcharges);
		$dates = array("return"=>$end_time,"depart"=>$start_time);


		#get the package surcharges ----------------------------------------------
		$package_surcharges = $this->surchargestorage->processSurchargePackage($surcharges, $dates);


		#count the package surcharges ----------------------------------------------
		$count_test = count($package_surcharges);

		//insert into array ----------------------------------------------
		$arr = array();
		if($count_test!=0){
			foreach ($package_surcharges as $categ) {
				foreach ($categ as $surcharge) {
					$arr[]=$surcharge;
				}		
			}
		}

		//final counter ----------------------------------------------
		$final_count = count($arr);
		
		#set if final checker count is set to zero ----------------------------------------------
		if($final_count!=0){
			echo json_encode(array("error"=>false, "surcharges"=>$arr));
		}else{
			echo json_encode(array("error"=>true, "msg"=>"empty"));
		}

	}
	/*
	*end of fetch all package surcharges ----------------------------------------------
	*/


	/*
	*get all the hotel surcharges ----------------------------------------------
	*/
	public function fetchHotelSurcharges(){
		$start_time = $_POST['depart_value'];
		$end_time = $_POST['return_value'];
		$hotel_id = $_POST['hotel_id'];

		$hotels_surcharges = $this->surchargestorage->getSurchargeByType_id("hotel",$hotel_id,TRUE); #fetch surcharge of hotels
		#$surcharges $this->surchargestorage->processSurchargeHotels("hotels");
		
		#insert surcharges into an array ----------------------------------------------
		$surcharges = array("hotels"=>$hotels_surcharges);
		$dates = array("return"=>$end_time,"depart"=>$start_time);

		#get the hotel surcharges ----------------------------------------------
		$hotel_surcharges = $this->surchargestorage->processSurchargeHotels($surcharges, $dates);

		#count the package surcharges ----------------------------------------------
		$count_test = count($hotel_surcharges);

		//insert into array ----------------------------------------------
		$arr = array();
		if($count_test!=0){
			foreach ($hotel_surcharges as $categ) {
				foreach ($categ as $surcharge) {
					$arr[]=$surcharge;
				}		
			}
		}

		//final counter ----------------------------------------------
		$final_count = count($arr);

		#set if final checker count is set to zero ----------------------------------------------
		if($final_count!=0){
			echo json_encode(array("error"=>false, "surcharges"=>$arr));
		}else{
			echo json_encode(array("error"=>true, "msg"=>"empty"));
		}

	}



	/*
	*generate the surcharges
	*/
	public function generateSurcharges(){
		$data['bdat'] = $_POST;
		/*dump($_POST);
		die();*/
		$this->load->view("panel/mainpanel/create_booking_surcharges",$data);
	}	
	/*	
	*end of generate the surcharges
	*/



	/*
	*compute surcharges
	*/
	public function computeSurcharges(){
		
	}
	/*	
	*end of compute surcharges
	*/


	/*
	*fetch the destinations for package creations
	*/
	public function fetchDestinations(){
		
		$destinations = @$_POST['destinations']; //get the destinations

		if(count($destinations)==0){
			$destinations[] = "NEIN";
		}

		
		$this->db->select("*");
		$this->db->from("countries");
		$this->db->where_not_in("code",$destinations);
		$q = $this->db->get()->result_array();
		if(count($q)!=0){
			foreach ($q as $country) {
				echo "<option value='{$country['code']}'>{$country['country']}</option>";
			}
		}else{
			echo "nein";
		}
		
	}

}
?>