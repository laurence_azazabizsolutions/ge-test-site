<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @author Joshua Paylaga
 *
 */
class PanelRegister extends MY_Controller {

	/**
	 *
	 */
	public function __construct() {
		parent::__construct();
		if ($this->userpersistence->getPersistedUser() instanceof Customer) {
			redirect('errors/restricted_area');
		}
		$this->load->helper(array('form','url'));
		$this->load->database();

		//	send mail
		$this->load->library('My_PHPMailer');
		$this->load->helper('email');
		$this->mail = new PHPMailer();
		$this->mail->SMTPDebug = 0;
		$this->mail->isSMTP();																// Set mailer to use SMTP
		$this->mail->Host = 'cot.cottoncare.com.sg';										// Specify main and backup SMTP servers
		$this->mail->SMTPAuth = true;														// Enable SMTP authentication
		$this->mail->Username = 'no-reply@budgetmaid.com.sg';								// SMTP username
		$this->mail->Password = 'noreply@123';												// SMTP password
		$this->mail->SMTPSecure = 'tls';													// Enable TLS encryption, `ssl` also accepted
		$this->mail->Port = 587;															// TCP port to connect to

    	$this->mail->addBCC('delabahan@gmail.com');
    	$this->mail->addBCC('jude@azazabizsolutions.com');
	}

	/**
	 *
	 */
	public function register_user() {

		$this->load->model('user/user');
		$this->load->model('user/userfactory');
		$this->load->model('user/userstorage');

		$userdata = $this->input->post();
		if(!empty($userdata['emergency_name'])){
			for ($i=0; $i < count($userdata['emergency_name']); $i++) {
				foreach ($userdata['emergency_name'] as $value) {
					$data_arr[]=$value;
				}
			}
			$data_array = json_encode($data_arr);
		}
		if(!empty($userdata['emergency_phone_number'])){
			for ($ii=0; $ii < count($userdata['emergency_phone_number']); $ii++) {
				foreach ($userdata['emergency_phone_number'] as $value) {
					$data_arrr[]=$value;
				}
			}
			$data_array1 = json_encode($data_arrr);
		}
		if ($userdata['password'] == $userdata['confirm_password'] ) {
			$this->user->setSalutation( $userdata['salutation'] );
			$this->user->setFirstname( $userdata['firstname'] );
			$this->user->setMiddlename( $userdata['middlename'] );
			$this->user->setLastname( $userdata['lastname'] );
			$this->user->setCompany( $userdata['company'] );
			$this->user->setPhone( $userdata['phone_number'] );
			$this->user->setEmergencyname( $data_array );
			$this->user->setEmergencyphone( $data_array1 );
			$this->user->setEmail( $userdata['email'] );
			$this->user->setUsername( $userdata['username'] );
			$this->user->setAlteremail( $userdata['alteremail'] );
			$this->user->setCountry( $userdata['country'] );
			$this->user->setPassword( $userdata['password'] );
			$this->user->setDateOfBirth( $userdata['dob'] );
			$this->userfactory->setUser( $this->user );
			$user = $this->userfactory->buildInstance( $userdata['user_type'] );
			$match = $this->db->get_where('users', array(
				'email' =>$userdata['email'],
				));

			$match_user = $this->db->get_where('users', array(
				'username' =>$userdata['username'],
				));
			if ($match_user->num_rows() <= 0) {
				if ($match->num_rows() <= 0) {
					$this->userstorage->store($user);
					$conc = '';
					/*if ($this->user->stored()) {*/
					// Create string to hash
						$toHash = sprintf("%d_%s_%s",$this->user->getId(),
							$this->user->getEmail(),
							$this->user->getPassword());

					// Use hashmonster to create hash
						$hash = hashmonster($toHash);
						$this->load->database();
						$this->db->insert('confirmation', array(
							'user_id' => $this->user->getId(),
							'hash' => $hash,
							'timestamp' => time(),
							));

					// Create link
						$link = base_url('register/confirmation/' . $hash);

						if ($this->email_confirmation($link,$this->user)) {
							$conc = ' An email verification was sent.';
						}

						echo json_encode(array(
							'alert_type'=>'Success',
							'message'=>'Successfully registered '.$userdata['firstname'].' '.$userdata['lastname'].'.'.$conc,
							'userdata'=>array(
								'user_id'=>$this->user->getId(),
								'firstname'=>$userdata['firstname'],
								'firstname'=>$userdata['firstname'],
								'lastname'=>$userdata['lastname'],
								'company'=>$userdata['company'],
								'email'=>$userdata['email'],
								))
						);
						/*} else {
						echo json_encode(
						array(
						'alert_type'=>'Error',
						'message'=>'Registration failed. Please try again.' . $conc
						)
						);
						}*/
						} else {
							echo json_encode(
								array(
									'alert_type'=>'Error',
									'message'=>'Email already exists.'
									)
								);
						}
					} else {
						echo json_encode(
							array(
								'alert_type'=>'Error',
								'message'=>'Username already exists.'
								)
							);
					}
				} else {
					echo json_encode(
						array(
							'alert_type'=>'Error',
							'message'=>'Passwords do not match'
							)
						);
				}
}

	public function update_user() {
		/*if(isset($_POST["testvariabel"]))
     	{
       		echo "testvariabel has been set!";
	     }*/
		$this->load->model('user/user');
		$this->load->model('user/userfactory');
		$this->load->model('user/userstorage');
		$this->userstorage->includePending(true);
		$user_data = $this->userpersistence->getPersistedUser();
		$aRemergencyname = $this->input->post('emergency_name');
		$aRemergencynumber = $this->input->post('emergency_phone_number');
		if(@$aRemergencyname != ""){
			for ($i=0; $i < count($aRemergencyname); $i++) {
				$data_arr[]=$aRemergencyname[$i];
			}
			$data_array = json_encode($data_arr);
		}

		if(@$aRemergencynumber != ""){
			for ($ii=0; $ii < count($aRemergencynumber); $ii++) {
				$data_arrr[]=$aRemergencynumber[$ii];
			}
			$data_array1 = json_encode($data_arrr);
		}

		$changePass = false;

		$userId = (!is_numeric($this->input->post('user_id')))?:$this->input->post('user_id');

		if ($userId) {
			if ($this->input->post('prof_form')=="1") {
				if ($this->input->post('old_password') !== "") {
					if (hashmonster($this->input->post('old_password')) === $user_data->getUser()->getPassword()) {
						if ($this->input->post('new_password')!=='') {
							if ($this->input->post('new_password') === $this->input->post('confirm_new_password')) {
								$changePass = true;
							} else {
								echo json_encode(array(
									'alert_type'=>'Error',
									'message'	=>'Your new password doesn\'t match.'
								));
								return false;
							}
						} else {
							echo json_encode(array(
								'alert_type'=>'Error',
								'message'	=>'You have not entered a new password.'
							));
							return false;
						}
					} else {
						echo json_encode(array(
							'alert_type'=>'Error',
							'message'	=>'Incorrect password (old). You are not allowed to change the password. Try again.'
						));
						return false;
					}
				}
			}
			$userId = intval($userId);
			$updateUser = $this->userstorage->getById($userId);
			$updateUser->getUser()->setSalutation( $this->input->post('salutation') );
			$updateUser->getUser()->setFirstname( $this->input->post('firstname') );
			$updateUser->getUser()->setMiddlename( $this->input->post('middlename') );
			$updateUser->getUser()->setLastname( $this->input->post('lastname') );
			$updateUser->getUser()->setUsername( $this->input->post('username') );
			$updateUser->getUser()->setDateOfBirth( $this->input->post('dob') );
			$updateUser->getUser()->setCompany( $this->input->post('company') );
			$updateUser->getUser()->setPhone( $this->input->post('phone_number') );
			$updateUser->getUser()->setEmergencyname( $data_array);
			$updateUser->getUser()->setEmergencyphone( $data_array1 );
			$updateUser->getUser()->setEmail( $this->input->post('email') );
			$updateUser->getUser()->setAlteremail( $this->input->post('alternative_email') );
			$updateUser->getUser()->setCountry( $this->input->post('country') );
			if ($changePass)
				$updateUser->getUser()->setPassword( $this->input->post('new_password') );

			// $this->userstorage->store($updateUser);
			// if ($userId === $user_data->getUser()->getId())
			// 	$this->userpersistence->persistUser($updateUser);

			$prev = $updateUser->getUser()->prevState();
			if ($updateUser->getUser()->prevState()->prevState())
				$prev = $updateUser->getUser()->prevState()->prevState();

			$isSuccess = !(
					$updateUser->getUser()->getSalutation() == $prev->getSalutation()
				&& $updateUser->getUser()->getFirstname() 	== $prev->getFirstname()
				&& $updateUser->getUser()->getMiddlename() 	== $prev->getMiddlename()
				&& $updateUser->getUser()->getLastname() 	== $prev->getLastname()
				&& $updateUser->getUser()->getUsername() 	== $prev->getUsername()
				&& $updateUser->getUser()->getDateOfBirth() == $prev->getDateOfBirth()
				&& $updateUser->getUser()->getCompany() 	== $prev->getCompany()
				&& $updateUser->getUser()->getPhone() 		== $prev->getPhone()
				&& $updateUser->getUser()->getEmergencyname() 		== $prev->getEmergencyname()
				&& $updateUser->getUser()->getEmergencyphone() 		== $prev->getEmergencyphone()
				&& $updateUser->getUser()->getEmail() 		== $prev->getEmail()
				&& $updateUser->getUser()->getAlteremail() 		== $prev->getAlteremail()
				&& $updateUser->getUser()->getCountry() 	== $prev->getCountry()
				&& $updateUser->getUser()->getPassword() 	== $prev->getPassword()
			);
			if(!empty($_POST['grant_all_permissions']))
     			{
		     		$packages_id = $this->userstorage->getAllPackages();
		     		foreach ($packages_id as $all_package_id) {
		     			$compare_packages = $this->userstorage->compare_packages($all_package_id->id,$userId);
		     			if ($compare_packages) {

		     			}else{
		     				$grant_packages = $this->userstorage->grant_packages($all_package_id->id,$userId);
		     			}
		     		}
		     	}
			if ($isSuccess) {

				$this->userstorage->store($updateUser);
				if ($userId === $user_data->getUser()->getId())
					$this->userpersistence->persistUser($updateUser);


				$change = (
					$this->input->post('firstname') != $prev->getFirstname()
				 || $this->input->post('lastname')	!= $prev->getLastname()
				 || $this->input->post('company')	!= $prev->getCompany()
				);

				$usertype = (isset($_POST['user_type'])) ? $_POST['user_type'] : null;
				echo json_encode(array(
					'alert_type'=>'Success',
					'message'	=>'Update successful',
					'change'	=>$change,
					'data'		=>array(
						'id'		=>$userId,
						'firstname'	=>$this->input->post('firstname'),
						'lastname'	=>$this->input->post('lastname'),
						'username'	=>$this->input->post('username'),
						'company'	=>$this->input->post('company'),
						'email'		=>$this->input->post('email'),
						'alternative_email'		=>$this->input->post('alternative_email'),
						'usertype'	=>$usertype
					)
				));
			} else {
				echo json_encode(array(
					'alert_type'=>'Error',
					'message'	=>'No change has been detected.'
				));
			}
		} else {
			echo json_encode(array(
				'alert_type'=>'Error',
				'message'	=>'Invalid user ID.'
			));
		}
	}

	public function get_user_details() {
		$this->load->model('user/userstorage');
		$this->userstorage->includePending(true);
		$user = $this->userstorage->getById($_POST['id']);
		if ($user) {
			echo json_encode(array(
				'alert_type'=>'Success',
				'user_info'	=>array(
					// 'id' 			=> $user->getUser()->getId(),
					'firstname' 	=> $user->getUser()->getFirstname(),
					'middlename' 	=> $user->getUser()->getMiddlename(),
					'lastname' 		=> $user->getUser()->getLastname(),
					'username' 		=> $user->getUser()->getUsername(),
					'company'		=> $user->getUser()->getCompany(),
					'phone_number' 	=> $user->getUser()->getPhone(),
					'emergency_phone_number' 	=> json_decode($user->getUser()->getEmergencyphone()),
					'emergency_name' 	=> json_decode($user->getUser()->getEmergencyname()),
					'country' 		=> $user->getUser()->getCountry(),
					'email' 		=> $user->getUser()->getEmail(),
					'alternative_email' 		=> $user->getUser()->getAlteremail(),
					'salutation' 	=> $user->getUser()->getSalutation(),
					'dob' 			=> $user->getUser()->getDateOfBirth(),
					'user_type' 	=> strtolower(get_class($user))
				),
			));
		} else {
			echo json_encode(array(
				'alert_type'=>'Error',
				'message'	=>'Can\'t retrieve user information.'
			));
		}
	}

	/**
	 *
	 */
	public function register_package() {
		$conc = '';
		$this->load->model('package/package');
		$this->load->model('package/destination');
		$this->load->model('package/packagestorage');
		$this->load->model('user/user');
		$this->load->model('user/userstorage');

		$this->userstorage->includePending(true);
		$admin = $this->userstorage->getById($this->input->post('package_admin'));
		$provider = $this->userstorage->getById($this->input->post('package_provider'));
		$path = '';
		if ($admin) {
			if ($provider) {

				$destinations = $this->input->post('destination');
				$nights 	  = $this->input->post('number_of_nights');
				$code 	  	  = $destinations[0].time();

				$this->package->setProvider($provider);
				$this->package->setAdmin($admin);
				$this->package->setTitle($this->input->post('package_title'));
				$this->package->setDescription($this->input->post('package_description'));
				$this->package->setTermsAndConditions($this->input->post('terms_and_conditions'));
				$this->package->setDateRange($this->input->post('valid_date_from'),$this->input->post('valid_date_until'));
				$this->package->setCode($code);
				$this->package->setStatus('active');
				/*$this->package->getImage_price($mathefucker);*/
				$this->package->setNights(array_sum($nights));

				foreach ($destinations as $k=>$dest) {
					$destination = new Destination();
					$destination->setCountryCode($dest);
					$destination->setNights($nights[$k]);
					$destination->setStatus('active');
					$this->package->addDestination($destination);
				}

				$filename_str = str_replace(" ","_",package_filename($this->package, $_FILES['userfile']['name']));
				if ($_FILES['userfile']['name'] != ""){
					//$path = 'uploads/package_images/' . str_replace(" ","_",package_filename($this->package, $_FILES['userfile']['name']));
					$path = 'uploads/package_images/' . $filename_str;
				}
				if ($path != ''){
					$this->package->setImagePath($path);
				}

				if($_FILES['file']['name'] != ""){
					$search_replace = array(' ','/','<','>',',');
					for ($i=0; $i < count($_FILES['file']['name']); $i++) {
						$name= str_replace($search_replace,"_",@$_FILES['file']['name'][$i]);
						$wang=time().$i.$name;
						$size = @$_FILES['file']['size'][$i];
						$tmp_name  = @$_FILES['file']['tmp_name'][$i];
				   		$allowed_ext = 'pdf|xls|csv|docx';
					    $max_size = '20000000';
					    $destination = 'uploads/package_prices/';
						 if($this->file_upload($wang,$size,$tmp_name,$allowed_ext,$max_size,$destination)){
						 	echo "";}
							$price_name[]=$wang;
					}
					$array_pricename=implode(',',$price_name);
		     	}
		     	if ($this->package->prepared()) {
					$this->packagestorage->store($this->package,@$array_pricename);
					if ($this->package->stored()) {
						//upload photo
						if (@$_FILES['userfile']['name'] != "") {
							$ext = end(explode('.', $filename_str));
							$config['upload_path'] = 'uploads/package_images';
							$config['allowed_types'] = 'png|jpg|jpeg';
							$config['max_size'] = '786432';
							$config['file_name'] = $filename_str;
							$this->load->library('upload', $config);
							if ($this->upload->do_upload()) {
								$data = array('upload_data'=>$this->upload->data());
								$this->resize($data['upload_data']['full_path']);
								$conc = ' successfully uploaded.';
										echo json_encode(array(
									'alert_type'=>'Success',
									'message'=>'Image'.$conc
								));
							} else {
								echo json_encode(array(
									'alert_type'=>'Error',
									'message'=>$this->upload->display_errors()
								));
								return false;
							}
						}else{
							echo json_encode(array(
								'alert_type'=>'Success'
							));
						}
					} else {
						echo json_encode(array(
							'alert_type'=>'Error',
							'message'=>'An error has occured. Package not created. Please try again.'
						));
					}
				} else {
					echo json_encode(array(
						'alert_type'=>'Error',
						'message'=>'Please fill in required fields. Check if date is not same as date today.'
					));
				}

			} else {
				echo json_encode(array(
					'alert_type'=>'Error',
					'message'=>'Please select a provider.'
				));
			}
		} else {
			echo json_encode(array(
				'alert_type'=>'Error',
				'message'=>'Current user is not an administrator. You cannot create this package.'
			));
		}

	}

	public function update_package() {

		//dump($_POST);
		//dump($this->input->post('pricefile'));
		//dump($_FILES);
		//dump($_FILES['fckshit']['name']);
		//die;

		$this->load->model('package/package');
		$this->load->model('package/packagestorage');
		$this->load->model('package/destination');
		$this->load->model('user/user');
		$this->load->model('user/userstorage');
		$this->load->helper('file');
		$conc = '';
		$old_path = '';
		$path = '';
		$photo_path = false;
		$isSuccess = false;

		$this->userstorage->includePending(true);
		$admin = $this->userstorage->getById($this->input->post('package_admin'));
		$provider = $this->userstorage->getById($this->input->post('package_provider'));
		$packageId = (!is_numeric($this->input->post('package_id')))?:$this->input->post('package_id');

		if ($this->input->post('package_image'))
			$old_path = './'.$this->input->post('package_image');

		if ($admin) {
			if ($provider) {
				if ($packageId) {
					$packageId = intval($packageId);

					$dest_id 	  = $this->input->post('dest_id');
					$destinations = $this->input->post('destination');
					$nights 	  = $this->input->post('number_of_nights');
					$updatePackages = $this->packagestorage->getById($packageId);
					$updatePackage 	= $updatePackages[0];
					$updatePackage->captureCurrentState();
					$updatePackage->setProvider($provider);
					$updatePackage->setTitle($this->input->post('package_title'));
					$updatePackage->setDescription($this->input->post('package_description'));
					$updatePackage->setTermsAndConditions($this->input->post('terms_and_conditions'));
					$updatePackage->setDateRange($this->input->post('valid_date_from'), $this->input->post('valid_date_until'));
					$updatePackage->setNights(array_sum($nights));

					$updatePackage->clearDestinations();

					//new package destinations
					foreach ($destinations as $k=>$dest) {
						// if ($this->destination->checkForDestination($packageId, $dest)->num_rows() == 0) {
						$r = $this->db->get_where('package_destinations', array('package_id'=>$packageId,'country_code'=>$dest))->row();

						if (!$r) {
							$updateDestination = new Destination();
							$updateDestination->setPackage($updatePackage);
						} else
							$updateDestination = $this->destination->getById($r->id);

						$updateDestination->setCountryCode($dest);
						$updateDestination->setNights($nights[$k]);
						$updateDestination->setStatus('active');
						$updatePackage->addDestination($updateDestination);
					}

					$price_name=array();
					if(@$_FILES['fckshit']['name'] != NULL){
						if ($this->input->post('old_price')) {
							foreach ($this->input->post('old_price') as $key) {
								$filename = 'uploads/package_prices/'.$key;
								if (file_exists($filename)) {
									unlink('uploads/package_prices/'.$key);
								}
							}
						}

						$search_replace = array(' ','/','<','>',',');
						for ($i=0; $i < count($_FILES['fckshit']['name']); $i++) {
						$name= str_replace($search_replace,"_",@$_FILES['fckshit']['name'][$i]);
						$wang=time().$i.$name;
						$size = @$_FILES['fckshit']['size'][$i];
						$tmp_name  = @$_FILES['fckshit']['tmp_name'][$i];
				   		$allowed_ext = 'pdf|xls|csv|docx|jpg';
					    $max_size = '20000000';
					    $destination = 'uploads/package_prices/';
						    if($this->file_upload($wang,$size,$tmp_name,$allowed_ext,$max_size,$destination)){
						    	echo "";}
							$price_name[]=$wang;
						}
						$array_pricename=implode(',',$price_name);
		     		}
					//insert first the package img and set image path here

					$filename_str = str_replace(" ","_",package_filename($this->package,@$_FILES['userfile']['name']));
					if (isset($_FILES['userfile']) && $_FILES['userfile']['name'] != NULL) {
						$string = read_file($old_path);
						if ($string) unlink($old_path);
						$ext = end(explode('.', $_FILES['userfile']['name']));
						$config['upload_path'] = './uploads/package_images';
						$config['allowed_types'] = 'pdf|jpg|jpeg';
						$config['max_size'] = '786432';
						$config['file_name'] = $filename_str;
						$this->load->library('upload', $config);
						if ($this->upload->do_upload()) {
							$data = array('upload_data'=>$this->upload->data());
							$this->resize($data['upload_data']['full_path']);
							$path = 'uploads/package_images/'. $filename_str;
							$photo_path = base_url('../'.$path);
							$conc = 'Image successfully uploaded.';
							$updatePackage->setImagePath($path);
						} else {
							echo json_encode(array(
								'alert_type'=>'Error',
								'message'=>$this->upload->display_errors()
							));
							return false;
						}
					}
					$this->packagestorage->store($updatePackage,@$array_pricename);

					$return = array(
						'alert_type'=>'Success',
						'message'=>'Package updated. '.$conc);
					$this->session->set_flashdata($return);

					//if photo upload submitted
					if ($photo_path) {
						$return['src_path'] = $photo_path;
						$return['db_path'] = $path;
					}

					//echo json_encode($return);
					redirect('panel/package_edit/'.$packageId);
					//header('Location:'.base_url('panel/package_edit/'.$packageId));

				} else {
					echo json_encode(array(
						'alert_type'=>'Error',
						'message'	=>'Invalid Package (ID).'
					));
				}
			} else {
				echo json_encode(array(
					'alert_type'=>'Error',
					'message'=>'Please select a provider.'
				));
			}
		} else {
			echo json_encode(array(
				'alert_type'=>'Error',
				'message'=>'Current user is not an administrator. You cannot create this package.'
			));
		}
	}
	/**
	 * Registers new hotel into the database.
	 */
	public function register_hotel() {

		$this->load->model('hotel/hotel');
		$this->load->model('hotel/hotelstorage');

		$this->hotel->setName($this->input->post('hotel_name'));
		$this->hotel->setLocation($this->input->post('hotel_location'));
		$this->hotel->setCountry($this->input->post('hotel_country'));
		$this->hotel->setWebsite($this->input->post('hotel_website'));
		$this->hotel->setPhone($this->input->post('hotel_phone'));
		$this->hotel->setEmail($this->input->post('hotel_email'));
		$this->hotel->setDescription($this->input->post('hotel_description'));
		$this->hotel->setStatus('active');

		if ($this->hotel->prepared()) {
			$this->hotelstorage->store($this->hotel);
			if ($this->hotel->stored()) {
				// $data['hotel'] = $this->hotel;
				echo json_encode(array(
					'alert_type'=>'Success',
				));
			} else
				echo json_encode(array(
					'alert_type'=>'Error',
					'message'=>'Storage Failed. Hotel already exists.'
				));
		} else
			echo json_encode(array(
				'alert_type'=>'Error',
				'message'=>'Required fields cannot be empty.'
			));
	}

	public function get_hotel_details() {
		$this->load->model('hotel/hotelstorage');
		$hotel = $this->hotelstorage->getById($_POST['id']);
		if ($hotel) {
			echo json_encode(array(
				'alert_type'=>'Success',
				'user_info'=>array(
					'hotel_name' => $hotel->getName(),
					'hotel_location' => $hotel->getLocation(),
					'hotel_country' => $hotel->getCountry(),
					'hotel_website' => $hotel->getWebsite(),
					'hotel_phone' => $hotel->getPhone(),
					'hotel_email' => $hotel->getEmail(),
					'hotel_status' => $hotel->getStatus(),
					'hotel_description' => $hotel->getDescription()
				),
			));
		} else {
			echo json_encode(array(
				'alert_type'=>'Error',
				'message'=>'Can\'t retrieve user information.'
			));
		}
	}

	public function update_hotel() {
		//to try the appending of new td
		$this->load->model('hotel/hotelstorage');
		$isSuccess = false;

		$hotelId = (!is_numeric($this->input->post('hotel_id')))?:$this->input->post('hotel_id');
		if ($hotelId) {
			$hotelId = intval($hotelId);
			$updateHotel = $this->hotelstorage->getById($hotelId);
			$updateHotel->setName($this->input->post('hotel_name'));
			$updateHotel->setLocation($this->input->post('hotel_location'));
			$updateHotel->setCountry($this->input->post('hotel_country'));
			$updateHotel->setWebsite($this->input->post('hotel_website'));
			$updateHotel->setPhone($this->input->post('hotel_phone'));
			$updateHotel->setEmail($this->input->post('hotel_email'));
			$updateHotel->setDescription($this->input->post('hotel_description'));
			$this->hotelstorage->store($updateHotel);

			$isSuccess = ($updateHotel->getName() == $updateHotel->prevState()->getName()
				&& $updateHotel->getLocation() == $updateHotel->prevState()->getLocation()
				&& $updateHotel->getCountry() == $updateHotel->prevState()->getCountry()
				&& $updateHotel->getWebsite() == $updateHotel->prevState()->getWebsite()
				&& $updateHotel->getPhone() == $updateHotel->prevState()->getPhone()
				&& $updateHotel->getEmail() == $updateHotel->prevState()->getEmail()
				&& $updateHotel->getDescription() == $updateHotel->prevState()->getDescription());

			if ($isSuccess) {
				$data['hotel'] = $updateHotel;
				echo "Success_:_Update successful._:_".$_POST['hotel_id']."_:_".$this->load->view('panel/mainpanel/append_hotel_details', $data);
			} else echo "Error_:_Update Failed.";
		} else echo "Error_:_The hotel id seem to be missing.";

	}

	public function register_hotelsurcharge() {
		$hotelId = $this->input->post('hotel_id');

		if (!is_numeric($hotelId)) return;

		$this->load->model('hotel/hotelstorage');
		$this->load->model('surcharge/surcharge');

		$hotel = $this->hotelstorage->getById(intval($hotelId));

		if ($hotel) {
			$data = $this->input->post();
			if ($data['hotelsurcharge_ruletype'] == 'date_range')
				$rule = $data['hotelsurcharge_rule_range_1'].' to '.$data['hotelsurcharge_rule_range_2'];
			elseif ($data['hotelsurcharge_ruletype'] == 'day_of_week')
				$rule = $data['hotelsurcharge_rule_day'];
			elseif ($data['hotelsurcharge_ruletype'] == 'agent')
				$rule = $data['hotelsurcharge_rule_agent'];
			elseif ($data['hotelsurcharge_ruletype'] == 'blackout')
				$rule = $data['hotelsurcharge_rule_out_1'].' to '.$data['hotelsurcharge_rule_out_2'];
			else {
				echo json_encode(array(
					'alert_type'=>'Error',
					'message'=>'Please specify a valid rule type.'
				));
				return;
			}

			$this->surcharge->setDescription($data['hotelsurcharge_description']);
			$this->surcharge->setRuletype(strtolower($data['hotelsurcharge_ruletype']));
			$this->surcharge->setRule($rule);
			$this->surcharge->setCost($data['hotelsurcharge_cost']);
			$this->surcharge->setProfit($data['hotelsurcharge_profit']);
			$this->surcharge->setRatetype($data['hotelsurcharge_ratetype']);
			$this->surcharge->setStatus('active');

			$hotel->addSurcharge($this->surcharge);
			$this->hotelstorage->store($hotel);
		}

		if ($this->surcharge->stored()) {
			$data['surcharge'] = $this->surcharge;
			echo 'Success_:_'.$this->load->view('panel/mainpanel/append_hotelsurcharge',$data);
		} else {
			echo 'Error_:_Cannot add hotel surcharge.';
		}
	}

	public function get_hotelsurcharge_details() {
		$this->load->model('surcharge/surchargestorage');
		$this->surchargestorage->setDbName('hotel_surcharges');
		$surcharge = $this->surchargestorage->getById($_POST['id']);

		if ($surcharge) {
			$ruletype = $surcharge->getRuletype();
			if ($ruletype == 'date_range') {
				$rule_details = explode(' to ', $surcharge->getRule());
				$surcharge_info['hotelsurcharge_rule_range_1'] = $rule_details[0];
				$surcharge_info['hotelsurcharge_rule_range_2'] = $rule_details[1];
			} elseif ($ruletype == 'blackout') {
				$rule_details = explode(' to ', $surcharge->getRule());
				$surcharge_info['hotelsurcharge_rule_out_1'] = $rule_details[0];
				$surcharge_info['hotelsurcharge_rule_out_2'] = $rule_details[1];
			} elseif ($ruletype == 'day_of_week') {
				$surcharge_info['hotelsurcharge_rule_day'] = $surcharge->getRule();
			} elseif ($ruletype == 'agent') {
				$surcharge_info['hotelsurcharge_rule_agent'] = $surcharge->getRule();
				//Kirst, do me
				// $rule = $data['roomratesurcharge_rule_agent'];
			   	// $agent = $this->db->get_where('users',array('id'=>$rule))->result_array();
			   	// $agent_name = $agent[0]['lastname'].', '.$agent[0]['firstname'];
			}

			$surcharge_info['hotelsurcharge_description'] = $surcharge->getDescription();
			$surcharge_info['hotelsurcharge_ruletype'] = $ruletype;
			$surcharge_info['hotelsurcharge_cost'] = $surcharge->getCost();
			$surcharge_info['hotelsurcharge_profit'] = $surcharge->getProfit();
			$surcharge_info['hotelsurcharge_ratetype'] = $surcharge->getRatetype();

			echo json_encode(array(
				'alert_type'=>'Success',
				'surcharge_info'=>$surcharge_info
			));
		} else {
			echo json_encode(array(
				'alert_type'=>'Error',
				'message'=>'Can\'t retrieve surcharge details.'
			));
		}
	}

	public function update_hotelsurcharge() {
		$hotelId = $this->input->post('hotel_id');
		if (!is_numeric($hotelId)) return;

		$surchargeId = $this->input->post('hotelsurcharge_id');
		if (!is_numeric($surchargeId)) return;

		$this->load->model('hotel/hotelstorage');
		$this->load->model('surcharge/surchargestorage');

		$hotel = $this->hotelstorage->getById(intval($hotelId));
		$this->surchargestorage->setDbName('hotel_surcharges');
		$updateSurcharge = $this->surchargestorage->getById(intval($surchargeId));

		if ($updateSurcharge) {
			$data = $this->input->post();
			if ($data['hotelsurcharge_ruletype'] == 'date_range')
				$rule = $data['hotelsurcharge_rule_range_1'].' to '.$data['hotelsurcharge_rule_range_2'];
			elseif ($data['hotelsurcharge_ruletype'] == 'day_of_week')
				$rule = $data['hotelsurcharge_rule_day'];
			elseif ($data['hotelsurcharge_ruletype'] == 'agent')
				$rule = $data['hotelsurcharge_rule_agent'];
			elseif ($data['hotelsurcharge_ruletype'] == 'blackout')
				$rule = $data['hotelsurcharge_rule_out_1'].' to '.$data['hotelsurcharge_rule_out_2'];
			else {
				echo json_encode(array(
					'alert_type'=>'Error',
					'message'=>'Please specify a valid rule type.'
				));
				return false;
			}

			$updateSurcharge->setDescription($data['hotelsurcharge_description']);
			$updateSurcharge->setRuletype(strtolower($data['hotelsurcharge_ruletype']));
			$updateSurcharge->setRule($rule);
			$updateSurcharge->setCost($data['hotelsurcharge_cost']);
			$updateSurcharge->setProfit($data['hotelsurcharge_profit']);
			$updateSurcharge->setRatetype($data['hotelsurcharge_ratetype']);
			$updateSurcharge->setSurchargeOf($hotel);

			$prev = $updateSurcharge->prevState();

			$isSuccess = !(
				   $updateSurcharge->getDescription() 	== $prev->getDescription()
				&& $updateSurcharge->getRuletype() 		== $prev->getRuletype()
				&& $updateSurcharge->getRule() 			== $prev->getRule()
				&& $updateSurcharge->getCost() 			== $prev->getCost()
				&& $updateSurcharge->getProfit() 		== $prev->getProfit()
				&& $updateSurcharge->getRatetype() 		== $prev->getRatetype()
			);

		}

		if ($isSuccess) {
			$this->surchargestorage->store($updateSurcharge);

			$data['surcharge'] = $updateSurcharge;
			echo 'Success_:_'.$this->load->view('panel/mainpanel/append_hotelsurcharge', $data);
		} else {
			echo 'Error_:_No updates to be saved.';
		}
	}

	function resize($path) {
		$config['image_library'] = 'gd2';
		$config['source_image']	= $path;
		$config['maintain_ratio'] = TRUE;
		$config['width']	= 1024;
		$config['height']	= 768;

		$this->load->library('image_lib', $config);

		if (!$this->image_lib->resize()) {
			echo json_encode(array(
				'alert_type'=>'Error',
				'message'=>$this->image_lib->display_errors()
			));
			return false;
		}
	}

	public function email_confirmation($link='', User $user) {
     	if (!$user instanceof User) return;

	    $data['email'] = $user->getEmail();
	    $data['firstname'] = $user->getFirstname();
	    $data['lastname'] = $user->getLastname();
	    $data['link'] = $link;

		$mail = $this->mail;
		$mail->setFrom('no-reply@ge.com.sg', 'Global Explorer');							// Sender
	    $mail->addAddress($data['email'], $data['firstname'].' '.$data['lastname']);		// Add a recipient

	    $mail->WordWrap = 50;																// Set word wrap to 50 characters
	    $mail->isHTML(true);																// Set email format to HTML
	    $mail->Subject = 'Email Confirmation [Global Explorer]';
	    $mail->Body    = $this->load->view('email_confirmation',$data,TRUE);

    	$mail->AltBody = 'Email Confirmation [Global Explorer]';

	    if (!$mail->send()) return false;
	    else return true;
    }

    public function add_packagehotels() {
    	$this->load->model('hotel/hotelstorage');
    	$package_id = $this->input->post('package_id_hotel');
    	$hotel_choice = $this->input->post('hotel_choice');
    	$result_chk = $this->hotelstorage->storePackagehotelsChk($package_id,$hotel_choice);
    	if($result_chk == false){
	    	$result = $this->hotelstorage->storePackagehotels($package_id,$hotel_choice);
	    	if (is_int($result)) {
	    		$data['package_hotel'] = $this->hotelstorage->getPackageHotelById($result);
	    		$data['packageid'] = $package_id;
	    		echo "Success_:_Successfully added Hotel to package._:_".$this->load->view('panel/mainpanel/append_packagehotel',$data);
	    	}else{
	    		echo "";
		    }
		}else{
			$data['package_hotel'] = $this->hotelstorage->getPackageHotelById($result_chk->id);
    		$data['packageid'] = $package_id;
    		echo "Success_:_Successfully added Hotel to package._:_".$this->load->view('panel/mainpanel/append_packagehotel',$data);

		}
    }

    public function add_packageaddons() {
     	$this->load->model('package/addon');

    	$addon['package_id'] = $this->input->post('package_id');
    	$addon['description'] = $this->input->post('addon_description');
    	$addon['unit'] = $this->input->post('addon_unit');
    	$addon['cost'] = $this->input->post('addon_cost');
     	$addon['profit'] = $this->input->post('addon_profit');
     	$addon['status'] = 'active';

    	$result=$this->addon->storepackageaddons($addon);
     	if (is_int($result)) {
    		$addon['id'] = $result;
    		$data['addon'] = $addon;
    		echo "Success_:_Successfully added Add On to package._:_".$this->load->view('panel/mainpanel/append_packageaddon', $data);
    	} else echo "Error_:_".$result;
    }

    //edit this tomorrow
    public function get_addon_details() {
     	$this->load->model('package/addon');
    	$id = $this->input->post('id');
		if (!$id) {
			echo json_encode(array(
				'alert_type'=>'Error',
				'message'=>'Invalid addon ID.'
			));
			return false;
		}

		$addon = $this->addon->getAddons_Id($id);
    	if ($addon) {
			$addon_info['addon_description'] = $addon->description;
			$addon_info['addon_unit'] 		= $addon->unit;
			$addon_info['addon_cost'] 		= $addon->cost;
			$addon_info['addon_profit'] 		= $addon->profit;

			echo json_encode(array(
				'alert_type'=>'Success',
				'info'=>$addon_info
			));
    	} else {
			echo json_encode(array(
				'alert_type'=>'Error',
				'message'=>'Can\'t retrieve addon details.'
			));
		}
    }

    public function update_addon() {
    	$this->load->model('package/addon');
    	$id = $this->input->post('addon_id');

    	if ($id) {
	    	$updateaddon = $this->addon->getAddons_Id($id);
	    	if ($updateaddon) {
	    		$updateaddon->prevState		= clone $updateaddon;
	    		$updateaddon->unit 			= $this->input->post('addon_unit');
	    		$updateaddon->description 	= $this->input->post('addon_description');
	    		$updateaddon->cost 			= $this->input->post('addon_cost');
	    		$updateaddon->profit 		= $this->input->post('addon_profit');

	    		$prev = $updateaddon->prevState;
				$isChanged = !(
					   $updateaddon->unit 			== $prev->unit
					&& $updateaddon->description 	== $prev->description
					&& $updateaddon->cost			== $prev->cost
					&& $updateaddon->profit			== $prev->profit
				);

	    		if ($isChanged) {
	    			$this->addon->storepackageaddons($updateaddon);
		    		$data['addon'] =	(array) $updateaddon;
			    	unset($data['addon']['prevState']);

		    		echo "Success_:_Successfully updated addon._:_".$this->load->view('panel/mainpanel/append_packageaddon', $data);
		   		} else echo "Error_:_No changes to be saved.";
		    } else echo "Error_:_No addon found.";
	    } else echo "Error_:_Can't retrieve id.";
    }

    public function add_hotelroomrate() {
    	$this->load->model('hotel/hotelroomrate');
    	$hotelroomrate['package_hotel_id'] 	= $this->input->post('parent_id');
    	$hotelroomrate['room_type']			= $this->input->post('hotelroomrate_name');
    	$hotelroomrate['rate_type'] 		= $this->input->post('hotelroomrate_type');
     	$hotelroomrate['description'] 		= $this->input->post('hotelroomrate_description');
    	$hotelroomrate['cost'] 				= ($this->input->post('hotelroomrate_cost')) ? $this->input->post('hotelroomrate_cost') : 0;
     	$hotelroomrate['profit']			= ($this->input->post('hotelroomrate_profit')) ? $this->input->post('hotelroomrate_profit') : 0;
    	$hotelroomrate['ext_cost'] 			= ($this->input->post('hotelroomrate_cost_ext')) ? $this->input->post('hotelroomrate_cost_ext') : 0;
     	$hotelroomrate['ext_profit']		= ($this->input->post('hotelroomrate_profit_ext')) ? $this->input->post('hotelroomrate_profit_ext') : 0;

    	if ($this->input->post('hotelroomrate_name') == 'twin') {
    		$hotelroomrate['pax'] = 2;
    	} else if ($this->input->post('hotelroomrate_name') == 'triple') {
    		$hotelroomrate['pax'] = 3;
    	} else if ($this->input->post('hotelroomrate_name') == 'child_half_twin') {
    	 	$hotelroomrate['pax'] = 1;
    	} else if ($this->input->post('hotelroomrate_name') == 'quadruple') {
    	 	$hotelroomrate['pax'] = 4;
    	} else {
    		$hotelroomrate['pax'] = 1;
    	}

    	$result = $this->hotelroomrate->storehotelroomrate($hotelroomrate);
     	if (is_int($result)) {
    		$hotelroomrate['id'] = $result;
    		$data['roomrate'] =	$hotelroomrate;
    		echo "Success_:_Successfully added hotel room rate to package._:_".$this->load->view('panel/mainpanel/append_packagehotel_roomrates', $data);
    	} else echo "Error_:_".$result;
    }

    public function get_hotelroomrate_details() {
		$this->load->model('hotel/hotelroomrate');
		$id = $this->input->post('id');
		if (!$id) return false;

		$roomrate = $this->hotelroomrate->getHotelRoomRate_Id($id);
		if ($roomrate) {
			$roomrate_info['id'] 						= $roomrate->id;
			$roomrate_info['package_hotel_id'] 			= $roomrate->package_hotel_id;
			$roomrate_info['hotelroomrate_description'] = $roomrate->description;
			$roomrate_info['hotelroomrate_name'] 		= $roomrate->room_type;
			$roomrate_info['hotelroomrate_type']		= $roomrate->rate_type;
			$roomrate_info['hotelroomrate_cost'] 		= $roomrate->cost;
			$roomrate_info['hotelroomrate_cost_ext'] 	= $roomrate->ext_cost;
			$roomrate_info['hotelroomrate_profit'] 		= $roomrate->profit;
			$roomrate_info['hotelroomrate_profit_ext'] 	= $roomrate->ext_profit;

			echo json_encode(array(
				'alert_type'=>'Success',
				'info'=>$roomrate_info
			));

		} else {
			echo json_encode(array(
				'alert_type'=>'Error',
				'message'=>'Can\'t retrieve surcharge details.'
			));
		}
	}

    public function update_hotelroomrate() {
    	$this->load->model('hotel/hotelroomrate');
    	//parent id here is the id of the of the record
    	$id = $this->input->post('parent_id');
    	if ($id) {
	    	$updateroomrate = $this->hotelroomrate->getHotelRoomRate_Id($id);
	    	if ($updateroomrate) {
	    		$updateroomrate->prevState	= clone $updateroomrate;
	    		$updateroomrate->room_type 	= $this->input->post('hotelroomrate_name');
	    		$updateroomrate->description= $this->input->post('hotelroomrate_description');
	    		$updateroomrate->rate_type 	= $this->input->post('hotelroomrate_type');
	    		$updateroomrate->cost 		= $this->input->post('hotelroomrate_cost');
	    		$updateroomrate->profit 	= $this->input->post('hotelroomrate_profit');
	    		$updateroomrate->ext_cost 	= $this->input->post('hotelroomrate_cost_ext');
	    		$updateroomrate->ext_profit = $this->input->post('hotelroomrate_profit_ext');

	    		if (strtolower($this->input->post('hotelroomrate_name')) == 'triple')
		    		 $updateroomrate->pax = 3;
	    		else if (strtolower($this->input->post('hotelroomrate_name')) == 'twin')
		    		 $updateroomrate->pax = 2;
		    	else if (strtolower($this->input->post('hotelroomrate_name')) == 'child_half_twin')
		    		 $updateroomrate->pax = 2;
		    	else $updateroomrate->pax = 1;

	    		$prev = $updateroomrate->prevState;

		    	$isChanged = !(
					   $updateroomrate->room_type 	== $prev->room_type
					&& $updateroomrate->description == $prev->description
					&& $updateroomrate->rate_type 	== $prev->rate_type
					&& $updateroomrate->cost 		== $prev->cost
					&& $updateroomrate->profit 		== $prev->profit
					&& $updateroomrate->ext_cost	== $prev->ext_cost
					&& $updateroomrate->ext_profit	== $prev->ext_profit
					&& $updateroomrate->pax			== $prev->pax
				);

		    	if ($isChanged) {
		    		$this->hotelroomrate->storehotelroomrate($updateroomrate);
		    		$data['roomrate'] =	(array) $updateroomrate;
			    	unset($data['roomrate']['prevState']);

		    		echo "Success_:_Successfully updated hotel room rate._:_".$this->load->view('panel/mainpanel/append_packagehotel_roomrates', $data);
			    } else echo "Error_:_No changes to be saved.";
		    } else echo "Error_:_No room rate found.";
	    } else echo "Error_:_Can't retrieve id.";
    }

    public function add_hotelroomsurcharge() {
    	$this->load->database();
	   	$data = $this->input->post();
	   	if ($data['roomratesurcharge_ruletype'] == 'date_range')
	    	$rule = $data['roomratesurcharge_rule_range_1'].' to '.$data['roomratesurcharge_rule_range_2'];
	   	elseif ($data['roomratesurcharge_ruletype'] == 'day_of_week')
	    	$rule = $data['roomratesurcharge_rule_day'];
	   	elseif ($data['roomratesurcharge_ruletype'] == 'agent') {
	    	$rule = $data['roomratesurcharge_rule_agent'];
	    	$agent = $this->db->get_where('users',array('id'=>$rule))->result_array();
	    	$agent_name = $agent[0]['lastname'].', '.$agent[0]['firstname'];
	   	} elseif ($data['roomratesurcharge_ruletype'] == 'blackout')
	    	$rule = $data['roomratesurcharge_rule_out_1'].' to '.$data['roomratesurcharge_rule_out_2'];
	   	else {
	   		echo "Error_:_Please specify a valid rule type";
	   		return false;
	   	}

    	$this->load->model('hotel/hotelroomrate');

    	$roomsurcharge['hotelroom_id'] 	= $this->input->post('parent_id');
    	$roomsurcharge['description'] 	= $this->input->post('roomratesurcharge_description');
    	$roomsurcharge['rule_type'] 	= $this->input->post('roomratesurcharge_ruletype');
     	$roomsurcharge['rule'] 			= $rule;
     	$roomsurcharge['cost'] 			= $this->input->post('roomratesurcharge_cost');
     	$roomsurcharge['profit'] 		= $this->input->post('roomratesurcharge_profit');
     	$roomsurcharge['rate_type'] 	= $this->input->post('roomratesurcharge_ratetype');
     	$roomsurcharge['status'] 		= 'active';
     	$roomsurcharge['timestamp'] 	= time();
    	$result = $this->hotelroomrate->hotelroomsurcharge($roomsurcharge);
     	if (is_int($result)) {
     		$roomsurcharge['id'] = $result;
     		if (isset($agent_name))
     			$roomsurcharge['rule'] = $agent_name;
    		$data['roomsurcharge'] =$roomsurcharge;
    		echo "Success_:_Successfully added hotel room surcharge to package._:_".$this->load->view('panel/mainpanel/append_packagehotel_roomsurcharge', $data);
    	} else echo "Error_:_".$result;
    }

    public function get_hotelroomsurcharge_details() {
		$this->load->model('surcharge/surchargestorage');
		$this->surchargestorage->setDbName('hotelroom_surcharges');
		$id = $this->input->post('id');
		if (!$id) return false;

		$surcharge = $this->surchargestorage->getById($id);

		if ($surcharge) {
			$ruletype = $surcharge->getRuletype();
			if ($ruletype == 'date_range') {
				$rule_details = explode(' to ', $surcharge->getRule());
				$surcharge_info['roomratesurcharge_rule_range_1'] = $rule_details[0];
				$surcharge_info['roomratesurcharge_rule_range_2'] = $rule_details[1];
			} elseif ($ruletype == 'blackout') {
				$rule_details = explode(' to ', $surcharge->getRule());
				$surcharge_info['roomratesurcharge_rule_out_1'] = $rule_details[0];
				$surcharge_info['roomratesurcharge_rule_out_2'] = $rule_details[1];
			} elseif ($ruletype == 'day_of_week') {
				$surcharge_info['roomratesurcharge_rule_day'] = $surcharge->getRule();
			} elseif ($ruletype == 'agent') {
				$surcharge_info['roomratesurcharge_rule_agent'] = $surcharge->getRule();
				//Kirst, do me
				// $rule = $data['roomratesurcharge_rule_agent'];
			   	// $agent = $this->db->get_where('users',array('id'=>$rule))->result_array();
			   	// $agent_name = $agent[0]['lastname'].', '.$agent[0]['firstname'];
			}

			$surcharge_info['roomratesurcharge_description'] 	= $surcharge->getDescription();
			$surcharge_info['roomratesurcharge_ruletype'] 		= $ruletype;
			$surcharge_info['roomratesurcharge_cost'] 			= $surcharge->getCost();
			$surcharge_info['roomratesurcharge_profit'] 		= $surcharge->getProfit();
			$surcharge_info['roomratesurcharge_ratetype'] 		= $surcharge->getRatetype();

			echo json_encode(array(
				'alert_type'=>'Success',
				'info'=>$surcharge_info
			));
		} else {
			echo json_encode(array(
				'alert_type'=>'Error',
				'message'=>'Can\'t retrieve surcharge details.'
			));
		}
	}

	public function update_hotelroomsurcharge() {
		$this->load->model('surcharge/surchargestorage');
		$data = $this->input->post();
		$id = $data['parent_id'];

		$updateSurcharge = $this->surchargestorage->getSurcharge_id('hotelroom_surcharges', intval($id));

		if ($updateSurcharge) {
			$updateSurcharge->prevState = clone $updateSurcharge;

			if ($data['roomratesurcharge_ruletype'] == 'date_range')
				$rule = $data['roomratesurcharge_rule_range_1'].' to '.$data['roomratesurcharge_rule_range_2'];
			elseif ($data['roomratesurcharge_ruletype'] == 'day_of_week')
				$rule = $data['roomratesurcharge_rule_day'];
			elseif ($data['roomratesurcharge_ruletype'] == 'agent') {
				$rule = $data['roomratesurcharge_rule_agent'];
				$agent = $this->db->get_where('users',array('id'=>$rule))->result_array();
	    		$agent_name = $agent[0]['lastname'].', '.$agent[0]['firstname'];
			} elseif ($data['roomratesurcharge_ruletype'] == 'blackout')
				$rule = $data['roomratesurcharge_rule_out_1'].' to '.$data['roomratesurcharge_rule_out_2'];
			else {
				echo json_encode(array(
					'alert_type'=>'Error',
					'message'=>'Please specify a valid rule type.'
				));
				return false;
			}

			$updateSurcharge->description 	= $data['roomratesurcharge_description'];
			$updateSurcharge->rule_type 	= strtolower($data['roomratesurcharge_ruletype']);
			$updateSurcharge->rule 			= $rule;
			$updateSurcharge->cost 			= $data['roomratesurcharge_cost'];
			$updateSurcharge->profit 		= $data['roomratesurcharge_profit'];
			$updateSurcharge->rate_type 	= $data['roomratesurcharge_ratetype'];

			$prev = $updateSurcharge->prevState;

	    	$isChanged = !(
				   $updateSurcharge->description 	== $prev->description
				&& $updateSurcharge->rule_type 		== $prev->rule_type
				&& $updateSurcharge->rule 			== $prev->rule
				&& $updateSurcharge->cost 			== $prev->cost
				&& $updateSurcharge->profit 		== $prev->profit
				&& $updateSurcharge->rate_type		== $prev->rate_type
			);

	    	if ($isChanged) {
				$this->surchargestorage->edit_HotelRoomSurcharge($updateSurcharge);

	     		if (isset($agent_name))
	     			$updateSurcharge->rule = $agent_name;
				$data['roomsurcharge'] = (array) $updateSurcharge;
		    	unset($data['roomsurcharge']['prevState']);
	    		echo "Success_:_Successfully added hotel room surcharge to package._:_".$this->load->view('panel/mainpanel/append_packagehotel_roomsurcharge', $data);
    		} else echo "Error_:_No changes to be saved";
    	} else echo "Error_:_Surcharge not found";
    }

    public function create_packagesurcharge() {
		$data = $this->input->post();
	   	if ($data['packagesurcharge_ruletype'] == 'date_range')
	    	$rule = $data['packagesurcharge_rule_range_1'].' to '.$data['packagesurcharge_rule_range_2'];
	   	elseif ($data['packagesurcharge_ruletype'] == 'day_of_week')
	    	$rule = $data['packagesurcharge_rule_day'];
	   	elseif ($data['packagesurcharge_ruletype'] == 'agent') {
	   		$rule = $data['packagesurcharge_rule_agent'];
	    	$agent = $this->db->get_where('users',array('id'=>$rule))->result_array();
	    	$agent_name = $agent[0]['lastname'].', '.$agent[0]['firstname'];
	   	} elseif ($data['packagesurcharge_ruletype'] == 'blackout')
	    	$rule = $data['packagesurcharge_rule_out_1'].' to '.$data['packagesurcharge_rule_out_2'];
	   	else {
	   		echo "Error_:_Please specify a valid rule type";
	   		return false;
	   	}

    	$this->load->model('surcharge/surchargestorage');

    	$surcharge['package_id'] 	= $this->input->post('package_id');
    	$surcharge['description'] 	= $this->input->post('packagesurcharge_description');
    	$surcharge['rule_type'] 	= $this->input->post('packagesurcharge_ruletype');
     	$surcharge['rule'] 			= $rule;
     	$surcharge['cost'] 			= $this->input->post('packagesurcharge_cost');
     	$surcharge['profit'] 		= $this->input->post('packagesurcharge_profit');
     	$surcharge['rate_type'] 	= $this->input->post('packagesurcharge_ratetype');
     	$surcharge['status'] 		= 'active';
     	$surcharge['timestamp'] 	= time();
    	$result=$this->surchargestorage->add_packagesurcharge($surcharge);
     	if (is_int($result)) {
     		if (isset($agent_name)) $surcharge['rule'] = $agent_name;
    		$surcharge['id'] = $result;
    		$data['surcharge'] = $surcharge;
    		echo "Success_:_Successfully added hotel room surcharge to package._:_".$this->load->view('panel/mainpanel/append_packagesurcharge', $data);
    	} else echo "Error_:_".$result;
    }

    public function get_packagesurcharge_details() {
		$this->load->model('surcharge/surchargestorage');
		$this->surchargestorage->setDbName('package_surcharges');
		$id = $this->input->post('id');

		if (!$id) return false;

		$surcharge = $this->surchargestorage->getById($id);

		if ($surcharge) {
			$ruletype = $surcharge->getRuletype();
			if ($ruletype == 'date_range') {
				$rule_details = explode(' to ', $surcharge->getRule());
				$surcharge_info['packagesurcharge_rule_range_1'] = $rule_details[0];
				$surcharge_info['packagesurcharge_rule_range_2'] = $rule_details[1];
			} elseif ($ruletype == 'blackout') {
				$rule_details = explode(' to ', $surcharge->getRule());
				$surcharge_info['packagesurcharge_rule_out_1'] = $rule_details[0];
				$surcharge_info['packagesurcharge_rule_out_2'] = $rule_details[1];
			} elseif ($ruletype == 'day_of_week') {
				$surcharge_info['packagesurcharge_rule_day']   = $surcharge->getRule();
			} elseif ($ruletype == 'agent') {
				$surcharge_info['packagesurcharge_rule_agent'] = $surcharge->getRule();
				//Kirst, do me
				// $rule = $data['packagesurcharge_rule_agent'];
			   	// $agent = $this->db->get_where('users',array('id'=>$rule))->result_array();
			   	// $agent_name = $agent[0]['lastname'].', '.$agent[0]['firstname'];
			}

			$surcharge_info['packagesurcharge_description'] = $surcharge->getDescription();
			$surcharge_info['packagesurcharge_ruletype'] 	= $ruletype;
			$surcharge_info['packagesurcharge_cost'] 		= $surcharge->getCost();
			$surcharge_info['packagesurcharge_profit'] 		= $surcharge->getProfit();
			$surcharge_info['packagesurcharge_ratetype'] 	= $surcharge->getRatetype();

			echo json_encode(array(
				'alert_type'=>'Success',
				'info'=>$surcharge_info
			));
		} else {
			echo json_encode(array(
				'alert_type'=>'Error',
				'message'=>'Can\'t retrieve surcharge details.'
			));
		}
	}

	public function update_packagesurcharge() {
		$package_id = $this->input->post('package_id');
		if (!is_numeric($package_id)) return;

		$surchargeId = $this->input->post('pckgsrchrg_id');
		if (!is_numeric($surchargeId)) return;

		$this->load->model('package/packagestorage');
		$this->load->model('surcharge/surchargestorage');

		$package = $this->packagestorage->getById(intval($package_id));
		$this->surchargestorage->setDbName('package_surcharges');
		$updateSurcharge = $this->surchargestorage->getById(intval($surchargeId));

		if ($updateSurcharge) {
			$data = $this->input->post();
			if ($data['packagesurcharge_ruletype'] == 'date_range')
				$rule = $data['packagesurcharge_rule_range_1'].' to '.$data['packagesurcharge_rule_range_2'];
			elseif ($data['packagesurcharge_ruletype'] == 'day_of_week')
				$rule = $data['packagesurcharge_rule_day'];
			elseif ($data['packagesurcharge_ruletype'] == 'agent') {
				$rule = $data['packagesurcharge_rule_agent'];
				$agent = $this->db->get_where('users',array('id'=>$rule))->row();
	    		$agent_name = $agent->lastname.', '.$agent->firstname;
			} elseif ($data['packagesurcharge_ruletype'] == 'blackout')
				$rule = $data['packagesurcharge_rule_out_1'].' to '.$data['packagesurcharge_rule_out_2'];
			else {
				echo json_encode(array(
					'alert_type'=>'Error',
					'message'=>'Please specify a valid rule type.'
				));
				return false;
			}

			$updateSurcharge->setDescription($data['packagesurcharge_description']);
			$updateSurcharge->setRuletype(strtolower($data['packagesurcharge_ruletype']));
			$updateSurcharge->setRule($rule);
			$updateSurcharge->setCost($data['packagesurcharge_cost']);
			$updateSurcharge->setProfit($data['packagesurcharge_profit']);
			$updateSurcharge->setRatetype($data['packagesurcharge_ratetype']);
			$updateSurcharge->setSurchargeOf($package[0]);

			$prev = $updateSurcharge->prevState();

			$isSuccess = !(
				   $updateSurcharge->getDescription() 	== $prev->getDescription()
				&& $updateSurcharge->getRuletype() 		== $prev->getRuletype()
				&& $updateSurcharge->getRule() 			== $prev->getRule()
				&& $updateSurcharge->getCost() 			== $prev->getCost()
				&& $updateSurcharge->getProfit() 		== $prev->getProfit()
				&& $updateSurcharge->getRatetype() 		== $prev->getRatetype()
			);

			if ($isSuccess) {
				$this->surchargestorage->store($updateSurcharge);

				$data = array();

				$data['surcharge']['id'] 			= $updateSurcharge->getId();
				$data['surcharge']['description'] 	= $updateSurcharge->getDescription();
				$data['surcharge']['rule_type'] 	= $updateSurcharge->getRuletype();
				$data['surcharge']['rule'] 			= $updateSurcharge->getRule();
	    		if (isset($agent_name))
	    			$data['surcharge']['rule'] 		= $agent_name;
				$data['surcharge']['cost'] 			= $updateSurcharge->getCost();
				$data['surcharge']['profit'] 		= $updateSurcharge->getProfit();
				$data['surcharge']['rate_type'] 	= $updateSurcharge->getRatetype();

	    		echo "Success_:_Successfully edited package surcharge to package._:_".$this->load->view('panel/mainpanel/append_packagesurcharge', $data);
	    	} else echo "Error_:_No changes have been made";
    	} else echo "Error_:_No surcharge found";
	}

    public function update_permission() {
    	$this->load->model('package/packagestorage');
    	$checker= $this->input->post('checked');
    	$arrdata['package_id'] = $this->input->post('package_id');
    	$arrdata['user_id'] = $this->input->post('agent_id');
	    if ($checker == 'true') {
	    	$result = $this->packagestorage->addPermission($arrdata);
	     	if ($result === TRUE)
	    		echo json_encode(array(
					'alert_type'=>'Success',
					'message'=>'User permitted.'
			 	));
			else
				echo json_encode(array(
		 		'alert_type'=>'Error',
		 		'message'=>$result
		 	));
	    } elseif ($checker == 'false') {
	    	$result = $this->packagestorage->deletePermission($arrdata);
	     	if ($result === TRUE)
	    		echo json_encode(array(
					'alert_type'=>'Success',
					'message'=>'Successfully restricted.'
			 	));

			else
				echo json_encode(array(
			 		'alert_type'=>'Error',
			 		'message'=>$result
			 	));
	    }
    }

    public function update_permission_all() {
    	$this->load->model('package/packagestorage');
    	$checker= $this->input->post('checked');
    	$agent_ids= $this->input->post('agent_ids');
    	$package_id= $this->input->post('package_id');

		if ($checker == 'true') {
			$method = 'addPermission';
			$message = 'Permission granted to all agents';
		} else {
			$method = 'deletePermission';
			$message = 'Restriction given to all agents';
		}

    	$arrdata['package_id'] = $this->input->post('package_id');
    	foreach ($agent_ids as $agent_id) {
	    	$arrdata['user_id'] = $agent_id;
	    	$result = $this->packagestorage->$method($arrdata);
    	}

    	echo json_encode(array(
	 		'alert_type'=>'Success',
	 		'message'=>$message
	 	));
    }

    public function create_destination(){
		$this->load->model('others');

		$dest = array('country'=> '', 'code'=>'');
		$country = $this->input->post('country');
   		$city = $this->input->post('city');

   		if((trim($country) == '' || trim($city) == '')) {
   			echo "Error_:_Please fill in at least one (Country or City)";
			return false;
   		}elseif (trim($country) != '' && trim($city) != '') {
   			$dest['country'] = ($country.' - '.$city);
   		} elseif (($country || $city)) {
   			$dest['country'] = ($country) ? $country : $city;
   		}

   		$dest['code'] = $this->input->post('code');
   		$dest['status'] = 'active';

   		$result = $this->others->storeDestination($dest);

   		if (strlen($result) <= 3) {
    		$data['destination'] = $this->others->getDestinationByCode($result);
    		echo "Success_:_Successfully added destination._:_".$this->load->view('panel/mainpanel/append_destination', $data);
    	} else echo "Error_:_".$result;
    }

    public function create_airportcode(){
		$this->load->model('others');

		$dest = array('name'=> '', 'code'=>'');
		$airportcode = $this->input->post('airportcode');
		$code = $this->input->post('code');

   		if(trim($airportcode) == ''){
   			echo "Error_:_Please type in an Airport/City";
   			return false;
   		}if(trim($code) == ''){
   			echo "Error_:_Please type in a Code";
   			return false;
   		}

   		$dest['name'] 	= $airportcode;
   		$dest['code'] 	= $code;
   		$dest['status'] = 'active';

   		$result = $this->others->storeAirportcode($dest);

   		if (is_int($result)) {
    		$data['airportcode'] = $this->others->getAirportCodeById($result);
    		echo "Success_:_Successfully added airport/city code._:_".$this->load->view('panel/mainpanel/append_airportcode', $data);
    	} else echo "Error_:_".$result;
    }

    public function edit_airportcode(){
		$this->load->model('others');
		$code = trim($this->input->post('code'));
		$code_active = trim($this->input->post('code_active'));
		$old_code = trim($this->input->post('old_code'));
		$destination = trim($this->input->post('name'));
		$new_code = trim($this->input->post('new_code'));
		if($code != ''){
   			$dest['status'] = 'deactivate';
	   		$this->others->storeDestinationAirportcode($dest,$code);
   			echo json_encode(array(
		 		'alert_type'=>'Success',
		 		'message'=>'Deactivate successful.',
		 		'info'=>$dest
		 	));
   		}

   		if($code_active != ''){
   			$dest['status'] = 'active';
	   		$this->others->storeDestinationAirportcode($dest,$code_active);
   			echo json_encode(array(
		 		'alert_type'=>'Success',
		 		'message'=>'Active successful.',
		 		'info'=>$dest
		 	));
   		}
		if ($destination=='') {
   			echo json_encode(array(
		 		'alert_type'=>'Error',
		 		'message'=>'Please type in a name/Destination.'
		 	));
   			return false;
		}

		if ($new_code=='') {
			echo json_encode(array(
		 		'alert_type'=>'Error',
		 		'message'=>'Please type in a code.'
		 	));
		}

		$old = $this->others->getAirportCodeById($old_code);
		$hasChanged = !(
					   $old->name == $destination
					&& $old->code == $new_code
					);

   		if($hasChanged){
			$dest['name'] = $destination;
			$dest['code']	 = $new_code;

	   		$result = $this->others->storeDestinationAirportcode($dest, $old_code);

   			echo json_encode(array(
		 		'alert_type'=>'Success',
		 		'message'=>'Edit successful.',
		 		'info'=>$dest
		 	));
   		}else{
   			echo json_encode(array(
		 		'alert_type'=>'Error',
		 		'message'=>'No changes on destination and code.'
		 	));
   		}
    }

    public function edit_destination(){
		$this->load->model('others');
		$code = trim($this->input->post('code'));
		$code_active = trim($this->input->post('code_active'));
		$old_code = trim($this->input->post('old_code'));
		$destination = trim($this->input->post('country'));
		$new_code = trim($this->input->post('new_code'));

		if($code != ''){
   			$dest['status'] = 'deactivate';
	   		$this->others->storeDestination($dest,$code);
   			echo json_encode(array(
		 		'alert_type'=>'Success',
		 		'message'=>'Deactivate successful.',
		 		'info'=>$dest
		 	));
   		}

   		if($code_active != ''){
   			$dest['status'] = 'active';
	   		$this->others->storeDestination($dest,$code_active);
   			echo json_encode(array(
		 		'alert_type'=>'Success',
		 		'message'=>'Active successful.',
		 		'info'=>$dest
		 	));
   		}

		if ($destination=='') {
   			echo json_encode(array(
		 		'alert_type'=>'Error',
		 		'message'=>'Please type in a Country/Destination.'
		 	));
   			return false;
		}

		if ($new_code=='') {
			echo json_encode(array(
		 		'alert_type'=>'Error',
		 		'message'=>'Please type in a code.'
		 	));
		}

		$old = $this->others->getDestinationByCode($old_code);
		$hasChanged = !(
					   $old->country == $destination
					&& $old->code 	 == $new_code
					);

   		if($hasChanged){
			$dest['country'] = $destination;
			$dest['code']	 = $new_code;

	   		$result = $this->others->storeDestination($dest, $old_code);

   			echo json_encode(array(
		 		'alert_type'=>'Success',
		 		'message'=>'Edit successful.',
		 		'info'=>$dest
		 	));
   		}else{
   			echo json_encode(array(
		 		'alert_type'=>'Error',
		 		'message'=>'No changes on destination and code.'
		 	));
   		}
    }
	/*
	*
	*Start file_upload function upload pdf link
	*
	*/

	function file_upload($name,$size,$tmp_name,$allowed_ext,$max_size,$destination){
		$ext = $this->get_extention($name);
		if($this->set_and_check_ext($allowed_ext,$ext)){
			if($this->max_size($max_size,$size)){
				if($this->upload_path($tmp_name,$destination,$name)){
					return true;
				}
				else return false;
			}
			else return false;
		}
		else return false;
	}
	/*
	*
	*End file_upload function upload pdf link
	*
	*/

	/*
	*
	*Start get_extention function upload pdf link
	*
	*/
	function get_extention($name){
	 	return strtolower(substr($name, strrpos($name,'.',-1)+1));
	}

 	/*
	*
	*End get_extention function upload pdf link
	*
	*/
	/*
	*
	*Start max_size function upload pdf link
	*
	*/
	function max_size($max_size,$size){
	 	if($size < $max_size)
			return true;
		else
	   		return false;
	}
	/*
	*
	*End max_size function upload pdf link
	*
	*/

	/*
	*
	*Start upload_path function upload pdf link
	*
	*/
	function upload_path($tmp_name,$destination,$name){
		if(move_uploaded_file($tmp_name,$destination.$name))
			return true;
		else
			return false;
	}
	/*
	*
	*End upload_path function upload pdf link
	*
	*/

	/*
	*
	*Start set_and_check_ext function upload pdf link
	*
	*/
	function set_and_check_ext($allowed_ext,$ext){
	 	$get_allowed_ext = explode('|',$allowed_ext);
	 	if(in_array($ext,$get_allowed_ext))
	 		return true;
	 	else
	 		return false;
	}
	/*
	*
	*End set_and_check_ext function upload pdf link
	*
	*/
}