<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('hashmonster'))
{
    function hashmonster($plaintext)
    {
        $hash = $plaintext;
		for ($i = 0; $i < 50000; $i++) {
			$hash = sha1($hash);
		}
		
		return $hash;
    }
}

if ( ! function_exists('setprivateid'))
{
	function setprivateid($object, $classname, $id)
	{
		if ($object instanceof $classname) {
			$ref = new ReflectionClass($classname);
			$idref = $ref->getProperty('id');
			$idref->setAccessible(true);
			$idref->setValue($object, $id);
		}
	}
}

if ( ! function_exists('countrysetter'))
{
	function countrysetter($countryCode, $db, $returnResult = 'country')
	{
		$country = false;
		$countryCode = (is_string($countryCode)) ? $countryCode : null;
		if ($countryCode) {
			$db->from('countries');
			$db->where('code', strtoupper($countryCode));
			$db->or_where('country', strtolower($countryCode));
			$result = $db->get()->result_array();
			if (count($result) === 1) {
				$country = ($returnResult == 'country') ? $result[0]['country'] : $result[0]['code'];
			}
		}
		return $country;
	}
}

if ( ! function_exists('countryservicesetter'))
{
	function countryservicesetter($countryCode, $db, $returnResult = 'country')
	{
		// EXPERIMENTAL
	}
}

if ( ! function_exists('surchargecmp'))
{
	function surchargecmp(Surcharge $subject, Surcharge $test) {
		if ($subject->getDescription() != $test->getDescription()) return false;
		if ($subject->getRuletype() != $test->getRuletype()) return false;
		if ($subject->getRule() != $test->getRule()) return false;
		if ($subject->getCost() != $test->getCost()) return false;
		if ($subject->getProfit() != $test->getProfit()) return false;
		if ($subject->getRatetype() != $test->getRatetype()) return false;
		return true;
	}
}

if ( ! function_exists('package_filename'))
{
	function package_filename(Package $package, $original_filename) {
		//modified kay libog. same file name ang result
		// $ext = end(explode('.', $original_filename));
		// return hashmonster($package->getCode()) . '_' . hashmonster($package->getTitle()) . '.' . $ext;
		return hashmonster($package->getCode()) . '_' . hashmonster($package->getTitle()) . '_' . $original_filename;
	}
}

if ( ! function_exists('arrangeToDropdown'))
{
	function arrangeToDropdown($data) {
		if(empty($data)) return array();

		$newDropDown ='<option value="None">- Select -</option>';
		foreach($data as $row){
			$newDropDown .= "<option value='".$row['package_hotel_id']."' id='ex_hotel_individual{$row['package_hotel_id']}' hotel_name='{$row['hotel_name']}'>".$row['hotel_name']."</option>";
		}

		return $newDropDown;
	}
}