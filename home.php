<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller {
	
	private $headDataInclude = NULL;

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->setAllAccess(true);
		parent::__construct();
		header('Access-Control-Allow-Origin: *');
		$this->load->database();

		//	send mail
		$this->load->library('My_PHPMailer');
		$this->load->helper('email');
		$this->mail = new PHPMailer();
		$this->mail->SMTPDebug = 0;
		$this->mail->isSMTP();																// Set mailer to use SMTP
		$this->mail->Host = 'cot.cottoncare.com.sg';										// Specify main and backup SMTP servers
		$this->mail->SMTPAuth = true;														// Enable SMTP authentication
		$this->mail->Username = 'no-reply@budgetmaid.com.sg';								// SMTP username
		$this->mail->Password = 'noreply@123';												// SMTP password
		$this->mail->SMTPSecure = 'tls';													// Enable TLS encryption, `ssl` also accepted
		$this->mail->Port = 587;															// TCP port to connect to
		
		//$this->mail->addBCC('johncabs018@yahoo.com');
    	// $this->mail->addBCC('delabahan@gmail.com');
    	// $this->mail->addBCC('jude@azazabizsolutions.com');

		/*if ($this->userpersistence->getPersistedUser() instanceof Agent) {
			redirect('errors/restricted_area');
		}*/
	}
	
	/**
	 * Default Controller action
	 */
	public function index() {
		
		if ('home' == uri_string()) redirect('');
		
		$this->load->model('user/user');
		$this->load->model('package/package');
		$this->load->model('package/packagestorage');
		$this->load->model('package/destination');
		
		//for sign up
		$countries_usr = $this->user->getAllCountries();
		$countries = $this->destination->getActiveDestinations();

		$error_message = $this->session->flashdata('flash_message');
		$confirmation_message = $this->session->flashdata('confirmation_message');
		$packages = $this->packagestorage->getByInformation(array());
		$packagesfronpages = $this->packagestorage->getPackagesFrontPages(array());
		$this->publicView(NULL,
			array(
				'confirmation_message' => $confirmation_message,
				'countries' => $countries,
				'countries_usr' => $countries_usr,
				'error_message' => $error_message,
				'login' => $this->isLoggedIn(),
				'packages' => $packagesfronpages,
				'usertype' => $this->userpersistence->getPersistedUser(),
			)
		);
	}


	public function package_jsonencode(){
		//if (!$this->isLoggedIn()) {
			$this->load->model('user/userauthentication');
			$this->load->model('user/userstorage');
			$this->load->library('form_validation');
			$this->form_validation->set_rules('email_login', 'Username', 'trim|required|min_length[5]|max_length[100]|xss_clean');
			if ($this->form_validation->run() == FALSE)
			{
				die(json_encode('You have input an invalid username'));
			}
			$uname=$this->input->post('email_login');
			$username=$this->userstorage->getByUser($uname);
			if(@$username[0]->email)
			{
				$email = $username[0]->email;
			}elseif(@$username[0]->email == $uname)
			{
				$email = $uname;
			}else
			{
				die(json_encode('You have entered an invalid username'));
			}
				$password = $this->input->post('password_login');
				if ($email && $password) 
				{
					$status = $this->userauthentication->getStatus($email);
					if($status !== false)
					{
						if ($status == 'active') {
							if ($this->userauthentication->setStored($email)) 
							{
								if ($this->userauthentication->verify($password)) 
								{
									$user = $this->userauthentication->getStoredUser();
									if ($user instanceof TypedUserInterface) 
									{
										$this->userpersistence->persistUser($user);
										if ($user instanceof Customer) 
										{
											$this->userpersistence->releaseUser();
											die(json_encode('restricted area for customer'));
										}else
										{
											//die(json_encode(array('Message'=>'You have entered an invalid username')));
											if($this->session->userdata('type')=="agent"){
												$key = '2f2c48ce73a7ebf9197c3dc8711fe3fb';
												if($key == $this->input->post('htxtseckey'))
												{
													$this->load->model('booking/booking');
													$data['package'] = $this->booking->package_hotel_mobile();
													foreach ($data['package'] as $key => $value) 
													{
														$data_result['data'][]=array($value,$this->booking->package_hotel_mobile_($value->package_id));
													}
													die(json_encode($data_result));
												} else 
												{
													die(json_encode('Restricted area for agent'));
												}
											} else 
											{
													die(json_encode('You are not agent'));
											}
										}
									}
								} else 
								{
									die(json_encode('You have entered an invalid password'));// Bad Password
								}
							} else 
							{
								die(json_encode('You have entered an invalid email'));// Bad Email
							}
						} elseif ($status == 'pending') 
						{
								die(json_encode('Pending Account: Please confirm this email'));
						} else {
							die(json_encode('Log in not allowed. User has been '. $status.'.'));
						}
					} else 
					{
						die(json_encode('You have entered an invalid email'));
					}
				} else 
				{
					die(json_encode('Input invalid'));// Bad Input
				}
		//}
	}
	
	public function result_package_jsonencode(){
				$this->load->model('booking/booking');
				$this->load->model('package/packagestorage');
				$this->load->model('package/package');
				$this->load->model('hotel/hotel');
				$this->load->model('surcharge/surchargestorage');
				$packageid = intval(@$_POST['package_id']);
				$hotel_name_id =  intval(@$_POST['hotel_name_id'])===0 ? false : intval(@$_POST['hotel_name_id']);
				$datein =  intval(@$_POST['date_in']);
				$dateout =  intval(@$_POST['date_out']);
				$list_srchg = array();
				$list_hotel = $this->booking->getBysummaryCheckRatePackageHotel($packageid, $hotel_name_id);
				for ($i=0; $i < count($list_hotel); $i++) { 
					$list_srchg[$i] = new stdClass();

					$list_srchg[$i]->pkg = $this->surchargestorage->getSurchargeByType_id("package",$list_hotel[$i]->PACK_ID, TRUE);
					$list_srchg[$i]->htl = $this->surchargestorage->getSurchargeByType_id("hotel",$list_hotel[$i]->PACK_HOTEL_ID, TRUE);

					if ($datein && $dateout) {
						$surcharges_pkg = array("packages"=>$list_srchg[$i]->pkg);
						$surcharges_htl = array("hotels"=>$list_srchg[$i]->htl);

						$dates = array("return"=>$dateout,"depart"=>$datein);

						// $list_srchg[$i]->pkg = $this->surchargestorage->processSurchargePackage($surcharges_pkg, $dates);
						// $list_srchg[$i]->htl = $this->surchargestorage->processSurchargeHotels($surcharges_htl, $dates);

						$tmp_pkg_surchg = $this->surchargestorage->processSurchargePackage($surcharges_pkg, $dates);
						$count_test_pkg = count($tmp_pkg_surchg);
						$tmp_pkg_surchg_list = array();
						if($count_test_pkg!=0){
							foreach ($tmp_pkg_surchg as $categ) {
								foreach ($categ as $surcharge) {
									$tmp_pkg_surchg_list[]=$surcharge;
								}		
							}
						}
						$list_srchg[$i]->pkg = $tmp_pkg_surchg_list;

						$tmp_htl_surchg = $this->surchargestorage->processSurchargeHotels($surcharges_htl, $dates);
						$count_test_htl = count($tmp_htl_surchg);
						$tmp_htl_surchg_list = array();
						if($count_test_htl!=0){
							foreach ($tmp_htl_surchg as $categ) {
								foreach ($categ as $surcharge) {
									$tmp_htl_surchg_list[]=$surcharge;
								}		
							}
						}
						$list_srchg[$i]->htl = $tmp_htl_surchg_list;

					}

					$list_hotel[$i]->hotelroom = $this->booking->getBysummaryCheckRateHotelRoom($list_hotel[$i]->PACK_HOTEL_ID);
					$list_srchg[$i]->hrm = array();
					for ($hrm=0; $hrm < count($list_hotel[$i]->hotelroom); $hrm++) { 
						@$tmp_var = $list_hotel[$i]->hotelroom[$hrm];
						
						if ($datein && $dateout) {
							$tmp_hrm_list = $this->surchargestorage->getSurchargeByType_id("hotelroom",$tmp_var->HRM_ID, TRUE);
							
							$tmp_hrm = $this->surchargestorage->getRoomRateSurcharge_id($tmp_var->HRM_ID);
							if ($tmp_hrm !== FALSE) {
								$surcharges_hrm = array("hotelrooms"=>$tmp_hrm);
								$dates = array("return"=>$dateout,"depart"=>$datein);

								$tmp_hrm_surchg = $this->surchargestorage->processSurchargeHotelRooms($surcharges_hrm, $dates);

								$count_test = count($tmp_hrm_surchg);

								$tmp_hrm_surchg_rm = array();
								if($count_test!=0){
									foreach ($tmp_hrm_surchg as $categ) {
										foreach ($categ as $surcharge) {
											$tmp_hrm_surchg_rm[]=$surcharge;
										}		
									}
								}
								$list_srchg[$i]->hrm[$hrm] = $tmp_hrm_surchg_rm;
							}
						}
						else {
							$list_srchg[$i]->hrm[] = $this->surchargestorage->getSurchargeByType_id("hotelroom",$tmp_var->HRM_ID, TRUE);
							 
						}

					}
					//dump($list_srchg);
					//dump($list_hotel);
				}
					//dump(array('ListHotel'=>$list_hotel,'ListSurcharges'=>$list_srchg));
					//die(json_encode($data));
					die(json_encode(array('ListHotel'=>$list_hotel,'ListSurcharges'=>$list_srchg)));
	}

	//Customer Booking Details
	public function json_voucher() {
		$key = $this->input->post('key');
		if($key == "2f2c48ce73a7ebf9197c3dc8711fe3fb"){
			$booking_code=$this->input->post('booking_code');
			$user_data = $this->userpersistence->getPersistedUser();
			$this->load->model('booking/booking');
			$this->load->model("booking/bookingstorage");
			$summary_data['guest']=$this->booking->get_by(array('code' => $booking_code));
			$count = (count($summary_data['guest'])-1) < 0 ? 0 : (count($summary_data['guest'])-1);
			$booking_id = $summary_data['guest'][$count]->id;
			$summary_data['invoice_pdf']=$this->booking->getBysummary($booking_id);
			/*$summary_data['provider_summary']=$this->booking->getBysummaryCompany($booking_id);
			$summary_data['room_summary']=$this->booking->getBysummaryRoomtype($booking_id);
			$summary_data['package_summarys']=$this->booking->getBysummaryPackage($booking_id);
			$summary_data['add_hotel_bookings']=$this->booking->getBysummaryPackageAddHotelBooking($booking_id);*/
			#get the individual summaries ----------------------------------------------
	        //$package_summary = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"type"=>"0package_surcharge"));
	        //$addon_summary = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"type"=>"4addon"));
	        //$extension_summary = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"type"=>"5extension"));
	        
	        #fetch the destination IDs ----------------------------------------------
	        $destination_summary = $this->bookingstorage->getSummaryDestinations($booking_id);

	        for($i=0;$i<count($destination_summary);$i++){
	        	$destination = $destination_summary[$i];
	        	#fetch the hotel, room surcharge, room summaries ----------------------------------------------
	            $hotel_summary[] = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"destination_id"=>$destination["destination_id"],"type"=>"1hotel_surcharge"));
	            $rs_summary[] = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"destination_id"=>$destination["destination_id"],"type"=>"2room_surcharge"));
	            $r_summary[] = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"destination_id"=>$destination["destination_id"],"type"=>"3room_payment"));    
	        }

	    	/*$summary_data['package_summary']=$package_summary;
	    	$summary_data['addon_summary']=$addon_summary;
	    	$summary_data['extension_summary']=$extension_summary;
	    	$summary_data['destination_summary']=$destination_summary;
	    	$summary_data['hotel_summary']=$hotel_summary;*/
	    	$summary_data['rs_summary']=$rs_summary;
	    	$summary_data['r_summary']=$r_summary;
	    	$summary_data['user']=$user_data;
			die(json_encode($summary_data));
			/*$this->pdf->set_paper( "letter", "portrait" ); 
			$this->pdf->load_view('panel/mainpanel/voucher_pdf_provider',$summary_data);
			$this->pdf->render();
			$this->pdf->stream('voucher.pdf',array('Attachment'=>0));*/
		}else{
			redirect('errors/restricted_area_for_agent');
		}
	}

	public function json_vouchersample() {
		//$key = $this->input->post('key');
		$key ="2f2c48ce73a7ebf9197c3dc8711fe3fb";
		if($key == "2f2c48ce73a7ebf9197c3dc8711fe3fb"){
			//$booking_code=$this->input->post('booking_code');
			$booking_code= "GE100183";
			$user_data = $this->userpersistence->getPersistedUser();
			$this->load->model('booking/booking');
			$this->load->model("booking/bookingstorage");
			$summary_data['guest']=$this->booking->get_by(array('code' => $booking_code));
			if(empty($summary_data['guest'])):
				die(json_encode(array('error' => true, 'message' => 'No data found')));
			endif;
			$count = (count($summary_data['guest'])-1) < 0 ? 0 : (count($summary_data['guest'])-1);
			$booking_id = $summary_data['guest'][$count]->id;
			$summary_data['invoice_pdf']=$this->booking->getBysummary($booking_id);
			/*$summary_data['provider_summary']=$this->booking->getBysummaryCompany($booking_id);
			$summary_data['room_summary']=$this->booking->getBysummaryRoomtype($booking_id);
			$summary_data['package_summarys']=$this->booking->getBysummaryPackage($booking_id);
			$summary_data['add_hotel_bookings']=$this->booking->getBysummaryPackageAddHotelBooking($booking_id);*/
			#get the individual summaries ----------------------------------------------
	        //$package_summary = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"type"=>"0package_surcharge"));
	        //$addon_summary = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"type"=>"4addon"));
	        //$extension_summary = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"type"=>"5extension"));
	        
	        #fetch the destination IDs ----------------------------------------------
	        $destination_summary = $this->bookingstorage->getSummaryDestinations($booking_id);

	        for($i=0;$i<count($destination_summary);$i++){
	        	$destination = $destination_summary[$i];
	        	#fetch the hotel, room surcharge, room summaries ----------------------------------------------
	            $hotel_summary[] = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"destination_id"=>$destination["destination_id"],"type"=>"1hotel_surcharge"));
	            $rs_summary[] = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"destination_id"=>$destination["destination_id"],"type"=>"2room_surcharge"));
	            $r_summary[] = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"destination_id"=>$destination["destination_id"],"type"=>"3room_payment"));    
	        }

	    	/*$summary_data['package_summary']=$package_summary;
	    	$summary_data['addon_summary']=$addon_summary;
	    	$summary_data['extension_summary']=$extension_summary;
	    	$summary_data['destination_summary']=$destination_summary;
	    	$summary_data['hotel_summary']=$hotel_summary;*/
	    	$summary_data['rs_summary']=$rs_summary;
	    	$summary_data['r_summary']=$r_summary;
	    	$summary_data['user']=$user_data;
			die(json_encode($summary_data));
			/*$this->pdf->set_paper( "letter", "portrait" ); 
			$this->pdf->load_view('panel/mainpanel/voucher_pdf_provider',$summary_data);
			$this->pdf->render();
			$this->pdf->stream('voucher.pdf',array('Attachment'=>0));*/
		}else{
			redirect('errors/restricted_area_for_agent');
		}
	}

	public function contact(){

		if ('home/contact' == uri_string()) redirect('contact');
		
		$this->load->model('user/user');
		$this->load->model('package/package');
		$this->load->model('package/packagestorage');
		$this->load->model('package/destination');

		$error_message = $this->session->flashdata('flash_message');
		//for sign up
		$countries_usr = $this->user->getAllCountries();
		$countries = $this->destination->getActiveDestinations();

		$this->publicView(null,
			array(
				'error_message' => $error_message,
				'countries' => $countries,
				'countries_usr' => $countries_usr,
				'login' => $this->isLoggedIn(),
				'usertype' => $this->userpersistence->getPersistedUser(),
		));		
	}

	public function about(){
		if ('home/about' == uri_string()) redirect('about');
		
		$this->load->model('user/user');
		$this->load->model('package/package');
		$this->load->model('package/packagestorage');
		$this->load->model('package/destination');

		
		$error_message = $this->session->flashdata('flash_message');
		//for sign up
		$countries_usr = $this->user->getAllCountries();
		$countries = $this->destination->getActiveDestinations();

		$this->publicView(null,
			array(
				'error_message' => $error_message,
				'countries' => $countries,				
				'countries_usr' => $countries_usr,
				'login' => $this->isLoggedIn(),
				'usertype' => $this->userpersistence->getPersistedUser(),
		));		
	}

	/**
	**the variable test here is just to simulate the sending of email
	**1 if the mail has been sent
	**0 if an error has occured
	**remove this comment and variable when done with emailing
	**/

	public function send_mail(){
		$post = $this->input->post();

		if($post){
			$this->load->model('booking/booking');
			$sender_name = trim($post['sender_name']);
			$sender_email = (valid_email($post['sender_email'])) ? $post['sender_email'] : null;
			$sender_message = (trim($post['sender_message']) != '') ? $post['sender_message'] : null;

			//dump($post);
			// if(null){
			if($sender_message && $sender_email){
				$mail = $this->mail;
		    	$mail->setFrom('no-reply@ge.com.sg', $sender_name);						// Sender
		    	$mail->addReplyTo($sender_email, $sender_name);							// Sender's optional
				$recipients=$this->booking->getEmailadmin();
				if($recipients != NULL || $recipients != ""){
					foreach($recipients as $row){
						$admin_email=$row->adminemail;
						$mail->addAddress($admin_email, 'Global Explorer');				// Add a recipient (Admin)
					}
				}
			    $mail->WordWrap = 50;													// Set word wrap to 50 characters
			    $mail->isHTML(true);													// Set email format to HTML
			    $mail->Subject = 'Inquiries [Global Explorer]';
			    $mail->Body    = $sender_message;

		    	$mail->AltBody = $sender_message;



				//dump($mail);
				//if done sending
				if($mail->send()){
					echo json_encode(array(
						'alert_type'=>'Success',
						'message'=>'Email Sent.'
					));
				}else{ 
					echo json_encode(array(
						'alert_type'=>'Error',
						'message'=>'Unable to send your message. Please try again.'
					));
				}
			} else { 
				echo json_encode(array(
					'alert_type'=>'Error',
					'message'=>'Unable to send mail. Please enter valid mail and/or message.'
				));
			}
		}		
	}

	public function browse(){
		// if ('home/browse' == uri_string()) redirect('browse');
		$destination_code = $this->uri->segment(2, 0);
		$this->load->model('user/user');
		$this->load->model('hotel/hotelstorage');
		$this->load->model('package/package');
		$this->load->model('package/packagestorage');
		$this->load->model('package/destination');

		$this->db->select('country');
		$dest = $this->db->get_where('countries', array('code'=>$destination_code))->row();

		$error_message = $this->session->flashdata('flash_message');
		$user_data = $this->userpersistence->getPersistedUser();
		$packages = $this->packagestorage->getByInformation(array('country_code'=>$destination_code));

		$pdfs=$this->hotelstorage->getDestinationLink_pdf($destination_code);
		
		//for sign up
		$countries_usr = $this->user->getAllCountries();
		$countries = $this->destination->getActiveDestinations();
		
		// $pdfs=$this->hotelstorage->getDestinationLink_pdf('TH');
		// $destination_display=$this->hotelstorage->getDestinationLink_display($destination_code);
		// $link_pdf=$this->hotelstorage->getDestinationLink($destination_code);
		// 
		$this->publicView(null,
			array(
				'error_message' => $error_message,
				'packages' => $packages,
				'countries' => $countries,
				'countries_usr' => $countries_usr,
				'login' => $this->isLoggedIn(),
				'usertype' => $this->userpersistence->getPersistedUser(),
				'dest'=>$dest->country,
				'pdfs'=>$pdfs,
				// 'linkpdf'=>$link_pdf
			)
		);
	}

	public function login() {
		$package_id = $this->input->post('h-id');
		if (!$this->isLoggedIn()) {
			$this->load->model('user/userauthentication');
			$email = $this->input->post('email_login');
			$password = $this->input->post('password_login');
			if ($email && $password) {
				$status = $this->userauthentication->getStatus($email);
				if($status !== false){
					if ($status == 'active') {
						if ($this->userauthentication->setStored($email)) {
							if ($this->userauthentication->verify($password)) {
								$user = $this->userauthentication->getStoredUser();
								if ($user instanceof TypedUserInterface) {
									$this->userpersistence->persistUser($user);
									if ($user instanceof Agent) {
										$this->userpersistence->releaseUser();
										redirect('errors/restricted_area_for_agent');
									} else if($user instanceof Admin){
										$this->flash_message('You have entered an invalid account because your are admin', TRUE, 'home/index');
										$this->userpersistence->releaseUser();
									}else{
										if($this->session->userdata('type')=="customer"){
											$this->load->database();
											$key = sha1(md5('~!@#$%^&*()_+ hacker is not allowed this area!', true));
											redirect('home/details2/'.$this->encrypt($package_id, $key));
										}else {
											redirect('home');
										}
									}
								}
							} else {
							// Bad Password
								$this->flash_message('You have entered an invalid password', TRUE, '');
							}
						} else {
						// Bad Email
							$this->flash_message('You have entered an invalid email', TRUE, '');
						}
					} elseif ($status == 'pending') {
						$this->flash_message('Pending Account: Please confirm this email', TRUE, '');
					} else {
						$this->flash_message('Log in not allowed. User has been '. $status .'.', TRUE, '');
					}
				} else {
					$this->flash_message('You have entered an invalid email', TRUE, '');
				}
			} else {
			// Bad Input
				$this->flash_message('Input invalid', TRUE, '');
			}
		}else{
			if ($user instanceof TypedUserInterface) {
				if($this->session->userdata('type')=="customer"){
					$key = sha1(md5('~!@#$%^&*()_+ hacker is not allowed this area!', true));
					redirect('home/details2/'.$this->encrypt($package_id, $key));
				}else {
					redirect('home');
				}
			}
		}
	}

	public function encryptwer(){
		$id=10;
		$url = substr(sha1(md5(uniqid($id, true))),0,64);
		echo $url;
		$token = substr(sha1(uniqid($url, true)),0,64);
		echo '<br>'.$token.'<br>';
		$key = md5('crypto key...', true); // For demonstration purpose
		$id=0;
		echo 'This id <br>';
		echo $id.'<br>';
		dump($this->encrypt($id, $key));
		$encrypted_id=$this->encrypt($id, $key);
		dump($this->decrypt($encrypted_id, $key));
		echo '<br>';
		$toHash = sprintf("%d_%s_%s",'~!@#$%^&*()_+',
									 'the hacker is not allowed',
									 'QWERTYUIOPqwertyuiop');
		// Use hashmonster to create hash
		$hash = hashmonster($toHash);
		dump($hash);
	}
	
	public function details1(){
		$uir_res=$this->uri->segment(3);
		$toHash = sprintf("%d_%s_%s",'~!@#$%^&*()_+',
									 'the hacker is not allowed',
									 'QWERTYUIOPqwertyuiop');
		$hash = hashmonster($toHash);
		$key = sha1(md5($hash, true));
		$package_id=$this->decrypt($uir_res,$key);
		//if ('home/details1' == uri_string()) redirect('details1');
		if(!$package_id) return;
		$package_id = intval($package_id);
		$this->load->model('user/user');
		$this->load->model('package/package');
		$this->load->model('package/packagestorage');
		$this->load->model('package/destination');
		$this->load->model('surcharge/surchargestorage');

		$error_message = $this->session->flashdata('flash_message');
		$package = $this->packagestorage->getById($package_id);
		//for sign up
		$countries_usr = $this->user->getAllCountries();
		$countries = $this->destination->getActiveDestinations();
		
		$pkg_srchg = $this->surchargestorage->getSurchargeByType_id("package",$package_id, TRUE);
		$this->publicView(null,
			array(
				'error_message' => $error_message,
				'package' => @$package[0],
				'countries' => $countries,
				'countries_usr' => $countries_usr,
				'login' => $this->isLoggedIn(),
				'usertype' => $this->userpersistence->getPersistedUser(),
				"pkg_srchg" => $pkg_srchg
			)
		);
	}

	public function details2(){
		if ($this->userpersistence->getPersistedUser() instanceof Customer) {
			$uir_res=$this->uri->segment(3);
			$key = sha1(md5('~!@#$%^&*()_+ hacker is not allowed this area!', true));
			$package_id=$this->decrypt($uir_res, $key);
			//dump($uir_res);
			//die();
			//if ('home/details2' == uri_string()) redirect('details2');
			$this->load->model("booking/bookingstorage");
			if(!$package_id) return;

			$booking_data = null;
			if(@$booking_id){
				$booking_data = $this->bookingstorage->getBookingInformation($booking_id);
			}

			#load models ----------------------------------------------u+
			$this->load->model('booking/booking');
			$this->load->model('user/userstorage');
			$this->load->model('package/package');
			$this->load->model('package/packagestorage');
			$this->load->model('hotel/hotelstorage');
			$this->load->model('surcharge/surchargestorage');
			$this->load->model('package/addon');
			$this->load->model('user/user');
			$this->load->model('package/destination');
			
			$package = $this->packagestorage->getById($package_id);
			//for sign up
			$countries_usr = $this->user->getAllCountries();
			$countries = $this->destination->getActiveDestinations();

			$error_message = $this->session->flashdata('flash_message');
			
			#$package_hotels = $this->hotelstorage->getHotelsByPackage($package_id);
			$this->db->order_by('country','asc');
			$this->userstorage->includePending(true);
			$countries = $this->db->get('countries')->result_array();
			$user_data = $this->userpersistence->getPersistedUser();
			$package = $this->packagestorage->getById($package_id);
			$addons = $this->addon->getAddons_packageId($package_id);
			$destinations = $this->packagestorage->getDestinations($package_id);

			#get the package addons ----------------------------------------------
			if(count($addons)){
				for($i=0;$i<count($addons);$i++){
					$addon_book = $this->addon->getAddons_ID_QTY($addons[$i]["id"], $booking_id);
					$addons[$i]["user_qty"] = @$addon_book->quantity;
				}
			}

			#get the package hotels ----------------------------------------------
			$package_hotels = array();
			if(count($destinations)){
				foreach($destinations as $dest){
					$destination_id = $dest['id'];
					$package_hotels[] = $this->hotelstorage->getHotelsByPackageAndCode($dest['code'],$dest["package_id"]);
				}
			}

			#get package surcharge ----------------------------------------------
			$pkg_srchg = $this->surchargestorage->getSurchargeByType_id("package",$package_id, TRUE);
			//dump($pkg_srchg);
			$pkg_surcharge_count = count($pkg_srchg);
			//dump($pkg_surcharge_count);
			$pkg_surcharge_price=0;
			$pkg_srchg_id = array();

			#get package surcharge info ----------------------------------------------
			if($pkg_surcharge_count!=0){
				foreach ($pkg_srchg as $pkg) {
					$pkg_surcharge_price += $pkg['cost']+$pkg['profit'];
					$pkg_srchg_id[] = array(
						'id' => $pkg['id'],
						'profit'=> $pkg['profit'],
						'cost' => $pkg['cost'],
						'rate_type' => $pkg['rate_type']
					);
				}
			}

			#set session package id ----------------------------------------------
			$this->session->set_userdata('package_id',$package_id);


			$this->publicView(null,
				array(
					'error_message' => $error_message,
					'package' => @$package[0],
					'countries' => $countries,
					'login' => $this->isLoggedIn(),
					'usertype' => $this->userpersistence->getPersistedUser(),
					'package_hotels' 	=> $package_hotels,
					'countries_usr' 		=> $countries_usr,
					'user' 			=> $user_data,
					'addons' => $addons,
					"package_surcharge" => $pkg_surcharge_price,
					"package_surcharge_id" => $pkg_srchg_id,
					"destinations" => $destinations,
					"booking_data" => $booking_data[0],
					"pkg_srchg" => $pkg_srchg
				)
			);
		}else{
			redirect('errors/restricted_area');
		}
	}

	public function details3(){
		if ($this->userpersistence->getPersistedUser() instanceof Customer) {
			$uir_res=$this->uri->segment(3);
			$key = sha1(md5('~!@#$%^&*()_+ hacker is not allowed this area!', true));
			$package_id=$this->decrypt($uir_res, $key);
			//$this->session->set_userdata('steptwo',$_POST);
			//if ('home/details3' == uri_string()) redirect('details3'.sha1($enc));

			if(!$package_id) return;

			$booking_data = null;
			if(@$booking_id){
				$booking_data = $this->bookingstorage->getBookingInformation($booking_id);
			}
			

			#load models ----------------------------------------------u+
			$this->load->model("booking/bookingstorage");
			$this->load->model('booking/booking');
			$this->load->model('user/userstorage');
			$this->load->model('package/package');
			$this->load->model('package/packagestorage');
			$this->load->model('hotel/hotelstorage');
			$this->load->model('surcharge/surchargestorage');
			$this->load->model('package/addon');
			$this->load->model('user/user');
			$this->load->model('package/destination');
			$addon_model = $this->load->model("package/addon");

			$data["prev_details"]["booking_id"] = @$_POST["booking_id"];
			$data["prev_details"]["booking_code"] = @$_POST["booking_code"];

			$data["prev_details"]["package_id"] = @$_POST['package_id'];

			$data["prev_details"]['package_days'] =  @$_POST['package_days'];
			$data["prev_details"]["extended_days"] = @$_POST["extended_days"];

			$data["prev_details"]["infant_cnt"] = isset($_POST["infant_cnt"]) ?  $this->input->post("infant_cnt", TRUE):0;
			$data["prev_details"]["adult_count"] = isset($_POST["adult_count"]) ?  $this->input->post('adult_count', TRUE):0;
			$data["prev_details"]["child_count"] = isset($_POST["child_count"]) ?  $this->input->post('child_count', TRUE):0;
			$data["prev_details"]["total_holder"] = @$_POST["total_holder"];

			$data["prev_details"]["depart_date"] =  $this->input->post("depart_date", TRUE);
			$data["prev_details"]["return_date"] =  $this->input->post("return_date", TRUE);

			$data["prev_details"]["package_hotel_choice"] =  @$_POST["package_hotel_choice"];
			#$data["prev_details"]["package_hotel_name"] = $_POST["package_hotel_name"];

			$data["prev_details"]["roomrate_id"] =  @$_POST["roomrate_id"];
			#$data["prev_details"]["roomrate_price"] = $_POST['roomrate_price'];
			$data["prev_details"]["roomrate_pax"] =  @$_POST['roomrate_pax'];
			#$data["prev_details"]["roomrate_quantity"] = $_POST['roomrate_quantity'];
			#$data["prev_details"]["roomrate_rate_type"] = $_POST['roomrate_rate_type'];
			$data["prev_details"]["roomrate_id_surcharges"] =  @$_POST['roomrate_surcharges'];

			$data["prev_details"]["child_date"] = isset($_POST['child_date']) ?  $_POST['child_date']:0;
	 		
			$data["prev_details"]["roomrate_quantity"] =  @$_POST["roomrate_quantity"];
			
			#package surcharge meta data 
			$data["prev_details"]["package_surcharge_id"] = @$_POST["pkg_surcharge_id"];
			#$data["prev_details"]["pkg_surcharge_description"] = @$_POST['pkg_surcharge_description'];
			#$data["prev_details"]["pkg_surcharge_rule"] = @$_POST['pkg_surcharge_rule'];
			#$data["prev_details"]["pkg_surcharge_rule_type"] = @$_POST['pkg_surcharge_rule_type'];
			#$data["prev_details"]["pkg_surcharge_profit"] = @$_POST['pkg_surcharge_profit'];
			#$data["prev_details"]["pkg_surcharge_cost"] = @$_POST['pkg_surcharge_cost'];
			#$data["prev_details"]["pkg_surcharge_price"] = @$_POST['pkg_surcharge_price'];
			

			#hotel surcharge metadata
			$data["prev_details"]["hotel_surcharge_id"] = @$_POST["hotel_surcharge_id"];
			#$data["prev_details"]["hotel_surcharge_rule_type"] = @$_POST['hotel_surcharge_rule_type'];
			#$data["prev_details"]["hotel_surcharge_profit"] = @$_POST['hotel_surcharge_profit'];
			#$data["prev_details"]["hotel_surcharge_cost"] = @$_POST['hotel_surcharge_cost'];
			#$data["prev_details"]["hotel_surcharge_price"] = @$_POST['hotel_surcharge_price'];
			#$data["prev_details"]["hotel_surcharge_rule"] = @$_POST['hotel_surcharge_rule'];

			#get addon for the package ----------------------------------------------
			$data["prev_details"]["addon_id"] = isset($_POST["addon_id"]) ? $_POST["addon_id"]:0; #check if addon is present
			$data["prev_details"]["addon_quantity"] = isset($_POST["addon_quantity"]) ?  $this->input->post('addon_quantity', TRUE):0;
			#$data["prev_details"]["addon_desc"] = isset($_POST['addon_desc']) ? $_POST['addon_desc']: "";
			#$data["prev_details"]["addon_price"] = isset($_POST['addon_cost']) ? intVal($_POST['addon_cost'])+intVal($_POST['addon_profit']) : 0;
			#$data["prev_details"]["addon_unit"] = isset($_POST['addon_unit']) ? $_POST["addon_unit"]: "";

			#get total days,payments,cost,and profit ----------------------------------------------
			$data["prev_details"]["total_payment"] =  @$_POST['package_total_payment'];
			$data["prev_details"]["total_cost"] = @$_POST['package_total_cost'];
			$data["prev_details"]["total_profit"] = @$_POST['package_total_profit'];
			@$data["prev_details"]["total_days"] = (int)$_POST['package_days']+(int)$_POST['extended_days'];
			
			#extension remarks ----------------------------------------------
			$data["prev_details"]["exremarks"] = isset($_POST["exremarks"]) ? $this->input->post('exremarks',TRUE):"";

			#additional hotel information ----------------------------------------------
			$data["prev_details"]["exhotel_remarks"] = @$this->input->post('exhotel_remarks', TRUE);
			$data["prev_details"]["exhotel_name"] = @$_POST['exhotel_name'];
			$data["prev_details"]["exhotel_name_full"] = @$_POST['exhotel_name_full'];

			$data["prev_details"]["room_count"] = @$_POST['room_count'];
			#$data['prev_details']['package_total_surcharge'] = $_POST['package_total_surcharge'];
			#$data['prev_details']['package_hotel_total_surcharge'] = $_POST['package_hotel_total_surcharge'];
			#$data['prev_details']['package_room_total_surcharges'] = $_POST['package_room_total_surcharges'];

			#destination block ----------------------------------------------
			$data["prev_details"]["destination_id"] = @$_POST["destination_id"];
			#$data["prev_details"]["destination_country"] = @$_POST["destination_country"];
			#$data["prev_details"]["destination_nights"] = @$_POST["destination_nights"];
			$data["prev_details"]["bd_destination_hotel"] = @$_POST["bd_destination_hotel"];

			#pkg surcharge summary 
			$data["prev_details"]["bd_pkg_sur_description"] = @$_POST["bd_pkg_sur_description"];
			$data["prev_details"]["bd_pkg_sur_computation"] = @$_POST["bd_pkg_sur_computation"];
			$data["prev_details"]["bd_pkg_sur_subtotal"] = @$_POST["bd_pkg_sur_subtotal"];

			#destination summary
			$data["prev_details"]["bd_destination_id"] = @$_POST["bd_destination_id"];
			$data["prev_details"]["bd_destination_hotel"] = @$_POST["bd_destination_hotel"];	

			#hotel summary
			$data["prev_details"]["bd_hotel_sur_description"] = @$_POST["bd_hotel_sur_description"];
			$data["prev_details"]["bd_hotel_sur_computation"] = @$_POST["bd_hotel_sur_computation"];
			$data["prev_details"]["bd_hotel_sur_subtotal"] = @$_POST["bd_hotel_sur_subtotal"];

			#room surcharge summary 
			$data["prev_details"]["bd_rs_description"] = @$_POST["bd_rs_description"];
			$data["prev_details"]["bd_rs_computation"] = @$_POST["bd_rs_computation"];
			$data["prev_details"]["bd_rs_subtotal"] = @$_POST["bd_rs_subtotal"];

			#room summary
			$data["prev_details"]["bd_room_description"] = @$_POST["bd_room_description"];
			$data["prev_details"]["bd_room_computation"] = @$_POST["bd_room_computation"];
			$data["prev_details"]["bd_room_subtotal"] = @$_POST["bd_room_subtotal"];

			#addon summary
			$data["prev_details"]["bd_addon_description"] = @$_POST["bd_addon_description"];
			$data["prev_details"]["bd_addon_computation"] = @$_POST["bd_addon_computation"];
			$data["prev_details"]["bd_addon_subtotal"] = @$_POST["bd_addon_subtotal"];

			#extensions summary
			$data["prev_details"]["bd_ext_description"] = @$_POST["bd_ext_description"];
			$data["prev_details"]["bd_ext_computation"] = @$_POST["bd_ext_computation"];
			$data["prev_details"]["bd_ext_subtotal"] = @$_POST["bd_ext_subtotal"];

			$this->session->set_userdata('step_booking',$data['prev_details']);
			#end ofinitialize the needed data ----------------------------------------------

			#get countries ----------------------------------------------
			$this->db->order_by('country');
			$countries = $this->db->get('countries')->result_array();
			$this->db->order_by('name');
			$airportcodes = $this->db->get('airportcodes')->result_array();

			#set session package id ----------------------------------------------
			$wang=$this->session->userdata('step_booking');
			$this->session->set_userdata("package_id",$wang['package_id']);
			
			
			$traveller_details = false;
			$booking_data = false;
			#get traveller details
			if($data["prev_details"]["booking_id"]!=null){	
				$traveller_details = $this->bookingstorage->getTravellerDetails($data["prev_details"]["booking_id"]);
				$booking_data = $this->bookingstorage->getBookingInformation($data["prev_details"]["booking_id"]);
			}
			
			#get package id ----------------------------------------------
			$package_id = $this->session->userdata('package_id'); 

			#get package information ----------------------------------------------
			$package = $this->packagestorage->getById($package_id);

			#get user data ----------------------------------------------
			$user_data = $this->userpersistence->getPersistedUser();
			$all_airportcodes = array();

			#concatenate the country code and id as per request ----------------------------------------------
			foreach($airportcodes as $airportcode){
				if(!in_array($airportcode['name'], $all_airportcodes)) {
					$all_airportcodes[$airportcode['code'].".".$airportcode['id']] = $airportcode['name'];
				}
			}

			$package = $this->packagestorage->getById($package_id);

			//for sign up
			$countries_usr = $this->user->getAllCountries(); 
			$countries = $this->destination->getActiveDestinations();

			$error_message = $this->session->flashdata('flash_message');
			
			$this->publicView(null,
				array(
					'error_message' => $error_message,
					'countries' => $countries,
					'countries_usr' => $countries_usr,
					'login' => $this->isLoggedIn(),
					'usertype' => $this->userpersistence->getPersistedUser(),
					'airportcodes' => $all_airportcodes,
					'prev_details' => $this->session->userdata('step_booking'),
					'package'=>@$package[0],
					'user' => $user_data,
					'traveller_details' => $traveller_details,
					"booking_data" => $booking_data[0],
					"addon_model" => $addon_model
			));
		}else{
			redirect('errors/restricted_area');
		}
	}

	public function getSummaryViewByEmail($booking_id){
		$this->load->model('booking/booking');
		$this->load->model("booking/bookingstorage");
		$this->load->model('user/user');
		$countries_usr    = $this->user->getAllCountries();
		$summary_data     =$this->booking->getBysummary($booking_id);
		$gettotalnights   =$this->booking->getBysummaryPackage($booking_id);
		$summary_guest    =$this->booking->getByGuest($booking_id);
		$provider_package =$this->booking->getBysummaryCompany($booking_id);


		#get the individual summaries ----------------------------------------------
		$package_summary   = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"type"=>"0package_surcharge"));
		$addon_summary     = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"type"=>"4addon"));
		$extension_summary = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"type"=>"5extension"));
        
        #fetch the destination IDs ----------------------------------------------
        $destination_summary = $this->bookingstorage->getSummaryDestinations($booking_id);

        for($i=0;$i<count($destination_summary);$i++){
			$destination     = $destination_summary[$i];
			#fetch the hotel, room surcharge, room summaries ----------------------------------------------
			$hotel_summary[] = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"destination_id"=>$destination["destination_id"],"type"=>"1hotel_surcharge"));
			$rs_summary[]    = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"destination_id"=>$destination["destination_id"],"type"=>"2room_surcharge"));
			$r_summary[]     = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"destination_id"=>$destination["destination_id"],"type"=>"3room_payment"));    
        }

		$user_data = $this->userpersistence->getPersistedUser();

		$data['countries_usr']        = $countries_usr;
		$data['user']                 =$user_data;
		$data['data_summary']         =$summary_data;
		$data['guest_summary']        =$summary_guest;
		$data['package_total_nights'] =$gettotalnights;
		$data['provider_summary']     =$provider_package;
		$data['package_summary']      =@$package_summary;
		$data['addon_summary']        =@$addon_summary;
		$data['extension_summary']    =@$extension_summary;
		$data['destination_summary']  =@$destination_summary;
		$data['hotel_summary']        =@$hotel_summary;
		$data['rs_summary']           =@$rs_summary;
		$data['r_summary']            =@$r_summary;

		$email_view_user = get_Class($data['user']);
		return $data;

		// dump($email_view_user);
		// die;
		//return $this->load->view("panel/mainpanel/append_email.php",$data,TRUE);
	}

	public function getSummaryViewByEmail_agent($booking_id){
		$data = $this->getSummaryViewByEmail($booking_id);
		return $this->load->view("panel/mainpanel/append_email.php",$data,TRUE);
	}

	public function getSummaryViewByEmail_provider($booking_id){
		$data = $this->getSummaryViewByEmail($booking_id);
		return $this->load->view("panel/mainpanel/append_email_provider.php",$data,TRUE);
	}
	/*
	*
	*Start booking function 
	*
	*/
	public function booking_cus() {
		$this->load->library("security");
		
		$step_booking = unserialize($_POST["step_booking_serialized"]);
		unset($_POST['step_booking_serialized']);

		$new_tmp_array = array_merge($step_booking,$this->security->xss_clean($_POST));
		$this->session->set_userdata("step_detail", $new_tmp_array);
		//dump_exit($new_tmp_array);
		$this->load->model('booking/bookingstorage');
		$this->load->model('package/packagestorage');
		$this->load->model('package/package');
		$this->load->model('user/userstorage');
		$this->load->model('hotel/hotelstorage');
		$this->load->model('package/addon');
		$this->load->model('booking/booking');
		$this->load->model('hotel/hotelroomrate');
		$this->load->model('surcharge/surchargestorage');
		$booking_code_=array();
		$user_data = $this->userpersistence->getPersistedUser();
		$package_id = $this->session->userdata('package_id');
		$step_booking = $this->session->userdata('step_detail');
		$step_details = $this->session->userdata('step_detail');
		$package = $this->packagestorage->getById($package_id);
		$flight_of_from = $step_details['flight_of_from'];
		$of_from = explode('.',$flight_of_from);
		$of_from_code = $of_from['0']; 
		$of_from_id = $of_from['1'];
		$flight_rf_from = $step_details['flight_rf_from'];
		$rf_from = explode('.',$flight_rf_from);
		$rf_from_code = $rf_from['0']; 
		$rf_from_id = $rf_from['1'];
		$flight_of_to = $step_details['flight_of_to'];
		$of_to = explode('.',$flight_of_to);
		$of_to_code = $of_to['0']; 
		$of_to_id = $of_to['1'];
		$flight_rf_to = $step_details['flight_rf_to'];
		$rf_to = explode('.',$flight_rf_to);
		$rf_to_code = $rf_to['0']; 
		$rf_to_id = $rf_to['1'];  
		$default_string='GE';
		$wang=date('Ymd',strtotime($step_booking['depart_date']));
		$ref_num = $this->bookingstorage->getRefnum();
		$bookingcode=$default_string.$wang.$ref_num[0]->code;
		$booking['user_id'] = $user_data->getUser()->getId(); #get user id // current agent
		$booking['package_id'] = $package_id; #get selected package id
		$booking['departure_date'] = $step_booking['depart_date']; #departure date from the dp
		$booking['return_date'] = $step_booking['return_date']; #return date from the dp
		$booking['rf_num'] = $step_details['flight_rf_no'];
		$booking['rf_from'] = $rf_from_id;
		$booking['rf_to'] = $rf_to_id;
		$booking['rf_departuretime'] = date('H:i:s',strtotime($step_details['flight_rf_date_departure']));
		$booking['rf_arrivaltime'] = date('H:i:s',strtotime($step_details['flight_rf_date_arrival'])); 
		$booking['extended_days'] = $step_details['extended_days']; 
		$booking['rf_remarks'] = $step_details['flight_remarks'];
		$booking['of_num'] = $step_details['flight_of_no'];
		$booking['of_from'] = $of_from_id;
		$booking['of_to'] = $of_to_id;
		$booking['of_departuretime'] =  date('H:i:s',strtotime($step_details['flight_of_date_departure']));
		$booking['of_arrivaltime'] = date('H:i:s',strtotime($step_details['flight_of_date_arrival']));
		$booking['of_remarks'] = $step_details['flight_remarks'];
		$booking['total_cost'] = $step_booking['total_cost'];
		$booking['total_profit'] = $step_booking['total_profit'];
		$booking['total_price'] =  $step_booking['total_payment'];
		$booking['agent_name'] = $step_booking['agent_details_name'];
		$booking['agent_ref'] = $step_booking['agent_details_no'];
		$booking['status'] = 'pending';
		$booking['confirmation_remarks'] = 'Booking details has been sent to the Administrator and Provider. Please wait for confirmation.';
		$booking['date_booked'] = date('Y-m-d');
		$booking['timestamp'] = time();
		$hotelname = $step_booking['package_hotel_choice'];
		$amendedcode=$step_booking['booking_code'];
		$booking['exhotel_id']=$step_booking['exhotel_name'];
		if($step_booking['booking_code'] != NULL || $step_booking['booking_code'] != ''){
			$booking['code'] = $amendedcode; #amended code based on the algorithm
			$booking_code_=$booking['code'];
			$booking['status'] = 'ammended';
			$result = $this->bookingstorage->addBooking($booking);
			$return_result_id=$result['id'];
		}else{
			$booking['code'] = $bookingcode; #generate code based on the algorithm
			$booking_code_=$booking['code'];
			$result = $this->bookingstorage->addBooking($booking);
			$return_result_id=$result['id'];
		}
		$admin = $package[0]->getAdmin();
		$provider = $package[0]->getProvider();
		$agent = $user_data;
		if($return_result_id != NULL && $return_result_id != '' && $return_result_id != 0)
		{
			/*
			*insert into the summary table
			*/
			$summary_data["destination_id"] = $step_details["destination_id"];
			$summary_data["bd_destination_hotel"] = $step_details["bd_destination_hotel"];
			$summary_data["bd_pkg_sur_description"] = $step_details["bd_pkg_sur_description"];
			$summary_data["bd_pkg_sur_computation"] = $step_details["bd_pkg_sur_computation"];
			$summary_data["bd_pkg_sur_subtotal"] = $step_details["bd_pkg_sur_subtotal"];
			$summary_data["bd_hotel_sur_description"] = $step_details["bd_hotel_sur_description"];
			$summary_data["bd_hotel_sur_computation"] = $step_details["bd_hotel_sur_computation"];
			$summary_data["bd_hotel_sur_subtotal"] = $step_details["bd_hotel_sur_subtotal"];
			$summary_data["bd_rs_description"] = $step_details["bd_rs_description"];
			$summary_data["bd_rs_computation"] = $step_details["bd_rs_computation"];
			$summary_data["bd_rs_subtotal"] = $step_details["bd_rs_subtotal"];
			$summary_data["bd_room_description"] = $step_details["bd_room_description"];
			$summary_data["bd_room_computation"] = $step_details["bd_room_computation"];
			$summary_data["bd_room_subtotal"] = $step_details["bd_room_subtotal"];
			$summary_data["bd_addon_description"] = $step_details["bd_addon_description"];
			$summary_data["bd_addon_computation"] = $step_details["bd_addon_computation"];
			$summary_data["bd_addon_subtotal"] = $step_details["bd_addon_subtotal"];
			$summary_data["bd_ext_description"] = $step_details["bd_ext_description"];
			$summary_data["bd_ext_computation"] = $step_details["bd_ext_computation"];
			$summary_data["bd_ext_subtotal"] = $step_details["bd_ext_subtotal"];
			$summary_data["bd_ext_remark"] = $step_details["exremarks"];
			$this->bookingstorage->insertSummaryBookings($summary_data,$return_result_id);
			/*
			*end of insert into the summary table
			*/

			//*Start add bookhotels*//
			foreach ($hotelname as $key) {
				$tmp_hotel = explode('.',$key);
				$hotelname_arr= $tmp_hotel['1'];
				$booking2['booking_id'] =$return_result_id;
				$booking2['package_hotel_id'] = $hotelname_arr;//set as default
				$booking2['checkin_date'] = $step_booking['depart_date'];
				$booking2['checkout_date'] = $step_booking['return_date'];
				$booking2['remarks'] = $step_booking['exhotel_remarks'];
				$booking2['status'] ='pending';
				$result2=$this->bookingstorage->addBookHotels($booking2);
			}
			//*End add bookhotels*//


			/*
			*Start insert database
			*/
			$count_room=count($step_booking['roomrate_quantity']);
			$room=$step_booking['roomrate_quantity'];
			$roomid=$step_booking['roomrate_id'];
			@$roomprice=$step_booking['roomrate_price'];
			$roompax=$step_booking['roomrate_pax'];
			$roomsurcharges=$step_booking['roomrate_id_surcharges'];
			$hotelsurcharges=$step_booking['hotel_surcharge_id'];
			$packagesurcharges=$step_booking['package_surcharge_id'];
			for($i=0;$i<count($room);$i++){
				if(!empty($room[$i])){
					for ($ii=0; $ii < count($roomid); $ii++){ 
						if($i == $ii){
							for ($iii=0; $iii < count($roomid[$ii]); $iii++){
								if(!empty($room[$ii][$iii])){
									$data_room=$room[$ii][$iii];
									$data_arr_=$return_result_id;
									$data_arr=$result2['id'];
									$data_roomid=$roomid[$ii][$iii];
									$data_roomsurcharges=$roomsurcharges[$ii][$iii];
									@$data_hotelsurcharges=$hotelsurcharges[$ii][$iii];
									@$data_packagesurcharges=$packagesurcharges[$ii][$iii];
									$this->bookingstorage->addBookHotelrooms($data_roomid,$data_arr,$data_room);
									if($data_hotelsurcharges != 0){
										$this->bookingstorage->addappliedhotelsurcharges($data_hotelsurcharges,$data_arr,$data_room);
									}
									if($data_packagesurcharges != 0){
										$this->bookingstorage->addappliedpackagesurcharges($data_packagesurcharges,$data_arr_,$data_room);
									}
									if($roomsurcharges[$ii][$iii] != 0){
										$tmp_arr=explode(",",$roomsurcharges[$ii][$iii]);
										foreach ($tmp_arr as $key) {
											$data_arr_=$key;
											$this->bookingstorage->addappliedhotelroomsurcharges($data_arr_,$data_arr,$data_room);
										}
									}
								}
							}
						}
					}
				}
			}
			/*
			*
			*End insert database
			*
			*/
			//*Start add addappliedaddons*//
			if($step_booking['addon_quantity'] != NULL && $step_booking['addon_quantity'] !=0 && $step_booking['addon_quantity'] !=''){
				foreach($step_booking['addon_quantity'] as $k => $qty){
					if($qty != '' && $step_booking['addon_id'][$k] != ''){	
						$appliedaddons['booking_id'] =$return_result_id;
						$appliedaddons['addon_id'] = $step_booking['addon_id'][$k]; //set as default
						$appliedaddons['quantity'] = $qty;
						$result6=$this->bookingstorage->addappliedaddons($appliedaddons);
					}
				}
			}
			//*End add addappliedaddons*//
			//*Start add addtravellerdetails*//
			if(@$step_details['adult_name'] != 0 && @$step_details['adult_name'] !=''){
				foreach($step_details['adult_name'] as $k => $qty){
					if($qty != '' && $step_details['adult_name'][$k] != ''){
						$travellerdetails['booking_id']=$return_result_id;
						$travellerdetails['user_id'] =$this->session->userdata('id');
						$travellerdetails['name'] = @$step_details['adult_name'][$k];
						$travellerdetails['date_of_birth'] = @$step_details['adult_dob'][$k];
						$travellerdetails['passport_no'] = @$step_details['adult_ppno'][$k];
						$travellerdetails['type'] ="Adult";
						$travellerdetails['status'] ='0';
						$this->bookingstorage->addtravellerdetails($travellerdetails);
					}
				}
		    }
			if(@$step_details['child_name'] != 0 && @$step_details['child_name'] !=''){
				foreach($step_details['child_name'] as $k => $qty){
					if($qty != '' && $step_details['child_name'][$k] != ''){
						$travellerdetails['booking_id']=$return_result_id;
						$travellerdetails['user_id'] =$this->session->userdata('id');
						$travellerdetails['name'] = @$step_details['child_name'][$k];
						$travellerdetails['passport_no'] = @$step_details['child_ppno'][$k];
						$travellerdetails['date_of_birth'] = @$step_details['child_dob'][$k];
						$travellerdetails['type'] ="Child";
						$travellerdetails['status'] ='0';
						$this->bookingstorage->addtravellerdetails($travellerdetails);
					}
				}
			}
			if(@$step_details['infant_name'] != 0 && @$step_details['infant_name'] !=''){
				foreach($step_details['infant_name'] as $k => $qty){
					if($qty != '' && $step_details['infant_name'][$k] != ''){
						$travellerdetails['booking_id']=$return_result_id;
						$travellerdetails['user_id'] =$this->session->userdata('id');
						$travellerdetails['name'] = @$step_details['infant_name'][$k];
						$travellerdetails['passport_no'] = @$step_details['infant_ppno'][$k];
						$travellerdetails['date_of_birth'] = @$step_details['infant_dob'][$k];
						$travellerdetails['type'] ="Infant";
						$travellerdetails['status'] ='0';
						$this->bookingstorage->addtravellerdetails($travellerdetails);
					}
				}
		    }
			/*}*/
			//*End add addtravellerdetails*//
			$conc = '';
			$booking = array();
			$booking['step_booking'] = $step_booking;
			$booking['user'] = $user_data;
			$booking['package'] = $package;
			$booking['admin'] = $admin;
			$booking['provider'] = $provider;
			$booking_ids = $this->booking->getBookingIDs($booking_code_);
			$booking_summary = false;
			$booking_summary_provider = false;
			if(count($booking_ids)){
				for($i=0;$i<count($booking_ids);$i++){
					$booking_summary[] = $this->getSummaryViewByEmail_agent($booking_ids[$i]["id"]);
					$booking_summary_provider[] = $this->getSummaryViewByEmail_provider($booking_ids[$i]["id"]);
				}
			}
			// dump($booking_summary);
			// dump($booking_summary_provider);
			// die;
			$booking['booking_id_mapper'] = $return_result_id;
			$booking['booking_summary'] = $booking_summary;
			$booking['booking_summary_provider'] = $booking_summary_provider;
			$booking_stored=1;
			if($booking_stored){//una nga email
				$this->load->helper('string');
				$toHash = random_string('alpha', 10);
				$hash = hashmonster($toHash);
				$this->db->insert('confirmation_booking', array(
					'booking_id' => $return_result_id,
					'hash' => $hash
				));
				$link = base_url('change/confirm_booking/'.$hash);
				if($this->booking_confirmation_customer($link,$booking) > 0 ){
					$conc .= ' Booking details has been sent to the Agent and the Administrator. Please wait for confirmation.';
				}
				if($this->booking_confirmation_provider_customer($link,$booking) > 0){
					$conc .= ' Booking details has been sent to the Provider. Please wait for confirmation.';
				}
			
			redirect('panel/package/'.$return_result_id);
			}
    		echo json_encode(array(
				'alert_type'=>'Success',
				'message'=>'Successfully added.'
		 	));
		}else{
    		echo json_encode(array(
		 		'alert_type'=>'Error',
		 		'message'=>$result
		 	));
    	}
	}

	/*
	*
	*End booking function 
	*
	*/

	public function booking_confirmation_customer($link='', $booking){
	    if(!$link) return;
     	//$cnt = 0;
     	$mail_customer=$booking['booking_id_mapper'];
		$this->load->model('booking/booking');
		$recipients=$this->booking->getEmailadmin();
		$get_customer_email=$this->booking->getEmailcustomer($mail_customer);
		//dump($get_customer_email);
		//dump($get_customer_email[0]->customeremail);
		//die();
        $mail = $this->mail;
    	$mail->setFrom('no-reply@ge.com.sg', 'Global Explorer');						// Sender
    	//$mail->addReplyTo('no-reply@ge.com.sg', 'Global Explorer');						// Sender's optional
    	
    	$mail->WordWrap = 50;                                 							// Set word wrap to 50 characters
	    $mail->isHTML(true);                                  							// Set email format to HTML
	    $mail->Subject = 'Booking Confirmation [Global Explorer]';
     	if($get_customer_email != NULL || $get_customer_email != ""){
		    //foreach ($get_customer_email as $key) {
				$booking['recipient'] =$get_customer_email[0]->customerfname.' '.$get_customer_email[0]->customerlname;					
	     		$mail->addAddress($get_customer_email[0]->customeremail,$booking['recipient']);
		    	$mail->Body    = $this->load->view('booking_confirmation',$booking,TRUE);
			    //if($mail->send()) $cnt++;
			//}
		}
     	if($recipients != NULL || $recipients != ""){
     		foreach($recipients as $row){
	     		$booking['recipient'] = $row->adminfname.' '.$row->adminlname;
	     		$booking['recipient_id'] = $row->adminid;
	     		if($row->alteremail != '' || $row->alteremail != NULL)
		    	{
			     	$alternative_email=explode(',',$row->alteremail);
		    		for ($i=0; $i < count($alternative_email); $i++) {
			     	$mail->addCC($alternative_email[$i],$booking['recipient']);
					}
			    }
		    	$mail->addCC($row->adminemail, $booking['recipient']);     		// Add a recipient (Admin)
	    		$mail->Body    = $this->load->view('booking_confirmation',$booking,TRUE);
		    	//if($mail->send()) $cnt++;
     		}
     	}
    	$mail->AltBody = 'Booking Confirmation [Global Explorer]';
    	if($mail->send());
     	//return $cnt;
    }

    public function booking_confirmation_provider_customer($link='', $booking){
	    if(!$link) return;
	    $booking['confirm'] = $link.'/confirm';
	    $booking['reject'] = $link.'/reject';
	    $booking['cancel'] = $link.'/cancel';
	    $this->load->model('booking/booking');
     	//$cnt = 0;
		$mail_agent=$booking['booking_id_mapper'];
		$recipients=$this->booking->getEmailadmin();
		$get_provider_email=$this->booking->getEmailprovider($mail_agent);
        
        $mail = $this->mail;
    	$mail->setFrom('no-reply@ge.com.sg', 'Global Explorer');						// Sender
    	//$mail->addReplyTo('no-reply@ge.com.sg', 'Global Explorer');						// Sender's optional
    	
    	$mail->WordWrap = 50;                                 							// Set word wrap to 50 characters
	    $mail->isHTML(true);                                  							// Set email format to HTML
	    $mail->Subject = 'Booking Confirmation [Global Explorer]';
		if($get_provider_email != NULL || $get_provider_email != ""){
		    //foreach ($get_provider_email as $key) {
				$booking['recipient'] =$get_provider_email[0]->providerfname.' '.$get_provider_email[0]->providerlname;
	 			$booking['recipient_id'] = $get_provider_email[0]->providerid;
	     		if($get_provider_email[0]->alteremail != "" || $get_provider_email[0]->alteremail != NULL){
					$wang=explode(',',$get_provider_email[0]->alteremail);
		    		for ($i=0; $i < count($wang); $i++) {
			     		$mail->addAddress($wang[$i],$booking['recipient']);
					}
				}   						
	     		$mail->addAddress($get_provider_email[0]->provideremail,$booking['recipient']);
		    	$mail->Body    = $this->load->view('booking_confirmation_provider',$booking,TRUE);
			    //if($mail->send()) $cnt++;
			//}
		}

     	if($recipients != NULL || $recipients != ""){
     		foreach($recipients as $row){
	     		$booking['recipient'] = $row->adminfname.' '.$row->adminlname;
	     		$booking['recipient_id'] = $row->adminid;
	     		if($row->alteremail != '' || $row->alteremail != NULL)
		    	{
			     	$alternative_email=explode(',',$row->alteremail);
			     	for ($i=0; $i < count($alternative_email); $i++) {
			     		$mail->addCC($alternative_email[$i],$booking['recipient']);
					}
			    }
		    	$mail->addCC($row->adminemail, $booking['recipient']);     		// Add a recipient (Admin)
	    		$mail->Body    = $this->load->view('booking_confirmation_provider',$booking,TRUE);
		    	//if($mail->send()) $cnt++;
     		}
     	}
     	//$mail->Body    = $this->load->view('booking_confirmation_provider',$booking,TRUE);
    	$mail->AltBody = 'Booking Confirmation [Global Explorer]';
		if($mail->send());
     	//return $cnt;
    }

	public function create_booking(){
		// print_r($_POST);
		$conc = '';
		$this->session->set_userdata('stepthree',$_POST);
		if ('home/create_booking' == uri_string()) redirect('booking');
		

		//after storage of booking
		$booking_stored=1;
		if($booking_stored){
			$this->load->helper('string');
			$toHash = random_string('alpha', 10);
			$hash = hashmonster($toHash);

			// $this->db->insert('confirmation_booking', array(
			// 	'booking_id' => $this->booking->getId(),
			// 	'hash' => $hash,
			// 	'timestamp' => time(),
			// ));
			
			$link = base_url('home/confirm_booking/' . $hash);
			if($this->booking_confirmation($link)){
				$conc = ' Booking details has been sent to the Provider. Please wait for confirmation.';
			}
			
			$this->session->unset_userdata('stepone');
			$this->session->unset_userdata('steptwo');
			$this->session->unset_userdata('stepthree');

			$this->load->helper('url');
			redirect('home/index'); //for now..
		}
	}

	//from old confirmation.. 
	//edited but please recheck this Joshua
	public function confirm_booking($arg = null, $action='confirm') {
		if (!$arg) return;
		if ($action === 'confirm') {
			$result = $this->db->get_where('confirmation_booking', array('hash' => $arg))->result_array();
			if (count($result) == 1) {
				
				$this->db->where('id', $result[0]['booking_id']);
				$this->db->update('bookings', array('status' => 'confirmed'));
				
				echo "Success";
			}
		}
	}

    public function forgot_password(){
    	$this->load->model('user/user');
		$this->load->model('user/userfactory');
		$this->load->model('user/userstorage');
		$this->load->helper('email');
		$email = $this->input->post('email');
		if (valid_email($email))
		{
			if ($email) {
				$this->userstorage->includeAll(true);
				$updateUser = $this->userstorage->getByInformation(array('email' => $email));
				if ($updateUser) {
					$updateUser = $updateUser[0];
					$this->load->helper('string');
					$newPass = random_string('alpha', 8);
					$updateUser->getUser()->setPassword( $newPass );
					
					if ($this->send_new_password($updateUser,  $newPass)) {
					// if (!1) {
						$this->userstorage->store($updateUser);
						echo json_encode(array(
							'alert_type'=>'Success',
							'message'	=>'New password sent to your email.'
						));
					} else {
						echo json_encode(array(
							'alert_type'=>'Error',
							'message'	=>'Password not sent. Please try again.'
						));
					}

				} else {
					echo json_encode(array(
						'alert_type'=>'Error',
						'message'	=>'Email not found. Please register using this email.'
					));
				}
			} else {
				echo json_encode(array(
					'alert_type'=>'Error',
					'message'	=>'You have not entered a valid email.'
				));
			}
		}
    }

    public function send_new_password($user, $newPass){
		$receiver_name = $user->getUser()->getFirstname().' '.$user->getUser()->getLastname();
		$receiver_email = $user->getUser()->getEmail();
		
		$data['user']['name'] = $receiver_name;
		$data['user']['password'] = $newPass;

        $mail = $this->mail;
    	$mail->setFrom('no-reply@ge.com.ge', 'Global Explorer');						// Sender recipient
		//$mail->addReplyTo('no-reply@ge.com.ge', 'Global Explorer');						// Sender's optional
	    $mail->addAddress($receiver_email, $receiver_name);								// Add a recipient

	    $mail->WordWrap = 50;															// Set word wrap to 50 characters
	    $mail->isHTML(true);															// Set email format to HTML
	    $mail->Subject = 'New Password [Global Explorer]';
	    $mail->Body    = $this->load->view('home/email_newpassword',$data,TRUE);

    	$mail->AltBody = 'New Password [Global Explorer]';

		if ($mail->send()) {
			return true;
		} else return false;
	}

	public function sending_mail_everyday(){
		$this->load->model('booking/booking');
		$book['booking_every_day']=$this->booking->getBookingEveryDay();
		
		$mail = $this->mail;
        $mail->setFrom('no-reply@ge.com.sg', 'Global Explorer');						// Sender
    	$mail->WordWrap = 50;                                 							// Set word wrap to 50 characters
	    $mail->isHTML(true);                                  							// Set email format to HTML
	    $mail->Subject = 'Daily Booking Notification [Global Explorer]';
	    $recipients=$this->booking->getEmailadmin();

     	if($recipients != NULL || $recipients != ""){
     		foreach($recipients as $row){
				$mail->ClearAddresses();
				$mail->ClearCCs();
	     		$booking['recipient'] = $row->adminfname.' '.$row->adminlname;
	     		if($row->alteremail != '' || $row->alteremail != NULL)
		    	{
			     	$alternative_email=explode(',',$row->alteremail);
			     	for ($i=0; $i < count($alternative_email); $i++) {
			     		$mail->addCC($alternative_email[$i],$booking['recipient']);
					}
			    }
		    	$mail->addAddress($row->adminemail, $booking['recipient']);     				// Add a recipient (Admin)
	    		$mail->Body    = $this->load->view('panel/mainpanel/email_summary',$book,true);
		    }
     	}

     	$mail->AltBody = 'Daily Booking Notification [Global Explorer]';
		if($mail->send());
	}

	public function sending_mail_everymonth_test(){
		$this->load->model('booking/booking');
		$recipients=$this->booking->getEmailadmin();
		$mail = $this->mail;
        $mail->setFrom('no-reply@ge.com.sg', 'Global Explorer');						// Sender
    	$mail->WordWrap = 50;                                 							// Set word wrap to 50 characters
	    $mail->isHTML(true);                                  							// Set email format to HTML
		$book['booking_every_month']=$this->booking->getBookingMonthlyRecipients();
		if(count($book['booking_every_month']) > 0){
			foreach ($book['booking_every_month'] as $key) {
			$books['count_pax']=$this->booking->getByGuest($key['id']);
			@$book['count_pax']=$books['count_pax'][0]->PAX;
			@$book['id']=$key['id'];
			@$book['user_data']=count($key['id']);
			@$book['title']=$key['title'];
			@$book['firstname']=$key['firstname'];
			@$book['lastname']=$key['lastname'];
			@$book['PAXNAME']=$key['PAXNAME'];
			@$book['numberPax']=$key['numberPax'];
			@$book['code']=$key['code'];
			@$book['total']=$key['total_price'];
				$book['mail_checker']=$key['email'];
					$mail->ClearAddresses();
					$mail->ClearCCs();
			 		@$book['recipient'] = $key['firstname'].' '.$key['lastname'];
		    		$mail->Subject = ''.strtoupper($key['name']).' - '.date("m",strtotime("-1 month")).' - MSOA [Agent]';
			 		if(@$key['alernative_email'] != '' || @$key['alernative_email'] != NULL) {
			    		$alternative_email=explode(',',@$key['alernative_email']);
				     	for ($i=0; $i < count($alternative_email); $i++) {
				     		//$mail->addCC($alternative_email[$i],$book['recipient']);
				     		//$mail->addCC('delabahan@gmail.com', $book['recipient']);
				     		//$mail->addCC('dion@azazabizsolutions.com', $book['recipient']);
						}
				    }
			    	//$mail->addAddress($key['email'], $book['recipient']);    				// Add a recipient (agent)
			    	//$mail->addCC('dion@azazabizsolutions.com', $book['recipient']);
			    	//$mail->addAddress('delabahan@gmail.com', $book['recipient']);

				    // if($recipients != NULL || $recipients != ""){
			     // 		foreach($recipients as $row){
				    //  		$book['recipient'] = $row->adminfname.' '.$row->adminlname;
				    //  		$book['recipient_id'] = $row->adminid;
				    //  		if($row->alteremail != '' || $row->alteremail != NULL) {
						  //    	$alternative_email=explode(',',$row->alteremail);
						  //    	for ($i=0; $i < count($alternative_email); $i++) {
						  //    		//$mail->addCC($alternative_email[$i],$book['recipient']);
						  //    		//$mail->addCC('delabahan@gmail.com', $book['recipient']);
						  //    		//$mail->addCC('dion@azazabizsolutions.com', $book['recipient']);
								// }
						  //   }
					   //  	//$mail->addAddress($row->adminemail, $book['recipient']);     		// Add a recipient (Admin)
					   //  	//$mail->addAddress('delabahan@gmail.com', $book['recipient']);
					   //  	//$mail->addCC('dion@azazabizsolutions.com', $book['recipient']);
			     // 		}
			     // 	}
					$mail->Body    = $this->load->view('panel/mainpanel/email_summary_monthly',$book,TRUE);
					if($mail->send());
			}
			$mail->AltBody = 'Monthly Booking Notification [Global Explorer]';
		}else{
			redirect('errors/restricted_area_for_agent');
		}
	}


	public function sending_mail_everymonth(){
		$this->load->model('booking/booking');
		$recipients=$this->booking->getEmailadmin();
		$mail = $this->mail;
        $mail->setFrom('no-reply@ge.com.sg', 'Global Explorer');						// Sender
    	$mail->WordWrap = 50;                                 							// Set word wrap to 50 characters
	    $mail->isHTML(true);                                  							// Set email format to HTML
		$book['booking_every_month']=$this->booking->getBookingMonthlyRecipients();
		for ($i=0; $i < count($book['booking_every_month']); $i++) { 
			$book['booking_every_month'][$i]["user_data"] = $this->booking->getBookingMonthlys($book['booking_every_month'][$i]["user_id"]);
		}
		//traveller data
		foreach ($book['booking_every_month'] as $value) {
			foreach ($value['user_data'] as $user_data) {
				$books[]=$this->booking->getByGuest($user_data['id']);
				//$pax[]=$this->booking->getByPax($user_data['id']);
			}
		}
		@$book['cust']=$books;
		foreach ($book['booking_every_month'] as $key) {
			@$book['user_data']=$key['user_data'];
			if($key['user_data'] == "" || $key['user_data'] == NULL){
			}else{
			if($key['email'] != "" || $key['email'] != NULL){

				$book['mail_checker']=$key['email'];
				$mail->ClearAddresses();
				$mail->ClearCCs();
		 		@$book['recipient'] = $key['firstname'].' '.$key['lastname'];
		 		$mail->Subject = 'Statement of Account for the Month of '.date("F",strtotime("-1 month"));
	    		//$mail->Subject = ''.strtoupper($key['name']).' - '.date("m",strtotime("-1 month")).' - MSOA [Agent]';

		 		if(@$key['alernative_email'] != '' || @$key['alernative_email'] != NULL) {
		    		$alternative_email=explode(',',@$key['alernative_email']);
			     	for ($i=0; $i < count($alternative_email); $i++) {
			     		$mail->addCC($alternative_email[$i],$book['recipient']);
			     		//$mail->addCC('delabahan@gmail.com', $book['recipient']); 
					}
			    }
		    	$mail->addAddress($key['email'], $book['recipient']);    				// Add a recipient (agent)
		    	//$mail->addAddress('delabahan@gmail.com', $book['recipient']);     				// Add a recipient (agent)
		    	//$mail->addAddress('dion@azazabizsolutions.com', $book['recipient']);     				// Add a recipient (agent)

			    if($recipients != NULL || $recipients != ""){
		     		foreach($recipients as $row){
			     		$book['recipient'] = $row->adminfname.' '.$row->adminlname;
			     		$book['recipient_id'] = $row->adminid;
			     		if($row->alteremail != '' || $row->alteremail != NULL) {
					     	$alternative_email=explode(',',$row->alteremail);
					     	for ($i=0; $i < count($alternative_email); $i++) {
					     		$mail->addBCC($alternative_email[$i],$book['recipient']);
					     		//$mail->addCC('delabahan@gmail.com', $book['recipient']); 
							}
					    }
				    	$mail->addBCC($row->adminemail, $book['recipient']);     		// Add a recipient (Admin)
				    	//$mail->addAddress('delabahan@gmail.com', $book['recipient']);     		// Add a recipient (Admin)
		     		}
		     	}
				$mail->Body    = $this->load->view('panel/mainpanel/email_summary_monthly_', $book,TRUE);
				if($mail->send());
			}
		  }
		}
		$mail->AltBody = 'Monthly Booking Notification [Global Explorer]';
	}

	public function sending_mail_everymonth_provider(){
		$this->load->model('booking/booking');
		$recipients=$this->booking->getEmailadmin();
		$mail = $this->mail;
        $mail->setFrom('no-reply@ge.com.sg', 'Global Explorer');						// Sender
    	$mail->WordWrap = 50;                                 							// Set word wrap to 50 characters
	    $mail->isHTML(true);                                  							// Set email format to HTML
		
		//list of booking
		$book['booking_every_month']=$this->booking->getBookingMonthlyRecipientsProviders();

		//user data
		for ($i=0; $i < count($book['booking_every_month']); $i++) { 
			$book['booking_every_month'][$i]["user_data"] = $this->booking->getBookingMonthlysProvider($book['booking_every_month'][$i]["user_id"]);
		}
		//traveller data
		foreach ($book['booking_every_month'] as $value) {
			foreach ($value['user_data'] as $user_data) {
				$books[]=$this->booking->getByGuest($user_data['id']);
			}
		}
		@$book['cust']=$books;
		foreach ($book['booking_every_month'] as $key) {
			@$book['user_data']=$key['user_data'];

			if($key['user_data'] == "" || $key['user_data'] == NULL){
			}else{
			if($key['provideremail'] != "" || $key['provideremail'] != NULL){
				$book['mail_checker']=$key['email'];
				$book['provider']='provider';
				$mail->ClearAddresses();
				$mail->ClearCCs();
		 		@$book['recipient'] = $key['providerfirstname'];
	    		$mail->Subject = 'Monthly Booking Notification ['.$key['user_data'][0]['name'].']';

		 		if(@$key['provideralternativeemail'] != '' || @$key['provideralternativeemail'] != NULL) {
		    		$alternative_email=explode(',',@$key['provideralternativeemail']);
			     	for ($i=0; $i < count($alternative_email); $i++) {
			     		$mail->addCC($alternative_email[$i],$book['recipient']);
			     		//$mail->addCC('delabahan@gmail.com', $book['recipient']); 
					}
			    }
		    	$mail->addAddress($key['provideremail'], $book['recipient']);    				// Add a recipient (agent)
		    	//$mail->addAddress('anuar@azazabizsolutions.com', $book['recipient']);    				// Add a recipient (agent)

			    if($recipients != NULL || $recipients != ""){
		     		foreach($recipients as $row){
			     		$book['recipient'] = $row->adminfname.' '.$row->adminlname;
			     		$book['recipient_id'] = $row->adminid;
			     		if($row->alteremail != '' || $row->alteremail != NULL) {
					     	$alternative_email=explode(',',$row->alteremail);
					     	for ($i=0; $i < count($alternative_email); $i++) {
					     		$mail->addCC($alternative_email[$i],$book['recipient']);
					     		//$mail->addCC('delabahan@gmail.com', $book['recipient']); 
							}
					    }
				    	$mail->addAddress($row->adminemail, $book['recipient']);     		// Add a recipient (Admin)
				    	//$mail->addAddress('anuar@azazabizsolutions.com', $book['recipient']);     		// Add a recipient (Admin)
		     		}
		     	}
				$mail->Body    = $this->load->view('panel/mainpanel/email_summary_monthly', $book,TRUE);
				if($mail->send());
			}
		  }
		}
		$mail->AltBody = 'Monthly Booking Notification [Global Explorer]';
	}
	
	public function sending_mail_everymonth_providers(){
		$this->load->model('booking/booking');
		$recipients=$this->booking->getEmailadmin();
		$mail = $this->mail;
        $mail->setFrom('no-reply@ge.com.sg', 'Global Explorer');						// Sender
    	$mail->WordWrap = 50;                                 							// Set word wrap to 50 characters
	    $mail->isHTML(true);                                  							// Set email format to HTML
		
		//list of booking
		$book['booking_every_month']=$this->booking->getBookingMonthlyRecipientsProviders();
		$book['booking_every']=$this->booking->getBookingMonthlyProviders();

		dump_exit($book['booking_every']);
		//user data
		for ($i=0; $i < count($book['booking_every_month']); $i++) { 
			$book['booking_every_month'][$i]["user_data"] = $this->booking->getBookingMonthlysProvider($book['booking_every_month'][$i]["user_id"]);
		}
		//traveller data
		foreach ($book['booking_every_month'] as $value) {
			foreach ($value['user_data'] as $user_data) {
				$books[]=$this->booking->getByGuest($user_data['id']);
			}
		}
		@$book['cust']=$books;
		foreach ($book['booking_every_month'] as $key) {
			@$book['user_data']=$key['user_data'];

			if($key['user_data'] == "" || $key['user_data'] == NULL){
			}else{
			if($key['provideremail'] != "" || $key['provideremail'] != NULL){
				$book['mail_checker']=$key['email'];
				$book['provider']='provider';
				$mail->ClearAddresses();
				$mail->ClearCCs();
		 		@$book['recipient'] = $key['providerfirstname'];
	    		$mail->Subject = 'Monthly Booking Notification ['.$key['user_data'][0]['name'].']';

		 		if(@$key['provideralternativeemail'] != '' || @$key['provideralternativeemail'] != NULL) {
		    		$alternative_email=explode(',',@$key['provideralternativeemail']);
			     	for ($i=0; $i < count($alternative_email); $i++) {
			     		$mail->addCC($alternative_email[$i],$book['recipient']);
			     		//$mail->addCC('delabahan@gmail.com', $book['recipient']); 
					}
			    }
		    	$mail->addAddress($key['provideremail'], $book['recipient']);    				// Add a recipient (agent)
		    	//$mail->addAddress('anuar@azazabizsolutions.com', $book['recipient']);    				// Add a recipient (agent)

			    if($recipients != NULL || $recipients != ""){
		     		foreach($recipients as $row){
			     		$book['recipient'] = $row->adminfname.' '.$row->adminlname;
			     		$book['recipient_id'] = $row->adminid;
			     		if($row->alteremail != '' || $row->alteremail != NULL) {
					     	$alternative_email=explode(',',$row->alteremail);
					     	for ($i=0; $i < count($alternative_email); $i++) {
					     		$mail->addCC($alternative_email[$i],$book['recipient']);
					     		//$mail->addCC('delabahan@gmail.com', $book['recipient']); 
							}
					    }
				    	$mail->addAddress($row->adminemail, $book['recipient']);     		// Add a recipient (Admin)
				    	//$mail->addAddress('anuar@azazabizsolutions.com', $book['recipient']);     		// Add a recipient (Admin)
		     		}
		     	}
				$mail->Body    = $this->load->view('panel/mainpanel/email_summary_monthly', $book);
				if($mail->send());
			}
		  }
		}
		$mail->AltBody = 'Monthly Booking Notification [Global Explorer]';
	}

	public function pdf(){
		$uir_res=$this->uri->segment(3);
		$key ='~!@#$%^&*()_+ hacker is not allowed this area!';
		$booking_id=$this->decrypt($uir_res, $key);
		$this->load->library('pdf');
		$this->load->model('booking/booking');
		$this->load->model("booking/bookingstorage");
		$user_data = $this->userpersistence->getPersistedUser();
		$summary_data['guest']=$this->booking->getByGuest($booking_id);
		$summary_data['invoice_pdf']=$this->booking->getBysummary($booking_id);
		//$summary_data['provider_summary']=$this->booking->getBysummaryCompany($booking_id);
		$summary_data['agent_summary']=$this->booking->getBysummaryCompanyAgent($booking_id);
		$summary_data['room_summary']=$this->booking->getBysummaryRoomtype($booking_id);
        #load the booking storage model ----------------------------------------------
       
        #get the individual summaries ----------------------------------------------
        $package_summary = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"type"=>"0package_surcharge"));
        $addon_summary = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"type"=>"4addon"));
        $extension_summary = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"type"=>"5extension"));
        
        #fetch the destination IDs ----------------------------------------------
        $destination_summary = $this->bookingstorage->getSummaryDestinations($booking_id);

        for($i=0;$i<count($destination_summary);$i++){
        	$destination = $destination_summary[$i];
        	#fetch the hotel, room surcharge, room summaries ----------------------------------------------
            $hotel_summary[] = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"destination_id"=>$destination["destination_id"],"type"=>"1hotel_surcharge"));
            $rs_summary[] = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"destination_id"=>$destination["destination_id"],"type"=>"2room_surcharge"));
            $r_summary[] = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"destination_id"=>$destination["destination_id"],"type"=>"3room_payment"));    
        }

    	$summary_data['package_summary']=$package_summary;
    	$summary_data['addon_summary']=$addon_summary;
    	$summary_data['extension_summary']=$extension_summary;
    	$summary_data['destination_summary']=$destination_summary;
    	$summary_data['hotel_summary']=$hotel_summary;
    	$summary_data['rs_summary']=$rs_summary;
    	$summary_data['r_summary']=$r_summary;
    	$summary_data['user']=$user_data;
    	
		$this->pdf->set_paper( "letter", "portrait" ); 
		$this->pdf->load_view('home/invoice_pdf',$summary_data);
		$this->pdf->render();
		$this->pdf->stream('invoice.pdf');
		//$this->pdf->stream('invoice.pdf',array('Attachment'=>0));
	}

	public function pdfss(){
		//$booking_id=$this->uri->segment(3,0);
		$uir_res=$this->uri->segment(3);
		$key ='~!@#$%^&*()_+ hacker is not allowed this area!';
		$booking_id=$this->decrypt($uir_res, $key);
		$this->load->library('pdf');
		$this->load->model('booking/booking');
		$this->load->model("booking/bookingstorage");
		$user_data = $this->userpersistence->getPersistedUser();
		$summary_data['guest']=$this->booking->getByGuest($booking_id);
		$summary_data['invoice_pdf']=$this->booking->getBysummary($booking_id);
		//$summary_data['provider_summary']=$this->booking->getBysummaryCompany($booking_id);
		$summary_data['agent_summary']=$this->booking->getBysummaryCompanyAgent($booking_id);
		$summary_data['room_summary']=$this->booking->getBysummaryRoomtype($booking_id);
        #load the booking storage model ----------------------------------------------
       
        #get the individual summaries ----------------------------------------------
        $package_summary = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"type"=>"0package_surcharge"));
        $addon_summary = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"type"=>"4addon"));
        $extension_summary = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"type"=>"5extension"));
        
        #fetch the destination IDs ----------------------------------------------
        $destination_summary = $this->bookingstorage->getSummaryDestinations($booking_id);

        for($i=0;$i<count($destination_summary);$i++){
        	$destination = $destination_summary[$i];
        	#fetch the hotel, room surcharge, room summaries ----------------------------------------------
            $hotel_summary[] = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"destination_id"=>$destination["destination_id"],"type"=>"1hotel_surcharge"));
            $rs_summary[] = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"destination_id"=>$destination["destination_id"],"type"=>"2room_surcharge"));
            $r_summary[] = $this->bookingstorage->getSummaryInformation(array("booking_id"=>$booking_id,"destination_id"=>$destination["destination_id"],"type"=>"3room_payment"));    
        }

    	$summary_data['package_summary']=$package_summary;
    	$summary_data['addon_summary']=$addon_summary;
    	$summary_data['extension_summary']=$extension_summary;
    	$summary_data['destination_summary']=$destination_summary;
    	$summary_data['hotel_summary']=$hotel_summary;
    	$summary_data['rs_summary']=$rs_summary;
    	$summary_data['r_summary']=$r_summary;
    	$summary_data['user']=$user_data;
    	
		$this->pdf->set_paper( "letter", "portrait" ); 
		$this->pdf->load_view('home/invoice_pdf',$summary_data);
		$this->pdf->render();
		$this->pdf->stream('invoice.pdf');
		//$this->pdf->stream('invoice.pdf',array('Attachment'=>0));
	}

	public function flash_message($message, $do_redirect=FALSE, $redirect_location=''){
		$this->session->set_flashdata('flash_message', $message);
		if($do_redirect) redirect($redirect_location);
	}


	public function encrypt($id, $key)
	{
	    $id = base_convert($id, 35, 36); // Save some space
	    $data = mcrypt_encrypt(MCRYPT_BLOWFISH, $key, $id, 'ecb');
	    $data = bin2hex($data);

	    return $data;
	}

	public function decrypt($encrypted_id, $key)
	{
	    $data = pack('H*', $encrypted_id); // Translate back to binary
	    $data = mcrypt_decrypt(MCRYPT_BLOWFISH, $key, $data, 'ecb');
	    $data = base_convert($data, 36, 35);

	    return $data;
	}

	public function anuar(){
		echo "Gwapo";
	}

	public function cron_emaildepart(){
		$this->load->model('booking/booking');
		$this->load->model('user/user');
		$this->load->model('user/customer');
		$this->load->model('hotel/hotel');
		$date = date('Y-m-d');
		$this->data['departure_list']    = $this->booking->get_by(array('departure_date =' => date('Y-m-d',strtotime($date."+ 2 days")), 'status' => 'confirmed'));
		$departures =  (array) $this->data['departure_list'];
		$of_from = $this->booking->get_airportcode($this->data['departure_list'][count($this->data['departure_list'])-1]->of_from);
		$distinct = $this->booking->get_distinct();
		//providers
		foreach($distinct as $d)
		{
			$this->db->select('company_designations.user_id,users.email,users.alternative_email');
			$this->db->join('company_designations','packages.provider_id=company_designations.company_id');
			$this->db->join('users','company_designations.user_id=users.id');
			$query = $this->db->from('packages')->where('packages.id',$d->package_id)->get()->result();
			$data['departure_list'] = $this->booking->get_by(array('id'=>$d->id));
			print_r($data['departure_list']);
			echo $query[0]->email;
			$mail = $this->mail;
			$mail->ClearAddresses();  // each AddAddress add to list
			$mail->ClearCCs();
			$mail->ClearBCCs();
		    $mail->setFrom('no-reply@ge.com.sg', 'Global Explorer');						// Sender
			$mail->WordWrap = 50;                                 							// Set word wrap to 50 characters
		    $mail->isHTML(true);                                  							// Set email format to HTML
		    $mail->Subject = 'Booking Arrival Reminders';
     		if($query[0]->alternative_email != '' || $query[0]->alternative_email != NULL)
	    	{
		     	$alternative_email=explode(',',$query[0]->alternative_email);
		     	for ($i=0; $i < count($alternative_email); $i++) {
		     		$mail->addCC($alternative_email[$i]);
				}
		    }
	    	$mail->addAddress($query[0]->email);
    		$mail->Body    = $this->load->view('panel/mainpanel/email_summary_departure',$data,true);
		 	$mail->AltBody = 'Booking Arrival Reminders';
			if($mail->send()):
				echo "sent";
			else:
				echo "failed";
			endif;
		}
	// 	foreach($distinct as $d)
	// 	{
	// 		echo $d->user_id;
	// 		$agent = $this->db->get_where('users', array('id'=>$d->user_id))->result();
	// 		//print_r($agent[0]->email);
	// 		$data['departure_list'] = $this->booking->get_by(array('departure_date =' => date('Y-m-d',strtotime($date."+ 2 days")), 'status' => 'confirmed','user_id'=>$d->user_id));
	// 		$mail = $this->mail;
	// 		$mail->ClearAddresses();  // each AddAddress add to list
	// 		$mail->ClearCCs();
	// 		$mail->ClearBCCs();
	// 	    $mail->setFrom('no-reply@ge.com.sg', 'Global Explorer');						// Sender
	// 		$mail->WordWrap = 50;                                 							// Set word wrap to 50 characters
	// 	    $mail->isHTML(true);                                  							// Set email format to HTML
	// 	    $mail->Subject = 'Booking Arrival Reminders';
 //     		if($agent[0]->alternative_email != '' || $agent[0]->alternative_email != NULL)
	//     	{
	// 	     	$alternative_email=explode(',',$agent[0]->alternative_email);
	// 	     	for ($i=0; $i < count($alternative_email); $i++) {
	// 	     		$mail->addCC($alternative_email[$i]);
	// 			}
	// 	    }
	//     	$mail->addAddress($agent[0]->email);
 //    		$mail->Body    = $this->load->view('panel/mainpanel/email_summary_departure',$data,true);
	// 	 	$mail->AltBody = 'Booking Arrival Reminders';
	// 		if($mail->send()):
	// 			echo "sent";
	// 		else:
	// 			echo "failed";
	// 		endif;
			
	// 	}
		//admins
		if(empty($departures)):
			die;
		else:			
			$booking_id = $this->data['departure_list'][count($this->data['departure_list'])-1]->id;
			$hotel_id = $this->data['departure_list'][count($this->data['departure_list'])-1]->hotel_id;
			// $this->data['traveller_details'] = $this->customer->get_by(array('booking_id' => $booking_id));
			//$this->data['traveller_details'] = $this->booking->get_booking_info($booking_id);
			$this->data['hotel_details'] = $this->hotel->get(array('id' => $booking_id));
			$mail = $this->mail;
			$mail->ClearAddresses();  // each AddAddress add to list
			$mail->ClearCCs();
			$mail->ClearBCCs();
		    $mail->setFrom('no-reply@ge.com.sg', 'Global Explorer');						// Sender
			$mail->WordWrap = 50;                                 							// Set word wrap to 50 characters
		    $mail->isHTML(true);                                  							// Set email format to HTML
		    $mail->Subject = 'Booking Arrival Reminders';
		    $recipients=$this->user->get_by(array('type' => 'admin'));
		 	if($recipients != NULL || $recipients != ""){
		 		foreach($recipients as $row){
					//$mail->ClearAddresses();
					//$mail->ClearCCs();
		     		$booking['recipient'] = $row->firstname.' '.$row->lastname;
		     		if($row->alternative_email != '' || $row->alternative_email != NULL)
			    	{
				     	$alternative_email=explode(',',$row->alternative_email);
				     	for ($i=0; $i < count($alternative_email); $i++) {
				     		$mail->addCC($alternative_email[$i],$booking['recipient']);
						}
				    }
			    	$mail->addAddress($row->email, $booking['recipient']);
			    	//$mail->addAddress('cabigas.jude@outlook.com', $booking['recipient']);
			    	// Add a recipient (Admin)
		    		$mail->Body    = $this->load->view('panel/mainpanel/email_summary_departure',$this->data,true);
			    }
		 	}

		 	$mail->AltBody = 'Daily Departure Booking Notification [Global Explorer]';
			if($mail->send()):
				echo "sent";
			else:
				echo "failed";
			endif;		
		endif;
	}

	public function cron_emailprovider(){

		$this->load->model('booking/booking');
		$this->load->model('user/user');
		$this->load->model('user/customer');
		$this->load->model('hotel/hotel');
		$date = date('Y-m-d');
		$this->db->select('company_designations.company_id,bookings.package_id,users.email,users.alternative_email');
		$this->db->join('company_designations','packages.provider_id=company_designations.company_id');
		$this->db->join('users','company_designations.user_id=users.id');
		$this->db->group_by('bookings.package_id'); 
		$this->db->join('bookings','bookings.package_id=packages.id');
		$query = $this->db->from('packages')->where('bookings.status','pending')->get()->result(); //packages distinct

		if(empty($query)){
			die();
		}
		else{
			foreach($query as $q){
				//query get in bookings table
				//$to_providers['infos']	=	$this->booking->get_by(array('status'=>'pending','package_id'=>$q->package_id));
				print_r($q->package_id);
				$this->db->select('*');
				$this->db->where('bookings.package_id',$q->package_id);
				$this->db->where('bookings.status','pending');
				$this->db->where('bookings.id IN (SELECT MAX(bookings.id) FROM bookings GROUP BY bookings.code)');
				$to_providers['infos'] = $this->db->get("bookings")->result();
				if($to_providers['infos'])
				{
				$mail = $this->mail;
				$mail->ClearAddresses();  // each AddAddress add to list
				$mail->ClearCCs();
				$mail->ClearBCCs();
			    $mail->setFrom('no-reply@ge.com.sg', 'Global Explorer');						// Sender
				$mail->WordWrap = 50;                                 							// Set word wrap to 50 characters
			    $mail->isHTML(true);                                  							// Set email format to HTML
			    $mail->Subject = 'Pending Booking Reminders';
	     		if($q->alternative_email != '' || $q->alternative_email != NULL)
		    	{
			     	$alternative_email=explode(',',$q->alternative_email);
			     	for ($i=0; $i < count($alternative_email); $i++) {
			     		$mail->addCC($alternative_email[$i]);
					}
			    }
		    	$mail->addAddress($q->email);
		    	//$mail->addAddress('laurence@azazabizsolutions.com');
	    		$mail->Body    = $this->load->view('panel/mainpanel/email_pending_provider',$to_providers,true);
			 	$mail->AltBody = 'Pending Booking Reminders';
				if($mail->send()):
					echo "sent";
				else:
					echo "failed";
				endif;
				}
			}
		}	
	}
}