$(document).ready(function(){
    //TYPEAHEAD
    var d = new Date();
    d.setDate(d.getDate() - 0);

    var substringMatcher = function(strs) {
        return function findMatches(q, cb) {
            var matches, substrRegex;
            matches = [];

            substrRegex = new RegExp(q, 'i');

            $.each(strs, function(i, str) {
                if (substrRegex.test(str)) {
                    matches.push({ value: str });
                }
            });
            cb(matches);
        };
    };

    if($('#myModalSuccess p#myModalLabel').html().length > 0){
        $('#myModalSuccess').modal('show');
    }

    if (typeof $('#myModalLogin').find('.alert').html() != 'undefined') {
        if ($('#myModalLogin').find('.alert').html().length > 0){
            $('#myModalLogin').modal('show');
        }
    }
    // if(jQuery('#json_value_dd').length > 0) {
    //     console.log(jQuery('#json_value_dd').val());
    //     companies = JSON.parse(jQuery('#json_value_dd').val());
    //     $('#typeahead').typeahead({
    //         hint: true,
    //         highlight: true,
    //         minLength: 1
    //     },
    //     {
    //         name: 'company_name',
    //         displayKey: 'value',
    //         source: substringMatcher(companies)
    //     });
    // }
    
    //DATETIMEPICKERS    
    $('#datetimepickerDepartTime').datetimepicker({
        minDate:d
    });
    $('#datetimepickerReturnTime').datetimepicker({
        pickTime: true
    });
    $("#datetimepickerDepartTime").on("dp.change",function (e) {
        $('#datetimepickerReturnTime').data("DateTimePicker").setMinDate(e.date);
    });
    $("#datetimepickerReturnTime").on("dp.change",function (e) {
        $('#datetimepickerDepartTime').data("DateTimePicker").setMaxDate(e.date);
    });

    $('#datetimepickerDepart').datetimepicker({
        minDate:d,
        pickTime: false
    });
    $('#datetimepickerReturn').datetimepicker({
        pickTime: false
    });
    $("#datetimepickerDepart").on("dp.change",function (e) {
        $('#datetimepickerReturn').data("DateTimePicker").setMinDate(e.date);
    });
    $("#datetimepickerReturn").on("dp.change",function (e) {
        $('#datetimepickerDepart').data("DateTimePicker").setMaxDate(e.date);
    });

    $('#datetimepickerCheckIn').datetimepicker({
        minDate:d,
        pickTime: false
    });
    $('#datetimepickerCheckOut').datetimepicker({
        pickTime: false
    });
    $("#datetimepickerCheckIn").on("dp.change",function (e) {
        $('#datetimepickerCheckOut').data("DateTimePicker").setMinDate(e.date);
    });
    $("#datetimepickerCheckOut").on("dp.change",function (e) {
        $('#datetimepickerCheckIn').data("DateTimePicker").setMaxDate(e.date);
    });

    $('#datetimepickerBookingDepart').datetimepicker({
        minDate:d,
        pickTime: false
    });

    $('#datetimepickerBookingReturn').datetimepicker({
        pickTime: false
    });

    $("#datetimepickerBookingDepart").on("dp.change",function (e) {
        day = parseInt($('#package_days').val()-1,10);
        // select new date
        var n = new Date(e.date);
        n.setDate(n.getDate() + day);

        $('#datetimepickerBookingReturn').data("DateTimePicker").setMinDate(n);
        $('#datetimepickerBookingReturn').data("DateTimePicker").setDate(n);
        $('#stephotel_form').bootstrapValidator('revalidateField', 'depart_date');

        //update the package, hotel and room surcharges ----------------------------------------------
        updatePackageSurcharges();
        listHotelSurcharges();
        
        $("[name='package_hotel_choice[]']").each(function(i,e){
            var destination_id = $(e).attr("destination-id");
            updateRoomRatesContainers(destination_id);
        });
    });

    $("#datetimepickerBookingReturn").on("dp.change",function (e) {
        $('#stephotel_form').bootstrapValidator('revalidateField', 'return_date');
        day = parseInt($('#package_days').val()-1,10);

        var r = new Date(e.date);
        var d = new Date($('#datetimepickerBookingDepart').data("DateTimePicker").date);
    
        var extension = Math.floor(((((r).getTime() - (d).getTime())/60/60/24)/1000)) - day;

        if(extension!=0 || extension>0){
            $('.extension_container:last').show();
            $('#label_extended_days_container_wrapper').show();
            $("#label_extended_days_container").empty().append(extension);
        }else{
            $('.extension_container').hide();
            $("#label_extended_days_container_wrapper").hide();
        }

        //set the extended days ----------------------------------------------
        $('#extended_days').val(extension);

        //updatet the package, hotel and room surcharges ----------------------------------------------
        updatePackageSurcharges();
        listHotelSurcharges();

        $("[name='package_hotel_choice[]']").each(function(i,e){
            var destination_id = $(e).attr("destination-id");
            updateRoomRatesContainers(destination_id);
        });
    });


    //step_detail part --- Time only
    $('.datetimepickerTimeOnly').datetimepicker({
        pickDate: false,
        pickTime: true
    });

    //login form
    $('#login_form')
    .bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            email_login: {
                validators: {
                    notEmpty: {
                        message: 'The username is required and can\'t be empty'
                    }/*,
                    emailAddress: {
                        message: 'The input is not a valid email address'
                    }*/
                }
            },
            password_login: {
                validators: {
                    notEmpty: {
                        message: 'The password is required and can\'t be empty'
                    },
                    stringLength: {
                        min: 8,
                        message: 'your password must be at least 8 characters short'
                    }
                }
            }
        }
    });
    
    //login form pub
    $('#login_form_')
    .bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            email_login: {
                validators: {
                    notEmpty: {
                        message: 'The email address is required and can\'t be empty'
                    },
                    emailAddress: {
                        message: 'The input is not a valid email address'
                    }
                }
            },
            password_login: {
                validators: {
                    notEmpty: {
                        message: 'The password is required and can\'t be empty'
                    },
                    stringLength: {
                        min: 8,
                        message: 'your password must be at least 8 characters short'
                    }
                }
            }
        }
    });

    //forget form
    $('#defaultFormForget')
    .bootstrapValidator({
        message: 'This value is not valid',
        //live: 'submitted',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            email: {
                validators: {
                    notEmpty: {
                        message: 'The email address is required and can\'t be empty'
                    },
                    emailAddress: {
                        message: 'The input is not a valid email address'
                    }
                }
            }
        }
    })
    .on('success.form.bv', function(e) {
        e.preventDefault();
        var $form = $(e.target);
        var bv = $form.data('bootstrapValidator');
        $.post($form.attr('action'), $form.serialize(), function(result) {
            $form.find('.alert').hide();
            if(result.alert_type == 'Success'){
                $form.find('.alert').removeClass('alert-danger').addClass('alert-success').html(result.message).show();
            }else $form.find('.alert').removeClass('alert-success').addClass('alert-danger').html(result.message).show();
        }, 'json');
    });
    
    $('#myModalForget').on('hide.bs.modal', function() {
        $('#defaultFormForget').bootstrapValidator('resetForm', true);
    });

    //sign up form
    $('#signup_form')
    .bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            salutation: {
                message: 'The salutation is not valid',
                validators: {
                    notEmpty: {
                        message: 'The salutation is required and can\'t be empty'
                    }
                }
            },
            firstname: {
                message: 'The firstname is not valid',
                validators: {
                    notEmpty: {
                        message: 'The first name is required and can\'t be empty'
                    },
                    regexp: {
                        regexp: /^([a-z0-9]*[a-z]){2}[a-zA-Z0-9 '.-]*$/i,
                        message: 'The first name can only consist of letters and some characters(. -\')'
                    }
                }
            },
            middlename: {
                message: 'The middlename is not valid',
                validators: {
                    regexp: {
                        regexp: /^[a-zA-Z0-9 '.-]*$/i,
                        message: 'The middle name can only consist of letters and some characters(. -\')'
                    }
                }
            },
            lastname: {
                message: 'The lastname is not valid',
                validators: {
                    notEmpty: {
                        message: 'The lastname is required and can\'t be empty'
                    },
                    regexp: {
                        regexp: /^([a-z0-9]*[a-z]){2}[a-zA-Z0-9 '.-]*$/i,
                        message: 'The lastname can only consist of letters and some characters(. -\')'

                    }
                }
            },
            dob: {
                message: 'The date is not valid',
                validators: {
                    notEmpty: {
                        message: 'The date of birth is required and can\'t be empty'
                    },
                    date: {
                        format: 'YYYY-MM-DD',
                        message: 'Invalid Date. Please follow the correct format (YYYY-MM-DD).'
                    }
                }
            },
            company: {
                message: 'The company is not valid',
                validators: {
                    regexp: {
                        regexp: /^[a-zA-Z0-9 !@#$%^&*()',.-]*$/i,
                        message: 'Enter a valid company name'
                    }
                }
            },
            phone: {
                message: 'The phone number is not valid',
                validators: {
                    notEmpty: {
                        message: 'Phone number is required and can\'t be empty'
                    },
                    stringLength: {
                        min: 6,
                        message: 'Your phone number must be at least 6 characters short'
                    },
                    regexp: {
                        regexp: /^[0-9 ()+\-]+$/i,
                        message: 'Enter the correct number format'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'The email address is required and can\'t be empty'
                    },
                    emailAddress: {
                        message: 'The input is not a valid email address'
                    }
                }
            },
            country: {
                message: 'The country is not valid',
                validators: {
                    notEmpty: {
                        message: 'Country is required and can\'t be empty'
                    }
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: 'The password is required and can\'t be empty'
                    },
                    stringLength: {
                        min: 8,
                        message: 'your password must be at least 8 characters short'
                    },
                    regexp: {
                        regexp: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/i,
                        message: 'Your password must contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character'
                    }
                }
            },
            confirm_password: {
                validators: {
                    notEmpty: {
                        message: 'The password is required and can\'t be empty'
                    },
                    identical: {
                        field: 'password',
                        message: 'The passwords do not match'
                    }
                }
            }
        }
    })
    .on('success.form.bv', function(e) {
        e.preventDefault();
        var $form = $(e.target);
        var bv = $form.data('bootstrapValidator');
        var form = $form[0];
        var formData = new FormData(form);
        $.ajax({
            url: $form.attr('action'),
            data: formData,
            processData: false,
            contentType: false,
            type: 'POST',
            dataType: 'json',
            beforeSend: function(){
                $form.find('.btn[type=submit]').attr('disable','disable');
            },
            success: function(result){
                $form.find('.alert').hide();

                if(result.alert_type == 'Success'){
                    $form.find('.btn[type=submit]').removeAttr('disable');
                    $form.find('.alert').removeClass('alert-warning').addClass('alert-success').html('').hide();
                    $form.data('bootstrapValidator').resetForm(true);
                    $('#myModalSignUp').modal('hide');
                    $('#myModalSuccess #myModalLabel').html(result.message);
                    $('#myModalSuccess').modal('show');
                } else{   
                    $('#myModalSuccess #myModalLabel').html('');
                    $('#myModalSuccess').modal('hide');
                    $('#myModalSignUp').animate({ scrollTop: 0 }, 'slow'); 
                    $form.find('.alert').removeClass('alert-success').addClass('alert-warning').html(result.message).show();
                }
            }    
        });    
    });

    $('#mail_form')
    .bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            sender_email: {
                validators: {
                    notEmpty: {
                        message: 'The email address is required and can\'t be empty'
                    },
                    emailAddress: {
                        message: 'The input is not a valid email address'
                    }
                }
            },
            sender_message: {
                message: 'The message is not valid',
                validators: {
                    notEmpty: {
                        message: 'A message is required and can\'t be empty'
                    }
                }
            }
        }
    })
    .on('success.form.bv', function(e) {
        e.preventDefault();
        var $form = $(e.target),
        bv    = $form.data('bootstrapValidator');
        $.post($form.attr('action'), $form.serialize(), function(result) {
            $form.find('.alert').hide();
            removeThis = '';
            new_class = 'alert-warning';

            oldClass = $form.find('.alert').attr('class');
            classes = oldClass.split(' ');
            if(classes[1] !== '') removeThis = classes[1];

            if(result.alert_type == 'Success'){
                new_class = 'alert-success';
                $form.find('textarea, input').val('');
            }

            $form.find('.alert').removeClass(removeThis).addClass(new_class).html(result.message).show();
        }, 'json');
    });
    
});
