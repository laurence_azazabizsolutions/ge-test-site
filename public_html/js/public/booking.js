$(document).ready(function(){

    $('.count_val').on('keyup', function(e) {
       validateCounterFields();
    });

    //sign up form ----------------------------------------------
    $('#stephotel_form')
    .bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            total_holder: {
                message: 'Total Guest is not valid',
                validators: {
                    notEmpty: {
                        message: 'Total Guest is required and can\'t be empty'
                    }
                }
            },
            depart_date: {
                message: 'Departure Date is not valid',
                validators: {
                    notEmpty: {
                        message: 'Departure Date is required and can\'t be empty'
                    }
                }
            },
            return_date: {
                message: 'Return Date is not valid',
                validators: {
                    notEmpty: {
                        message: 'Return Date is required and can\'t be empty'
                    }
                }
            }
        }
    })
    .on('status.field.bv', function(e, data) {
        if (data.bv.getSubmitButton()) {
            data.bv.disableSubmitButtons(false);
        }
    });
    //end of validate booking form ----------------------------------------------

    
    //validate the step details form ----------------------------------------------
    $('#stepdetails_form')
    .bootstrapValidator({ 
        group: 'td',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            "adult_name[]": {
                message: 'Total is not valid',
                validators:{
                    notEmpty: {
                        message: 'Please enter a valid name'
                    }
                }
            },
            // "adult_ppno[]":{
            //     message: 'Total is not valid',
            //     validators:{
            //         notEmpty: {
            //             message: 'Please enter a valid passport no.'
            //         }
            //     }
            // },
            "child_name[]":{
               message: 'Total is not valid',
                validators:{
                    notEmpty: {
                        message: 'Please enter a valid name'
                    }
                } 
            },
            // "child_ppno[]":{
            //     message: 'Total is not valid',
            //     validators:{
            //         notEmpty: {
            //             message: 'Please enter a valid passport no.'
            //         }
            //     } 
            // },
            "infant_name[]":{
               message: 'Total is not valid',
                validators:{
                    notEmpty: {
                        message: 'Please enter a valid name'
                    }
                } 
            },
            // "infant_ppno[]":{
            //     message: 'Total is not valid',
            //     validators:{
            //         notEmpty: {
            //             message: 'Please enter a valid passport no.'
            //         }
            //     } 
            // },
            flight_of_no:{
                message: 'Total is not valid',
                validators:{
                    notEmpty: {
                        message: 'Please enter a valid flight no.'
                    }
                } 
            },
            flight_rf_no:{
                message: 'Total is not valid',
                validators:{
                    notEmpty: {
                        message: 'Please enter a valid flight no.'
                    }
                } 
            },
            flight_of_from:{
                message: 'Total is not valid',
                validators:{
                    notEmpty: {
                        message: 'Please select a valid location'
                    }
                } 
            },
            flight_rf_from:{
                message: 'Total is not valid',
                validators:{
                    notEmpty: {
                        message: 'Please select a valid location'
                    }
                } 
            },
            flight_of_to:{
                message: 'Total is not valid',
                validators:{
                    notEmpty: {
                        message: 'Please select a valid destination'
                    }
                } 
            },
            flight_rf_to:{
                message: 'Total is not valid',
                validators:{
                    notEmpty: {
                        message: 'Please select a valid destination'
                    }
                } 
            }
        }
    })
    .on('success.form.bv', function(e) {
        e.preventDefault();
        var $form = $(e.target);
        var bv = $form.data('bootstrapValidator');
    });
    //end of validate the step details form ----------------------------------------------
    

    // $('.count_val').change(function(){
    //     $('#stepone_form').bootstrapValidator('revalidateField', 'total_holder');
    // });


    //sign up form
    // $('#stepone_form')
    // .bootstrapValidator({
    //     message: 'This value is not valid',
    //     feedbackIcons: {
    //         valid: 'glyphicon glyphicon-ok',
    //         invalid: 'glyphicon glyphicon-remove',
    //         validating: 'glyphicon glyphicon-refresh'
    //     },
        // fields: {
        //     adult_count: {
        //         message: 'The salutation is not valid',
        //         validators: {
        //             integer: {
        //                 message: 'The value must not contain decimals or other special characters.'
        //             },
        //             greaterThan: {
        //                 value: 1,
        //                 message: 'Please enter a value greater than or equal to %s'
        //             }
        //         }
        //     },
        //     child_count: {
        //         message: 'The firstname is not valid',
        //         validators: {
        //             integer: {
        //                 message: 'The value must not contain decimals or other special characters.'
        //             },
        //         }
        //     },
        //     total_holder: {
        //         message: 'The middlename is not valid',
        //         validators: {
        //             notEmpty: {
        //                 message: 'The date of birth is required and can\'t be empty'
        //             },
        //             greaterThan: {
        //                 value: 1,
        //                 message: 'Please enter a value greater than or equal to %s'
        //             }
        //         }
        //     },
        //     depart_date: {
        //         message: 'The date is not valid',
        //         validators: {
        //             notEmpty: {
        //                 message: 'Departure date is required and can\'t be empty'
        //             },
        //             date: {
        //                 format: 'YYYY-MM-DD',
        //                 message: 'Invalid Date. Please follow the correct format (YYYY-MM-DD).'
        //             }
        //         }
        //     },
        //     return_date: {
        //         message: 'The date is not valid',
        //         validators: {
        //             notEmpty: {
        //                 message: 'Return date is required and can\'t be empty'
        //             },
        //             date: {
        //                 format: 'YYYY-MM-DD',
        //                 message: 'Invalid Date. Please follow the correct format (YYYY-MM-DD).'
        //             }
        //         }
        //     }
        // }
    // }).on('success.form.bv', function(e) {
    //     e.preventDefault();
    //     var $form = $(e.target);
    //     var bv = $form.data('bootstrapValidator');
    //     var form = $form[0];
    //     var formData = new FormData(form);
    //     if($('[name=child_count]').val() > 0){
    //         $($form.find('.btn').attr('data-target')).modal('show');
    //     }else{

    //     }    
    // });
    
//use this to validate child_check modal form
// 'project[]': {
//                 validators: {
//                     notEmpty: {
//                         message: 'The project name is required'
//                     }
//                 }
//             },

});

function validateCounterFields(){
    var total = 0;
    $('.count_val').each(function(index, elem) {
        if((parseInt(this.value)))
            total = total + parseInt(this.value);
    });
    $('[name=total_holder]').val(total);
    $('#stephotel_form').bootstrapValidator('revalidateField', 'total_holder');
}
