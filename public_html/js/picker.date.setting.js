var date = new Date();
var yr = date.getFullYear();
var mo = date.getMonth();
var day = date.getDate();
/**
 * Dropdown selectors
 */
$( '.pickadate_default' ).pickadate({
    labelMonthNext: 'Go to the next month',
    labelMonthPrev: 'Go to the previous month',
    labelMonthSelect: 'Pick a month from the dropdown',
    labelYearSelect: 'Pick a year from the dropdown',

    selectYears: true,
    selectMonths: true,
    showMonthsShort: true,

    format: 'yyyy-mm-dd',
    container: 'body',
    selectYears: 100,
    min: [1914,0,1]
})

$( '.pickadate_DOB' ).pickadate({
    labelMonthNext: 'Go to the next month',
    labelMonthPrev: 'Go to the previous month',
    labelMonthSelect: 'Pick a month from the dropdown',
    labelYearSelect: 'Pick a year from the dropdown',

    selectYears: true,
    selectMonths: true,
    showMonthsShort: true,

    format: 'yyyy-mm-dd',
    container: 'body',
    selectYears: 100,
    min: [1914,0,1],
    max: true
})

$( '.pickadate_DOB_adult' ).pickadate({
    labelMonthNext: 'Go to the next month',
    labelMonthPrev: 'Go to the previous month',
    labelMonthSelect: 'Pick a month from the dropdown',
    labelYearSelect: 'Pick a year from the dropdown',

    selectYears: true,
    selectMonths: true,
    showMonthsShort: true,

    today: '',

    format: 'yyyy-mm-dd',
    container: 'body',
    min: [1914,0,1],
    max: [yr-12,mo,day-1]
})

$( '.pickadate_DOB_child' ).pickadate({
    labelMonthNext: 'Go to the next month',
    labelMonthPrev: 'Go to the previous month',
    labelMonthSelect: 'Pick a month from the dropdown',
    labelYearSelect: 'Pick a year from the dropdown',

    selectYears: true,
    selectMonths: true,
    showMonthsShort: true,

    today: '',

    format: 'yyyy-mm-dd',
    container: 'body',
    min: [yr-12,mo,day],
    max: [yr-3,mo,day-1]
})

$( '.pickadate_DOB_infant' ).pickadate({
    labelMonthNext: 'Go to the next month',
    labelMonthPrev: 'Go to the previous month',
    labelMonthSelect: 'Pick a month from the dropdown',
    labelYearSelect: 'Pick a year from the dropdown',

    selectYears: true,
    selectMonths: true,
    showMonthsShort: true,

    today: '',

    format: 'yyyy-mm-dd',
    container: 'body',
    selectYears: 12,
    min: [yr-3,mo,day],
    max: true
})

$( '.pickadate_min_max' ).pickadate({
    labelMonthNext: 'Go to the next month',
    labelMonthPrev: 'Go to the previous month',
    labelMonthSelect: 'Pick a month from the dropdown',
    labelYearSelect: 'Pick a year from the dropdown',

    selectYears: true,
    selectMonths: true,
    showMonthsShort: true,

    format: 'yyyy-mm-dd',
    container: 'body',
    selectYears: 12,
    min: [yr-12,mo,day],
    max: true
})