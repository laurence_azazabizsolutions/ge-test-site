var yr = new Date().getFullYear();
var mo = new Date().getMonth();
var day = new Date().getDay();
/**
 * Dropdown selectors
 */
$( '#pickadate_min_max' ).pickadate({
    labelMonthNext: 'Go to the next month',
    labelMonthPrev: 'Go to the previous month',
    labelMonthSelect: 'Pick a month from the dropdown',
    labelYearSelect: 'Pick a year from the dropdown',

    selectYears: true,
    selectMonths: true,
    showMonthsShort: true,

    format: 'yyyy-mm-dd',
    container: 'body',
    selectYears: 12,
    min: [yr-12,mo,day],
    max: true

})