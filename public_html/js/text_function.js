//text functions
function isNumberKey(evt){
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && charCode != 46 && (charCode < 48 || charCode > 57)){
		return false;}
		return true;
}
function isCharKey(evt){
	var charCode = (evt.which) ? evt.which : event.keyCode
	if ((charCode > 32 && charCode != 46 && charCode < 65) || (charCode > 90 && charCode < 97) || charCode > 122){
		return false;}
		return true;
}
function isCharNumKey(evt){
	var charCode = (evt.which) ? evt.which : event.keyCode
	if ((charCode > 32 && charCode < 48) || (charCode > 57 && charCode < 65) || (charCode > 90 && charCode < 97 || charCode > 122)){
		return false;}
		return true;
}
function isEmailKey(evt){
	var charCode = (evt.which) ? evt.which : event.keyCode
	if ((charCode > 31 && charCode < 45) || charCode == 47 || (charCode > 57 && charCode < 64) || (charCode > 90 && charCode < 95) || charCode == 96 || charCode > 122){
		return false;}
		return true;
}
function isExceptKey(evt){
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode == 39){
		return false;}
		return true;
}