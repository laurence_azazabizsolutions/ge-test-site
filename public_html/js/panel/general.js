/*
*remove hotel from package
*/
function removePackageHotel(obj){
	//declare the hotel id
	var hotel_id = $(obj).attr("hotel_id");
	var package_id = $(obj).attr("package_id");
	var package_hotel_id = $(obj).attr("package_hotel_id");

	bootbox.confirm("Are you sure?", function(result){
		if(result){
			//remove the object	
			$("[packageHotelId="+package_hotel_id+"]").animate({opacity:".6"},200);

			$.post("/general_json/removePackageHotelID", { "package_hotel_id":package_hotel_id} , function(data){	
				//remove the object	
				$("[packageHotelId="+package_hotel_id+"]").remove();
			})
			.fail(function(x){
				//return the opacity of the object
				$("[packageHotelId="+package_hotel_id+"]").animate({opacity:"1"},200);
			});
		}
	});	
}

/*
*remove surcharge from package
*/
function removeSurchargeHotelRooms(obj){
	//get the hotelroom surcharge id
	var surcharge_id = $(obj).attr("surcharge-id");
	
	//check if the user REALLY wants to delete the surcharge
	bootbox.confirm("Are you sure? ", function(result){
		if(result){
			//reduce opacity
			$("[roomsurchargeId="+surcharge_id+"]").animate({opacity:".6"},200);
			$.post("/general_json/removeSurchargeHotelRooms",{"surcharge_id" : surcharge_id},function(data){
				//remmove the object completely
				$("[roomsurchargeId="+surcharge_id+"]").remove();
			})	
			.fail(function(x){
				//return opacity to normal
				$("[roomsurchargeId="+surcharge_id+"]").animate({opacity:"1"},200);
			});
		}
	});		
}
		
/*
*remove roomrate from package
*/
function removePackageRoomRate(obj){
	//fetch the roomrate id 
	var roomrate_id = $(obj).attr("roomrate-id");

	bootbox.confirm("Do you really want to remove this roomrate surcharge? ", function(result){
		if(result){
			$("[roomrateId='"+roomrate_id+"']").animate({opacity:".6"},200);
			$.post("/general_json/removePackageRoomRate", {"roomrate_id":roomrate_id} , function(data){
				$("[roomrateId='"+roomrate_id+"']").remove();
			}).fail(function(x){
				alert(x.responseText);
				$("[roomrateId='"+roomrate_id+"']").animate({opacity:"1"},200);
			});
		}
	});
}

/*
*remove package addon
*/
function removeGeneralElement(obj){
	//fetch the element id
	var element_id = $(obj).attr("element-id");
	//fetch the element container 
	var element_container = $(obj).attr("element-container");
	//confirmation message
	var element_message = $(obj).attr("element-message");
	//database table
	var element_table = $(obj).attr("element-table");


	//alert(element_id+" "+element_container+" "+element_table);

	bootbox.confirm(element_message, function(result){
		if(result){
			$(element_container).animate({opacity:".6"},200);
			$.post("/general_json/removeGeneralElement", {"table":element_table,"id":element_id}, function(data){
				$(element_container).remove();
			})
			.fail(function(x){
				$(element_container).animate({opacity:"1"},200);
			});
		}
	});
}
