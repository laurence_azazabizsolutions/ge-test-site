$(document).ready(function(){

    $('.count_val').on('keyup', function(e) {
       validateCounterFields();
    });
      
    //sign up form ----------------------------------------------
    $('#stephotel_form')
    .bootstrapValidator({ 
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            total_holder: {
                message: 'Total is not valid',
                validators:{
                    notEmpty: {
                        message: 'Please fil in the number of travellers.'
                    },
                    integer: {
                        message: 'The value must not contain decimals or other special characters.'
                    },
                    greaterThan: {
                        value: 1,
                        message: 'Please enter a value greater than or equal to %s'
                    }
                }
            },
            depart_date: {
                message: 'The date is not valid',
                validators: {
                    notEmpty: {
                        message: 'Departure date is required.'
                    },
                    // date: {
                    //     format: 'DD-MMM-YYYY',
                    //     message: 'Invalid Date. aaaaaPlease follow the correct format (YYYY-MM-DD).'
                    // }
                }
            },
            return_date: {
                message: 'The date is not valid',
                validators: {
                    notEmpty: {
                        message: 'Return date is required.'
                    },
                    // date: {
                    //     format: 'DD-MMM-YYYY',
                    //     message: 'Invalid Date. bbbbPlease follow the correct format (YYYY-MM-DD).'
                    // }
                }
            },
            "package_hotel_choice[]":{
                message: "",
                validators: {
                    notEmpty:{
                        message: "Please select a hotel before proceeding"
                    }
                }
            }
        }
    })
    .on('status.field.bv', function(e, data) {
        if (data.bv.getSubmitButton()) {
            data.bv.disableSubmitButtons(false);
        }
    });
    //end of validate booking form ----------------------------------------------
    

    //validate the step details form ----------------------------------------------
    $('#stepdetails_form')
    .bootstrapValidator({ 
        group: 'td',
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            "adult_name[]": {
                message: 'Total is not valid',
                validators:{
                    notEmpty: {
                        message: 'Please enter a valid name'
                    }
                }
            },
            "adult_dob[]": {
                message: 'The date is not valid',
                validators: {
                    notEmpty: {
                        message: 'The date of birth is required and can\'t be empty'
                    },
                    // date: {
                    //     format: 'YYYY-MM-DD',
                    //     message: 'Invalid Date. Please follow the correct format (YYYY-MM-DD).'
                    // }
                    callback: {
                        message: 'Invalid Date. Please follow the correct format (DD-MMM-YYYY).',
                        callback: function(value, validator) {
                            var m = new moment(value, 'DD-MMMM-YYYY', true);
                            // Check if the input value follows the MMMM D format
                            if (!m.isValid()) {
                                return false;
                            }
                            // US independence day is July 4
                            return true;
                        }
                    }
                }
            },
            "child_dob[]": {
                message: 'The date is not valid',
                validators: {
                    notEmpty: {
                        message: 'The date of birth is required and can\'t be empty'
                    },
                    // date: {
                    //     format: 'YYYY-MM-DD',
                    //     message: 'Invalid Date. Please follow the correct format (YYYY-MM-DD).'
                    // }
                    callback: {
                        message: 'Invalid Date. Please follow the correct format (DD-MMM-YYYY).',
                        callback: function(value, validator) {
                            var m = new moment(value, 'DD-MMMM-YYYY', true);
                            // Check if the input value follows the MMMM D format
                            if (!m.isValid()) {
                                return false;
                            }
                            // US independence day is July 4
                            return true;
                        }
                    }
                }
            },
            "infant_dob[]": {
                message: 'The date is not valid',
                validators: {
                    notEmpty: {
                        message: 'The date of birth is required and can\'t be empty'
                    },
                    // date: {
                    //     format: 'YYYY-MM-DD',
                    //     message: 'Invalid Date. Please follow the correct format (YYYY-MM-DD).'
                    // }
                    callback: {
                        message: 'Invalid Date. Please follow the correct format (DD-MMM-YYYY).',
                        callback: function(value, validator) {
                            var m = new moment(value, 'DD-MMMM-YYYY', true);
                            // Check if the input value follows the MMMM D format
                            if (!m.isValid()) {
                                return false;
                            }
                            // US independence day is July 4
                            return true;
                        }
                    }
                }
            },
            "child_name[]":{
               message: 'Total is not valid',
                validators:{
                    notEmpty: {
                        message: 'Please enter a valid name'
                    }
                } 
            },
            // "child_ppno[]":{
            //     message: 'Total is not valid',
            //     validators:{
            //         notEmpty: {
            //             message: 'Please enter a valid passport no.'
            //         }
            //     } 
            // },
            "infant_name[]":{
               message: 'Total is not valid',
                validators:{
                    notEmpty: {
                        message: 'Please enter a valid name'
                    }
                } 
            },
            // "infant_ppno[]":{
            //     message: 'Total is not valid',
            //     validators:{
            //         notEmpty: {
            //             message: 'Please enter a valid passport no.'
            //         }
            //     } 
            // },
            flight_of_no:{
                message: 'Total is not valid',
                validators:{
                    notEmpty: {
                        message: 'Please enter a valid flight no.'
                    }
                } 
            },
            // flight_rf_no:{
            //     message: 'Total is not valid',
            //     validators:{
            //         notEmpty: {
            //             message: 'Please enter a valid flight no.'
            //         }
            //     } 
            // },
            flight_of_from:{
                message: 'Total is not valid',
                validators:{
                    notEmpty: {
                        message: 'Please select a valid location'
                    }
                } 
            },
            // flight_rf_from:{
            //     message: 'Total is not valid',
            //     validators:{
            //         notEmpty: {
            //             message: 'Please select a valid location'
            //         }
            //     } 
            // },
            flight_of_to:{
                message: 'Total is not valid',
                validators:{
                    notEmpty: {
                        message: 'Please select a valid destination'
                    }
                } 
            },
            // flight_rf_to:{
            //     message: 'Total is not valid',
            //     validators:{
            //         notEmpty: {
            //             message: 'Please select a valid destination'
            //         }
            //     } 
            // },
            flight_of_date_departure:{
                message: 'Total is not valid',
                validators:{
                    notEmpty: {
                        message: 'Flight Time is required.'
                    }
                } 
            },
            flight_of_date_arrival:{
                message: 'Total is not valid',
                validators:{
                    notEmpty: {
                        message: 'Arrival Time is required.'
                    }
                } 
            }
        }
    })
    .on('success.form.bv', function(e) {
        e.preventDefault();
        var $form = $(e.target);
        var bv = $form.data('bootstrapValidator');
    });
    //end of validate the step details form ----------------------------------------------

});

function validateCounterFields(){
    surchargeGeneration("false");
    var total = 0;
    $('.count_val').each(function(index, elem) {
        if((parseInt(this.value)))
            total = total + parseInt(this.value);
    });
    $('[name=total_holder]').val(total);
    $('#stephotel_form').bootstrapValidator('revalidateField', 'total_holder');
}
