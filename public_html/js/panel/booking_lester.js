//ready function ----------------------------------------------
$(document).ready(function(){
	//update the package surcharges ----------------------------------------------
	updatePackageSurcharges();


	//clear the fields ----------------------------------------------
	$("[name='package_hotel_choice'] option:first").attr("selected","selected");
	


	


	//catch trigger for flights ----------------------------------------------
	$("[name='flight_of_from'], [name='flight_rf_from']").change(function(){
		var target_select = $("[name='"+$(this).attr("target-select")+"']");
		var this_value = $.trim($(this).val());
		var base_url = $.trim($('#base_url_container').val());


		//check if the select value is not empty ----------------------------------------------
		if(this_value.length!=0){
			
			//query to the server to get the countries relative to the selected value ----------------------------------------------
			$.post(base_url+"json/fetchDynamicAirportCodes",{"getAirportCode_curr_code":this_value},function(data){
				//$('#something_step_details_finalize').empty().append(data);
				data = $.parseJSON(data);
				if(!data.error){
					var countries = data.countries;
					target_select.empty();
					target_select.append("<option value=' '>- Select -</option>");
					for(i=0;i<countries.length;i++){
						var c_id = countries[i].id;
						var c_code = countries[i].code;
						var c_name = countries[i].name;
						target_select.append("<option value='"+c_code+"."+c_id+"'>"+c_name+"</option>");
					}
					//enable target select if the user has selected a valid flight 
					target_select.removeAttr("disabled");
				}else{
					console.log("ERROR, CAN'T LOAD THE COUNTRIES BY AIRPORT CODE");
					target_select.attr("disabled","disabled");
				}
			})
			.fail(function(){
				target_select.attr("disabled","disabled");
			});
			//query to the server to get the countries relative to the selected value ----------------------------------------------

		}else{
			//retain disabled if the user has not selected a valid flight
			target_select.attr("disabled","disabled");
		}
		//end of check if the select value is not empty ----------------------------------------------
	});
	// end of catch trigger of change  flight selection ----------------------------------------------


	//detect change of date in the step details ----------------------------------------------
	$("[name='return_date'],[name='depart_date']").change(function(){
		var this_data = $.trim($(this).val());
		surchargeGeneration("false");
		if(this_data.length!=0){
			$("[name='package_hotel_choice[]']").removeAttr('disabled');
		}else{
			$("[name='package_hotel_choice[]']").attr('disabled','disabled');
		}
	});
	
	//remove the disabled state from the button ----------------------------------------------
	$('#step_booking_continue').removeAttr("disabled");	


	//on set hotel name choice 
	$("[name='exhotel_name']").change(function(){
		var tmp = $(this).val();
		var txt = $("[name='exhotel_name'] option[value='"+tmp+"']").text();
		$("#exhotel_name_full").val(txt);
		$("#exhotel_name_full").attr("value",""+txt+"");
		$("[name='exhotel_name_full']").val(txt);
		$("[name='exhotel_name_full']").attr("value",""+txt+"");
	});
});
//ready function --------------------------------------------------------------------------------------------


//function to check the number of total guests ----------------------------------------------
function checkTotalGuestsCounter(){
	$('#step_booking_continue').removeAttr("disabled");

}
//end of function to check the number of total guest ----------------------------------------------


//------------------------------------------------------------------------------------------------------------------------------------------
//step details sectin
//step details section
//step details section
function stepDetailsFinalize(ele){
	//proper initialization of needed doms and elements ----------------------------------------------
	var element = $(ele);
	var email = $('#step_details_sending_email');

	var ser = $('#stepdetails_form').serialize();
	var container = $('#something_step_details_finalize');
	var base_url = $('#base_url_container').val();

	//submit form to get the number of errors
	$('#stepdetails_form').data('bootstrapValidator').validate();



	element.attr("disabled","disabled");
	element.hide();


	//check dates ----------------------------------------------
	//
	var child_dob_counter = $("[name='child_dob[]']").length;
	var adult_dob_counter = $("[name='adult_dob[]']").length;
	var infant_dob_counter = $("[name='infant_dob[]']").length;

	var tmp_child_dob_checker = true; //set this to true by default;
	var tmp_adult_dob_checker = true;
	var tmp_infant_dob_checker = true;
	var flight_of_date_checker = $.trim($("[name='flight_of_date']").val()).length == 0 ? false:true;
	var flight_rf_date_checker = $.trim($("[name='flight_rf_date']").val()).length == 0 ? false:true;
	var step_details_total_checker = true;

	/*if(child_dob_counter!=0){
		//check child dob ----------------------------------------------
		$("[name='child_dob[]']").each(function(i,e){
			var tmp_child_dob = $.trim($(e).val());
			tmp_child_dob_checker = tmp_child_dob.length == 0 ? false:true;
		});
	}

	if(adult_dob_counter!=0){
		//check adult dob ----------------------------------------------
		$("[name='adult_dob[]']").each(function(i,e){
			var tmp_adult_dob = $.trim($(e).val());
			tmp_adult_dob_checker = tmp_adult_dob.length == 0 ? false:true;
		});
	}

	if(infant_dob_counter!=0){
		//check adult dob ----------------------------------------------
		$("[name='infant_dob[]']").each(function(i,e){
			var tmp_infant_dob = $.trim($(e).val());
			tmp_infant_dob_checker = tmp_infant_dob.length == 0 ? false:true;
		});
	}


	if(flight_of_date_checker && flight_rf_date_checker){
		step_details_total_checker = true;
		$("#step_details_supplementary_errors").empty().hide();
	}else{
		$("#step_details_supplementary_errors").empty().show().append("<p>Please set the appropriate dates for each lacking field.</p>");
		step_details_total_checker = false;
	}*/
	//
	//end of check dates ----------------------------------------------



	
	//get bootstrap validator ----------------------------------------------
	var boostrapValidator =  $("#stepdetails_form").data('bootstrapValidator');

	//get all the invalid fields = ----------------------------------------------
	var test_invalid_fields = boostrapValidator.getInvalidFields().length;


	setTimeout(function(){
		// start of check if there are error in the form ----------------------------------------------	
		if(test_invalid_fields==0 && step_details_total_checker){
			email.show();
			$('html, body').animate({ scrollTop: 0}, 100);
			$('#stepdetails_form').data('bootstrapValidator').defaultSubmit();
		}else{
			//remove the disabled status
			element.removeAttr("disabled");
			
			//show the button
			element.show();
		}
		// end of checking ----------------------------------------------
	},0);

}
//----------------------------------------------

//step booking function ----------------------------------------------
function stepBookingFunctionFinalize(element){
	$(element).attr("disabled","disabled");
	$(element).empty().append("Loading the next page...");


	//check if there's an overflow error ----------------------------------------------
	$('.days_overflow').each(function(i,e){
		var tmp_overflow_checker = $.trim($(e).val());
		if(tmp_overflow_checker.length){
			console.log("troll = "+tmp_overflow_checker);
			surchargeGeneration("false");
		}
	});

	//check if a summary has been generated ----------------------------------------------
	var boolie = $("#package_surcharge_generation").val();

	//check if the boolie length is not 0 ----------------------------------------------
	if(boolie=="true"){
		$('#stephotel_form').data('bootstrapValidator').defaultSubmit();
	}else{
		//scroll to top ----------------------------------------------
		var pos = $(".package_destination_container").offset();
		$("html,body").scrollTop(pos.top);

		//temporary variable for the error container ----------------------------------------------
		var span_container = "";

		//check if there's an overflow error ----------------------------------------------
		$('.days_overflow').each(function(i,e){
			var tmp_overflow_checker = $.trim($(e).val());
			var destination_id = $(e).attr("destination-id");
			if(tmp_overflow_checker.length){
				$("#roomrates_container"+destination_id).find('.hotel_package_user_num').each(function(i,e){
					$(e).css("box-shadow","inset 0px 0px 3px red");
				});
				surchargeGeneration("false");
				span_container = "Room count must not be less than the total guest count!";
			}else{
				$("#roomrates_container"+destination_id).find('.hotel_package_user_num').each(function(i,e){
					$(e).css("box-shadow","inset 0px 0px 0px transparent");
				});
			}
		});

		//check if a summary has been generated ----------------------------------------------
		if(boolie=="false"){
			span_container = "Please generate a summary before proceeding!";
		}

		//display the supplementary errors ----------------------------------------------
		$("#supplementary_errors").empty().append(span_container).show();


		//remove the disabled state ----------------------------------------------
		$(element).removeAttr("disabled");
		//empty and append the continue function ----------------------------------------------
		$(element).empty().append("Continue");
	}
	
}
//end of step booking function ----------------------------------------------


//get package surcharges ----------------------------------------------
function updatePackageSurcharges(){
	surchargeGeneration("false");
	var depart_value = $.trim($('#datetimepickerBookingDepart').val());
	var return_value = $.trim($("#datetimepickerBookingReturn").val());
	var package_id = $("#package_id").val();
	var base_url = $.trim($("#base_url_container").val());
	var container = $('#hidden_package_surcharge_div');

	//if both dates are working
	if( depart_value.length!=0 && return_value.length!=0 && package_id.length!=0){
		//fetch the post data ----------------------------------------------
		$.post(base_url+"json/fetchPackageSurcharges",{"depart_value":depart_value,"return_value":return_value,"package_id":package_id},function(data){
			//jsonify the return value ---------------------------------------------
			data = $.parseJSON(data);


				var cnt_alrt = 0;
			if(!data.error){
				if ($('#sam_mod').length!=0) {
					$('#sam_mod').modal('show');
				};
				
				
				//pass the surcharges the ti a tmp variable ----------------------------------------------
				var tmp = data.surcharges;

				//set the container to this ----------------------------------------------
				var tmp_appender = "";

				//empty the container ----------------------------------------------
				container.empty();

				//iterate through the container ----------------------------------------------
				for(i=0;i<tmp.length;i++){
					var srchg = tmp[i];
					tmp_appender+="<div class='col-md-12'>";
					tmp_appender+= "<input type='hidden' id='pkg_surcharge_id"+srchg.id+"' name='pkg_surcharge_id[]' value='"+srchg.id+"' />";
					tmp_appender+= "<input type='hidden' name='pkg_surcharge_description[]' value='"+srchg.description+"' />";	
					tmp_appender+= "<input type='hidden' name='pkg_surcharge_rule[]' value='"+srchg.rule+"' />";	
					tmp_appender+= "<input type='hidden' name='pkg_surcharge_rule_type[]' value='"+srchg.rule_type+"' />";	
					tmp_appender+= "<input type='hidden' name='pkg_surcharge_profit[]' value='"+srchg.profit+"' />";
					tmp_appender+= "<input type='hidden' name='pkg_surcharge_cost[]' value='"+srchg.cost+"' />";
					tmp_appender+= "<input type='hidden' name='pkg_surcharge_price[]' value='"+(parseInt(srchg.cost)+parseInt(srchg.profit))+"' />";		
					tmp_appender+="</div>";
				}
				//end iterate through the container ----------------------------------------------
				container.append(tmp_appender);
			}else{
				container.empty();
				container.append("<input type='hidden' name='pkg_surcharge_id[]' value='0' />");
			}
			
		})
		.fail(function(){
			container.empty();
			container.append("<input type='hidden' name='pkg_surcharge_id[]' value='0' />");
			surchargeGeneration("false");
		});
	}else{
		var pkg_surcharge_container = $('#summary_package_surcharges_table');
		pkg_surcharge_container.hide(); //hide the body container
		$('.summary_package_surcharges_table_header').hide(); //hide the header containr
		surchargeGeneration("false");
	}
}
//end of get package surcharges ----------------------------------------------



//list all the hotel destinations ----------------------------------------------
function listHotelSurcharges(controller){
	$("[name='package_hotel_choice[]']").each(function(i,e){
		
		var package_hotel_id = $(e).val();
       	var package_id= $(e).attr('package-id');

       	//get hotel meta data  ----------------------------------------------
        var pkg_hotel_id = package_hotel_id.split(".");
        var pkg_htl_id = pkg_hotel_id[0];
        var destination_id = $(e).attr("destination-id");
        
        //alert(pkg_htl_id+" -> "+destination_id);
        if(!isNaN(parseInt(pkg_htl_id))){
        	updateHotelSurcharges(pkg_htl_id, destination_id);
        }
	});
}
//end of list all the hotel destinations ----------------------------------------------


//get hotel surcharges ----------------------------------------------
function updateHotelSurcharges(hotel_id,destination_id){
	surchargeGeneration("false");
	var depart_value = $.trim($('#datetimepickerBookingDepart').val());
	var return_value = $.trim($("#datetimepickerBookingReturn").val());
	var base_url = $.trim($("#base_url_container").val());
	var container = $('#destination_hotel_surcharges'+destination_id);

	var destination_index = $("#package_hotel_choice"+destination_id).attr("destination-index");


	//if both dates are working
	if( depart_value.length!=0 && return_value.length!=0 && hotel_id>0){
		
		//fetch the post data ----------------------------------------------
		$.post(base_url+"json/fetchHotelSurcharges",{"depart_value":depart_value,"return_value":return_value,"hotel_id":hotel_id},function(data){
			data = $.parseJSON(data);
			if(!data.error){
				//pass the surcharges the ti a tmp variable ----------------------------------------------
				var tmp = data.surcharges;
		

				//empty the container ----------------------------------------------
				container.empty();
				var tmp_appender = "";
				//iterate through the container ----------------------------------------------
				for(i=0;i<tmp.length;i++){
					var srchg = tmp[i];
					tmp_appender+="<div class='col-md-12'>";
					tmp_appender+="<input type='hidden' id='hotel_surcharge_id"+srchg.id+"'  name='hotel_surcharge_id["+destination_index+"][]' value='"+srchg.id+"' />";
					tmp_appender+="<input type='hidden' name='hotel_surcharge_rule_type["+destination_index+"][]' value='"+srchg.rule_type+"'  class='hotel_surcharge_rule' rule-attr='"+srchg.rule+"'/>";
					tmp_appender+="<input type='hidden' name='hotel_surcharge_profit["+destination_index+"][]' value='"+srchg.profit+"' />";
					tmp_appender+="<input type='hidden' name='hotel_surcharge_cost["+destination_index+"][]' value='"+srchg.cost+"' />";
					tmp_appender+="<input type='hidden' name='hotel_surcharge_price["+destination_index+"][]' value='"+(parseInt(srchg.profit)+parseInt(srchg.cost))+"' />";
					tmp_appender+="<input type='hidden' name='hotel_surcharge_rule["+destination_index+"][]' value='"+srchg.rule+"' />";
					tmp_appender+="<input type='hidden' name='hotel_surcharge_desc["+destination_index+"][]' value='"+srchg.description+"' />";
					tmp_appender+="</div>";
				}
				container.append(tmp_appender);
				//end iterate through the container ----------------------------------------------			
				 //check if this hotel is blacked out
	           	checkHotelBlackedOut();
			}else{
				//empty the container ----------------------------------------------
				container.empty();
				//append the surcharge id to 0 value ----------------------------------------------
				container.append("<input type='hidden'   name='hotel_surcharge_id[]' value='0' />");
				container.append("<input type='hidden' name='hotel_surcharge_rule_type[]' value='null'  class='hotel_surcharge_rule'/>");
			}
			
		})
		.fail(function(){
				container.empty();
				container.append("<input type='hidden'   name='hotel_surcharge_id[]' value='0' />");
		});
		
	}else{
		var hotel_surcharge_container = $('#summary_hotel_surcharges_table');
		hotel_surcharge_container.hide(); //hide the body container
		$('.summary_hotel_surcharges_table_header').hide(); //hide the header containr
	}
}
//end of get hotel surcharges ----------------------------------------------
	
//check overflows
function packageOverflows(){
	var days_overflow = true;
	var child_overflow = true;
	var bfast_overflow = true;
	var extrabed_overflow = true;

	$('.days_overflow').each(function(i,e){
		var tmp = $.trim($(e).val());
		if(tmp.length!=0){
			days_overflow = false;
		}
	});

	$('.bfast_overflow').each(function(i,e){
		var tmp = $.trim($(e).val());
		if(tmp.length!=0){
			bfast_overflow = false;
		}
	});	

	$('.extrabed_overflow').each(function(i,e){
		var tmp = $.trim($(e).val());
		if(tmp.length!=0){
			extrabed_overflow = false;
		}
	});	

	$('.child_overflow').each(function(i,e){
		var tmp = $.trim($(e).val());
		if(tmp.length!=0){
			child_overflow = false;
		}
	});	

	if(!days_overflow){
		console.log("adult overflow");
	}

	if(!bfast_overflow){
		console.log("bfast overflow");
	}

	if(!extrabed_overflow){
		console.log("extra bed overflow");
	}

	if(!child_overflow){
		console.log("child overflow");
	}

}
















//get hotelrooms sucharges ----------------------------------------------
function updateRoomSurcharges(){
}
//end of get hotel surcharges ----------------------------------------------


//function update room rates when a date is selected ----------------------------------------------
function updateRoomRatesContainers(destination_id, booking_id){
	
	//set the surcharge generation to false ----------------------------------------------
	surchargeGeneration("false");

	var package_hotel_id = $.trim($('#package_hotel_choice'+destination_id).val());
	var package_id= $('[name=package_id]').val();
	var base_url = $.trim($("#base_url_container").val());
	var url_link = base_url+"panel/gethotelroom_packagehotelid";
	console.log(base_url);
	//return and depart value ----------------------------------------------
	var depart_value = $.trim($('#datetimepickerBookingDepart').val());
	var return_value = $.trim($("#datetimepickerBookingReturn").val());

	//index of destination ----------------------------------------------
	var destination_index = $("#package_hotel_choice"+destination_id).attr("destination-index");

	//total number of destinations ----------------------------------------------
	var total_destinations = parseInt($("#total_destinations").val())-1;


	//check if the length of the package hotel id is not less than 0
	if(package_hotel_id.length!=0){
		$.ajax({
	        url: url_link,
	        data: 'package_hotel_id='+package_hotel_id+"&package_id="+package_id+"&depart_value="+depart_value+"&return_value="+return_value+"&destination_index="+destination_index+"&total_destinations="+total_destinations+"&booking_id"+booking_id,
	        type: 'POST',
	        dataType: 'html',
	        success: function(result){
	        		
	            var resultObj = result.split('_:_'); 
	            var alert_type = resultObj[0]; 
	            var html_data = resultObj[1]; 

	            $("#roomrates_container"+destination_id).empty().append(html_data);

	           	var extension = parseInt($('#extended_days').val());
	            if(extension!=0){
                	$('.extension_container:last').show();
                }
	        },
	        error: function(x){
	        	alert(x.responseText);
	        }
	    });
	}
	

}
//end of update the roomrate containers ----------------------------------------------




//isNaN
function nanChecker(integ){
	return isNaN(parseInt(integ)) ? 0 : parseInt(integ);
}

//function generate surcharges ----------------------------------------------
function generateSurcharges(element){

	//check the guests counter extension or not ----------------------------------------------
	checkTotalGuestsCounter();

	//fetch the base url
	var base_url = $('#base_url_container').val();
	
	//form for step booking
	var form_cont = $('#stephotel_form');

	//serialization of step hotel form
	var ser = form_cont.serialize();

	//surcharge loader 
	var sucharageSpinner = $('#generateSurchargeSpinner');

	//summary dynamic container 
	var surchargeContainer = $('#surchargeGeneratorContainer');

	//get extended days values 
	var extended_days = nanChecker($('#extended_days').val());

	//final checker for guest counter and room inputs ----------------------------------------------
	var final_step_booking_checker = true;
	var room_input_class = "";

	//hide the button ----------------------------------------------
	$(element).hide();

	//show the spinner
	sucharageSpinner.show();

	//get bootstrap validator ----------------------------------------------
	var boostrapValidator =  form_cont.data('bootstrapValidator');
	
	console.log(boostrapValidator);
	//validate the form ----------------------------------------------
	boostrapValidator.validate();

	//get all the invalid fields = ----------------------------------------------
	var test_invalid_fields = boostrapValidator.getInvalidFields().length;

	//check if there's an overflow error
	$('.days_overflow').each(function(i,e){
		var tmp_overflow_checker = $.trim($(e).val());
		if(tmp_overflow_checker.length){
			final_step_booking_checker = false;
		}
	});

	if(test_invalid_fields==0 && final_step_booking_checker){
		$("#supplementary_errors").hide();
		var counter = $('.hotel_package_user_num_roomrates').length;
		if(counter!=0){ 
			$.post(base_url+"json/generateSurcharges",ser,function(data){
				$('#main_surcharges_table').find('.table-responsive').empty().append(data);
				sucharageSpinner.hide();
				surchargeContainer.show();
				$(element).show();
				surchargeGeneration("true");
			}).fail(function(){
				sucharageSpinner.hide();
				surchargeContainer.hide();
				$(element).show();
				surchargeGeneration("false");
			});
		}
	}
	else{
		var pos = $(".package_destination_container").offset();

		//check if overflow...
		if(!final_step_booking_checker){
			var span_container = "";
			$("html,body").scrollTop(pos.top);

			//check if there's an overflow error
			$('.days_overflow').each(function(i,e){
				var tmp_overflow_checker = $.trim($(e).val());
				var destination_id = $(e).attr("destination-id");
				if(tmp_overflow_checker.length){
					$("#roomrates_container"+destination_id).find('.hotel_package_user_num').each(function(i,e){
						$(e).css("box-shadow","inset 0px 0px 3px red");
					});
					surchargeGeneration("false");
					span_container = "Room count must not be less than the total guest count!";
				}else{
					$("#roomrates_container"+destination_id).find('.hotel_package_user_num').each(function(i,e){
						$(e).css("box-shadow","inset 0px 0px 0px transparent");
					});
				}
			});

			//display thane supplementary errors ----------------------------------------------
			$("#supplementary_errors").empty().append(span_container).show();
		}

		//disable the hotel ----------------------------------------------
		sucharageSpinner.hide();
		surchargeContainer.hide();
		$(element).show();
		surchargeGeneration("false");
	}

}
//end of generate surcharges ----------------------------------------------

//fill the surcharge cotainer ----------------------------------------------
function surchargeGeneration(boolie){
	var surchargeGenerator = $('#package_surcharge_generation');
	surchargeGenerator.val(boolie);
}
//end of fill the surcharge container ----------------------------------------------


//check if the selected hotel is blacked out ----------------------------------------------
function checkHotelBlackedOut(){
	$('.package_destination_container').each(function(i,e){
		var destination_id = $(e).attr("destination-id");

		var container = $('#destination_hotel_surcharges'+destination_id);

		//set the flag to false ----------------------------------------------
		var checker_if_blackout = false;
		//set the blackout container to a variable ----------------------------------------------
		var blackout_container = $('#blackout_error_container'+destination_id);
		//set hotel name to a variable ----------------------------------------------
		var set_hotel_name = $.trim($("#package_hotel_name"+destination_id).val());

		

		//set the tmp appender to a default value ----------------------------------------------
		var tmp_appender = "<div class='text-center'>";
			tmp_appender += "<div style='font-weight:bold;'><i class='fa fa-warning'></i> Blackout Error</div>";
			tmp_appender += "<div>"+set_hotel_name+" is blacked out during this date range</div>";


		//loop through the surcharge value ----------------------------------------------
		container.find('.hotel_surcharge_rule').each(function(i,e){
			var tmp = $.trim($(e).val());
			var rule = $.trim($(e).attr('rule-attr'));
			if(tmp.length!=0){
				if(tmp=="blackout"){
					checker_if_blackout  = true;
					tmp_appender += "<div>"+rule+"</div>";
				}
			}
		});
		//end of loop through the surcharge value ----------------------------------------------

		tmp_appender += "</div>";

		//append the container of the error ----------------------------------------------
		

		
		//check if the hotel has a surcharge rule ----------------------------------------------
		if(checker_if_blackout){
			blackout_container.empty().append(tmp_appender).show();
			container.empty();
			$("#package_hotel_choice"+destination_id).find("option").removeAttr("selected");
			$("#package_hotel_choice"+destination_id).find("option:first").attr("selected","selected");
			$("#roomrates_container"+destination_id).empty();
		}else{
			//declare the blackout container ----------------------------------------------
			blackout_container.hide();
			blackout_container.empty();
		}

	});

/*	//set the container ----------------------------------------------
	
	//end of check if the hotel has a surcharge rule ----------------------------------------------*/
}
//end of check if the selected hotel is blacked out ----------------------------------------------


/*
*get the current pax of a group
*/
function getDestinationPax(destination){
	var current_pax_destination = 0;
	destination.find('.total_overflow_counter').each(function(i,e){
		var tmp = nanChecker($(e).val());
		current_pax_destination += tmp;
	});
	return current_pax_destination;
}